/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoInit;

import com.soleil.nanoscopium.hdf5Opener.Hdf5Handler;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * Created by antoine bergamaschi on 14/01/14.
 */
public class SortedPanel extends JPanel {
    public static final int IMAGE=0,SPECTRE=1,SAI=2,MOTORS=3;

    private int ID = 0;
    private DefaultListModel<DataHdf5> displayNames = new DefaultListModelInner<>();
    private JListSorted<DataHdf5> jList;
    private final RXTomoJViewController controller;

    public SortedPanel(final RXTomoJViewController controller,int ID){
        super();
        this.ID = ID;
        this.controller  = controller;
        this.setLayout(new GridBagLayout());
        this.build();
        this.jList.addFocusListener(new CustomFocusListener(this.jList));
    }

    private void build(){
//      Set Border
        TitledBorder title = BorderFactory.createTitledBorder(this.controller.getRessourceValue("label_Title_Sorted_"+this.ID));
        this.setBorder(title);

//      Set the Jlist properties for drag n drop
        this.jList  =  new JListSorted<>(this.displayNames);

        //Add scroll
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(jList);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setPreferredSize(jList.getPreferredSize());

        Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, this, scrollPane);
    }


    public void setNewModel(ArrayList<String> model){
        for(String elem : model){
            this.displayNames.addElement(new DataHdf5(elem,this.displayNames));
        }
    }

    public void resetModel(){
        this.displayNames.removeAllElements();
    }

    public void addToModel(String datasetName, DefaultListModel oldModel){
        DataHdf5 dataHdf5 = new DataHdf5(datasetName,this.displayNames);
        this.displayNames.addElement(dataHdf5);
        dataHdf5.setOldModel(oldModel);
    }

    public void addToModel(Hdf5Handler.Dataset_Iterate_cb_o1.DatasetObject datasetName, DefaultListModel oldModel){
        DataHdf5 dataHdf5 = new DataHdf5(datasetName,this.displayNames);
        this.displayNames.addElement(dataHdf5);
        dataHdf5.setOldModel(oldModel);
    }

    public void updateSessionObject(){
        for ( int i = 0 ; i < displayNames.getSize() ; i++) {
            controller.updateSessionObjectList(displayNames.get(i), ID);
        }
    }

    //Inner Class Jlist Listener ( Use to Update the Model
    private class DefaultListModelInner<E> extends DefaultListModel<E>{
        public DefaultListModelInner(){
            super();
        }

        @Override
        protected void fireContentsChanged(Object source, int index0, int index1) {
            super.fireContentsChanged(source, index0, index1);
            System.err.println("Test");
        }

        @Override
        protected void fireIntervalAdded(Object source, int index0, int index1) {
            super.fireIntervalAdded(source, index0, index1);
            DataHdf5  dataHdf5 = (DataHdf5) ((DefaultListModelInner) source).getElementAt(index0);
            dataHdf5.setCurrentModel(this);
//            controller.updateSessionObjectList(dataHdf5, ID);
        }

        @Override
        protected void fireIntervalRemoved(Object source, int index0, int index1) {
            super.fireIntervalRemoved(source, index0, index1);
        }
    }

    //TODO override the ListCellRender ? to implement a better way to delete or other option selection
    private class JListSorted<E> extends JList<E>{
        private JListSorted<E> self;
        private int index;

        public JListSorted(DefaultListModel<E> listModel){
            super(listModel);
            self = this;
            index = 0;
            this.setDropMode(DropMode.ON_OR_INSERT);
            this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            this.setTransferHandler(new ListTransferHandler(displayNames, this.getClass().toString()));


            this.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent e) {
                    if (SwingUtilities.isRightMouseButton(e)) {
                        index = self.locationToIndex(e.getPoint());
                        JPopupMenu jPopupMenu = new JPopupMenu();
                        JMenuItem removeMenu = new JMenuItem("remove") {
                            @Override
                            protected void fireActionPerformed(ActionEvent event) {
                                super.fireActionPerformed(event);
                                //Get the selected dataHdf5  object
                                DataHdf5 dataHdf5 = (DataHdf5) ((DefaultListModelInner) self.getModel()).getElementAt(index);
                                //Remove this hdf5Object from the sessionObject
//                                controller.updateSessionObjectList(dataHdf5, ID);
                                //Reset dataHdf5 object Position
                                dataHdf5.removeFromModel();
                            }
                        };

                        jPopupMenu.add(removeMenu);
                        jPopupMenu.show(e.getComponent(), e.getX(), e.getY());
                    }
                }
            });

//            this.addMouseListener(mouseListener);

        }

        @Override
        public String getToolTipText(MouseEvent event) {
//            ((DataHdf5)event.getSource()).getToolTipText();
            int index = locationToIndex (event.getPoint ());
            if (index > -1){
                ListModel lm = (ListModel) getModel ();
                DataHdf5 link = (DataHdf5) lm.getElementAt (index);
                return link.getToolTipText();

            }
            else {
                return null;
            }
        }
    }

}
