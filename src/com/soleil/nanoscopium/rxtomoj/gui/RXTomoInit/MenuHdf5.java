/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoInit;

import com.soleil.nanoscopium.hdf5Opener.Hdf5Handler;
import com.soleil.nanoscopium.hdf5Opener.Hdf5PlugIn;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import ij.io.OpenDialog;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.HashMap;

/**
 * Created by antoine bergamaschi on 15/01/14.
 * @deprecated
 */
public class MenuHdf5 extends JMenu {
    private final RXTomoJViewController controller;

    public MenuHdf5(String title,RXTomoJViewController controller){
        super(title);
        this.controller = controller;
        this.add(new JMenuItem0(this.controller.getRessourceValue("title_Menu_principal_sub_openHdf5")));
        this.add(new JMenuItem1(this.controller.getRessourceValue("title_Menu_principal_sub_openSessionObj")));
        this.add(new JMenuItem2(this.controller.getRessourceValue("title_Menu_principal_saveObj")));
    }

    //Inner function
    private class JMenuItem0 extends JMenuItem{
        public JMenuItem0(String title){
            super(title);
        }

        @Override
        protected void fireActionPerformed(ActionEvent event) {
            super.fireActionPerformed(event);
            controller.openNewHdf5Frame();
        }
    }

    //Inner function
    private class JMenuItem1 extends JMenuItem{
        public JMenuItem1(String title){
            super(title);
        }

        @Override
        protected void fireActionPerformed(ActionEvent event) {
            super.fireActionPerformed(event);

            OpenDialog openDialog  = new OpenDialog("Select Session Object",OpenDialog.getLastDirectory(),OpenDialog.getLastName());

            if ( openDialog.getPath() != null){
                RXTomoJ.getInstance().getModelController().getSessionController().openSessionObject(openDialog.getPath());

                Hdf5PlugIn hdf5PlugIn = new Hdf5PlugIn();
                hdf5PlugIn.run(RXTomoJ.getInstance().getModelController().getSessionController().getPathToHdf5());
               HashMap<String, Hdf5Handler.Dataset_Iterate_cb_o1.DatasetObject> datasets= hdf5PlugIn.getDatasetInformation();
                ((Hdf5openerJFrame)controller.getFrame(RXTomoJViewController.HDF5_FRAME_CLASS)).setLockSessionUpdate(true);
                ((Hdf5openerJFrame)controller.getFrame(RXTomoJViewController.HDF5_FRAME_CLASS)).updateDataFromHdf5(datasets);
                ((Hdf5openerJFrame)controller.getFrame(RXTomoJViewController.HDF5_FRAME_CLASS)).setLockSessionUpdate(false);
                hdf5PlugIn.closeHdf5File();
            }
        }
    }

    private class JMenuItem2 extends JMenuItem{
        public JMenuItem2(String title){
            super(title);
        }

        @Override
        protected void fireActionPerformed(ActionEvent event) {
            super.fireActionPerformed(event);
            RXTomoJ.getInstance().getModelController().getSessionController().saveSessionObject();
//            SessionObject.getCurrentSessionObject().writteXML(null);
        }
    }
}
