/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoInit;

import com.soleil.nanoscopium.hdf5Opener.Hdf5Handler;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by antoine bergamaschi on 15/01/14.
 */
public class DataHdf5 extends JPanel{

    private String datasetName;
    private DefaultListModel  oldModel;
    private DefaultListModel  currentModel;

    public DataHdf5(String datasetName,DefaultListModel oldModel){
        super();
        this.datasetName = datasetName;
        this.oldModel = oldModel;
        this.currentModel = oldModel;
    }

    public DataHdf5(Hdf5Handler.Dataset_Iterate_cb_o1.DatasetObject object,DefaultListModel oldModel){
        super();
        this.datasetName = object.getName();
        String tooltips = "<html>";
        for ( String key : object.getAttributes().keySet()){
            tooltips += "<div>";
            tooltips += key;
            tooltips += " :: ";
            tooltips += object.getAttributes().get(key);
            tooltips +="</div>";
        }
        tooltips +="</html>";
        this.setToolTipText(tooltips);
        this.oldModel = oldModel;
        this.currentModel = oldModel;

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                super.mouseEntered(e);
//                System.out.println("Test");
            }
        });
    }

    public JPanel getText(){
        JPanel panel = new JPanel();
        panel.add(new JLabel("Super Test"));
        return panel;
    }

    @SuppressWarnings("unchecked")
    public void removeFromModel(){
        this.currentModel.removeElement(this);
        DefaultListModel tmp = this.currentModel;
        this.currentModel = this.oldModel;
        this.oldModel = tmp;
        this.currentModel.addElement(this);
    }

    public void setCurrentModel(DefaultListModel defaultListModel){
        this.oldModel = currentModel;
        this.currentModel = defaultListModel;
    }

    public void setOldModel(DefaultListModel defaultListModel){
        this.oldModel  = defaultListModel;
    }

    @Override
    public String toString() {
        return this.datasetName;
    }
}
