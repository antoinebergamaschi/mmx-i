/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoInit;

import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.DialogMessage;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import ij.gui.YesNoCancelDialog;
import ij.io.SaveDialog;

import javax.swing.*;
import java.awt.*;

/**
 * Created by bergamaschi on 04/02/14.
 */
public class ButtonPanel extends JPanel {

    private JButton valide;
    private JButton cancel;
    private JButton reset;
    private RXTomoJViewController controller;

    public ButtonPanel(RXTomoJViewController controller){
        super(new GridBagLayout());
        this.controller = controller;
        this.build();
    }

    private void build(){
        this.valide = new JButton(this.controller.getRessourceValue("Hdf5openerJFrame_button_validate"));
        this.cancel = new JButton(this.controller.getRessourceValue("Hdf5openerJFrame_button_cancel"));
        this.reset = new JButton(this.controller.getRessourceValue("Hdf5openerJFrame_button_reset"));

        this.valide.addActionListener(e -> {
            JFrame currentHdf5Frame = controller.getFrame(RXTomoJViewController.HDF5_FRAME_CLASS);
            YesNoCancelDialog yesNoCancelDialog = new YesNoCancelDialog(currentHdf5Frame,controller.getRessourceValue("Hdf5openerJFrame_message_title"),
                    controller.getRessourceValue("Hdf5openerJFrame_message_text"));
            //If Cancel return imediatly
            if ( yesNoCancelDialog.cancelPressed() ){
                return;
            }
            //If Yes Erase or create a new SessionObject file
            else if(yesNoCancelDialog.yesPressed()){
                //Set the new SessionObject
                ((Hdf5openerJFrame)controller.getFrame(RXTomoJViewController.HDF5_FRAME_CLASS)).updateSessionObject();

//                RXTomoJ.getInstance().getModel().saveSessionObject();
                //Save session
                if ( RXTomoJ.getInstance().getModelController().getSessionController().canSaveSessionObject() ){
                    RXTomoJ.getInstance().getModelController().getSessionController().saveSessionObject();
                }else{
                    //Open dialog where to save the session object file
                    SaveDialog saveDialog = new SaveDialog(controller.getRessourceValue("Hdf5openerJFrame_saveDialog_title")," SessionObject",".xml");
                    if ( !RXTomoJ.getInstance().getModelController().getSessionController().setSessionObjectPath(saveDialog.getDirectory()+saveDialog.getFileName()) ){
                        DialogMessage.showMessage(controller.getRessourceValue("Hdf5openerJFrame_error_save"),controller.getRessourceValue("rxTomoJViewController_error_title"), DialogMessage.ERROR);
                    }else{
                        RXTomoJ.getInstance().getModelController().getSessionController().saveSessionObject();
                    }
                }
            }
            controller.removeFrame(currentHdf5Frame);
        });

        this.cancel.addActionListener(e ->  {
            JFrame currentHdf5Frame = controller.getFrame(RXTomoJViewController.HDF5_FRAME_CLASS);
            controller.removeFrame(currentHdf5Frame);
            if ( controller.getOldSessionPath() != null ) {
                RXTomoJ.getInstance().getModelController().getSessionController().openSessionObject(controller.getOldSessionPath());
                controller.setOldSessionPath(null);
            }
            System.err.println("Warning :: Possible update Bug :"+this.getClass().toString());
//            ((Updatable)controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME)).update();
        });

        this.reset.addActionListener(e -> {
            JFrame currentHdf5Frame = controller.getFrame(RXTomoJViewController.HDF5_FRAME_CLASS);
            ((Hdf5openerJFrame)currentHdf5Frame).resetInterface();
        });

        //anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
        Utils.addGridBag(GridBagConstraints.EAST, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, this, this.reset);

        //anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 0, this, this.valide);

        //anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
        Utils.addGridBag(GridBagConstraints.WEST, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 2, 0, this, this.cancel);
    }

}
