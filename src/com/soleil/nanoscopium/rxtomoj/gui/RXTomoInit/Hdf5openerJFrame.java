/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoInit;


import com.soleil.nanoscopium.hdf5Opener.Hdf5Handler;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by antoine bergamaschi on 13/01/14.
 *
 * JFrame implementing the code for Selection of Hdf5 dataset.
 */
public class Hdf5openerJFrame extends JFrame {

    private RXTomoJViewController controller;

    private JPanel currentDatasetInfo;
    private JPanel hdf5_information;
    private SortedPanel imageRecap;
    private SortedPanel motorPosition;
    private SortedPanel sai;
    private SortedPanel spectre;
    private ButtonPanel buttonPanel;

    public Hdf5openerJFrame(RXTomoJViewController controller){
        super();
        this.controller = controller;
        this.addWindowListener(new Hdf5OpenerWindowListener(this.controller,this));
        this.build();
    }

    /**
     * Set the JFrame basic Option, Listener, position
     */
    private void build(){
        this.setTitle(this.controller.getRessourceValue("Hdf5openerJFrame_title"));

        //Set the content of the Jpanel
        this.setContentPane(this.buildContentPane());

        this.setVisible(true);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize(new Dimension(1300,800));
        this.setMinimumSize(new Dimension(1000,500));
        Dimension windowSize = this.getSize();

        int windowX = Math.max(0, (screenSize.width  - windowSize.width ) / 2);
        int windowY = Math.max(0, (screenSize.height - windowSize.height) / 2);

        setLocation(windowX, windowY);
    }

    /**
     * Initialize every component in this JFrame
     * @return JPanel, the constructed Interface
     */
    final private JPanel buildContentPane(){
        JPanel principal = new JPanel(new GridBagLayout());
        JPanel hdf5_sort_information  =  new JPanel(new GridBagLayout());
        this.hdf5_information  = new JPanel(new GridBagLayout());
        this.buttonPanel = new ButtonPanel(this.controller);

//        principal.setBackground(Color.blue);
        hdf5_sort_information.setBackground(Color.darkGray);
//        hdf5_information.setBackground(Color.green);


//        currentDatasetInfo = new JPanel();
//        currentDatasetInfo.setBackground(Color.RED);

        //Image
        this.imageRecap = new SortedPanel(this.controller,SortedPanel.IMAGE);
        this.motorPosition = new SortedPanel(this.controller,SortedPanel.MOTORS);
        this.spectre = new SortedPanel(this.controller,SortedPanel.SPECTRE);
        this.sai = new SortedPanel(this.controller,SortedPanel.SAI);

        Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, hdf5_sort_information, this.imageRecap);
        Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 1, hdf5_sort_information, this.motorPosition);
        Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 0, hdf5_sort_information, this.spectre);
        Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 1, hdf5_sort_information, this.sai);


        Border raisedbevel = BorderFactory.createRaisedBevelBorder(),
                loweredbevel = BorderFactory.createLoweredBevelBorder(),
                compound = BorderFactory.createCompoundBorder(raisedbevel, loweredbevel);

        hdf5_information.setBorder(compound);
        hdf5_sort_information.setBorder(compound);

        //Finally add every component in the BIG principal component

        //anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 0, principal, hdf5_sort_information);

        //anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 0.5, 1, 0, 0, 1, 1, 0, 0, principal, hdf5_information);

        //anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 0, 0, 0, 0, 3, 1, 0, 1, principal, buttonPanel);

//        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 0, 0, 0, 0, 1, 1, 2, 0, principal, currentDatasetInfo);

        return principal;
    }


    /**
     * Update this frame with data obtained in the Hdf5 but not yet setted in the sessionObject class ( model parameter )
     * @param datasets Object[String datasetName ][ int Dimension ]
     */
    @Deprecated
    public void updateDataFromHdf5(Object[][] datasets){

        this.resetInterface();

        HashMap<Integer,ArrayList<String> > result = new HashMap<>();

        for ( int i = 0 ; i < datasets[1].length ; i++  ) {
            ArrayList<String> obj = result.get(datasets[1][i]);
            if ( obj == null ){
                obj = new ArrayList<>();
            }
            obj.add((String) datasets[0][i]);

            result.put((Integer) datasets[1][i],obj);
        }
        for ( Integer index :  result.keySet()){
            this.addHdf5Object(result.get(index), index);
        }
        //Validate the Frame to update the view
        this.validate();
    }

    /**
     * Update this frame with data obtained in the Hdf5 but not yet setted in the sessionObject class ( model parameter )
     * @param datasets Object[String datasetName ][ int Dimension ]
     */
    public void updateDataFromHdf5(HashMap<String, Hdf5Handler.Dataset_Iterate_cb_o1.DatasetObject> datasets){

        this.resetInterface();

        HashMap<Integer,ArrayList<Hdf5Handler.Dataset_Iterate_cb_o1.DatasetObject> > result = new HashMap<>();

        for ( String datasetName : datasets.keySet() ){
            ArrayList<Hdf5Handler.Dataset_Iterate_cb_o1.DatasetObject> datasetObjects = result.get(datasets.get(datasetName).getRank());
            if ( datasetObjects == null ){
                datasetObjects = new ArrayList<>();
            }
            datasetObjects.add(datasets.get(datasetName));
            result.put(datasets.get(datasetName).getRank(),datasetObjects);
        }

        for ( Integer index :  result.keySet()){
            this.addHdf5Objects(result.get(index), index);
        }

        //Validate the Frame to update the view
        this.validate();
    }

    /**
     * Add the names in the specified dimension panel if the session object does not already contains them
     * @param datasetObjects String, a Dataset retrieve from a HDF5 file
     * @param dimension int, the number of dimension of this dataset
     */
    public void addHdf5Objects(ArrayList<Hdf5Handler.Dataset_Iterate_cb_o1.DatasetObject> datasetObjects, int dimension){
        DimensionPanel p = new DimensionPanel(this.controller,dimension);
        //For each Dataset names
        for ( Hdf5Handler.Dataset_Iterate_cb_o1.DatasetObject datasetObject : datasetObjects ){
            //Check in SessionObject if this dataset is already defined
            int id = RXTomoJ.getInstance().getModelController().getSessionController().getSessionObjectDataType(datasetObject.getName());
            if ( id < 0  ){
                p.addToModel(datasetObject);
            }
            //Else enter in one of the sortedPane
            else{
                switch (id){
                    case SortedPanel.IMAGE:
                        this.imageRecap.addToModel(datasetObject,p.getDisplayNames());
                        break;
                    case SortedPanel.SAI:
                        this.sai.addToModel(datasetObject,p.getDisplayNames());
                        break;
                    case SortedPanel.SPECTRE:
                        this.spectre.addToModel(datasetObject,p.getDisplayNames());
                        break;
                    case SortedPanel.MOTORS:
                        this.motorPosition.addToModel(datasetObject,p.getDisplayNames());
                        break;
                    default:
                        System.err.println("Unknown Type "+id);
                }
            }
        }
        Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, DimensionPanel.INDEX_IN_GRID, this.hdf5_information, p);
    }

    /**
     * Add the names in the specified dimension panel if the session object does not already contains them
     * @param names String, a Dataset retrieve from a HDF5 file
     * @param dimension int, the number of dimension of this dataset
     */
    public void addHdf5Object(ArrayList<String> names, int dimension  ){
        DimensionPanel p = new DimensionPanel(this.controller,dimension);
        //For each Dataset names
        for ( String name : names ){
            //Check in SessionObject if this dataset is already defined
            int id = RXTomoJ.getInstance().getModelController().getSessionController().getSessionObjectDataType(name);
            if ( id < 0  ){
                p.addToModel(name);
            }
            //Else enter in one of the sortedPane
            else{
                switch (id){
                    case SortedPanel.IMAGE:
                        this.imageRecap.addToModel(name,p.getDisplayNames());
                        break;
                    case SortedPanel.SAI:
                        this.sai.addToModel(name,p.getDisplayNames());
                        break;
                    case SortedPanel.SPECTRE:
                        this.spectre.addToModel(name,p.getDisplayNames());
                        break;
                    case SortedPanel.MOTORS:
                        this.motorPosition.addToModel(name,p.getDisplayNames());
                        break;
                    default:
                        System.err.println("Unknown Type "+id);
                }
            }
        }
        Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, DimensionPanel.INDEX_IN_GRID, this.hdf5_information, p);
    }

    /**
     * Reset the entire Hdf5openerJFrame Interface
     */
    final public void resetInterface(){
        this.hdf5_information.removeAll();
        this.imageRecap.resetModel();
        this.motorPosition.resetModel();
        this.spectre.resetModel();
        this.sai.resetModel();
    }

    public void updateSessionObject(){
        RXTomoJ.getInstance().getModelController().getSessionController().resetRawDataset();
        imageRecap.updateSessionObject();
        motorPosition.updateSessionObject();
        sai.updateSessionObject();
        spectre.updateSessionObject();
    }

//
//    public void setDatasetInformation(){
//        currentDatasetInfo.setToolTipText();
//    }

    public void setLockSessionUpdate(boolean bol){
        this.controller.setLockSessionUpdate(bol);
    }

}
