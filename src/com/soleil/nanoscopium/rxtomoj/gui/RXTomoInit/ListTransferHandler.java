/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoInit;

import javax.swing.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

/**
 * Custom Transfert Function for List Exchange
 * Created by antoine bergamaschi on 14/01/14.
 */
public class ListTransferHandler extends TransferHandler {
    DataFlavor listFlavor;
    private int[] indices = null;
    private String className;
    private DefaultListModel<DataHdf5> defaultListModel;
    private int addIndex = -1; //Location where items were added
    private int addCount = 0;  //Number of items added.

    public ListTransferHandler(DefaultListModel<DataHdf5> defaultListModel,String className){
        super();
        this.className = className;
        this.defaultListModel = defaultListModel;

        String mimeType = DataFlavor.javaJVMLocalObjectMimeType
                + ";class=\""
                + java.util.List.class
                .getName() + "\"";
        try {
            listFlavor = new DataFlavor(mimeType);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public boolean canImport(TransferHandler.TransferSupport info) {
        // Check for String flavor
        if (!info.isDataFlavorSupported(listFlavor)) {
            return false;
        }
        else if( this.className.compareToIgnoreCase(DimensionPanel.class.toString())==0) {
            return false;
        }

//        if ( info. )
        return true;
    }

    protected Transferable createTransferable(JComponent c) {
        return new ElementsTransferable(exportString(c));
    }

    public int getSourceActions(JComponent c) {
        return TransferHandler.MOVE;
    }


    @SuppressWarnings("unchecked")
    public boolean importData(TransferSupport info) {
        if (!info.isDrop()) {
            return false;
        }


        JList.DropLocation dl = (JList.DropLocation)info.getDropLocation();
        int index = dl.getIndex();
//        boolean insert = dl.isInsert();

        // Get the string that is being dropped.
        Transferable t = info.getTransferable();
        List<DataHdf5> data;
        try {
            data = (List<DataHdf5>)t.getTransferData(listFlavor);
        }
        catch (Exception e) {
            e.printStackTrace();
            return false; }

        // Perform the actual import.
//        if (insert) {
        for (DataHdf5 item :  data ){

            defaultListModel.addElement(item);
        }
//        } else {
//            for (String item :  data ){
//                defaultListModel.add(index, item);
//                index++;
//            }
//        }
        return true;
    }

    protected void exportDone(JComponent c, Transferable data, int action) {
        cleanup(c, action == TransferHandler.MOVE);
    }

    //Bundle up the selected items in the list
    //as a single string, for export.
    @SuppressWarnings("unchecked")
    protected List<String> exportString(JComponent c) {
        JList list = (JList)c;
        indices = list.getSelectedIndices();
//            Object[] values = list.getSelectedValues();
        List<String> values = (List<String>)list.getSelectedValuesList();

        return values;
    }


    //If the remove argument is true, the drop has been
    //successful and it's time to remove the selected items
    //from the list. If the remove argument is false, it
    //was a Copy operation and the original list is left
    //intact.
    protected void cleanup(JComponent c, boolean remove) {
        if (remove && indices != null) {
            JList source = (JList)c;
            DefaultListModel model  = (DefaultListModel)source.getModel();

            for (int i = indices.length - 1; i >= 0; i--) {
                model.remove(indices[i]);
            }
        }
        indices = null;
        addCount = 0;
        addIndex = -1;
    }

    public class ElementsTransferable implements Transferable{
        private List<String> toTransfert;

        public ElementsTransferable(List<String> toTransfert){
            this.toTransfert = toTransfert;
        }

        @Override
        public DataFlavor[] getTransferDataFlavors() {
            DataFlavor  list;
            DataFlavor[] flavors = new DataFlavor[1];
            String mimeType = DataFlavor.javaJVMLocalObjectMimeType
                    + ";class=\""
                    + java.util.List.class
                    .getName() + "\"";
            try {
                list = new DataFlavor(mimeType);
                 flavors[0] = list;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            return flavors;
        }

        @Override
        public boolean isDataFlavorSupported(DataFlavor flavor) {
            return listFlavor.equals(flavor);
        }

        @Override
        public List<String> getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
            return this.toTransfert;
        }
    }

}
