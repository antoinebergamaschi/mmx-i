/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoInit;

import com.soleil.nanoscopium.hdf5Opener.Hdf5Handler;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * Dimension Panel used to display the hdf5 data set
 * Created by antoine bergamaschi on 13/01/14.
 */
public class DimensionPanel extends JPanel {
    private RXTomoJViewController controller;
    private DefaultListModel<DataHdf5> displayNames = new DefaultListModel<DataHdf5>();
    private JListDimensoin<DataHdf5> jList;
    private int numberOfDimension;
    public static int INDEX_IN_GRID = 0;

    /**
     * Constructor
     * @param controller RXTomoJViewController, the view controller
     * @param numberOfDimension int, number of dimension of the contained data set
     * @see com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController
     */
    public DimensionPanel(final RXTomoJViewController controller,int numberOfDimension){
        super();
        this.controller  = controller;
        this.numberOfDimension = numberOfDimension;
        this.setLayout(new GridBagLayout());
        this.build();

        jList.addFocusListener(new CustomFocusListener(this.jList));
        DimensionPanel.INDEX_IN_GRID++;
    }

    /**
     * Private Function use to create the interface
     */
    private void build(){
//        Set Border
        TitledBorder titler = BorderFactory.createTitledBorder(this.controller.getRessourceValue("label_Title_Dimension_"+this.numberOfDimension));
        this.setBorder(titler);

        //Set the Jlist properties for drag n drop
        this.jList  =  new JListDimensoin<>(this.displayNames);
        jList.setDragEnabled(true);
        jList.setTransferHandler(new ListTransferHandler(this.displayNames,this.getClass().toString()));

        //Add scroll
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(jList);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setPreferredSize(jList.getPreferredSize());

        this.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,this,scrollPane);
    }


    /**
     * Add The entire {@link ArrayList} in the JList model
     * @param model ArrayList the data set names to add in the model
     */
    public void setNewModel(ArrayList<String> model){
//        int i = this.displayNames.getSize();
        for(String elem : model){
            this.displayNames.addElement(new DataHdf5(elem,this.displayNames));
//            this.displayNames.add(i,elem);
//            i++;
        }
    }

    public void addToModel(String datasetName){
        this.displayNames.addElement(new DataHdf5(datasetName,this.displayNames));
    }

    public void addToModel(Hdf5Handler.Dataset_Iterate_cb_o1.DatasetObject datasetName){
        this.displayNames.addElement(new DataHdf5(datasetName,this.displayNames));
    }

    //TODO implement this object when an ohter way than dnd is used
    public void removeObject(String ID){
        System.err.println("Not Implemented yet");
    }


    public DefaultListModel<DataHdf5> getDisplayNames() {
        return displayNames;
    }


    //TODO override the ListCellRender ? to implement a better way to delete or other option selection
    private class JListDimensoin<E> extends JList<E>{

        public JListDimensoin(DefaultListModel<E> listModel){
            super(listModel);
        }

        @Override
        public String getToolTipText(MouseEvent event) {
//            ((DataHdf5)event.getSource()).getToolTipText();
            int index = locationToIndex (event.getPoint ());
            if (index > -1){
                ListModel lm = (ListModel) getModel ();
                DataHdf5 link = (DataHdf5) lm.getElementAt (index);
                return link.getToolTipText();

            }
            else {
                return null;
            }
        }
    }

    private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
        c.fill = fill;
        c.anchor = anchor;
        c.weightx = weightx;
        c.weighty = weighty;
        c.ipadx = ipadx;
        c.ipady = ipady;
        c.gridx = gridx;
        c.gridy = gridy;
        c.gridwidth = width;
        c.gridheight = height;
        container.add(component, c);
    }

}
