/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.utils;

import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.component.RXImageComponentIJ;
import com.soleil.nanoscopium.rximage.component.RXimageComponent;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import ij.ImageListener;
import ij.ImagePlus;
import ij.WindowManager;
import ij.gui.ImageCanvas;
import ij.gui.ImageWindow;
import ij.gui.RoiListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by bergamaschi on 24/04/2014.
 */
public class JPanelImage extends JPanel implements ScrollUpdatable {
    protected RXImageComponentIJ componentIJ;
    protected Rectangle currentRoi;

    public JPanelImage(){
        super(new GridBagLayout());
        this.setBackground(Color.WHITE);
        this.setPreferredSize(new Dimension(100, 200));
        this.build();
    }

    public void setImage(RXImage image){
        this.removeAll();

        componentIJ = new RXImageComponentIJ(image);
        JPanel panel = componentIJ.getImage();
        Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.HORIZONTAL,1,0,0,0,3,1,0,0,this,background_panel());
        Utils.addGridBag(GridBagConstraints.EAST, 0, 0, 0, 0, GridBagConstraints.VERTICAL, 0, 1, 0, 0, 1, 1, 0, 1, this, background_panel());
        Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 1, this, panel);
        Utils.addGridBag(GridBagConstraints.WEST, 0, 0, 0, 0, GridBagConstraints.VERTICAL, 0, 1, 0, 0, 1, 1, 2, 1, this, background_panel());
        Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.HORIZONTAL, 1, 0, 0, 0, 3, 1, 0, 2, this, background_panel());

        addRoiListener();
    }


    private void build(){
    }

    protected void addRoiListener(){
        componentIJ.addListener((RoiListener) (imp, id) -> {
            if (imp != null ) {
                currentRoi = imp.getRoi().getBounds();
            }
        });
    }

    public RXImageComponentIJ getImageComponent(){
        return componentIJ;
    }

    @Override
    public void update(long position) {
        this.componentIJ.updateImage((int) position);
    }

    public JPanel background_panel(){
        JPanel b = new JPanel();
        b.setBackground(Color.WHITE);
        return b;
    }


    public Rectangle getCurrentRoi() {
        return currentRoi;
    }

}
