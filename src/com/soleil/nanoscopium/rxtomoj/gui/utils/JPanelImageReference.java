/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.utils;

import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import ij.ImageListener;
import ij.ImagePlus;

/**
 * Created by bergamaschi on 18/09/2015.
 */
public class JPanelImageReference extends JPanelImage {
    private boolean lockModification = false;

    @Override
    public void setImage(RXImage image){
        super.setImage(image);

        componentIJ.addListener(new ImageListener() {
            @Override
            public void imageOpened(ImagePlus imp) {
            }

            @Override
            public void imageClosed(ImagePlus imp) {

            }

            @Override
            public void imageUpdated(ImagePlus imp) {
                if ( componentIJ.isImage(imp) ) {
                    int[][] rec = RXTomoJ.getInstance().getModelController().getDataController().getReductionInfo().getReductionROI();
                    imp.setRoi(rec[0][0],rec[1][0],rec[0][1]-rec[0][0],rec[1][1]-rec[1][0]);
//                    imp.setRoi(RXTomoJ.getInstance().getModelController().getDataController().getComputeSpot(ComputationUtils.StackType.ABSORPTION).reconstructionToRec());
                }
            }
        });
    }

    public void lockROIModification(boolean bol){
        lockModification = bol;
    }
}
