/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.utils;

//Created By bergamaschi on 07/08/2014

import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;

import javax.swing.*;

/**
 * Class to generate Error or Warning Messages
 *
 * @author Antoine Bergamaschi
 */
public class DialogMessage {

    private RXTomoJViewController controller;
    public final static int ERROR=2,WARNING=1,NORMAL=0;
    private static DialogMessage instance;

    private DialogMessage(RXTomoJViewController controller){
        this.controller = controller;
        this.instance = this;
    }

    public static void init(RXTomoJViewController controller){
        new DialogMessage(controller);
    }

    public static void showMessage(String message, String title, int type ){
        switch (type){
            case ERROR:
                instance.errorMessage(message,title);
                break;
            case WARNING:
                instance.warningMessage(message,title);
                break;
            case NORMAL:
                instance.normalMessage(message,title);
                break;
            default:
                break;
        }
    }

    public static void showMessage_ID(String message,String message2, String title, int type ){
        //Test if DialogMessage has been init by the GUI
        if ( instance != null ) {
            showMessage(instance.getRessource(message) + message2, instance.getRessource(title), type);
        }
    }

    private String getRessource(String ID){
        return controller.getRessourceValue(ID);
    }

    private void warningMessage(String message, String title){

        JOptionPane.showMessageDialog(controller.getCurrentFrame(),
                message,
                title,
                JOptionPane.WARNING_MESSAGE);
    }

    private void errorMessage(String message, String title){

        JOptionPane.showMessageDialog(controller.getCurrentFrame(),
                message,
                title,
                JOptionPane.ERROR_MESSAGE);
    }

    private void normalMessage(String message, String title){

        JOptionPane.showMessageDialog(controller.getCurrentFrame(),
                message,
                title,
                JOptionPane.PLAIN_MESSAGE);
    }

}
