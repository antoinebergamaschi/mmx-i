/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.utils;

import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.gui.LoadingBar;
import com.soleil.nanoscopium.rxtomoj.model.utils.ProgressFunction;

import javax.swing.*;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Random;

/**
 * Created by antoine bergamaschi on 24/01/14.
 */
public class Utils {

    private static LoadingBar l;
    public static Random random = new Random();

    //anchors,top,left,bottom,right,fill,wX,wY,padx,pady,x,y
    public static void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, Container container, Component component){
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
        c.fill = fill;
        c.anchor = anchor;
        c.weightx = weightx;
        c.weighty = weighty;
        c.ipadx = ipadx;
        c.ipady = ipady;
        c.gridx = gridx;
        c.gridy = gridy;
        c.gridwidth = width;
        c.gridheight = height;
        container.add(component, c);
    }


    public static ImageIcon resizeIcon(int height, int width, ImageIcon imageIcon){
        return new ImageIcon(imageIcon.getImage().getScaledInstance(width, height, java.awt.Image.SCALE_SMOOTH));
    }

    public static String extractLastNameComponent(String datasetID){
        String[] split = datasetID.split("/");
        return split[split.length-1];
    }


    public static void openWaitingLoadBar(final ProgressFunction function, String title){

        if ( SwingUtilities.isEventDispatchThread() ){
            LoadingBar l = new LoadingBar(function);
            l.start();
            l.setLoadTitle(title);
        }else{
            SwingUtilities.invokeLater(() -> {
                LoadingBar l = new LoadingBar(function);
                l.start();
                l.setLoadTitle(title);
            });
        }
    }

    public static void openWaitingLoadBar(){
        l = new LoadingBar(LoadingBar.WINDOW);
        l.start();
    }

    public static void addToLoadBar(final ProgressFunction function, String title,final BasicListener basicListener){
        if ( l != null && l.isShowing() ){
            l.addFunction(function);
        }else{
            l = new LoadingBar(function);
            l.start();
            l.addEndListener(basicListener);
            l.setLoadTitle(title);
        }
    }


    public static EditableNumericField createNumberTextField(int maximumFractionDigit, int maximumIntegerDigit, int format, Object min, Object max, Object initVal){
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        if ( maximumFractionDigit >= 0 ) {
            numberFormat.setMaximumFractionDigits(maximumFractionDigit);
        }
        if ( maximumIntegerDigit >= 0 ) {
            numberFormat.setMaximumIntegerDigits(maximumIntegerDigit);
        }

        numberFormat.setParseIntegerOnly(false);

        NumberFormatter numberFormatter = new NumberFormatter(numberFormat);
        numberFormatter.setAllowsInvalid(true);
        numberFormatter.setCommitsOnValidEdit(true);

        if ( min != null ){
            numberFormatter.setMinimum((Comparable) min);
        }
        if ( max != null ){
            numberFormatter.setMaximum((Comparable) max);
        }

        DefaultFormatterFactory factory = new DefaultFormatterFactory(numberFormatter);
//        NumberFormatter factory = new NumberFormatter(numberFormat);

//        if ( format >= 0 ){
//            numberFormat.format(format);
//        }

        return new EditableNumericField(initVal,factory);
    }

    public static EditableNumericField createNumberTextField(){
        return createNumberTextField(-1,-1,-1,null,null,null);
    }

    public static EditableNumericField createNumberTextField(Object initVal){
        return createNumberTextField(-1,-1,-1,null,null,initVal);
    }


    public static JFormattedTextField createKevNumberFormatted(){
        return new JFormattedTextField(new DefaultFormatterFactory(new NumberFormatter(NumberFormat.getNumberInstance()),
            new NumberFormatter(NumberFormat.getNumberInstance()) {
                public String valueToString(Object o)
                        throws ParseException {
                    Number number = (Number)o;
                    if (number != null) {
                        float d = number.floatValue();
                        number = new Float(d);
                    }
                    return super.valueToString(number)+" Kev";
                }

                public Object stringToValue(String s)
                        throws ParseException {
                    Number number = (Number)super.stringToValue(s);
                    if (number != null) {
                        float d = number.floatValue();
                        number = new Float(d);
                    }
                    return number;
                }
            },
            new NumberFormatter(NumberFormat.getNumberInstance()) {
                public String valueToString(Object o)
                        throws ParseException {
                    Number number = (Number)o;
                    if (number != null) {
                        float d = number.floatValue();
                        number = new Float(d);
                    }
                    return super.valueToString(number);
                }

                public Object stringToValue(String s)
                        throws ParseException {
                    Number number = (Number)super.stringToValue(s);
                    if (number != null) {
                        float d = number.floatValue();
                        number = new Float(d);
                    }
                    return number;
                }
             })
        );
    }

    /**
     * Create a Basic Option line with Help tooltips and label and this input JComponent
     * @param inputComponent The input JComponent used in this option line
     * @return A JPanel containing an option line
     */
    public static JPanel createOptionLine(JComponent inputComponent, String ressourceID, String ressourceID_tooltip){
        JPanel panel = new JPanel(new GridBagLayout());
        ImageIcon help_gen = Utils.resizeIcon(32, 32, RXTomoJ.getInstance().getView().getRessourceIcon(RXTomoJ.getInstance().getView().getRessourceValue("help-icon")));
        JLabel help_genL = new JLabel(help_gen);
        help_genL.setToolTipText(RXTomoJ.getInstance().getView().getRessourceValue(ressourceID_tooltip));
        JLabel gen = new JLabel(RXTomoJ.getInstance().getView().getRessourceValue(ressourceID));
        gen.setHorizontalAlignment(JLabel.LEFT);
        gen.setLabelFor(inputComponent);

        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, panel, gen);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 0, panel, inputComponent);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 0, 1, 0, 0, 1, 1, 2, 0, panel, help_genL);

        return panel;
    }

    public static JButton createValidateButton(){
        return new JButton(Utils.resizeIcon(32, 32, RXTomoJ.getInstance().getView().getRessourceIcon(RXTomoJ.getInstance().getView().getRessourceValue("validate-icon"))));
    }

}
