/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.utils;

import com.soleil.nanoscopium.hdf5Opener.Hdf5ImagePanel;
import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStack;
import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.RXVirtualImage;
import com.soleil.nanoscopium.rximage.util.FilterRawFunction;
import com.soleil.nanoscopium.rximage.util.RXVirtualImageData;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.DataController;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.JPanelCroppedSpot;
import com.soleil.nanoscopium.rxtomoj.model.computationFunction.ComputeSpot;
import com.soleil.nanoscopium.rxtomoj.model.data.ReductionInfo;
import com.soleil.nanoscopium.rxtomoj.model.filters.HotSpot_Detection;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;
import ij.ImageJ;
import ij.ImageListener;
import ij.ImagePlus;
import ij.gui.RoiListener;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by bergamaschi on 24/04/2014.
 */
public class JPanelImageHdf5 extends JPanelImage {

//    private Hdf5ImagePanel imagePanel;
    private RXVirtualImage rxVirtualImage;

    private boolean isLocked = false;
    private JPanelCroppedSpot croppedSpot;

    public JPanelImageHdf5(JPanelCroppedSpot croppedSpot){
        this();
        this.croppedSpot = croppedSpot;
    }

    public JPanelImageHdf5(){
        super();
        RXTomoJ.getInstance().getModelController().addMMXIControllerUpdatedListener(l -> {
            switch (l.getEventID()) {
                case COMPUTESPOT_CHANGED:
                case CURRENT_IMAGE_CHANGED:
                    if (componentIJ != null) {
                        updateFilter();
                        componentIJ.repaintImage();
                    }
                    break;
                case HOT_SPOT_ITERATION:
                case HOT_SPOT_SPREADCOEF:
                case HOT_SPOT_RADIUS:
                    if (componentIJ != null) {
                        updateFilter();
                        componentIJ.repaintImage();
                    }
                    break;
            }
        });
    }

    public void setImage(RXVirtualImage hdf5VirtualStack){
        this.rxVirtualImage = hdf5VirtualStack;
        super.setImage(rxVirtualImage);
//        this.setRoi();
        componentIJ.addListener(new ImageListener() {
            @Override
            public void imageOpened(ImagePlus imp) {
            }

            @Override
            public void imageClosed(ImagePlus imp) {

            }

            @Override
            public void imageUpdated(ImagePlus imp) {
                if (!isLocked) {
                    isLocked = true;
                    if ( componentIJ.isImage(imp) ) {
                        int[][] roi = RXTomoJ.getInstance().getModelController().getDataController().getReductionInfo().getTransmissionROI();
                        imp.setRoi(roi[0][0],roi[1][0],roi[0][1]-roi[0][0],roi[1][1]-roi[1][0]);
//                        imp.setRoi(RXTomoJ.getInstance().getModelController().getDataController().getComputeSpot(StackType.ABSORPTION).toRec());
                        updateFilter();
                    }
                    isLocked = false;
                }
            }
        });

        componentIJ.addListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                RXTomoJ.getInstance().getModelController().getDataController().setRoiSpot(new int[][]{{currentRoi.x, (int) (currentRoi.x+currentRoi.getWidth())},
                        {currentRoi.y, (int) (currentRoi.y+currentRoi.getHeight())}});
                updateFilter();
                componentIJ.repaintImage();
            }
        });
    }

    public void reset(){
        if ( this.componentIJ != null ) {
            this.componentIJ.removeListeners();
            this.componentIJ.removeFilterFunction();
            this.componentIJ = null;
            this.rxVirtualImage = null;
            this.removeAll();
        }
    }

    @Override
    public void update(long position) {
        super.update(position);
    }

    /**
     * Update the filter used on the displayed hdf5ImagePlus
     */
    public void updateFilter(){
        if ( rxVirtualImage != null ) {
            ReductionInfo reductionInfo = RXTomoJ.getInstance().getModelController().getDataController().getReductionInfo();
            if (croppedSpot != null && croppedSpot.getCurrent() == StackType.DARKFIELD ) {
                componentIJ.setFilterFunction((data, width, height, size) -> {
//                ReductionInfo reductionInfo = RXTomoJ.getInstance().getModelController().getDataController().getReductionInfo();
                    RXImage spot = RXTomoJ.getInstance().getModelController().getDataController().getComputedStack(StackType.SPOT_MAP);

                    if (spot == null) {
                        Utils.openWaitingLoadBar(null,"");
                        RXTomoJ.getInstance().getModelController().getComputationController().spotMapInitialization();
                        return;
                    }

                    final int[] spotMap = new int[spot.getGlobalSize()];

                    //Create Spot map
                    spot.computeData(inside -> {
                        for (int i = 0; i < inside.length; i++) {
                            spotMap[i] = (int) inside[i];
                        }
                    });

                    ArrayList<ComputationUtils.PixelType> pixelTypes = new ArrayList<>();
                    pixelTypes.add(ComputationUtils.PixelType.OTHER);

                    //Filter the data with the Spot Map
                    spot.computeData(l -> {
                        for (int k = 0; k < l.length; k++) {
                            if (!ComputationUtils.PixelType.isDarkFieldPixel((int) l[k])) {
                                data[k] = 0;
                            }
                        }
                    });

                    if ( reductionInfo.isDoFilter_diffusion()) {
                        HotSpot_Detection.run3D(data, spotMap, width, height, size, reductionInfo.getNumberOfIteration_diffusion(),
                                reductionInfo.getSpreadCoefficient_diffusion(), reductionInfo.getRadius_diffusion(),
                                false, pixelTypes);
                    }

                });
            }else if (croppedSpot != null && croppedSpot.getCurrent() == StackType.ABSORPTION ) {
                componentIJ.setFilterFunction((data, width, height, size) -> {
//                ReductionInfo reductionInfo = RXTomoJ.getInstance().getModelController().getDataController().getReductionInfo();
                    RXImage spot = RXTomoJ.getInstance().getModelController().getDataController().getComputedStack(StackType.SPOT_MAP);

                    if (spot == null) {
                        Utils.openWaitingLoadBar(null,"");
                        RXTomoJ.getInstance().getModelController().getComputationController().spotMapInitialization();
                        return;
                    }

                    final int[] spotMap = new int[spot.getGlobalSize()];

                    //Create Spot map
                    spot.computeData(inside -> {
                        for (int i = 0; i < inside.length; i++) {
                            spotMap[i] = (int) inside[i];
                        }
                    });

                    ArrayList<ComputationUtils.PixelType> pixelTypes = new ArrayList<>();
                    pixelTypes.add(ComputationUtils.PixelType.SPOT);

                    //Filter the data with the Spot Map
                    spot.computeData(l -> {
                        for (int k = 0; k < l.length; k++) {
                            if (!ComputationUtils.PixelType.isTransmissionPixel((int) l[k])) {
                                data[k] = 0;
                            }
                        }
                    });

                    if ( reductionInfo.isDoFilter() ){
                        HotSpot_Detection.run3D(data, spotMap, width, height, size, reductionInfo.getNumberOfIteration(),
                                reductionInfo.getSpreadCoefficient(), reductionInfo.getRadius(),
                                false, pixelTypes);
                    }
                });
            }


        }
    }
}
