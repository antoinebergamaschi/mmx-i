/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.utils;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by bergamaschi on 27/05/2014.
 */
public class BasicPanel extends JPanel{
    public static final int margin_left_global=10,margin_left_inside=5,
            margin_top_global=10,margin_top_inside=5,margin_right_global=10,
            margin_right_inside=5,margin_bottom_global=10,margin_bottom_inside=5,margin_inside=5,margin_global=10;

    public static final Color mouseOverLabel_background = new Color(0.5f,0.6f,0.5f),
            mouseOverLabel_forground = new Color(0.9f, 0.9f, 1.0f),
            mouseClickLabel_background = new Color(0.5f,0.6f,0.3f),
            mouseClickLabel_forground = new Color(1.0f, 1.0f, 1.0f);

    private ArrayList<BasicListener> basicListeners = new ArrayList<>();

    public BasicPanel(){
        super();
    }

    public BasicPanel(LayoutManager layoutManager){
        super(layoutManager);
    }

    public void addBasicListener(BasicListener listener){
        if ( !basicListeners.contains(listener) ) {
            this.basicListeners.add(listener);
        }
    }

    public void removeAllBasicListener(){
        this.basicListeners = new ArrayList<>();
    }

    public void removeBasicListener(BasicListener listener){
        if ( basicListeners.contains(listener) ) {
            this.basicListeners.remove(listener);
        }
    }

    public void fireBasicListener(){
        basicListeners.forEach(b->b.basic_actionPerformed());
    }
}
