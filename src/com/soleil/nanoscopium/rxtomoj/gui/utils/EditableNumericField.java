/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.utils;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.text.DefaultFormatterFactory;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.ParseException;

/**
 * Created by antoine bergamaschi on 22/07/2014.
 */
public class EditableNumericField extends JFormattedTextField implements FocusListener, KeyListener {

    private UIDefaults defaults = UIManager.getDefaults( );
    private Color editColor = Color.yellow;
    private Color errorColor = Color.red;
    private Color toValidateColor = Color.green;
    private Color  defaultsColor  =  defaults.getColor("FormattedTextField.background");
    private Border  defaultsBorder  = defaults.getBorder("FormattedTextField.border");
    private Border toValidate =  BorderFactory.createLineBorder(toValidateColor,3);

//######################################################################################
//Constructor

    /**
     * TextField specified in number. No character alpha authorized
     *
     * @param abstractFormatter The formatter to use
     */
    public EditableNumericField(AbstractFormatter abstractFormatter){
        this(null,abstractFormatter);
    }

    /**
     * TextField specified in number. No character alpha authorized
     *
     * @param abstractFormatter The formatter to use
     */
    public EditableNumericField(DefaultFormatterFactory abstractFormatter){
        this(null,abstractFormatter);
    }

    /**
     * TextField specified in number. No character alpha authorized
     *
     * @param abstractFormatter The formatter to use
     */
    public EditableNumericField(Object initialValue, DefaultFormatterFactory abstractFormatter){
        super(abstractFormatter,initialValue);

        this.setFocusLostBehavior(JFormattedTextField.PERSIST);
        this.addKeyListener(this);
        this.addFocusListener(this);
    }

    /**
     *
     * @param initialValue Initial value of the field
     * @param abstractFormatter The formatter to use
     */
    public EditableNumericField(Object initialValue, AbstractFormatter abstractFormatter){
        this(initialValue, new DefaultFormatterFactory(abstractFormatter));
    }

//###########################################################################################
//Methode

    @Override
    public void keyPressed(KeyEvent e){
    }

    @Override
    public void keyReleased(KeyEvent e){
//        if ( this.rand_window == null ){
            // // if Enter is typed
            // if ( e.getKeyCode() == KeyEvent.VK_ENTER ){
            // KeyboardFocusManager focusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
            // focusManager.clearGlobalFocusOwner();
            // }
            // // if Identifier == -1 this is a Object value
            // else if ( this.identifier < 0 ){
            // if ( this.getText().length()  >= 1 && !Pattern.matches(".*[^0-9].*", this.getText()) ){
            // principal_window.setNewMax(Integer.parseInt(this.getText()),this.identifier);
            // principal_window.applyO();
            // }
            // // }
            // }
//        }
//        else{
//            if ( e.getKeyCode() == KeyEvent.VK_ENTER ){
//                KeyboardFocusManager focusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
//                focusManager.clearGlobalFocusOwner();
//            }
//        }
    }

    @Override
    public void keyTyped(KeyEvent e){
    }

    @Override
    public void focusGained(FocusEvent e){
        this.setBackground(editColor);
        this.selectAll();
    }

    @Override
    public void focusLost(FocusEvent e){
        if ( this.isEditValid() ){
            this.setBorder(toValidate);
            this.setBackground(defaultsColor);
        }else{
            this.setBackground(errorColor);
        }
//        if ( this.rand_window == null ){
            // If the Text is empty or if the is something other than a Number in the Text
//            if(this.getText().length()  < 1 || Pattern.matches(".*[^0-9].*", this.getText())){
//                this.BgColor = Color.red;
//                KeyboardFocusManager focusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
//                focusManager.clearGlobalFocusOwner();
//                this.requestFocusInWindow();
//            }
//            else{
//                this.BgColor = Color.yellow;
//                // principal_window.setNewMax(Integer.parseInt(this.getText()),this.identifier);
//                this.setBackground(Color.white);
//            }
////        }
//        // case its in the randomGeneratorFrame
//        else{
//            if(this.getText().length()  < 1 || Pattern.matches(".*[^0-9].*", this.getText())){
//                this.BgColor = Color.red;
//                KeyboardFocusManager focusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
//                focusManager.clearGlobalFocusOwner();
//                this.requestFocusInWindow();
//            }
//            else{
//                this.BgColor = Color.yellow;
//                this.setBackground(Color.white);
//            }
//        }
    }


    @Override
    public void commitEdit() throws ParseException {

        this.setBorder(this.defaultsBorder);
        this.setBackground(this.defaultsColor);


        super.commitEdit();
    }
}