/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.utils;



import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualSpectrum;
import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStack;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Created by bergamaschi on 12/03/14.
 */
public class JPanelPlot extends JPanel implements MouseListener, MouseMotionListener, ScrollUpdatable {

    public final static String MOUSE_PRESSED = "custom_jpanelPlot_mousepressed",MOUSE_RELEASED_START = "custom_jpanelPlot_mousereleased_start",MOUSE_RELEASED_END = "custom_jpanelPlot_mousereleased_end";

    private final int marginBottom = 15;
    private final int marginTop = 10;
    private final int marginLeft = 10;
    private final int marginRight = 10;

    private final int arrowSize = 5;
    private final int girdLineWidth = 2;

    private final float linesWidth = 3.0f;

    private final Stroke stroke = new BasicStroke(linesWidth, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL);

    private final int marginRoiPositionTag = 25;

    private final Color minScaleBarColor = new Color(0.8f,0.8f,0.8f,0.8f);
    private final Color bigScaleBarColor = new Color(0.5f,0.5f,0.5f,0.5f);
    private final Color tagScaleColor = Color.black;
    private final Color axesColor = Color.black;
    private final Color backgroundRoiColor = new Color(0.5f,0.5f,0.5f,0.5f);
    private final Color roiPositionTag = new Color(0.0f,0.55f,0.0f,1.0f);
    private final Color lineRoiColor = new Color(0.2f, 0.2f, 1.0f, 1.0f);
    private final Color shapeRemainingRoiColor = new Color(0.7f,0.2f,0.2f);

    private float width = 2048, height = 50;
    private int numberOfScaleBar = 16;
    private int bigBarEach = 4;

    private int roiRecStart=0,roiRecEnd=0;
    private boolean roiSelection = false;
    private boolean isLog10 = false;

    private float minY = Float.MAX_VALUE;
    private float maxY = Float.MIN_VALUE;

    private float minX = Float.MAX_VALUE;
    private float maxX = Float.MIN_VALUE;


    private ArrayList<float[][]> data = new ArrayList<>();
    private ArrayList<Color> linesColor  = new ArrayList<>();

    private ArrayList<Hdf5VirtualSpectrum> hdf5Data = new ArrayList<>();
    private ArrayList<Color> hdf5Colors = new ArrayList<>();

    private boolean hdf5Mean = false;
    private boolean allowRoi = false;
    private boolean allowPeak = false;

    private ArrayList<Shape> shapes = new ArrayList<>();
    private ArrayList<Color> shapesColors = new ArrayList<>();

    private ArrayList<JPlotListener> customListener = new ArrayList<>();

    private Dimension currentDimension;

    private int numberOfData = 0;
    private int roiClickCount = 0;

    public JPanelPlot(){
        super();
        this.setLayout(null);
        this.setOpaque(true);
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
    }

    private void build(Graphics2D g){
        if ( g != null ){
            currentDimension = this.getSize();

            createPlotGrid(g);

            //recompute the Min / Max heigth
            if ( hdf5Mean ){
                minY = Float.MAX_VALUE;
                maxY = Float.MIN_VALUE;

                for ( Hdf5VirtualSpectrum data : hdf5Data ){
                    updateMinMax(new float[2],data.getMeanSpectrum());
                }
                updateSize();
            }else{
                minY = Float.MAX_VALUE;
                maxY = Float.MIN_VALUE;

//                for ( Hdf5VirtualStack data : hdf5Data ){
//                    updateMinMax(data);
//                }
                updatePlotDimension();


                updateSize();
            }

            //Data set in array
            drawPlotLines(g);
            //Data set in Hdf5
            drawPlotLinesHdf5(g);

            if ( allowPeak ){
                createPeak(g);
            }

            if ( allowRoi ) {
                createRecRoi(g);
            }

            drawCustomShape(g);
        }
    }


    private void createPlotGrid(Graphics2D g){

        g.setColor(axesColor);
        int[] x = {marginLeft+arrowSize,marginLeft,marginLeft+2*arrowSize,marginLeft+arrowSize,marginLeft+arrowSize, currentDimension.width-marginRight, currentDimension.width-arrowSize-marginRight, currentDimension.width-arrowSize-marginRight, currentDimension.width-marginRight,marginLeft+arrowSize};
        int[] y = {marginTop ,marginTop+arrowSize,marginTop+arrowSize,marginTop , currentDimension.height-arrowSize-marginBottom, currentDimension.height-arrowSize-marginBottom, currentDimension.height-marginBottom, currentDimension.height-2*arrowSize-marginBottom, currentDimension.height-arrowSize-marginBottom, currentDimension.height-arrowSize-marginBottom};

        g.drawPolygon(x,y,x.length);
        g.fillPolygon(x,y,x.length);

        x = new int[2];
        y = new int[2];
        //Create graduation on X
        // i + 2 => here 16
        int grad = (currentDimension.width-marginRight-marginLeft-arrowSize)/numberOfScaleBar;
        for ( int i = 1 ; i < numberOfScaleBar ; i++ ){
            if ( i%bigBarEach == 0){
                x[0] = i*grad + marginLeft + arrowSize;
                x[1] = i*grad + marginLeft + arrowSize;
                y[0] = (int) (currentDimension.height-marginBottom-1.75*arrowSize);
                y[1] = (int) (currentDimension.height-marginBottom-0.25*arrowSize);

                g.setColor(bigScaleBarColor);
                g.fillRect(x[0]-girdLineWidth/2,marginTop,girdLineWidth, currentDimension.height-marginBottom-marginTop-arrowSize);
            }else{
                x[0] = i*grad + marginLeft + arrowSize;
                x[1] = i*grad + marginLeft + arrowSize;
                y[0] = (int) (currentDimension.height-marginBottom-1.5*arrowSize);
                y[1] = (int) (currentDimension.height-marginBottom-0.5*arrowSize);
                g.setColor(minScaleBarColor);
                g.fillRect(x[0]-girdLineWidth/2,marginTop,girdLineWidth, currentDimension.height-marginBottom-marginTop-arrowSize);
            }
            //Draw graduation
            g.setColor(tagScaleColor);
            g.drawPolyline(x,y,x.length);
            String test = ""+((width/numberOfScaleBar)*i);
            Rectangle2D rec = g.getFont().getStringBounds(test, g.getFontRenderContext());
            g.drawString(test, (float) (x[0] - rec.getWidth()/2), (float) (currentDimension.height + rec.getMinY() + 5 ));
        }
    }

    /**
     * Create a Buffered Image on top of the Plot to select a ROI
     * @param g the {@link java.awt.Graphics2D} where to draw the ROI
     */
    private void createRecRoi(Graphics2D g){
        if ( roiSelection ){

            if ( this.roiRecEnd - this.roiRecStart <= 0){
                int tmp = this.roiRecStart;
                this.roiRecStart = this.roiRecEnd;
                this.roiRecEnd = tmp;
            }

            if ( this.roiRecStart < this.marginLeft+this.arrowSize ){
                this.roiRecStart = this.marginLeft+this.arrowSize;
            }

            if ( this.roiRecEnd > currentDimension.width - marginRight ){
                this.roiRecEnd = currentDimension.width - marginRight;
            }

            //Construct the buffered Image
            BufferedImage roiBuffuredImage = new BufferedImage(currentDimension.width, currentDimension.height,BufferedImage.TYPE_INT_ARGB);
            Graphics2D roiGraphics = roiBuffuredImage.createGraphics();


            RenderingHints rh = new RenderingHints(RenderingHints.KEY_ALPHA_INTERPOLATION,RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
            rh.add(new RenderingHints(RenderingHints.KEY_COLOR_RENDERING,RenderingHints.VALUE_COLOR_RENDER_QUALITY));
            rh.add(new RenderingHints(RenderingHints.KEY_FRACTIONALMETRICS,RenderingHints.VALUE_FRACTIONALMETRICS_ON));
            rh.add(new RenderingHints(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BICUBIC));
            rh.add(new RenderingHints(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY));
            rh.add(new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_OFF));
            rh.add(new RenderingHints(RenderingHints.KEY_DITHERING,RenderingHints.VALUE_DITHER_DISABLE));

            roiGraphics.setRenderingHints(rh);
//            Font font = new Font(graphics2D.getFont().deriveFont(Font.BOLD));
            roiGraphics.setFont(roiGraphics.getFont().deriveFont(Font.BOLD));


            roiGraphics.setColor(backgroundRoiColor);
            Rectangle2D rec = new Rectangle2D.Float(0,0, currentDimension.width, currentDimension.height);
            Rectangle2D rec2 = new Rectangle2D.Float(this.roiRecStart,0,this.roiRecEnd-this.roiRecStart, currentDimension.height);
            roiGraphics.fill(rec);
            roiGraphics.setComposite(AlphaComposite.getInstance(AlphaComposite.CLEAR, 1.0f));
            roiGraphics.fill(rec2);
            roiGraphics.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
            roiGraphics.setColor(roiPositionTag);

            String start = positionXConverter(this.roiRecStart)+"";
            String end =  positionXConverter(this.roiRecEnd)+"";


            Rectangle2D sizeStringStart = g.getFont().getStringBounds(start, g.getFontRenderContext());
            Rectangle2D sizeStringEnd = g.getFont().getStringBounds(end, g.getFontRenderContext());
            float positionLeftTag = (float) (this.roiRecStart-sizeStringStart.getWidth()-5);
            float positionRightTag = (float) (this.roiRecEnd + 5);

            int addY = 0;
            if ( positionLeftTag - marginLeft < 0 ){
                positionLeftTag = this.roiRecStart + 5;
                addY = (int) sizeStringStart.getHeight() + 5;
            }
            int addY2 = 0;
            if ( positionRightTag + marginRight > currentDimension.width ){
                positionRightTag = (float) (this.roiRecEnd-sizeStringStart.getWidth()-5);
                addY2 = (int) sizeStringEnd.getHeight() + 5;
            }

//            System.out.println("Roi Start : " +this.roiRecStart);
//            System.out.println("Roi End : " +this.roiRecEnd);

            roiGraphics.drawString(start, positionLeftTag,marginRoiPositionTag+addY);
            roiGraphics.drawString(end, positionRightTag, marginRoiPositionTag+addY2);
            //Set the Buffered Image in the Graphics
            g.drawImage(roiBuffuredImage, null, 0, 0);
        }
    }


    private void drawCustomShape(Graphics2D g){
        for ( int i = 0 ; i  <shapes.size() ; i++ ){
            g.setColor(this.shapesColors.get(i));
            g.draw(this.shapes.get(i));
        }
    }

    private float positionXConverter(int realPosition){
        return this.width*((float)(realPosition-marginRight-arrowSize)/(float)(currentDimension.width-this.marginLeft-this.marginRight-arrowSize));
    }

    private int valueXConverter(float realValue){
        return (int) ((realValue/this.width)*(currentDimension.width-marginRight-marginLeft-arrowSize))+marginLeft+arrowSize;
    }


    /**
     * Create a Buffered Image on top of the Plot to select a ROI
     * @param g the {@link java.awt.Graphics2D} where to draw the ROI
     */
    private void createPeak(Graphics2D g){
            g.setColor(lineRoiColor);
            g.drawLine(this.roiRecStart,0,this.roiRecStart, currentDimension.height);
    }

    private void drawPlotLines(Graphics2D g){
        int x1,x2,y1,y2,index=0;

        float incrementX = (float) (currentDimension.width - this.marginLeft - this.marginRight - arrowSize) / this.width;
        float incrementY = (float) (currentDimension.height-this.marginBottom-this.marginTop-arrowSize) / this.height;

        for ( float[][] dataToDraw : this.data){
            g.setColor(this.linesColor.get(index));
            //By default make a line plot without points
//            for ( int i = 0 ; i < dataToDraw[0].length - 1; i++){
//                x1 = (int) (dataToDraw[0][i]*incrementX) + this.marginLeft + arrowSize;
//                y1 = currentDimension.height-this.marginBottom-arrowSize - (int) (dataToDraw[1][i]*incrementY);
//                x2 = (int) (dataToDraw[0][i+1]*incrementX) + this.marginLeft + arrowSize;
//                y2 = currentDimension.height-this.marginBottom-arrowSize - (int) (dataToDraw[1][i+1]*incrementY);
//                g.drawLine(x1,y1,x2,y2);
//            }

            for ( int i = 0 ; i < dataToDraw[0].length - 1; i++){
                x1 = (int) (dataToDraw[0][i]*incrementX) + this.marginLeft + arrowSize;
                y1 = currentDimension.height-this.marginBottom-arrowSize - (int) ((dataToDraw[1][i]-this.minY)*incrementY);
                x2 = (int) ((dataToDraw[0][i+1])*incrementX) + this.marginLeft + arrowSize;
                y2 = currentDimension.height-this.marginBottom-arrowSize - (int) ((dataToDraw[1][i+1]-this.minY)*incrementY);
                g.drawLine(x1,y1,x2,y2);
            }

            index++;
        }
    }

    private void drawPlotLinesHdf5(Graphics2D g){
        int x1,x2,y1,y2,index=0;

        float incrementX = (float) (currentDimension.width - this.marginLeft - this.marginRight - arrowSize) / this.width;
        float incrementY = (float) (currentDimension.height-this.marginBottom-this.marginTop-arrowSize) / this.height;

        g.setStroke(this.stroke);
        for ( Hdf5VirtualSpectrum dataToDraw : this.hdf5Data){
            g.setColor(this.hdf5Colors.get(index));
            float[] data;
            if ( hdf5Mean ){
                data = dataToDraw.getMeanSpectrum();
            }else{
                //By default make a line plot without points
                data = dataToDraw.getPixels(dataToDraw.getCurrentPosition());
            }

            if ( isLog10 ){
                log10(data);
            }

            for ( int i = 0 ; i < data.length - 1; i++){
                x1 = (int) (i*incrementX) + this.marginLeft + arrowSize;
                y1 = currentDimension.height-this.marginBottom-arrowSize - (int) ((data[i]-this.minY)*incrementY);
                x2 = (int) ((i+1)*incrementX) + this.marginLeft + arrowSize;
                y2 = currentDimension.height-this.marginBottom-arrowSize - (int) ((data[i+1]-this.minY)*incrementY);
                g.drawLine(x1,y1,x2,y2);
            }
            index++;
        }
    }


    public void addData(float[] x, float[] y){
        this.addData(x,y,Color.black);
    }

    /**
     * Add data in the plot add draw it with the given Color
     * @param x 1D array the X data
     * @param y 1D array the Y data
     * @param color The drawing {@link java.awt.Color}
     */
    public void addData(float[] x, float[] y, Color color){
        if ( x.length == y.length ){
            float[][] dataToadd = new float[2][x.length];
            System.arraycopy(x,0,dataToadd[0],0,x.length);
            System.arraycopy(y,0,dataToadd[1],0,x.length);
            if ( isLog10 ){
                log10(dataToadd[1]);
            }
            this.data.add(dataToadd);
            this.linesColor.add(color);
            updateMinMax(dataToadd[0], dataToadd[1]);
            //Update Plot
            this.repaint();
        }else{
            System.err.println("Bug :: X length != Y length "+this.getClass().toString());
        }
    }

    /**
     * @param data the {@link com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStack} containing the X/Y datas
     * @param color The drawing {@link java.awt.Color}
     */
    public void addData(Hdf5VirtualSpectrum data,Color color){
        if ( !hdf5Data.contains(data) ){
            this.hdf5Data.add(data);
            this.hdf5Colors.add(color);
            updateMinMax(data);
        }else{
            this.hdf5Colors.remove(hdf5Data.indexOf(data));
            this.hdf5Colors.add(hdf5Data.indexOf(data),color);
        }
        this.repaint();
    }

    /**
     * Permits to synchronize two arrayList in two different plot (Better use synchroniseData)
     * @param datas The {@link com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStack} containing the X/Y datas
     * @param colors The drawing {@link java.awt.Color}
     */
    public void addData(ArrayList<Hdf5VirtualSpectrum> datas, ArrayList<Color> colors){
        this.hdf5Data = datas;
        this.hdf5Colors = colors;
        for ( int index = 0 ; index < datas.size() ; index++ ){
            if ( index < colors.size() ) {
                addData(datas.get(index), colors.get(index));
            }else{
                addData(datas.get(index), new Color((int)(Math.random()*10000.0f)));
            }
        }
    }

    public void synchroniseData(ArrayList<Hdf5VirtualSpectrum> datas, ArrayList<Color> colors){
        this.hdf5Data = datas;
        this.hdf5Colors = colors;
        for ( Hdf5VirtualSpectrum data : datas) {
            updateMinMax(data);
        }
        this.repaint();
    }

    public Object[] getDatas(){
        Object[] objects = new Object[2];
        objects[0] = this.hdf5Data;
        objects[1] = this.hdf5Colors;
        return objects;
    }

    public void removeData(int index){
        if ( index < this.hdf5Data.size() ) {
            this.releaseHdf5(index);
        }
    }

    public void removeAllData(){
//        for ( int i = 0 ; i < this.hdf5Data.size() ; i++ ){
//            this.hdf5Data.get(i).destroy();
//        }
        this.data = new ArrayList<>();
        this.linesColor = new ArrayList<>();
        this.hdf5Data = new ArrayList<>();
        this.hdf5Colors = new ArrayList<>();
        this.repaint();
    }

    public void setMeanSpectrum(boolean bol){
        this.hdf5Mean = bol;
        this.repaint();
    }

    public void releaseHdf5(String id){
        for ( int i = 0 ; i < this.hdf5Data.size() ; i++ ){
            if ( this.hdf5Data.get(i).compareName(id)){
                this.releaseHdf5(i);
                break;
            }
        }
        updatePlotDimension();
        repaint();
    }

    public void releaseHdf5(int position){
//        this.hdf5Data.get(position).destroy();
        this.hdf5Data.remove(position);
        this.hdf5Colors.remove(position);
    }

    public ArrayList<Hdf5VirtualSpectrum> getHdf5Data(){
        return this.hdf5Data;
    }

    @Deprecated
    public void updatePlot(long position){
        for ( Hdf5VirtualStack stack : this.hdf5Data ){
            stack.setCurrentPosition(position);
        }
        repaint();
    }

    /**
     * Set a Log 10 based scale on the Y axis
     * @param logYaxis if true set the ordinate to log
     */
    //TODO add the case of Hdf5Virtual stacks
    public void setLogYaxis(boolean logYaxis){
        isLog10 = logYaxis;
        this.repaint();
    }

    /**
     * Set a Log 10 based scale on the Y axis
     * @param data compute log10 for every data
     */
    private void log10(float[] data){
        int i;
        for ( i = 0 ; i < data.length ; i++ ){
            if ( data[i] != 0 ){
                data[i] = (float) Math.log10(data[i]);
            }else{
                data[i] = Float.NaN;
            }
        }
    }

    /**
     * Warning here 0 will be ecrased because log10(0) is 0 ( instead of -inf ) and log10(1) = 0
     * @param data compute log10-1 for every data
     */
    private void invLog10(float[] data){
        int i;
        for ( i = 0 ; i < data.length ; i++ ){
            data[i] = (float) Math.pow(10,data[i]);
        }
    }

    /**
     * Set the width of the current graphics to be applied
     */
    private void updateSize(){
        this.height = maxY - minY;
        this.width = maxX - minX;
    }

    private void updatePlotDimension(){
        //Reset Dimension
        minY = Float.MAX_VALUE;
        maxY = Float.MIN_VALUE;

        minX = Float.MAX_VALUE;
        maxX = Float.MIN_VALUE;
        for ( float[][] datas : this.data){
            updateMinMax(datas[0],datas[1]);
        }

        for ( Hdf5VirtualSpectrum hdf5VirtualStack : this.hdf5Data){
            updateMinMax(hdf5VirtualStack);
        }
    }


    private void updateMinMax(float[] x,float[] y){
        int i;

        if ( isLog10 ) {
            //Update Heigth
            for (i = 0; i < y.length; i++) {
                float value = -1;
                if ( y[i] > 0 ){
                    value = (float) Math.log10(y[i]);
                }
                if ( this.minY > value ) {
                    this.minY = value;
                } else if ( this.maxY < value ) {
                    this.maxY = value;
                }
            }
        }else{
            //Update Heigth
            for (i = 0; i < y.length; i++) {
                if ( this.minY > y[i]) {
                    this.minY = y[i];
                } else if ( this.maxY < y[i]) {
                    this.maxY = y[i];
                }
            }
        }

        //Update Width
        if (this.minX > x[0]) {
            minX = x[0];
        }

        if (maxX < x[x.length - 1]) {
            maxX = x[x.length - 1];
        }

        if ( this.maxY <= this.minY){
            this.maxY = Float.MIN_VALUE;
            this.minY = Float.MAX_VALUE;
        }
    }

    private void updateMinMax(Hdf5VirtualSpectrum hdf5VirtualStack){

        if ( isLog10 ) {

            //Update Heigth
            if (hdf5VirtualStack.getMinValue() > 0 && this.minY > Math.log10(hdf5VirtualStack.getMinValue())) {
                minY = (float) Math.log10(hdf5VirtualStack.getMinValue());
            }else{
                if ( hdf5VirtualStack.getMinValue() == 0 && minY > -1){
                    minY = -1;
                }
            }

            if (hdf5VirtualStack.getMaxValue() > 0  && maxY < Math.log10(hdf5VirtualStack.getMaxValue())) {
                maxY = (float) Math.log10(hdf5VirtualStack.getMaxValue());
            }else if ( hdf5VirtualStack.getMaxValue() == 0 && maxY < -1 ){
                maxY = -1;
            }

        }else{
            //Update Heigth
            if (this.minY > hdf5VirtualStack.getMinValue()) {
                minY = hdf5VirtualStack.getMinValue();
            }

            if (maxY < hdf5VirtualStack.getMaxValue()) {
                maxY = hdf5VirtualStack.getMaxValue();
            }
        }

        //Update Width
        if (this.minX > 0) {
            minX = 0;
        }

        if (maxX < hdf5VirtualStack.getWidth()) {
            maxX = hdf5VirtualStack.getWidth();
        }
    }


    public boolean isAllowRoi() {
        return allowRoi;
    }

    public void setAllowRoi(boolean allowRoi) {
        this.allowRoi = allowRoi;
    }

    public boolean isAllowPeak(){
        return allowPeak;
    }

    public void setAllowPeak(boolean allowPeak){
        this.allowPeak = allowPeak;
    }

    public void addShape(Shape shape , Color color){
        this.shapes.add(shape);
        this.shapesColors.add(color);
    }

    public void addShape(Shape shape){
        this.addShape(shape,Color.black);
    }

    public void addRoi(float realValueStart, float realValueEnd){
        positionXConverter(valueXConverter(realValueStart));

        addRoi(valueXConverter(realValueStart), 3, valueXConverter(realValueEnd) - valueXConverter(realValueStart), (float) currentDimension.getHeight());
    }

    public void addRoi(float x, float y, float w, float h){
        this.shapesColors.add(shapeRemainingRoiColor);
        this.shapes.add(new Rectangle2D.Float(x, y, w, h));
        this.repaint();
    }

    public void clearShape(){
        this.shapes.clear();
        this.shapesColors.clear();
    }

    public void addListener(JPlotListener component){
        this.customListener.add(component);
    }

    public void removeListener(JPlotListener component){
        this.customListener.remove(component);
    }

    public void removeAllListeners(){
        this.customListener = new ArrayList<>();
    }

    public int getRoiStart(){
        return this.roiRecStart;
    }

    public int getRoiEnd(){
        return this.roiRecEnd;
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D)g;
        RenderingHints rh = new RenderingHints(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        rh.add(new RenderingHints(RenderingHints.KEY_ALPHA_INTERPOLATION,RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY));
        rh.add(new RenderingHints(RenderingHints.KEY_COLOR_RENDERING,RenderingHints.VALUE_COLOR_RENDER_QUALITY));
        rh.add(new RenderingHints(RenderingHints.KEY_FRACTIONALMETRICS,RenderingHints.VALUE_FRACTIONALMETRICS_ON));
        rh.add(new RenderingHints(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BICUBIC));
        rh.add(new RenderingHints(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY));
        rh.add(new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_ON));
        rh.add(new RenderingHints(RenderingHints.KEY_DITHERING,RenderingHints.VALUE_DITHER_DISABLE));

        g2.setRenderingHints(rh);
        super.paintComponent(g);
        build((Graphics2D) g);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
//        this.roiClickCount++;
//        this.shapes.clear();
//        this.shapesColors.clear();
//        this.repaint();

    }

    @Override
    public void mousePressed(MouseEvent e) {
        //Test mouseClickCount
        //If 1 reset the count and remove the ROI
        if ( isAllowRoi() ) {
            this.shapes.clear();
            this.shapesColors.clear();
            this.repaint();
        }

        //Else do nothing

        for ( JPlotListener component : customListener ){
            component.fireMousePressed(positionXConverter(e.getX()));
        }
        this.roiRecStart = e.getX();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
//        float[] newTest = {this.roiRecStart,this.roiRecEnd};
//        float[] oldTest = {-1,-1};
        for ( JPlotListener component : customListener ){
            component.fireMouseReleased(positionXConverter(e.getX()));
            component.fireRoiSet(positionXConverter(this.roiRecStart), positionXConverter(this.roiRecEnd));
        }

        //Add a Remaining structure when realising the selection
        if (isAllowRoi()) {
            addRoi(this.roiRecStart, 3, this.roiRecEnd - this.roiRecStart, (float)this.currentDimension.getHeight()-5);
        }

        roiSelection = false;
        this.paintComponent(this.getGraphics());
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        roiSelection = true;
        //Open the Roi region
        this.roiRecEnd = e.getX();
        this.paintComponent(this.getGraphics());
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    @Override
    public void update(long position) {
        for ( Hdf5VirtualStack stack : this.hdf5Data ){
            stack.setCurrentPosition(position);
        }
        repaint();
    }


    @Override
    protected void finalize() throws Throwable {
        this.removeAllData();
        super.finalize();
    }
}
