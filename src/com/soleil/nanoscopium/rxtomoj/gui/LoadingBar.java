/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui;


import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerUpdatedListener;
import com.soleil.nanoscopium.rxtomoj.gui.utils.BasicListener;
import com.soleil.nanoscopium.rxtomoj.gui.utils.DialogMessage;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.utils.ProgressFunction;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.util.*;

/**
 * Created by antoine bergamaschi on 17/01/14.
 */
public class LoadingBar extends JFrame implements MMXIControllerUpdatedListener{
    private JLabel loading = null;
    private static final String[] T_loading = {"Loading...",
            "Loading..",
            "Loading.",
            "Loading.."};

    private JLabel progressLabel = null;
    private BarPanel barPanel = null;
    @Deprecated
    private ThreadLoadingFrame t = null;
    private ProgressFunction function = null;
    private boolean showProgressText = true;
    private JButton buttonStop = null;

    private ArrayList<BasicListener> end_listeners = new ArrayList<>();

    //Loadtype
    private int type = 0;

    public final static int WINDOW = 0, PANEL = 1;

    private int displayType = 0;

    private JPanel content;

//    private ArrayList<UUID> currentComputation = new ArrayList<>();
    private HashMap<UUID,BarPanel> currentComputationP = new HashMap<>();

    private ArrayList<ProgressFunction> functions = new ArrayList<>();

    /********************************************/
//Constructor

    public LoadingBar(int display_type){
        super();
        this.displayType = display_type;
        this.build();
    }

    public LoadingBar(){
        super();
        this.build();
    }

    public LoadingBar(ProgressFunction function){
        this(function,WINDOW);
    }

    public LoadingBar(ProgressFunction function, int display_type){
//        functions.add(function);
//        this.function =  function;
        this.displayType = display_type;
        this.build();
        addFunction(function);
    }

    /**
     * Private Method, continue the building process of the Frame
     */
    private void build(){
        buildContentPane();
        if ( displayType == WINDOW ) {
            setResizable(false);
            setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            setContentPane(content);
            this.setAlwaysOnTop( true );
            this.pack();

            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            Dimension windowSize = this.getSize();

            int windowX = Math.max(0, (screenSize.width - windowSize.width) / 2);
            int windowY = Math.max(0, (screenSize.height - windowSize.height) / 2);

            setLocation(windowX, windowY);
            this.setVisible(true);
//            while (!this.isShowing()){
//                try {
//                    wait(10);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
        }else if ( displayType == PANEL ){
            //Do nothing the panel is already created in buildContentPane
        }
    }

    public void setDisplayType(int type){
        this.displayType = type;
    }

    /**
     * Final method use to build up this frame.Add all the elements constituting the
     * default Frame.
     */
    private void buildContentPane(){

        Border raisedbevel;
        raisedbevel = BorderFactory.createRaisedBevelBorder();

        content = new JPanel(new GridBagLayout());

        loading = new JLabel(LoadingBar.T_loading[0]);

        //anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 0, 0, 0, 0, 1, 1, 0, 0, content, loading);

        if (function != null ){
            barPanel = new BarPanel();
            barPanel.setBorder(raisedbevel);

            //anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 0, 0, 0, 1, 1, 0, 1, content, barPanel);


            progressLabel = new JLabel("Test");
            //anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 0, 0, 0, 0, 1, 1, 0, 2, content, progressLabel);

            //Add Button a button to Stop the process
            buttonStop = new JButton("Stop");
            buttonStop.addActionListener(e -> {
                System.out.println("Fire Stop");
                functions.forEach(f->f.abort());
            });

            //anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 0, 0, 0, 0, 1, 1, 0, 3, content, buttonStop);
        }
    }




    public void setInfo(int type){
        this.type = type;
    }

    public void setInfo_obj(int type){
        this.type = type;
    }

    private BarPanel addBarPlot(){
        int i = currentComputationP.size()+1;
        BarPanel barPanel = new BarPanel();
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 0, 0, 0, 1, 1, 0, i, content, barPanel);
        this.pack();
        return barPanel;
    }

    private void removeBarPlot(BarPanel barPanel){
        int i = 0;
        for ( Component component : this.getContentPane().getComponents() ){
            if ( component.equals(barPanel) ){
                this.getContentPane().remove(i);
                break;
            }else{
                i++;
            }
        }
        for ( ; i < this.getComponentCount() ; i ++ ){
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 0, 0, 0, 1, 1, 0, i-1, content, this.getComponent(i));
        }
        this.pack();
    }

    public void start(){
        RXTomoJ.getInstance().getModelController().addMMXIControllerUpdatedListener(this);
    }

    public void setText(int com){
        loading.setText(T_loading[com]);
    }

    public void setProgressText(String text){
        progressLabel.setText(text);
    }

    /**
     *Compute an Int based on the score of process obtained by the getWorkDone Function from Function to Apply
     */
    @Deprecated
    public void ask_update(){
        //If there is style work to be done
        if ( function.getWorkDone() < 1.0f ) {
            this.t.setX((int) (function.getWorkDone() * 200));
            if (this.showProgressText) {
                this.setProgressText(function.getWorkDoneString());
                this.pack();
            }
        }
        else{
            this.dispose();
        }
    }


    public Graphics getGraphics_bar(){
        return barPanel.getGraphics();
    }

    public void repaint_(BarPanel barPanel, int x){
        barPanel.setX(x);
        barPanel.repaint();
    }


    public void repaint_(int x){
        barPanel.setX(x);
        barPanel.repaint();
    }

    @Deprecated
    public void setTypeBar(int type){
        t.setTypeBar(type);
    }

    public void setLoadTitle(String title){
        loading.setText(title);
    }

    public void addFunction(final ProgressFunction function) {
        if ( functions.size() == 0 ){
            //Add the Spot Button
            buttonStop = new JButton("Stop");
            buttonStop.addActionListener(e -> {
                System.out.println("Fire Stop");
                functions.forEach(f->f.abort());
            });
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 0, 0, 0, 0, 1, 1, 0, content.getComponentCount()+1, content, buttonStop);
            this.pack();

        }
        functions.add(function);
    }

    @Override
    public void mmxiControllerUpdated(MMXIControllerEvent event) {
        BarPanel barPanel;
        if ( event.getEventID() == MMXIControllerEvent.MMXIControllerEventID.PROGRESS_FUNCTION ){
            switch (event.getEventType()){
                case INITIALIZE:
                    currentComputationP.put(event.getEventData().getID(),addBarPlot());
                    break;
                case UPDATE:
                    int x = (int) (event.getEventData().getValue() * 200);
                    if (this.showProgressText) {
                        this.pack();
                    }

                    barPanel = currentComputationP.get(event.getEventData().getID());
                    if ( barPanel != null ) {
                        this.repaint_(barPanel, x);
//                            this.setProgressText(l.getEventType().getMsg());
                    }

                    break;
                case FINISHED:
                case ERROR:
                    barPanel = currentComputationP.get(event.getEventData().getID());
                    if ( barPanel != null ) {
                        removeBarPlot(barPanel);
                    }

                    currentComputationP.remove(event.getEventData().getID());


//                        currentComputation.remove(l.getEventType().getID());
                    if ( currentComputationP.size() == 0 ){
                        this.dispose();
                    }
                    break;
            }
        }
    }
    //INNER class


    class BarPanel extends JPanel{
        // private Graphics g = null;
        private int x=0;


        public BarPanel(){
            super();
            this.setLayout(null);
            this.setPreferredSize(new Dimension(200,30));
            this.setBackground(Color.GRAY);
        }

        private void build(Graphics g){
            g.setColor(Color.red);
            g.fill3DRect(0,0,x,30,true);
        }

        public void setX(int x){
            this.x = x ;
        }


        public void paintComponent(Graphics g){
            super.paintComponent(g);
            build(g);
        }
    }

    /**
     * override
     **/
    public void dispose(){
//        t.Arret();
        end_listeners.forEach(b->b.basic_actionPerformed());

        //Remove this from listener list
        RXTomoJ.getInstance().getModelController().removeMMXIControllerUpdatedListener(this);

        if ( displayType == WINDOW ) {
            super.dispose();
        }
        else if ( displayType == PANEL ){
//            Container container =  this.content.getParent();
//            if ( container != null ) {
//                container.remove(this.content);
//                container.repaint();
//            }
        }
    }


    public void addEndListener(BasicListener basicListener){
        this.end_listeners.add(basicListener);
    }

    public JPanel getContent(){
        return this.content;
    }

}
