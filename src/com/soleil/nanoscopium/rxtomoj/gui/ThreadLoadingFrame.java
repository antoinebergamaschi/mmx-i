/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui;

import java.util.concurrent.TimeUnit;

/**
 * Created by antoine bergamaschi on 17/01/14.
 */
@Deprecated
public class ThreadLoadingFrame extends Thread {
    private LoadingBar loading_frame;
    private boolean run;
    private boolean isRunning;
    private Thread t;

    private int x=0;
    private int type;
    private String text="";


    private boolean _sens = true;

    public final static int BOUCLE = 0;
    public final static int AVANCE = 1;

    public ThreadLoadingFrame(LoadingBar loading_frame,int type){
        super("Thread_LoadingFrame");
//        this.t = new Thread (this,"Thread_LoadingFrame");
        this.type = type;
        this.loading_frame = loading_frame;
        this.run = true;
    }

    //Runnable Fonction

    @Override
    public void run (){
        // int i = 0;
        isRunning = true;
        while (run){
            //Ask for update

            this.loading_frame.setProgressText(text);

            if ( this.type == BOUCLE ){
                drawGraphics_boucle();
            }
            else if ( this.type == AVANCE ){
                this.loading_frame.ask_update();
                drawGraphics();
            }


            try{
                TimeUnit.MILLISECONDS.sleep(500);
            }
            catch(InterruptedException e){
                e.printStackTrace();
            }
        }
        isRunning = false;
    }

    public void drawGraphics(){
        //get entier correspondant a l'avancement
        this.loading_frame.repaint_(x);
    }


    public void drawGraphics_boucle(){
        if (_sens){
            x+=20;
        }
        else if ( !_sens){
            x-=20;
        }

        this.loading_frame.repaint_(x);

        if ( x == 200 ){
            _sens = false;
        }
        else if ( x == 0 && !_sens ){
            _sens = true;
        }
    }


    public void setX(int x){
        this.x = x;
    }

    public void setText(String text){
        this.text = text;
    }

    public void setTypeBar(int type){
        this.type = type;
    }

    public void Arret (){
        this.run = false;
    }

//    public String getOtherName(){
//        return this.getOtherName();
//    }
//
//    public void start(){
//        this.start();
//    }

//    public boolean isAlive() {
//        return this.isAlive();
//    }

//    public void	join()	throws InterruptedException{
//        t.join();
//    }
}
