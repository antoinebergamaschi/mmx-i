/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput;

import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import ij.io.OpenDialog;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.URI;

/**
 * Created by bergamaschi on 27/01/2016.
 */
public class MenuHelp extends JMenu {
    private RXTomoJViewController controller;

    public MenuHelp(String title,RXTomoJViewController controller){
        super(title);
        this.controller = controller;

        //Open a new SessionObject file
        JMenuItem menu1  =  new JMenuItem(this.controller.getRessourceValue("help_Menu_sub_about"));
        menu1.addActionListener(e ->   {
            try {
                Desktop.getDesktop().browse(URI.create(this.controller.getRessourceValue("wiki-home")));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });

        //Open a new Hdf5 file
        JMenuItem menu2  =  new JMenuItem(this.controller.getRessourceValue("help_Menu_sub_report"));
        menu2.addActionListener(e -> {
            try {
                Desktop.getDesktop().browse(URI.create(this.controller.getRessourceValue("issue-tracker")));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });


        this.add(menu1);
        this.add(menu2);
    }
}
