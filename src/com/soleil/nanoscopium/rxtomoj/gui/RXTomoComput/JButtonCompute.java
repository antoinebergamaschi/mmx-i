/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput;

import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerUpdatedListener;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerPanel.ModalitiesSelectionPanel;
import com.soleil.nanoscopium.rxtomoj.gui.utils.BasicListener;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;
import com.soleil.nanoscopium.rxtomoj.model.utils.ProgressFunction;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.UUID;


/**
 * Created by antoine bergamaschi on 07/02/14.
 */
public class JButtonCompute extends JButton implements ActionListener, BasicListener, MMXIControllerUpdatedListener {

//    private int numbOfWaitingFrame = 0;
    private RXTomoJViewController controller;

    ArrayList<UUID> currentComputation = new ArrayList<>();

    public JButtonCompute(String title, RXTomoJViewController controller){
        super(title);
//        this.self = this;
        this.controller = controller;
        this.addActionListener(this);
        RXTomoJ.getInstance().getModelController().addMMXIControllerUpdatedListener(this);

        this.setToolTipText(this.controller.getRessourceValue("jButton_compute_tooltip"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if ( ((JComponent)e.getSource()).getParent() instanceof ModalitiesSelectionPanel) {
            Utils.openWaitingLoadBar();
            this.setWaitingLoadBar(RXTomoJ.getInstance().getModelController().getComputationController().computeModalities());
//            this.setWaitingLoadBar(RXTomoJ.getInstance().getModelController().getComputationController().computeModalities());
//            this.setWaitingLoadBar(RXTomoJ.getInstance().getModelController().getComputationController().imageCreation(StackType.FLUORESCENCE));
            this.setEnabled(false);
        }
    }



    private void setWaitingLoadBar(final ProgressFunction function){
        Utils.addToLoadBar(function,this.controller.getRessourceValue("label_Title_rawData_wait"),this);
        //Lock the innerPanel
        ((RXTomoJComputationFrame)this.controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).setLockInnerPanel(true);
    }



    @Override
    public void basic_actionPerformed() {
    }

    @Override
    public void mmxiControllerUpdated(MMXIControllerEvent event) {
        if ( event.getEventID() == MMXIControllerEvent.MMXIControllerEventID.PROGRESS_FUNCTION ) {
            if ( event.getEventType() == MMXIControllerEvent.MMXIControllerEventType.INITIALIZE ){
                currentComputation.add(event.getEventData().getID());
            }
            if (event.getEventType() == MMXIControllerEvent.MMXIControllerEventType.FINISHED) {
                currentComputation.remove(event.getEventData().getID());
                if (currentComputation.size() == 0) {
                    JButtonCompute.this.setEnabled(true);
                    //Lock the innerPanel
                    ((RXTomoJComputationFrame) this.controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).setLockInnerPanel(false);
                }
            }
        }
    }
}
