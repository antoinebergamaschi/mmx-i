/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput;

import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.gui.utils.BasicPanel;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.data.DarkFieldInfo;
import com.soleil.nanoscopium.rxtomoj.model.data.DataStorage;
import com.soleil.nanoscopium.rxtomoj.model.data.StackData;
import com.soleil.nanoscopium.rxtomoj.model.fluorescence.FluorescenceInfo;
import com.soleil.nanoscopium.rxtomoj.model.fluorescence.FluorescenceObject;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.lang.reflect.Array;
import java.util.*;
import java.util.List;

/**
 * Created by bergamaschi on 27/05/2014.
 */
public class JPanelModalities extends BasicPanel {

//    private boolean withDescription = false;
    private ArrayList<ModalityPanel> panels = new ArrayList<>();
//    private ArrayList<BasicListener> listeners = new ArrayList<>();
    private int ncol = 0;
    private boolean withDescription = false;

    private boolean multipleSelection = true;

    public JPanelModalities(){
        this(new ArrayList<>());
    }

    public JPanelModalities(int ncol){
        this(new ArrayList<>());
        this.ncol = ncol;
    }

//    public JPanelModalities(HashMap<StackType,String> modalities) {
//        this(modalities.keySet());
//    }

//    public JPanelModalities(HashMap<StackType,String> modalities,int ncol) {
//        this(modalities.keySet(),ncol);
//    }

//    public JPanelModalities(HashMap<StackType,String> modalities, int ncol, boolean withDescription) {
//        this(modalities.keySet(),ncol,withDescription);
//    }

    public JPanelModalities(StackData[] values) {
        this(Arrays.asList(values));
    }

    public JPanelModalities(StackData[] values, int ncol) {
        this(Arrays.asList(values),ncol,false);
    }

    public JPanelModalities(StackType[] values, int ncol, boolean withDescription) {
        this(Arrays.asList(values), ncol, withDescription);
    }

    public JPanelModalities(List<StackType> values, int ncol, boolean withDescription) {
        super(new GridBagLayout());
        ArrayList<StackData> stackDatas = new ArrayList<>();
        for( StackType stackType : values){
            stackDatas.add(new StackData(stackType));
        }

        this.ncol = ncol;
        this.withDescription = withDescription;
        this.build(stackDatas);
    }




    public JPanelModalities(Collection<StackData> modalities){
        this(modalities, 3);
    }

    public JPanelModalities(Collection<StackData> modalities, int ncol){
        this(modalities, ncol, false);
    }

    public JPanelModalities(Collection<StackData> modalities, int ncol, boolean withDescription){
        super(new GridBagLayout());
        this.ncol = ncol;
        this.withDescription = withDescription;
        this.build(modalities);
    }

    public JPanelModalities(Map<StackType,ArrayList<StackData>> modalities, int ncol){
        super(new GridBagLayout());
        this.ncol = ncol;
        this.withDescription = false;
        this.build(modalities);
    }

    private void build(Collection<StackData> modalities){
        int i = 0;
        int j = 0;
        for (StackData type : modalities){
            if ( withDescription ){
                ModalityPanel modalityPanel = new ModalityPanel(type);
                addModality(modalityPanel,i,j);
                if ( i < ncol-1 ) {
                    i++;
                }else{
                    j++;
                    i=0;
                }
            }else {
                final DataStorage dataStorage = RXTomoJ.getInstance().getModelController().getDataController().getDataStorage();
                switch (type.getStackType()) {
                    case ABSORPTION:
                    case PHASECONTRAST:
                        ModalityPanel modalityPanel = new ModalityPanel(type);
                        addModality(modalityPanel, i, j);
                        if (i < ncol - 1) {
                            i++;
                        } else {
                            j++;
                            i = 0;
                        }
                        break;
                    case DARKFIELD:
                    case FLUORESCENCE:

                        ArrayList<StackData> datas = dataStorage.getStackDataStorage().get(type);
                        for ( StackData stackData : datas ){
                            ModalityPanel modalityPanel1 = new ModalityPanel(stackData);
                            addModality(modalityPanel1, i, j);

                            if (i < ncol - 1) {
                                i++;
                            } else {
                                j++;
                                i = 0;
                            }
                        }

//                        DarkFieldInfo darkFieldInfo = RXTomoJ.getInstance().getModelController().getDataController().getDarkFieldInfo();
//                        HashMap<String, int[]> da = darkFieldInfo.getDarkArea();
//
//                        for ( String s : da.keySet()) {
//                            StackType darkfield = StackType.DARKFIELD;
//                            darkfield.setAdditionalName(s);
//
//                            ModalityPanel modalityPanel1 = new ModalityPanel(darkfield);
//                            addModality(modalityPanel1, i, j);
//
//                            if (i < ncol - 1) {
//                                i++;
//                            } else {
//                                j++;
//                                i = 0;
//                            }
//                        }
                    break;
                }
            }

        }
    }


    private void build(Map<StackType,ArrayList<StackData>> modalities){
        int i = 0;
        int j = 0;
        for (StackType type : modalities.keySet()){
            if ( withDescription ){
                ModalityPanel modalityPanel = new ModalityPanel(type);
                addModality(modalityPanel,i,j);
                if ( i < ncol-1 ) {
                    i++;
                }else{
                    j++;
                    i=0;
                }
            }else {
//                final DataStorage dataStorage = RXTomoJ.getInstance().getModelController().getDataController().getDataStorage();
                switch (type) {
                    case ABSORPTION:
                    case PHASECONTRAST:
                    case PHASE_GRAD_X:
                        ModalityPanel modalityPanel = new ModalityPanel(modalities.get(type).get(0));
                        addModality(modalityPanel, i, j);
                        if (i < ncol - 1) {
                            i++;
                        } else {
                            j++;
                            i = 0;
                        }
                        break;
                    case DARKFIELD:
                    case FLUORESCENCE:

                        ArrayList<StackData> datas = modalities.get(type);
                        for ( StackData stackData : datas ){
                            ModalityPanel modalityPanel1 = new ModalityPanel(stackData);
                            addModality(modalityPanel1, i, j);

                            if (i < ncol - 1) {
                                i++;
                            } else {
                                j++;
                                i = 0;
                            }
                        }

//                        DarkFieldInfo darkFieldInfo = RXTomoJ.getInstance().getModelController().getDataController().getDarkFieldInfo();
//                        HashMap<String, int[]> da = darkFieldInfo.getDarkArea();
//
//                        for ( String s : da.keySet()) {
//                            StackType darkfield = StackType.DARKFIELD;
//                            darkfield.setAdditionalName(s);
//
//                            ModalityPanel modalityPanel1 = new ModalityPanel(darkfield);
//                            addModality(modalityPanel1, i, j);
//
//                            if (i < ncol - 1) {
//                                i++;
//                            } else {
//                                j++;
//                                i = 0;
//                            }
//                        }
                        break;
                }
            }

        }
    }


    private void addModality(ModalityPanel modalityPanel, int i, int j){
        Utils.addGridBag(GridBagConstraints.CENTER, BasicPanel.margin_top_inside, BasicPanel.margin_left_inside, BasicPanel.margin_bottom_inside, BasicPanel.margin_right_inside, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, i, j, this, modalityPanel);

        modalityPanel.setDescription(withDescription);

        panels.add(modalityPanel);
    }

    public void setDescriptionVisible(boolean visibility){
        for ( ModalityPanel modalityPanel : panels ){
            modalityPanel.setDescription(visibility);
        }
    }

//    public void setModalities(HashMap<StackData,String> modalities){
//        this.setModalities(modalities.keySet());
//    }

    public void setModalities(StackData[] modalities){
        this.setModalities(Arrays.asList(modalities));
    }

    public void setModalities(Collection<StackData> modalities){
        this.removeAll();
        this.panels = new ArrayList<>();
        this.build(modalities);
    }

    public void setModalities(Map<StackType,ArrayList<StackData>> modalities){
        this.removeAll();
        this.panels = new ArrayList<>();
        this.build(modalities);
    }

    public void setSelected(Collection<StackData> modalities){
        for ( ModalityPanel modalityPanel : panels ){
            modalityPanel.setSelected(modalities.contains(modalityPanel.getModality()));
        }
    }

    public void setStackTypeSelected(Collection<StackType> modalities){
        for ( ModalityPanel modalityPanel : panels ){
            modalityPanel.setSelected(modalities.contains(modalityPanel.getModality().getStackType()));
        }
    }

//    public void update(HashMap<StackType, String> stackTypes){
//        ArrayList<StackData> selected = this.getSelected();
//        this.setModalities(stackTypes);
//        this.setSelected(selected);
//    }

    public void update(Map<StackType,ArrayList<StackData>> dataStorage){
        ArrayList<StackData> selected = this.getSelected();
        this.setModalities(dataStorage);
        this.setSelected(selected);
    }

    public void update(Collection<StackData> stackTypes){
        ArrayList<StackData> selected = this.getSelected();
        this.setModalities(stackTypes);
        this.setSelected(selected);
    }

    public ArrayList<StackData> getSelected(){
        ArrayList<StackData> sel = new ArrayList<>();
        for ( ModalityPanel modalityPanel : panels ){
            if ( modalityPanel.isSelected() ){
                sel.add(modalityPanel.getModality());
            }
        }
        return sel;
    }

    private class ModalityPanel extends JPanel{
        private JCheckBox checkBox;
        private JPanel description;
//        private StackType modality;
        private StackData modality;

        public ModalityPanel(StackData type){
            super(new GridBagLayout());
            Border raisedbevel = BorderFactory.createRaisedBevelBorder();
            Border lineBorder = BorderFactory.createLineBorder(new Color(0.49f,0.69f,0.83f));

            this.setBorder(raisedbevel);

            this.modality = type;

            this.checkBox = new JCheckBox(type.getSaveName());
            this.checkBox.addActionListener(l->{
                if ( multipleSelection ) {
                    fireBasicListener();
                }else{
                    panels.forEach(panel -> {
                        if ( panel != this){
                            panel.setSelected(false);
                        }
                    });
                    fireBasicListener();
                }
            });
//            this.checkBox.setBackground(Color.white);
            this.description = new JPanel(new GridBagLayout());
//            Border line = new
            this.description.setBorder(lineBorder);

            JTextArea jTextArea = new JTextArea();
//            jTextArea.setText(type.getDescription());
            jTextArea.setLineWrap(true);
            jTextArea.setWrapStyleWord(true);
            jTextArea.setBackground(UIManager.getColor("Label.background"));

            Utils.addGridBag(GridBagConstraints.CENTER, BasicPanel.margin_inside, BasicPanel.margin_inside, BasicPanel.margin_inside, BasicPanel.margin_inside, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, this.description, jTextArea);

            Utils.addGridBag(GridBagConstraints.CENTER, BasicPanel.margin_top_global, BasicPanel.margin_left_global, BasicPanel.margin_bottom_inside, BasicPanel.margin_right_global, GridBagConstraints.BOTH, 0, 0, 0, 0, 1, 1, 0, 0, this, this.checkBox);
            Utils.addGridBag(GridBagConstraints.CENTER, BasicPanel.margin_top_inside, BasicPanel.margin_left_global, BasicPanel.margin_bottom_global, BasicPanel.margin_right_global, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 1, this, this.description);
        }

        public ModalityPanel(StackType type){
            this(new StackData(type));
        }

        public void setDescription(boolean sel){
            this.description.setVisible(sel);
        }

        public boolean isSelected(){
            return this.checkBox.isSelected();
        }

        public void setSelected(boolean sel){
            this.checkBox.setSelected(sel);
        }

        public StackData getModality(){
            return this.modality;
        }
    }

    public void setMultipleSelection(boolean multipleSelection) {
        this.multipleSelection = multipleSelection;
    }
}
