/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput;

import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.BasicPanel;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.AnalyseFactory;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import java.awt.*;

/**
 * Created by bergamaschi on 18/02/14.
 */
public class JPanelComputeButton extends JPanel {
    private RXTomoJViewController controller;
    private JButtonCompute jButtonCompute;
//    private JComboBoxSpectrum saiList;
    private JComboBox<String> jComboBox;

    public JPanelComputeButton(RXTomoJViewController controller){
        super(new GridBagLayout());
        this.controller = controller;
        this.build();
    }

    private void build(){

        TitledBorder titlePaneCheck = BorderFactory.createTitledBorder(controller.getRessourceValue("JPanelComputeButton_title"));
        Border raisedbevel = BorderFactory.createRaisedBevelBorder();
        Border re = BorderFactory.createCompoundBorder(raisedbevel,titlePaneCheck);
        this.setBorder(re);

        jComboBox = new JComboBox<String>();
        jComboBox.addItem(controller.getRessourceValue("jcombobox_absorption"));
        jComboBox.addItem(controller.getRessourceValue("jcombobox_darkfield"));
        jComboBox.addItem(controller.getRessourceValue("jcombobox_phase_contrast"));
        jComboBox.addItem(controller.getRessourceValue("jcombobox_fluorescence"));

        TitledBorder tset = BorderFactory.createTitledBorder(controller.getRessourceValue("JPanelComputeButton_computationOption_title"));
        jComboBox.setBorder(tset);

//        saiList = new JComboBoxSpectrum(JComboBoxSpectrum.SAIA, (RXTomoJComputationFrame) this.controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME));
//
//        tset = BorderFactory.createTitledBorder(controller.getRessourceValue("JPanelComputeButton_saiList_title"));
//        saiList.setBorder(tset);

        jButtonCompute = new JButtonCompute(controller.getRessourceValue("jButton_compute"),this.controller);


        Utils.addGridBag(GridBagConstraints.CENTER, BasicPanel.margin_top_global, BasicPanel.margin_left_global, BasicPanel.margin_bottom_global, BasicPanel.margin_right_inside, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, this, jComboBox);
        //anchors,top,left,bottom,right,fill,wX,wY,padx,pady,x,y
        Utils.addGridBag(GridBagConstraints.CENTER, BasicPanel.margin_top_global, BasicPanel.margin_left_inside, BasicPanel.margin_bottom_global, BasicPanel.margin_right_global, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 0, this, jButtonCompute);
    }


    public void update(){
//        this.saiList.update();
    }

    @Deprecated
    public int getSelectedAction(){
        if( controller.getRessourceValue("jcombobox_absorption").compareToIgnoreCase((String) this.jComboBox.getSelectedItem()) == 0 ) {
            return AnalyseFactory.ABSORPTION;
        }
        else if( controller.getRessourceValue("jcombobox_darkfield").compareToIgnoreCase((String) this.jComboBox.getSelectedItem()) == 0 ) {
            return AnalyseFactory.DARKFIELD;
        }
        else if( controller.getRessourceValue("jcombobox_phase_contrast").compareToIgnoreCase((String) this.jComboBox.getSelectedItem()) == 0 ) {
            return AnalyseFactory.PHASECONTRASTE;
        }
        else if( controller.getRessourceValue("jcombobox_fluorescence").compareToIgnoreCase((String) this.jComboBox.getSelectedItem()) == 0 ){
            return AnalyseFactory.FLUORESCENCE;
        }
        return -1;
    }
}
