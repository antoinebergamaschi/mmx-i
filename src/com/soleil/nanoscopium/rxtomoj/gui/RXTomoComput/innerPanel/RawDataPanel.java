/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerPanel;

import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualSpectrum;
import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStack;
import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStackFactory;
import com.soleil.nanoscopium.rximage.RXSpectrum;
import com.soleil.nanoscopium.rximage.RXVirtualImage;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.ComputationController;
import com.soleil.nanoscopium.rxtomoj.controller.DataController;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerUpdatedListener;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.*;
import com.soleil.nanoscopium.rxtomoj.gui.utils.JPanelImageHdf5;
import com.soleil.nanoscopium.rxtomoj.gui.utils.JPanelPlot;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.data.DataStorage;
import com.soleil.nanoscopium.rxtomoj.model.data.StackData;
import com.soleil.nanoscopium.rxtomoj.model.fluorescence.FluorescenceInfo;
import com.soleil.nanoscopium.rxtomoj.model.fluorescence.FluorescenceObject;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by antoine bergamaschi on 31/03/2014.
 */
public class RawDataPanel extends InnerPanel implements MMXIControllerUpdatedListener{


//    private PanelImageChoice panelImageChoice;
    private JPanelImageHdf5 spot;
    private JPanelPlot spcImage;

    private JPanelCroppedSpot cropedSpot;
    private JScrollBarRawData jScrollBarRawData;

    private JPanelSelectSpectrum jPanelSelectSpectrum;
    private JPanelElements jPanelElements;
    private JComboBox<String> jComboBox;
//    private JPanelComputeButton jPanelComputeButton;

//    private boolean lock_paint = false;

    public RawDataPanel(RXTomoJViewController controller){
        super(controller);
        RXTomoJ.getInstance().getModelController().addMMXIControllerUpdatedListener(this);
    }

    public void createInterface(){
        JPanel selectReconstructionType = new JPanel(new BorderLayout());

        jComboBox = new JComboBox<>();
        jComboBox.addItem(controller.getRessourceValue("jcombobox_absorption"));
        jComboBox.addItem(controller.getRessourceValue("jcombobox_darkfield"));
        jComboBox.addItem(controller.getRessourceValue("jcombobox_phase_contrast"));
        selectReconstructionType.add(jComboBox,BorderLayout.CENTER);

        jPanelSelectSpectrum = new JPanelSelectSpectrum(this.controller);
        jPanelElements = new JPanelElements(this.controller);
        jPanelSelectSpectrum.addRightPanel(jPanelElements);

        JPanel compute = new JPanel(new BorderLayout());
        JButtonCompute computeButton = new JButtonCompute(controller.getRessourceValue("jButton_compute"),this.controller);

        compute.add(computeButton, BorderLayout.CENTER);
        compute.setPreferredSize(new Dimension(50,50));


        this.cropedSpot = new JPanelCroppedSpot(this.controller);
        this.spot = new JPanelImageHdf5(cropedSpot);

//        spcImage  = new JPanelSpectrum();
        spcImage = new JPanelPlot();

        jScrollBarRawData = new JScrollBarRawData(JScrollBar.HORIZONTAL,0,1,0,100);
        ScrollViewPanel scrollViewPanel = new ScrollViewPanel(jScrollBarRawData,this.controller);
        jScrollBarRawData.addView(scrollViewPanel);
        jScrollBarRawData.setPreferredSize(new Dimension(50, 50));
        jScrollBarRawData.addHdf5(spcImage);

//        jPanelComputeButton = new JPanelComputeButton(this.controller);

        Utils.addGridBag(GridBagConstraints.CENTER, margin_top_global, margin_left_global, margin_bottom_inside, margin_right_inside, GridBagConstraints.BOTH, 0.3, 1, 0, 0, 1, 1, 0, 0, this, this.cropedSpot);
        Utils.addGridBag(GridBagConstraints.CENTER,margin_top_global,margin_left_inside,margin_bottom_inside,margin_right_global,GridBagConstraints.BOTH,1,1,0,0,1,1,1,0,this,this.spot);

        Utils.addGridBag(GridBagConstraints.CENTER,margin_top_inside,margin_left_global,margin_bottom_inside,margin_right_inside,GridBagConstraints.BOTH,0,1,0,0,1,1,0,1,this,jPanelSelectSpectrum);
        Utils.addGridBag(GridBagConstraints.CENTER,margin_top_inside,margin_left_inside,margin_bottom_inside,margin_right_global,GridBagConstraints.BOTH,1,1,0,0,2,1,1,1,this,this.spcImage);

        Utils.addGridBag(GridBagConstraints.CENTER,margin_top_inside,margin_left_global,margin_bottom_global,margin_right_inside,GridBagConstraints.BOTH,0,0,0,0,1,1,0,2,this,scrollViewPanel);
        Utils.addGridBag(GridBagConstraints.CENTER,margin_top_inside,margin_left_inside,margin_bottom_global,margin_right_global,GridBagConstraints.BOTH,1,0,0,0,1,1,1,2,this,this.jScrollBarRawData);

    }


    /**
     * Construct the menu bar associated with this JFrame
     */
    public ArrayList<JMenu> createMenu(){
        ArrayList<JMenu> menus = new ArrayList<>();

        menus.add(new MenuOption(this.controller.getRessourceValue("preProcessingPanel_title_Menu_option"),this.controller));

        return menus;
    }

    public  void setRawSpotImage(Hdf5VirtualStack image){
        if ( image != null ) {

            RXVirtualImage rxVirtualImage = new RXVirtualImage(image);
            spot.setImage(rxVirtualImage);
            //Add the cropped Spot Image
            //Link the cropped Spot image to the image
            cropedSpot.setImage(rxVirtualImage);

            this.jScrollBarRawData.addHdf5(spot);
            this.jScrollBarRawData.setMaximum((int) image.getNSlice());

            //Update Spot
        }
    }

    public Object[] getJPlotHdf5(){
        return  this.spcImage.getDatas();
    }

    public void setMeanSpectrum(){
        spcImage.setMeanSpectrum(true);
    }

    public void removeMeanSpectrum(){
        spcImage.setMeanSpectrum(false);
    }

    public void logSpectrum(boolean bol){
        spcImage.setLogYaxis(bol);
    }

    public void addSpectrum(String datasetID, Color color){
        ArrayList<StackData> sumSpectrums = RXTomoJ.getInstance().getModelController().getDataController().getDataStorage().getStackDataStorage().get(StackType.SUM_SPECTRA);
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

        if (DataStorage.containsSPC(sumSpectrums,datasetID)){
            //Retrieve the sum spectrum in system file
            RXSpectrum sumSpectrum = (RXSpectrum) dataController.getStorageData(DataStorage.getFromSpectrum(sumSpectrums, datasetID));
            //Load the Hdf5VirtualStack for display purpose
            Hdf5VirtualStack hdf5VirtualStack = Hdf5VirtualStackFactory.createVirtualStack(RXTomoJ.getInstance().getModelController().getSessionController().getPathToHdf5(), datasetID, true);
            ((Hdf5VirtualSpectrum)hdf5VirtualStack).setMean(sumSpectrum.copyData());
            ((Hdf5VirtualSpectrum)hdf5VirtualStack).computeMinMax();
            spcImage.addData((Hdf5VirtualSpectrum)hdf5VirtualStack, color);
        }else{
            RXTomoJ.getInstance().getModelController().getComputationController().updateSumSpectra();
        }
    }

    public void removeSpectrum(String datasetID){
        spcImage.releaseHdf5(datasetID);
    }

    public void setDisplayPosition(int positionDisplay) {
        jScrollBarRawData.changeDisplay(positionDisplay);
    }


    @Override
    public void update() {
//        Spectrum and SAI list do update the JPanelSpectrum associated
//        if ( isReseted ) {
//            this.jPanelComputeButton.update();
            this.jPanelSelectSpectrum.update();
            this.jPanelElements.update();
            this.spcImage.update(this.spcImage.getGraphics());
            this.validate();
            isReseted = false;
//        }
    }

    @Override
    public void updateGraphics() {
//        this.spot.resetDrawingSize();
    }

    public void reset(){
        isReseted = true;
        spcImage.removeAllData();
        spot.reset();
        this.jScrollBarRawData.removeHdf5(spot);
        this.jScrollBarRawData.setMaximum(1);
        this.jScrollBarRawData.setValue(1);
    }

    public void updateRoi(){
//        this.spot.setRoi(RXTomoJ.getInstance().getModelController().getDataController().getComputeSpot(StackType.ABSORPTION).toRec());
        this.cropedSpot.update();
    }

    public void updateFluorescenceElements(){
        this.jPanelElements.update();
    }

    @Override
    public void mmxiControllerUpdated(MMXIControllerEvent event) {
        if ( event.getEventID() == MMXIControllerEvent.MMXIControllerEventID.PROGRESS_FUNCTION ){
            switch (event.getEventType()){
                case INITIALIZE:
                    if ( event.getEventData().getIdentifier() == ComputationController.COMPUTE_SUMSPECTRA ) {
                        //A waiting Gif While Be Show while the Spectrum is added
                        jPanelSelectSpectrum.setWaitingGif();
                        //Lock the innerPanel
                        ((RXTomoJComputationFrame) this.controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).setLockInnerPanel(true);
                    }
                    break;
                case FINISHED:
                    if ( event.getEventType() == MMXIControllerEvent.MMXIControllerEventType.FINISHED ){
                        if ( event.getEventData().getIdentifier() == ComputationController.COMPUTE_SUMSPECTRA ){

                            FluorescenceInfo fluorescenceInfo = RXTomoJ.getInstance().getModelController().getDataController().getFluorescenceInfo();
                            for (FluorescenceObject fluorescenceObject : fluorescenceInfo.getFluoData() ){
                                String s = fluorescenceObject.getPath();
                                Hdf5VirtualStack virtualStack = Hdf5VirtualStackFactory.createVirtualStack(RXTomoJ.getInstance().getModelController().getSessionController().getPathToHdf5(), s, true);
                                ((Hdf5VirtualSpectrum)virtualStack).setMean(fluorescenceObject.getMeanSpectrum().copyData());
                                ((Hdf5VirtualSpectrum)virtualStack).computeMinMax( ((Hdf5VirtualSpectrum)virtualStack).getMeanSpectrum());
                                spcImage.addData((Hdf5VirtualSpectrum)virtualStack, jPanelSelectSpectrum.getDataSetColor(s));

                            }
                            //TODO bug here when computing the ref image
//                            HashMap<String,RXSpectrum> spectrums = (HashMap<String,RXSpectrum>) event.getEventData().getObject();
//                            for ( String s : spectrums.keySet() ){
//
//                                ((Hdf5VirtualSpectrum)virtualStack).setMean(spectrums.get(s).copyData());
//                                ((Hdf5VirtualSpectrum)virtualStack).computeMinMax( ((Hdf5VirtualSpectrum)virtualStack).getMeanSpectrum());
//                                spcImage.addData((Hdf5VirtualSpectrum)virtualStack, jPanelSelectSpectrum.getDataSetColor(s));
//                            }


                            //Remove the Gif
                            jPanelSelectSpectrum.removeWaitingGif();

                            //UnLock the innerPanel
                            ((RXTomoJComputationFrame)this.controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).setLockInnerPanel(false);
                        }
                    }
                    break;
            }
        }
    }
}
