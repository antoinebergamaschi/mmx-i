/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerPanel;

import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.BasicPanel;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Updatable;
import com.soleil.nanoscopium.rxtomoj.gui.utils.UpdateGraphics;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by antoine bergamaschi on 31/03/2014.
 */
public abstract class InnerPanel extends JPanel implements Updatable, UpdateGraphics {

    protected final int margin_left_global= BasicPanel.margin_left_global,margin_left_inside=BasicPanel.margin_left_inside,
            margin_top_global=BasicPanel.margin_top_global,margin_top_inside=BasicPanel.margin_top_inside,margin_right_global=BasicPanel.margin_right_global,
            margin_right_inside=BasicPanel.margin_right_inside,margin_bottom_global=BasicPanel.margin_bottom_global,margin_bottom_inside=BasicPanel.margin_bottom_inside;

    protected RXTomoJViewController controller;
    private boolean isActive = false;
    protected boolean isReseted = true;

    public InnerPanel(RXTomoJViewController controller){
        super(new GridBagLayout());
        this.controller = controller;
        createInterface();
        this.setVisible(isActive);
    }

    public void display(){
        isActive  = !isActive;
        this.setVisible(isActive);
    }

    public abstract void createInterface();

    public abstract void update();

    public abstract void reset();

    public abstract ArrayList<JMenu> createMenu();

    private boolean isActive(){return isActive;}

    @Override
    public void updateGraphics() {
        System.err.println("Update Graphics not implemented :: "+this.getClass().toString());
    }
}
