/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerPanel;


import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.DataController;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.ButtonContainer;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.InformationPanel;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.JButtonComputeReconstruction;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.JPanelModalities;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow.*;
import com.soleil.nanoscopium.rxtomoj.gui.utils.EditableNumericField;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.data.DataStorage;
import com.soleil.nanoscopium.rxtomoj.model.data.ReconstructionInfo;
import com.soleil.nanoscopium.rxtomoj.model.data.StackData;
import com.soleil.nanoscopium.rxtomoj.model.filters.ProjectionFilter;
import com.soleil.nanoscopium.rxtomoj.model.reconstructionFunction.ReconstructionAlgebraic;
import com.soleil.nanoscopium.rxtomoj.model.reconstructionFunction.ReconstructionFilteredBack;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.ReconstructionType;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;

import javax.swing.*;
import java.awt.*;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;


/**
 * Created by bergamaschi on 01/04/2014.
 */
public class ReconstructionPanel extends InnerPanel {

    private static final int art = 1, fbp = 0;

    private JSpinner spinnerARTIterations;
    private JSpinner spinnerARTRelaxationCoeff;
    private JSpinner samplingSpinner;
    private JSpinner spinnerBgARTk;
    private JCheckBox  BgARTCheckbox;
    private JCheckBox usePositivityContrainst;
    private JPanelModalities modalities;
    private Reconstruction_InnerPanel reconstruction_innerPanel;
    private JLabel size_r ;
    private JLabel diameter_r ;
    private JLabel centerRotation_r ;


    public ReconstructionPanel(RXTomoJViewController controller) {
        super(controller);
    }


    @Override
    public void createInterface() {
        ButtonContainer buttonContainer = new ButtonContainer();
        buttonContainer.addLabel(controller.getRessourceValue("ReconstructionPanel_button_fbp"),fbp+"");
        buttonContainer.addLabel(controller.getRessourceValue("ReconstructionPanel_button_art"),art+"");

        reconstruction_innerPanel = new Reconstruction_InnerPanel();

        buttonContainer.addBasicListener(()->reconstruction_innerPanel.setPanel(Integer.parseInt((String) buttonContainer.getCurrentSelection())));

        Utils.addGridBag(GridBagConstraints.CENTER, margin_top_global, margin_left_global, margin_bottom_global, margin_right_inside, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 3, 0, 0, this, reconstruction_innerPanel );
        Utils.addGridBag(GridBagConstraints.CENTER, margin_top_global, margin_left_inside, margin_bottom_inside, margin_right_global, GridBagConstraints.BOTH, 0, 1, 0, 0, 1, 1, 1, 0, this, createGlobalParametersPanel());
        Utils.addGridBag(GridBagConstraints.CENTER, margin_top_inside, margin_left_inside, margin_bottom_inside, margin_right_global, GridBagConstraints.BOTH, 0, 1, 0, 0, 1, 1, 1, 1, this, createGlobalInformation());
        Utils.addGridBag(GridBagConstraints.CENTER, margin_top_inside, margin_left_inside, margin_bottom_global, margin_right_global, GridBagConstraints.BOTH, 0, 1, 0, 0, 1, 1, 1, 2, this, createModalitiesPanel());

        Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 0, 0, 0, 2, 1, 0, 3, this, buttonContainer);

        buttonContainer.init(0);
        reconstruction_innerPanel.setPanel(0);
    }

    @Override
    public void update() {
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

        ReconstructionInfo reconstructionInfo = dataController.getReconstructionInfo();
        DataStorage dataStorage = dataController.getDataStorage();

        this.modalities.update(dataStorage.getStackDataStorage());
        this.samplingSpinner.setValue(reconstructionInfo.getNumberOfLine());

//        ReconstructionAlgebraic reconstructionAlgebraic = (ReconstructionAlgebraic) dataController.getReconstruction(ReconstructionType.ART, StackType.ABSORPTION);

        RXImage imageStack = dataController.getComputedStack(StackType.getBasicType().get(0));
        if ( imageStack == null ){
            imageStack = new RXImage(0,0,0);
        }

        size_r.setText(" : "+imageStack.getHeight());
        diameter_r.setText(" : "+imageStack.getWidth());
        centerRotation_r.setText(" : "+ reconstructionInfo.getCenterOfRotationX());
//        centerRotation_r.setText(" : "+ reconstructionAlgebraic.getCenterOfRotationY());
//        this.validate_();
        this.reconstruction_innerPanel.update();
    }

    @Override
    public void reset() {
        System.err.println("No reset Function implemented "+this.getClass().toString());
    }

    @Override
    public ArrayList<JMenu> createMenu() {
        ArrayList<JMenu> menus = new ArrayList<>();

        //Add Menu Frame for Reconstruction Option ( BETA only )
        JMenu menu = new JMenu(this.controller.getRessourceValue("ReconstructionPanel_title_menu"));
        JMenuItem jMenuItem = new JMenuItem(this.controller.getRessourceValue("preProcessingPanel_title_menu_beta"));

        jMenuItem.addActionListener( l -> new JFrameReconstructionBetaOption(this.controller));

        menu.add(jMenuItem);

        menus.add(menu);

        return menus;
    }

    private JPanel createGlobalParametersPanel(){
        JPanel globalParamsPanel=new JPanel(new GridBagLayout());
        JLabel label = new JLabel(controller.getRessourceValue("ReconstructionPanel_globalParameters_sampling"));
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

        ReconstructionInfo reconstructionInfo = dataController.getReconstructionInfo();

        globalParamsPanel.setBorder(BorderFactory.createTitledBorder(controller.getRessourceValue("ReconstructionPanel_globalParameters")));


        samplingSpinner=new JSpinner(new SpinnerListModel(new Integer[]{1,2,3,4,5,6,7,8,9,10}));
        samplingSpinner.setValue(reconstructionInfo.getNumberOfLine());

        samplingSpinner.addChangeListener(l ->RXTomoJ.getInstance().getModelController().getDataController().setReconstructionPixelSampling((Integer) samplingSpinner.getValue()));

        Utils.addGridBag(GridBagConstraints.CENTER, margin_top_global, margin_left_global, margin_bottom_global, margin_right_global, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, globalParamsPanel, label);
        Utils.addGridBag(GridBagConstraints.CENTER, margin_top_global, margin_left_global, margin_bottom_global, margin_right_global, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 0, globalParamsPanel, samplingSpinner);


        return globalParamsPanel;
    }

    private JPanel createGlobalInformation(){
        JPanel panel = new JPanel(new GridBagLayout());
        panel.setBorder(BorderFactory.createTitledBorder(controller.getRessourceValue("ReconstructionPanel_globalInformation")));
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

        ReconstructionInfo reconstructionInfo = dataController.getReconstructionInfo();
//        ReconstructionAlgebraic reconstructionAlgebraic = (ReconstructionAlgebraic) dataController.getReconstruction(ReconstructionType.ART, StackType.ABSORPTION);

        RXImage imageStack = dataController.getComputedStack(StackType.getBasicType().get(0));
        if ( imageStack == null ){
            imageStack = new RXImage(0,0,0);
        }

        JLabel size = new JLabel(controller.getRessourceValue("ReconstructionPanel_globalInformation_size"));
        JLabel diameter = new JLabel(controller.getRessourceValue("ReconstructionPanel_globalInformation_diameter"));
        JLabel centerRotation = new JLabel(controller.getRessourceValue("ReconstructionPanel_globalInformation_rotationCenter"));

        size_r = new JLabel(" : "+imageStack.getHeight());
        diameter_r = new JLabel(" : "+imageStack.getWidth());
        centerRotation_r = new JLabel(" : 000");
        centerRotation_r = new JLabel(" : " + reconstructionInfo.getCenterOfRotationX());

        Utils.addGridBag(GridBagConstraints.CENTER, margin_top_inside, margin_left_inside, margin_bottom_inside, margin_right_inside, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, panel, size);
        Utils.addGridBag(GridBagConstraints.CENTER, margin_top_inside, margin_left_inside, margin_bottom_inside, margin_right_inside, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 0, panel, size_r);

        Utils.addGridBag(GridBagConstraints.CENTER, margin_top_inside, margin_left_inside, margin_bottom_inside, margin_right_inside, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 1, panel, diameter);
        Utils.addGridBag(GridBagConstraints.CENTER, margin_top_inside, margin_left_inside, margin_bottom_inside, margin_right_inside, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 1, panel, diameter_r);

        Utils.addGridBag(GridBagConstraints.CENTER, margin_top_inside, margin_left_inside, margin_bottom_inside, margin_right_inside, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 2, panel, centerRotation);
        Utils.addGridBag(GridBagConstraints.CENTER, margin_top_inside, margin_left_inside, margin_bottom_inside, margin_right_inside, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 2, panel, centerRotation_r);

        return panel;
    }

    private JPanel createModalitiesPanel(){
        DataStorage dataStorage = RXTomoJ.getInstance().getModelController().getDataController().getDataStorage();

        this.modalities = new JPanelModalities(dataStorage.getStackDataStorage(),1);
        this.modalities.setMultipleSelection(false);
        modalities.setBorder(BorderFactory.createTitledBorder(controller.getRessourceValue("ReconstructionPanel_modalitiesPanel")));
        return modalities;
    }

    /**
     *
     */
    private class Reconstruction_InnerPanel extends InformationPanel {

        private ProjectionFilter.FilterType current_filter;
        private ProjectionFilter.WindowType current_window;

        public Reconstruction_InnerPanel(){
            super(InformationPanel.VERTICAL);
        }

        public void setPanel(int type){
            super.setPanel(type);
            switch (type){
                case fbp:
                    createFBP();
                    break;
                case art:
                    createART();
                    break;
                default:
                    System.err.println("Error :: "+this.getClass().toString());
            }

            validate();
        }


        private void createFBP(){
            JButtonComputeReconstruction button = new JButtonComputeReconstruction(controller.getRessourceValue("jButton_compute"),controller);

            ReconstructionInfo reconstructionInfo = RXTomoJ.getInstance().getModelController().getDataController().getReconstructionInfo();


            JPanel filterParamsPanel=new JPanel();
            filterParamsPanel.setLayout(new GridBagLayout());
            filterParamsPanel.setBorder(BorderFactory.createTitledBorder(controller.getRessourceValue("ReconstructionPanel_fbp")));
            JCheckBox box = null;
            ButtonGroup bg = null;

            JCheckBox useFilter = new JCheckBox(controller.getRessourceValue("ReconstructionPanel_fbp_useFilter"));
            useFilter.setSelected(reconstructionInfo.isUseFilter());

            //Create the Windowing panel
            JPanel windP=new JPanel();
            windP.setLayout(new GridBagLayout());
            windP.setBorder(BorderFactory.createTitledBorder(controller.getRessourceValue("ReconstructionPanel_fbp_windowing")));
            bg=new ButtonGroup();
            int index=0;

            for ( ProjectionFilter.WindowType windowType : ProjectionFilter.WindowType.values() ){
                box = new JCheckBox(controller.getRessourceValue(windowType.getResource()));
                if ( windowType.equals(reconstructionInfo.getWindowType()) ){
                    box.setSelected(true);
                    current_window = windowType;
                }
                bg.add(box);
                box.addActionListener(l -> current_window = windowType);
                Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 2, 1, 0, index, windP, box);
                index++;
            }

            JLabel dimension_label = new JLabel(controller.getRessourceValue("ReconstructionPanel_fbp_dimension_label"));
            EditableNumericField dimension = Utils.createNumberTextField(-1,-1, NumberFormat.INTEGER_FIELD,new Float(1),new Float(1000),null);
            dimension.setValue(reconstructionInfo.getWindowDimension());

            Utils.addGridBag(GridBagConstraints.FIRST_LINE_START, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, index, windP, dimension_label);
            Utils.addGridBag(GridBagConstraints.CENTER, 10, 0, 10, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, index, windP, dimension);

            //Create the Filter Panel
            index=0;
            JPanel filterPanel=new JPanel();
            filterPanel.setLayout(new GridBagLayout());
            filterPanel.setBorder(BorderFactory.createTitledBorder(controller.getRessourceValue("ReconstructionPanel_fbp_function")));
            bg=new ButtonGroup();
            for ( ProjectionFilter.FilterType filterType : ProjectionFilter.FilterType.values() ){
                box = new JCheckBox(controller.getRessourceValue(filterType.getResource()));
                if ( filterType.equals(reconstructionInfo.getFilterType()) ){
                    box.setSelected(true);
                    current_filter = filterType;
                }
                bg.add(box);
                box.addActionListener(l -> current_filter = filterType);
                Utils.addGridBag(GridBagConstraints.CENTER, margin_top_global, margin_left_global, margin_bottom_global, margin_right_global, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, index, filterPanel, box);
                index++;
            }


            Utils.addGridBag(GridBagConstraints.CENTER, margin_top_global, margin_left_global, margin_bottom_global, margin_right_global, GridBagConstraints.BOTH, 1, 0, 0, 0, 1, 1, 0, 0, filterParamsPanel, useFilter);
            Utils.addGridBag(GridBagConstraints.CENTER, margin_top_global, margin_left_global, margin_bottom_global, margin_right_global, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 1, filterParamsPanel, windP);
            Utils.addGridBag(GridBagConstraints.CENTER, margin_top_global, margin_left_global, margin_bottom_global, margin_right_global, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 2, filterParamsPanel, filterPanel);


            button.addActionListener(e->{

                try {
                    dimension.commitEdit();
                    Utils.openWaitingLoadBar();
                    button.setEnabled(false);
                    for (StackData type : modalities.getSelected() ) {

                        RXTomoJ.getInstance().getModelController().getDataController().setWindowDimension((Float) dimension.getValue());
                        RXTomoJ.getInstance().getModelController().getDataController().setWindowType(current_window);
                        RXTomoJ.getInstance().getModelController().getDataController().setFilterType(current_filter);

                        button.setWaitingLoadBar(RXTomoJ.getInstance().getModelController().getReconstructionController().filterdBackProjection(type));
                    }
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
            });

            useFilter.addActionListener(l->RXTomoJ.getInstance().getModelController().getDataController().setUseReconstructionFilter(useFilter.isSelected()));


            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, this.getFirstPane(), filterParamsPanel);
            Utils.addGridBag(GridBagConstraints.CENTER, margin_top_inside, 0, 0, 0, GridBagConstraints.NONE, 0, 0, 0, 0, 1, 1, 0, 1, this.getFirstPane(), button);
            this.getSecondPane().setText(controller.getRessourceValue("ReconstructionPanel_fbp_title"));
        }

        private void createART(){
            JButtonComputeReconstruction button = new JButtonComputeReconstruction(controller.getRessourceValue("jButton_compute"),controller);
            button.addActionListener(e-> {
                Utils.openWaitingLoadBar();
                button.setEnabled(false);
                for (StackData type : modalities.getSelected() ) {
                    button.setWaitingLoadBar(RXTomoJ.getInstance().getModelController().getReconstructionController().ART(type));
                }
            });

            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, this.getFirstPane(), createARTParametersPanel());
            Utils.addGridBag(GridBagConstraints.CENTER, margin_top_inside, 0, 0, 0, GridBagConstraints.NONE, 0, 0, 0, 0, 1, 1, 0, 1, this.getFirstPane(), button);
            this.getSecondPane().setText(controller.getRessourceValue("ReconstructionPanel_art_title"));
        }

        private JPanel createARTParametersPanel(){
            //Retrive the reconstructionInfo
            ReconstructionInfo reconstructionInfo = RXTomoJ.getInstance().getModelController().getDataController().getReconstructionInfo();


            JPanel ARTParamsPanel=new JPanel();
            ARTParamsPanel.setLayout(new GridBagLayout());
            ARTParamsPanel.setBorder(BorderFactory.createTitledBorder(controller.getRessourceValue("ReconstructionPanel_art_parameter")));

            usePositivityContrainst = new JCheckBox(controller.getRessourceValue("ReconstructionPanel_art_checkBox"));
            Utils.addGridBag(GridBagConstraints.CENTER, margin_top_global, margin_left_global, margin_bottom_global, margin_right_global, GridBagConstraints.BOTH, 1, 0, 0, 0, 1, 1, 0, 0, ARTParamsPanel, usePositivityContrainst);

            Utils.addGridBag(GridBagConstraints.CENTER, margin_top_global, margin_left_global, margin_bottom_global, margin_right_global, GridBagConstraints.BOTH, 0, 0, 0, 0, 1, 1, 0, 1, ARTParamsPanel, new JLabel("number of iterations"));
            spinnerARTIterations=new JSpinner(new SpinnerNumberModel(reconstructionInfo.getNumberOfIteration(),1,100000,1));
            Utils.addGridBag(GridBagConstraints.CENTER, margin_top_global, margin_left_global, margin_bottom_global, margin_right_global, GridBagConstraints.BOTH, 1, 0, 0, 0, 1, 1, 1, 1, ARTParamsPanel, spinnerARTIterations);


            Utils.addGridBag(GridBagConstraints.CENTER, margin_top_global, margin_left_global, margin_bottom_global, margin_right_global, GridBagConstraints.BOTH, 0, 0, 0, 0, 1, 1, 0, 2, ARTParamsPanel, new JLabel("relaxation coefficient"));
            spinnerARTRelaxationCoeff=new JSpinner(new SpinnerNumberModel(reconstructionInfo.getCoefOfRelaxation(),0.00001,1.0,0.01));
//            spinnerARTRelaxationCoeff=new RXSpinner(new SpinnerNumberModel(reconstructionAlgebric.getARTrelaxation(),0.00001,1.0,0.01));
            Utils.addGridBag(GridBagConstraints.CENTER, margin_top_global, margin_left_global, margin_bottom_global, margin_right_global, GridBagConstraints.BOTH, 1, 0, 0, 0, 1, 1, 1, 2, ARTParamsPanel, spinnerARTRelaxationCoeff);

            spinnerARTRelaxationCoeff.addChangeListener(l -> RXTomoJ.getInstance().getModelController().getDataController().setRelaxationCoef((Double) spinnerARTRelaxationCoeff.getValue()));
            spinnerARTIterations.addChangeListener(l -> RXTomoJ.getInstance().getModelController().getDataController().setNumberOfIteration((Integer) spinnerARTIterations.getValue()));

            BgARTCheckbox=new JCheckBox(controller.getRessourceValue("ReconstructionPanel_art_bgArt"), reconstructionInfo.isUseBgNormalization());
            Utils.addGridBag(GridBagConstraints.CENTER, margin_top_global, margin_left_global, margin_bottom_global, margin_right_global, GridBagConstraints.BOTH, 0, 0, 0, 0, 1, 1, 0, 3, ARTParamsPanel, BgARTCheckbox);
            Utils.addGridBag(GridBagConstraints.CENTER, margin_top_global, margin_left_global, margin_bottom_global, margin_right_global, GridBagConstraints.BOTH, 0, 0, 0, 0, 1, 1, 0, 4, ARTParamsPanel, new JLabel("k"));
            spinnerBgARTk=new JSpinner(new SpinnerNumberModel(reconstructionInfo.getK(),0.00001,1000.0,0.1));

            spinnerBgARTk.setEnabled(reconstructionInfo.isUseBgNormalization());

            spinnerBgARTk.addChangeListener(l-> RXTomoJ.getInstance().getModelController().getDataController().setCoefK(Float.parseFloat(spinnerBgARTk.getValue() + "")));
            BgARTCheckbox.addActionListener(l->{
                RXTomoJ.getInstance().getModelController().getDataController().setUseBgNormalization(BgARTCheckbox.isSelected());
                spinnerBgARTk.setEnabled(BgARTCheckbox.isSelected());
            });

            usePositivityContrainst.addActionListener(l->{
                RXTomoJ.getInstance().getModelController().getDataController().setUsePositivityContrainst(usePositivityContrainst.isSelected());
            });

            Utils.addGridBag(GridBagConstraints.CENTER, margin_top_global, margin_left_global, margin_bottom_global, margin_right_global, GridBagConstraints.BOTH, 1, 0, 0, 0, 1, 1, 1, 4, ARTParamsPanel, spinnerBgARTk);


            return ARTParamsPanel;
        }


        @Override
        public void update() {
            this.setPanel(this.getCurrentDisplay());
        }
    }
}
