/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerPanel;

import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.JButtonCompute;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.JPanelModalities;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow.JFrameDarkFieldOption;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow.JFramePhaseContrastOption;
import com.soleil.nanoscopium.rxtomoj.gui.utils.BasicListener;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by bergamaschi on 27/05/2014.
 */
public class ModalitiesSelectionPanel extends InnerPanel implements BasicListener{
    private JPanelModalities jPanelModalities;


    public ModalitiesSelectionPanel(RXTomoJViewController controller) {
        super(controller);
    }

    @Override
    public void createInterface() {
        //TODO set a special view for computation type
        jPanelModalities = new JPanelModalities(StackType.getBasicType(),3,true);

        jPanelModalities.setSelected(RXTomoJ.getInstance().getModelController().getSessionController().getModalities());

        jPanelModalities.addBasicListener(this);

        JButtonCompute start = new JButtonCompute(controller.getRessourceValue("ModalitiesSelectionPanel_button_start"),this.controller);

        Utils.addGridBag(GridBagConstraints.CENTER, margin_top_global, margin_left_global, margin_bottom_inside, margin_right_global, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, this, jPanelModalities);
        Utils.addGridBag(GridBagConstraints.CENTER, margin_top_inside, margin_left_global, margin_bottom_global, margin_right_global, GridBagConstraints.NONE, 0, 0, 0, 0, 1, 1, 0, 1, this, start);
    }

    @Override
    public void update() {
        //Update the selected Modalities to compute automatically
        jPanelModalities.setStackTypeSelected(RXTomoJ.getInstance().getModelController().getSessionController().getStackTypeModalities());
    }

    @Override
    public void reset() {

    }

    @Override
    public ArrayList<JMenu> createMenu() {
        ArrayList<JMenu> menus = new ArrayList<>();

        JMenu menu = new JMenu(this.controller.getRessourceValue("ModalitiesSelectionPanel_title_Menu_option"));
        JMenuItem jMenuItem = new JMenuItem(this.controller.getRessourceValue("ModalitiesSelectionPanel_title_Menu_phaseContrast"));
        JMenuItem jMenuItem1 = new JMenuItem(this.controller.getRessourceValue("ModalitiesSelectionPanel_title_Menu_darfField"));


        jMenuItem.addActionListener( l -> new JFramePhaseContrastOption(this.controller));
        jMenuItem1.addActionListener( l -> new JFrameDarkFieldOption(this.controller));

        menu.add(jMenuItem1);
        menu.add(jMenuItem);
        menus.add(menu);

        return menus;
    }

    @Override
    public void basic_actionPerformed() {
        RXTomoJ.getInstance().getModelController().getSessionController().setModalities(this.jPanelModalities.getSelected());
    }
}
