/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerPanel;


import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualSpectrum;
import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.util.RXUtils;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.ComputationController;
import com.soleil.nanoscopium.rxtomoj.controller.DataController;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.LoadingBar;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.*;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow.JFrameProjectionOrientation;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow.JFrameRegriddingOption;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow.JFrameSamplingOption;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow.JFrameSpotMapOption;
import com.soleil.nanoscopium.rxtomoj.gui.utils.JPanelImage;
import com.soleil.nanoscopium.rxtomoj.gui.utils.JPanelImageReference;
import com.soleil.nanoscopium.rxtomoj.gui.utils.JPanelPlot;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.computationFunction.ComputeSpot;
import com.soleil.nanoscopium.rxtomoj.model.data.DataStorage;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;
import com.soleil.nanoscopium.rxtomoj.model.utils.ProgressFunction;
import ij.ImagePlus;
import ij.gui.Plot;
import ij.plugin.Slicer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * This Interface contains the Absorbtion Image where user can set the pre-processing option
 * Created by bergamaschi on 24/04/2014.
 */
public class PreProcessingPanel extends InnerPanel{
    private static final int crop = 0, fluo = 1, background = 2;

    private JPanelImageReference imagePanel;
    private Preprocessing_InnerPanel informationPanel;
    private JScrollBarRawData jScrollBarRawData;
    private ImageIcon resetPng;

    private ImageIcon reset2Png;
    private boolean lockComputation = false;

    public PreProcessingPanel(RXTomoJViewController controller) {
        super(controller);
    }

    @Override
    public void createInterface() {
        //The image Containing the Absorbtion image of the latter reconstruction
        imagePanel = new JPanelImageReference();
        resetPng = Utils.resizeIcon(64, 64, this.controller.getRessourceIcon(this.controller.getRessourceValue("reset-icon")));
        reset2Png = Utils.resizeIcon(64, 64, this.controller.getRessourceIcon(this.controller.getRessourceValue("reset-2-icon")));

        ButtonContainer buttonContainer = new ButtonContainer();
        buttonContainer.addLabel(controller.getRessourceValue("preProcessingPanel_button_label_reconstruction"),crop+"");
        buttonContainer.addLabel(controller.getRessourceValue("preProcessingPanel_button_label_fluo"),fluo+"");

        informationPanel = new Preprocessing_InnerPanel();

        buttonContainer.addBasicListener(()->this.informationPanel.setPanel( Integer.parseInt((String) buttonContainer.getCurrentSelection())));

        jScrollBarRawData = new JScrollBarRawData(JScrollBar.HORIZONTAL,0,1,0,100);
        ScrollViewPanel scrollViewPanel = new ScrollViewPanel(jScrollBarRawData,this.controller);
        jScrollBarRawData.addView(scrollViewPanel);
        jScrollBarRawData.setPreferredSize(new Dimension(50, 50));

//        JPanel test = new JPanel();
        JLabel resetButton = new JLabel(resetPng);

        resetButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if ( !lockComputation ){
                    lockComputation = true;
                    reset();
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                ((JLabel)e.getSource()).setIcon(reset2Png);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //Reset JLabel Colors
                ((JLabel)e.getSource()).setIcon(resetPng);
            }
        });


        JPanel scroll = new JPanel(new GridBagLayout());

        Utils.addGridBag(GridBagConstraints.CENTER, margin_top_inside, margin_left_global, margin_bottom_inside, margin_right_global, GridBagConstraints.BOTH, 1, 0, 0, 0, 2, 1, 0, 0, this, informationPanel);

        Utils.addGridBag(GridBagConstraints.CENTER,margin_top_global,margin_left_inside,margin_bottom_global,margin_right_inside,GridBagConstraints.BOTH,0,0,0,0,2,1,0,1,this,new JSeparator(JSeparator.HORIZONTAL));

        Utils.addGridBag(GridBagConstraints.CENTER, margin_top_global, margin_left_global, margin_bottom_global, margin_right_inside, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 2, this, imagePanel);
        Utils.addGridBag(GridBagConstraints.FIRST_LINE_START, margin_top_global, margin_left_inside, margin_bottom_global, margin_right_global, GridBagConstraints.NONE, 0, 0, 0, 0, 1, 1, 1, 2, this, resetButton);

        Utils.addGridBag(GridBagConstraints.CENTER,margin_top_inside,margin_left_global,margin_bottom_inside,margin_right_inside,GridBagConstraints.BOTH,0.3,0,0,0,1,1,0,3,scroll,scrollViewPanel);
        Utils.addGridBag(GridBagConstraints.CENTER,margin_top_inside,margin_left_inside,margin_bottom_inside,margin_right_global,GridBagConstraints.BOTH,1,0,0,0,2,1,1,3,scroll,jScrollBarRawData);
        Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,3,this,scroll);

        Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.HORIZONTAL, 1, 0, 0, 0, 2, 1, 0, 4, this, buttonContainer);
//        imagePanel.setPreferredSize(new Dimension(200,1000));

        informationPanel.setPreferredSize(new Dimension(0,200));
        buttonContainer.init(0);
        informationPanel.setPanel(0);
    }

    @Override
    public void update() {
        //Display a Big Computation Button to start computing the absorption image
        imagePanel.removeAll();
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
        DataStorage dataStorage = dataController.getDataStorage();
        //If already contained directly displays
        if ( dataStorage.getStackDataStorage().containsKey(StackType.ABSORPTION_REF) ){
//            ImagePlus imagePlus = new ImagePlus(dataController.getStackPath(StackType.ABSORPTION_REF));
            RXImage image = dataController.getComputedStack(dataStorage.getStackDataStorage().get(StackType.ABSORPTION_REF).get(0));
            this.jScrollBarRawData.setMaximum(image.getSize());
//            imagePanel.setImage(imagePlus);
            imagePanel.setImage(image);

            jScrollBarRawData.addHdf5(imagePanel);
//            imagePanel.resetDrawingSize();
            informationPanel.update();
        }
        //Else do nothing, wait for the refresh event ( Maybe show the waiting in the interface )
        else{
            if ( RXTomoJ.getInstance().getModelController().getSessionController().isSessionOpen() ){
//                ((RXTomoJComputationFrame)this.controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).setLockInnerPanel(true);
//
//                //Compute Absorption image
//                ProgressFunction progressFunction = RXTomoJ.getInstance().getModel().imageCreation(RXTomoJModelController.StackType.ABSORPTION_REF);
//
//                LoadingBar loadingBar = new LoadingBar(progressFunction,LoadingBar.PANEL);
//                loadingBar.addEndListener(()->{
//                    int timeOut = 20;
//                    //Wait utile the stack is saved
//                    while ( !RXTomoJ.getInstance().getModel().stackExist(RXTomoJModelController.StackType.ABSORPTION_REF) && timeOut > 0 ){
//                        try {
//                            TimeUnit.SECONDS.sleep((long) 1.0);
//                            timeOut--;
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                    if ( timeOut == 0 ){
//                        System.err.println("ERROR : TIME OUT, no output image REFERENCE");
//                        return;
//                    }
//
//                    this.update();
//                });
//                loadingBar.start();
//                imagePanel.add(loadingBar.getContent());

            }
        }
//        ComputeSpot computeSpot = dataController.getComputeSpot(StackType.ABSORPTION);
//        if ( computeSpot != null ) {
            this.informationPanel.getRoiPanel().updateRoiPanel(RXTomoJ.getInstance().getModelController().getDataController().getReductionInfo().getReductionROI());
//        }

        //Unlock the innerPanel
        ((RXTomoJComputationFrame)this.controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).setLockInnerPanel(false);
    }

    @Override
    public void updateGraphics() {
//        this.imagePanel.resetDrawingSize();
    }

    @Override
    public void reset() {
        imagePanel.removeAll();


        LoadingBar loadingBar = new LoadingBar(LoadingBar.PANEL);
//        loadingBar.setDisplayType(LoadingBar.PANEL);
        loadingBar.addEndListener(()->{
            lockComputation = false;
            //Initialize the crop limits
            informationPanel.updateCrop();
            this.update();
        });
        loadingBar.start();
        imagePanel.add(loadingBar.getContent());

        ProgressFunction progressFunction = RXTomoJ.getInstance().getModelController().getComputationController().imageCreation(StackType.ABSORPTION_REF);

        loadingBar.addFunction(progressFunction);

        //Lock the innerPanel
        ((RXTomoJComputationFrame) this.controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).setLockInnerPanel(true);
        this.validate();
    }

    @Override
    public ArrayList<JMenu> createMenu() {
        ArrayList<JMenu> menus = new ArrayList<>();

        JMenu menu = new JMenu(this.controller.getRessourceValue("preProcessingPanel_title_Menu_option"));
//        JMenuItem jMenuItem = new JMenuItem(this.controller.getRessourceValue("preProcessingPanel_title_Menu_reslice"));

        JMenuItem jMenuItem2 = new JMenuItem(this.controller.getRessourceValue("preProcessingPanel_title_Menu_sampling"));
        JMenuItem jMenuItem3 = new JMenuItem(this.controller.getRessourceValue("preProcessingPanel_title_Menu_spotMap"));
        JMenuItem jMenuItem4 = new JMenuItem(this.controller.getRessourceValue("preProcessingPanel_title_Menu_setOrientation"));
        JMenuItem jMenuItem5 = new JMenuItem(this.controller.getRessourceValue("preProcessingPanel_title_Menu_regridding"));


        jMenuItem2.addActionListener( l -> new JFrameSamplingOption(this.controller));
//        jMenuItem3.addActionListener( l -> new JFrameSpotMapOption(this.controller));
        jMenuItem4.addActionListener(l -> new JFrameProjectionOrientation(this.controller));
        jMenuItem5.addActionListener(l -> new JFrameRegriddingOption(this.controller));


//        menu.add(jMenuItem);
        menu.add(jMenuItem2);
        menu.add(jMenuItem3);
        menu.add(jMenuItem4);
        menu.add(jMenuItem5);
        menus.add(menu);
        return menus;
    }




    /**
     * Container of information
     */
    private class Preprocessing_InnerPanel extends InformationPanel {

        private JPanelPlot jPanelPlot;

        private RoiPanel roiPanel;

        public Preprocessing_InnerPanel(){
            super();
            //Init the JPlot for latter use in Fluo
            jPanelPlot = new JPanelPlot();
            roiPanel = new RoiPanel(controller,RoiPanel.HORIZONTAL);
//            ComputeSpot computeSpot = RXTomoJ.getInstance().getModelController().getDataController().getComputeSpot(StackType.ABSORPTION);
//            if ( computeSpot != null ) {
                roiPanel.updateRoiPanel(RXTomoJ.getInstance().getModelController().getDataController().getReductionInfo().getReductionROI());
//            }
            roiPanel.addBasicListener(()->this.updateCrop());
        }

        @Override
        public void setPanel(int type){
            super.setPanel(type);

            switch (type){
                case PreProcessingPanel.crop:
                    createCropReconstruction();
                    break;
                case PreProcessingPanel.background:
                    createSubBackground();
                    break;
                case PreProcessingPanel.fluo:
                    createFluorescence();
                    break;
                default:
                    this.setCurrentDisplay(-1);
                    System.err.println("Unknown Type : "+this.getClass().toString());
                    break;
            }

            validate();
        }

        /**
         * Set the PanelInformation with the subBackground interface
         */
        private void createSubBackground(){
            if ( imagePanel != null && imagePanel.getImageComponent() != null ) {
//                imagePanel.getImagePlus().deleteRoi();
            }
            JPanel firstPane = this.getFirstPane();
            firstPane.removeAll();
            firstPane.setBackground(Color.blue);
            this.getSecondPane().setText(controller.getRessourceValue("PreprocessingPanel_information_background_man"));
        }

        /**
         * Set the PanelInformation with the subBackground interface
         */
        private void createFluorescence(){
//            if ( imagePanel.getImageComponent() != null ) {
//                imagePanel.lockROIModification();
//            }

            //Synchronize the Spectrum display with the selected Spectrums
            Object[] datas = ((RXTomoJComputationFrame)controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).getJPlotHdf5(0);
            jPanelPlot.synchroniseData((ArrayList<Hdf5VirtualSpectrum>) datas[0], (ArrayList<Color>) datas[1]);
            jPanelPlot.setMeanSpectrum(true);

            this.getFirstPane().removeAll();
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, this.getFirstPane(), jPanelPlot);

            this.getSecondPane().setText(controller.getRessourceValue("preProcessingPanel_information_fluo_man"));
        }

        /**
         * Set the PanelInformation with the subBackground interface
         */
        private void createCropReconstruction(){
            this.getFirstPane().removeAll();

//            if ( SessionObject.isOpen() ) {
//            TODO set and Get the current crop parameter
//            if ( imagePanel != null && imagePanel.getImageComponent() != null ) {
//                imagePanel.getImagePlus().setRoi(RXTomoJ.getInstance().getModelController().getDataController().getComputeSpot(StackType.ABSORPTION).reconstructionToRec());
//            }

            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, this.getFirstPane(), roiPanel);
            this.getSecondPane().setText(controller.getRessourceValue("preProcessingPanel_information_crop_man"));
        }

        public void addCanvasListener(){
            if ( imagePanel != null && imagePanel.getImageComponent() != null ) {
                imagePanel.getImageComponent().addListener(new MouseAdapter() {

                    @Override
                    public void mouseReleased(MouseEvent e) {
                        switch (Preprocessing_InnerPanel.this.getCurrentDisplay()) {
                            case PreProcessingPanel.fluo:
                                //Compute the fluorescence on the ROI
                                fluoMouseEvent();
                                break;
                            case PreProcessingPanel.background:
                                //Set the Background normalization

                                break;
                            case PreProcessingPanel.crop:
                                //Crop the current image with the defined ROI
                                cropMouseEvent();
                                break;
                            default:
                                System.err.println("Unknown Type");
                                break;
                        }

                    }
                });
            }
        }

        private void fluoMouseEvent(){
            //get Roi modification
            Rectangle rec = imagePanel.getCurrentRoi();

            DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
            ComputationController computationController =  RXTomoJ.getInstance().getModelController().getComputationController();

            int[] roi = {rec.x, (int) (rec.x+rec.getWidth()),rec.y, (int) (rec.y+rec.getHeight()), jScrollBarRawData.getValue(), jScrollBarRawData.getValue()};
            int[][] newRoi = computationController.computeRegriddedROI(roi);

            for ( int i = 0 ; i < newRoi.length ; i++ ) {
                System.out.println(i+" : "+newRoi[i][0]+"/"+newRoi[i][1]);
            }

            computationController.computeSumSpectra(newRoi,dataController.getSelectedSpectrum());
//            for ( String spectrum : dataController.getSelectedSpectrum() ) {
//                if ( dataController.getFluorescenceData(spectrum).getGlobalSize() < 1 ||
//                        dataController.getFluorescenceData(spectrum).copyData() == null ){
//                    spcToComput.add(spectrum);
//                }
//            }
//            computeSumSpectra(null,spcToComput,true);

//            ArrayList<Hdf5VirtualSpectrum> hdf5VirtualStacks = jPanelPlot.getHdf5Data();
//            RXImage image = (RXImage) RXTomoJ.getInstance().getModelController().getDataController().getStorageData(StackType.ABSORPTION_REF);
//            int width = image.getWidth();
//            int height = image.getHeight();
//
//            for (Hdf5VirtualSpectrum hdf5VirtualStack : hdf5VirtualStacks) {
//                float[] finalSpectre = new float[2048];
//                //Construct the image by getting every points
//                for (int y = (int) rec.getY(); y < rec.getMaxY(); y++) {
//                    for (int x = (int) rec.getX(); x < rec.getMaxX(); x++) {
//                        float[] data = hdf5VirtualStack.getPixels(x + y * width + jScrollBarRawData.getValue() * width * height);
//                        for (int i = 0; i < data.length; i++) {
//                            finalSpectre[i] += data[i];
//                        }
//                    }
//                }
//
//                Plot plot = new Plot("Test Plot", "spectrum", "intensity");
//                float[] xVal = new float[2048];
//                float maxVal = 0.0f;
//                for (int i = 0; i < 2048; i++) {
//                    xVal[i] = i;
//                    if (maxVal < finalSpectre[i]) {
//                        maxVal = finalSpectre[i];
//                    }
//
//                }
//                plot.setLineWidth(2);
//                plot.setLimits(0, finalSpectre.length, 0, maxVal);
//                plot.setColor(Color.red);
//                plot.addPoints(xVal, finalSpectre, Plot.LINE);
//
//                plot.show();
//                plot.setColor(Color.red);
//                plot.draw();
//            }
        }

        private void cropMouseEvent(){
            if ( imagePanel != null && imagePanel.getImageComponent() != null ) {
                Rectangle rec = imagePanel.getCurrentRoi();
                int[] limits = roiPanel.getRoiLimit();
                limits[0] = rec.x;
                limits[1] = (int) (rec.x+ rec.getWidth());
                limits[2] = rec.y;
                limits[3] = (int) (rec.y + rec.getHeight());
                this.updateCrop(limits);
//                RXTomoJ.getInstance().getModel().setAllReconstructionRec(limits);

//                RXTomoJ.getInstance().getModel().setAllReconstructionRec(rec);
                roiPanel.updateRoiPanel(RXTomoJ.getInstance().getModelController().getDataController().getReductionInfo().getReductionROI());
//                this.updateCrop();
//                SessionObject.getCurrentSessionObject().getAnalyseFactory().getCurrent().setCurrentRoiImage(rec);
//                roiPanel.updateRoiPanel(rec);
            }


        }

        private void updateCrop(int[] roi){
//            int[] roi = roiPanel.getRoiLimit();
            int[][] roi_s = new int[3][2];
            roi_s[0][0] = roi[0];
            roi_s[0][1] = roi[1];
            roi_s[1][0] = roi[2];
            roi_s[1][1] = roi[3];
            roi_s[2][0] = roi[4];
            roi_s[2][1] = roi[5];
            RXTomoJ.getInstance().getModelController().getDataController().setReductionRoi(roi_s);
        }

        private void updateCrop(){
            int[] roi = roiPanel.getRoiLimit();
            int[][] roi_s = new int[3][2];
            roi_s[0][0] = roi[0];
            roi_s[0][1] = roi[1];
            roi_s[1][0] = roi[2];
            roi_s[1][1] = roi[3];
            roi_s[2][0] = roi[4];
            roi_s[2][1] = roi[5];
            RXTomoJ.getInstance().getModelController().getDataController().setReductionRoi(roi_s);
        }

        @Override
        public void update() {
            this.addCanvasListener();
            if ( this.getCurrentDisplay() != -1 ) {
                this.setPanel(this.getCurrentDisplay());
            }
            else{
                this.setPanel(PreProcessingPanel.background);
            }
            this.roiPanel.updateSpinnerLimits((RXTomoJ.getInstance().getModelController().getDataController().getComputedStack(StackType.ABSORPTION_REF)).getSize());
        }



        public RoiPanel getRoiPanel() {
            return roiPanel;
        }
    }
}