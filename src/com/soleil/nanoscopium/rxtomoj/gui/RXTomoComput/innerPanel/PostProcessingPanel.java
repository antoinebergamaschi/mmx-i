/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerPanel;

import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.ComputationController;
import com.soleil.nanoscopium.rxtomoj.controller.DataController;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.*;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow.JFrameDefaultModalityOption;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow.JFrameMobileMeanOption;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow.JFrameSineFittingOption;
import com.soleil.nanoscopium.rxtomoj.gui.utils.EditableNumericField;
import com.soleil.nanoscopium.rxtomoj.gui.utils.JPanelImage;
import com.soleil.nanoscopium.rxtomoj.gui.utils.JPanelPlot;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.data.NormalizationInfo;
import com.soleil.nanoscopium.rxtomoj.model.data.ShiftImageInfo;
import com.soleil.nanoscopium.rxtomoj.model.filters.BackgroundSubtraction;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.OrientationType;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;

import javax.swing.*;
import java.awt.*;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by antoine bergamaschi on 05/08/2014.
 */
//TODO add select Modalites, add save modalities ( but not initial one or give possibilitie to return to old initial )

public class PostProcessingPanel extends InnerPanel {

    public final static int normalization=0,sinusoidalshift=1;

    private JPanelImage imagePanel_initial;
    private JPanelImage imagePanel;
    private PostProcessing_innerPanel informationPanel;
    private JScrollBarRawData jScrollBarRawData;
    public PostProcessingPanel(RXTomoJViewController controller) {
        super(controller);
    }

    @Override
    public void createInterface() {
        //The image Containing the Absorbtion image of the latter reconstruction
        imagePanel_initial = new JPanelImage();
        imagePanel = new JPanelImage();

        ButtonContainer buttonContainer = new ButtonContainer();
        buttonContainer.addLabel(controller.getRessourceValue("PostprocessingPanel_button_label_normalization"),normalization+"");
        buttonContainer.addLabel(controller.getRessourceValue("PostprocessingPanel_button_label_sinusShift"),sinusoidalshift+"");
//        buttonContainer.addLabel(controller.getRessourceValue("PreprocessingPanel_button_label_fluo"),fluo+"");

        informationPanel = new PostProcessing_innerPanel();

        buttonContainer.addBasicListener(() -> {
            this.informationPanel.setPanel(Integer.parseInt((String) buttonContainer.getCurrentSelection()));
            this.update();
        });

        jScrollBarRawData = new JScrollBarRawData(JScrollBar.HORIZONTAL,0,1,0,100);
        ScrollViewPanel scrollViewPanel = new ScrollViewPanel(jScrollBarRawData,this.controller);
        jScrollBarRawData.addView(scrollViewPanel);
        jScrollBarRawData.setPreferredSize(new Dimension(50, 50));

        Utils.addGridBag(GridBagConstraints.CENTER, margin_top_global, margin_left_global, margin_bottom_global, margin_right_inside, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, this, imagePanel_initial);
        Utils.addGridBag(GridBagConstraints.CENTER, margin_top_global, margin_left_inside, margin_bottom_global, margin_right_global, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 0, this, imagePanel);

        JPanel scroll = new JPanel(new GridBagLayout());
        Utils.addGridBag(GridBagConstraints.CENTER,margin_top_inside,margin_left_global,margin_bottom_inside,margin_right_inside,GridBagConstraints.BOTH,0.3,0,0,0,1,1,0,0,scroll,scrollViewPanel);
        Utils.addGridBag(GridBagConstraints.CENTER,margin_top_inside,margin_left_inside,margin_bottom_inside,margin_right_global,GridBagConstraints.BOTH,1,0,0,0,1,1,1,0,scroll,jScrollBarRawData);

        Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,1,this,scroll);

        Utils.addGridBag(GridBagConstraints.CENTER,margin_top_global,margin_left_inside,margin_bottom_global,margin_right_inside,GridBagConstraints.BOTH,0,0,0,0,2,1,0,2,this,new JSeparator(JSeparator.HORIZONTAL));
        Utils.addGridBag(GridBagConstraints.CENTER, margin_top_inside, margin_left_global, margin_bottom_inside, margin_right_global, GridBagConstraints.BOTH, 1, 0, 0, 0, 2, 1, 0, 3, this, informationPanel);
        Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.HORIZONTAL, 1, 0, 0, 0, 2, 1, 0, 4, this, buttonContainer);
//        imagePanel.setPreferredSize(new Dimension(200,1000));
        informationPanel.setPreferredSize(new Dimension(0,200));
        buttonContainer.init(0);
        informationPanel.setPanel(0);

        this.update();
    }

    @Override
    public void update() {
        this.imagePanel_initial.removeAll();
        this.imagePanel.removeAll();
        jScrollBarRawData.removeAll();
        ComputationController computationController  =  RXTomoJ.getInstance().getModelController().getComputationController();

        //Try to open one of the three modalities
        RXImage imageStack = computationController.setStackOrientation(RXTomoJ.getInstance().getModelController().getDataController().getDefaultModality(),OrientationType.XZY);
        if ( imageStack != null ) {
//            computationController.initializeShiftImage(imageStack);

            this.jScrollBarRawData.setMaximum(imageStack.getSize());
            imagePanel_initial.setImage(imageStack);
            jScrollBarRawData.addHdf5(imagePanel_initial);


//            if ( normalization ){
//                this.jScrollBarRawData.setMaximum(imageStack.getSize());
//                imagePanel.setImage(imageStack);
//                jScrollBarRawData.addHdf5(imagePanel);
//            }
        }

        informationPanel.update();

        this.revalidate();
        this.repaint();
    }

    @Override
    public void updateGraphics() {
//        this.imagePanel.resetDrawingSize();
//        this.imagePanel_initial.resetDrawingSize();
    }

    @Override
    public void reset() {
        this.update();
    }

    @Override
    public ArrayList<JMenu> createMenu() {
        ArrayList<JMenu> menus = new ArrayList<>();

        JMenu menu = new JMenu(this.controller.getRessourceValue("preProcessingPanel_title_Menu_option"));
        JMenuItem jMenuItem = new JMenuItem(this.controller.getRessourceValue("preProcessingPanel_title_Menu_SineFit"));
        JMenuItem jMenuItem2 = new JMenuItem(this.controller.getRessourceValue("preProcessingPanel_title_Menu_defaultModality"));

        jMenuItem.addActionListener( l -> new JFrameSineFittingOption(this.controller));
//        jMenuItem.addActionListener( l -> new JFrameMobileMeanOption(this.controller));
        jMenuItem2.addActionListener( l -> new JFrameDefaultModalityOption(this.controller));

        menu.add(jMenuItem);
        menu.add(jMenuItem2);
        menus.add(menu);

        return menus;
    }

    private class PostProcessing_innerPanel extends InformationPanel {
        public PostProcessing_innerPanel(){
            super();
        }

        @Override
        public void update() {
            if ( this.getCurrentDisplay() != -1 ) {
                this.setPanel(this.getCurrentDisplay());
            }
            else{
                this.setPanel(PostProcessingPanel.normalization);
            }
        }

        @Override
        public void setPanel(int type) {
            super.setPanel(type);

            switch (type){
                case PostProcessingPanel.normalization:
                    createSubBackground();
                    break;
                case PostProcessingPanel.sinusoidalshift:
                    createShiftImage();
                    break;
                default:
                    this.setCurrentDisplay(-1);
                    System.err.println("Unknown Type : "+this.getClass().toString());
                    break;
            }
            validate();
        }

        public void createSubBackground(){
            JLabel label_numberOfForeseenPixel = new JLabel(controller.getRessourceValue("Postprocessing_information_background_button_numbOfPixel"));
            JLabel label_variationCoef = new JLabel(controller.getRessourceValue("Postprocessing_information_background_button_varCoef"));

            DataController dataController  = RXTomoJ.getInstance().getModelController().getDataController();

            NormalizationInfo normalizationInfo = dataController.getNormalizationInfo();
            //Update with saved value
//            BackgroundSubtraction backgroundSubtraction = dataController.getBackgroundSubstraction();

            JPanel firstPane = this.getFirstPane();
            firstPane.removeAll();

            JCheckBox useNormalization = new JCheckBox(controller.getRessourceValue("Postprocessing_information_background_check_doBackground"));
            useNormalization.addActionListener(l-> dataController.setUseBackgroundCorrection(useNormalization.isSelected()));

            EditableNumericField numberOfForSeenPixel = Utils.createNumberTextField(-1,-1,-1,new Integer(1),new Integer(20),null);
            EditableNumericField variationCoef = Utils.createNumberTextField(5,5, NumberFormat.FRACTION_FIELD,0.00000f,10000.000f,null);

            JCheckBox computeLeft = new JCheckBox(controller.getRessourceValue("Postprocessing_information_background_button_left"));
            JCheckBox computeRight = new JCheckBox(controller.getRessourceValue("Postprocessing_information_background_button_right"));

            //JButton use to validate_ the changes
            JButton validate = new JButton(controller.getRessourceValue("validate"));

            validate.addActionListener(l -> {
                try {
                    //Disable the button
                    validate.setEnabled(false);

                    numberOfForSeenPixel.commitEdit();
                    variationCoef.commitEdit();

                    RXImage rxImage = RXTomoJ.getInstance().getModelController().getDataController().getDefaultRXImage();

                    DataController da = RXTomoJ.getInstance().getModelController().getDataController();

                    da.setBackgroundIntegrationDistance((Integer) numberOfForSeenPixel.getValue());
                    da.setBackgroundLeftComputation(computeLeft.isSelected());
                    da.setBackgroundRightComputation(computeRight.isSelected());
                    da.setBackgroundMaximalIntegrationValue((Float) variationCoef.getValue());

                    //The function finished normally
                    RXTomoJ.getInstance().getModelController().addMMXIControllerSpecialListener(
                            MMXIControllerEvent.MMXIControllerEventID.COMPUTE_NORMALIZATION,
                            MMXIControllerEvent.MMXIControllerEventType.FINISHED, k->{
                                da.storeVolatileStack(StackType.PRECOMPUTED_BACKGROUND_NORMALIZED, rxImage);

                                imagePanel.setImage(rxImage);
                                jScrollBarRawData.addHdf5(imagePanel);

                                this.doLayout();
                                //Enable back
                                validate.setEnabled(true);


                                //Remove the error listener
                                RXTomoJ.getInstance().getModelController().removeMMXIControllerSpecialListener(
                                        MMXIControllerEvent.MMXIControllerEventID.COMPUTE_NORMALIZATION,
                                        MMXIControllerEvent.MMXIControllerEventType.ERROR);
                            }
                    );

                    //An error occurs
                    RXTomoJ.getInstance().getModelController().addMMXIControllerSpecialListener(
                            MMXIControllerEvent.MMXIControllerEventID.COMPUTE_NORMALIZATION,
                            MMXIControllerEvent.MMXIControllerEventType.ERROR, k->{
                                //Enable back
                                validate.setEnabled(true);

                                //Remove the finished listener
                                RXTomoJ.getInstance().getModelController().removeMMXIControllerSpecialListener(
                                        MMXIControllerEvent.MMXIControllerEventID.COMPUTE_NORMALIZATION,
                                        MMXIControllerEvent.MMXIControllerEventType.FINISHED);

                            }
                    );

                    RXTomoJ.getInstance().getModelController().getComputationController().computeBackgroundNormalization(rxImage);


                } catch (ParseException e) {
                    e.printStackTrace();
                }
            });

            numberOfForSeenPixel.setValue(normalizationInfo.getNumberOfForeseenPixel());
            variationCoef.setValue(normalizationInfo.getVariationCoefficient());
            computeLeft.setSelected(normalizationInfo.isComputeLeft());
            computeRight.setSelected(normalizationInfo.isComputeRigth());
            useNormalization.setSelected(normalizationInfo.isDoBackgroundNormalization());


            if ( dataController.getComputedStack(StackType.PRECOMPUTED_BACKGROUND_NORMALIZED) != null ){
                imagePanel.setImage(dataController.getComputedStack(StackType.PRECOMPUTED_BACKGROUND_NORMALIZED));
                jScrollBarRawData.addHdf5(imagePanel);
            }

            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 0, 1, 0, 0, 1, 1, 0, 0, this.getFirstPane(), useNormalization);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 0, 1, 0, 0, 1, 1, 0, 1, this.getFirstPane(), label_numberOfForeseenPixel);
            Utils.addGridBag(GridBagConstraints.CENTER, 10, 5, 10, 5, GridBagConstraints.BOTH, 1, 0, 0, 0, 1, 1, 1, 1, this.getFirstPane(), numberOfForSeenPixel);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 0, 1, 0, 0, 1, 1, 0, 2, this.getFirstPane(), label_variationCoef);
            Utils.addGridBag(GridBagConstraints.CENTER, 10, 5, 10, 5, GridBagConstraints.BOTH, 1, 0, 0, 0, 1, 1, 1, 2, this.getFirstPane(), variationCoef);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 0, 1, 0, 0, 1, 1, 0, 3, this.getFirstPane(), computeLeft);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 0, 1, 0, 0, 1, 1, 1, 3, this.getFirstPane(), computeRight);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 2, 1, 0, 4, this.getFirstPane(), validate);
            this.getSecondPane().setText(controller.getRessourceValue("Postprocessing_information_background_man"));
        }

        public void createShiftImage(){
            JPanel firstPane = this.getFirstPane();
            firstPane.removeAll();
            JPanelPlot panelPlot = new JPanelPlot();
            JButton validate = new JButton(controller.getRessourceValue("validate"));

            JCheckBox useNormalization = new JCheckBox(controller.getRessourceValue("Postprocessing_information_shiftImage_check_doNormalization"));
            useNormalization.addActionListener(l-> RXTomoJ.getInstance().getModelController().getDataController().setUseWobbleCorrection(useNormalization.isSelected()));


            JLabel shiftVariation_label = new JLabel(controller.getRessourceValue("Postprocessing_information_shiftImage_label_threshold"));
            JLabel recursiveFit_label = new JLabel(controller.getRessourceValue("Postprocessing_information_shiftImage_label_recursiveFit"));

            EditableNumericField recursiveFit = Utils.createNumberTextField(-1,-1,NumberFormat.INTEGER_FIELD,new Integer(1),new Integer(10000),null);
            EditableNumericField shiftVariation = Utils.createNumberTextField(-1,-1,NumberFormat.FRACTION_FIELD,0.000f,100.000f,null);

            JCheckBox interpolation = new JCheckBox(controller.getRessourceValue("Postprocessing_information_shiftImage_check_interpolation"));
            final JCheckBox applyBackgroundSub = new JCheckBox(controller.getRessourceValue("Postprocessing_information_shiftImage_applyTransformation"));


            applyBackgroundSub.addActionListener(l->{
                RXImage image = RXTomoJ.getInstance().getModelController().getDataController().getDefaultRXImage();
                RXTomoJ.getInstance().getModelController().getDataController().setUseBackgroundCorrection(applyBackgroundSub.isSelected());
                if ( applyBackgroundSub.isSelected() ) {
                    RXTomoJ.getInstance().getModelController().getComputationController().computeBackgroundNormalization(image);
                    //Wait for completion
                    RXTomoJ.getInstance().getModelController().addMMXIControllerSpecialListener(
                            MMXIControllerEvent.MMXIControllerEventID.COMPUTE_NORMALIZATION,
                            MMXIControllerEvent.MMXIControllerEventType.FINISHED, k->{
                                ComputationUtils.setAbsoptionCoef(image);
                                imagePanel_initial.setImage(image);
                            }
                    );
                }
            });

            //Event fire when Computing the Shift
            validate.addActionListener(l->{
                try {
                    //Lock the Button
                    validate.setEnabled(false);
                    //Lock interface
                    ((RXTomoJComputationFrame) controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).setLockInnerPanel(true);

                    shiftVariation.commitEdit();
                    recursiveFit.commitEdit();

                    DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();


                    dataController.setMinimumShiftValue((Float) shiftVariation.getValue());
                    dataController.useWobblingInterpolation(interpolation.isSelected());
                    dataController.setWobblingIterationCorrectionNumber((Integer) recursiveFit.getValue());

                    RXImage ref = RXTomoJ.getInstance().getModelController().getDataController().getDefaultRXImage();

                    RXImage toShift = RXTomoJ.getInstance().getModelController().getDataController().getDefaultRXImage();

                    //Fire the computation of the ShiftImage
                    RXTomoJ.getInstance().getModelController().getComputationController().shiftImageWithUpdate(toShift, ref);

                    //Wait that shiftImageWithUpdate finish
                    RXTomoJ.getInstance().getModelController().addMMXIControllerSpecialListener(
                            MMXIControllerEvent.MMXIControllerEventID.COMPUTE_SINUS_SHIFT,
                            MMXIControllerEvent.MMXIControllerEventType.FINISHED, k->{
                                imagePanel.setImage(RXTomoJ.getInstance().getModelController().getComputationController().wobbleCorrection(toShift));
                                jScrollBarRawData.addHdf5(imagePanel);

                                panelPlot.removeAllData();

                                float[] x = dataController.getComputedStack(StackType.LINE_POSITIONS).copyData();
                                float[] yReal = dataController.getComputedStack(StackType.CENTER_OF_MASS).copyData();
                                float[] yCoputed = dataController.getComputedStack(StackType.COMPUTED_FIT).copyData();

                                panelPlot.addData(x,yCoputed,Color.red);
                                panelPlot.addData(x,yReal,Color.black);

                                //UnLock the Button
                                validate.setEnabled(true);
                                //Lock interface
                                ((RXTomoJComputationFrame) controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).setLockInnerPanel(false);


                                //Remove the error listener
                                RXTomoJ.getInstance().getModelController().removeMMXIControllerSpecialListener(
                                        MMXIControllerEvent.MMXIControllerEventID.COMPUTE_SINUS_SHIFT,
                                        MMXIControllerEvent.MMXIControllerEventType.ERROR);
                            }
                    );


                    RXTomoJ.getInstance().getModelController().addMMXIControllerSpecialListener(
                            MMXIControllerEvent.MMXIControllerEventID.COMPUTE_SINUS_SHIFT,
                            MMXIControllerEvent.MMXIControllerEventType.ERROR, k->{
                                //UnLock the Button
                                validate.setEnabled(true);
                                //Unlock interface
                                ((RXTomoJComputationFrame) controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).setLockInnerPanel(false);

                                //Remove the finished listener
                                RXTomoJ.getInstance().getModelController().removeMMXIControllerSpecialListener(
                                        MMXIControllerEvent.MMXIControllerEventID.COMPUTE_SINUS_SHIFT,
                                        MMXIControllerEvent.MMXIControllerEventType.FINISHED);
                            }
                    );

//                    imagePanel.setImage(imageStack);
//                    jScrollBarRawData.addHdf5(imagePanel);
//
//                    panelPlot.removeAllData();
//
//                    float[] x = dataController.getComputedStack(StackType.LINE_POSITIONS).copyData();
//                    float[] yReal = dataController.getComputedStack(StackType.CENTER_OF_MASS).copyData();
//                    float[] yCoputed = dataController.getComputedStack(StackType.COMPUTED_FIT).copyData();
////                    ShiftImage shiftImage = dataController.getShiftImage();
//
//                    panelPlot.addData(x,yCoputed,Color.red);
//                    panelPlot.addData(x,yReal,Color.black);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            });

            DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

//            ShiftImage shiftImage = dataController.getShiftImage();
            NormalizationInfo normalizationInfo = dataController.getNormalizationInfo();
//            ShiftImageInfo shiftImageInfo = dataController.getShiftImageInfo();
            shiftVariation.setValue(normalizationInfo.getMinimumShift());
            interpolation.setSelected(normalizationInfo.isUseInterpolation());
            recursiveFit.setValue(normalizationInfo.getRecursiveNumber());
            applyBackgroundSub.setSelected(normalizationInfo.isUseBackgroundNormalization());
            useNormalization.setSelected(normalizationInfo.isDoWobbleCorrection());

            RXImage x = dataController.getComputedStack(StackType.LINE_POSITIONS);
            RXImage yReal = dataController.getComputedStack(StackType.CENTER_OF_MASS);
            float[] x1 = new float[1];


            panelPlot.removeAllData();
            if ( x != null && yReal != null ) {
                x1 = x.copyData();
                float[] yReal1 = yReal.copyData();
                if (x1 != null && yReal1 != null) {
                    panelPlot.addData(x1, yReal1, Color.black);
                }
            }

            RXImage yComputed = dataController.getComputedStack(StackType.COMPUTED_FIT);
            if ( yComputed != null ) {
                float[] yCoputed = yComputed.copyData();
                if ( x1 != null && yCoputed != null ) {
                    panelPlot.addData(x1, yCoputed, Color.red);
                }
            }

            if ( dataController.getComputedStack(StackType.PRECOMPUTED_SHIFT) != null ){
                imagePanel.setImage(dataController.getComputedStack(StackType.PRECOMPUTED_SHIFT));
                jScrollBarRawData.addHdf5(imagePanel);
            }

            //IF the Background is already selected
            if ( applyBackgroundSub.isSelected() ){
                RXImage image = RXTomoJ.getInstance().getModelController().getDataController().getDefaultRXImage();
                RXTomoJ.getInstance().getModelController().getComputationController().computeBackgroundNormalization(image);
                ComputationUtils.setAbsoptionCoef(image);
                imagePanel_initial.setImage(image);
            }

            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 0, 1, 0, 0, 2, 1, 0, 0, this.getFirstPane(), useNormalization);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 0, 1, 0, 0, 2, 1, 0, 1, this.getFirstPane(), applyBackgroundSub);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 0, 1, 0, 0, 1, 1, 0, 2, this.getFirstPane(), shiftVariation_label);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 0, 1, 0, 0, 1, 1, 1, 2, this.getFirstPane(), shiftVariation);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 0, 1, 0, 0, 1, 1, 0, 3, this.getFirstPane(), recursiveFit_label);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 0, 1, 0, 0, 1, 1, 1, 3, this.getFirstPane(), recursiveFit);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 0, 1, 0, 0, 2, 1, 0, 4, this.getFirstPane(), interpolation);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 0, 1, 0, 0, 2, 1, 0, 5, this.getFirstPane(), validate);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 4, 2, 0, this.getFirstPane(), panelPlot);
            this.getSecondPane().setText(controller.getRessourceValue("Postprocessing_information_shift_man"));
        }
    }
}
