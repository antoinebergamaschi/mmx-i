/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput;


import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStack;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Updatable;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.SessionObject.DataType;
import com.soleil.nanoscopium.rxtomoj.model.data.ReductionInfo;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by antoine bergamaschi on 24/01/14.
 */
public class PanelImageChoice extends JPanel implements Updatable {

    private ButtonGroup buttonGroup;
    private JPanel image;
    private RXTomoJViewController controller;
    private Hdf5VirtualStack hdf5VirtualStack;
    private String backPng;
    private String nextPng;

    JButton buttonBack,buttonNext;

    public PanelImageChoice(RXTomoJViewController controller){
        super(new GridBagLayout());
        this.controller = controller;
        backPng = this.controller.getRessourceValue("back-icon");
        nextPng = this.controller.getRessourceValue("next-icon");
        this.build();

        RXTomoJ.getInstance().getModelController().addMMXIControllerUpdatedListener(l -> {
            switch (l.getEventID()) {
                case REDUCTIONINFO_TRANSMISSION_ROI:
                    this.controller.setRawSpotImage(hdf5VirtualStack);
                break;
            }
        });
    }


    private void build(){
        this.image = new JPanel();
        JRadioButton basic = new JRadioButton("No element");
        basic.setEnabled(false);
        this.image.add(basic);
        this.buttonGroup = new ButtonGroup();

//        this.buttonNextBack = new JPanel(new GridLayout(0,2));

        buttonBack = new JButton(this.controller.getRessourceValue("PanelImageChoice_back"), Utils.resizeIcon(32, 32, this.controller.getRessourceIcon(backPng)));
        buttonNext = new JButton(this.controller.getRessourceValue("PanelImageChoice_next"),Utils.resizeIcon(32, 32, this.controller.getRessourceIcon(nextPng)));

        buttonBack.addActionListener(e-> {
            if ( !((RXTomoJComputationFrame)this.controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).navigateInnerPanel(-1)){
                buttonBack.setEnabled(false);
                buttonNext.setEnabled(true);
            }
        } );
        buttonNext.addActionListener(e-> {
            if ( !((RXTomoJComputationFrame)this.controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).navigateInnerPanel(1)){
                buttonNext.setEnabled(false);
                buttonBack.setEnabled(true);
            }
        });


        Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,0,0,this,this.image);
        Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,1,0,this,buttonBack);
        Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,2,0,this,buttonNext);
        this.setSize(this.getPreferredSize());
    }




    /**
     * Set the image choice by checking in the SessionObject
     * Warning SessionObject should be set
     */
    private void setImageChoice(){
        boolean firstObject = true;

        ArrayList<String> imageIDs =  RXTomoJ.getInstance().getModelController().getSessionController().getDataset(DataType.IMAGE);
        //Remove the Old disable no element object
        if ( imageIDs.size()  > 0 ){
            this.image.removeAll();
            this.buttonGroup = new ButtonGroup();
        }

        //Add every images
        for ( String datasetName : imageIDs ){
            JRadioButton radioButton  =  new JRadioButton(datasetName);
            this.buttonGroup.add(radioButton);
            this.image.add(radioButton);
            radioButton.addActionListener(e -> System.err.println("Not implemented : "+this.getClass().toGenericString()));
            if ( firstObject ){
                firstObject = false;
                radioButton.setSelected(true);
                //Open Image after Image creation
                //Set the current Selected Object in the current Analyses Object
                RXTomoJ.getInstance().getModelController().getDataController().setCurrentDataset(datasetName);
            }
        }

        //There was an Obejct
        if ( !firstObject ){
            if ( this.controller.isRawDataView() ){
                showImage();
            }
        }
    }


    public String getSelectedDataset(){
        while ( this.buttonGroup.getElements().hasMoreElements() ){
            JRadioButton radio  = (JRadioButton) this.buttonGroup.getElements().nextElement();
            if ( radio.isSelected() ){
                return radio.getText();
            }
        }
        return null;
    }

    private void showImage(){
        hdf5VirtualStack =  RXTomoJ.getInstance().getModelController().getIoController().openDefaultRawImage();


        ReductionInfo reductionInfo = RXTomoJ.getInstance().getModelController().getDataController().getReductionInfo();
        //Test if the Roi Spot has already Been defined
        //If already defined set the spot image
        if ( reductionInfo.getTransmissionROI() == null || (reductionInfo.getTransmissionROI()[0][0] == 0 &&
                reductionInfo.getTransmissionROI()[0][1] == 0) ){
            //Else compute the roi spot with a default low sampling to be relatively accurate and fast
            Utils.openWaitingLoadBar(null,"");
            RXTomoJ.getInstance().getModelController().getComputationController().spotMapInitialization();
        }else{
            this.controller.setRawSpotImage(hdf5VirtualStack);
        }
    }


    public void setButtonDisable(boolean back, boolean next){
        this.buttonBack.setEnabled(back);
        this.buttonNext.setEnabled(next);
    }

    @Override
    public void update(){
        this.setImageChoice();
    }

//    @Override
//    protected void finalize() throws Throwable {
//        this.hdf5VirtualStack.destroy();
//        super.finalize();
//    }
}
