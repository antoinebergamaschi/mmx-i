/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput;

import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStack;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.CustomWindowListener;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerPanel.*;
import com.soleil.nanoscopium.rxtomoj.gui.utils.DialogMessage;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Updatable;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;

/**
 * Created by antoine bergamaschi on 24/01/14.
 */
public class RXTomoJComputationFrame extends JFrame implements Updatable {

    private RXTomoJViewController controller;

    private RXTomoJComputationFrame self;

    private PanelImageChoice panelImageChoice;

    private ArrayList<InnerPanel> innerPanels;
    private int currentPosition = 0;

    private boolean lockInnerPanel = false;

    public RXTomoJComputationFrame(RXTomoJViewController controller){
        super();
        self = this;
        this.controller  = controller;
        this.build();
    }


    private void build(){

        this.setTitle(this.controller.getRessourceValue("RXTomoJComputationFrame_title"));
        this.setResizable(true);

        this.addWindowListener(new CustomWindowListener(this));

        this.createInterface();
        //Set the MenuBar
        this.setMenu();

        this.setVisible(true);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize(new Dimension(1300,800));
        this.setMinimumSize(new Dimension(1300,800));
        Dimension windowSize = this.getSize();

        int windowX = Math.max(0, (screenSize.width  - windowSize.width ) / 2);
        int windowY = Math.max(0, (screenSize.height - windowSize.height) / 2);

        setLocation(windowX, windowY);


        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                self.updateDrawingSize();
            }
        });

    }


    private void createInterface(){
        JPanel jPanel = new JPanel(new GridBagLayout());
        innerPanels = new ArrayList<>(5);

        //Add in the rigth order every InnerPanel
        innerPanels.add(RXTomoJViewController.INNER_SPOT,new RawDataPanel(this.controller));
        innerPanels.add(RXTomoJViewController.INNER_PREPROCESSING,new PreProcessingPanel(this.controller));
        innerPanels.add(RXTomoJViewController.INNER_MODALITIES,new ModalitiesSelectionPanel(this.controller));
        innerPanels.add(RXTomoJViewController.INNER_POSTPROCESSING,new PostProcessingPanel(this.controller));
        innerPanels.add(RXTomoJViewController.INNER_RECONSTRUCTION,new ReconstructionPanel(this.controller));
        currentPosition = RXTomoJViewController.INNER_SPOT;

        for ( InnerPanel pane : innerPanels ){
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, jPanel, pane);
        }

        this.add(jPanel,BorderLayout.CENTER);
        innerPanels.get(currentPosition).display();
        this.pack();
    }


    /**
     * Construct the menu bar associated with this JFrame
     */
     private void setMenu(){

         JMenuBar menuBar  = new JMenuBar();
         menuBar.setLayout(new GridBagLayout());

         JMenu menu  =  new MenuLoadSession(this.controller.getRessourceValue("title_Menu_principal"),this.controller);
         this.panelImageChoice = new PanelImageChoice(this.controller);

         Utils.addGridBag(GridBagConstraints.LINE_START,0,5,0,0,GridBagConstraints.NONE,0,1,0,0,1,1,0,0,menuBar,menu);
         int i = 1;
         for ( JMenu menus : innerPanels.get(currentPosition).createMenu() ){
             Utils.addGridBag(GridBagConstraints.LINE_START,0,2,0,0,GridBagConstraints.NONE,0,1,0,0,1,1,i,0,menuBar,menus);
             i++;
         }

         JMenu helpMenu = new MenuHelp(this.controller.getRessourceValue("help_Menu_principal"),this.controller);
         Utils.addGridBag(GridBagConstraints.LINE_START,0,5,0,0,GridBagConstraints.NONE,0,1,0,0,1,1,i,0,menuBar,helpMenu);
         i++;

         Utils.addGridBag(GridBagConstraints.LINE_START,0,0,0,0,GridBagConstraints.HORIZONTAL,1,1,0,0,1,1,i,0,menuBar,new JPanel());
         Utils.addGridBag(GridBagConstraints.LINE_END,0,0,0,5,GridBagConstraints.NONE,0,1,0,0,1,1,i+1,0,menuBar,this.panelImageChoice);


         this.setJMenuBar(menuBar);
    }

    /**
     * Update the Frame to load a Specific Object
     */
    @Deprecated
    public void updateFromSessionObject(){
        this.update();

        //Set default Motors if not known
//        if( SessionObject.getCurrentSessionObject().getMotorsID().size() > 0){
//            CustomAnalyse customAnalyse = SessionObject.getCurrentSessionObject().getAnalyseFactory().getCurrent();
//            customAnalyse.setMotorPositionX(SessionObject.getCurrentSessionObject().getMotorsID().get(0));
//            customAnalyse.setMotorPositionY(SessionObject.getCurrentSessionObject().getMotorsID().get(1));
//        }
    }

    public void updateRoi(){
        ((RawDataPanel)innerPanels.get(currentPosition)).updateRoi();
    }

    public  void setRawSpotImage(Hdf5VirtualStack image){
        ((RawDataPanel)innerPanels.get(currentPosition)).setRawSpotImage(image);
    }


    public Object[] getJPlotHdf5(){
        return  ((RawDataPanel)innerPanels.get(currentPosition)).getJPlotHdf5();
    }

    public Object[] getJPlotHdf5(int position){
        return  ((RawDataPanel)innerPanels.get(position)).getJPlotHdf5();
    }

    public void setMeanSpectrum(){
        ((RawDataPanel)innerPanels.get(currentPosition)).setMeanSpectrum();
    }

    public void removeMeanSpectrum(){
        ((RawDataPanel)innerPanels.get(currentPosition)).removeMeanSpectrum();
    }

    public void logSpectrum(boolean bol){
        ((RawDataPanel)innerPanels.get(currentPosition)).logSpectrum(bol);
    }

    public void addSpectrum(String datasetID,Color color){
        ((RawDataPanel)innerPanels.get(currentPosition)).addSpectrum(datasetID, color);
    }

    public void removeSpectrum(String datasetID){
        ((RawDataPanel)innerPanels.get(currentPosition)).removeSpectrum(datasetID);
    }


    public void updateFluorescenceElements(){
        ((RawDataPanel)innerPanels.get(currentPosition)).updateFluorescenceElements();
    }

    public void update(){
        //PanelImageChoice update the spot image
        this.panelImageChoice.update();
        innerPanels.get(currentPosition).update();
    }


    public void reset(){
        innerPanels.get(currentPosition).reset();
    }

    /**
     * Fire when component is resized
     */
    public void updateDrawingSize(){
        innerPanels.get(currentPosition).updateGraphics();
    }

    public void setInnerPanel(int position){
        if ( position < this.innerPanels.size() && position >= 0 ) {
            //Hide the old innerPanel
            this.innerPanels.get(currentPosition).display();
            //Show the position
            this.innerPanels.get(position).display();

            this.currentPosition = position;
        }
    }

    /**
     * Navigate throught inner Panels
     * @param move The move in the array of innerPanel
     * @return True if the move has been done
     */
    public boolean navigateInnerPanel(int move){

        boolean bool = true;
        if ( !lockInnerPanel ) {

            //Hide the old innerPanel
            this.innerPanels.get(currentPosition).display();

            //Move to the new position
            this.currentPosition += move;

            if (this.currentPosition < 0) {
                this.currentPosition = 0;
                bool = false;
            } else if (this.currentPosition > this.innerPanels.size() - 1) {
                this.currentPosition = this.innerPanels.size() - 1;
                bool = false;
            }

            // Show the new innerPanel
            this.innerPanels.get(currentPosition).display();

            this.innerPanels.get(currentPosition).update();
            this.setMenu();
        }else{
            //Show error message
            DialogMessage.showMessage_ID("rxTomoJComputationFrame_warning_process","","PROCESS_WAIT",DialogMessage.WARNING);
        }
        return bool;
    }

//    /**
//     * get the currently selected Combobox String object
//     * @return int the command currently selected
//     */
//    public int getCommand(){
//        return  ((RawDataPanel)innerPanels.get(currentPosition)).getCommand();
//    }

    public void setDisplayPosition(int positionDisplay){
        ((RawDataPanel)innerPanels.get(currentPosition)).setDisplayPosition(positionDisplay);
    }

    public void setLockInnerPanel(boolean lockInnerPanel) {
        this.lockInnerPanel = lockInnerPanel;
    }
}