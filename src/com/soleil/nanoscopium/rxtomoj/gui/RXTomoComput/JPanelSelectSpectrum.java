/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput;

import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.DataController;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.controller.SessionController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.BasicPanel;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.SessionObject.DataType;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

//import com.soleil.nanoscopium.rxtomoj.model.SessionObject;

/**
 * Created by bergamaschi on 12/02/14.
 */
public class JPanelSelectSpectrum extends JPanel implements ActionListener{
//    public static final int SPECTRUM=0,SAIA=1;
    private JPanel pane;
//    private int type;
    private int position = 0;

    private JPanel topPanel;
    private JPanel rightPanel;

    private JCheckBox meanCheckBox;
    private JCheckBox logCheckBox;
    private RXTomoJViewController controller;
//    private ArrayList<Color> usedColors;

    private HashMap<String,Color> usedColors = new HashMap<>();
    private String waiticon;
    private JLabel waitingImg;
    private JSeparator jSeparator = new JSeparator(JSeparator.VERTICAL);

    public JPanelSelectSpectrum(RXTomoJViewController controller){
        super(new GridBagLayout());
        this.controller = controller;
        waiticon = this.controller.getRessourceValue("wait-icon");
        this.build();


        TitledBorder titlePaneCheck = BorderFactory.createTitledBorder(controller.getRessourceValue("JPanelSelectSpectrum_title"));
        Border raisedbevel = BorderFactory.createRaisedBevelBorder();
        Border re = BorderFactory.createCompoundBorder(raisedbevel,titlePaneCheck);
        this.setBorder(re);

        this.setMinimumSize(this.getSize());
        this.setPreferredSize(this.getSize());

    }

    private void build(){
        meanCheckBox = new JCheckBox(controller.getRessourceValue("JPanelSelectSpectrum_mean"));
        meanCheckBox.setToolTipText(controller.getRessourceValue("JPanelSelectSpectrum_mean_tooltipText"));
        meanCheckBox.addActionListener(this);

        logCheckBox = new JCheckBox(controller.getRessourceValue("JPanelSelectSpectrum_log"));
        logCheckBox.setToolTipText(controller.getRessourceValue("JPanelSelectSpectrum_log_tooltipText"));
        logCheckBox.addActionListener(this);

        this.topPanel = new JPanel(new GridBagLayout());
        this.rightPanel = new JPanel(new GridBagLayout());
        this.pane = new JPanel(new GridBagLayout());
        waitingImg = new JLabel(this.controller.getRessourceIcon(waiticon));

        Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,0,0,this.topPanel,meanCheckBox);
        Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,1,0,this.topPanel,logCheckBox);
//        this.topPanel.add(meanCheckBox);
//        this.topPanel.add(logCheckBox);

        Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,3,1,0,0,this,this.topPanel);
        //anchors,top,left,bottom,right,fill,wX,wY,padx,pady,x,y
//        Utils.addGridBag(GridBagConstraints.LINE_START,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,2,1,0,0,this,meanCheckBox);

        Utils.addGridBag(GridBagConstraints.CENTER, BasicPanel.margin_top_inside,BasicPanel.margin_left_inside,BasicPanel.margin_bottom_inside,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,1,this,this.pane);
        Utils.addGridBag(GridBagConstraints.CENTER,0,BasicPanel.margin_left_inside,0,BasicPanel.margin_right_inside,GridBagConstraints.BOTH,0,1,0,0,1,1,1,1,this,jSeparator);
        Utils.addGridBag(GridBagConstraints.CENTER,BasicPanel.margin_top_inside,0,BasicPanel.margin_bottom_inside,BasicPanel.margin_right_inside,GridBagConstraints.BOTH,1,1,0,0,1,1,2,1,this,this.rightPanel);

        Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 3, 1, 0, 2, this, waitingImg);
        waitingImg.setVisible(false);
        jSeparator.setVisible(false);
    }

    public void removeTopPanel(){
        this.topPanel.removeAll();
    }

    public void removeRightPanel(){
        this.rightPanel.removeAll();
    }

    public void addTopPanel(Component component){
        this.topPanel.add(component);
    }

    public void addRightPanel(Component component){
        Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, this.rightPanel, (Container) component);
        jSeparator.setVisible(true);
    }

    public Color getDataSetColor(String name){
        return usedColors.get(name);
    }

    public void add(String dataName){
       add(dataName, false);
    }

    public void add(String dataName, boolean setSelected){
        PaneChoice paneChoice = new PaneChoice(dataName);
        Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, position, this.pane, paneChoice);
//        this.pane.add(paneChoice);
        position++;
        paneChoice.setSelected(setSelected);
    }

    public void setWaitingGif(){
        for ( Component component : this.getComponents()  ){
            component.setVisible(false);
        }
        waitingImg.setVisible(true);
    }

    public void removeWaitingGif(){

        for ( Component component : this.getComponents()  ){
            component.setVisible(true);
        }
        waitingImg.setVisible(false);
        if ( this.rightPanel.getComponentCount() > 0 ){
            this.jSeparator.setVisible(true);
        }
    }


    public void update(){
        //Reset Pane View
        this.pane.removeAll();
        position = 0;
        setPane();
    }

    public void setPane(){
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
        SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
        for ( String name : sessionController.getDataset(DataType.SPECTRUM)){
            this.add(name,dataController.getSelectedSpectrum().contains(name));
        }
    }


    public boolean isMean(){
        return  this.meanCheckBox.isSelected();
    }

    /**
     * Fire when the mean view is selected
     */
    public void setMeanSpectrum(){
        ((RXTomoJComputationFrame)controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).setMeanSpectrum();
    }

    /**
     * Fire when the mean view is unselected
     */
    public void removeMeanSpectrum(){
        ((RXTomoJComputationFrame)controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).removeMeanSpectrum();
    }

    public void setLogSpectrum(){
        ((RXTomoJComputationFrame)controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).logSpectrum(true);
    }

    public void removeLogSpectrum(){
        ((RXTomoJComputationFrame)controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).logSpectrum(false);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if ( e.getSource().equals(meanCheckBox)){
            if ( meanCheckBox.isSelected() ){
                //Fire Show only Mean
                setMeanSpectrum();
            }else{
                //Remove show only Mean
                removeMeanSpectrum();
            }
        }
        else if ( e.getSource().equals(logCheckBox) ){
            //Fire Log absice
            if ( logCheckBox.isSelected() ){
                //Fire Show only Mean
                setLogSpectrum();
            }else{
                //Remove show only Mean
                removeLogSpectrum();
            }
        }
    }

    /**
     * Fire by the PaneChoice innerClass when a data set is added
     * @param dataName the data set identifier to add
     * @param color the color which will serve to display this data set
     */
    public void addSpectrum(String dataName, Color color){
        ((RXTomoJComputationFrame)controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).addSpectrum(dataName,color);
        //Add the Spc name in the Custom analysis session object
//        RXTomoJ.getInstance().getModelController().getDataController().manageSpectrum(dataName);
    }

    /**
     * Fire by the PaneChoice when remove a dataset
     * @param dataName String, the Dataset ID in the Hdf5 file
     */
    public void removeSpectrum(String dataName){
        ((RXTomoJComputationFrame)controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).removeSpectrum(dataName);
        //remove the Spc name in the Custom analysis session object
//        RXTomoJ.getInstance().getModelController().getDataController().manageSpectrum(dataName);
    }

    public void manageSpectrum(String dataName){
        RXTomoJ.getInstance().getModelController().getDataController().manageSpectrum(dataName);
    }

    /**
     * Inner Class representing a Data set choice and the possibility to select one or more
     */
    private class PaneChoice extends JPanel{

        private Color color;
        private JCheckBox checkBox;
        private String dataName="";
        private JLabel additionalInformation = new JLabel();

        public PaneChoice(String dataName){
            super(new GridBagLayout());
            this.dataName = dataName;

            JLabel jLabel = new JLabel(Utils.extractLastNameComponent(dataName));
            this.checkBox = new JCheckBox();
            JPanel colorPane = new JPanel();
            colorPane.setPreferredSize(new Dimension(10,10));
//            colorPane.setMinimumSize(new Dimension(10,10));

            this.color = new Color(Utils.random.nextInt());

            if ( usedColors.containsKey(dataName) ){
                this.color = usedColors.get(dataName);
            }else{
                usedColors.put(dataName,this.color);
            }

            colorPane.setBackground(this.color);

            Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,1,1,0,0,1,1,0,0,this,jLabel);
            Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,1,0,this,checkBox);
            Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,2,0,this,colorPane);
            Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,3,0,this,additionalInformation);

            checkBox.addActionListener(e -> {
                manageSpectrum(this.dataName);

                if ( this.checkBox.isSelected() ) {
                    addSpectrum(this.dataName, this.color);
                }else{
                    removeSpectrum(this.dataName);
                }
            } );
        }

        public void setAdditionalInformation(String information){
            this.additionalInformation.setText(information);
        }

        public void setSelected(boolean sel){
            System.err.println("set selected only works with true :: "+this.getClass().toString());
            this.checkBox.setSelected(sel);
            if ( sel ){
//                this.checkBox.doClick();
                addSpectrum(dataName,color);
            }
        }
    }
}
