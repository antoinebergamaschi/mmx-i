/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput;

import com.soleil.nanoscopium.rxtomoj.gui.utils.BasicPanel;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

/**
 * @author Antoine Bergamaschi
 */
public class ModalitiesBar extends BasicPanel{

    private ArrayList<ModalityBarPanel> panels = new ArrayList<>();

    public ModalitiesBar(){
        super();
    }

    public ModalitiesBar(ArrayList<StackType> stackTypes){
        super(new GridLayout());
        this.build(stackTypes);
    }


    public void setModalities(HashMap<StackType,String> modalities){
        this.setModalities(modalities.keySet());
    }

    public void setModalities(StackType[] modalities){
        this.setModalities(Arrays.asList(modalities));
    }

    public void setModalities(Collection<StackType> modalities){
        this.removeAll();
        this.build(modalities);
    }

    private void build(Collection<StackType> modalities){
        int i = 0;
        for (StackType type : modalities){
            ModalityBarPanel modalityBarPanel = new ModalityBarPanel(type);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, i, 0, this, modalityBarPanel);
            i++;
            panels.add(modalityBarPanel);
        }

    }

    public void setSelected(Collection<StackType> modalities){
        for ( ModalityBarPanel modalityPanel : panels ){
            modalityPanel.setSelected(modalities.contains(modalityPanel.getModality()));
        }
    }

    public ArrayList<StackType> getSelected(){
        ArrayList<StackType> sel = new ArrayList<>();
        for ( ModalityBarPanel modalityPanel : panels ){
            if ( modalityPanel.isSelected() ){
                sel.add(modalityPanel.getModality());
            }
        }
        return sel;
    }

    private class ModalityBarPanel extends JPanel{
        private StackType stackType;
        private boolean isSelected = false;

        public ModalityBarPanel(StackType type){
            super(new GridBagLayout());
            stackType = type;
            build();
        }

        private void build(){
            Utils.addGridBag(GridBagConstraints.CENTER, BasicPanel.margin_inside, BasicPanel.margin_inside, BasicPanel.margin_inside, BasicPanel.margin_inside, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, this, new JLabel(stackType.getSaveName()));
        }

        public boolean isSelected(){
            return isSelected;
        }

        public StackType getModality(){
            return this.stackType;
        }

        public void setSelected(boolean bol){
            this.isSelected = bol;
        }
    }
}
