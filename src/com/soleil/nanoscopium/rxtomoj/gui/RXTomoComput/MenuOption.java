/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput;

import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow.*;
import com.soleil.nanoscopium.rxtomoj.gui.utils.DialogMessage;
import com.soleil.nanoscopium.rxtomoj.model.SessionObject.DataType;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by bergamaschi on 05/02/14.
 */
public class MenuOption extends JMenu implements ActionListener {
    private final RXTomoJViewController controller;

    public MenuOption(String title, RXTomoJViewController controller){
        super(title);
        this.controller = controller;
        JMenuItem menu1  =  new JMenuItem(this.controller.getRessourceValue("preProcessingPanel_title_Menu_option_spotDetection"));
        menu1.setActionCommand("1");
        JMenuItem menu2 = new JMenuItem(this.controller.getRessourceValue("preProcessingPanel_title_Menu_option_motors"));
        menu2.setActionCommand("2");
        JMenuItem menu3 = new JMenuItem(this.controller.getRessourceValue("preProcessingPanel_title_Menu_option_interface"));
        menu3.setActionCommand("3");
        JMenuItem menu4 = new JMenuItem(this.controller.getRessourceValue("preProcessingPanel_title_Menu_option_sai"));
        menu4.setActionCommand("4");
        JMenuItem menu5 = new JMenuItem(this.controller.getRessourceValue("preProcessingPanel_title_Menu_option_fluo"));
        menu5.setActionCommand("5");
        this.add(menu1);
        this.add(menu2);
        this.add(menu3);
        this.add(menu4);
        this.add(menu5);
        menu1.addActionListener(this);
        menu2.addActionListener(this);
        menu3.addActionListener(this);
        menu4.addActionListener(this);
        menu5.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if ( e.getActionCommand().compareToIgnoreCase("1") == 0){
            //Open the automatic detection Frame ( may be use to accuratly find the spot )
            if ( !this.controller.getInnerFrame(JFrameSpotMapOption.class.toString()) ){
                new JFrameSpotMapOption(this.controller);
            }
        }
        else if ( e.getActionCommand().compareToIgnoreCase("2") == 0){
            if ( RXTomoJ.getInstance().getModelController().getSessionController().getDataset(DataType.MOTOR).size() == 0 ){
                DialogMessage.showMessage(controller.getRessourceValue("title_Menu_option_motors_error_openMotors_message"),controller.getRessourceValue("title_Menu_option_motors_error_openMotors_title"),DialogMessage.ERROR);

            }else{
                //Open the motors option Frame
                if ( !this.controller.getInnerFrame(JFrameMotorsChoice.class.toString()) ){
                    new JFrameMotorsChoice(this.controller);
                }
            }
        }
        else if ( e.getActionCommand().compareToIgnoreCase("3") == 0 ){
            if ( !this.controller.getInnerFrame(JFrameInterfaceOption.class.toString()) ){
                new JFrameInterfaceOption(this.controller);
            }
        }
        else if ( e.getActionCommand().compareToIgnoreCase("4") == 0 ){
            if ( !this.controller.getInnerFrame(JFrameSaiChoice.class.toString()) ){
                new JFrameSaiChoice(this.controller);
            }
        }
        else if ( e.getActionCommand().compareToIgnoreCase("5") == 0 ){
            if ( !this.controller.getInnerFrame(JFrameFluorescence.class.toString()) ){
                Object[] datas = ((RXTomoJComputationFrame)this.controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).getJPlotHdf5();
                if ( datas != null &&  datas[0] != null ){
                    new JFrameFluorescence(this.controller);
                }else{
                    DialogMessage.showMessage(controller.getRessourceValue("title_Menu_option_fluorescence_error_openMotors_message"),controller.getRessourceValue("title_Menu_option_fluorescence_error_openMotors_title"),DialogMessage.ERROR);
                }
            }
        }
    }

}
