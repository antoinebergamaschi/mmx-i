/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow;

import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.EditableNumericField;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.data.ReductionInfo;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.text.ParseException;

/**
 * Created by bergamaschi on 17/06/2014.
 */
public class JFrameSamplingOption extends JFrameInner {
    private EditableNumericField numberOfLineReader;
    private JCheckBox lowResolutionMode;
    private JSpinner sampling;

    public JFrameSamplingOption(RXTomoJViewController controller) {
        super(controller);
    }



    @Override
    protected JPanel buildContentPane() {
        JPanel pane = new JPanel(new GridBagLayout());

        JPanel scrollOption = new JPanel(new GridBagLayout());
        TitledBorder titledBorder = BorderFactory.createTitledBorder(controller.getRessourceValue("JFrameSamplingOption_title"));
        scrollOption.setBorder(titledBorder);

//        int[] sampling = RXTomoJ.getInstance().getModelController().getDataController().getSamplingOption();
//        JSpinner general_sampling = new JSpinner(new SpinnerNumberModel(sampling[1],1,6,1));
        lowResolutionMode = new JCheckBox();
        sampling = new JSpinner(new SpinnerNumberModel(1,1,100,1));
        numberOfLineReader = Utils.createNumberTextField(0);

        JPanel line0 = Utils.createOptionLine(lowResolutionMode, "JFrameSamplingOption_spinner_label_general", "JFrameSamplingOption_spinner_label_general_tooltip");
        JPanel line1 = Utils.createOptionLine(sampling, "JFrameSamplingOption_spinner_label_reference", "JFrameSamplingOption_spinner_label_reference_tooltip");
        JPanel line2 = Utils.createOptionLine(numberOfLineReader, "JFrameSamplingOption_spinner_label_numberOfLine", "JFrameSamplingOption_spinner_label_numberOfLine_tooltip");

        JButton validate_button = Utils.createValidateButton();

        validate_button.addActionListener(l->{
//            RXTomoJ.getInstance().getModelController().getDataController().setSamplingOption((Integer)sampling.getValue(),(Integer)general_sampling.getValue());
            RXTomoJ.getInstance().getModelController().getDataController().setSamplingOption((Integer)sampling.getValue());
            RXTomoJ.getInstance().getModelController().getDataController().useLowResolutionMode(lowResolutionMode.isSelected());
            try {
                numberOfLineReader.commitEdit();
                RXTomoJ.getInstance().getModelController().getDataController().setNumberOfLineRead(Integer.parseInt(numberOfLineReader.getValue().toString()));
//                System.out.println(general_sampling.getValue()+" "+reference_sampling.getValue()+" "+numericField.getValue());
                dispose();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });



        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, scrollOption, line0);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 1, scrollOption, line1);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 2, scrollOption, line2);

        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 0, 0, 0, 0, 3, 1, 0, 3, scrollOption, validate_button);

        //anchors,top,left,bottom,right,fill,wX,wY,padx,pady,x,y
        Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.BOTH,1,0,0,0,1,1,0,0,pane,scrollOption);

        return pane;
    }

    @Override
    public boolean setUpInModel() {
        this.key = this.getClass().toString();
        if ( !this.controller.getInnerFrame(key) ) {
            this.controller.addInnerFrame(this.key, this);
            return true;
        }
        return false;
    }

    @Override
    public void update() {
        ReductionInfo reductionInfo = RXTomoJ.getInstance().getModelController().getDataController().getReductionInfo();

        numberOfLineReader.setText(reductionInfo.getNumberOfLine()+"");
        sampling.setValue(reductionInfo.getPixelSpacing());
        lowResolutionMode.setSelected(reductionInfo.isUseLowResolution());

    }
}
