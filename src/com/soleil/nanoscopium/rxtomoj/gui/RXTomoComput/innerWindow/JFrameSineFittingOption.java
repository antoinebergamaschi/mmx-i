/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow;

import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.JPanelImage;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.data.NormalizationInfo;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.text.NumberFormat;
import java.text.ParseException;

/**
 * @author Antoine Bergamaschi
 */
public class JFrameSineFittingOption extends JFrameInner {
    private JCheckBox useMeanMobile;
    private JCheckBox usePrecomputedAngular;
    private JFormattedTextField angularRange;
    private JFormattedTextField window;

    public JFrameSineFittingOption(RXTomoJViewController controller) {
        super(controller);
//        this.pack();
        this.setSize((int) (getWidth()*1.3),getHeight());
    }

    @Override
    protected JPanel buildContentPane() {
        JPanel pane = new JPanel(new GridBagLayout());

        JPanel scrollOption = new JPanel(new GridBagLayout());
        TitledBorder titledBorder = BorderFactory.createTitledBorder(controller.getRessourceValue("JFrameSineFittingOption_title"));
        scrollOption.setBorder(titledBorder);

        JPanel meanOption = constructMeanOptionPanel();
        JPanel angularOption = constructAngularPanel();

//        ImageIcon help_gen = Utils.resizeIcon(32, 32, this.controller.getRessourceIcon(this.controller.getRessourceValue("help-icon")));
//        ImageIcon help_ref = Utils.resizeIcon(32, 32, this.controller.getRessourceIcon(this.controller.getRessourceValue("help-icon")));
//
//        useMeanMobile = new JCheckBox(this.controller.getRessourceValue("JFrameMobileMeanOption_checkbox_useMean"));
//        NumberFormat format = NumberFormat.getNumberInstance();
//        format.setMaximumFractionDigits(0);
//        window = new JFormattedTextField(format);
//
//
//        JLabel help_genL = new JLabel(help_gen);
//        JLabel help_refL = new JLabel(help_ref);
//
//        help_genL.setToolTipText(controller.getRessourceValue("JFrameMobileMeanOption_useMean_tooltip"));
//        help_refL.setToolTipText(controller.getRessourceValue("JFrameMobileMeanOption_window_tooltip"));
//
//
//
//        JLabel ref = new JLabel(controller.getRessourceValue("JFrameMobileMeanOption_label_window"));
//
//        ref.setHorizontalAlignment(JLabel.LEFT);
//
//        useMeanMobile.addActionListener(l->{
//            RXTomoJ.getInstance().getModelController().getDataController().useWobblingWindowParameter(useMeanMobile.isSelected());
//        });
//
//        window.addActionListener(l->{
//            try {
//                RXTomoJ.getInstance().getModelController().getDataController().setWobblingWindowParameter(format.parse(window.getText()).intValue());
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//        });
//
//        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 2, 1, 0, 0, scrollOption, useMeanMobile);
//        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 0, 1, 0, 0, 1, 1, 2, 0, scrollOption, help_genL);
//        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 1, scrollOption, ref);
//        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 1, scrollOption, window);
//        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 0, 1, 0, 0, 1, 1, 2, 1, scrollOption, help_refL);

        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, scrollOption, meanOption);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 1, scrollOption, angularOption);

        //anchors,top,left,bottom,right,fill,wX,wY,padx,pady,x,y
        Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.BOTH,1,0,0,0,1,1,0,0,pane,scrollOption);

        return pane;
    }

    private JPanel constructMeanOptionPanel(){

        JPanel panel = new JPanel(new GridBagLayout());
        TitledBorder titledBorder = BorderFactory.createTitledBorder(controller.getRessourceValue("JFrameMobileMeanOption_mean_subtitle"));
        panel.setBorder(titledBorder);

        ImageIcon help_gen = Utils.resizeIcon(32, 32, this.controller.getRessourceIcon(this.controller.getRessourceValue("help-icon")));
        ImageIcon help_ref = Utils.resizeIcon(32, 32, this.controller.getRessourceIcon(this.controller.getRessourceValue("help-icon")));

        useMeanMobile = new JCheckBox(this.controller.getRessourceValue("JFrameMobileMeanOption_checkbox_useMean"));
        NumberFormat format = NumberFormat.getNumberInstance();
        format.setMaximumFractionDigits(0);
        window = new JFormattedTextField(format);


        JLabel help_genL = new JLabel(help_gen);
        JLabel help_refL = new JLabel(help_ref);

        help_genL.setToolTipText(controller.getRessourceValue("JFrameMobileMeanOption_useMean_tooltip"));
        help_refL.setToolTipText(controller.getRessourceValue("JFrameMobileMeanOption_window_tooltip"));



        JLabel ref = new JLabel(controller.getRessourceValue("JFrameMobileMeanOption_label_window"));

        ref.setHorizontalAlignment(JLabel.LEFT);

        useMeanMobile.addActionListener(l->{
            RXTomoJ.getInstance().getModelController().getDataController().useWobblingWindowParameter(useMeanMobile.isSelected());
        });

        window.addActionListener(l->{
            try {
                RXTomoJ.getInstance().getModelController().getDataController().setWobblingWindowParameter(format.parse(window.getText()).intValue());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });

        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 2, 1, 0, 0, panel, useMeanMobile);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 0, 1, 0, 0, 1, 1, 2, 0, panel, help_genL);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 1, panel, ref);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 1, panel, window);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 0, 1, 0, 0, 1, 1, 2, 1, panel, help_refL);

        return panel;
    }

    private JPanel constructAngularPanel(){
        JPanel panel = new JPanel(new GridBagLayout());
        TitledBorder titledBorder = BorderFactory.createTitledBorder(controller.getRessourceValue("JFrameMobileMeanOption_angular_subtitle"));
        panel.setBorder(titledBorder);

        ImageIcon help_gen = Utils.resizeIcon(32, 32, this.controller.getRessourceIcon(this.controller.getRessourceValue("help-icon")));
        ImageIcon help_ref = Utils.resizeIcon(32, 32, this.controller.getRessourceIcon(this.controller.getRessourceValue("help-icon")));

        usePrecomputedAngular = new JCheckBox(this.controller.getRessourceValue("JFrameMobileMeanOption_checkbox_usePrecomputedAngular"));
        NumberFormat format = NumberFormat.getNumberInstance();
        format.setMaximumIntegerDigits(3);
        format.setMaximumFractionDigits(2);
        format.setMinimumFractionDigits(2);
        angularRange = new JFormattedTextField(format);


        JLabel help_genL = new JLabel(help_gen);
        JLabel help_refL = new JLabel(help_ref);

        help_genL.setToolTipText(controller.getRessourceValue("JFrameMobileMeanOption_usePrecomputedAngular_tooltip"));
        help_refL.setToolTipText(controller.getRessourceValue("JFrameMobileMeanOption_angularRange_tooltip"));



        JLabel ref = new JLabel(controller.getRessourceValue("JFrameMobileMeanOption_label_angularRange"));

        ref.setHorizontalAlignment(JLabel.LEFT);

        usePrecomputedAngular.addActionListener(l->{
            RXTomoJ.getInstance().getModelController().getDataController().setUsePrecomputedAngularStep(usePrecomputedAngular.isSelected());
        });

        angularRange.addActionListener(l->{
            try {
                RXTomoJ.getInstance().getModelController().getDataController().setAngularRange(Math.toRadians(format.parse(angularRange.getText()).doubleValue()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });

        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 2, 1, 0, 0, panel, usePrecomputedAngular);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 0, 1, 0, 0, 1, 1, 2, 0, panel, help_genL);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 1, panel, ref);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 1, panel, angularRange);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 0, 1, 0, 0, 1, 1, 2, 1, panel, help_refL);

        return panel;
    }

    @Override
    public void update() {
        NormalizationInfo normalizationInfo = RXTomoJ.getInstance().getModelController().getDataController().getNormalizationInfo();
        useMeanMobile.setSelected(normalizationInfo.isUseMobileMean());
        window.setValue(normalizationInfo.getMobileWindow());

        usePrecomputedAngular.setSelected(normalizationInfo.isUsePrecomputedAngularStep());
        angularRange.setValue(Math.toDegrees(normalizationInfo.getAngularRange()));
    }
}
