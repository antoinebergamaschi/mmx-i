/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow;


import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualSpectrum;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.DataController;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.RXTomoJComputationFrame;
import com.soleil.nanoscopium.rxtomoj.gui.utils.DialogMessage;
import com.soleil.nanoscopium.rxtomoj.gui.utils.JPanelPlot;
import com.soleil.nanoscopium.rxtomoj.gui.utils.JPlotListener;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.fluorescence.FluorescenceElement;
import com.soleil.nanoscopium.rxtomoj.model.fluorescence.FluorescenceInfo;
import com.soleil.nanoscopium.rxtomoj.model.fluorescence.FluorescenceObject;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

/**
 * Created by bergamaschi on 19/03/14.
 */
//TODO add Draw Shape to see peaks positions
//TODO add visual design to signify to the users that errors occurs
public class JFrameFluorescence extends JFrameInner implements JPlotListener {
    private JPanelPlot jPanelPlot;
    private ElementsPane elementsPane;
    private CalibrationPane calibrationPane;
    private JButton calibrationJButton,elementEditionJButton;
//    private FluorescenceInfo fluorescenceInfo;

    private boolean lockUpdate = false;

    private int fluoFrameWidth = 500;
    private int fluoFrameHeight = 400;
    final private int iconSize = 20;
    private Icon validate_icon;
    private Icon remove_icon;
    private Icon modify_icon;

    public JFrameFluorescence(RXTomoJViewController controller) {
        super(controller);

//        fluorescenceInfo = RXTomoJ.getInstance().getModel().getFluorescenceInfo();

        setMinimumSize(new Dimension(fluoFrameWidth,fluoFrameHeight));
        //Create a MenuBar  that will be use to select Calibration ( By default if not set ) or Elements Selection
        setMenu();

        this.setResizable(true);
        this.lockUpdate = true;
        this.calibrationPane.update();
        this.elementsPane.update();
        this.lockUpdate = false;
    }

    /**
     * Construct the menu bar associated with this JFrame
     */
    private void setMenu(){
        //Menu Construction
        JMenuBar menuBar  = new JMenuBar();
        menuBar.setLayout(new GridBagLayout());


        calibrationJButton = new JButton(this.controller.getRessourceValue("JFrameFluorescence_calibrationButton_menu"));
        calibrationJButton.setToolTipText(this.controller.getRessourceValue("JFrameFluorescence_calibrationButton_menu_tooltip"));

        elementEditionJButton = new JButton(this.controller.getRessourceValue("JFrameFluorescence_addElementButton_menu"));
        elementEditionJButton.setToolTipText(this.controller.getRessourceValue("JFrameFluorescence_addElementButton_menu_tooltip"));


        calibrationJButton.addActionListener(e -> {
            if ( calibrationJButton.isEnabled() ){
                jPanelPlot.setAllowPeak(true);
                jPanelPlot.setAllowRoi(false);

                calibrationJButton.setEnabled(false);
                calibrationPane.setVisible(true);

                elementsPane.setVisible(false);
                elementEditionJButton.setEnabled(true);
            }
        });

        elementEditionJButton.addActionListener(e -> {
            if ( elementEditionJButton.isEnabled() ){
                jPanelPlot.setAllowPeak(false);
                jPanelPlot.setAllowRoi(true);

                elementEditionJButton.setEnabled(false);
                elementsPane.setVisible(true);


                calibrationPane.setVisible(false);
                calibrationJButton.setEnabled(true);
            }
        });

        if ( RXTomoJ.getInstance().getModelController().getDataController().isFluorescenceCalibrated() ){
            elementEditionJButton.doClick();
        }else{
            calibrationJButton.doClick();
        }

        Utils.addGridBag(GridBagConstraints.CENTER,5,20,5,20,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,menuBar,calibrationJButton);
        Utils.addGridBag(GridBagConstraints.CENTER,5,20,5,20,GridBagConstraints.BOTH,1,1,0,0,1,1,1,0,menuBar,elementEditionJButton);

        this.setJMenuBar(menuBar);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected JPanel buildContentPane() {
        JPanel pane = new JPanel(new GridBagLayout());

        validate_icon = Utils.resizeIcon(iconSize,iconSize,controller.getRessourceIcon(controller.getRessourceValue("validate-icon")));
        remove_icon = Utils.resizeIcon(iconSize,iconSize,controller.getRessourceIcon(controller.getRessourceValue("remove-icon")));
        modify_icon = Utils.resizeIcon(iconSize,iconSize,controller.getRessourceIcon(controller.getRessourceValue("modify-icon")));

        jPanelPlot = new JPanelPlot();
        elementsPane = new ElementsPane();
        calibrationPane = new CalibrationPane();

        //Retrieve from the controller the currently displayed Spectrum and displays Them


        JPanel paneCheck = new JPanel(new GridLayout(2,1));
        //Small Jpanel to edit view / See mean / log10
        JCheckBox logCheckBox = new JCheckBox("log");

        logCheckBox.addActionListener(e -> {
            jPanelPlot.setLogYaxis(((JCheckBox) e.getSource()).isSelected());
            jPanelPlot.repaint();
        });

        Object[] datas = ((RXTomoJComputationFrame)this.controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).getJPlotHdf5();

        jPanelPlot.synchroniseData((ArrayList<Hdf5VirtualSpectrum>) datas[0], (ArrayList<Color>) datas[1]);
        jPanelPlot.setMeanSpectrum(true);

        jPanelPlot.addListener(this);

        paneCheck.add(logCheckBox);

        //anchors,top,left,bottom,right,fill,wX,wY,padx,pady,x,y
        Utils.addGridBag(GridBagConstraints.FIRST_LINE_START,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,0,0,pane,paneCheck);

        Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,1,0,pane,jPanelPlot);

        //anchors,top,left,bottom,right,fill,wX,wY,padx,pady,x,y
        Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,1,pane, elementsPane);
        //anchors,top,left,bottom,right,fill,wX,wY,padx,pady,x,y
        Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,1,pane, calibrationPane);

        return pane;
    }

    @Override
    public boolean setUpInModel() {
        this.key = this.getClass().toString();
        if ( !this.controller.getInnerFrame(key) ) {
            this.controller.addInnerFrame(this.key, this);
            return true;
        }
        return false;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    @Override
    public void fireMousePressed(float positionX) {

    }

    @Override
    public void fireMouseReleased(float positionX) {
        if ( !elementEditionJButton.isEnabled() ){
        }else if  ( !calibrationJButton.isEnabled() ){
            this.calibrationPane.setValue(positionX);
        }
    }

    @Override
    public void fireRoiSet(float begin, float end) {
        if ( !elementEditionJButton.isEnabled() ){
            elementsPane.setRoiValues(begin,end);
        }
    }

    @Override
    public void update() {
        System.err.println("Empty update :"+this.getClass().toString());
    }

    public void retrieveFocus(){
        this.requestFocus();
    }

    /**
     * Panel used to display the Calibration interface
     */
    private class CalibrationPane extends JPanel{
        private PeakPane firstPeak;
        private PeakPane secondPeak;



        public CalibrationPane(){
            super(new GridBagLayout());
            build();
        }

        private void build(){
            JLabel jLabel  =  new JLabel("Select two peak on the Plot");

            firstPeak  = new PeakPane(0);
            secondPeak  =  new PeakPane(1);

            Utils.addGridBag(GridBagConstraints.CENTER,5,0,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,0,0,this,jLabel);
            Utils.addGridBag(GridBagConstraints.CENTER,2,5,2,5,GridBagConstraints.BOTH,1,0,0,0,1,1,0,1,this,firstPeak);
            Utils.addGridBag(GridBagConstraints.CENTER,2,5,5,5,GridBagConstraints.BOTH,1,0,0,0,1,1,0,2,this,secondPeak);
        }


        public void update(){
            firstPeak.update();
            secondPeak.update();
        }

        public void setValue(float value){
            if ( firstPeak.isInModificationMode() ) {
                firstPeak.setPeakValue(value);
            }
            else if ( secondPeak.isInModificationMode() ){
                secondPeak.setPeakValue(value);
            }
        }

        private class PeakPane extends JPanel implements MouseListener{
            //TODO set FormattedText to Float
            private JFormattedTextField positionOntheGraph;
            private JFormattedTextField valueInKev;

            private JButton validate;
            private boolean isInModificationMode  = false;

            int position;

            public PeakPane(final int position) {
                super(new GridBagLayout());
                this.position = position;
                build();
                this.addMouseListener(this);
                for ( Component component : this.getComponents() ){
                    component.addMouseListener(this);
                }
                this.setBorder(BorderFactory.createLineBorder(Color.white));
            }

            private void build(){

//                validate_ = new JButton(controller.getRessourceValue("JFrameFluorescence_calibrationPane_Peak_jButton_2"));
                validate = new JButton();
                validate.setIcon(modify_icon);
                validate.setToolTipText(controller.getRessourceValue("JFrameFluorescence_calibrationPane_Peak_jButton_tooltip"));

                validate.addActionListener(e -> {
                    if (position == 0) {
                        secondPeak.lockModification();
                    } else if (position == 1) {
                        firstPeak.lockModification();
                    }

                    isInModificationMode = !isInModificationMode;
                    //Set Editable the field
                    positionOntheGraph.setEditable(isInModificationMode);
                    valueInKev.setEditable(isInModificationMode);
                    //Change Jbutton title
                    if (isInModificationMode) {
//                        validate_.setText(controller.getRessourceValue("JFrameFluorescence_calibrationPane_Peak_jButton"));
                        validate.setIcon(validate_icon);
                    } else {
//                        validate_.setText(controller.getRessourceValue("JFrameFluorescence_calibrationPane_Peak_jButton_2"));
                        validate.setIcon(modify_icon);
                    }

                });

                positionOntheGraph = new JFormattedTextField();
                valueInKev = Utils.createKevNumberFormatted();

                positionOntheGraph.addPropertyChangeListener("value", evt -> {
                    if (!lockUpdate) {
                        //Warning TODO none safe args passing
                        RXTomoJ.getInstance().getModelController().getDataController().setCalibrationPeakChannel((Float) evt.getNewValue(), position);
                    }
                });

                valueInKev.addPropertyChangeListener("value", evt -> {
                    if (!lockUpdate) {
                        //Warning TODO none safe args passing
                        RXTomoJ.getInstance().getModelController().getDataController().setCalibrationPeakEnergy((Float) evt.getNewValue(), position);
                    }
                });


                positionOntheGraph.setEditable(isInModificationMode);
                valueInKev.setEditable(isInModificationMode);


                Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.NONE,0,0,0,0,1,1,0,0,this,validate);
                Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,1,0,this,positionOntheGraph);
                Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,2,0,this,valueInKev);

                positionOntheGraph.setPreferredSize(positionOntheGraph.getSize());
                valueInKev.setPreferredSize(valueInKev.getSize());
            }

            /**
             * Update the currently displayed Values
             */
            public void update(){

                DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
                FluorescenceObject fluorescenceObject = dataController.getFluorescenceInfo().getFluoData().get(0);
                if ( fluorescenceObject != null ) {
//                FluorescenceInfo fluorescenceInfo = RXTomoJ.getInstance().getModelController().getDataController().getFluorescenceInfo();
                    positionOntheGraph.setValue(fluorescenceObject.getCalibrationPeak_channel()[position]);
                    valueInKev.setValue(fluorescenceObject.getCalibrationPeak_energy()[position]);
                }
            }

            public boolean isInModificationMode(){
                return isInModificationMode;
            }

            public void setPeakValue(float value){
                if ( this.positionOntheGraph.isEditable() ) {
                    this.positionOntheGraph.setValue(value);
                }
            }


            public void lockModification(){
                if ( isInModificationMode ){
                    isInModificationMode = !isInModificationMode;
                    //Set Editable the field
                    positionOntheGraph.setEditable(isInModificationMode);
                    valueInKev.setEditable(isInModificationMode);
                    //Change Jbutton title
                    if (isInModificationMode) {
//                        validate_.setText(controller.getRessourceValue("JFrameFluorescence_calibrationPane_Peak_jButton"));
                        validate.setIcon(validate_icon);
                    } else {
//                        validate_.setText(controller.getRessourceValue("JFrameFluorescence_calibrationPane_Peak_jButton_2"));
                        validate.setIcon(modify_icon);
                    }
                }
            }

            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                this.setBorder(BorderFactory.createLineBorder(new Color(0.4f,0.8f,0.5f,0.7f)));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                this.setBorder(BorderFactory.createLineBorder(Color.white));
            }
        }

    }

    /**
     * ElementsPane regroup the function to edit and update the Fluorescence Elements to display / add / remove
     */
    private class ElementsPane extends JPanel{
        private ArrayList<ElementLine> elements = new ArrayList<>();
        private int current = 0;
        private JPanel elementsPanel;

        public ElementsPane(){
            super(new GridBagLayout());
            build();
        }

        private void build(){
            elementsPanel = new JPanel(new GridBagLayout());
            //Set Scrolling property for this panel has its can be relatively large
            JScrollPane jScrollPane = new JScrollPane();
            jScrollPane.setViewportView(elementsPanel);

            this.setPreferredSize(new Dimension(300, 200));

            JButton addButton = new JButton(controller.getRessourceValue("JFrameFluorescence_elementPane_addElement"));
            addButton.setToolTipText(controller.getRessourceValue("JFrameFluorescence_elementPane_addElement_tooltip"));

            addButton.addActionListener(e -> addElement() );
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 1, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, this, jScrollPane);
            Utils.addGridBag(GridBagConstraints.CENTER, 1, 5, 5, 5,GridBagConstraints.BOTH,1,0,0,0,1,1,0,1,this,addButton);

            //Add every Elements Based on the saved Fluorescence Elements
        }

        public void setRoiValues(float begin, float end){
            for ( ElementLine element : this.elements ) {
                if ( element.isInModification() ) {
                    element.setRoiValues(begin, end);
                }
            }
        }

        public void addElement(){
            //Add the fluorescence element in the model
            FluorescenceElement fluorescenceElement = new FluorescenceElement();
            //If add fail
            if ( !RXTomoJ.getInstance().getModelController().getDataController().manageFluorescenceElement(fluorescenceElement) ){
                return;
            }

            ElementLine panel  =  new ElementLine(fluorescenceElement);
            panel.setPosition(elements.size());
            elements.add(panel);
            Utils.addGridBag(GridBagConstraints.FIRST_LINE_START, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, elements.size() - 1, elementsPanel, panel);

            elementsPanel.getParent().revalidate();
            elementsPanel.repaint();
        }


        public void removeElement(int index){
            ElementLine removed = this.elements.remove(index);
            RXTomoJ.getInstance().getModelController().getDataController().removeFluorescenceElement(removed.getElementName());
            int i = 0;
            for ( ElementLine element : this.elements ) {
                element.setPosition(i);
                i++;
            }
            elementsPanel.remove(removed);
            elementsPanel.repaint();
        }

        public void update(){
            this.elementsPanel.removeAll();
            for ( FluorescenceElement fluorescenceElement : RXTomoJ.getInstance().getModelController().getDataController().getFluorescenceInfo().getElements() ){
                ElementLine panel  =  new ElementLine(fluorescenceElement);
                panel.setPosition(elements.size());
                elements.add(panel);
                Utils.addGridBag(GridBagConstraints.FIRST_LINE_START, 1, 0, 1, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, elements.size() - 1, elementsPanel, panel);
            }
        }


        public void lockEveryElement(){
            for ( ElementLine elementLine : this.elements ){
                elementLine.lockModification();
            }
        }

        public boolean isInEditionMode(){
            for ( ElementLine elementLine : elements){
                if ( elementLine.isInModification() ){
                    return true;
                }
            }
            return false;
        }

        private class ElementLine extends JPanel implements MouseListener{
            private JFormattedTextField emissionValbegin;
            private JFormattedTextField emissionValend;
            private JFormattedTextField elementsName;
            private FluorescenceElement fluorescenceElement;
            private JButton validate;
            private JButton remove;
            private Border mouseOverBorder =  BorderFactory.createLineBorder(new Color(0.4f,0.8f,0.5f,0.7f));
            private Border restBorder = BorderFactory.createLineBorder(Color.white);
            private Color mouseOverColor;
            private Color restColor;

            private boolean isInModification = false;
            private int position;

            public ElementLine(FluorescenceElement fluorescenceElement){
                super(new GridBagLayout());
                this.fluorescenceElement = fluorescenceElement;
                build();
                this.addMouseListener(this);
                for ( Component component : this.getComponents() ){
                    component.addMouseListener(this);
                }
                this.setBorder(BorderFactory.createLineBorder(Color.white));
            }

            public float[] getRoi(){
                return new float[]{(float) emissionValbegin.getValue(), (float) emissionValend.getValue()};
            }

            private void build(){
//                validate_ = new JButton(controller.getRessourceValue("JFrameFluorescence_elementPane_validate_2"));
                validate = new JButton();
                validate.setIcon(modify_icon);
                validate.setToolTipText(controller.getRessourceValue("JFrameFluorescence_elementPane_modify_tooltip"));

                validate.addActionListener(e -> {
                        FluorescenceInfo fluorescenceInfo = RXTomoJ.getInstance().getModelController().getDataController().getFluorescenceInfo();
                        //Test if the modification is error free
                        if (isInModification) {
                            if (emissionValbegin.getValue() == null || emissionValend.getValue() == null ||
                                    (Float) emissionValbegin.getValue() > (Float) emissionValend.getValue() ||
                                    fluorescenceInfo.isUniqueName((String) elementsName.getValue()) != 0) {
                                //Display Error Message
                                DialogMessage.showMessage(controller.getRessourceValue("JFrameFluorescence_elementPane_error_message"),controller.getRessourceValue("JFrameFluorescence_elementPane_error_title"),DialogMessage.ERROR);
                                return;
                            }
                        } else {
                            lockEveryElement();
                        }

                        isInModification = !isInModification;
                        //Enable validation
                        if (isInModification) {
                            validate.setIcon(validate_icon);
                            validate.setToolTipText(controller.getRessourceValue("JFrameFluorescence_elementPane_validate_tooltip"));
                        }
                        //Fire validation
                        else {
                            validate.setIcon(modify_icon);
                            validate.setToolTipText(controller.getRessourceValue("JFrameFluorescence_elementPane_modify_tooltip"));

                            //Update the RXComputationFrame
                            ((RXTomoJComputationFrame)controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).updateFluorescenceElements();
                        }
                        emissionValend.setEditable(isInModification);
                        emissionValbegin.setEditable(isInModification);
                        elementsName.setEditable(isInModification);

                        emissionValend.setFocusable(isInModification);
                        emissionValbegin.setFocusable(isInModification);
                        elementsName.setFocusable(isInModification);
                    }
                );

                remove = new JButton();
                remove.setIcon(remove_icon);
                remove.setToolTipText(controller.getRessourceValue("JFrameFluorescence_elementPane_remove_tooltip"));

                remove.addActionListener(e -> {
                    removeElement(this.position);
                    //Update the RXComputationFrame
                    ((RXTomoJComputationFrame)controller.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS)).updateFluorescenceElements();
                });


                emissionValbegin = Utils.createKevNumberFormatted();
                emissionValbegin.setHorizontalAlignment(JFormattedTextField.RIGHT);
                emissionValbegin.setToolTipText(controller.getRessourceValue("JFrameFluorescence_elementPane_emissionValbegin_tooltip"));

                emissionValend = Utils.createKevNumberFormatted();
                emissionValend.setHorizontalAlignment(JFormattedTextField.RIGHT);
                emissionValend.setToolTipText(controller.getRessourceValue("JFrameFluorescence_elementPane_emissionValend_tooltip"));

                elementsName = new JFormattedTextField();
                elementsName.setHorizontalAlignment(JFormattedTextField.CENTER);
                elementsName.setToolTipText(controller.getRessourceValue("JFrameFluorescence_elementPane_elementsName_tooltip"));

                float[] roi = this.fluorescenceElement.getRoiPosition();
                emissionValbegin.setValue(RXTomoJ.getInstance().getModelController().getDataController().getFluorescenceInfo().getFluoData().get(0).convertValue(roi[0]));
                emissionValend.setValue(RXTomoJ.getInstance().getModelController().getDataController().getFluorescenceInfo().getFluoData().get(0).convertValue(roi[1]));

                elementsName.setValue(fluorescenceElement.getName());

                //First set init the Formater
//                elementsName.setValue("null");

                emissionValbegin.addActionListener(e -> {
                    FluorescenceObject fluorescenceInfo = RXTomoJ.getInstance().getModelController().getDataController().getFluorescenceInfo().getFluoData().get(0);
                    float[] position = this.fluorescenceElement.getRoiPosition();
                    position[0] = fluorescenceInfo.inverseConvertValue((Float) emissionValbegin.getValue());
                });

                emissionValend.addActionListener(e -> {
                    FluorescenceObject fluorescenceInfo = RXTomoJ.getInstance().getModelController().getDataController().getFluorescenceInfo().getFluoData().get(0);
                    float[] position = this.fluorescenceElement.getRoiPosition();
                    position[1] = fluorescenceInfo.inverseConvertValue((Float) emissionValend.getValue());
                });

                elementsName.addPropertyChangeListener("value", evt -> this.fluorescenceElement.setName((String) evt.getNewValue()) );

                emissionValend.setEditable(isInModification);
                emissionValbegin.setEditable(isInModification);
                elementsName.setEditable(isInModification);

                Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,2,GridBagConstraints.BOTH,0,1,0,0,1,1,0,0,this,validate);
                Utils.addGridBag(GridBagConstraints.CENTER,5,2,5,5,GridBagConstraints.BOTH,0,1,0,0,1,1,1,0,this,remove);
                Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,2,0,this,emissionValbegin);
                Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,3,0,this,emissionValend);
                Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,4,0,this,elementsName);

                emissionValbegin.setPreferredSize(emissionValbegin.getSize());
                emissionValend.setPreferredSize(emissionValend.getSize());
                elementsName.setPreferredSize(elementsName.getSize());

            }

            public void lockModification(){
                if ( this.isInModification ){
                    this.validate.doClick();
                }
            }

            public String getElementName(){
                return (String) elementsName.getValue();
            }

            public void setRoiValues(float begin, float end){
                FluorescenceObject fluorescenceInfo = RXTomoJ.getInstance().getModelController().getDataController().getFluorescenceInfo().getFluoData().get(0);
                emissionValbegin.setValue(fluorescenceInfo.convertValue(begin));
                emissionValend.setValue(fluorescenceInfo.convertValue(end));

                float[] position = this.fluorescenceElement.getRoiPosition();
                position[0] = begin;
                position[1] = end;
            }

            public boolean isInModification(){
                return isInModification;
            }

            public int getPosition(){
                return this.position;
            }

            public void setPosition(int position){
                this.position = position;
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                //Set the current Object has selected, show the energy selection in the graph
//                jPanelPlot.setAllowRoi();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                if ( !isInEditionMode() ) {
                    this.setBorder(mouseOverBorder);
                    jPanelPlot.addRoi(fluorescenceElement.getRoiPosition()[0], fluorescenceElement.getRoiPosition()[1]);
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                if ( !isInEditionMode() ) {
                    this.setBorder(restBorder);
                    jPanelPlot.clearShape();
                }
            }
        }
    }
}
