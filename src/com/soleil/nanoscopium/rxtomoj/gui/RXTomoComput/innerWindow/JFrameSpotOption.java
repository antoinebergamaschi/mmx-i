/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow;


import com.soleil.nanoscopium.hdf5Opener.Hdf5ImagePlus;
import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStackFactory;
import com.soleil.nanoscopium.hdf5Opener.ImageWindowHdf5;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.LoadingBar;
import com.soleil.nanoscopium.rxtomoj.gui.utils.DialogMessage;
import com.soleil.nanoscopium.rxtomoj.gui.utils.EditableNumericField;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.computationFunction.ComputeSpot;
import com.soleil.nanoscopium.rxtomoj.model.data.ReductionInfo;
import com.soleil.nanoscopium.rxtomoj.model.filters.HotSpot_Detection;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;
import com.soleil.nanoscopium.rxtomoj.model.utils.ProgressFunction;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.text.NumberFormat;
import java.text.ParseException;

/**
 * Created by bergamaschi on 25/02/14.
 */
@Deprecated
public class JFrameSpotOption extends JFrameInner {

    private JLabel minimalPixelNumber,minimalPixelValue;
    private JCheckBox eliminatedPixel;
    private EditableNumericField pixelValue;
    private EditableNumericField pixelNumb;
    private ImageWindowHdf5 imageWindowHdf5 = null;
    private ComputeSpot computeSpot;
    private JButton jButton;
    private JPanel panelControl;
//    private JPanelFilterSpot jPanelFilterSpot;
    private LoadingBar  l;

    public JFrameSpotOption(RXTomoJViewController controller){
        super(controller);
    }


    protected JPanel buildContentPane(){
        JPanel contentPane = new JPanel(new GridBagLayout());

        JPanel firstPane = new JPanel(new GridBagLayout());
//        JPanel secondPane = new JPanel(new GridBagLayout());

        TitledBorder firstPaneTitle = BorderFactory.createTitledBorder(controller.getRessourceValue("JFrameSpotOption_title_firstPane"));
        firstPane.setBorder(firstPaneTitle);

//        TitledBorder secondPaneTitle = BorderFactory.createTitledBorder(controller.getRessourceValue("JFrameSpotOption_title_ROI_Selection"));
//        secondPane.setBorder(secondPaneTitle);

        //Create the Second Panel
        panelControl = new JPanel(new GridBagLayout());

        minimalPixelNumber = new JLabel(controller.getRessourceValue("rawDataPanel_label_minimalPixelnumb"));
        minimalPixelValue = new JLabel(controller.getRessourceValue("rawDataPanel_label_minimalPixelValue"));

        minimalPixelValue.setToolTipText(controller.getRessourceValue("rawDataPanel_label_minimalPixelValue_tooltip"));
        minimalPixelNumber.setToolTipText(controller.getRessourceValue("rawDataPanel_label_minimalPixelnumb_tooltip"));


        eliminatedPixel = new JCheckBox(controller.getRessourceValue("rawDataPanel_label_eliminatedPixel"));
        eliminatedPixel.setToolTipText(controller.getRessourceValue("rawDataPanel_label_eliminatedPixel_tooltip"));

        NumberFormat pn = NumberFormat.getNumberInstance();
        pn.setParseIntegerOnly(true);

        NumberFormatter pnf = new NumberFormatter();
        pnf.setAllowsInvalid(false);
        pnf.setMaximum(10);
        pnf.setMinimum(1);

        NumberFormat pv = NumberFormat.getNumberInstance();
        pv.setMinimumFractionDigits(1);
        pv.setMinimumIntegerDigits(1);

        NumberFormatter pvf = new NumberFormatter(pv);
        pvf.setMinimum(0);
        pvf.setAllowsInvalid(false);

        pixelValue = new EditableNumericField(pvf);
        pixelNumb = new EditableNumericField(pnf);

        pixelValue.setToolTipText(controller.getRessourceValue("rawDataPanel_label_minimalPixelValue_tooltip"));
        pixelNumb.setToolTipText(controller.getRessourceValue("rawDataPanel_label_minimalPixelnumb_tooltip"));


        jButton = new JButton(this.controller.getRessourceValue("jButton_compute"));

        jButton.addActionListener(event -> {

        });

        //Add listener
//        eliminatedPixel.addActionListener(e -> computeSpot.setEliminatedPixelToZero(((JCheckBox)e.getSource()).isSelected()));

        //Add Listener
//        pixelNumb.addPropertyChangeListener("value", evt -> computeSpot.setMinimalPixelNumber(((Number)pixelNumb.getValue()).intValue()));


//        pixelValue.addPropertyChangeListener("value", evt -> computeSpot.setMinimalPixelValue(((Number)pixelValue.getValue()).floatValue()));

        //Set Up controlPanel
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 10, 5, 10, GridBagConstraints.BOTH, 1, 1, 0, 0, 2, 1, 0, 0, panelControl, eliminatedPixel);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 10, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 1, panelControl, minimalPixelNumber);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 10, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 1, panelControl, pixelNumb);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 10, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 2, panelControl, minimalPixelValue);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 10, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 2, panelControl, pixelValue);

        //Set Up this panel
        Utils.addGridBag(GridBagConstraints.CENTER, 10, 10, 5, 10, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, firstPane, panelControl);
        Utils.addGridBag(GridBagConstraints.CENTER, 0, 5, 0, 5, GridBagConstraints.BOTH, 1, 0, 0, 0, 1, 1, 0, 1, firstPane, new JSeparator(JSeparator.HORIZONTAL));
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 10, 10, 10, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 2, firstPane, jButton);


        //Set Up this panel
//        utils.addGridBag(GridBagConstraints.CENTER, 10, 10, 5, 10, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, secondPane, eliminatedPixel);

        //JButton use to validate_ the changes
//        JButton validate_ = new JButton(controller.getRessourceValue("validate_"));

//        validate_.addActionListener(l->{
//            try {
//                pixelNumb.commitEdit();
//                computeSpot.setMinimalPixelNumber(((Number) pixelNumb.getValue()).intValue());
//                pixelValue.commitEdit();
//                computeSpot.setMinimalPixelValue(((Number) pixelValue.getValue()).floatValue());
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//
////            jPanelFilterSpot.validate_();
//        });


//        jPanelFilterSpot = new JPanelFilterSpot();
        //Final Build
        Utils.addGridBag(GridBagConstraints.CENTER, 10, 10, 5, 10, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, contentPane, firstPane);
//        Utils.addGridBag(GridBagConstraints.CENTER, 5, 10, 10, 10, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 2, contentPane, jPanelFilterSpot);
//        Utils.addGridBag(GridBagConstraints.CENTER, 5, 10, 10, 10, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 1, contentPane, validate_);

        //Update the interface
        updateInterface();

        return contentPane;
    }

    /**
     * Update the component contained in this interface with the values retrieve from the model
     */
    public void updateInterface(){
        ReductionInfo reductionInfo = RXTomoJ.getInstance().getModelController().getDataController().getReductionInfo();

        pixelValue.setValue(reductionInfo.getMinimalPixelValue());
        pixelNumb.setValue(reductionInfo.getMinimalPixelNumber());
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

//    @Override
//    public boolean setUpInModel() {
//        this.key = this.getClass().toString();
//        if ( !this.controller.getInnerFrame(key) ) {
//            this.controller.addInnerFrame(this.key, this);
//            return true;
//        }
//        return false;
//    }

    @Override
    public void update() {
        System.err.println("Empty update :"+this.getClass().toString());
    }

    private void setWaitingLoadBar(ProgressFunction function){
        Utils.openWaitingLoadBar(function,this.controller.getRessourceValue("label_Title_rawData_wait"));
    }

    private class JPanelFilterSpot extends JPanel{

//        private JCheckBox keepZero;
        private JCheckBox useFilter;
        private EditableNumericField radius;
        private EditableNumericField maxLoop;
        private EditableNumericField spreadCoef;
        private JCheckBox doFilter;

        public JPanelFilterSpot(){
            super(new GridBagLayout());
            this.build();
            TitledBorder secondPaneTitle = BorderFactory.createTitledBorder(controller.getRessourceValue("JFrameSpotOption_title_filterOption"));
            setBorder(secondPaneTitle);
        }

        private void build(){

            useFilter = new JCheckBox(controller.getRessourceValue("JFrameSpotOption_JPanelFilterSpot_useFilter"));
            useFilter.setToolTipText(controller.getRessourceValue("JFrameSpotOption_JPanelFilterSpot_useFilter_tooltip"));
            useFilter.addActionListener(e->{
                maxLoop.setEnabled( useFilter.isSelected());
                radius.setEnabled( useFilter.isSelected());
                spreadCoef.setEnabled( useFilter.isSelected());
//                computeSpot.setDoHotSpotFilter(useFilter.isSelected());
            });

            JLabel radiusLabel = new JLabel(controller.getRessourceValue("JFrameSpotOption_JPanelFilterSpot_radius")+ " : ");
            radiusLabel.setToolTipText(controller.getRessourceValue("JFrameSpotOption_JPanelFilterSpot_radius_tooltip"));
            JLabel maxLoopLabel = new JLabel(controller.getRessourceValue("JFrameSpotOption_JPanelFilterSpot_iteration")+ " : ");
            maxLoopLabel.setToolTipText(controller.getRessourceValue("JFrameSpotOption_JPanelFilterSpot_iteration_tooltip"));
            JLabel spreadCoefLabel = new JLabel(controller.getRessourceValue("JFrameSpotOption_JPanelFilterSpot_spreadCoef")+ " : ");
            spreadCoefLabel.setToolTipText(controller.getRessourceValue("JFrameSpotOption_JPanelFilterSpot_spreadCoef_tooltip"));


            NumberFormatter pnf = new NumberFormatter();
            pnf.setAllowsInvalid(false);
            pnf.setMinimum(1);

            radius = new EditableNumericField(pnf);
            maxLoop = new EditableNumericField(pnf);
            spreadCoef = new EditableNumericField(pnf);


            Utils.addGridBag(GridBagConstraints.CENTER, 5, 10, 5, 10, GridBagConstraints.BOTH, 1, 1, 0, 0, 2, 1, 0, 0, this, useFilter);
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 10, 5, 5, GridBagConstraints.NONE, 0, 1, 0, 0, 1, 1, 0, 1, this, radiusLabel);
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 10, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 1, this, radius);
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 10, 5, 5, GridBagConstraints.NONE, 0, 1, 0, 0, 1, 1, 0, 2, this, maxLoopLabel);
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 10, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 2, this, maxLoop);
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 10, 5, 5, GridBagConstraints.NONE, 0, 1, 0, 0, 1, 1, 0, 3, this, spreadCoefLabel);
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 10, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 3, this, spreadCoef);

        }

        public void validate(){
//            try {
//                radius.commitEdit();
//                computeSpot.getHotSpot_detection().setVois((Integer) radius.getValue());
//                maxLoop.commitEdit();
//                computeSpot.getHotSpot_detection().setMaxLoop((Integer) maxLoop.getValue());
//                spreadCoef.commitEdit();
//                computeSpot.getHotSpot_detection().setSpreadCoef((Integer) spreadCoef.getValue());
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
        }


        public void updateInterface(){
//            HotSpot_Detection hotSpot_detection = computeSpot.getHotSpot_detection();
//            maxLoop.setValue(hotSpot_detection.getMaxLoop());
//            radius.setValue(hotSpot_detection.getVois());
//            spreadCoef.setValue(hotSpot_detection.getSpreadCoef());
//            useFilter.setSelected(computeSpot.getDoHotSpotFilter());
//
//            maxLoop.setEnabled(computeSpot.getDoHotSpotFilter());
//            radius.setEnabled(computeSpot.getDoHotSpotFilter());
//            spreadCoef.setEnabled(computeSpot.getDoHotSpotFilter());
        }
    }
}
