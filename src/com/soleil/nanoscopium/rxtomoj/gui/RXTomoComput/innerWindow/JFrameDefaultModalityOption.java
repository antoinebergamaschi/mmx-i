/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow;

import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.RXTomoJComputationFrame;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.data.StackData;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by bergamaschi on 15/09/2015.
 */
public class JFrameDefaultModalityOption extends JFrameInner {
    public JFrameDefaultModalityOption(RXTomoJViewController controller) {
        super(controller);
        this.pack();
    }

    @Override
    protected JPanel buildContentPane() {
        JPanel pane = new JPanel(new GridBagLayout());
        ArrayList<StackData> test = RXTomoJ.getInstance().getModelController().getDataController().getBasicData();

        ButtonGroup buttonGroup = new ButtonGroup();
        int i = 0;

        StackData sel = RXTomoJ.getInstance().getModelController().getDataController().getDefaultModality();

        for (final StackData stackData : test ){
            JRadioButton button = new JRadioButton(stackData.getSaveName());
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, i, pane, button);
            i++;

            if ( stackData.getSystemPath() != null && sel != null && sel.getSystemPath() != null &&
                    stackData.getSystemPath().equalsIgnoreCase(sel.getSystemPath()) ){
                button.setSelected(true);
            }

            button.addActionListener(l->{
                if ( button.isSelected() ) {
                    RXTomoJ.getInstance().getModelController().getDataController().setDefaultStackType(stackData);
                    ((RXTomoJComputationFrame) controller.getCurrentFrame()).reset();
                }
            });

            buttonGroup.add(button);
        }

        return pane;
    }

    @Override
    public void update() {
        System.err.println("Empty update :"+this.getClass().toString());
    }
}
