/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow;

import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.EditableNumericField;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.ShiftImage;
import com.soleil.nanoscopium.rxtomoj.model.data.NormalizationInfo;
import com.soleil.nanoscopium.rxtomoj.model.data.ShiftImageInfo;
import com.soleil.nanoscopium.rxtomoj.model.utils.FittingUtils;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.text.NumberFormat;
import java.text.ParseException;

/**
 * @author Antoine Bergamaschi
 */
public class JFrameMobileMeanOption extends JFrameInner {
    private JCheckBox useMeanMobile;
    private JFormattedTextField window;

    public JFrameMobileMeanOption(RXTomoJViewController controller) {
        super(controller);
//        this.setSize(new Dimension(300,200));
    }

    @Override
    protected JPanel buildContentPane() {
        JPanel pane = new JPanel(new GridBagLayout());

        JPanel scrollOption = new JPanel(new GridBagLayout());
        TitledBorder titledBorder = BorderFactory.createTitledBorder(controller.getRessourceValue("JFrameMobileMeanOption_title"));
        scrollOption.setBorder(titledBorder);

        ImageIcon help_gen = Utils.resizeIcon(32, 32, this.controller.getRessourceIcon(this.controller.getRessourceValue("help-icon")));
        ImageIcon help_ref = Utils.resizeIcon(32, 32, this.controller.getRessourceIcon(this.controller.getRessourceValue("help-icon")));
//        ImageIcon validate_icon = Utils.resizeIcon(32, 32, this.controller.getRessourceIcon(this.controller.getRessourceValue("validate-icon")));

        useMeanMobile = new JCheckBox(this.controller.getRessourceValue("JFrameMobileMeanOption_checkbox_useMean"));
//        window = Utils.createNumberTextField(-1,-1,1,2,100,null);
        NumberFormat format = NumberFormat.getNumberInstance();
        format.setMaximumFractionDigits(0);
        window = new JFormattedTextField(format);


        JLabel help_genL = new JLabel(help_gen);
        JLabel help_refL = new JLabel(help_ref);

        help_genL.setToolTipText(controller.getRessourceValue("JFrameMobileMeanOption_useMean_tooltip"));
        help_refL.setToolTipText(controller.getRessourceValue("JFrameMobileMeanOption_window_tooltip"));



        JLabel ref = new JLabel(controller.getRessourceValue("JFrameMobileMeanOption_label_window"));

        ref.setHorizontalAlignment(JLabel.LEFT);

        useMeanMobile.addActionListener(l->{
//                RXTomoJ.getInstance().getModelController().getDataController().updateFittingUtils(format.parse(window.getText()).intValue(),useMeanMobile.isSelected());
            RXTomoJ.getInstance().getModelController().getDataController().useWobblingWindowParameter(useMeanMobile.isSelected());
        });

        window.addActionListener(l->{
            try {
                RXTomoJ.getInstance().getModelController().getDataController().setWobblingWindowParameter(format.parse(window.getText()).intValue());
//                RXTomoJ.getInstance().getModelController().getDataController().updateFittingUtils(format.parse(window.getText()).intValue(),useMeanMobile.isSelected());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });

//        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, scrollOption, gen);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 2, 1, 0, 0, scrollOption, useMeanMobile);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 0, 1, 0, 0, 1, 1, 2, 0, scrollOption, help_genL);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 1, scrollOption, ref);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 1, scrollOption, window);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 0, 1, 0, 0, 1, 1, 2, 1, scrollOption, help_refL);
//        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 0, 0, 0, 0, 3, 1, 0, 3, scrollOption, validate_button);

        //anchors,top,left,bottom,right,fill,wX,wY,padx,pady,x,y
        Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.BOTH,1,0,0,0,1,1,0,0,pane,scrollOption);

        return pane;
    }


    @Override
    public void update() {
        NormalizationInfo normalizationInfo = RXTomoJ.getInstance().getModelController().getDataController().getNormalizationInfo();
//        ShiftImageInfo shiftImageInfo = RXTomoJ.getInstance().getModelController().getDataController().getShiftImageInfo();
        useMeanMobile.setSelected(normalizationInfo.isUseMobileMean());
        window.setValue(normalizationInfo.getMobileWindow());
    }
}
