/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow;

import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.data.ReconstructionInfo;
import com.soleil.nanoscopium.rxtomoj.model.reconstructionFunction.BasicReconstructionFunction;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.text.NumberFormat;
import java.text.ParseException;

/**
 * Created by bergamaschi on 09/10/2015.
 */
public class JFrameReconstructionBetaOption extends JFrameInner {

    private JSlider numberOfLine;
    private JSlider numberOfPixel;

    private JFormattedTextField centerX;
    private JComboBox<String> projection_backproj;


    public JFrameReconstructionBetaOption(RXTomoJViewController controller) {
        super(controller);
    }

    @Override
    protected JPanel buildContentPane() {
        JPanel pane = new JPanel(new GridBagLayout());
//        JPanel scrollOption = new JPanel();
        TitledBorder titledBorder = BorderFactory.createTitledBorder(controller.getRessourceValue("JFrameReconstructionBetaOption_title"));
        pane.setBorder(titledBorder);

        JLabel numberOfLineLabel = new JLabel(controller.getRessourceValue("JFrameReconstructionBetaOption_label_line"));

        JLabel numberOfPixelLabel = new JLabel(controller.getRessourceValue("JFrameReconstructionBetaOption_label_pixel"));

        JLabel centerXLabel = new JLabel(controller.getRessourceValue("JFrameReconstructionBetaOption_label_centerOfRotation"));

        JLabel valueLine = new JLabel("000");
        JLabel valuePixel = new JLabel("000");

        numberOfLine = new JSlider(JSlider.HORIZONTAL,1,100,1);
        numberOfPixel = new JSlider(JSlider.HORIZONTAL,1,100,1);

        NumberFormat format = NumberFormat.getNumberInstance();

        centerX = new JFormattedTextField(format);
        centerX.setValue(new Double(0));

        //TODO remove That
        projection_backproj = new JComboBox<>();

        for ( BasicReconstructionFunction.ProjectionMethod val : BasicReconstructionFunction.ProjectionMethod.values() ){
            projection_backproj.addItem(val.name());
        }

        numberOfLine.addChangeListener(l->{
            valueLine.setText(String.valueOf(numberOfLine.getValue()));
            RXTomoJ.getInstance().getModelController().getDataController().setNumberOfLineRead(numberOfLine.getValue());
        });

        centerX.addActionListener(l->{
            try {
                RXTomoJ.getInstance().getModelController().getDataController().setProjectionCenterOfRotation(format.parse(centerX.getText()).doubleValue());
//                RXTomoJ.getInstance().getModelController().getDataController().setBasicReconstructionInfo(format.parse(centerX.getText()).doubleValue(),numberOfLine.getValue(),numberOfPixel.getValue());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });


        projection_backproj.addActionListener(l->{
            for ( BasicReconstructionFunction.ProjectionMethod val : BasicReconstructionFunction.ProjectionMethod.values() ){
                if ( projection_backproj.getSelectedItem().toString().equalsIgnoreCase(val.name()) ){
                    RXTomoJ.getInstance().getModelController().getDataController().setProjectionMethode(val);
                }
            }
        });

        //anchors,top,left,bottom,right,fill,wX,wY,padx,pady,x,y
        Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.NONE,1,0,0,0,1,1,0,0,pane,numberOfLineLabel);
        Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.NONE,1,0,0,0,1,1,1,0,pane,numberOfLine);
        Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.NONE,1,0,0,0,1,1,2,0,pane,valueLine);

        Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.NONE,1,0,0,0,1,1,0,1,pane,numberOfPixelLabel);
        Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.BOTH,1,0,0,0,1,1,1,1,pane,numberOfPixel);
        Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.NONE,1,0,0,0,1,1,2,1,pane,valuePixel);

        Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.NONE,1,0,0,0,1,1,0,2,pane,centerXLabel);
        Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.BOTH,1,0,0,0,2,1,1,2,pane,centerX);
        Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.BOTH,1,0,0,0,3,1,0,3,pane,projection_backproj);


        return pane;
    }

    @Override
    public void update() {
        ReconstructionInfo reconstructionInfo = RXTomoJ.getInstance().getModelController().getDataController().getReconstructionInfo();
        centerX.setValue(reconstructionInfo.getCenterOfRotationX());
//        numberOfPixel.setValue(reconstructionInfo.getNumberOfStepByPixel());
        numberOfLine.setValue(reconstructionInfo.getNumberOfLine());
        projection_backproj.setSelectedItem(reconstructionInfo.getProjectionMethod().name());
    }
}
