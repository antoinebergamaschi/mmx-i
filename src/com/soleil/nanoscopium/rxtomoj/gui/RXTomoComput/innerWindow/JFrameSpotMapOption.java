/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow;

import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.computationFunction.ComputeSpot;
import com.soleil.nanoscopium.rxtomoj.model.data.ReductionInfo;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

/**
 * Created by bergamaschi on 17/06/2014.
 */
public class JFrameSpotMapOption extends JFrameInner {

    public JFrameSpotMapOption(RXTomoJViewController controller) {
        super(controller);
        this.setSize(new Dimension(400,300));
    }

    @Override
    protected JPanel buildContentPane() {
        ReductionInfo reductionInfo = RXTomoJ.getInstance().getModelController().getDataController().getReductionInfo();

        JPanel pane = new JPanel(new GridBagLayout());

        JButton compute = new JButton(controller.getRessourceValue("JFrameSpotMapOption_button_compute"));

        JPanel scrollOption = new JPanel(new GridBagLayout());
        TitledBorder titledBorder = BorderFactory.createTitledBorder(controller.getRessourceValue("JFrameSpotMapOption_title"));
        scrollOption.setBorder(titledBorder);

        ImageIcon help_gen = Utils.resizeIcon(32, 32, this.controller.getRessourceIcon(this.controller.getRessourceValue("help-icon")));
        ImageIcon help_ref = Utils.resizeIcon(32, 32, this.controller.getRessourceIcon(this.controller.getRessourceValue("help-icon")));

        JLabel help_genL = new JLabel(help_gen);
        JLabel help_refL = new JLabel(help_ref);

        help_genL.setToolTipText(controller.getRessourceValue("JFrameSpotMapOption_spinner_label_general_tooltip"));
        help_refL.setToolTipText(controller.getRessourceValue("JFrameSpotMapOption_spinner_label_reference_tooltip"));

        JSpinner percentOfMax = new JSpinner(new SpinnerNumberModel(reductionInfo.getSpotFilter(),0,1,0.1));
        JSpinner threshold = new JSpinner(new SpinnerNumberModel(reductionInfo.getMinimalPixelValue(),0,999,1));


        JLabel gen = new JLabel(controller.getRessourceValue("JFrameSpotMapOption_spinner_label_general"));
        JLabel ref = new JLabel(controller.getRessourceValue("JFrameSpotMapOption_spinner_label_reference"));

        gen.setHorizontalAlignment(JLabel.LEFT);
        ref.setHorizontalAlignment(JLabel.LEFT);

        gen.setLabelFor(percentOfMax);
        ref.setLabelFor(threshold);

        //Add listeners
        percentOfMax.addChangeListener(l-> RXTomoJ.getInstance().getModelController().getDataController().setSpotFilter(new Float((double) ((JSpinner) l.getSource()).getValue())));
        threshold.addChangeListener(l-> RXTomoJ.getInstance().getModelController().getDataController().setSpotMinimumValue(new Float((double) ((JSpinner) l.getSource()).getValue())));

        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, scrollOption, gen);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 0, scrollOption, percentOfMax);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 0, 0, 0, 0, 1, 1, 2, 0, scrollOption, help_genL);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 1, scrollOption, ref);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 1, scrollOption, threshold);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 0, 0, 0, 0, 1, 1, 2, 1, scrollOption, help_refL);


        //anchors,top,left,bottom,right,fill,wX,wY,padx,pady,x,y
        Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.BOTH,1,0,0,0,1,1,0,0,pane,scrollOption);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 0, 0, 0, 0, 1, 1, 0, 1, pane, compute);

        compute.addActionListener(l->{
            Utils.openWaitingLoadBar();
            RXTomoJ.getInstance().getModelController().getComputationController().spotMapInitialization();
            JFrameSpotMapOption.this.dispose();
        });

        return pane;
    }

    @Override
    public void update() {
        System.err.println("Empty update :"+this.getClass().toString());
    }
}
