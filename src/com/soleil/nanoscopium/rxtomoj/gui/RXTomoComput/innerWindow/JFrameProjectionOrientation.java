/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow;

import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;

import javax.swing.*;
import java.awt.*;

/**
 * @author Antoine Bergamaschi
 */
public class JFrameProjectionOrientation extends JFrameInner {

    public JFrameProjectionOrientation(RXTomoJViewController controller) {
        super(controller);
        this.setSize(new Dimension(400,300));
    }

    @Override
    protected JPanel buildContentPane() {
        JPanel pane = new JPanel(new GridBagLayout());
        ButtonGroup buttonGroup = new ButtonGroup();

        JLabel explanation = new JLabel(this.controller.getRessourceValue("JFrameProjectionOrientation_label_explanation"));

        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, pane, explanation);

        int i = 1;
        for ( ComputationUtils.OrientationType type : ComputationUtils.OrientationType.values()){
            JCheckBox checkBox = new JCheckBox(type.getDescription());
            buttonGroup.add(checkBox);
            if ( RXTomoJ.getInstance().getModelController().getDataController().isCurrentOrientation(type) ){
                //Set Checked
                checkBox.setSelected(true);
            }
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, i, pane, checkBox);
            i++;

            checkBox.addActionListener( l -> RXTomoJ.getInstance().getModelController().getDataController().setCurrentOrientation(type));
        }

        return pane;
    }

//    @Override
//    public boolean setUpInModel() {
//        this.key = this.getClass().toString();
//        if ( !this.controller.getInnerFrame(key) ) {
//            this.controller.addInnerFrame(this.key, this);
//            return true;
//        }
//        return false;
//    }

    @Override
    public void update() {
        System.err.println("Empty update :"+this.getClass().toString());
    }
}
