/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow;

import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

/**
 * Created by bergamaschi on 24/02/14.
 */
public class JFrameInterfaceOption extends JFrameInner {
//    ArrayList<JComboBox> jComboBoxes = new ArrayList<JComboBox>();
//    ArrayList<JLabel> labels = new ArrayList<JLabel>();

//    private final String nullString = "void";
    private final String cmdRD = "rd",cmdCP = "cp";

    public JFrameInterfaceOption(RXTomoJViewController controller){
        super(controller);
    }



    protected JPanel buildContentPane(){
        JPanel pane = new JPanel(new GridBagLayout());

        JPanel scrollOption = new JPanel(new GridBagLayout());
        TitledBorder titledBorder = BorderFactory.createTitledBorder(controller.getRessourceValue("JFrameInterfaceOption_subtitle"));
        scrollOption.setBorder(titledBorder);

        ButtonGroup buttonGroup = new ButtonGroup();
        JCheckBox rawPosition = new JCheckBox(controller.getRessourceValue("JFrameInterfaceOption_check_rawData"));

        JCheckBox cartesianPosition = new JCheckBox(controller.getRessourceValue("JFrameInterfaceOption_check_cartesian"));

//        rawPosition.setActionCommand(cmdRD);
//        cartesianPosition.setActionCommand(cmdCP);

        rawPosition.addActionListener(e->this.controller.setDisplayPosition(RXTomoJViewController.POSITION_RAW));
        cartesianPosition.addActionListener(e->this.controller.setDisplayPosition(RXTomoJViewController.POSITION_CARTESIAN));

        buttonGroup.add(rawPosition);
        buttonGroup.add(cartesianPosition);

        switch (controller.getPositionDisplay()){
            case RXTomoJViewController.POSITION_CARTESIAN:
                cartesianPosition.setSelected(true);
                break;
            case RXTomoJViewController.POSITION_RAW:
                rawPosition.setSelected(true);
                break;
            default:
                rawPosition.setSelected(true);
                break;
        }


        Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.NONE,1,0,0,0,1,1,0,0,scrollOption,rawPosition);
        Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.NONE,1,0,0,0,1,1,1,0,scrollOption,cartesianPosition);

        //anchors,top,left,bottom,right,fill,wX,wY,padx,pady,x,y
        Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.NONE,1,0,0,0,1,1,0,0,pane,scrollOption);

        return pane;
    }

//    @Override
//    public void actionPerformed(ActionEvent e) {
//        if ( e.getActionCommand().compareToIgnoreCase(cmdCP) == 0){
//            this.controller.setDisplayPosition(RXTomoJViewController.POSITION_CARTESIAN);
//        }else if (  e.getActionCommand().compareToIgnoreCase(cmdRD) == 0 ){
//            this.controller.setDisplayPosition(RXTomoJViewController.POSITION_RAW);
//        }
//    }

//    @Override
//    public boolean setUpInModel() {
//        this.key = this.getClass().toString();
//        if ( !this.controller.getInnerFrame(key) ) {
//            this.controller.addInnerFrame(this.key, this);
//            return true;
//        }
//        return false;
//    }

    @Override
    public void update() {
        System.err.println("Empty update :"+this.getClass().toString());
    }
}
