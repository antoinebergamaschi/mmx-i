/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow;

import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.SessionObject.DataType;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.MotorType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by bergamaschi on 05/02/14.
 */
public class JFrameMotorsChoice extends JFrameInner implements ItemListener {

    ArrayList<JComboBox> jComboBoxes;
    ArrayList<JLabel> labels;

    private final String nullString = "void";
//    private final String cmdValidate = "cv",cmdCancel = "cc";

    public JFrameMotorsChoice(RXTomoJViewController controller){
        super(controller);
    }



    protected JPanel buildContentPane(){
        JPanel pane = new JPanel(new GridBagLayout());

        labels = new ArrayList<>();
        jComboBoxes = new ArrayList<>();

        int i;
        ArrayList<String> motors = RXTomoJ.getInstance().getModelController().getSessionController().getDataset(DataType.MOTOR);
        for ( i = 0 ; i < motors.size() ; i++ ){
            Utils.addGridBag(GridBagConstraints.CENTER,10,10,10,5,GridBagConstraints.BOTH,0,0,0,0,2,1,0,i,pane,createNewChoicePanel(motors.get(i)));
        }

        //Add the Button Valide / Cancel
//        JButton cancel = new JButton(this.controller.getRessourceValue("Hdf5openerJFrame_button_cancel"));
//        cancel.setActionCommand(this.cmdCancel);
//        JButton validate = new JButton(this.controller.getRessourceValue("Hdf5openerJFrame_button_validate"));
//        validate.setActionCommand(this.cmdValidate);

//        cancel.addActionListener(l-> dispose());
//
//        validate.addActionListener(l->{
//            for ( int c = 0 ; c < this.jComboBoxes.size() ; c++ ){
//                String name = this.jComboBoxes.get(c).getSelectedItem().toString();
//                if ( name != nullString ){
//                    for ( MotorType type : MotorType.values() ){
//                        if ( type.getName().equalsIgnoreCase(name) ){
//                            RXTomoJ.getInstance().getModelController().getDataController().manageMotor(type, this.labels.get(c).getText());
//                        }
//                    }
//                }else{
////                    for ( MotorType type : MotorType.values() ){
////                        if ( type.getName().equalsIgnoreCase(name) ){
//                            //Remove the motors
//                            RXTomoJ.getInstance().getModelController().getDataController().manageMotor(null, this.labels.get(c).getText());
////                        }
////                    }
//                }
//            }
//            dispose();
//        });


        //anchors,top,left,bottom,right,fill,wX,wY,padx,pady,x,y
//        Utils.addGridBag(GridBagConstraints.LINE_END,5,5,5,5,GridBagConstraints.NONE,1,0,0,0,1,1,0,i+1,pane,cancel);

//        Utils.addGridBag(GridBagConstraints.LINE_START,5,5,5,5,GridBagConstraints.NONE,1,0,0,0,1,1,1,i+1,pane,validate);

        return pane;
    }

    private JPanel createNewChoicePanel(String motorID){
        JPanel pane = new JPanel();
        JLabel name = new JLabel(motorID);
        labels.add(name);

        BorderLayout borderLayout = new BorderLayout();
        borderLayout.setHgap(10);

        pane.setLayout(borderLayout);

        JComboBox jComboBox = new JComboBox(MotorType.getNames());
        jComboBox.addItem(nullString);

        jComboBox.addItemListener(this);

        jComboBoxes.add(jComboBox);

        pane.add(name,BorderLayout.WEST);
        pane.add(jComboBox,BorderLayout.EAST);

        return pane;
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if ( !lockUpdate ) {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                if (((String) e.getItem()).compareToIgnoreCase(nullString) != 0) {
                    for (MotorType type : MotorType.values()) {
                        if (type.getName().equalsIgnoreCase((String) e.getItem())) {
                            RXTomoJ.getInstance().getModelController().getDataController().manageMotor(type, this.labels.get(jComboBoxes.indexOf(e.getSource())).getText());
                            break;
                        }
                    }

                    //Select this item and remove this choice from the other Jcombobox
                    for (JComboBox jComboBox : jComboBoxes) {
                        if (!jComboBox.equals(e.getSource())) {
                            jComboBox.removeItem(e.getItem());
                        }
                    }
                } else {
                    RXTomoJ.getInstance().getModelController().getDataController().manageMotor(null, this.labels.get(jComboBoxes.indexOf(e.getSource())).getText());
                }
            } else if (e.getStateChange() == ItemEvent.DESELECTED) {
                for (JComboBox jComboBox : jComboBoxes) {
                    //Remove this selection, add this item to each other JcomboBox
                    if (!jComboBox.equals(e.getSource())) {
                        boolean test = true;
                        for (int i = 0; i < jComboBox.getItemCount(); i++) {
                            if (jComboBox.getItemAt(i).equals(e.getItem())) {
                                test = false;
                            }
                        }
                        if (test) {
                            jComboBox.addItem(e.getItem());
                        }
                    }
                }
            }
        }
    }

//    @Override
//    public boolean setUpInModel() {
//        this.key = this.getClass().toString();
//        if ( !this.controller.getInnerFrame(key) ) {
//            this.controller.addInnerFrame(this.key, this);
//            return true;
//        }
//        return false;
//    }

    @Override
    public void update(){
        lockUpdate = true;
        boolean isInit;

        HashMap<MotorType,String> motors = RXTomoJ.getInstance().getModelController().getDataController().getSelectedMotors();
        for ( int i = 0 ; i < this.labels.size() ; i++ ){
            isInit = false;
            for ( MotorType motorType : motors.keySet() ) {
                if (motors.get(motorType).equalsIgnoreCase(this.labels.get(i).getText())){
                    this.jComboBoxes.get(i).setSelectedItem(motorType.getName());
                    isInit = true;
                    break;
                }
            }

            if ( !isInit ){
                this.jComboBoxes.get(i).setSelectedItem(nullString);
            }
        }

        lockUpdate = false;
    }
}
