/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow;

import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.ShiftImage.MirroringType;
import com.soleil.nanoscopium.rxtomoj.model.computationFunction.ComputePhaseContrast.PhaseRetrievalAlgo;
import com.soleil.nanoscopium.rxtomoj.model.computationFunction.ComputePhaseContrast.PhaseComputationTechniques;
import com.soleil.nanoscopium.rxtomoj.model.data.DataStorage;
import com.soleil.nanoscopium.rxtomoj.model.data.PhaseContrastInfo;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

import static com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType.PHASE_GRAD_X;
import static com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType.PHASE_GRAD_Y;

/**
 * Created by bergamaschi on 28/05/2015.
 */
public class JFramePhaseContrastOption extends JFrameInner  {

    public JFramePhaseContrastOption(RXTomoJViewController controller) {
        super(controller);
    }

    private JCheckBox centerOfGravity;
    private JCheckBox intensityDistribution;
    private JCheckBox fourier;
    private JCheckBox southwell;
    private JCheckBox mirroring;
    private JCheckBox ASDI;
    private JCheckBox MDI;
    private JCheckBox gaussian;
    private JCheckBox jacobi;
    private JCheckBox normalizeX;
    private JCheckBox normalizeY;

    @Override
    protected JPanel buildContentPane() {
        JPanel pane = new JPanel(new GridBagLayout());

        JPanel scrollOption = new JPanel(new GridBagLayout());
        TitledBorder titledBorder = BorderFactory.createTitledBorder(controller.getRessourceValue("JFramePhaseContrastOption_subtitle_construction"));
        scrollOption.setBorder(titledBorder);

        JPanel retrievalPanel = new JPanel(new GridBagLayout());
        TitledBorder titledBorder2 = BorderFactory.createTitledBorder(controller.getRessourceValue("JFramePhaseContrastOption_subtitle_retrieval"));
        retrievalPanel.setBorder(titledBorder2);

        ButtonGroup buttonGroup = new ButtonGroup();
        centerOfGravity = new JCheckBox(controller.getRessourceValue("JFramePhaseContrastOption_check_centerOfGravity"));
        intensityDistribution = new JCheckBox(controller.getRessourceValue("JFramePhaseContrastOption_check_intensityDistribution"));

        normalizeX = new JCheckBox(controller.getRessourceValue("JFramePhaseContrastOption_check_normalizeX"));
        normalizeY = new JCheckBox(controller.getRessourceValue("JFramePhaseContrastOption_check_normalizeY"));

        ButtonGroup buttonGroupRetrieval = new ButtonGroup();
        fourier = new JCheckBox(controller.getRessourceValue("JFramePhaseContrastOption_check_retrieval_fourrier"));
        southwell = new JCheckBox(controller.getRessourceValue("JFramePhaseContrastOption_check_retrieval_southwell"));

        ButtonGroup buttonGroupSouthwell = new ButtonGroup();
        gaussian = new JCheckBox(controller.getRessourceValue("JFramePhaseContrastOption_check_retrieval_southwell_gaussian"));
        jacobi = new JCheckBox(controller.getRessourceValue("JFramePhaseContrastOption_check_retrieval_southwell_jacobi"));


        mirroring = new JCheckBox(controller.getRessourceValue("JFramePhaseContrastOption_check_retrieval_mirroring"));

        ButtonGroup buttonGroupMirroring = new ButtonGroup();
        ASDI = new JCheckBox(controller.getRessourceValue("JFramePhaseContrastOption_check_retrieval_asdi"));
        MDI = new JCheckBox(controller.getRessourceValue("JFramePhaseContrastOption_check_retrieval_mdi"));

        buttonGroup.add(centerOfGravity);
        buttonGroup.add(intensityDistribution);

        buttonGroupRetrieval.add(fourier);
        buttonGroupRetrieval.add(southwell);

        buttonGroupMirroring.add(ASDI);
        buttonGroupMirroring.add(MDI);

        buttonGroupSouthwell.add(gaussian);
        buttonGroupSouthwell.add(jacobi);

        //If the Gradient exist
        if ( RXTomoJ.getInstance().getModelController().getDataController().getComputedStack(ComputationUtils.StackType.PHASE_GRAD_X) != null &&
                RXTomoJ.getInstance().getModelController().getDataController().getComputedStack(ComputationUtils.StackType.PHASE_GRAD_Y) !=null  ){
            //Add a new option to directly recompute the phase avoiding recomputation of X/Y ( normalization is done )

            JPanel recomputationPanel = new JPanel(new GridBagLayout());
            TitledBorder titledBorder3 = BorderFactory.createTitledBorder(controller.getRessourceValue("JFramePhaseContrastOption_subtitle_recomputation"));
            retrievalPanel.setBorder(titledBorder3);

            JButton button = new JButton(controller.getRessourceValue("JFramePhaseContrastOption_button_recalculate"));
            button.addActionListener(l->RXTomoJ.getInstance().getModelController().getComputationController().computePhaseRetrieval());

            Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 1, 0, 0, 0, 1, 1, 1, 1, recomputationPanel, button);

            Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.BOTH,1,0,0,0,1,1,0,2,pane,recomputationPanel);
        }

        centerOfGravity.addActionListener(l -> RXTomoJ.getInstance().getModelController().getDataController().setPhaseComputationTechnique(PhaseComputationTechniques.CENTEROFGRAVITY));
        intensityDistribution.addActionListener(l -> RXTomoJ.getInstance().getModelController().getDataController().setPhaseComputationTechnique(PhaseComputationTechniques.INTENSITYMAP));

        fourier.addActionListener(l->{
            if ( fourier.isSelected() ){
                mirroring.setEnabled(true);

                if ( mirroring.isSelected() ){
                    ASDI.setEnabled(true);
                    MDI.setEnabled(true);
                }

                gaussian.setEnabled(false);
                jacobi.setEnabled(false);
            }
        });
        southwell.addActionListener(l->{
            if ( southwell.isSelected() ){
                gaussian.setEnabled(true);
                jacobi.setEnabled(true);

                mirroring.setEnabled(false);
                ASDI.setEnabled(false);
                MDI.setEnabled(false);
            }
        });
        mirroring.addActionListener(l->{
            if ( mirroring.isSelected() && fourier.isSelected() ){
                ASDI.setEnabled(true);
                MDI.setEnabled(true);
            }
            else{
                ASDI.setEnabled(false);
                MDI.setEnabled(false);
            }
        });

        gaussian.addActionListener(l->RXTomoJ.getInstance().getModelController().getDataController().setPhaseRetrievalAlgo(PhaseRetrievalAlgo.REALSPACE_GAUSSIAN));
        jacobi.addActionListener(l->RXTomoJ.getInstance().getModelController().getDataController().setPhaseRetrievalAlgo(PhaseRetrievalAlgo.REALSPACE_JACOBI));

        ASDI.addActionListener(l->RXTomoJ.getInstance().getModelController().getDataController().setPhaseContrastMirroring(MirroringType.ASDI_X, MirroringType.ASDI_Y));
        MDI.addActionListener(l->RXTomoJ.getInstance().getModelController().getDataController().setPhaseContrastMirroring(MirroringType.MDI, MirroringType.MDI));

        fourier.addActionListener(l->RXTomoJ.getInstance().getModelController().getDataController().setPhaseRetrievalAlgo(PhaseRetrievalAlgo.FOURRIER));
        mirroring.addActionListener(l->RXTomoJ.getInstance().getModelController().getDataController().setUseMirroring(mirroring.isSelected()));

        normalizeX.addActionListener(l->RXTomoJ.getInstance().getModelController().getDataController().setGradientNormalizeX(normalizeX.isSelected()));
        normalizeY.addActionListener(l->RXTomoJ.getInstance().getModelController().getDataController().setGradientNormalizeY(normalizeY.isSelected()));


        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 1, 0, 0, 0, 1, 1, 0, 0, scrollOption, centerOfGravity);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 1, 0, 0, 0, 1, 1, 1, 0, scrollOption, intensityDistribution);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 1, 0, 0, 0, 1, 1, 0, 1, scrollOption, normalizeX);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 1, 0, 0, 0, 1, 1, 1, 1, scrollOption, normalizeY);

        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 1, 0, 0, 0, 1, 1, 0, 0, retrievalPanel, fourier);
        Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.NONE,1,0,0,0,1,1,1,0,retrievalPanel,southwell);

        Utils.addGridBag(GridBagConstraints.LINE_START,5,5,5,5,GridBagConstraints.HORIZONTAL,1,0,0,0,1,1,0,1,retrievalPanel,mirroring);
        Utils.addGridBag(GridBagConstraints.LINE_START,5,5,5,5,GridBagConstraints.HORIZONTAL,1,0,0,0,1,1,1,2,retrievalPanel,ASDI);
        Utils.addGridBag(GridBagConstraints.LINE_START,5,5,5,5,GridBagConstraints.HORIZONTAL,1,0,0,0,1,1,1,3,retrievalPanel,MDI);
        Utils.addGridBag(GridBagConstraints.LINE_START,5,5,5,5,GridBagConstraints.HORIZONTAL,1,0,0,0,1,1,0,4,retrievalPanel,gaussian);
        Utils.addGridBag(GridBagConstraints.LINE_START,5,5,5,5,GridBagConstraints.HORIZONTAL,1,0,0,0,1,1,0,5,retrievalPanel,jacobi);

        //anchors,top,left,bottom,right,fill,wX,wY,padx,pady,x,y
        Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.NONE,1,0,0,0,1,1,0,0,pane,scrollOption);
        Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.BOTH,1,0,0,0,1,1,0,1,pane,retrievalPanel);

        return pane;
    }

    @Override
    public void update() {
        PhaseContrastInfo phaseContrastInfo = RXTomoJ.getInstance().getModelController().getDataController().getPhaseContrastInfo();

        centerOfGravity.setSelected(phaseContrastInfo.getPhaseComputationTechniques() == PhaseComputationTechniques.CENTEROFGRAVITY);
        intensityDistribution.setSelected(phaseContrastInfo.getPhaseComputationTechniques() == PhaseComputationTechniques.INTENSITYMAP);
        fourier.setSelected(phaseContrastInfo.getPhaseRetrievalAlgo() == PhaseRetrievalAlgo.FOURRIER);
        southwell.setSelected(phaseContrastInfo.getPhaseRetrievalAlgo() == PhaseRetrievalAlgo.REALSPACE_GAUSSIAN ||
                phaseContrastInfo.getPhaseRetrievalAlgo() == PhaseRetrievalAlgo.REALSPACE_JACOBI);
        mirroring.setSelected(phaseContrastInfo.isUseMirroring());
        ASDI.setSelected(phaseContrastInfo.getReal() == MirroringType.ASDI_X);
        MDI.setSelected(phaseContrastInfo.getReal() == MirroringType.MDI);
        gaussian.setSelected(phaseContrastInfo.getPhaseRetrievalAlgo() == PhaseRetrievalAlgo.REALSPACE_GAUSSIAN);
        jacobi.setSelected(phaseContrastInfo.getPhaseRetrievalAlgo() == PhaseRetrievalAlgo.REALSPACE_JACOBI);
        normalizeX.setSelected(phaseContrastInfo.isNormalizeX());
        normalizeY.setSelected(phaseContrastInfo.isNormalizeY());


        if ( fourier.isSelected() ){
            mirroring.setEnabled(true);
            if ( mirroring.isSelected() ){
                ASDI.setEnabled(true);
                MDI.setEnabled(true);
            }

            gaussian.setEnabled(false);
            jacobi.setEnabled(false);
        }
        if ( southwell.isSelected() ){
            mirroring.setEnabled(false);
            ASDI.setEnabled(false);
            MDI.setEnabled(false);

            gaussian.setEnabled(true);
            jacobi.setEnabled(true);
        }

    }
}
