/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow;

import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.data.NormalizationInfo;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

/**
 * Created by bergamaschi on 09/03/2015.
 */
public class JFrameRegriddingOption extends JFrameInner {


    public JFrameRegriddingOption(RXTomoJViewController controller) {
        super(controller);
    }

    @Override
    protected JPanel buildContentPane() {
        JPanel pane = new JPanel(new GridBagLayout());

        JPanel scrollOption = new JPanel(new GridBagLayout());
        TitledBorder titledBorder = BorderFactory.createTitledBorder(controller.getRessourceValue("JFrameRegriddingOption_title"));
        scrollOption.setBorder(titledBorder);

        NormalizationInfo normalizationInfo = RXTomoJ.getInstance().getModelController().getDataController().getNormalizationInfo();

//        int[] dist = RXTomoJ.getInstance().getModelController().getDataController().getNearestNeighbourDistance();
        int[] dist = new int[]{normalizationInfo.getInterpolationDistance(),normalizationInfo.getInterpolationDistance_fluorescence()};
        JSpinner interpolationDistance = new JSpinner(new SpinnerNumberModel(dist[0],0,20,1));
        JSpinner interpolationDistance_fluo = new JSpinner(new SpinnerNumberModel(dist[1],0,20,1));
        JPanel line = Utils.createOptionLine(interpolationDistance, "JFrameSamplingOption_spinner_label_distance", "JFrameSamplingOption_spinner_label_distance_tooltip");
        JPanel line1 = Utils.createOptionLine(interpolationDistance_fluo, "JFrameSamplingOption_spinner_label_distanceFluo", "JFrameSamplingOption_spinner_label_distanceFluo_tooltip");

        JButton validate_button = Utils.createValidateButton();

        validate_button.addActionListener(l-> {
            RXTomoJ.getInstance().getModelController().getDataController().setRegriddingInterpolationDistance((Integer) interpolationDistance.getValue());
            RXTomoJ.getInstance().getModelController().getDataController().setRegriddingInterpolationDistance_fluorescence((Integer) interpolationDistance_fluo.getValue());
//            RXTomoJ.getInstance().getModelController().getDataController().setNearestNeighbourDistance(new int[]{(Integer) interpolationDistance.getValue(),(Integer) interpolationDistance_fluo.getValue()});
            this.dispose();
        });

        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, scrollOption, line);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 1, scrollOption, line1);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.NONE, 0, 0, 0, 0, 1, 1, 0, 2, scrollOption, validate_button);

        //anchors,top,left,bottom,right,fill,wX,wY,padx,pady,x,y
        Utils.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,pane,scrollOption);

        return pane;
    }

    @Override
    public void update() {
        System.err.println("Empty update :"+this.getClass().toString());
    }
}
