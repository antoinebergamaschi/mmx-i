/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow;

import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Updatable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Created by bergamaschi on 25/02/14.
 */
abstract class JFrameInner extends JFrame implements ActionListener, WindowListener, Updatable {
    protected RXTomoJViewController controller;
    protected String key;
    protected boolean lockUpdate = false;

    protected JFrameInner(RXTomoJViewController controller){
        this.controller = controller;
        if (this.setUpInModel()) {
            this.build();
            this.addWindowListener(this);
        }
    }

    protected void build(){
//        this.setUndecorated(true);

        this.setContentPane(this.buildContentPane());
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
        this.setResizable(false);
        this.setAlwaysOnTop( true );

        //Set a Border
//        Border blackline = BorderFactory.createLineBorder(Color.black,5);
//        Border raisedbevel = BorderFactory.createRaisedBevelBorder();
//        Border loweredbevel = BorderFactory.createLoweredBevelBorder();
//        Border compound = BorderFactory.createCompoundBorder(raisedbevel, loweredbevel);
//        compound = BorderFactory.createCompoundBorder(blackline, compound);
//        this.getRootPane().setBorder(compound);



        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension windowSize = this.getSize();

        int windowX = Math.max(0, (screenSize.width  - (windowSize.width*2) ) / 2);
        int windowY = Math.max(0, (screenSize.height - (windowSize.height*2) ) / 2);

        setLocation(windowX, windowY);
        this.update();

        this.validate();
        this.pack();
    }

    abstract protected JPanel buildContentPane();

    public boolean setUpInModel(){
        this.key = this.getClass().toString();
        if ( !this.controller.getInnerFrame(key) ) {
            this.controller.addInnerFrame(this.key, this);
            return true;
        }
        return false;
    }

    public void removeFromModel(){
        this.controller.removeInnerFrame(this.key);
    }


    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {
        this.removeFromModel();
    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

}
