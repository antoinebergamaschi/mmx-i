/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow;

import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.data.DarkFieldInfo;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

/**
 * Created by bergamaschi on 01/06/2015.
 */
public class JFrameDarkFieldOption extends JFrameInner  {
    private int numbOfSel = 0;
    private JButton addSelection;
    private JPanel darkFieldListPanel;
    private boolean isSymmetrical = false;
    private JCheckBox circleShape,squareShape;

    public JFrameDarkFieldOption(RXTomoJViewController controller) {
        super(controller);
    }


    @Override
    protected JPanel buildContentPane() {
        //For better display here set a prefered Dimension
        this.setPreferredSize(new Dimension(400,600));

        JPanel pane = new JPanel(new GridBagLayout());

        //Create the JButton group containing the circle or square mode
        ButtonGroup buttonGroup = new ButtonGroup();
        circleShape = new JCheckBox(controller.getRessourceValue("JFameDarkFieldOption_shape_check_circle"));
        squareShape = new JCheckBox(controller.getRessourceValue("JFameDarkFieldOption_shape_check_square"));

        buttonGroup.add(circleShape);
        buttonGroup.add(squareShape);

        circleShape.addActionListener(l->{
            if ( circleShape.isSelected() ){
                RXTomoJ.getInstance().getModelController().getDataController().setDarkFieldShape(DarkFieldInfo.SpotGeometry.CIRCLE);
            }
        });

        squareShape.addActionListener(l->{
            if (squareShape.isSelected()){
                RXTomoJ.getInstance().getModelController().getDataController().setDarkFieldShape(DarkFieldInfo.SpotGeometry.SQUARE);
            }
        });

        //Create the panel containing the shape selection
        JPanel shapePanel = new JPanel(new GridBagLayout());

        TitledBorder shapeTitle = BorderFactory.createTitledBorder(controller.getRessourceValue("JFameDarkFieldOption_shape_title"));
        shapePanel.setBorder(shapeTitle);

        //Add the buttons in the panel
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.HORIZONTAL, 1, 0, 0, 0, 1, 1, 0, 0, shapePanel, circleShape);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.HORIZONTAL, 1, 0, 0, 0, 1, 1, 1, 0, shapePanel, squareShape);

        //Add button to change interface in case of symmetrical spot
        JCheckBox symmetricalShape = new JCheckBox(controller.getRessourceValue("JFameDarkFieldOption_shape_option_symmetrical"));
        symmetricalShape.setSelected(isSymmetrical);

        symmetricalShape.addActionListener(l-> {
            isSymmetrical = symmetricalShape.isSelected();
            for (  Component radiusSelection : darkFieldListPanel.getComponents() ){
                if ( radiusSelection instanceof RadiusSelection ){
                    ((RadiusSelection)radiusSelection).setVisibleY(!symmetricalShape.isSelected());
                }
            }
        });


        //Add the Scroll display for the darkField selection
        darkFieldListPanel = new JPanel(new GridBagLayout());
        JScrollPane scrollPane = new JScrollPane(darkFieldListPanel);

        addSelection = new JButton(controller.getRessourceValue("JFameDarkFieldOption_add_range"));

        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.HORIZONTAL, 1, 0, 0, 0, 1, 1, 0, 0, pane, shapePanel);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.HORIZONTAL, 1, 0, 0, 0, 1, 1, 0, 1, pane, symmetricalShape);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 2, pane, scrollPane);
        Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.HORIZONTAL, 1, 0, 0, 0, 1, 1, 0, 3, pane, addSelection);

        addSelection.addActionListener(l -> {
            //Update the Model
            RXTomoJ.getInstance().getModelController().getDataController().addDarkFieldRange("null", new int[]{0, 10, 0 , 10});
            update();
        });
        return pane;
    }

    @Override
    public void update() {
        darkFieldListPanel.removeAll();
        DarkFieldInfo darkFieldInfo = RXTomoJ.getInstance().getModelController().getDataController().getDarkFieldInfo();
        numbOfSel = 0;
        for (  String name : darkFieldInfo.getDarkArea().keySet() ){
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.HORIZONTAL, 1, 0, 0, 0, 1, 1, 0, numbOfSel, darkFieldListPanel, new RadiusSelection(name,darkFieldInfo.getDarkArea().get(name)));
            numbOfSel++;
        }

        //Update the current shape
        if ( darkFieldInfo.getIntegrationType() == DarkFieldInfo.SpotGeometry.CIRCLE ){
            circleShape.setSelected(true);
            squareShape.setSelected(false);
        }else if ( darkFieldInfo.getIntegrationType() == DarkFieldInfo.SpotGeometry.SQUARE ){
            circleShape.setSelected(false);
            squareShape.setSelected(true);
        }

        this.revalidate();
        this.pack();
    }

    private class RadiusSelection extends JPanel{
        JSlider XStart;
        JSlider Xrange;

        //Only display when not symmetrical is selected
        JSlider YStart;
        JSlider Yrange;

        JPanel panelX;
        JPanel panelY;

        //Range Type
        int X_TYPE = 0, Y_TYPE = 1;

        String ID;

        public RadiusSelection(String ID){
            this(ID, new int[]{0, 10, 0 , 10});
        }

        public RadiusSelection(String ID, int[] range){
            super(new GridBagLayout());

            TitledBorder titledBorder = BorderFactory.createTitledBorder(controller.getRessourceValue("JFameDarkFieldOption_range_subtitle"));
            this.setBorder(titledBorder);

            this.ID = ID;


            JFormattedTextField name = new JFormattedTextField(this.ID);
            name.setFocusLostBehavior(JFormattedTextField.COMMIT);

            JLabel nameID = new JLabel(controller.getRessourceValue("JFameDarkFieldOption_range_ID"));

            JButton button = new JButton(controller.getRessourceValue("JFameDarkFieldOption_range_delete"));
            int maxRadius = RXTomoJ.getInstance().getModelController().getDataController().getDarkFieldMaxRadius();



            XStart = new JSlider(JSlider.HORIZONTAL,0,maxRadius-1,range[0]);
            Xrange = new JSlider(JSlider.HORIZONTAL,1,maxRadius,range[1]);

            YStart = new JSlider(JSlider.HORIZONTAL,0,maxRadius-1,range[2]);
            Yrange = new JSlider(JSlider.HORIZONTAL,1,maxRadius,range[3]);

            panelX = createRange(XStart,Xrange,0);
            panelY = createRange(YStart,Yrange,1);

            if ( isSymmetrical ){
                panelY.setVisible(false);
            }

            Xrange.addChangeListener(l -> {
                fireUpdate();
            });

            XStart.addChangeListener(l -> {
                Xrange.setMinimum(XStart.getValue());
                fireUpdate();
            });

            Yrange.addChangeListener(l -> {
                fireUpdate();
            });

            YStart.addChangeListener(l -> {
                Yrange.setMinimum(YStart.getValue());
                fireUpdate();
            });

            button.addActionListener(l -> {
                RXTomoJ.getInstance().getModelController().getDataController().removeDarkFieldRange(this.ID);
                JFrameDarkFieldOption.this.update();
            });

            name.addPropertyChangeListener("value",l -> {
                if (!name.hasFocus()) {
                    this.ID = RXTomoJ.getInstance().getModelController().getDataController().updateDarkFieldRangeID(this.ID, name.getText());
                }
            });

            name.addActionListener(l -> this.ID = name.getText());


            Utils.addGridBag(GridBagConstraints.LINE_START, 5, 5, 5, 5, GridBagConstraints.NONE, 1, 0, 0, 0, 1, 1, 0, 0, this, nameID);
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 0, 0, 0, 1, 1, 1, 0, this, name);
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 0, 0, 0, 2, 1, 0, 1, this, panelX);
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 0, 0, 0, 2, 1, 0, 2, this, panelY);
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 0, 0, 0, 2, 1, 0, 3, this, button);

        }

        private JPanel createRange(JSlider start, JSlider range, int type){
            JPanel panel = new JPanel(new GridBagLayout());
            JLabel radiusInitName = new JLabel();
            JLabel radiusFinalName = new JLabel();
            if ( type == X_TYPE ) {
                radiusInitName.setText(controller.getRessourceValue("JFameDarkFieldOption_range_init_X"));
                radiusFinalName.setText(controller.getRessourceValue("JFameDarkFieldOption_range_final_X"));
            }
            else if ( type == Y_TYPE ){
                radiusInitName.setText(controller.getRessourceValue("JFameDarkFieldOption_range_init_Y"));
                radiusFinalName.setText(controller.getRessourceValue("JFameDarkFieldOption_range_final_Y"));
            }
            JLabel valInit = new JLabel(start.getValue()+"     ");
            JLabel valFinal = new JLabel(range.getValue()+"     ");

            range.addChangeListener(l -> {
                valFinal.setText(range.getValue() + "");
            });

            start.addChangeListener(l -> {
                valInit.setText(start.getValue() + "");
                range.setMinimum(start.getValue()+1);
            });

            Utils.addGridBag(GridBagConstraints.LINE_START, 5, 5, 5, 5, GridBagConstraints.NONE, 1, 0, 0, 0, 1, 1, 0, 0, panel, radiusInitName);
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 0, 0, 0, 1, 1, 1, 0, panel, start);
            Utils.addGridBag(GridBagConstraints.LINE_END, 5, 5, 5, 5, GridBagConstraints.NONE, 1, 0, 0, 0, 1, 1, 2, 0, panel, valInit);

            Utils.addGridBag(GridBagConstraints.LINE_START, 5, 5, 5, 5, GridBagConstraints.NONE, 1, 0, 0, 0, 1, 1, 0, 1, panel, radiusFinalName);
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 5, GridBagConstraints.BOTH, 1, 0, 0, 0, 1, 1, 1, 1, panel, range);
            Utils.addGridBag(GridBagConstraints.LINE_END, 5, 5, 5, 5, GridBagConstraints.NONE, 1, 0, 0, 0, 1, 1, 2, 1, panel, valFinal);

            return panel;
        }

        public void setVisibleY(boolean visible){
            panelY.setVisible(visible);
            fireUpdate();
        }

        private void fireUpdate(){
            if ( JFrameDarkFieldOption.this.isSymmetrical ){
                RXTomoJ.getInstance().getModelController().getDataController().updateDarkFieldRange(this.ID, XStart.getValue(), Xrange.getValue());
            }else{
                RXTomoJ.getInstance().getModelController().getDataController().updateDarkFieldRange(this.ID, XStart.getValue(), Xrange.getValue(), YStart.getValue(), Yrange.getValue());
            }
        }
    }

}
