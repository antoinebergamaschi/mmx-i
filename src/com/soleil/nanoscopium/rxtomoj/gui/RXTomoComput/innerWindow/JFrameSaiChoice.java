/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.innerWindow;


import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualSpectrum;
import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStack;
import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStackFactory;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.JPanelSelectSpectrum;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.JScrollBarRawData;
import com.soleil.nanoscopium.rxtomoj.gui.utils.JPanelPlot;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.SessionObject.DataType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Created by bergamaschi on 25/02/14.
 */
public class JFrameSaiChoice extends JFrameInner {
    private ExtendedJPanelSelectSpectrum select;
    private JPanelPlot spc;
    private ExtendedJScrollBarRawData jScollBarRawData;

    public JFrameSaiChoice(RXTomoJViewController controller) {
        super(controller);
        this.setMinimumSize(new Dimension(600,200));
    }

    @Override
    protected JPanel buildContentPane() {
        JPanel pane = new JPanel(new GridBagLayout());
        jScollBarRawData = new ExtendedJScrollBarRawData();
        select = new ExtendedJPanelSelectSpectrum(this.controller);
//        spc = new ExtendedSpectrumImage();
        spc = new JPanelPlot();
        select.update();
        jScollBarRawData.addHdf5(spc);

        select.removeTopPanel();


        //anchors,top,left,bottom,right,fill,wX,wY,padx,pady,x,y
        Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 0.5, 1, 0, 0, 1, 2, 0, 0, pane, select);

        //anchors,top,left,bottom,right,fill,wX,wY,padx,pady,x,y
        Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,1,0,pane,spc);

        //anchors,top,left,bottom,right,fill,wX,wY,padx,pady,x,y
        Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,0.2,0,0,1,1,1,1,pane,jScollBarRawData);

//        updateInterface();
        return pane;
    }

//    @Override
//    public boolean setUpInModel() {
//        this.key = this.getClass().toString();
//        if ( !this.controller.getInnerFrame(key) ) {
//            this.controller.addInnerFrame(this.key, this);
//            return true;
//        }
//        return false;
//    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }



    public void updateInterface(){
       select.update();
    }

    @Override
    public void update() {
        System.err.println("Empty update :"+this.getClass().toString());
    }

//    private class ExtendedSpectrumImage extends JPanelSpectrum {
//        public ExtendedSpectrumImage(){
//            super();
//        }
//    }

    private class ExtendedJPanelSelectSpectrum extends JPanelSelectSpectrum{
        public ExtendedJPanelSelectSpectrum(RXTomoJViewController controller){
            super(controller);
        }

        @Override
        public void addSpectrum(final String dataName,final Color color) {
            new Thread( () -> {
                //A waiting Gif While Be Show while the Spectrum is added
                setWaitingGif();
                Hdf5VirtualStack hdf5VirtualStack = Hdf5VirtualStackFactory.createVirtualStack(RXTomoJ.getInstance().getModelController().getSessionController().getPathToHdf5(), dataName, true);
//                Hdf5VirtualStack hdf5VirtualStack = new Hdf5VirtualStack(SessionObject.getCurrentSessionObject().getPathToHdf5(),dataName,true);

                //Set min and max / Mean
                ((Hdf5VirtualSpectrum)hdf5VirtualStack).initializeSpectrum();

                spc.addData((Hdf5VirtualSpectrum)hdf5VirtualStack, color);
                //Remove the Gif
                removeWaitingGif();
                jScollBarRawData.setMaximum((int) hdf5VirtualStack.getNSlice());

//                RXTomoJ.getInstance().getModelController().getDataController().manageXBPM(dataName);
            }).start();
//            manageSpectrum(dataName);
        }

        @Override
        public void setMeanSpectrum() {
//            spc.setConstantMeanPlot();
        }

        @Override
        public void removeMeanSpectrum() {
//            spc.removeConstantMeanPlot();
        }

        @Override
        public void setLogSpectrum() {
//            super.setLogSpectrum();
        }

        @Override
        public void removeLogSpectrum() {
//            super.removeLogSpectrum();
        }

        @Override
        public void manageSpectrum(String dataName) {
            RXTomoJ.getInstance().getModelController().getDataController().manageXBPM(dataName);
        }

        @Override
        public void removeSpectrum(String dataName) {
//            spc.remove(dataName);
            spc.releaseHdf5(dataName);
//            RXTomoJ.getInstance().getModelController().getDataController().manageXBPM(dataName);
        }

        @Override
        public void setPane() {
            for ( String name : RXTomoJ.getInstance().getModelController().getSessionController().getDataset(DataType.XBPM)){
                this.add(name,RXTomoJ.getInstance().getModelController().getDataController().getSelectedXBPM().contains(name));
            }
        }
    }

    private class ExtendedJScrollBarRawData extends JScrollBarRawData {
        public ExtendedJScrollBarRawData(){
            super(JScrollBar.HORIZONTAL,0,1,0,100);
        }
    }

}
