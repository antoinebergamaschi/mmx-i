/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput;

import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStack;
import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStackFactory;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.controller.SessionController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.BasicPanel;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.SessionObject.DataType;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by bergamaschi on 18/02/14.
 */
public class ScrollViewPanel extends JPanel implements PropertyChangeListener, KeyListener {
    private JScrollBar scrollBar;
    private JLabel labelRaw;
    private JLabel[] labels;
    private JFormattedTextField[] dims;
    private JFormattedTextField pixelValue;
    private NumberFormatter pnf;
    private boolean isEdit = false;
    private RXTomoJViewController controller;
    private DataConverter dataConverter;

    public ScrollViewPanel(JScrollBar scrollBar, RXTomoJViewController controller){
        super(new GridBagLayout());
        this.controller = controller;
        this.scrollBar = scrollBar;
        build();
        isEdit = true;
        this.pixelValue.setValue(0);
        isEdit = false;
        dataConverter = new DataConverter();
    }

    private void build(){
        Border raisedbevel = BorderFactory.createRaisedBevelBorder();
        this.setBorder(raisedbevel);

        this.pnf = new NumberFormatter();
        this.pnf.setAllowsInvalid(false);
        this.pnf.setMinimum(0);

        labelRaw = new JLabel(this.controller.getRessourceValue("SCrollViewPanel_label_name"));

        pixelValue = new JFormattedTextField(this.pnf);
        pixelValue.addPropertyChangeListener(this);
        pixelValue.addKeyListener(this);

        labels = new JLabel[3];
        labels[0] = new JLabel(this.controller.getRessourceValue("X")+" : ");
        labels[1] = new JLabel(this.controller.getRessourceValue("Y")+" : ");
        labels[2] = new JLabel(this.controller.getRessourceValue("Z")+" : ");

        dims = new JFormattedTextField[3];
        dims[0] = new JFormattedTextField(this.pnf);
        dims[1] = new JFormattedTextField(this.pnf);
        dims[2] = new JFormattedTextField(this.pnf);

        dims[0].addKeyListener(this);
        dims[0].addPropertyChangeListener(this);
        dims[1].addKeyListener(this);
        dims[1].addPropertyChangeListener(this);
        dims[2].addKeyListener(this);
        dims[2].addPropertyChangeListener(this);
        //anchors,top,left,bottom,right,fill,wX,wY,padx,pady,x,y
        Utils.addGridBag(GridBagConstraints.CENTER, BasicPanel.margin_inside, BasicPanel.margin_inside, BasicPanel.margin_inside, BasicPanel.margin_inside, GridBagConstraints.BOTH, 0, 0, 0, 0, 1, 1, 0, 0, this, this.labelRaw);
        Utils.addGridBag(GridBagConstraints.CENTER, BasicPanel.margin_inside, BasicPanel.margin_inside, BasicPanel.margin_inside, BasicPanel.margin_inside, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 0, this, this.pixelValue);

        for ( int i = 0 ; i < 3 ; i++ ){
            Utils.addGridBag(GridBagConstraints.CENTER, BasicPanel.margin_inside, BasicPanel.margin_inside, BasicPanel.margin_inside, BasicPanel.margin_inside, GridBagConstraints.BOTH, 0, 0, 0, 0, 1, 1, i*2, 0, this, this.labels[i]);
            Utils.addGridBag(GridBagConstraints.CENTER, BasicPanel.margin_inside, BasicPanel.margin_inside, BasicPanel.margin_inside, BasicPanel.margin_inside, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, i*2 + 1, 0, this, this.dims[i]);
            dims[i].setVisible(false);
            labels[i].setVisible(false);
        }

    }


//    public void updateScroll(int value){
//        this.scrollBar.setValue(value);
//    }

//    public void setMaximalValue(int maximalValue){
//        this.pnf.setMaximum(maximalValue);
//    }

    public void update(int position){
        if ( !isEdit ){
            isEdit = true;
            pixelValue.setValue(position);
            int[] positions = dataConverter.getPosition(position);
            for ( int i = 0 ; i < 3 ; i++ ){
                dims[i].setValue(positions[2-i]);
            }
            isEdit = false;
        }
    }


    public void changeDisplay(int displayType){
        dataConverter = new DataConverter();
        switch (displayType){
            case RXTomoJViewController.POSITION_RAW:
                pixelValue.setVisible(true);
                this.labelRaw.setVisible(true);
                for ( int i = 0 ; i < 3 ; i++ ){
                    dims[i].setVisible(false);
                    labels[i].setVisible(false);
                }
                break;
            case RXTomoJViewController.POSITION_CARTESIAN:
                this.pixelValue.setVisible(false);
                this.labelRaw.setVisible(false);
                for ( int i = 0 ; i < 3 ; i++ ){
                    dims[i].setVisible(true);
                    labels[i].setVisible(true);
                }
                break;
            default:
                break;
        }
    }


    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ( !isEdit ) {
            if (evt.getSource().equals(this.pixelValue)) {
                if (evt.getPropertyName() == "value") {
                    ((JScrollBarRawData) this.scrollBar)._setValue((Integer) evt.getNewValue());
                }
            } else if (evt.getSource().equals(this.dims[0]) || evt.getSource().equals(this.dims[1]) || evt.getSource().equals(this.dims[2])) {
                if (evt.getPropertyName() == "value") {
                    int[] val = new int[3];
                    if (dims[0].getValue() != null) {
                        val[0] = (Integer) dims[0].getValue();
                    }
                    if (dims[1].getValue() != null) {
                        val[1] = (Integer) dims[1].getValue();
                    }
                    if (dims[2].getValue() != null) {
                        val[2] = (Integer) dims[2].getValue();
                    }
                    ((JScrollBarRawData) this.scrollBar)._setValue(this.dataConverter.getPosition(val[0], val[1], val[2]));
                }
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if ( e.getSource().equals(this.pixelValue)){
            //Implemente Key modification
            if ( e.getKeyCode() == KeyEvent.VK_KP_UP || e.getKeyCode() == KeyEvent.VK_UP ){
                int newVal = (Integer)this.pixelValue.getValue();
                newVal ++;
                this.pixelValue.setValue(newVal);
            }
            else if ( e.getKeyCode() == KeyEvent.VK_KP_DOWN || e.getKeyCode() == KeyEvent.VK_DOWN ){
                int newVal = (Integer)this.pixelValue.getValue();
                newVal --;
                this.pixelValue.setValue(newVal);
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if ( e.getSource().equals(this.pixelValue)){
            try {
//                this.isEdit = true;
                this.pixelValue.commitEdit();
//                this.isEdit = false;
            } catch (ParseException e1) {
                e1.printStackTrace();
            }
        }else if ( e.getSource().equals(this.dims[0]) ){
            try {
//                this.isEdit = true;
                this.dims[0].commitEdit();
//                this.isEdit = false;
            } catch (ParseException e1) {
                e1.printStackTrace();
            }
        }else if ( e.getSource().equals(this.dims[1])){
            try {
//                this.isEdit = true;
                this.dims[1].commitEdit();
//                this.isEdit = false;
            } catch (ParseException e1) {
                e1.printStackTrace();
            }
        }else if (e.getSource().equals(this.dims[2])){
            try {
//                this.isEdit = true;
                this.dims[2].commitEdit();
//                this.isEdit = false;
            } catch (ParseException e1) {
                e1.printStackTrace();
            }
        }
    }


    private class DataConverter{
        private long y,x;
//        private long z;

        private float xy;
        boolean isInit = false;

        public DataConverter(){
            SessionController sessionController  =  RXTomoJ.getInstance().getModelController().getSessionController();
            ArrayList<String> imageIDs = sessionController.getDataset(DataType.IMAGE);
            if ( imageIDs.size() > 0 ){
                Hdf5VirtualStack hdf5VirtualStack = Hdf5VirtualStackFactory.createVirtualStack(sessionController.getPathToHdf5(), imageIDs.get(0), false);
//                Hdf5VirtualStack hdf5VirtualStack = new Hdf5VirtualStack(sessionObject.getPathToHdf5(),sessionObject.getImageID().get(0),false);
                long[] dims = hdf5VirtualStack.getGlobalDimension();
                y = dims[1];
                x = dims[0];

                xy = x*y;
//                hdf5VirtualStack.destroy();
                isInit = true;
            }
        }

        /**
         * Return the cartesian position from the raw data
         * @param rawPosition the current raw position
         * @return int[] {x,y,z} position in the resulting image
         */
        public int[] getPosition(long rawPosition){
            int[] result = new int[3];
            if ( isInit ){
                result[0] = (int) (rawPosition/xy);
                result[1] = (int) ((rawPosition%xy)/(float)x);
                result[2] = (int) ((rawPosition%xy)%x);
            }
            return result;
        }

        /**
         * return the long position in the Raw Data
         *
         * @param x the current x position in the resulting image
         * @param y the current y position in the resulting image
         * @param z the current z position in the resulting image
         * @return int the raw position
         */
        public int getPosition(int x, int y, int z){
            return (int) (x + (y * this.x) + (z * this.xy));
        }
    }
}
