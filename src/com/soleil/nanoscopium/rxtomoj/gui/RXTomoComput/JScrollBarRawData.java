/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput;

import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.ScrollUpdatable;

import javax.swing.*;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.util.ArrayList;

/**
 * Created by antoine bergamaschi on 24/01/14.
 */
public class JScrollBarRawData extends JScrollBar implements AdjustmentListener {

    private ArrayList<ScrollUpdatable> listenToHdf5 = new ArrayList<>();
    private ArrayList<ScrollViewPanel> views = new ArrayList<>();

    private boolean lock_adjustement = false;

    public JScrollBarRawData(int orientation, int value, int extent, int min, int max){
        super(orientation,value,extent,min,max);
        addAdjustmentListener(this);
    }


    public void addView(ScrollViewPanel view){
        if ( !this.views.contains(view) ) {
            this.views.add(view);
        }
    }

    public void addHdf5(ScrollUpdatable hdf5){
        if ( !listenToHdf5.contains(hdf5)) {
            this.listenToHdf5.add(hdf5);
        }
        this.fireUpdate();
    }

    public void removeHdf5(ScrollUpdatable hdf5){
        if ( listenToHdf5.contains(hdf5)) {
            this.listenToHdf5.remove(hdf5);
        }
        this.fireUpdate();
    }

    public void removeAll(){
        this.listenToHdf5 = new ArrayList<>();
        //Reset view
        changeDisplay(RXTomoJViewController.POSITION_RAW);
    }


    private void fireUpdate(){
        for ( ScrollUpdatable jPanelImage :  this.listenToHdf5){
            jPanelImage.update((long)this.getValue());
        }
    }

    public void changeDisplay(int displayType){
        for ( ScrollViewPanel view : this.views ){
            view.changeDisplay(displayType);
        }
    }


    public void _setValue(int value) {
        this.lock_adjustement = true;
        super.setValue(value);
        this.lock_adjustement = false;
    }

    @Override
    public void adjustmentValueChanged(AdjustmentEvent e) {
        if ( !lock_adjustement) {
            for (ScrollUpdatable jPanelImage : this.listenToHdf5) {
                jPanelImage.update(e.getValue());
            }
            for (ScrollViewPanel view : this.views) {
                view.update(e.getValue());
            }
        }
    }
}

