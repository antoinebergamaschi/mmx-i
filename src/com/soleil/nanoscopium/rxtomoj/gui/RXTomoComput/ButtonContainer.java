/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput;

import com.soleil.nanoscopium.rxtomoj.gui.utils.BasicPanel;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

/**
 * Created by bergamaschi on 25/07/2014.
 */
public class ButtonContainer extends BasicPanel implements MouseListener {
    private ArrayList<JLabel> button = new ArrayList<>();

    private String currentSelection = null;

    public ButtonContainer(){
        super(new GridBagLayout());
        Border mat = BorderFactory.createMatteBorder(1, 0, 0, 0, Color.black);
        this.setBorder(mat);
        build();
    }

    private void build(){
        int i = 0;
        for ( JLabel jLabel : button ){
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.VERTICAL, 0, 1, 20, 10, 1, 1, i, 0, this, jLabel);
            i++;
        }

        Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, i+1, 0, this, new JPanel());

    }

    public void addLabel(String name,String ID){
        addLabel(name,"",ID);
    }

    public void addLabel(String name, String tooltips, String ID){
        this.removeAll();

        JLabel label = new JLabel(name);
        label.setToolTipText(tooltips);
        label.addMouseListener(this);
        label.setOpaque(true);
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setName(ID);
        button.add(label);

        build();

        this.validate();
    }

    public void removeAllLabel(){
        button = new ArrayList<>();
        this.removeAll();
        this.validate();
    }

    public void init(int pos){
        button.get(pos).setBackground(BasicPanel.mouseClickLabel_background);
        button.get(pos).setForeground(BasicPanel.mouseClickLabel_forground);
    }

    public void init(String ID){
        for ( JLabel label : button ){
            if ( label.getName().equalsIgnoreCase(ID) ){
                label.setBackground(BasicPanel.mouseClickLabel_background);
                label.setForeground(BasicPanel.mouseClickLabel_forground);
            }else{
                label.setBackground(UIManager.getDefaults().getColor("Label.background"));
                label.setForeground(UIManager.getDefaults().getColor("Label.foreground"));
            }
        }
    }


    @Override
    public void mouseClicked(MouseEvent e) {
        for ( JLabel jLabel : button){
            if ( jLabel.equals(e.getSource()) ){
                currentSelection = jLabel.getName();
//                informationPanel.setPanel(Integer.parseInt(jLabel.getOtherName()));
            }
            //Reset JLabel Colors
            jLabel.setBackground(UIManager.getDefaults().getColor("Label.background"));
            jLabel.setForeground(UIManager.getDefaults().getColor("Label.foreground"));
        }

        ((Component)e.getSource()).setBackground(BasicPanel.mouseClickLabel_background);
        ((Component)e.getSource()).setForeground(BasicPanel.mouseClickLabel_forground);

        fireBasicListener();
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }


    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        for ( JLabel jLabel : button){
            if (jLabel.equals(e.getSource()) && jLabel.getForeground().equals(BasicPanel.mouseClickLabel_forground) ){
                return;
            }
        }

        ((Component)e.getSource()).setBackground(BasicPanel.mouseOverLabel_background);
        ((Component)e.getSource()).setForeground(BasicPanel.mouseOverLabel_forground);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        for ( JLabel jLabel : button){
            if ( jLabel.equals(e.getSource()) && jLabel.getForeground().equals(BasicPanel.mouseClickLabel_forground) ){
                return;
            }
        }
        //Reset JLabel Colors
        ((Component)e.getSource()).setBackground(UIManager.getDefaults().getColor("Label.background"));
        ((Component)e.getSource()).setForeground(UIManager.getDefaults().getColor("Label.foreground"));
    }

    public Object getCurrentSelection() {
        return currentSelection;
    }

    public String getCurrentSelectionID() {
        return currentSelection;
    }
}
