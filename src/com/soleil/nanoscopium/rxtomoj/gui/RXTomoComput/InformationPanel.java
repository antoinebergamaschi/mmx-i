/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput;

import com.soleil.nanoscopium.rxtomoj.gui.utils.Updatable;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;

import javax.swing.*;
import java.awt.*;

/**
 * Created by bergamaschi on 25/07/2014.
 */
abstract public class InformationPanel extends JPanel implements Updatable {
    public static int HORIZONTAL= 0,VERTICAL=1;

    //Content Panel
    private JPanel firstPane;
    //Title Panel
    private JLabel secondPane;

    //Identifier of the current Display
    private int currentDisplay;

    public InformationPanel(){
        super(new GridBagLayout());

        firstPane = new JPanel(new GridBagLayout());

        secondPane = new JLabel();
        secondPane.setVerticalAlignment(SwingConstants.TOP);
        secondPane.setHorizontalAlignment(JLabel.CENTER);
        secondPane.setBorder(BorderFactory.createRaisedBevelBorder());

        Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, this, firstPane);
        Utils.addGridBag(GridBagConstraints.CENTER, 0, 5, 0, 0, GridBagConstraints.VERTICAL, 0, 1, 10, 10, 1, 1, 1, 0, this, secondPane);
    }

    public InformationPanel(int type) {
        super(new GridBagLayout());

        firstPane = new JPanel(new GridBagLayout());

        if (type == HORIZONTAL) {
            secondPane = new JLabel();
            secondPane.setVerticalAlignment(SwingConstants.TOP);
            secondPane.setHorizontalAlignment(JLabel.CENTER);
            secondPane.setBorder(BorderFactory.createRaisedBevelBorder());

            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 5, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, this, firstPane);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 5, 0, 0, GridBagConstraints.VERTICAL, 0, 1, 10, 10, 1, 1, 1, 0, this, secondPane);
        }
        else if ( type == VERTICAL){
            secondPane = new JLabel();
            secondPane.setVerticalAlignment(SwingConstants.CENTER);
            secondPane.setHorizontalAlignment(JLabel.CENTER);
            secondPane.setBorder(BorderFactory.createRaisedBevelBorder());

            Utils.addGridBag(GridBagConstraints.CENTER, 5, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 1, this, firstPane);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 5, 0, GridBagConstraints.HORIZONTAL, 1, 0, 10, 10, 1, 1, 0, 0, this, secondPane);
        }
    }

    /**
     * Update the first and second Panel with the current panel
     * @param type The identifier of the panel to create and display
     */
    public void setPanel(int type){
        this.firstPane.removeAll();
        this.secondPane.setText("");
        this.currentDisplay = type;
    }


    public int getCurrentDisplay() {
        return currentDisplay;
    }

    public void setCurrentDisplay(int currentDisplay) {
        this.currentDisplay = currentDisplay;
    }

    public JPanel getFirstPane() {
        return firstPane;
    }

    public void setFirstPane(JPanel firstPane) {
        this.firstPane = firstPane;
    }

    public JLabel getSecondPane() {
        return secondPane;
    }

    public void setSecondPane(JLabel secondPane) {
        this.secondPane = secondPane;
    }

}
