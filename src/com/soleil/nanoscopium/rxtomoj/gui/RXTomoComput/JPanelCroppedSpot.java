/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput;

import com.soleil.nanoscopium.rximage.RXVirtualImage;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.DataController;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.BasicPanel;
import com.soleil.nanoscopium.rxtomoj.gui.utils.EditableNumericField;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Updatable;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.data.ReductionInfo;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.text.ParseException;

/**
 * Created by antoine bergamaschi on 27/01/14.
 */
public class JPanelCroppedSpot extends BasicPanel implements Updatable{

//    private Hdf5ImagePlus hdf5ImagePlus;
    private RXVirtualImage rxVirtualImage;
    private RoiPanel roiPanel;
    private RXTomoJViewController controller;
    private DetectorVisualizationOption option;



    private StackType current = StackType.ABSORPTION;
//    private boolean lockUpdate = false;

    public JPanelCroppedSpot(RXTomoJViewController controller){
        super(new GridBagLayout());
        this.controller = controller;

        Border raisedbevel = BorderFactory.createRaisedBevelBorder();
        this.setBorder(raisedbevel);

        this.roiPanel = new RoiPanel(this.controller);
        this.option = new DetectorVisualizationOption();

        this.setPreferredSize(this.getPreferredSize());

        Utils.addGridBag(GridBagConstraints.CENTER, margin_top_global, margin_left_global, margin_bottom_inside, margin_right_global, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, this, this.roiPanel);
        Utils.addGridBag(GridBagConstraints.CENTER, margin_top_inside, margin_left_global, margin_bottom_global, margin_right_global, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 1, this, this.option);


        RXTomoJ.getInstance().getModelController().addMMXIControllerUpdatedListener(l -> {
            switch (l.getEventID()) {
                case REDUCTIONINFO_TRANSMISSION_ROI:
                    update();
                    break;
            }
        });
    }

    public void setImage(RXVirtualImage rxVirtualImage){
        this.rxVirtualImage = rxVirtualImage;
        this.update();
    }



    @Override
    public void update(){
//        updateFilter();
        updateComputeSpot();
    }

    /**
     * Update the computeSpot interface
     */
    public void updateComputeSpot(){
            ReductionInfo reductionInfo = RXTomoJ.getInstance().getModelController().getDataController().getReductionInfo();
//            DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

            this.roiPanel.updateRoiPanel(reductionInfo.getTransmissionROI());
            this.option.update();
    }

    public void reset(){
//        if ( this.hdf5ImagePlus != null ) {
//            this.hdf5ImagePlus.close();
//        }
    }

    private class DetectorVisualizationOption extends JPanel{
        private ButtonContainer buttonContainer;
        private JPanelFilterSpot jPanelFilterSpot;

        private final String ABSORPTION = "abs", DARKFIELD = "dark";

        public DetectorVisualizationOption(){
            super(new GridBagLayout());
            this.build();
        }

        private void build(){

            TitledBorder titlePaneCheck = BorderFactory.createTitledBorder(controller.getRessourceValue("label_Title_rawData_showOption"));
            this.setBorder(titlePaneCheck);

            jPanelFilterSpot = new JPanelFilterSpot();

            buttonContainer = new ButtonContainer();
            buttonContainer.addLabel(controller.getRessourceValue("rawDataPanel_label_absorption"),controller.getRessourceValue("rawDataPanel_label_absorption_tooltips"),ABSORPTION);
            buttonContainer.addLabel(controller.getRessourceValue("rawDataPanel_label_darkfield"),controller.getRessourceValue("rawDataPanel_label_darkfield_tooltips"),DARKFIELD);

            buttonContainer.addBasicListener(()->{
                if ( buttonContainer.getCurrentSelectionID().equalsIgnoreCase(ABSORPTION)){
                    current = StackType.ABSORPTION;
                }else if( buttonContainer.getCurrentSelectionID().equalsIgnoreCase(DARKFIELD) ){
                    current = StackType.DARKFIELD;
                }
                jPanelFilterSpot.updateInterface();
//                updateFilter();
            });

            JButton validate = new JButton("Update");
            validate.addActionListener(l->{
                jPanelFilterSpot.validate_();
//                updateFilter();
            });

            Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,1,this,jPanelFilterSpot);

            Utils.addGridBag(GridBagConstraints.CENTER,5,30,5,30,GridBagConstraints.BOTH,1,1,0,0,1,1,0,2,this,validate);

            Utils.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,3,this,buttonContainer);

        }

        public void update(){
            jPanelFilterSpot.updateInterface();
            if ( current == StackType.DARKFIELD ){
                buttonContainer.init(DARKFIELD);
            }else{
                buttonContainer.init(ABSORPTION);
            }
        }
    }

    private class JPanelFilterSpot extends JPanel{

        //        private JCheckBox keepZero;
        private JCheckBox useFilter;
        private EditableNumericField radius;
        private EditableNumericField maxLoop;
        private EditableNumericField spreadCoef;
//        private JCheckBox doFilter;

        public JPanelFilterSpot(){
            super(new GridBagLayout());
            this.build();
            TitledBorder secondPaneTitle = BorderFactory.createTitledBorder(controller.getRessourceValue("JFrameSpotOption_title_filterOption"));
            setBorder(secondPaneTitle);
        }

        private void build(){

            useFilter = new JCheckBox(controller.getRessourceValue("JFrameSpotOption_JPanelFilterSpot_useFilter"));
            useFilter.setToolTipText(controller.getRessourceValue("JFrameSpotOption_JPanelFilterSpot_useFilter_tooltip"));

            useFilter.addActionListener(e->{
                maxLoop.setEnabled( useFilter.isSelected());
                radius.setEnabled( useFilter.isSelected());
                spreadCoef.setEnabled( useFilter.isSelected());
                if ( current == StackType.ABSORPTION ) {
                    RXTomoJ.getInstance().getModelController().getDataController().setDoFilter(useFilter.isSelected());
                }else{
                    RXTomoJ.getInstance().getModelController().getDataController().setDoFilterForDiffusion(useFilter.isSelected());
                }
            });

            JLabel radiusLabel = new JLabel(controller.getRessourceValue("JFrameSpotOption_JPanelFilterSpot_radius")+ " : ");
            radiusLabel.setToolTipText(controller.getRessourceValue("JFrameSpotOption_JPanelFilterSpot_radius_tooltip"));
            JLabel maxLoopLabel = new JLabel(controller.getRessourceValue("JFrameSpotOption_JPanelFilterSpot_iteration")+ " : ");
            maxLoopLabel.setToolTipText(controller.getRessourceValue("JFrameSpotOption_JPanelFilterSpot_iteration_tooltip"));
            JLabel spreadCoefLabel = new JLabel(controller.getRessourceValue("JFrameSpotOption_JPanelFilterSpot_spreadCoef")+ " : ");
            spreadCoefLabel.setToolTipText(controller.getRessourceValue("JFrameSpotOption_JPanelFilterSpot_spreadCoef_tooltip"));

            radius = Utils.createNumberTextField(0,-1,-1,null,null,null);
            maxLoop = Utils.createNumberTextField(0,-1,-1,null,null,null);
            spreadCoef = Utils.createNumberTextField(0,-1,-1,null,null,null);

            Utils.addGridBag(GridBagConstraints.CENTER, 5, 10, 5, 10, GridBagConstraints.BOTH, 1, 1, 0, 0, 2, 1, 0, 0, this, useFilter);
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 10, 5, 5, GridBagConstraints.NONE, 0, 1, 0, 0, 1, 1, 0, 1, this, radiusLabel);
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 10, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 1, this, radius);
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 10, 5, 5, GridBagConstraints.NONE, 0, 1, 0, 0, 1, 1, 0, 2, this, maxLoopLabel);
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 10, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 2, this, maxLoop);
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 10, 5, 5, GridBagConstraints.NONE, 0, 1, 0, 0, 1, 1, 0, 3, this, spreadCoefLabel);
            Utils.addGridBag(GridBagConstraints.CENTER, 5, 5, 5, 10, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 3, this, spreadCoef);

            updateInterface();
        }

        public void validate_(){
            try {
                DataController dataController  = RXTomoJ.getInstance().getModelController().getDataController();

                if ( current == StackType.ABSORPTION ) {
                    radius.commitEdit();
                    //Update the computeSpot in the model
                    dataController.setHotSpotDetectionRadius(Integer.parseInt(radius.getValue().toString()));
                    maxLoop.commitEdit();
                    //Update the computeSpot in the model
                    dataController.setHotSpotDetectionIterationNumber(Integer.parseInt(maxLoop.getValue().toString()));
                    spreadCoef.commitEdit();
                    //Update the computeSpot in the model
                    dataController.setHotSpotDetectionSpreadCoefficient(Integer.parseInt(spreadCoef.getValue().toString()));
                }else{
                    radius.commitEdit();
                    //Update the computeSpot in the model
                    dataController.setHotSpotDetectionRadiusForDiffusion(Integer.parseInt(radius.getValue().toString()));
                    maxLoop.commitEdit();
                    //Update the computeSpot in the model
                    dataController.setHotSpotDetectionIterationNumberForDiffustion(Integer.parseInt(maxLoop.getValue().toString()));
                    spreadCoef.commitEdit();
                    //Update the computeSpot in the model
                    dataController.setHotSpotDetectionSpreadCoefficientForDiffusion(Integer.parseInt(spreadCoef.getValue().toString()));
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        public void updateInterface(){
            ReductionInfo reductionInfo = RXTomoJ.getInstance().getModelController().getDataController().getReductionInfo();

            if ( current == StackType.DARKFIELD ){
                useFilter.setSelected(reductionInfo.isDoFilter_diffusion());
                maxLoop.setValue(reductionInfo.getNumberOfIteration_diffusion());
                radius.setValue(reductionInfo.getRadius_diffusion());
                spreadCoef.setValue(reductionInfo.getSpreadCoefficient_diffusion());

                maxLoop.setEnabled( reductionInfo.isDoFilter_diffusion());
                radius.setEnabled( reductionInfo.isDoFilter_diffusion());
                spreadCoef.setEnabled( reductionInfo.isDoFilter_diffusion());
            }else{
                useFilter.setSelected(reductionInfo.isDoFilter());
                maxLoop.setValue(reductionInfo.getNumberOfIteration());
                radius.setValue(reductionInfo.getRadius());
                spreadCoef.setValue(reductionInfo.getSpreadCoefficient());

                maxLoop.setEnabled( reductionInfo.isDoFilter());
                radius.setEnabled( reductionInfo.isDoFilter());
                spreadCoef.setEnabled( reductionInfo.isDoFilter());
            }
        }
    }

    public StackType getCurrent() {
        return current;
    }
}
