/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput;

import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.fluorescence.FluorescenceElement;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

/**
 * Created by bergamaschi on 25/03/2014.
 */
public class JPanelElements extends JPanel {
//    private JScrollPane jScrollPane;
    private JPanel elements;
    private RXTomoJViewController controller;

    public JPanelElements(RXTomoJViewController controller){
        super(new GridBagLayout());
        this.controller = controller;
        build();
    }

    private void build(){
        elements = new JPanel(new GridBagLayout());
//        JScrollPane jScrollPane = new JScrollPane(elements);
//        this.setPreferredSize(new Dimension(300, 200));
        Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, this, elements);

        TitledBorder titlePaneCheck = BorderFactory.createTitledBorder(controller.getRessourceValue("JPanelElements_title"));
        this.setBorder(titlePaneCheck);
    }


    private void createElement(FluorescenceElement fluorescenceElement){
        JLabel label = new JLabel(fluorescenceElement.getName());
        elements.add(label);
        Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, elements, label);
        this.revalidate();
    }

    public void update(){
        elements.removeAll();
        int i = 0;
        for ( FluorescenceElement fluorescenceElement : RXTomoJ.getInstance().getModelController().getDataController().getFluorescenceElements() ){
            JLabel label = new JLabel(fluorescenceElement.getName());
            elements.add(label);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, i, elements, label);
            i++;
        }
        this.revalidate();
    }
}
