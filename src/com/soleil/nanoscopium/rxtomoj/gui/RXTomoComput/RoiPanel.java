/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput;

import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.BasicPanel;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Updatable;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

/**
 * Created by bergamaschi on 28/04/2014.
 */
public class RoiPanel extends BasicPanel implements Updatable {

    private JLabel widthPanel;
    private JLabel heightPanel;
    private JLabel xPanel;
    private JLabel yPanel;
    private RXTomoJViewController controller;
    private JLabel sizePanel;
    private JLabel zPanel;
    private boolean lockUpdate = false;


//    private int[][] currentROI;

    private JSpinner editableSize;
    private JSpinner editablez;

    public static final int HORIZONTAL = 0, VERTICAL = 1;

    private int type = VERTICAL;

    public RoiPanel(RXTomoJViewController controller){
        super(new GridBagLayout());
        this.controller = controller;
        this.build();
    }

    public RoiPanel(RXTomoJViewController controller, int orientation){
        super(new GridBagLayout());
        lockUpdate = true;
        this.controller = controller;
        if ( orientation == HORIZONTAL || orientation == VERTICAL ){
            this.type = orientation;
        }
//        this.currentROI = roi;
        this.build();
        this.update();
        lockUpdate = false;
    }

    private void build(){
        TitledBorder titlePaneCheck = BorderFactory.createTitledBorder(controller.getRessourceValue("label_Title_rawData_RoiSpot"));
        this.setBorder(titlePaneCheck);

        JLabel wp = new JLabel(controller.getRessourceValue("width")+" : ");
        JLabel hp = new JLabel(controller.getRessourceValue("height")+" : ");
        JLabel sp = new JLabel(controller.getRessourceValue("size")+" : ");
        JLabel x = new JLabel(controller.getRessourceValue("X")+" : ");
        JLabel y = new JLabel(controller.getRessourceValue("Y")+" : ");
        JLabel z = new JLabel(controller.getRessourceValue("Z")+" : ");


        editablez=new JSpinner(new SpinnerNumberModel(10,0,100,1));
        editablez.addChangeListener(l->this.fireUpdate());

        editableSize=new JSpinner(new SpinnerNumberModel(10,1,100,1));
        editableSize.addChangeListener(l->this.fireUpdate());


        this.widthPanel = new JLabel("");
        this.heightPanel = new JLabel("");
        this.xPanel = new JLabel("");
        this.yPanel = new JLabel("");
        this.zPanel = new JLabel("");
        this.sizePanel = new JLabel("");

        if ( type == VERTICAL ) {
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, this, x);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 0, this, xPanel);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 2, 0, this, wp);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 3, 0, this, widthPanel);

            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 1, this, y);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 1, this, yPanel);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 2, 1, this, hp);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 3, 1, this, heightPanel);

            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 2, this, z);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 2, this, zPanel);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 2, 2, this, sp);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 3, 2, this, sizePanel);
        }
        else if ( type == HORIZONTAL ) {
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, this, x);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 0, this, xPanel);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 1, this, wp);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 1, 1, this, widthPanel);

            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 2, 0, this, y);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 3, 0, this, yPanel);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 2, 1, this, hp);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 3, 1, this, heightPanel);

            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 4, 0, this, z);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 5, 0, this, editablez);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 4, 1, this, sp);
            Utils.addGridBag(GridBagConstraints.CENTER, 0, 0, 0, 0, GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 5, 1, this, editableSize);
        }
    }



    //TODO add support for Z dimension
    public void updateRoiPanel(int[][] roi){
        int w = roi[0][1] - roi[0][0];
        int h = roi[1][1] - roi[1][0];
        updateRoiPanel(new Rectangle(roi[0][0],roi[1][0],w,h));

        if ( roi.length == 3){
            this.editablez.setValue(roi[2][0]);
            this.editableSize.setValue(roi[2][1] - roi[2][0]);
//            updateSpinnerLimits(roi[2][1]);
        }
    }

    public void updateRoiPanel(Rectangle roi){
        this.xPanel.setText(String.valueOf((int)roi.getX()));
        this.yPanel.setText(String.valueOf((int)roi.getY()));

        this.widthPanel.setText(String.valueOf((int)roi.getWidth()));
        this.heightPanel.setText(String.valueOf((int)roi.getHeight()));
    }


    public void updateSpinnerLimits(int max){
        lockUpdate = true;
        if ( editableSize !=null && editablez !=null ) {
            ((SpinnerNumberModel) this.editableSize.getModel()).setMaximum(max);
            ((SpinnerNumberModel) this.editablez.getModel()).setMaximum(max);
        }
        lockUpdate = false;
    }

    public int[] getRoiLimit(){
        int[] roi = new int[6];
        roi[0] = Integer.parseInt(this.xPanel.getText());
        roi[1] = Integer.parseInt(this.widthPanel.getText()) + Integer.parseInt(this.xPanel.getText());
        roi[2] = Integer.parseInt(this.yPanel.getText());
        roi[3] = Integer.parseInt(this.heightPanel.getText()) + Integer.parseInt(this.yPanel.getText());
        roi[4] = (int) this.editablez.getValue();
        roi[5] = (int) this.editableSize.getValue() + (int) this.editablez.getValue();
        return roi;
    }



    @Override
    public void update() {
        System.err.println("No Update is done :: "+this.getClass().toString());
//        if ( this.currentROI != null ) {
//            lockUpdate = true;
//            this.widthPanel.setText(""+(this.currentROI[0][1]-this.currentROI[0][0]));
//            this.heightPanel.setText(""+(this.currentROI[1][1]-this.currentROI[1][0]));
//            this.xPanel.setText(""+this.currentROI[0][0]);
//            this.yPanel.setText(""+this.currentROI[1][0]);
//            this.editablez.setValue(this.currentROI[2][0]);
//            this.editableSize.setValue(this.currentROI[2][1]-this.currentROI[2][0]);
//            lockUpdate = false;
//        }
    }

    private void fireUpdate(){
        if ( !lockUpdate ) {
            fireBasicListener();
        }
    }
}