/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.gui;

import com.soleil.nanoscopium.rxtomoj.RXTomoJ;

import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.event.WindowListener;

/**
 * Created by antoine bergamaschi on 24/01/14.
 */
public class CustomWindowListener implements WindowListener, WindowFocusListener{
    private Window listenedWindow;

    public CustomWindowListener(Window listenObject){
        super();
        this.listenedWindow  = listenObject;
    }

    @Override
    public void windowOpened(WindowEvent e) {
//        System.err.println("windowOpened");
    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.err.println("windowClosing");
        //TODO add custum dispose function ?
        listenedWindow.dispose();

        RXTomoJ.getInstance().getModelController().getSessionController().saveSessionObject();
        //Completely close the program
        System.exit(0);
    }

    @Override
    public void windowClosed(WindowEvent e) {
//        System.err.println("windowClosed");
    }

    @Override
    public void windowIconified(WindowEvent e) {
//        System.err.println("windowIconified");
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
//        System.err.println("windowDeiconified");
    }

    @Override
    public void windowActivated(WindowEvent e) {
//        System.err.println("windowActivated");
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
//        System.err.println("windowDeactivated");
    }

    @Override
    public void windowGainedFocus(WindowEvent e) {

    }

    @Override
    public void windowLostFocus(WindowEvent e) {

    }
}
