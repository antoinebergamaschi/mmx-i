/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.fluorescence;


import com.soleil.nanoscopium.rximage.RXSpectrum;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.IOController;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;


import javax.xml.bind.annotation.XmlTransient;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by antoine bergamaschi on 26/04/2015.
 */
public final class FluorescenceObject {
    /**
     * Path of this Spectrum in the Hdf5 file
     */
    private String path;

    /**
     * Path to a save Mean Spectrum
     */
    private String meanSpectrumPath;

    //Calibration
    private final float[] calibrationPeak_channel = new float[2];
    private final float[] calibrationPeak_energy = new float[2];

    @XmlTransient
    private RXSpectrum meanSpectrum = new RXSpectrum();

    private boolean automatedComputation = false;

    public FluorescenceObject(){

    }

    public FluorescenceObject(FluorescenceObject old){
        this.copy(old);
    }

    public FluorescenceObject(String path){
        this.path = path;
    }

    private void copy(FluorescenceObject objToCopy){
        this.path = objToCopy.getPath();
        System.arraycopy(objToCopy.getCalibrationPeak_channel(),0,this.calibrationPeak_channel,0,this.calibrationPeak_channel.length);
        System.arraycopy(objToCopy.getCalibrationPeak_energy(), 0, this.calibrationPeak_energy, 0, this.calibrationPeak_energy.length);
//        this.setMeanSpectrum(new RXSpectrum(objToCopy.getMeanSpectrum()));
        this.setMeanSpectrumPath(objToCopy.getMeanSpectrumPath());
        this.setAutomatedComputation(objToCopy.isAutomatedComputation());
    }

    public boolean isCalibrate(){
        return this.calibrationPeak_channel[0] !=  0 && this.calibrationPeak_channel[1] != 0 && this.calibrationPeak_channel[0] !=  this.calibrationPeak_channel[1];
    }

    /**
     * Lineare extrapolation of the energy based on the calibration peaks and energy
     * @param valueToConvert value to be converted from position to Kev
     * @return float the converted value in Kev
     */
    public float convertValue(float valueToConvert){
        float x = (calibrationPeak_energy[1] - calibrationPeak_energy[0])/(calibrationPeak_channel[1] - calibrationPeak_channel[0]);
        float a = calibrationPeak_energy[0] - calibrationPeak_channel[0]*x;

        float convertedValue = valueToConvert*x + a;

        return convertedValue;
    }

    public int inverseConvertValue(float valueToConvert){
        float x = (calibrationPeak_energy[1] - calibrationPeak_energy[0])/(calibrationPeak_channel[1] - calibrationPeak_channel[0]);
        float a = calibrationPeak_energy[0] - calibrationPeak_channel[0]*x;

//        float convertedValue = valueToConvert*x + a;
        int convertedValue = (int)((valueToConvert - a)/x);

        return convertedValue;
    }


    public float[] getCalibrationPeak_channel() {
        return calibrationPeak_channel;
    }

    public float[] getCalibrationPeak_energy() {
        return calibrationPeak_energy;
    }

    public void setCalibrationPeak_channel(float[] toSet){
        System.arraycopy(toSet,0,this.calibrationPeak_channel,0,this.calibrationPeak_channel.length);
    }

    public void setCalibrationPeak_energy(float[] toSet){
        System.arraycopy(toSet,0,this.calibrationPeak_energy,0,this.calibrationPeak_energy.length);
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public RXSpectrum getMeanSpectrum() {
        return meanSpectrum;
    }

    public void setMeanSpectrum(RXSpectrum data) {
        this.meanSpectrum = data;
    }

    public void setMeanSpectrumData(float[] data){
        this.meanSpectrum.setData(data);
    }

    public boolean isAutomatedComputation() {
        return automatedComputation;
    }

    public void setAutomatedComputation(boolean automatedComputation) {
        this.automatedComputation = automatedComputation;
    }

    public String getMeanSpectrumPath() {
        return meanSpectrumPath;
    }

    public void setMeanSpectrumPath(String meanSpectrumPath) {
        if ( meanSpectrumPath != null && Files.exists(Paths.get(meanSpectrumPath)) ){
            IOController ioController = RXTomoJ.getInstance().getModelController().getIoController();
            this.meanSpectrum = (RXSpectrum) ioController.laodStack(meanSpectrumPath, ComputationUtils.StackType.SUM_SPECTRA);
            this.meanSpectrumPath = meanSpectrumPath;
        }
    }
}
