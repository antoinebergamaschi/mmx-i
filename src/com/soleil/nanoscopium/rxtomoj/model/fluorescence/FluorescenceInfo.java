/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.fluorescence;

import java.util.ArrayList;

/**
 * Created by antoine bergamaschi on 20/03/2014.
 */
public class FluorescenceInfo {

    /**
     * The List of every elements selected by the user
     */
    private ArrayList<FluorescenceElement> elements = new ArrayList<>();

    /**
     * The List of the Channel and data associted used to compute fluorescence
     */
    private ArrayList<FluorescenceObject> fluoData = new ArrayList<>();

    /**
     * Default constructor
     */
    public FluorescenceInfo(){
    }

    public FluorescenceInfo(FluorescenceInfo obj){
        this.copy(obj);
    }


    private void copy(FluorescenceInfo objToCopy) {
        this.setElements(new ArrayList<>(objToCopy.getElements()));
        this.setFluoData(new ArrayList<>(objToCopy.getFluoData()));
    }

    public boolean addElement(FluorescenceElement fluorescenceElement){
        boolean add = true;
        if ( fluorescenceElement.getName() != null  ) {
            for (FluorescenceElement element : elements) {
                //Remove test with Type for Test with Name
                //            if ( element.getElementType() == fluorescenceElement.getElementType() ){
                //                System.err.println("This Elements Already exist in the model : "+this.getClass().toString());
                //                add = false;
                //                break;
                //            }
                if (element.getName().equals(fluorescenceElement.getName())) {
                    System.err.println("This Elements Already exist in the model : " + this.getClass().toString());
                    add = false;
                    break;
                }
            }
        }
        if ( add  ) {
            this.elements.add(fluorescenceElement);
        }
        return add;
    }

    public void removeElement(int type){
        boolean remove = false;
        for ( int index = 0 ;  index < this.elements.size() ; index++ ){
            if ( elements.get(index).getElementType() == type ){
                this.elements.remove(index);
                remove = true;
                break;
            }
        }
        if ( !remove  ) {
            System.err.println("This Elements does not exist in the model : " + this.getClass().toString());
        }
    }

    public void removeElement(String name){
        boolean remove = false;
        for ( int index = 0 ;  index < this.elements.size() ; index++ ){
            if ( elements.get(index).getName().equals(name) ){
                this.elements.remove(index);
                remove = true;
                break;
            }
        }
        if ( !remove  ) {
            System.err.println("This Elements does not exist in the model : "+this.getClass().toString());
        }
    }

    public void removeElement(FluorescenceElement element){
        elements.remove(element);
    }


    public ArrayList<FluorescenceElement> getElements() {
        return elements;
    }

    public void setElements(ArrayList<FluorescenceElement> elements) {
        this.elements = elements;
    }

    public int isUniqueName(String name){
        int unique = -1;
        if ( name != null && !name.equals("") ){
            for ( FluorescenceElement fluorescenceElement : this.elements ){
                if (  fluorescenceElement.getName() != null && fluorescenceElement.getName().equals(name) ){
                    unique++;
                }
            }
        }
        return unique;
    }

    public FluorescenceElement getElement(String fluorescenceElementName) {
        FluorescenceElement element = null;
        for ( int index = 0 ;  index < this.elements.size() ; index++ ){
            if ( elements.get(index).getName().equalsIgnoreCase(fluorescenceElementName) ){
                element = elements.get(index);
                break;
            }
        }
        return element;
    }

    public FluorescenceObject getFluoData(String path){
        FluorescenceObject object = null;

        for (FluorescenceObject fluorescenceObject : fluoData ){
            if ( fluorescenceObject.getPath().equalsIgnoreCase(path) ){
                object = fluorescenceObject;
                break;
            }
        }

        return object;
    }

    public ArrayList<FluorescenceObject> getFluoData() {
        return fluoData;
    }

    public void setFluoData(ArrayList<FluorescenceObject> fluoData) {
        this.fluoData = fluoData;
//        for ( FluorescenceObject fluorescenceObject : fluoData ){
//            this.fluoData.add(new FluorescenceObject(fluorescenceObject));
//        }
    }
}
