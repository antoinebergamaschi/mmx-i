/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.data;

import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;
import com.soleil.nanoscopium.rxtomoj.model.utils.StackDataStorageAdaptater;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by antoine bergamaschi on 26/04/2015.
 */
public class DataStorage {

    /**
     * ArrayList containing Every image currently saved in the model
     */
    private ConcurrentHashMap<StackType,ArrayList<StackData>> stackDataStorage = new ConcurrentHashMap<>();

    /**
     * HashMap containing the String path for the given StackType
     */
    private ConcurrentHashMap<StackType,String> stackPath = new ConcurrentHashMap<>();

    //SPC -> NAME / PATH
    /**
     * HashMap dedicated for the fluorescence Path. The SPC dataset name is the key to each elements path
     */
    private ConcurrentHashMap<String, MultiplePath> fluoPath = new ConcurrentHashMap<>();

    /**
     * Same that fluoPath but more general, used for darkfield
     */
    private ConcurrentHashMap<StackType,MultiplePath> stackPaths = new ConcurrentHashMap<>();


    public DataStorage(){

    }

    /**
     * Do a Hard copy of this ParameterStorage
     * @param oldStorage the Old parameter storage to be replaced
     */
    public DataStorage (DataStorage oldStorage){
        super();
        this.setStackPaths(new ConcurrentHashMap<>(oldStorage.getStackPaths()));
        this.setStackPath(new ConcurrentHashMap<>(oldStorage.getStackPath()));
        this.setFluoPath(new ConcurrentHashMap<>(oldStorage.getFluoPath()));

        this.setStackDataStorage(new ConcurrentHashMap<>(oldStorage.getStackDataStorage()));
    }

    @Deprecated
    public ConcurrentHashMap<StackType, String> getStackPath() {
        return stackPath;
    }

    @Deprecated
    public void setStackPath(ConcurrentHashMap<StackType, String> stackPath) {
        //Warning change the function to update the StackDataStorage and no more the old HashMaps

        File file = new File("");
        this.stackPath = new ConcurrentHashMap<>();

        //Add Warning/Errors message when the resource is not found
        for ( StackType stackType : stackPath.keySet() ){
            if ( stackType == null ){
                System.err.println("Warning :: Null StackType -> "+stackType);
            }
            else if ( !new File(stackPath.get(stackType)).exists() ){
                System.err.println("Warning :: Path to invalid File -> "+stackPath.get(stackType));
            }else{
//                this.stackPath.put(stackType,stackPath.get(stackType));
                //Warning Code change Add the retrieved data in the StackDataStorage instead of the old stackPath
                if ( this.stackDataStorage.containsKey(stackType) ){
                    this.stackDataStorage.get(stackType).add(new StackData(stackType,stackPath.get(stackType)));
                }else{
                    ArrayList<StackData> stackDatas = new ArrayList<>();
                    stackDatas.add(new StackData(stackType,stackPath.get(stackType)));
                    this.stackDataStorage.put(stackType,stackDatas);
                }
            }
        }
    }

    @Deprecated
    public ConcurrentHashMap<String,MultiplePath> getFluoPath() {
        return fluoPath;
    }

    @Deprecated
    public void setFluoPath(ConcurrentHashMap<String,MultiplePath> fluoPath) {
        //Warning change the function to update the StackDataStorage and no more the old HashMaps

//        this.fluoPath = fluoPath;
        File file = new File("");
        this.fluoPath = new ConcurrentHashMap<>();

        //Add Warning/Errors message when the resource is not found
        for ( String spc : fluoPath.keySet() ){
            if ( spc == null ){
                System.err.println("Warning :: Null StackType -> " + spc);
            }
            else{
                if ( fluoPath.get(spc).getMap() == null ){
                    System.err.println("Warning :: Null PathFluo");
                }else{
                    for ( String name : fluoPath.get(spc).getMap().keySet() ){
                        if ( name == null ){
                            System.err.println("Warning :: Null Name -> " + name);
                        }
                        else if ( !new File(fluoPath.get(spc).getMap().get(name)).exists() ){
                            System.err.println("Warning :: Path to invalid File -> "+fluoPath.get(spc).getMap().get(name));
                        }
                        else{
//                            if ( !this.fluoPath.containsKey(spc) ){
//                                this.fluoPath.put(spc, new MultiplePath(name, fluoPath.get(spc).getMap().get(name)));
//                            }
//                            else if ( !this.fluoPath.get(spc).getMap().containsKey(name) ){
//                                this.fluoPath.get(spc).getMap().put(name,fluoPath.get(spc).getMap().get(name));
//                            }


                            //Warning Code change Add the retrieved data in the StackDataStorage instead of the old stackPath
                            if ( this.stackDataStorage.containsKey(StackType.FLUORESCENCE) ){
                                StackData data = new StackData(StackType.FLUORESCENCE,fluoPath.get(spc).getMap().get(name));
                                data.setSpc(spc);
                                data.setAdditionalName(name);
                                this.stackDataStorage.get(StackType.FLUORESCENCE).add(data);
                            }else{
                                ArrayList<StackData> datas = new ArrayList<>();
                                StackData data = new StackData(StackType.FLUORESCENCE,fluoPath.get(spc).getMap().get(name));
                                data.setSpc(spc);
                                data.setAdditionalName(name);
                                datas.add(data);
                                this.stackDataStorage.put(StackType.FLUORESCENCE, datas);
                            }
                        }
                    }
                }
            }
        }
    }

    @Deprecated
    public ConcurrentHashMap<StackType, MultiplePath> getStackPaths() {
        return stackPaths;
    }

    @Deprecated
    public void setStackPaths(ConcurrentHashMap<StackType, MultiplePath> stackPaths) {
        //Warning change the function to update the StackDataStorage and no more the old HashMaps
        this.stackPaths = stackPaths;

        //Add Warning/Errors message when the resource is not found
        for ( StackType stackType : stackPaths.keySet() ){
            if ( stackType == null ){
                System.err.println("Warning :: Null StackType -> " + stackType);
            }
            else{
                if ( stackPaths.get(stackType).getMap() == null ){
                    System.err.println("Warning :: Null MultiPath");
                }else{
                    for ( String name : stackPaths.get(stackType).getMap().keySet() ){
                        if ( name == null ){
                            System.err.println("Warning :: Null Name -> "+name);
                        }
                        else if ( !new File(stackPaths.get(stackType).getMap().get(name)).exists() ){
                            System.err.println("Warning :: Path to invalid File -> "+stackPaths.get(stackType).getMap().get(name));
                        }
                        else{
//                            if ( !this.stackPaths.containsKey(stackType) ){
//                                this.stackPaths.put(stackType, new MultiplePath(name, stackPaths.get(stackType).getMap().get(name)));
//                            }
//                            else if ( !this.stackPaths.get(stackType).getMap().containsKey(name) ){
//                                this.stackPaths.get(stackType).getMap().put(name,stackPaths.get(stackType).getMap().get(name));
//                            }

//                          Warning Code change Add the retrieved data in the StackDataStorage instead of the old stackPath
                            if ( this.stackDataStorage.containsKey(stackType) ){
                                StackData data = new StackData(stackType,stackPaths.get(stackType).getMap().get(name));
                                data.setAdditionalName(name);
                                this.stackDataStorage.get(stackType).add(data);
                            }else{
                                ArrayList<StackData> datas = new ArrayList<>();
                                StackData data = new StackData(stackType,stackPaths.get(stackType).getMap().get(name));
                                data.setAdditionalName(name);
                                datas.add(data);
                                this.stackDataStorage.put(stackType, datas);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Test if this ArrayList of StackData already contains this path
     * @param stackDataArrayList The ArrayList containing the StackData to test
     * @param path The System path to find in the ArrayList of StackData
     * @return True if a stackData contains the given SystemPath
     */
    public static boolean contains(ArrayList<StackData> stackDataArrayList, String path){
        if ( stackDataArrayList != null ) {
            for (int i = 0; i < stackDataArrayList.size(); i++) {
                if (stackDataArrayList.get(i).getSystemPath().equalsIgnoreCase(path)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean containsSPC(ArrayList<StackData> stackDataArrayList, String spc){
        if ( stackDataArrayList != null ) {
            for (int i = 0; i < stackDataArrayList.size(); i++) {
                if (stackDataArrayList.get(i).getSpc().equalsIgnoreCase(spc)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static StackData get(ArrayList<StackData> stackDataArrayList, String path){
        if ( stackDataArrayList != null ) {
            for (int i = 0; i < stackDataArrayList.size(); i++) {
                if (stackDataArrayList.get(i).getSystemPath().equalsIgnoreCase(path)) {
                    return stackDataArrayList.get(i);
                }
            }
        }
        return null;
    }

    public static StackData getFromSpectrum(ArrayList<StackData> stackDataArrayList, String spcName){
        if ( stackDataArrayList != null ) {
            for (int i = 0; i < stackDataArrayList.size(); i++) {
                if (stackDataArrayList.get(i).getSpc().equalsIgnoreCase(spcName)) {
                    return stackDataArrayList.get(i);
                }
            }
        }
        return null;
    }

    public static boolean remove(ArrayList<StackData> stackDataArrayList, String path){
        if ( stackDataArrayList != null ) {
            for (int i = 0; i < stackDataArrayList.size(); i++) {
                if (stackDataArrayList.get(i).getSystemPath().equalsIgnoreCase(path)) {
                    stackDataArrayList.remove(i);
                    return true;
                }
            }
        }
        return false;
    }


    @XmlJavaTypeAdapter(StackDataStorageAdaptater.class)
    public ConcurrentHashMap<StackType, ArrayList<StackData>> getStackDataStorage() {
        return stackDataStorage;
    }

    public void setStackDataStorage(ConcurrentHashMap<StackType, ArrayList<StackData>> stackDataStorage) {
        this.stackDataStorage = new ConcurrentHashMap<>();
//        Filter the stackDataStorage, each dataStorage save here should be also save on the FileSystem else it remove
//        from the model
        for ( StackType stackType : stackDataStorage.keySet() ){
            if ( stackType == null ){
                System.err.println("Warning :: Null StackType -> " + stackType);
            }
            else{
                if ( stackDataStorage.get(stackType) == null ){
                    System.err.println("Warning :: Null Array of StackData");
                }else{
                    if ( stackDataStorage.get(stackType).size() > 0) {
                        for (StackData stackData : stackDataStorage.get(stackType)) {
                            if (stackData == null) {
                                System.err.println("Warning :: Null StackData -> " + stackData);
                            } else if (!new File(stackData.getSystemPath()).exists()) {
                                System.err.println("Warning :: Path to invalid File -> " + stackData.getSystemPath());
                            } else {
                                if (this.stackDataStorage.containsKey(stackType)) {
                                    this.stackDataStorage.get(stackType).add(stackData);
                                } else {
                                    ArrayList<StackData> stackDatas = new ArrayList<>();
                                    stackDatas.add(stackData);
                                    this.stackDataStorage.put(stackType, stackDatas);
                                }
                            }
                        }
                    }else{
                        stackDataStorage.remove(stackType);
                    }
                }
            }
        }
    }
}
