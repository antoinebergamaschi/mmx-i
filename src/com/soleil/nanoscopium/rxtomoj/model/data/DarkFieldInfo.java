/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.data;

import com.soleil.nanoscopium.rxtomoj.model.computationFunction.ComputeDarkField.OrientationFitFunction;

import java.util.HashMap;

/**
 * Created by bergamaschi on 02/06/2015.
 */
public class DarkFieldInfo extends AbstractInfo {

    /**
     * Number of segment in which every range will be divided to retrieve the orientation of the darkField
     */
    private int numberOfSegment = 8;

    /**
     * The Function used to compute a Sine fit on the data
     */
    private OrientationFitFunction fitFunction = OrientationFitFunction.HALF;

    /**
     * Perform or not the darkfield orientation function
     */
    private boolean doOrientationComputation = false;

    /**
     * ID and range {fromX,toX,fromY,toY} normalize to 0 being the center of the spot
     */
    private HashMap<String , int[]> darkArea = new HashMap<>();

    /**
     * The Spot Geometry Type
     */
    private SpotGeometry integrationType = SpotGeometry.CIRCLE;

    /**
     * Enum type of DarkField integration
     */
    public enum SpotGeometry{
        CIRCLE, SQUARE
    }

    public DarkFieldInfo(){

    }

    public DarkFieldInfo(DarkFieldInfo old){
        copy(old);
    }

    private void copy(DarkFieldInfo old) {
        this.setDarkArea(new HashMap<>(old.getDarkArea()));
        this.setNumberOfSegment(old.getNumberOfSegment());
        this.setIntegrationType(old.getIntegrationType());
        this.setFitFunction(old.getFitFunction());
        this.setDoOrientationComputation(old.isDoOrientationComputation());
    }

    public static boolean isCorrectRange(int[] range){
        return range.length == 4 && range[0]<range[1] && range[0]>=0 &&
               range[2]<range[3] && range[2] >= 0;
    }

    public HashMap<String,int[]> getDarkArea() {
        return darkArea;
    }

    public void setDarkArea(HashMap<String,int[]> darkArea) {
        //Test the data
        for ( String key : darkArea.keySet() ){
            int[] newRange = new int[]{0,10,0,10};
            int[] range = darkArea.get(key);

            System.arraycopy(range,0,newRange,0,range.length);
            if ( !isCorrectRange(newRange) ){
                newRange = new int[]{0,10,0,10};
            }

            this.darkArea.put(key,newRange);
        }
    }

    public int getNumberOfSegment() {
        return numberOfSegment;
    }

    public void setNumberOfSegment(int numberOfSegment) {
        this.numberOfSegment = numberOfSegment;
    }

    public SpotGeometry getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(SpotGeometry integrationType) {
        this.integrationType = integrationType;
    }

    public OrientationFitFunction getFitFunction() {
        return fitFunction;
    }

    public void setFitFunction(OrientationFitFunction fitFunction) {
        this.fitFunction = fitFunction;
    }

    public boolean isDoOrientationComputation() {
        return doOrientationComputation;
    }

    public void setDoOrientationComputation(boolean doOrientationComputation) {
        this.doOrientationComputation = doOrientationComputation;
    }
}
