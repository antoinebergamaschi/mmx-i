/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.data;

import java.util.ArrayList;

/**
 * Created by bergamaschi on 25/11/2015.
 */
public class GlobalInfo extends AbstractInfo {

    /**
     * Contains the DataSet which are selected to be computed in the reduction process
     */
    private ArrayList<StackData> stackTypesToReduce = new ArrayList<>();

    /**
     * Contains the StackTypes to reconstruct
     */
    private ArrayList<StackData> stackTypesToReconstruct = new ArrayList<>();

    public GlobalInfo(){}

    public GlobalInfo(GlobalInfo old){
        this.setStackTypesToReduce(new ArrayList<>(old.getStackTypesToReduce()));
        this.setStackTypesToReconstruct(new ArrayList<>(old.getStackTypesToReconstruct()));
    }

    public ArrayList<StackData> getStackTypesToReduce() {
        return stackTypesToReduce;
    }

    public void setStackTypesToReduce(ArrayList<StackData> stackTypesToReduce) {
        this.stackTypesToReduce = stackTypesToReduce;
    }

    public ArrayList<StackData> getStackTypesToReconstruct() {
        return stackTypesToReconstruct;
    }

    public void setStackTypesToReconstruct(ArrayList<StackData> stackTypesToReconstruct) {
        this.stackTypesToReconstruct = stackTypesToReconstruct;
    }
}
