/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.data;

import java.util.Arrays;

/**
 * Created by bergamaschi on 27/10/2015.
 */
public class ReductionInfo extends AbstractInfo {
    //Read Parameter
    /**
     * Number of time the entire smallest size of dataset, will be read
     * in one read call to the file.
     *
     * Named numberOfLine because in Images this correspond to a whole line repeated x times
     */
    private int numberOfLine = 1;

    /**
     * The Size of the spool of Arrays given in Hdf5Opener Input
     */
    private int numberOfDataReaded = 5;

    /**
     * Spacing between each consecutive pixel in each dimension
     * ( Used for fast computation )
     */
    private int pixelSpacing = 20;

    /**
     * Use the pixelSpacing option ( Used only for low computation )
     */
    private boolean useLowResolution = false;

    //Spot detection parameter
    /**
     * percent of minimum presence of a pixel to be taken has account for the spot
     */
    private float spotFilter = 0.9f;

    /**
     * When looking for the spot, the minimal pixel value of the spot
     * (set to 1 to avoid looking in background pixels)
     */
    private float minimalPixelValue = 5.0f;

    /**
     * The minimal contiguous pixel accepted to mark this pixel has been may be part of the spot
     */
    private int minimalPixelNumber = 3;

    //HotSpot Detection parameters

    //Parameters for Absorption
    /**
     * Maximum number of iteration to perform with the HotSpot pixel retrieval function
     * Iteration may be stop if no hot pixel is detected anymore
     */
    private int numberOfIteration = 1;
    /**
     * Distance of the kernel used in the hot Spot pixel retrieval function.
     * The kernel is a variance one
     */
    private int radius = 2;
    /**
     * The Histogram of variance spreadCoefficient to apply threshold.
     * This means that every pixel in the histogram with a value separated from others by more than 3 "0"
     * is considered to be hot spot.
     */
    private int spreadCoefficient = 10;

    //Parameter fo diffusion
    /**
     * Maximum number of iteration to perform with the HotSpot pixel retrieval function
     * Iteration may be stop if no hot pixel is detected anymore
     */
    private int numberOfIteration_diffusion = 1;
    /**
     * Distance of the kernel used in the hot Spot pixel retrieval function.
     * The kernel is a variance one
     */
    private int radius_diffusion = 2;
    /**
     * The Histogram of variance spreadCoefficient to apply threshold.
     * This means that every pixel in the histogram with a value separated from others by more than 3 "0"
     * is considered to be hot spot.
     */
    private int spreadCoefficient_diffusion = 6;

    /**
     * Performs the filtering operation on the raw data
     */
    private boolean doFilter_diffusion = true;
    private boolean doFilter = true;

    //Image limits parameter
    /**
     * The ROI for transmission modalities such as Abs, PhaseContrast.
     * The coordinate are given in the Map
     * Only for 2D detectors
     */
    private int[][] transmissionROI = new int[2][2];

    /**
     * Maximum width and Height of the spot map Image.
     * Only in case Of 2D detectors
     * {width,height}
     */
    private int[] maxSpotDimension = new int[2];

    /**
     * Maximum width and Height of the final scan Image.
     * This is used to reset the reductionRoi parameters in some cases
     * {{xMin,xMax},{yMin,yMax},{zMin,zMax}}
     */
    private int[][] maxImageDimension = new int[3][2];

    /**
     * Reduction ROI. Region of interest inside the finally image which will be reconstruct.
     * {{xMin,xMax},{yMin,yMax},{zMin,zMax}}
     */
    private int[][] reductionROI = new int[3][2];

    /**
     * Whenever the image dataset is issue from a 2D detector or not.
     * In case not, roiSpot have to be skipped
     */
    private boolean is2Ddetector = true;


    public ReductionInfo(){
        super();
    }

    public ReductionInfo(ReductionInfo old){
        this.setNumberOfDataReaded(old.getNumberOfDataReaded());
        this.setNumberOfLine(old.getNumberOfLine());

        this.setMinimalPixelNumber(old.getMinimalPixelNumber());
        this.setMinimalPixelValue(old.getMinimalPixelValue());
        this.setSpotFilter(old.getSpotFilter());
        this.setUseLowResolution(old.isUseLowResolution());
        this.setPixelSpacing(old.getPixelSpacing());

        this.setMaxSpotDimension(old.getMaxSpotDimension());
        this.setMaxImageDimension(old.getMaxImageDimension());
        this.setReductionROI(old.getReductionROI());
        this.setTransmissionROI(old.getTransmissionROI());
        this.setIs2Ddetector(old.is2Ddetector());

        this.setNumberOfIteration(old.getNumberOfIteration());
        this.setSpreadCoefficient(old.getSpreadCoefficient());
        this.setRadius(old.getRadius());
        this.setDoFilter(old.isDoFilter());

        this.setNumberOfIteration_diffusion(old.getNumberOfIteration_diffusion());
        this.setSpreadCoefficient_diffusion(old.getSpreadCoefficient_diffusion());
        this.setRadius_diffusion(old.getRadius_diffusion());
        this.setDoFilter_diffusion(old.isDoFilter_diffusion());
    }

    public int getNumberOfLine() {
        return numberOfLine;
    }

    public void setNumberOfLine(int numberOfLine) {
        this.numberOfLine = numberOfLine;
    }

    public float getSpotFilter() {
        return spotFilter;
    }

    public void setSpotFilter(float spotFilter) {
        this.spotFilter = spotFilter;
    }

    public float getMinimalPixelValue() {
        return minimalPixelValue;
    }

    public void setMinimalPixelValue(float minimalPixelValue) {
        this.minimalPixelValue = minimalPixelValue;
    }

    public int getMinimalPixelNumber() {
        return minimalPixelNumber;
    }

    public void setMinimalPixelNumber(int minimalPixelNumber) {
        this.minimalPixelNumber = minimalPixelNumber;
    }

    public int[] getMaxSpotDimension() {
        return maxSpotDimension;
    }

    public void setMaxSpotDimension(int[] maxSpotDimension) {
        System.arraycopy(maxSpotDimension,0,this.maxSpotDimension,0,2);
    }

    public int[][] getReductionROI() {
        return reductionROI;
    }

    public void setReductionROI(int[][] reductionROI) {
        for ( int i = 0 ; i < this.reductionROI.length ; i++ ){
            System.arraycopy(reductionROI[i],0,this.reductionROI[i],0,2);
        }
    }

    public int getNumberOfDataReaded() {
        return numberOfDataReaded;
    }

    public void setNumberOfDataReaded(int numberOfDataReaded) {
        this.numberOfDataReaded = numberOfDataReaded;
    }

    public int[][] getTransmissionROI() {
        return transmissionROI;
    }

    public void setTransmissionROI(int[][] transmissionROI) {
        for ( int i = 0 ; i < this.transmissionROI.length ; i++ ){
            System.arraycopy(transmissionROI[i],0,this.transmissionROI[i],0,2);
        }
    }

    public int getPixelSpacing() {
        return pixelSpacing;
    }

    public void setPixelSpacing(int pixelSpacing) {
        this.pixelSpacing = pixelSpacing;
    }

    public boolean isUseLowResolution() {
        return useLowResolution;
    }

    public void setUseLowResolution(boolean useLowResolution) {
        this.useLowResolution = useLowResolution;
    }

    public int getSpreadCoefficient() {
        return spreadCoefficient;
    }

    public void setSpreadCoefficient(int spreadCoefficient) {
        this.spreadCoefficient = spreadCoefficient;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getNumberOfIteration() {
        return numberOfIteration;
    }

    public void setNumberOfIteration(int numberOfIteration) {
        this.numberOfIteration = numberOfIteration;
    }

    public int[][] getMaxImageDimension() {
        return maxImageDimension;
    }

    public void setMaxImageDimension(int[][] maxImageDimension) {
        for ( int i = 0 ; i < this.maxImageDimension.length ; i++ ){
            System.arraycopy(maxImageDimension[i],0,this.maxImageDimension[i],0,2);
        }
    }

    public boolean is2Ddetector() {
        return is2Ddetector;
    }

    public void setIs2Ddetector(boolean is2Ddetector) {
        this.is2Ddetector = is2Ddetector;
    }

    public int getNumberOfIteration_diffusion() {
        return numberOfIteration_diffusion;
    }

    public void setNumberOfIteration_diffusion(int numberOfIteration_diffusion) {
        this.numberOfIteration_diffusion = numberOfIteration_diffusion;
    }

    public int getRadius_diffusion() {
        return radius_diffusion;
    }

    public void setRadius_diffusion(int radius_diffusion) {
        this.radius_diffusion = radius_diffusion;
    }

    public int getSpreadCoefficient_diffusion() {
        return spreadCoefficient_diffusion;
    }

    public void setSpreadCoefficient_diffusion(int spreadCoefficient_diffusion) {
        this.spreadCoefficient_diffusion = spreadCoefficient_diffusion;
    }

    public boolean isDoFilter_diffusion() {
        return doFilter_diffusion;
    }

    public void setDoFilter_diffusion(boolean doFilter_diffusion) {
        this.doFilter_diffusion = doFilter_diffusion;
    }

    public boolean isDoFilter() {
        return doFilter;
    }

    public void setDoFilter(boolean doFilter) {
        this.doFilter = doFilter;
    }
}
