/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.data;

import com.soleil.nanoscopium.rxtomoj.model.fluorescence.FluorescenceInfo;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;

/**
 * @author Antoine Bergamaschi
 */
public class ParameterStorage {
    private static ParameterStorage instance;

    public GlobalInfo getGlobalInfo() {
        return globalInfo;
    }

    public void setGlobalInfo(GlobalInfo globalInfo) {
        this.globalInfo = globalInfo;
    }

    public enum InfoByType{
        COMPUTE_SPOT,ALGEBRAIC_RECONSTRUCTION,FILTERED_RECONSTRUCTION,FLUORESCENCE_INFO,DARKFIELD_INFO,PHASE_INFO,RECONSTRUCTION_INFO,SHIFTIMAGE_INFO;

        public AbstractInfo getParameter(){
            switch (this){
                case RECONSTRUCTION_INFO:
                    return instance.getDarkFieldInfo();
                case COMPUTE_SPOT:
                    break;
                case ALGEBRAIC_RECONSTRUCTION:
                    break;
                case FILTERED_RECONSTRUCTION:
                    break;
                case FLUORESCENCE_INFO:
                    break;
                case DARKFIELD_INFO:
                    return instance.getDarkFieldInfo();
                case PHASE_INFO:
                    return instance.getPhaseContrastInfo();
                case SHIFTIMAGE_INFO:
                    return instance.getShiftImageInfo();
            }

            System.err.println("Warning non Parameters Fund (not implemented) "+this.getClass().toString());
            return null;
        }
    }

    /**
     * Contains the parameters to reconstruct fluorescence like every elements to reconstruct and also every
     * Chanel with there owne calibration
     */
    private FluorescenceInfo fluorescenceInfo = new FluorescenceInfo();
    /**
     * Contains the parameters used by the phaseContrast Algorithms
     */
    private PhaseContrastInfo phaseContrastInfo = new PhaseContrastInfo();
    /**
     * Contains the range to be use to reconstruct DarkField Image and additional parameters
     */
    private DarkFieldInfo darkFieldInfo = new DarkFieldInfo();
    /**
     * Contains basic parameters for reconstruction execution
     */
    private ReconstructionInfo reconstructionInfo = new ReconstructionInfo();

    /**
     * Contains Data used by the ShiftImage class
     */
    private ShiftImageInfo shiftImageInfo = new ShiftImageInfo();

    /**
     * Contains Parameters to compute Spot and reduction image
     */
    private ReductionInfo reductionInfo = new ReductionInfo();

    /**
     * Contains Parameters to normalize the reduced data
     */
    private NormalizationInfo normalizationInfo = new NormalizationInfo();

    /**
     * Contains info used  by the computation function ( in case of no parameters )
     */
    private GlobalInfo globalInfo = new GlobalInfo();

    /**
     * The default modality to use when computing either the background reduction or sinus fit
     */
    private StackData defaultModality = null;

    public ParameterStorage(){
        super();
        instance = this;
    }

    /**
     * Do a Hard copy of this ParameterStorage
     * @param oldStorage the Old parameter storage to be replaced
     */
    public ParameterStorage (ParameterStorage oldStorage){
        super();

        this.setFluorescenceInfo(new FluorescenceInfo(oldStorage.getFluorescenceInfo()));
        this.setDarkFieldInfo(new DarkFieldInfo(oldStorage.getDarkFieldInfo()));
        this.setPhaseContrastInfo(new PhaseContrastInfo(oldStorage.getPhaseContrastInfo()));
        this.setReconstructionInfo(new ReconstructionInfo(oldStorage.getReconstructionInfo()));
        this.setReductionInfo(new ReductionInfo(oldStorage.getReductionInfo()));
        this.setNormalizationInfo(new NormalizationInfo(oldStorage.getNormalizationInfo()));

        this.setGlobalInfo(new GlobalInfo(oldStorage.getGlobalInfo()));
        this.setDefaultData(oldStorage.getDefaultData());
    }

    public FluorescenceInfo getFluorescenceInfo() {
        return fluorescenceInfo;
    }

    public void setFluorescenceInfo(FluorescenceInfo fluorescenceInfo) {
        this.fluorescenceInfo = fluorescenceInfo;
    }


    public PhaseContrastInfo getPhaseContrastInfo() {
        return phaseContrastInfo;
    }

    public void setPhaseContrastInfo(PhaseContrastInfo phaseContrastInfo) {
        this.phaseContrastInfo = phaseContrastInfo;
    }

    public DarkFieldInfo getDarkFieldInfo() {
        return darkFieldInfo;
    }

    public void setDarkFieldInfo(DarkFieldInfo darkFieldInfo) {
        this.darkFieldInfo = darkFieldInfo;
    }

    public ReconstructionInfo getReconstructionInfo() {
        return reconstructionInfo;
    }

    public void setReconstructionInfo(ReconstructionInfo reconstructionInfo) {
        this.reconstructionInfo = reconstructionInfo;
    }

    public ShiftImageInfo getShiftImageInfo() {
        return shiftImageInfo;
    }

    public void setShiftImageInfo(ShiftImageInfo shiftImageInfo) {
        this.shiftImageInfo = shiftImageInfo;
    }

    public StackData getDefaultData() {
        return defaultModality;
    }

    public void setDefaultData(StackData defaultModality) {
        this.defaultModality = defaultModality;
    }

    public ReductionInfo getReductionInfo() {
        return reductionInfo;
    }

    public void setReductionInfo(ReductionInfo reductionInfo) {
        this.reductionInfo = reductionInfo;
    }

    public NormalizationInfo getNormalizationInfo() {
        return normalizationInfo;
    }

    public void setNormalizationInfo(NormalizationInfo normalizationInfo) {
        this.normalizationInfo = normalizationInfo;
    }

}
