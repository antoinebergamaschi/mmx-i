/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.data;

import com.soleil.nanoscopium.rxtomoj.model.filters.ProjectionFilter.WindowType;
import com.soleil.nanoscopium.rxtomoj.model.filters.ProjectionFilter.FilterType;
import com.soleil.nanoscopium.rxtomoj.model.reconstructionFunction.BasicReconstructionFunction;
import com.soleil.nanoscopium.rxtomoj.model.reconstructionFunction.BasicReconstructionFunction.ProjectionMethod;

/**
 * Created by bergamaschi on 24/08/2015.
 */
public class ReconstructionInfo extends AbstractInfo {

    //Basic Reconstruction information
    /**
     * Sampling use on the projection direction
     */
    private int numberOfLine = 10;

    /**
     * The current center of rotation in X/Y position
     */
    private double centerOfRotationX = 0;
    private double centerOfRotationY = 0;


    /**
     * Method of projection/Backprojection to use
     */
    private ProjectionMethod projectionMethod = ProjectionMethod.RAYTRACING;

    //Info WBP
    /**
     * The Filter to apply in fourrier Space
     */
    private FilterType filterType = FilterType.RAMP;
    private WindowType windowType = WindowType.HAMMING;

    /**
     * Set the dimension of the windowType to use
     */
    private float windowDimension = 200;

    /**
     * Set if filter has to be used or not
     */
    private boolean useFilter = false;

    //Info for iterative reconstruction
    /**
     * The current relaxationCoefficient
     */
    private double coefOfRelaxation = 0.1;

    /**
     * The number of iteration
     */
    private int numberOfIteration = 3;

    /**
     * Use Background Normalization
     */
    private boolean useBgNormalization = false;

    /**
     * Coef of divergence for the computation of the background normalization
     */
    private float k = 2.0f;

    /**
     * Add positivity constraint
     */
    private boolean usePositivityContrainst = false;

    public ReconstructionInfo(){
        super();
    }

    public ReconstructionInfo(ReconstructionInfo old){
        super();
        this.setNumberOfLine(old.getNumberOfLine());
        this.setCenterOfRotationX(old.getCenterOfRotationX());

        this.setProjectionMethod(old.getProjectionMethod());
        this.setUseBgNormalization(old.isUseBgNormalization());
        this.setUsePositivityContrainst(old.isUsePositivityContrainst());
        this.setNumberOfIteration(old.getNumberOfIteration());
        this.setCoefOfRelaxation(old.getCoefOfRelaxation());
        this.setK(old.getK());
        this.setFilterType(old.getFilterType());
        this.setWindowType(old.getWindowType());
        this.setUseFilter(old.isUseFilter());
        this.setWindowDimension(old.getWindowDimension());
    }


    public int getNumberOfLine() {
        return numberOfLine;
    }

    public void setNumberOfLine(int numberOfLine) {
        this.numberOfLine = numberOfLine;
    }

    public double getCenterOfRotationX() {
        return centerOfRotationX;
    }

    public void setCenterOfRotationX(double centerOfRotationX) {
        this.centerOfRotationX = centerOfRotationX;
    }

    public double getCenterOfRotationY() {
        return centerOfRotationY;
    }

    public void setCenterOfRotationY(double centerOfRotationY) {
        this.centerOfRotationY = centerOfRotationY;
    }

    public ProjectionMethod getProjectionMethod() {
        return projectionMethod;
    }

    public void setProjectionMethod(ProjectionMethod projectionMethod) {
        this.projectionMethod = projectionMethod;
    }

    public double getCoefOfRelaxation() {
        return coefOfRelaxation;
    }

    public void setCoefOfRelaxation(double coefOfRelaxation) {
        this.coefOfRelaxation = coefOfRelaxation;
    }

    public int getNumberOfIteration() {
        return numberOfIteration;
    }

    public void setNumberOfIteration(int numberOfIteration) {
        this.numberOfIteration = numberOfIteration;
    }

    public boolean isUseBgNormalization() {
        return useBgNormalization;
    }

    public void setUseBgNormalization(boolean useBgNormalization) {
        this.useBgNormalization = useBgNormalization;
    }

    public boolean isUsePositivityContrainst() {
        return usePositivityContrainst;
    }

    public void setUsePositivityContrainst(boolean usePositivityContrainst) {
        this.usePositivityContrainst = usePositivityContrainst;
    }

    public float getK() {
        return k;
    }

    public void setK(float k) {
        this.k = k;
    }

    public FilterType getFilterType() {
        return filterType;
    }

    public void setFilterType(FilterType filterType) {
        this.filterType = filterType;
    }

    public WindowType getWindowType() {
        return windowType;
    }

    public void setWindowType(WindowType windowType) {
        this.windowType = windowType;
    }

    public float getWindowDimension() {
        return windowDimension;
    }

    public void setWindowDimension(float windowDimension) {
        this.windowDimension = windowDimension;
    }

    public boolean isUseFilter() {
        return useFilter;
    }

    public void setUseFilter(boolean useFilter) {
        this.useFilter = useFilter;
    }
}
