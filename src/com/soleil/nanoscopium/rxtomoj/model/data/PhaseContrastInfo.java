/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.data;

import com.soleil.nanoscopium.rxtomoj.model.ShiftImage;
import com.soleil.nanoscopium.rxtomoj.model.computationFunction.ComputePhaseContrast.PhaseRetrievalAlgo;
import com.soleil.nanoscopium.rxtomoj.model.computationFunction.ComputePhaseContrast.PhaseComputationTechniques;

/**
 * Created by bergamaschi on 05/06/2015.
 */
public class PhaseContrastInfo extends AbstractInfo{

    /**
     * Mirroring Function used in the PhaseConstruction process
     */
    private ShiftImage.MirroringType real = ShiftImage.MirroringType.ASDI_X,imaginary = ShiftImage.MirroringType.ASDI_Y;

    /**
     * Which of the two function will be used in the realSpace phase retrivial algorithm
     */
    private PhaseRetrievalAlgo phaseRetrievalAlgo = PhaseRetrievalAlgo.REALSPACE_GAUSSIAN;

    /**
     * Select the Computation mode for phase contrast computation
     */
    private PhaseComputationTechniques phaseComputationTechniques = PhaseComputationTechniques.CENTEROFGRAVITY;

    /**
     * If True use the selected Mirroring ( only for Fourrier )
     */
    private boolean useMirroring = false;

    /**
     * Perform normalization in one or both direction be aware that
     * normalize should be used when sample is fully visible
     */
    private boolean normalizeX = false;
    private boolean normalizeY = false;

    /**
     * Process the raw phase gradient with background normalization before phase reconstruction
     */
    private boolean useBackgroundNormalization = false;

    /**
     * Process the raw phase gradient with wobbling correction before phase reconstruction
     */
    private boolean useWobbleCorrection = false;

    /**
     * The minimal step value between two updates in iterative phase reconstruction algorithms
     * Together with the expectedErrorPerPixel and maximumNumberOfIteration define a "stop" condition for the iterative algorithm.
     */
    private double minimalStepValue = 0.00001;

    /**
     * The error per pixel expected between computed and real value in the iterative phase reconstruction algorithms
     * Together with the minimalStepValue and maximumNumberOfIteration define a "stop" condition for the iterative algorithm.
     */
    private double expectedErrorPerPixel = 0.0001;

    /**
     * The maximal number of iteration that are allowed for the iterative phase reconstruction algorithms.
     * Together with the minimalStepValue and expectedErrorPerPixel define a "stop" condition for the iterative algorithm.
     */
    private int maximumNumberOfIteration = 50000;

    public PhaseContrastInfo(){

    }

    public PhaseContrastInfo(PhaseContrastInfo old){
        this.copy(old);
    }

    private void copy(PhaseContrastInfo old){
        this.setImaginary(old.getImaginary());
        this.setReal(old.getReal());
        this.setPhaseComputationTechniques(old.getPhaseComputationTechniques());
        this.setPhaseRetrievalAlgo(old.getPhaseRetrievalAlgo());
        this.setUseMirroring(old.isUseMirroring());
        this.setNormalizeX(old.isNormalizeX());
        this.setNormalizeY(old.isNormalizeY());

        this.setUseBackgroundNormalization(old.isUseBackgroundNormalization());
        this.setUseWobbleCorrection(old.isUseWobbleCorrection());

        this.setExpectedErrorPerPixel(old.getExpectedErrorPerPixel());
        this.setMaximumNumberOfIteration(old.getMaximumNumberOfIteration());
        this.setMinimalStepValue(old.getMinimalStepValue());
    }

    public PhaseRetrievalAlgo getPhaseRetrievalAlgo() {
        return phaseRetrievalAlgo;
    }

    public void setPhaseRetrievalAlgo(PhaseRetrievalAlgo phaseRetrievalAlgo) {
        this.phaseRetrievalAlgo = phaseRetrievalAlgo;
    }

    public PhaseComputationTechniques getPhaseComputationTechniques() {
        return phaseComputationTechniques;
    }

    public void setPhaseComputationTechniques(PhaseComputationTechniques phaseComputationTechniques) {
        this.phaseComputationTechniques = phaseComputationTechniques;
    }

    public ShiftImage.MirroringType getImaginary() {
        return imaginary;
    }

    public void setImaginary(ShiftImage.MirroringType imaginary) {
        this.imaginary = imaginary;
    }

    public boolean isUseMirroring() {
        return useMirroring;
    }

    public void setUseMirroring(boolean useMirroring) {
        this.useMirroring = useMirroring;
    }

    public ShiftImage.MirroringType getReal() {
        return real;
    }

    public void setReal(ShiftImage.MirroringType real) {
        this.real = real;
    }

    public boolean isNormalizeX() {
        return normalizeX;
    }

    public void setNormalizeX(boolean normalizeX) {
        this.normalizeX = normalizeX;
    }

    public boolean isNormalizeY() {
        return normalizeY;
    }

    public void setNormalizeY(boolean normalizeY) {
        this.normalizeY = normalizeY;
    }

    public double getExpectedErrorPerPixel() {
        return expectedErrorPerPixel;
    }

    public void setExpectedErrorPerPixel(double expectedErrorPerPixel) {
        this.expectedErrorPerPixel = expectedErrorPerPixel;
    }

    public double getMinimalStepValue() {
        return minimalStepValue;
    }

    public void setMinimalStepValue(double minimalStepValue) {
        this.minimalStepValue = minimalStepValue;
    }

    public int getMaximumNumberOfIteration() {
        return maximumNumberOfIteration;
    }

    public void setMaximumNumberOfIteration(int maximumNumberOfIteration) {
        this.maximumNumberOfIteration = maximumNumberOfIteration;
    }

    public boolean isUseWobbleCorrection() {
        return useWobbleCorrection;
    }

    public void setUseWobbleCorrection(boolean useWobbleCorrection) {
        this.useWobbleCorrection = useWobbleCorrection;
    }

    public boolean isUseBackgroundNormalization() {
        return useBackgroundNormalization;
    }

    public void setUseBackgroundNormalization(boolean useBackgroundNormalization) {
        this.useBackgroundNormalization = useBackgroundNormalization;
    }
}
