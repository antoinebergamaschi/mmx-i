/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.data;

import com.soleil.nanoscopium.rxtomoj.model.utils.FittingUtils;

/**
 * Created by bergamaschi on 10/09/2015.
 */
@Deprecated
public class ShiftImageInfo extends AbstractInfo{

    /**
     * If interpolation have to be done when fitting with FittingUtils
     */
    private boolean interpolate = true;
    /**
     * Minimal distance shift to apply before interpolation is performed
     */
    private float minimumShift = 0.5f;

    /**
     * Number of repetition to improve the fit with FittingUtils
     */
    private int numberOfRepetition = 2;
    /**
     * Interpolation distance to apply after regrid
     */
    private int interpolationDistance = 3;

    /**
     * Interpolation distance to apply after regrid
     */
    private int interpolationDistance_fluorescence = 0;



    private FittingUtils fittingUtils = new FittingUtils();

    public ShiftImageInfo(){}

    public ShiftImageInfo(ShiftImageInfo toCopy){
        this.setFittingUtils(toCopy.getFittingUtils());
        this.setInterpolate(toCopy.isInterpolate());
        this.setNumberOfRepetition(toCopy.getNumberOfRepetition());
        this.setInterpolationDistance_fluorescence(toCopy.getInterpolationDistance_fluorescence());
        this.setInterpolationDistance(toCopy.getInterpolationDistance());
        this.setMinimumShift(toCopy.getMinimumShift());
    }


    public boolean isInterpolate() {
        return interpolate;
    }

    public void setInterpolate(boolean interpolate) {
        this.interpolate = interpolate;
    }

    public int getNumberOfRepetition() {
        return numberOfRepetition;
    }

    public void setNumberOfRepetition(int numberOfRepetition) {
        this.numberOfRepetition = numberOfRepetition;
    }

    public int getInterpolationDistance() {
        return interpolationDistance;
    }

    public void setInterpolationDistance(int interpolationDistance) {
        this.interpolationDistance = interpolationDistance;
    }

    public int getInterpolationDistance_fluorescence() {
        return interpolationDistance_fluorescence;
    }

    public void setInterpolationDistance_fluorescence(int interpolationDistance_fluorescence) {
        this.interpolationDistance_fluorescence = interpolationDistance_fluorescence;
    }

    public FittingUtils getFittingUtils() {
        return fittingUtils;
    }

    public void setFittingUtils(FittingUtils fittingUtils) {
        this.fittingUtils = fittingUtils;
    }

    public float getMinimumShift() {
        return minimumShift;
    }

    public void setMinimumShift(float minimumShift) {
        this.minimumShift = minimumShift;
    }
}
