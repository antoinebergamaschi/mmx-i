/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.data;

import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;

import java.util.Date;
import java.util.Objects;


/**
 * Created by bergamaschi on 25/11/2015.
 * Contains the Data of each stack saved in the Model
 */
public class StackData {

    /**
     * The StackType correspondinf to this data
     */
    private StackType stackType;


    /**
     * Path to the file (if saved or loaded) in the fileSystem
     */
    private String systemPath = "";


    /**
     * An additional string that will be added to the FileName ( used for multi-path data such as fluorescence or darkfield )
     */
    private String additionalName = null;

    //ONLY for fluo
    //Implement a new version
    /**
     * The Spectrum name in the Hdf5 File ( used to separate differente fluo channel )
     * This information is mandatory to retrieve Spectrum data
     */
    private String spc = "";

    /**
     * If this stack is represented with a MultiPath Object,
     * Contains the name where is store this StackType in the MultiPath Object
     * Else is empty
     */
    private String otherName = "";

    /**
     * Warning Do not delete this ( serve to proose to users always the latest version of the data )
     */
    private long date;

    public StackData(){
        date = new Date().getTime();
    }

    public StackData(StackType stackType){
        this();
        this.stackType = stackType;
    }

    public StackData(StackType stackType, String systemPath){
        this(stackType);
        this.systemPath = systemPath;
    }

    public StackData(StackData old){
        this.setAdditionalName(old.getAdditionalName());
        this.setOtherName(old.getOtherName());
        this.setSpc(old.getSpc());
        this.setSystemPath(old.getSystemPath());
        this.setStackType(old.getStackType());
        this.setDate(old.getDate());
    }

    public String getSaveName(){
        switch (stackType){
            case FLUORESCENCE:
                if ( additionalName != null ){
                    return stackType.getSaveName()+"_"+this.additionalName+"_"+this.spc.substring(this.spc.lastIndexOf("/") + 1);
                }
            case SUM_SPECTRA:
                return this.stackType.getSaveName() + "_"+this.spc.substring(this.spc.lastIndexOf("/") + 1);
            case DARKFIELD:
                if ( additionalName != null ){
                    return  this.stackType.getSaveName() +"_"+this.additionalName;
                }else{
                    return  this.stackType.getSaveName();
                }
            default:
                return this.stackType.getSaveName();
        }
    }

    public String getOtherName() {
        return otherName;
    }

    public void setOtherName(String otherName) {
        this.otherName = otherName;
    }

    public StackType getStackType() {
        return stackType;
    }

    public void setStackType(StackType stackType) {
        this.stackType = stackType;
    }

    public String getSpc() {
        return spc;
    }

    public void setSpc(String spc) {
        this.spc = spc;
    }

    public String getAdditionalName() {
        return additionalName;
    }

    public void setAdditionalName(String additionalName) {
        this.additionalName = additionalName;
    }

    public String getSystemPath() {
        return systemPath;
    }

    public void setSystemPath(String systemPath) {
        this.systemPath = systemPath;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public boolean _equals(StackData obj){
        if ( obj.getSystemPath()!= null && obj.getSystemPath().length() > 0 &&
                obj.getSystemPath().equalsIgnoreCase(this.getSystemPath())){
            return true;
        }

        if ( obj.getSpc() != null && obj.getSpc().length() > 0 &&
                obj.getSpc().equalsIgnoreCase(this.getSpc())){
            return true;
        }

        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if ( !super.equals(obj)  ){
            if (obj instanceof StackData ){
                return _equals((StackData) obj);
            }
        }
        return false;
    }
}
