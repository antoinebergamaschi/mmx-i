/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.data;

/**
 * Created by bergamaschi on 23/11/2015.
 */
public class NormalizationInfo extends AbstractInfo {
    //******************************************************************************************************************
    //Background normalization data
    //******************************************************************************************************************
    /**
     * Integration of the first derivative to obtain with the numberOfForseenPixel before stopping the computation
     */
    private float variationCoefficient = 0.35f;
    /**
     * Number of foreseen pixel to compute an integration value
     */
    private int numberOfForeseenPixel = 2;

    /**
     * Compute from left to right to search for border
     */
    private boolean computeLeft = true;
    /**
     * Compute from right to left to search for border
     */
    private boolean computeRigth = true;

    /**
     * True if the background normalization have to be integrated in the computation process
     */
    private boolean doBackgroundNormalization = true;

    /**
     * Use a mobile mean function to smooth the image before computing the border of the image
     */
    private boolean useMobileMeanBackground = true;
    /**
     * The size of the mobile mean to use in the background normalization process
     */
    private int mobileMeanSizeBackground = 8;

    //******************************************************************************************************************
    //Wobbling correction data
    //******************************************************************************************************************
    /**
     * Minimum difference value between real and fitted value, if lower the displacement is set to 0
     */
    private float minimumShift = 0.0f;
    /**
     * Number of iteration of the wobbling correction process
     */
    private int recursiveNumber = 1000;

    /**
     * Use interpolation with computing the shift ( only linear interpolation )
     */
    private boolean useInterpolation = true;

    /**
     * Parameters for the mobile Mean filter on to apply on the Raw center of gravity values
     */
    private boolean useMobileMean = true;
    /**
     * Size of the mobile Window to use
     */
    private int mobileWindow = 16;

    /**
     * Compute the normalization of the background before trying to fit on sinus
     */
    private boolean useBackgroundNormalization = true;

    /**
     * True if the wobble correction have to be integrated in the computation process
     */
    private boolean doWobbleCorrection = true;

    /**
     * True if information of angularRange have to be taken in account for the sinogram fit.
     */
    private boolean usePrecomputedAngularStep = true;

    /**
     * The angular range represented by the sinogram. This value divided by the number of projection will give the
     * angular step, if useRotationMotor is set to false and usePrecomputedAngularStep set to true.
     */
    private double angularRange = Math.PI*2;

    /**
     * True if user want to use angular motor as the angular step in the sine fitting process
     */
    //TODO implements the fit function
    private boolean useRotationMotor = false;

    //******************************************************************************************************************
    //Motors Normalization (regridding)
    //******************************************************************************************************************
    /**
     * Interpolation distance for other modalities
     */
    private int interpolationDistance = 3;

    /**
     * Interpolation distance used for fluorescence interpolation
     */
    private int interpolationDistance_fluorescence = 0;

    public NormalizationInfo(){};

    public NormalizationInfo(NormalizationInfo old){
        this.setVariationCoefficient(old.getVariationCoefficient());
        this.setNumberOfForeseenPixel(old.getNumberOfForeseenPixel());
        this.setComputeLeft(old.isComputeLeft());
        this.setComputeRigth(old.isComputeRigth());
        this.setDoBackgroundNormalization(old.isDoBackgroundNormalization());
        this.setUseMobileMeanBackground(old.isUseMobileMeanBackground());
        this.setMobileMeanSizeBackground(old.getMobileMeanSizeBackground());
        this.setAngularRange(old.getAngularRange());
        this.setUsePrecomputedAngularStep(old.isUsePrecomputedAngularStep());

        this.setMinimumShift(old.getMinimumShift());
        this.setRecursiveNumber(old.getRecursiveNumber());
        this.setMobileWindow(old.getMobileWindow());
        this.setUseMobileMean(old.isUseMobileMean());
        this.setUseInterpolation(old.isUseInterpolation());
        this.setUseBackgroundNormalization(old.isUseBackgroundNormalization());
        this.setDoWobbleCorrection(old.isDoWobbleCorrection());

        this.setInterpolationDistance(old.getInterpolationDistance());
        this.setInterpolationDistance_fluorescence(old.getInterpolationDistance_fluorescence());
    }

    public float getVariationCoefficient() {
        return variationCoefficient;
    }

    public void setVariationCoefficient(float variationCoefficient) {
        this.variationCoefficient = variationCoefficient;
    }

    public int getNumberOfForeseenPixel() {
        return numberOfForeseenPixel;
    }

    public void setNumberOfForeseenPixel(int numberOfForeseenPixel) {
        this.numberOfForeseenPixel = numberOfForeseenPixel;
    }

    public boolean isComputeLeft() {
        return computeLeft;
    }

    public void setComputeLeft(boolean computeLeft) {
        this.computeLeft = computeLeft;
    }

    public boolean isComputeRigth() {
        return computeRigth;
    }

    public void setComputeRigth(boolean computeRigth) {
        this.computeRigth = computeRigth;
    }

    public float getMinimumShift() {
        return minimumShift;
    }

    public void setMinimumShift(float minimumShift) {
        this.minimumShift = minimumShift;
    }

    public int getRecursiveNumber() {
        return recursiveNumber;
    }

    public void setRecursiveNumber(int recursiveNumber) {
        this.recursiveNumber = recursiveNumber;
    }

    public int getMobileWindow() {
        return mobileWindow;
    }

    public void setMobileWindow(int mobileWindow) {
        this.mobileWindow = mobileWindow;
    }

    public boolean isUseMobileMean() {
        return useMobileMean;
    }

    public void setUseMobileMean(boolean useMobileMean) {
        this.useMobileMean = useMobileMean;
    }

    public boolean isUseInterpolation() {
        return useInterpolation;
    }

    public void setUseInterpolation(boolean useInterpolation) {
        this.useInterpolation = useInterpolation;
    }

    public int getInterpolationDistance() {
        return interpolationDistance;
    }

    public void setInterpolationDistance(int interpolationDistance) {
        this.interpolationDistance = interpolationDistance;
    }

    public int getInterpolationDistance_fluorescence() {
        return interpolationDistance_fluorescence;
    }

    public void setInterpolationDistance_fluorescence(int interpolationDistance_fluorescence) {
        this.interpolationDistance_fluorescence = interpolationDistance_fluorescence;
    }

    public boolean isUseBackgroundNormalization() {
        return useBackgroundNormalization;
    }

    public void setUseBackgroundNormalization(boolean useBackgroundNormalization) {
        this.useBackgroundNormalization = useBackgroundNormalization;
    }

    public boolean isDoBackgroundNormalization() {
        return doBackgroundNormalization;
    }

    public void setDoBackgroundNormalization(boolean doBackgroundNormalization) {
        this.doBackgroundNormalization = doBackgroundNormalization;
    }

    public boolean isDoWobbleCorrection() {
        return doWobbleCorrection;
    }

    public void setDoWobbleCorrection(boolean doWobbleCorrection) {
        this.doWobbleCorrection = doWobbleCorrection;
    }

    public int getMobileMeanSizeBackground() {
        return mobileMeanSizeBackground;
    }

    public void setMobileMeanSizeBackground(int mobileMeanSizeBackground) {
        this.mobileMeanSizeBackground = mobileMeanSizeBackground;
    }

    public boolean isUseMobileMeanBackground() {
        return useMobileMeanBackground;
    }

    public void setUseMobileMeanBackground(boolean useMobileMeanBackground) {
        this.useMobileMeanBackground = useMobileMeanBackground;
    }

    public boolean isUsePrecomputedAngularStep() {
        return usePrecomputedAngularStep;
    }

    public void setUsePrecomputedAngularStep(boolean usePrecomputedAngularStep) {
        this.usePrecomputedAngularStep = usePrecomputedAngularStep;
    }

    public double getAngularRange() {
        return angularRange;
    }

    public void setAngularRange(double angularRange) {
        this.angularRange = angularRange;
    }
}
