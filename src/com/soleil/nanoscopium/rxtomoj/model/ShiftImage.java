/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model;

import cern.colt.matrix.tfcomplex.impl.DenseFComplexMatrix1D;
import cern.colt.matrix.tfcomplex.impl.DenseFComplexMatrix2D;
import cern.colt.matrix.tfloat.FloatMatrix1D;
import cern.colt.matrix.tfloat.FloatMatrix2D;
import cern.colt.matrix.tfloat.impl.DenseFloatMatrix1D;
import cern.colt.matrix.tfloat.impl.DenseFloatMatrix2D;
import cern.jet.math.tfcomplex.FComplexFunctions;
import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rxtomoj.model.filters.BackgroundSubtraction;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import com.soleil.nanoscopium.rxtomoj.model.utils.FittingUtils;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Created with IntelliJ IDEA.
 * This Object permit to compute a wobble correction.
 * Shift can be either computed by fitting the sinusogram with a sinus function.
 * Or given by a specified array in a Hdf5 file, or not ( raw array permitted )
 * TODO Warning this Class only works for "X" shifting ( implements Y and Z )
 * @author : antoine bergamaschi
 *         Date: 02/12/13
 *         Time: 10:30
 */
public class ShiftImage {
    public final static int MIRRORING_FLIP_FLOP=1,MIRRORING_NULL=0,MIRRORING_FLIP=2,MIRRORING_FLOP=3,MIRRORING_NEG_NULL = 4,MIRRORING_NEG_FLIP_FLOP = 5,MIRRORING_NEG_FLIP = 6, MIRRORING_NEG_FLOP = 7;
    private boolean interpolate = true;
//    private int numberOfRepetition = 2;
//    private int interpolationDistance = 3;
//    private int interpolationDistance_fluorescence = 0;

    private FittingUtils fittingUtils;


    /**
     * Issue from article :
     * Noniterative boundary-artifact-free wavefront reconstruction from its derivatives
     * Pierre Bon and al.
     */
    public enum MirroringType {
        ASDI_Y(5,2,7,0),ASDI_X(5,6,3,0),MDI(1,2,3,0);

        private int[] square =  new  int[4];

        private MirroringType(int square_top_left, int square_top_right, int square_bottom_left, int square_bottom_right){
            square[0] = square_top_left;
            square[1] = square_top_right;
            square[2] = square_bottom_left;
            square[3] = square_bottom_right;
        }

        public int[] getSquare(){
            return square;
        }

    }

    private float[] shifting;
    private float[] realValueX;
    private float[] realValueY;
//    private Plot plotFit;

    private float minimumShift = 0.5f;
    private int recursiveNumber = 1;

    private float[] shiftingX;
    private float[] shiftingY;

    public ShiftImage(){
        //Basic constructor, used in case of retriving info from hdf5
    }

    /**
     * Directly insert shifting values
     * @param shifting array of float, the shift to apply on each image
     */
    public ShiftImage(float[]  shifting){
        this.shifting = shifting;
    }


    /**
     * Fit a Sine function on the values given in parameters, and create from this fitted curve a shifting array.
     * @param valuesToFit float[], the center of mass
     * @param valuesX  float[],  position of each center of mass
     */
    public ShiftImage( float[] valuesX, float[] valuesToFit){
        setValues(valuesX, valuesToFit);

        fittingUtils.fitCurveOnSine(this.getRealValueX(),this.getRealValueY());

//        this.plotFit = new Plot("Fit Gravity Center Shift","Position Z","Center of Gravity position X",valuesX,valuesToFit);
//        this.plotFit.draw();
    }


    public static RXImage wobbleCorrection(RXImage toUnshift, RXImage shiftData){
        RXImage unShiftedImage  =  new RXImage();
        unShiftedImage.setDims(toUnshift.getDims());

        shift(toUnshift, unShiftedImage, shiftData.copyData());

        return unShiftedImage;
    }

    /**
     * Correction of the shift by fitting the sinogram curve and re-injection it to the absorption image
     * @param shiftedImage  the primary image
     * @return The unshifted image
     */
    public RXImage shiftImage(RXImage shiftedImage){
        //If every parameters are OK
//        if ( canShift() ) {

//            RXImage unShiftedImage = new RXImage(shiftedImage.getWidth(), shiftedImage.getHeight(), shiftedImage.getSize());
            RXImage unShiftedImage  =  new RXImage();
            unShiftedImage.setDims(shiftedImage.getDims());

//            float[] fittedValue = new float[this.realValueX.length];
            float[] fittedValue = new float[1];
            shifting = new float[this.realValueX.length];

            double[] sineParameters = fittingUtils.getSineParameters();

            for (int i = 0; i < this.realValueX.length; i++) {
                fittedValue[0] = (float) (fittingUtils.computeSine(sineParameters[0], sineParameters[1], sineParameters[2], sineParameters[3], (double) this.realValueX[i]));
                //float shift
                shifting[i] = this.realValueY[i] - fittedValue[0];
                if (Math.abs(shifting[i]) < minimumShift) {
                    shifting[i] = 0;
                }
                if (!interpolate) {
                    shifting[i] = (float) (Math.signum(shifting[i])*Math.ceil(Math.abs(shifting[i])));
                    //Transform the shift in Integer shift
//                    if (shift[i] > 0) {
//                        shift[i] = (float) Math.ceil((double) (shift[i]));
//                    } else {
//                        shift[i] = (float) Math.floor((double) (shift[i]));
//                    }
                }

            }

            //Perform the shift on the shiftedImage to obtain the unshifted Image
            shift(shiftedImage, unShiftedImage, shifting);

            return unShiftedImage;
            //Crop the shifted Image to not show "black value"
//            Arrays.parallelSort(shift);
//
//            int newWidth = (int) (unShiftedImage.getWidth() - (shift[shift.length - 1] - shift[0]));
//            RXImage unshiftCrop = new RXImage(newWidth, unShiftedImage.getHeight(), unShiftedImage.getSize());
//
//            for (int z = 0; z < unShiftedImage.getSize(); z++) {
//                float[] tmp = new float[newWidth * unshiftCrop.getHeight()];
//                float[] tmp_ori = (float[]) unShiftedImage.getPixels(z + 1);
//                for (int y = 0; y < unShiftedImage.getHeight(); y++) {
//                    System.arraycopy(tmp_ori, (int) (shift[shift.length - 1] + y * unShiftedImage.getWidth()), tmp, y * newWidth, newWidth);
//                }
//                unshiftCrop.setPixels(tmp, z + 1);
//            }
//            return unshiftCrop;
//        }
//        else {
//            RXTomoJ.getInstance().getModel().initializeShiftImage(shiftedImage);
//            if ( canShift() ){
//                return shiftImage(shiftedImage);
//            }
//            return null;
//        }
    }


    /**
     * Correction of the shift by fitting the sinogram curve and re-injection it to the absorption image
     * @param shiftedImage  the primary image
     * @param adv
     * @return The unshifted image
     */
    public RXImage shiftImage(RXImage shiftedImage, float adv){
        //If every parameters are OK
//        if ( canShift() ) {

//            RXImage unShiftedImage = new RXImage(shiftedImage.getWidth(), shiftedImage.getHeight(), shiftedImage.getSize());
        RXImage unShiftedImage  =  new RXImage();
        unShiftedImage.setDims(shiftedImage.getDims());

//            float[] fittedValue = new float[this.realValueX.length];
        float[] fittedValue = new float[1];
        shifting = new float[this.realValueX.length];

        double[] sineParameters = fittingUtils.getSineParameters();

        for (int i = 0; i < this.realValueX.length; i++) {
            fittedValue[0] = (float) (fittingUtils.computeSine(sineParameters[0], sineParameters[1], sineParameters[2], sineParameters[3], (double) this.realValueX[i]));
            //float shift
            shifting[i] = (this.realValueY[i] - fittedValue[0])*adv;
            if (Math.abs(shifting[i]) < minimumShift) {
                shifting[i] = 0;
            }
            if (!interpolate) {
                shifting[i] = (float) (Math.signum(shifting[i])*Math.ceil(Math.abs(shifting[i])));
                //Transform the shift in Integer shift
//                    if (shift[i] > 0) {
//                        shift[i] = (float) Math.ceil((double) (shift[i]));
//                    } else {
//                        shift[i] = (float) Math.floor((double) (shift[i]));
//                    }
            }

        }

        //Perform the shift on the shiftedImage to obtain the unshifted Image
        shift(shiftedImage, unShiftedImage, shifting);

        return unShiftedImage;
    }


    /**
     * Display the resulting fitted curbe on the curve to fit
     * @param addFittedValue Add to the current plot the fitted value ( if there is some )
     */
    @Deprecated
    public void displayFittedPlot(boolean addFittedValue){
//        this.plotFit = new Plot("Fit Gravity Center Shift","Position Z","Center of Gravity position X",this.realValueX, this.realValueY);
//        this.plotFit.draw();

        if ( addFittedValue){

            double[] sineParameters = fittingUtils.getSineParameters();
            float[] fittedValue = new float[this.realValueX.length];
            for ( int i = 0 ; i < this.realValueX.length ; i++) {
                fittedValue[i] = (float) (fittingUtils.computeSine(sineParameters[0], sineParameters[1], sineParameters[2], sineParameters[3], (double) this.realValueX[i]));
            }
//            this.plotFit.setColor(Color.red);
//            this.plotFit.addPoints(this.realValueX, fittedValue, PlotWindow.LINE);
//            this.plotFit.draw();
        }
//        this.plotFit.show();
//        while ( plotWindow.isActive() ){
//        //Wait
//            try {
//              Thread.sleep(100);
//            } catch (InterruptedException e) {
//              e.printStackTrace();
//            }
//        }
//        plotWindow.close();
    }

    public float[] getFittedValue(float[] x){
        if ( x != null && fittingUtils.isUpdated()) {
            double[] sineParameters = fittingUtils.getSineParameters();
            float[] fittedValue = new float[this.realValueX.length];
            for (int i = 0; i < x.length; i++) {
                fittedValue[i] = (float) (fittingUtils.computeSine(sineParameters[0], sineParameters[1], sineParameters[2], sineParameters[3], (double) x[i]));
            }
            return fittedValue;
        }
        return new float[1];
    }

    public float[] getFittedValue(){
        return getFittedValue(this.realValueX);
    }

    public float[] getRealValueX(){
        return realValueX;
    }

    public float[] getRealValueY(){
        return realValueY;
    }

    /**
     * Shift the imageToShift to obtain the unShiftedImage with for each row value
     * with a linear extrapolation
     * @param imageToShift RXImage, support image to be shifted
     * @param unShiftedImage RXImage, constructed image from imageToShift and the shift parameters
     * @param shift float[], the shift values for each ray in imageToShift
     */
    private static void shift(RXImage imageToShift, RXImage unShiftedImage, float[] shift){
        int z,y,x;
        float xPos,value;

        float[]  absorbtion  = imageToShift.copyData();
        float[] absorbtionShift = new float[absorbtion.length];

        //[0] = Current Slice position, [1] = Current Height position in [0]
        final int[] position = new int[2];

        //Create the shift free absorbtion image.
        for ( z = 0; z < imageToShift.getSize(); z++) {
            position[0] = z * imageToShift.getHeight()*imageToShift.getWidth();
            for ( y= 0; y < unShiftedImage.getHeight(); y++){
                int positionInShiftArray = (z * imageToShift.getHeight()) + y;
                position[1] = y*imageToShift.getWidth()+position[0];
                for ( x = 0; x < unShiftedImage.getWidth() ; x++){
//                    int currentPosition = positionY+positionInImageZ+x;
                    xPos = x + shift[positionInShiftArray];
                    //Get the Sifhted position in the data array
                    value = (float) ComputationUtils.getLinearInterpolationValue(xPos,position[1],imageToShift.getWidth(),absorbtion);
                    //If the position exist set the current position
                    if ( Float.isFinite(value) ) {
                        absorbtionShift[position[1]+x] = value;
                    }
                }
            }
        }
        unShiftedImage.setData(absorbtionShift);
    }

    public void setYvalues(float[] values){
        this.realValueY = values;
    }

    public void setValues(float[] xValue, float[] yValue){
        this.realValueX = xValue;
        this.realValueY = yValue;
    }

    public void resetValue(RXImage imageStack){
        this.realValueY = new float[imageStack.getSize()*imageStack.getHeight()];
        this.realValueX = new float[imageStack.getSize()*imageStack.getHeight()];

        int  i;

        for ( i = 0; i < this.realValueX.length ; i++ ){
            this.realValueX[i] = i;
        }

        this.realValueY = ComputationUtils.computeGravityCenter1D(imageStack);
    }

    /**
     * Recompute the X and Y values of the sinogram fit values. imageStack is the whole image where the GravityCenter
     * will be extracted and serve as Y, while each line represent an X value.
     * @param imageStack The image where on each line the center of gravity will be plotted
     */
    public void updateValues(RXImage imageStack){
        this.realValueY = new float[imageStack.getSize()*imageStack.getHeight()];
        this.realValueX = new float[imageStack.getSize()*imageStack.getHeight()];

        int  i;

        for ( i = 0; i < this.realValueX.length ; i++ ){
            this.realValueX[i] = i;
        }

        this.realValueY = ComputationUtils.computeGravityCenter1D(imageStack);


    }

    /**
     * Try to fit the current X/Y value with a Sine function.
     * @param withBasicParameters Boolean allowing to keep the old parameters already stored in the fitting Object
     */
    public void fitValueOnSine(boolean withBasicParameters){
        this.fittingUtils.fitCurveOnSine(this.realValueX,this.realValueY,withBasicParameters);
    }

    /**
     * Try to fit the current X/Y value with a Sine function.
     * @param fixeAngularStep Allow to fixe the angular step in the Sine fit function
     * @param withBasicParameters Boolean allowing to keep the old parameters already stored in the fitting Object
     */
    public void fitValueOnSine(double fixeAngularStep, boolean withBasicParameters){
        this.fittingUtils.fitCurveOnSine(this.realValueX, this.realValueY, fixeAngularStep, withBasicParameters);
    }


    public float getOrdinateToOrigin(){
        return (float)this.fittingUtils.getSineParameters()[3];
    }


    /**
     * For each line get the Max and Min position of the motors. Simultaneously compute the step size of the motors.
     * @param motorsPosition Array containing the position
     * @param width Width of the array
     * @param height Height of the array
     * @param size Size of the array
     * @param minmaxPos Computed Min-Max position in the position array
     * @param step Computed space between pixel or pixel size
     */
    private static void gridRange_w(float[] motorsPosition, int width, int height, int size, int[] minmaxPos, float[] step){
        int[] position = new int[3];
        int[] currentZ = new int[1];
        int[] currentY = new int[1];
        float[] numbOfStep = new float[1];
        int[] pos = new int[1];
        int[] pos_before = new int[1];
        int[] tmp_minmax = new int[2];

        for ( int k = 0 ; k < size ; k++ ) {
            currentZ[0] = k * width * height;
            for (position[1] = 0; position[1] < height; position[1]++) {
                //Note that value HAVE to be ranged in ascending order
                currentY[0] = position[1] * width;
                tmp_minmax[0] = tmp_minmax[1] = currentY[0];
                pos[0] = pos_before[0] = currentY[0] + currentZ[0];
                int bugMotorPosition = 0;
                //For each row look for the minimum position
                for (position[2] = 0; position[2] < width; position[2]++) {
                    pos[0] = position[2] + currentY[0] + currentZ[0];

                    if ((pos[0]) < motorsPosition.length) {

                        //If position don't change there is a motor position error
                        if (position[2] > 0 && (motorsPosition[pos[0]] == motorsPosition[pos_before[0]])) {
                            bugMotorPosition++;
                        } else if (position[2] > 0 && motorsPosition[pos[0]] != 0) {
                            numbOfStep[0]++;
                            step[0] += Math.abs(motorsPosition[pos_before[0]] - motorsPosition[pos[0]]);
                        }


                        if (motorsPosition[tmp_minmax[0]] > motorsPosition[pos[0]]) {
                            //TODO WARNING TEST ONLY ( BUG on Image with brutal 0 position in the motors position )
                            if (motorsPosition[pos[0]] != 0.0f) {
                                tmp_minmax[0] = pos[0];
                            }
                        }

                        if (motorsPosition[tmp_minmax[1]] < motorsPosition[pos[0]]) {
                            //TODO WARNING TEST ONLY ( BUG on Image with brutal 0 position in the motors position )
                            if (motorsPosition[pos[0]] != 0.0f) {
                                tmp_minmax[1] = pos[0];
                            }
                        }

                        //If there is to much error in motor position in this row
                        //                if ( bugMotorPosition  > 5 ){
                        //                    tmp_maxPosition  =  maxPosition;
                        //                    tmp_minPosition  = minPosition;
                        //                    break;
                        //                }
                        pos_before[0] = pos[0];
                    }
                }
                if ( tmp_minmax[0] < 0 || tmp_minmax[1] < 0 || tmp_minmax[0] > motorsPosition.length || tmp_minmax[1] > motorsPosition.length){
                    System.err.println("test WARNING BUG BUG");
                }
                else {
                    if (position[1] == 0) {
                        minmaxPos[0] = tmp_minmax[0];
                        minmaxPos[1] = tmp_minmax[1];
                    } else {
                        //Taking the Max of the min and min of the max ensure that the data are correctly regrouped on a small grid
                        //Take the max min position between the global en tmp current
                        //TODO WARNING TEST ONLY ( BUG on Image with brutal 0 position in the motors position )
                        if (motorsPosition[tmp_minmax[0]] > motorsPosition[minmaxPos[0]] && motorsPosition[tmp_minmax[0]] != 0.0f) {
                            minmaxPos[0] = tmp_minmax[0];
                        }
                        //Take the min max position between the global and tmp current
                        //TODO WARNING TEST ONLY ( BUG on Image with brutal 0 position in the motors position )
                        if (motorsPosition[tmp_minmax[1]] < motorsPosition[minmaxPos[1]] && motorsPosition[tmp_minmax[1]] != 0.0f) {
                            minmaxPos[1] = tmp_minmax[1];
                        }
                    }
                }
            }
        }

        if ( numbOfStep[0] == 0 ){
            step[0] = 0;
        }else {
            step[0] /= numbOfStep[0];
        }
    }


    /**
     * For each column get the Max and Min position of the motors. Simultaneously compute the step size of the motors.
     * @param positionData Array containing the position
     * @param width Width of the array
     * @param height Height of the array
     * @param size Size of the array
     * @param minmaxPos Computed Min-Max position in the position array
     * @param step Computed space between pixel or pixel size
     */
    private static void gridRange_h(float[] positionData, int width, int height, int size, float[] minmaxPos, float[] step){
        int[] position = new int[2],currentZ = new int[1];
        float[] numbOfStep = new float[1];
        float[][] meanSd = new float[height][2];
        float[] tmp_minmax = {Float.MAX_VALUE,-Float.MAX_VALUE};
        float[] tmp = new float[width];

        for (position[0] = 0 ; position[0] < size ; position[0]++ ) {
            currentZ[0] = position[0] * width * height;

            //Compute Mean and SD for each line
            for ( int k = 0 ; k < height ; k++ ){
                System.arraycopy(positionData, currentZ[0] + k * width, tmp, 0, tmp.length);
                System.arraycopy(ComputationUtils.computeStatistic(tmp, true), 0, meanSd[k], 0, meanSd[k].length);
            }
//            IntStream.range(0, height).parallel().forEach(k -> {
//                float[] tmp = new float[width];
//                System.arraycopy(positionData, currentZ[0] + k * width, tmp, 0, tmp.length);
//                System.arraycopy(ComputationUtils.computeStatistic(tmp, true), 0, meanSd[k], 0, meanSd[k].length);
//            });


            //TODO do not remember why its here ??
//            float sign = 0;
//            float tmp_sign = 0;
//            //Rapidly read every Row to know if the order is respected
//            for ( position[1] = 0; position[1] < height ; position[1]++ ){
//                if ( position[1] > 0 ) {
//                    tmp_sign = meanSd[position[1] - 1][0] - meanSd[position[1]][0];
//                    if ( tmp_sign > 0 ){
//                        sign++;
//                    }else{
//                        sign--;
//                    }
//                }
//            }

            //For each column look for the minimum position
            for (position[1] = 0; position[1] < height; position[1]++) {
//                pos[0] = (position[1]);
                //We suppose that an SD == 0 is a bugged line
                if (meanSd[position[1]][1] != 0) {

                    if (position[1] > 0) {
                        numbOfStep[0]++;
                        step[0] += Math.abs(meanSd[position[1] - 1][0] - meanSd[position[1]][0]);
                    }

                    if (tmp_minmax[0] > (meanSd[position[1]][0]+meanSd[position[1]][1])) {
                        //TODO WARNING TEST ONLY ( BUG on Image with brutal 0 position in the motors position )
                        if (meanSd[position[1]][0] != 0.0f) {
                            tmp_minmax[0] = meanSd[position[1]][0]+meanSd[position[1]][1];
                        }
                    }

                    if (tmp_minmax[1] <(meanSd[position[1]][0]-meanSd[position[1]][1])) {
                        //TODO WARNING TEST ONLY ( BUG on Image with brutal 0 position in the motors position )
                        if (meanSd[position[1]][0] != 0.0f) {
                            tmp_minmax[1] = meanSd[position[1]][0]-meanSd[position[1]][1];
                        }
                    }
                }
//                pos_before[0] = position[1];
            }

            if (position[0] == 0) {
                minmaxPos[0] = tmp_minmax[0];
                minmaxPos[1] = tmp_minmax[1];
            } else {
                //Taking the Max of the min and min of the max ensure that the data are correctly regrouped on a small grid
                //Take the max min position between the global en tmp current
                //TODO WARNING TEST ONLY ( BUG on Image with brutal 0 position in the motors position )
                if (tmp_minmax[0] > minmaxPos[0] && tmp_minmax[0] != 0.0f) {
                    minmaxPos[0] = tmp_minmax[0];
                }
                //Take the min max position between the global and tmp current
                //TODO WARNING TEST ONLY ( BUG on Image with brutal 0 position in the motors position )
                if (tmp_minmax[1] < minmaxPos[1] && tmp_minmax[1] != 0.0f) {
                    minmaxPos[1] = tmp_minmax[1];
                }
            }
        }

        if ( numbOfStep[0] == 0 ){
            step[0] = 0;
        }else {
            step[0] /= numbOfStep[0];
        }
    }

    /**
     * For each column get the Max and Min position of the motors. Simultaneously compute the step size of the motors.
     * @param positionData Array containing the position
     * @param width Width of the array
     * @param height Height of the array
     * @param size Size of the array
     * @param minmaxPos Computed Min-Max position in the position array
     * @param step Computed space between pixel or pixel size
     */
    @Deprecated
    private void gridRange_h(float[] positionData, int width, int height, int size, int[] minmaxPos, float[] step){
        int[] position = new int[3],pos = new int[1],pos_before = new int[1],currentZ = new int[1];
        float[] numbOfStep = new float[1];
        float[][] meanSd = new float[height][2];

        for (position[0] = 0 ; position[0] < size ; position[0]++ ) {
            currentZ[0] = position[0]*width*height;

            //Compute Mean and SD for each line
            IntStream.range(0,height).parallel().forEach(k-> {
                float[] tmp = new float[width];
                System.arraycopy(positionData,currentZ[0]+k*width,tmp,0,tmp.length);
                System.arraycopy(ComputationUtils.computeStatistic(tmp,true),0,meanSd[k],0,meanSd[k].length);
            });

            float sign = 0;
            float tmp_sign = 0;
            //Rapidly read every Row to know if the order is respected
            for ( position[1] = 0; position[1] < height ; position[1]++ ){
                if ( position[1] > 0 ) {
                    tmp_sign = positionData[position[1] - 1] - positionData[position[1]];
                    if ( tmp_sign > 0 ){
                        sign++;
                    }else{
                        sign--;
                    }
                }
            }

            if ( height*0.8 > Math.abs(sign)){
                System.err.println("#####");
                System.err.println("Bug in the Y positioning");
                System.err.println("#####");
                return;
            }

            for (position[2] = 0; position[2] < width; position[2]++) {
                //Note that value HAVE to be ranged in ascending order
                int tmp_minPosition = position[2];
                int tmp_maxPosition = position[2];
                int bugMotorPosition = 0;
//                pos[0] = pos_before[0] = position[2];

                //For each column look for the minimum position
                for (position[1] = 0; position[1] < height; position[1]++) {
                    pos[0] = currentZ[0] + position[2] + (position[1] * width);
                    //We suppose that an SD == 0 is a bugged line
                    if ( meanSd[position[1]][1] != 0 ){
//                        &&
//                            ( (position[1] > 0 && Math.signum(meanSd[position[1]-1][0] - meanSd[position[1]][0]) == Math.signum(sign)) ||
//                            (position[1]==0 && Math.signum(meanSd[position[1]][0] - meanSd[position[1]+1][0]) == Math.signum(sign)) ) ){

                        if (position[1] > 0) {
                            numbOfStep[0]++;
                            step[0] += Math.abs(meanSd[position[1]-1][0] - meanSd[position[1]][0]);
                        }
                        //                if ( (pos[0]) < positionData.length ) {

                        //If poistion dont change there is a motor position error
//                        if (position[1] > 0 && (positionData[pos[0]] == positionData[pos_before[0]])) {
//                            bugMotorPosition++;
//                        } else if (position[1] > 0 && positionData[pos[0]] != positionData[pos_before[0]]) {
//                            numbOfStep[0]++;
//                            step[0] += Math.abs(positionData[pos_before[0]] - positionData[pos[0]]);
//                        }

                        if (positionData[tmp_minPosition] > positionData[pos[0]]) {
                            //TODO WARNING TEST ONLY ( BUG on Image with brutal 0 position in the motors position )
                            if (positionData[pos[0]] != 0.0f) {
                                tmp_minPosition = pos[0];
                            }
                        }

                        if (positionData[tmp_maxPosition] < positionData[pos[0]]) {
                            //TODO WARNING TEST ONLY ( BUG on Image with brutal 0 position in the motors position )
                            if (positionData[pos[0]] != 0.0f) {
                                tmp_maxPosition = pos[0];
                            }
                        }

                        //If there is to much error in motor position in this row
                        //                if ( bugMotorPosition  > 5 ){
                        //                    tmp_maxPosition  =  maxPosition;
                        //                    tmp_minPosition  = minPosition;
                        //                    break;
                        //                }

                    }
                    pos_before[0] = pos[0];
                }

                if (position[0] == 0 && position[2] == 0) {
                    minmaxPos[0] = tmp_minPosition;
                    minmaxPos[1] = tmp_maxPosition;
                } else {
                    //Taking the Max of the min and min of the max ensure that the data are correctly regrouped on a small grid
                    //Take the max min position between the global en tmp current
                    //TODO WARNING TEST ONLY ( BUG on Image with brutal 0 position in the motors position )
                    if (positionData[tmp_minPosition] > positionData[minmaxPos[0]] && positionData[tmp_minPosition] != 0.0f) {
                        minmaxPos[0] = tmp_minPosition;
                    }
                    //Take the min max position between the global and tmp current
                    //TODO WARNING TEST ONLY ( BUG on Image with brutal 0 position in the motors position )
                    if (positionData[tmp_maxPosition] < positionData[minmaxPos[1]] && positionData[tmp_maxPosition] != 0.0f) {
                        minmaxPos[1] = tmp_maxPosition;
                    }
                }
            }
        }


        step[0] /= numbOfStep[0];

        //Remove Table
        numbOfStep = null;
        pos = null;
        pos_before = null;
    }


    /**
     * Set up a step of 1
     * @param position The motors positions which are modify by this functio
     * @param minmax The minimum and maximum position
     * @param step The step between each motors position (here 1)=
     * @param width The width of the position array
     * @param height The height of the position array
     * @param size The size of the position array
     */
    private static void initXpos(float[] position, int[] minmax, float[] step, int width, int height, int size){
        int[] s = new int[3];
        int[] current = new int[2];
        for ( s[0] = 0 ; s[0] < size ; s[0]++ ) {
            current[0] = (s[0]*width*height);
            for (s[1] = 0; s[1] < height; s[1]++) {
                current[1] =  s[1] * width + current[0];
                for (s[2] = 0; s[2] < width; s[2]++) {
                    position[current[1] + s[2]] = s[2];
                }
            }
        }

        minmax[0] = 0;
        minmax[1] = width-1;
        step[0] = 1;
    }

    /**
     * Set up a step of 1
     * @param position The motors positions which are modify by this functio
     * @param minmax The minimum and maximum position
     * @param step The step between each motors position (here 1)=
     * @param width The width of the position array
     * @param height The height of the position array
     * @param size The size of the position array
     */
    private static void initYpos(float[] position, int[] minmax, float[] step, int width, int height, int size){
       int[] s = new int[3];
        int[] current = new int[2];
        for ( s[0] = 0 ; s[0] < size ; s[0]++ ) {
            for (s[1] = 0; s[1] < width; s[1]++) {
                current[0] = s[1] + (s[0]*width*height);
                for (s[2] = 0; s[2] < height; s[2]++) {
                    current[1] = current[0] + s[2]*width;
                    position[current[1]] = s[2];
                }
            }
        }

        minmax[0] = 0;
        minmax[1] = (height*width) - 1;
        step[0] = 1;
    }

    /**
     * Re-grid the given stack with the given data position. The dataToRegrid Stack and positionData must have the same
     * number of elements.
     * Due to the  continuous motors value, interpolation have to be done. Warning Here, in some case two motors value are computed to be parts of the same
     * pixel. In this case the pixel value is divided by  two.
     * @param dataToRegrid RXImage, the data to regrid
     * @param positionDataX float[], the X position elements by elements
     * @param positionDataY float[], the Y position elements by elements
     * @return RXImage the resulting re-gridded imageStack
     */
    public static RXImage regriding(RXImage dataToRegrid, float[] positionDataX, float[] positionDataY, int interpolationDistance){
        float[] pixelsToRegrid = null;
        int width=dataToRegrid.getWidth(),height=dataToRegrid.getHeight(),size=dataToRegrid.getSize(),
                positionZReal,positionY,positionX,posLeft,posRight,positionYReal,newWidth=width,newHeight=height,newSize=size;
//        float step = 0.0f;
        float[] step_x = new float[1];
        float[] step_y = new float[1];
        int[] posMin_Max_x = new int[2];
        int[] posMin_Max_y = new int[2];
        int[] pos = new int[3];

        float[] valMinMax_x = new float[2];
        float[] valMinMax_y = new float[2];

        if ( positionDataX == null ){
            positionDataX = new float[width*height*size];
        }

        if ( positionDataY == null ){
            positionDataY = new float[width*height*size];
        }

        int[] newDims = initDimensionRegrid(positionDataX,positionDataY,valMinMax_x,valMinMax_y,posMin_Max_x,posMin_Max_y,step_x,step_y,width,height,size);

        newWidth = newDims[0];
        newHeight = newDims[1];

        System.err.println("New Width  = "+newWidth+"\t Step = "+step_x[0]);
        System.err.println("New Height = "+newHeight+"\t Step = "+step_y[0]);


        //Warning Special Case when only 1 Line in the final image
        int test = 1;
        if ( newHeight == 0 ){
            test = 2;
            newHeight = 1;
        }

        final float[] zeros = new float[newWidth*newHeight*size];
        RXImage resultingImage = new RXImage(newWidth,newHeight,newSize);

        final float[] result = new float[resultingImage.getGlobalSize()];
        pixelsToRegrid = dataToRegrid.copyData();

        //Interpolation method, good when motor position are relatively correct
        if ( test == 0 ) {
            System.err.println("Use the Interpolation Function");
            computeExtrapolation(resultingImage, dataToRegrid, width, height, newWidth, newHeight, posMin_Max_x, posMin_Max_y, step_x, step_y, positionDataX, positionDataY);
        }
        //Regrid methode, place each pixel in a new int position then interpolate value between each values
        else if ( test == 1 ) {
            System.err.println("Regrid function with both X and Y");
            int bugVal = 0;
            //Regridding
            //For each z position
            for (pos[2] = 0; pos[2] < size; pos[2]++) {
                //Real position in the [x,y,z] coordinate
                positionZReal = pos[2] * height * width;
                int positionZ = pos[2] * newHeight * newWidth;

                //For each y position
                for (pos[1] = 0; pos[1] < height; pos[1]++) {
                    //Real position in the [x,y,z] coordinate
                    positionYReal = pos[1] * width;
                    //For each x position
                    for (pos[0] = 0; pos[0] < width; pos[0]++) {
                        if (positionDataX[pos[0] + positionZReal + positionYReal] <= valMinMax_x[1] &&
                                positionDataY[pos[0] + positionZReal + positionYReal] <= valMinMax_y[1] &&
                                positionDataX[pos[0] + positionZReal + positionYReal] >= valMinMax_x[0] &&
                                positionDataY[pos[0] + positionZReal + positionYReal] >= valMinMax_y[0] ){

                            positionX = (int) Math.rint((positionDataX[pos[0] + positionZReal + positionYReal] - valMinMax_x[0]) / step_x[0]);
                            positionY = (int) Math.rint((positionDataY[pos[0] + positionZReal + positionYReal] - valMinMax_y[0]) / step_y[0]);

                            if (positionX >= 0 && positionX < newWidth &&
                                    positionY >= 0 && positionY < newHeight) {
                                positionY *= newWidth;


                                //If this value has already been assigned
                                if (zeros[positionX + positionY + positionZ] != 0) {
                                    result[positionX + positionY + positionZ] += pixelsToRegrid[pos[0] + positionYReal + positionZReal];
//                                    result[positionX + positionY + positionZ] /= 2.0f;
                                    zeros[positionX + positionY + positionZ]++;
                                    //Count has a bug value
                                    bugVal++;
                                } else {
                                    result[positionX + positionY + positionZ] = pixelsToRegrid[pos[0] + positionYReal + positionZReal];
                                    //Update Zero value ( this is not a zero )
                                    zeros[positionX + positionY + positionZ] = 1;
                                }
                            }
                        }
                    }
                }

                if (bugVal > 0) {
                    System.err.println("Bug value already assigned " + ShiftImage.class.toString() + " : " + bugVal);
                }

                //Divide by zeros if different from 0
//                IntStream.range(0,result.length).parallel().forEach(k -> {
//                    if (zeros[k] > 1) {
//                        result[k] /= zeros[k];
//                    }
//                });

                for (pos[0] = positionZ; pos[0] < positionZ+newHeight*newWidth; pos[0]++) {
                    if (zeros[pos[0]] > 1) {
                        result[pos[0]] /= zeros[pos[0]];
                    }
                }


                if (interpolationDistance > 0) {

                    int posTop = 0;
                    int posBottom = 0;
                    //Loop over pixel to simply fill 0 value with neighbours mean
                    for (pos[0] = positionZ; pos[0] < positionZ+newHeight*newWidth; pos[0]++) {
                        boolean val = false;
                        boolean val2 = false;
                        if (zeros[pos[0]] == 0) {
                            posLeft = pos[0] - 1;
                            posRight = pos[0] + 1;
                            posBottom = pos[0] + newWidth;
                            posTop = pos[0] - newWidth;

                            //Find the first Good value within the maxDist range for each dimension
                            //The first good value should not be 0, be contains in the row or colums and in the range of interpolation
                            int i = 0;
                            while (posLeft >= 0 && posLeft % (width-1) > 0 && zeros[posLeft] == 0.0f && i < interpolationDistance) {
                                posLeft--;
                                i++;
                            }

                            i = 0;
                            while (posRight < result.length && posRight % width > 0 && zeros[posRight] == 0.0f && i < interpolationDistance) {
                                posRight++;
                                i++;
                            }

                            i = 0;
                            while (posBottom < result.length && zeros[posBottom] == 0.0f && i < interpolationDistance) {
                                posBottom += newWidth;
                                i++;
                            }

                            i = 0;
                            while (posTop >= 0 && zeros[posTop] == 0.0f && i < interpolationDistance) {
                                posTop -= newWidth;
                                i++;
                            }


                            //Finally test each position and add them together to create the new interpolate value
                            if (posLeft >= 0 && zeros[posLeft] != 0.0f && posLeft % (width-1) > 0) {
                                if (posRight < zeros.length && zeros[posRight] != 0.0f && posRight % width > 0) {
                                    result[pos[0]] += (result[posLeft] + result[posRight]) / 2.0f;
                                    val = true;
                                } else {
                                    result[pos[0]] += result[posLeft];
                                    val = true;
                                }
                            } else if (posRight < zeros.length && zeros[posRight] != 0.0f && posRight % width > 0) {
                                result[pos[0]] += result[posRight];
                                val = true;
                            }

                            if (posTop >= 0 && zeros[posTop] != 0.0f) {
                                if (posBottom < result.length && zeros[posBottom] != 0.0f) {
                                    result[pos[0]] += (result[posTop] + result[posBottom]) / 2.0f;
                                    val2 = true;
                                } else {
                                    result[pos[0]] += result[posTop];
                                    val2 = true;
                                }
                            } else if (posBottom < result.length && zeros[posBottom] != 0.0f) {
                                result[pos[0]] += result[posBottom];
                                val2 = true;
                            }

                            if (val && val2) {
                                result[pos[0]] /= 2.0f;
                            }

                        }
                    }
                }
            }
            resultingImage.setData(result);
        }
        //Regrid the X position only
        else if ( test == 2 ) {
            System.err.println("Regrid function with X");
            int bugVal = 0;
            //Regridding
            //TODO Add a more restraining
            for ( pos[2] = 0 ; pos[2] < size  ; pos[2]++){
                positionZReal = pos[2]*height*width;
//                pixelsToRegrid  = dataToRegrid.copySlice(pos[2]);
                int positionZ = pos[2] * newHeight * newWidth;

                for (pos[1] =0 ; pos[1] < newHeight ; pos[1]++ ){
                    positionY =  pos[1]*newWidth;
                    positionYReal = pos[1]*width;
                    for ( pos[0] = 0 ; pos[0] < width ; pos[0]++ ){
                        if ( (pos[0] + positionZReal + positionYReal) < positionDataX.length ) {
//                    toAdd += x
                            if (positionDataX[pos[0] + positionZReal + positionYReal] <= positionDataX[posMin_Max_x[1]]) {
                                int position = (int) Math.rint((positionDataX[pos[0] + positionZReal + positionYReal] - positionDataX[posMin_Max_x[0]]) / step_x[0]);
                                if (position >= 0 && position < newWidth) {
                                    if (result[position + positionY + positionZ] != 0) {
                                        result[position + positionY + positionZ] += pixelsToRegrid[pos[0] + positionYReal + positionZReal];
                                        zeros[position + positionY + positionZ]++;
                                        bugVal++;
                                    } else {
                                        result[position + positionY + positionZ] = pixelsToRegrid[pos[0] + positionYReal + positionZReal];
                                        zeros[position + positionY + positionZ] = 1;
                                    }
                                }
                            }
                        }
                    }
                }

                if ( bugVal > 0 ){
                    System.err.println("Bug value already assigned "+ShiftImage.class.toString()+" : "+bugVal);
                }

            }

            //Divide by zeros if different from 0
            IntStream.range(0,result.length).parallel().forEach(k -> {
                if (zeros[k] > 1) {
                    result[k] /= zeros[k];
                }
            });

            if (interpolationDistance > 0) {
                //Loop over pixel to simply fill 0 value with neighbours mean
                for (pos[0] = 0; pos[0] < result.length; pos[0]++) {
                    if (zeros[pos[0]] == 0) {

                        posLeft = pos[0] - 1;
                        posRight = pos[0] + 1;

                        //Find the first Good value within the maxDist range
                        int i = 0;
                        while (posLeft >= 0 && posLeft % (width-1) > 0 && zeros[posLeft] == 0.0f && i < interpolationDistance) {
                            posLeft--;
                            i++;
                        }

                        i = 0;
                        while (posRight < result.length && posRight % width > 0 && zeros[posRight] == 0.0f && i < interpolationDistance) {
                            posRight++;
                            i++;
                        }

                        if (posLeft >= 0 && zeros[posLeft] != 0.0f && posLeft % (width-1) > 0) {
                            if (posRight < zeros.length && zeros[posRight] != 0.0f && posRight % width > 0) {
                                result[pos[0]] = (result[posLeft] + result[posRight]) / 2.0f;
                            } else {
                                result[pos[0]] = result[posLeft];
                            }
                        } else if (posRight < zeros.length && zeros[posRight] != 0.0f && posRight % width > 0) {
                            result[pos[0]] = result[posRight];
                        }

                    }
                }
            }
            resultingImage.setData(result);
        }

        //Realise ressource
        step_x = null;
        step_y = null;
        posMin_Max_x = null;
        posMin_Max_y = null;
//        zeros = null;
        pos = null;

        return resultingImage;
    }

    /**
     * Re-grid the given stack with the given data position. The dataToRegrid Stack and positionData must have the same
     * number of elements.
     * Due to the  continuous motors value, interpolation have to be done. Warning Here, in some case two motors value are computed to be parts of the same
     * pixel. In this case the pixel value is divided by  two.
     * @param dataToRegrid RXImage, the data to regrid
     * @param positionDataX float[], the X position elements by elements
     * @param positionDataY float[], the Y position elements by elements
     * @return RXImage the resulting re-gridded imageStack
     */
    @Deprecated
    public RXImage regriding(RXImage dataToRegrid, float[] positionDataX, float[] positionDataY){
        if ( positionDataX == null ){
            positionDataX = new float[dataToRegrid.getWidth()*dataToRegrid.getHeight()*dataToRegrid.getSize()];
        }

        if ( positionDataY == null ){
            positionDataY = new float[dataToRegrid.getWidth()*dataToRegrid.getHeight()*dataToRegrid.getSize()];
        }

        return null;
//        return ShiftImage.regriding(dataToRegrid,positionDataX,positionDataY,interpolationDistance);
    }


    private static int[] initDimensionRegrid(float[] positionDataX, float[] positionDataY, float[] valMinMax_x, float[] valMinMax_y, int[] posMin_Max_x,
                             int[] posMin_Max_y, float[] step_x, float[] step_y, int width, int height, int size){

        int[] newSize = new int[2];

        if ( positionDataX != null  ){
            gridRange_w(positionDataX, width, height, size, posMin_Max_x, step_x);

            valMinMax_x[0] = positionDataX[posMin_Max_x[0]];
            valMinMax_x[1] = positionDataX[posMin_Max_x[1]];
            if ( step_x[0] == 0 ){
                initXpos(positionDataX, posMin_Max_x, step_x, width, height, size);
                valMinMax_x[0] = positionDataX[posMin_Max_x[0]];
                valMinMax_x[1] = positionDataX[posMin_Max_x[1]];
            }
        }
//        else{
//            //Warning BUG HERE new float will not Works
//            positionDataX = new float[width*height*size];
//            initXpos(positionDataX,posMin_Max_x,step_x,width,height,size);
//            valMinMax_x[0] = positionDataX[posMin_Max_x[0]];
//            valMinMax_x[1] = positionDataX[posMin_Max_x[1]];
//        }

        if ( positionDataY != null){
            gridRange_h(positionDataY, width, height, size, valMinMax_y, step_y);
            //Bug Occurs
            if ( step_y[0] == 0 ){
                initYpos(positionDataY,posMin_Max_y,step_y,width,height,size);
                valMinMax_y[0] = positionDataY[posMin_Max_y[0]];
                valMinMax_y[1] = positionDataY[posMin_Max_y[1]];
            }
        }
//        else{
//            //Warning BUG HERE new float will not Works
//            positionDataY = new float[width*height*size];
//            initYpos(positionDataY,posMin_Max_y,step_y,width,height,size);
//            valMinMax_y[0] = positionDataY[posMin_Max_y[0]];
//            valMinMax_y[1] = positionDataY[posMin_Max_y[1]];
//        }

        //Create a cropped resulting image which will be shorter than the original one
//        newWidth = (int)Math.floor(Math.abs((valMinMax_x[1] - valMinMax_x[0]) / step_x[0]));
//        newHeight = (int)Math.floor(Math.abs((valMinMax_y[1] - valMinMax_y[0]) / step_y[0]));

        newSize[0] = (int)Math.floor((valMinMax_x[1] - valMinMax_x[0]) / step_x[0]);
        newSize[1] = (int)Math.floor((valMinMax_y[1] - valMinMax_y[0]) / step_y[0]);

        if ( newSize[0] < 0 ){
            float tmp = valMinMax_x[1];
            valMinMax_x[1] = valMinMax_x[0];
            valMinMax_x[0] = tmp;
        }

        if ( newSize[1] < 0 ){
            float tmp = valMinMax_y[1];
            valMinMax_y[1] = valMinMax_y[0];
            valMinMax_y[0] = tmp;
        }


        newSize[0] = (int)Math.floor((valMinMax_x[1] - valMinMax_x[0]) / step_x[0]);
        newSize[1] = (int)Math.floor((valMinMax_y[1] - valMinMax_y[0]) / step_y[0]);

        return newSize;
    }

    /**
     * Create the motors image, Store in the same dataset Xmin, Xmax, Ymin, Ymax and mean real motors position
     * @param motors The RXImage containing data arranged as {motorX, motorY, motorZ}
     * @return The Mean image of position.
     */
    public static RXImage initRegrid(RXImage[] motors){
        return ShiftImage.initRegrid(motors[0].copyData(),motors[1].copyData(),motors[0].getWidth(),motors[0].getHeight(),motors[0].getSize());
    }

    /**
     * Create a Map of size 6 containings the motors Xmin/Xmax/Ymin/Ymax/Xreal/Yreal position. This map will be used to build the correct reconstruction
     * ROI when users will select it from the preprocessing interface ( usefull for Spectrum correction also ).
     * Be aware that only the Maximum position are save, thus regrid must Always be perform when reconstructing datas.
     * This map must be computed each type motors are add/removed.
     * @param positionDataX float[], the X position elements by elements
     * @param positionDataY float[], the Y position elements by elements
     * @return RXImage the resulting re-gridded imageStack
     */
    public static RXImage initRegrid(float[] positionDataX, float[] positionDataY, int width, int height, int size){
        float[] result  = null,pixelsToRegrid = null;
        int positionZReal,positionY,positionX,posLeft,posRight,positionYReal,newWidth,newHeight,newSize=size;
//        float step = 0.0f;
        float[] step_x = new float[1];
        float[] step_y = new float[1];
        int[] posMin_Max_x = new int[2];
        int[] posMin_Max_y = new int[2];
        int[] pos = new int[3];

        float[] valMinMax_x = new float[2];
        float[] valMinMax_y = new float[2];


        int[] newDims = initDimensionRegrid(positionDataX,positionDataY,valMinMax_x,valMinMax_y,posMin_Max_x,posMin_Max_y,step_x,step_y,width,height,size);

        newWidth = newDims[0];
        newHeight = newDims[1];

        System.err.println("New Width  = "+newWidth+"\t Step = "+step_x[0]);
        System.err.println("New Height = "+newHeight+"\t Step = "+step_y[0]);


        //Warning Special Case when only 1 Line in the final image
        int test = 1;
        if ( newHeight == 0 ){
            test = 2;
            newHeight = 1;
        }

//        int[] zeros = new int[newWidth*newHeight*size];
        RXImage resultingImage = new RXImage(newWidth,newHeight,6);

        newSize = newHeight*newWidth;
        result = new float[resultingImage.getGlobalSize()];
        Arrays.fill(result,0,newSize,Float.MAX_VALUE);
        Arrays.fill(result,newSize,2*newSize,Float.MIN_VALUE);
        Arrays.fill(result,2*newSize,3*newSize,Float.MAX_VALUE);
        Arrays.fill(result,3*newSize,4*newSize,Float.MIN_VALUE);
        Arrays.fill(result,3*newSize,5*newSize,Float.MIN_VALUE);
        Arrays.fill(result,3*newSize,6*newSize,Float.MIN_VALUE);
//        pixelsToRegrid = dataToRegrid.copyData();

        //Interpolation method, good when motor position are relatively correct
        if ( test == 0 ) {
            System.err.println("This Function is not implemented :: "+ShiftImage.class.toString()+" L.1096");
//            computeExtrapolation(resultingImage, dataToRegrid, width, height, newWidth, newHeight, posMin_Max_x, posMin_Max_y, step_x, step_y, positionDataX, positionDataY);
        }
        //Regrid methode, place each pixel in a new int position then interpolate value between each values
        else if ( test == 1 ) {
            System.err.println("Regrid function with both X and Y");
            int bugVal = 0;
            //Regridding
            //For each z position
            for (pos[2] = 0; pos[2] < size; pos[2]++) {
                //Real position in the [x,y,z] coordinate
                positionZReal = pos[2] * height * width;
                int positionZ = pos[2] * newHeight * newWidth;

                //For each y position
                for (pos[1] = 0; pos[1] < height; pos[1]++) {
                    //Real position in the [x,y,z] coordinate
                    positionYReal = pos[1] * width;
                    //For each x position
                    for (pos[0] = 0; pos[0] < width; pos[0]++) {
                        if (positionDataX[pos[0] + positionZReal + positionYReal] <= valMinMax_x[1] &&
                                positionDataY[pos[0] + positionZReal + positionYReal] <= valMinMax_y[1] &&
                                positionDataX[pos[0] + positionZReal + positionYReal] >= valMinMax_x[0] &&
                                positionDataY[pos[0] + positionZReal + positionYReal] >= valMinMax_y[0] ){

                            positionX = (int) ((positionDataX[pos[0] + positionZReal + positionYReal] - valMinMax_x[0]) / step_x[0]);
                            positionY = (int) ((positionDataY[pos[0] + positionZReal + positionYReal] - valMinMax_y[0]) / step_y[0]);

                            if (positionX >= 0 && positionX < newWidth &&
                                    positionY >= 0 && positionY < newHeight) {
                                positionY *= newWidth;

                                //Store the Min X position
                                result[positionX + positionY] = Math.min(pos[0],result[positionX + positionY]);
                                //Store the Max X position
                                result[positionX + positionY + newSize] = Math.max(pos[0],result[positionX + positionY + newSize]);
                                //Store the Min Y position
                                result[positionX + positionY + 2*newSize] = Math.min(pos[1],result[positionX + positionY + 2*newSize]);
                                //Store the Max Y position
                                result[positionX + positionY + 3*newSize] = Math.max(pos[1],result[positionX + positionY + 3*newSize]);

                                //Store the real position
                                result[positionX + positionY + 4*newSize] = positionDataX[pos[0] + positionZReal + positionYReal];
                                result[positionX + positionY + 5*newSize] = positionDataY[pos[0] + positionZReal + positionYReal];
                            }
                        }
                    }
                }
            }
            resultingImage.setData(result);
        }
        //Regrid the X position only
        else if ( test == 2 ) {
            System.err.println("Regrid function with X");
            int bugVal = 0;
            //Regridding
            //TODO Add a more restraining
            for ( pos[2] = 0 ; pos[2] < size  ; pos[2]++){
                positionZReal = pos[2]*height*width;
//                pixelsToRegrid  = dataToRegrid.copySlice(pos[2]);
//                result = new float[resultingImage.getWidth()*resultingImage.getHeight()];
                for (pos[1] =0 ; pos[1] < newHeight ; pos[1]++ ){
                    positionY =  pos[1]*newWidth;
                    positionYReal = pos[1]*width;
                    for ( pos[0] = 0 ; pos[0] < width ; pos[0]++ ){
                        if ( (pos[0] + positionZReal + positionYReal) < positionDataX.length ) {
//                    toAdd += x
                            if (positionDataX[pos[0] + positionZReal + positionYReal] <= positionDataX[posMin_Max_x[1]]) {
                                positionX = (int) ((positionDataX[pos[0] + positionZReal + positionYReal] - positionDataX[posMin_Max_x[0]]) / step_x[0]);
                                if (positionX >= 0 && positionX < newWidth) {
                                    //Store the Min X position
                                    result[positionX + positionY] = Math.min(pos[0],result[positionX + positionY]);
                                    //Store the Max X position
                                    result[positionX + positionY + newSize] = Math.max(pos[0],result[positionX + positionY + newSize]);
                                    //Store the Min Y position
                                    result[positionX + positionY + 2*newSize] = Math.min(pos[1],result[positionX + positionY + 2*newSize]);
                                    //Store the Max Y position
                                    result[positionX + positionY + 3*newSize] = Math.max(pos[1],result[positionX + positionY + 3*newSize]);

                                    //Store the real position
                                    result[positionX + positionY + 4*newSize] = positionDataX[pos[0] + positionZReal + positionYReal];
//                                    result[positionX + positionY + 5*newSize] = positionDataY[pos[0] + positionZReal + positionYReal];
                                }
                            }
                        }
                    }
                }
            }
            resultingImage.setData(result);
        }

        //Realise ressource
        step_x = null;
        step_y = null;
        posMin_Max_x = null;
        posMin_Max_y = null;
        pos = null;

        return resultingImage;
    }


    public static int[][] getCorrectedReconstructionROI(int[] initRoi, RXImage positionImage){
        int[][] formatedROI = new int[3][2];
        formatedROI[0][0] = initRoi[0];
        formatedROI[0][1] = initRoi[1];
        formatedROI[1][0] = initRoi[2];
        formatedROI[1][1] = initRoi[3];
        formatedROI[2][0] = initRoi[4];
        formatedROI[2][1] = initRoi[5];
        return getCorrectedReconstructionROI(formatedROI, positionImage);
    }

    /**
     * Conversion of the ROI created in a reconstructed image and the position in the hdf5 file
     * @param initRoi The Given reconstruction ROI as display in a real image ( whitout motors correction )
     * @param positionImage The motors position image, this image is contituted by 4 slice ( xmin, xmax, ymin, ymax )
     * @return int[] the bounds of the image to retrieve to have the corresponding full field of view
     */
    public static int[][] getCorrectedReconstructionROI(int[][] initRoi, RXImage positionImage){
        int[][] bounds = new int[3][2];
        bounds[0][0] = Integer.MAX_VALUE;
        bounds[0][1] = Integer.MIN_VALUE;
        bounds[1][0] = Integer.MAX_VALUE;
        bounds[1][1] = Integer.MIN_VALUE;
        //Do not modify the Z position
        bounds[2][0] = initRoi[2][0];
        bounds[2][1] = initRoi[2][1];

        float[] positionData = positionImage.copyData();
        int zAdd = positionImage.getHeight()*positionImage.getWidth();
        //Only 4 z position in the ImagePosition Data ( xmin,xmax,ymin,ymax )
        for ( int y = initRoi[1][0]; y < initRoi[1][1]; y++){
            for ( int x = initRoi[0][0]; x < initRoi[0][1]; x++){
                bounds[0][0] = Math.min((int) positionData[x+y*positionImage.getWidth()],bounds[0][0]);
                bounds[0][1] = Math.max((int) positionData[x + y * positionImage.getWidth()+zAdd],bounds[0][1]);
                bounds[1][0] = Math.min((int) positionData[x+y*positionImage.getWidth()+2*zAdd],bounds[1][0]);
                bounds[1][1] = Math.max((int) positionData[x + y * positionImage.getWidth()+3*zAdd],bounds[1][1]);
            }
        }
        if ( bounds[1][0] == bounds[1][1] ){
            bounds[1][1] += 1;
        }

        if ( bounds[0][0] == bounds[0][1] ){
            bounds[0][1] += 1;
        }

        if ( bounds[2][0] == bounds[2][1] ){
            bounds[2][1] += 1;
        }

        return bounds;
    }

    @Deprecated
    private static void computeExtrapolation(RXImage resultingImage, RXImage dataToRegrid, int width, int height, int regridWidth, int regridHeight,
                                      int[] posMin_Max_x, int[] posMin_Max_y, float[] step_x, float[] step_y, float[] positionDataX,
                                      float[] positionDataY){
        int z,y,x,positionZ,positionYReal,bugVal=0;
        double positionX,positionY;
        float[] pixelsToRegrid;
        float[] result;

        float[] map = new float[resultingImage.getWidth()*resultingImage.getHeight()];
        for ( z = 0 ; z < resultingImage.getSize()  ; z++){
            positionZ = z*height*width;
            pixelsToRegrid  = dataToRegrid.copySlice(z);
            result = new float[resultingImage.getWidth()*resultingImage.getHeight()];
            for (y =0 ; y < height ; y++ ){
//                positionY =  y*regridHeight;
                positionYReal = y*width;
                for ( x = 0 ; x < width ; x++ ){
//                    toAdd += x
//                    if ( positionDataX[x + positionZ + positionYReal] <= positionDataX[posMin_Max_x[1]] &&
//                            positionDataY[x + positionZ + positionYReal] <= positionDataY[posMin_Max_y[1]]){
                        positionX = (positionDataX[x + positionZ + positionYReal] - positionDataX[posMin_Max_x[0]]) / step_x[0];
                        positionY = (positionDataY[x + positionZ + positionYReal] - positionDataY[posMin_Max_y[0]]) / step_y[0];

                        ComputationUtils.setBilinearInterpolationValue(positionX,positionY,regridWidth,pixelsToRegrid[x + positionYReal],result);
                        ComputationUtils.setBilinearInterpolationValue(positionX,positionY,regridWidth,1.0f,map);
//                    }
                }
            }

            if ( bugVal > 0 ){
                System.err.println("Bug value already assigned "+ShiftImage.class.toString()+" : "+bugVal);
            }

            ComputationUtils.arrayDivide(result,map);

            resultingImage.setSlice(result,z);
        }
    }

    /**
     * Launch the mirroring function with the parameter saved in the MirroringType Object.
     * @param toMirror The image to mirror.
     * @param type The Transformation to apply
     * @return RXImage the resulting mirrored RXImage
     */
    public static RXImage mirroring(RXImage toMirror,MirroringType type){
        return mirroring(toMirror,type.getSquare()[0],type.getSquare()[1],type.getSquare()[2],type.getSquare()[3]);
    }


    @Deprecated
    public static RXImage changeMirroring(RXImage stack, MirroringType from, MirroringType to){
        RXImage result=null;
        switch (from){
            case MDI:
                //Crop the rigth bottom corner
                result = ComputationUtils.cropInStack(stack,stack.getWidth()/2,stack.getHeight()/2,1,stack.getWidth()/2,stack.getHeight()/2,stack.getSize());
                break;
            case ASDI_X:
                //Crop the rigth bottom corner
                result = ComputationUtils.cropInStack(stack,stack.getWidth()/2,stack.getHeight()/2,1,stack.getWidth()/2,stack.getHeight()/2,stack.getSize());
                break;
            case ASDI_Y:
                //Crop the rigth bottom corner
                result = ComputationUtils.cropInStack(stack,stack.getWidth()/2,stack.getHeight()/2,1,stack.getWidth()/2,stack.getHeight()/2,stack.getSize());
                break;
        }

        System.err.println("Why normalization here "+ShiftImage.class.toString()+" l.1369");
        BackgroundSubtraction backgroundSubtraction = new BackgroundSubtraction();
        backgroundSubtraction.computeNormalization(result, true);

        return mirroring(result,to);
    }


    /**
     * Create a mirroring image based on the imageStack toMirror. The flip are designed by the int given
     * for each squarred position.
     * @param toMirror The image to mirror.
     * @param square_bottom_left The transformation to apply on the bottom left square
     * @param square_bottom_right The transformation to apply on the bottom right square
     * @param square_top_left The transformation to apply on the top left square
     * @param square_top_right The transformation to apply on the top right square
     * @return RXImage the resulting mirrored RXImage
     * @see com.soleil.nanoscopium.rxtomoj.model.ShiftImage
     */
//    0 = (+  +); 1 = ( -  - ) ; 2 = ( -  + ); 3 = ( +  - );
//    4 = -0 ; 5 = - 1 ; 6 = - 2 ; 7 = - 3;
    public static RXImage mirroring(RXImage toMirror, int square_top_left, int square_top_right, int square_bottom_left, int square_bottom_right){
        RXImage mirrorStack = new RXImage(toMirror.getWidth()*2,toMirror.getHeight()*2,toMirror.getSize());
        int x,y,z;
        float[] data,original;
        int[] square_orientation = {square_top_left,square_top_right,square_bottom_left,square_bottom_right};

        original = toMirror.copyData();
        data = new float[mirrorStack.getWidth()*mirrorStack.getHeight()*mirrorStack.getSize()];
        for ( z = 0 ; z < toMirror.getSize() ; z++ ){
            int posZ = z*toMirror.getHeight()*toMirror.getWidth();
            int posZData = z*mirrorStack.getWidth()*mirrorStack.getHeight();
//            data = (float[]) mirrorStack.getPixels(z);
            for ( int square = 0 ; square < 4 ; square++){
                int a = toMirror.getWidth()*(square%2);
                int b = toMirror.getHeight()*(square/2);
                int xPos;
                int yPos;
                switch (square_orientation[square]){
                    case MIRRORING_NEG_FLIP_FLOP:
                        for ( y = 0 ; y < toMirror.getHeight() ; y++ ){
                            for ( x = 0; x < toMirror.getWidth() ; x++){
                                xPos = (toMirror.getWidth()-1) - x + a;
                                yPos = (toMirror.getHeight()-1) - y + b;
                                data[xPos+yPos*mirrorStack.getWidth()+posZData] = - original[x+y*toMirror.getWidth()+posZ];
                            }
                        }
                        break;
                    case MIRRORING_FLIP_FLOP:
                        for ( y = 0 ; y < toMirror.getHeight() ; y++ ){
                            for ( x = 0; x < toMirror.getWidth() ; x++){
                                xPos = (toMirror.getWidth()-1) - x + a;
                                yPos = (toMirror.getHeight()-1) - y + b;
                                data[xPos+yPos*mirrorStack.getWidth()+posZData] = original[x+y*toMirror.getWidth()+posZ];
                            }
                        }
                        break;
                    case MIRRORING_NEG_FLIP:
                        for ( y = 0 ; y < toMirror.getHeight() ; y++ ){
                            for ( x = 0 ; x < toMirror.getWidth() ; x++ ){
                                xPos = (toMirror.getWidth()-1)-x +a;
                                yPos = y + b;
                                data[xPos+yPos*mirrorStack.getWidth()+posZData] = -original[x+y*toMirror.getWidth()+posZ];
                            }
                        }
                        break;
                    case MIRRORING_FLIP:
                        for ( y = 0 ; y < toMirror.getHeight() ; y++ ){
                            for ( x = 0 ; x < toMirror.getWidth() ; x++ ){
                                xPos = (toMirror.getWidth()-1)-x +a;
                                yPos = y + b;
                                data[xPos+yPos*mirrorStack.getWidth()+posZData] = original[x+y*toMirror.getWidth()+posZ];
                            }
                        }
                        break;
                    case MIRRORING_NEG_FLOP:
                        for ( y = 0 ; y < toMirror.getHeight() ; y++ ){
                            yPos = (toMirror.getHeight()-1) - y + b;
                            System.arraycopy(original,y*toMirror.getWidth()+posZ,data,a+yPos*mirrorStack.getWidth()+posZData,toMirror.getWidth());
                            for ( x = a+yPos*mirrorStack.getWidth()+posZData ; x < a+yPos*mirrorStack.getWidth() + toMirror.getWidth() + posZData ; x++ ){
                                data[x] = -data[x];
                            }
                        }
                        break;
                    case MIRRORING_FLOP:
                        for ( y = 0 ; y < toMirror.getHeight() ; y++ ){
                            yPos = (toMirror.getHeight()-1) - y + b;
                            System.arraycopy(original,y*toMirror.getWidth()+posZ,data,a+yPos*mirrorStack.getWidth()+posZData,toMirror.getWidth());
                        }
                        break;
                    case MIRRORING_NEG_NULL:
                        for ( y = 0 ; y < toMirror.getHeight() ; y++ ){
                            yPos = y + b;
                            System.arraycopy(original,y*toMirror.getWidth()+posZ,data,a+yPos*mirrorStack.getWidth()+posZData,toMirror.getWidth());
                            for ( x = a+yPos*mirrorStack.getWidth()+posZData ; x < toMirror.getWidth() + a+yPos*mirrorStack.getWidth()+posZData ; x++ ){
                                data[x] = -data[x];
                            }
                        }
                        break;
                    case MIRRORING_NULL:
                        for ( y = 0 ; y < toMirror.getHeight() ; y++ ){
                            yPos = y + b;
                            System.arraycopy(original,y*toMirror.getWidth()+posZ,data,a+yPos*mirrorStack.getWidth()+posZData,toMirror.getWidth());
                        }
                        break;
                    default:
                        System.err.println("Warning unrecognized mirroring Type : ShiftImage l1469");
                        break;
                }
            }
        }
        mirrorStack.setData(data);
        return mirrorStack;
    }

    /**
     * Compute the correlation between reference and moving array
     * @param reference 1D reference array
     * @param moving 1D moving array
     */
//    @TODO warning this function do nothings
    public void correlationFFT1D(float[] reference, float[] moving){
//        boolean withPadding
//        if ( withPadding ){
//            float[] padData0 = new float[reference.length+reference.length/4];
//            System.arraycopy(reference,0,padData0,padData0.length/8,reference.length);
//
//            float[] padData1 = new float[reference.length+reference.length/4];
//            System.arraycopy(data1,0,padData1,padData1.length/8,data1.length);
//
//            DenseFloatMatrix1D denseFloatMatrix1D_0  = new DenseFloatMatrix1D(padData0);
//            DenseFloatMatrix1D denseFloatMatrix1D_1 = new DenseFloatMatrix1D(padData1);
//
//            DenseFComplexMatrix1D denseFComplexMatrix1D_0 = denseFloatMatrix1D_0.getFft();
//            DenseFComplexMatrix1D denseFComplexMatrix1D_1 = denseFloatMatrix1D_1.getFft();
//
//            denseFComplexMatrix1D_0.assign(denseFComplexMatrix1D_1, FComplexFunctions.multConjSecond);
//            denseFComplexMatrix1D_0.ifft(true);
//
//            FloatMatrix1D floatMatrix1D = denseFComplexMatrix1D_0.getRealPart();
//            float[] test = floatMatrix1D.getMaxLocation();
//        }

        DenseFComplexMatrix1D denseFloatMatrix1D_0  = new DenseFComplexMatrix1D(new DenseFloatMatrix1D(reference));
        DenseFComplexMatrix1D denseFloatMatrix1D_1 = new DenseFComplexMatrix1D(new DenseFloatMatrix1D(moving));

        denseFloatMatrix1D_0.fft();
        denseFloatMatrix1D_1.fft();

        denseFloatMatrix1D_0.assign(denseFloatMatrix1D_1, FComplexFunctions.multConjSecond);
        denseFloatMatrix1D_0.ifft(true);

        FloatMatrix1D floatMatrix1D = denseFloatMatrix1D_0.getRealPart();
        float[] test = floatMatrix1D.getMaxLocation();
//        System.out.println("Max Loc")
        System.err.println("Error Not implemented Yet : "+ this.getClass().toString() );
    }



    /**
     * Compute the shift to apply on the moving array to fit the reference array
     * @param reference 1D reference array
     * @param moving 1D moving array
     * @param width the width of the image represented by the 1D arrays
     * @param height the height of the image represented by the 1D arrays
     * @param position the position in the size of the image represented by the 1D arrays
     * @return The movement to apply to the moving stack to fit the reference
     */
    public static float[] correlationFFT2D(float[] reference, float[] moving, int width, int height, int position){
        return correlationFFT2D(reference,moving,width,height,position,true);
    }

    /**
     * Compute the shift to apply on the moving array to fit the reference array
     * @param reference 1D reference array
     * @param moving 1D moving array
     * @param width the width of the image represented by the 1D arrays
     * @param height the height of the image represented by the 1D arrays
     * @param position the position in the size of the image represented by the 1D arrays
     * @param doShift True if the shift has to be perform
     * @return The movement to apply to the moving stack to fit the reference
     */
    public static float[] correlationFFT2D(float[] reference, float[] moving, int width, int height, int position, boolean doShift){
        DenseFloatMatrix2D denseFloatMatrix2D_0 = new DenseFloatMatrix2D(height,width);
        DenseFloatMatrix2D denseFloatMatrix2D_1 = new DenseFloatMatrix2D(height,width);

        denseFloatMatrix2D_0.assign(reference);
        denseFloatMatrix2D_1.assign(Arrays.copyOfRange(moving,position*width*height,width*height*(position+1)));

        DenseFComplexMatrix2D denseFloatComplexMatrix2D_0  = new DenseFComplexMatrix2D(denseFloatMatrix2D_0);
        DenseFComplexMatrix2D denseFloatComplexMatrix2D_1 = new DenseFComplexMatrix2D(denseFloatMatrix2D_1);

        denseFloatComplexMatrix2D_0.fft2();
        denseFloatComplexMatrix2D_1.fft2();

        denseFloatComplexMatrix2D_0.assign(denseFloatComplexMatrix2D_1,FComplexFunctions.multConjSecond);
        denseFloatComplexMatrix2D_0.ifft2(true);

        FloatMatrix2D floatMatrix2D = denseFloatComplexMatrix2D_0.getRealPart();
        //Retrieve the maximum
        float[] test = floatMatrix2D.getMaxLocation();
//        System.err.println("Error Not implemented Yet : "+ this.getClass().toString() );
//        System.err.println("Shift ::   ["+test[1]+" , "+test[2]+"]");

        int shiftX  = 0;
        int shiftY = 0;
        //Test on Maximum position
        if ( test[2] > width/2.0f ){
            shiftX = (int) (test[2] - width);
        }else{
            shiftX = (int) test[2];
        }

        if ( test[1] > height/2.0f ){
            shiftY = (int) (test[1] - height);
        }else{
            shiftY = (int) test[1];
        }

        if ( doShift ) {
            //Create a tmp  data set for copying purpose
            //For each ligne copy
            float[] tmp = new float[width];
            //For Copying the entire data
            float[] tmpShift = new float[moving.length];

            //If shiftX > 0 et shiftY >0 then positive shift on each row/colon
            if (shiftX >= 0 && shiftY >= 0) {
                for (int y = 0; y < height - shiftY; y++) {
                    //Copy from data1   to tmp  the row with
                    System.arraycopy(moving, y * width, tmp, shiftX, width - shiftX);
                    //Copy the row in the global tmpShift array
                    System.arraycopy(tmp, 0, tmpShift, (y + shiftY) * width, width);
                }
            }
            //If shiftX < 0 et shiftY <0 then negative shift on each row/colon
            else if (shiftX < 0 && shiftY < 0) {
                for (int y = Math.abs(shiftY); y < height + shiftY; y++) {
                    System.arraycopy(moving, Math.abs(shiftX) + y * width, tmp, 0, width - Math.abs(shiftX));
                    System.arraycopy(tmp, 0, tmpShift, (y + shiftY) * width, width);
                }
            }
            //If shiftX <0 and shiftY > 0 then positive shift on row and negative on col
            else if (shiftX < 0 && shiftY >= 0) {
                for (int y = 0; y < height - shiftY; y++) {
                    System.arraycopy(moving, Math.abs(shiftX) + y * width, tmp, 0, width - Math.abs(shiftX));
                    System.arraycopy(tmp, 0, tmpShift, (y + shiftY) * width, width);
                }
            }
            //If ShiftX > 0 and shiftY < 0 then positive shift on row and negative on col
            else if (shiftX >= 0 && shiftY < 0) {
                for (int y = Math.abs(shiftY); y < height + shiftY; y++) {
                    System.arraycopy(moving, y * width, tmp, shiftX, width - shiftX);
                    System.arraycopy(tmp, 0, tmpShift, (y + shiftY) * width, width);
                }
            }

            System.arraycopy(tmpShift, 0, moving, 0, moving.length);
        }
        //Debug Mode display Shift
//        System.out.println("Shift X :: "+shiftX);
//        System.out.println("Shift Y :: " + shiftY);
//        shiftingY[position] = shiftY;
//        shiftingX[position] = shiftX;
        return new float[]{shiftX,shiftY};
    }

    /**
     * Compute the correlation Between Every contiguous stack in ascending order.
     * @param stack RXImage, the stack to be Modified.
     */
    public void correlationFFT2D(RXImage stack){
        shiftingX = new float[stack.getSize()-1];
        shiftingY = new float[stack.getSize()-1];
        this.realValueX  =  new float[stack.getSize()-1];
        //Generate a copy of the stack
//        RXImage result = ComputationUtils.copyStack(stack);

        for ( int i =0 ; i < stack.getSize() - 1; i ++ ){
            this.realValueX[i] = i;
            System.err.println("Will not Work because Stack are just copy");
            this.correlationFFT2D(stack.copySlice(i + 1), stack.copySlice(i + 2), stack.getWidth(), stack.getHeight(), i);
        }

//        this.plotFit  = new Plot("Correlation","X","Y");
//        this.plotFit.setLimits(0,stack.getSize(),-20,20);
//        this.plotFit.setColor(Color.blue);
//        this.plotFit.addPoints(this.realValueX,shiftingX,Plot.LINE);
//        this.plotFit.setColor(Color.red);
//        this.plotFit.addPoints(this.realValueX,shiftingY,Plot.LINE);
//        this.plotFit.draw();
    }


    public void setMinimumShift(float minimumShift) {
        this.minimumShift = minimumShift;
    }

    public void setInterpolate(boolean interpolate) {
        this.interpolate = interpolate;
    }

    public void setRecursiveNumber(int recursiveNumber) {
        this.recursiveNumber = recursiveNumber;
    }


    public double getCenterOfRotation(){
        double tmp = 0;
        boolean bol = false;
        double min = Double.MAX_VALUE;
        double max = Double.MIN_VALUE;
        double[] sineParameters = this.fittingUtils.getSineParameters();
        if ( sineParameters != null ){
            for ( int i = 0 ; i < this.realValueX.length ; i++) {
                bol = true;
                tmp = (fittingUtils.computeSine(sineParameters[0], sineParameters[1], sineParameters[2], sineParameters[3], (double) this.realValueX[i]));
                if ( tmp < min){
                    min = tmp;
                }
                if ( tmp > max ){
                    max = tmp;
                }
            }
            return (max+min)/2.0f;
        }

        return -1;
    }

    /**
     * Test whenever the Shift Function can be called or not.
     * @return true if every data is present.
     */
    public boolean canShift(){
        if ( realValueX == null ){
            return false;
        }
        if ( realValueY == null ){
            return false;
        }
        return this.fittingUtils.isUpdated();
    }

    public FittingUtils getFittingUtils() {
        return fittingUtils;
    }

    public void setFittingUtils(FittingUtils fittingUtils) {
        this.fittingUtils = fittingUtils;
    }

    public float[] getShifting() {
        return shifting;
    }
}
