/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.filters;


import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rxtomoj.model.data.NormalizationInfo;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;

/**
 * Created by bergamaschi on 18/06/2014.
 */
public class BackgroundSubtraction {
    private float variationCoefficient = 0.35f;
    private float numberOfForeseenPixel = 2;
    private float[] mean;

    private float[] posType;
    private float[] posObject;

    private boolean computeLeft = true;
    private boolean computeRigth = true;

    private boolean useMobileMean = true;
    private int mobileMeanSize = 8;

    public BackgroundSubtraction(){
        super();
    }

    public BackgroundSubtraction(NormalizationInfo normalizationInfo){
        super();
        this.setComputeRigth(normalizationInfo.isComputeRigth());
        this.setComputeLeft(normalizationInfo.isComputeLeft());
        this.setVariationCoefficient(normalizationInfo.getVariationCoefficient());
        this.setNumberOfForeseenPixel(normalizationInfo.getNumberOfForeseenPixel());
    }

    private float[][] computeNormalization_l(RXImage imageStack){
        int z,k,i,j,forseenPixel;
        float tmp_meanIntensity=0,tmp_sumIntensity=0,
                meanIntensity=0, growthCoef,tmp_val,
                tmp_growthcoef;

        float[][] result = new float[imageStack.getSize()*imageStack.getHeight()][2];

        float[] absorptionImage = imageStack.copyData();
        for ( z = 0; z < imageStack.getSize() ; z++) {
            int posZ = z*imageStack.getWidth()*imageStack.getHeight();
            //For each Line
            for ( k = 0 ; k < imageStack.getHeight() ; k++) {
                tmp_sumIntensity = absorptionImage[k*imageStack.getWidth()+posZ];
                tmp_meanIntensity = tmp_sumIntensity;
                meanIntensity = tmp_meanIntensity;

                for (i = k*imageStack.getWidth() + 1 + posZ; i < ((k+1) * imageStack.getWidth()) - numberOfForeseenPixel + posZ; i++) {

                    tmp_val = absorptionImage[i-1];
                    growthCoef = 0;
                    tmp_sumIntensity += absorptionImage[i];
                    tmp_meanIntensity = tmp_sumIntensity / ((i+1) -  (k*imageStack.getWidth()) );

                    for (forseenPixel = 0; forseenPixel < numberOfForeseenPixel; forseenPixel++) {
//                        tmp_growthcoef = 1 - (tmp_val / (absorptionImage[i + forseenPixel]));
                        tmp_growthcoef = (tmp_val - (absorptionImage[i + forseenPixel]))/tmp_val;
//                        if ( tmp_growthcoef < variationCoefficient ){
                            growthCoef += tmp_growthcoef;
//                        }
                    }

                    //We considere to be entered in the object
                    if (Math.abs(growthCoef) > variationCoefficient) {
                        result[k + z * imageStack.getHeight()][1] = tmp_meanIntensity;
                        result[k+z*imageStack.getHeight()][0] = i - (k*imageStack.getWidth()) - posZ;

                        System.out.println("Left -- Line :: "+k+"  Variation detected at X = " + (i - (k*imageStack.getWidth()) - posZ) );
                        break;
                    }
                }
                //IF NO OBJECT FOUND
                if ( result[k+z*imageStack.getHeight()][0] == 0 &&  result[k + z * imageStack.getHeight()][1] == 0){
                    result[k+z*imageStack.getHeight()][0] = imageStack.getWidth()-1;
                    for (int h = k*imageStack.getWidth() + 1; h < ((k+1) * imageStack.getWidth()) - numberOfForeseenPixel; h++) {
                        result[k + z * imageStack.getHeight()][1] += absorptionImage[h]/imageStack.getWidth();
                    }
                }
            }
        }

        return result;
    }



    private float[][] computeNormalization_r(RXImage imageStack){
        int z,k,i,j,forseenPixel,counter;
        float tmp_meanIntensity=0,tmp_sumIntensity=0,
                meanIntensity=0, growthCoef,tmp_val = 0,
                tmp_growthcoef;

        float[][] result = new float[imageStack.getHeight()*imageStack.getSize()][2];

        float[] absorptionImage = imageStack.copyData();

        for ( z = 0; z < imageStack.getSize() ; z++) {
            int posZ = z*imageStack.getHeight()*imageStack.getWidth();
            //For each Line
            for ( k = 0 ; k < imageStack.getHeight(); k++) {
                tmp_sumIntensity = absorptionImage[(k+1)*imageStack.getWidth() - 1 + posZ];
                counter = 1;
                for (i = ((k+1) * imageStack.getWidth()) - 2 + posZ; i > k*imageStack.getWidth() + numberOfForeseenPixel + posZ; i--) {
                    counter++;
                    tmp_val = absorptionImage[i+1];
                    growthCoef = 0;
                    tmp_sumIntensity += absorptionImage[i];
                    tmp_meanIntensity = tmp_sumIntensity / (float)counter;

                    for (forseenPixel = 0; forseenPixel < numberOfForeseenPixel; forseenPixel++) {
//                        tmp_growthcoef = 1 - (tmp_val / (absorptionImage[i - forseenPixel]));
                        tmp_growthcoef = (tmp_val - (absorptionImage[i - forseenPixel]))/tmp_val;
//                        if ( Math.abs(tmp_growthcoef) < variationCoefficient ){
                            growthCoef += tmp_growthcoef;
//                        }
                    }


                    //We considere to be entered in the object
                    if (Math.abs(growthCoef) > variationCoefficient) {

                        result[k + z * imageStack.getHeight()][1] = tmp_meanIntensity;
                        result[k+z*imageStack.getHeight()][0] = i - (k*imageStack.getWidth()) - posZ;

                        //Debug purpose
                        System.out.println("Right -- Line :: "+k+"  Variation detected at X = " + (i - (k*imageStack.getWidth()) - posZ) );
//                        absorptionImage[i] =-1;
                        break;
                    }
                }
                //IF NO OBJECT FOUND
                if ( result[k+z*imageStack.getHeight()][0] == 0 &&  result[k + z * imageStack.getHeight()][1] == 0){
                    result[k+z*imageStack.getHeight()][0] = imageStack.getWidth()-1;
                    for (int h = ((k+1) * imageStack.getWidth()) - 2; h > k*imageStack.getWidth() + numberOfForeseenPixel; h--) {
                        result[k + z * imageStack.getHeight()][1] += absorptionImage[h]/imageStack.getWidth();
                    }
                }
            }
        }
        return result;
    }

    public void computeNormalization(RXImage imageStack){
        computeNormalization(imageStack,false);
    }

    public void computeSubtraction(RXImage imageStack){
        this.subtract(this.posObject, this.posType, imageStack);
    }

    public void computeNormalization(RXImage imageStack,boolean withNormalization){
        if ( this.posObject == null || this.posType == null ){
            if ( useMobileMean ){
                ComputationUtils.computeLinearMobileMean(imageStack,mobileMeanSize);
            }

            computeNormalization(imageStack,this.computeLeft,this.computeRigth,withNormalization);
        }else {
            this.normalize(this.posObject,this.posType, imageStack);
        }
    }

    public void computeNormalization(RXImage imageStack, boolean leftSide, boolean rigthSide, boolean withMean){
//        float[] result_pos = new float[imageStack.getHeight()*imageStack.getSize()];
//        int[] type_pos = new int[imageStack.getHeight()*imageStack.getSize()];
        float[] mean = new float[imageStack.getHeight()*imageStack.getSize()];
        float[] position = new float[imageStack.getHeight()*imageStack.getSize()];
        float[] type = new float[imageStack.getHeight()*imageStack.getSize()];

        if ( leftSide && rigthSide ){
            float[][] leftPos = computeNormalization_l(imageStack);
            float[][] rightPos = computeNormalization_r(imageStack);


            for ( int i = 0 ; i < leftPos.length ; i++ ){
                if ( leftPos[i][1] > rightPos[i][1] ){
                    mean[i] = leftPos[i][1];
                    position[i] = leftPos[i][0];
                    type[i] = 0;
                }else{
                    mean[i] = rightPos[i][1];
                    position[i] = rightPos[i][0];
                    type[i] = 1;
                }
            }
        }
        else if ( leftSide ){
            float[][] leftPos = computeNormalization_l(imageStack);
            for ( int i = 0 ; i < leftPos.length ; i++ ){
                mean[i] = leftPos[i][1];
                position[i] = leftPos[i][0];
                type[i] = 0;
            }
        }
        else if ( rigthSide ){
            float[][] rightPos = computeNormalization_r(imageStack);
            for ( int i = 0 ; i < rightPos.length ; i++ ){
                mean[i] = rightPos[i][1];
                position[i] = rightPos[i][0];
                type[i] = 1;
            }
        }


        this.mean = mean;
        this.posType = type;
        this.posObject = position;

        normalize(imageStack,withMean);
    }

    private void subtract(RXImage imageStack, boolean withMean){
        if ( withMean ){
            this.subtract(this.mean, imageStack);
        }else{
            this.subtract(this.posObject, this.posType, imageStack);
        }
    }

    private void subtract(float[] mean, RXImage imageStack){
        imageStack.computeData(data->{
            for ( int i = 0; i < data.length ; i++){
                data[i] -= mean[i];
            }
        });
    }

    private void subtract(float[] backgroundLimits, float[] left_right, RXImage imageStack){
        imageStack.computeData(data->{
            float meanIntensity = 1;
            int currentPosition = 0;

//            float[] absorptionImage = imageStack.copyData();

            for ( int z = 0 ; z < imageStack.getSize() ; z++ ) {
                int posZ = z*imageStack.getHeight()*imageStack.getWidth();
                for ( int y = 0 ; y < imageStack.getHeight() ; y++ ) {
                    meanIntensity = 0;
                    currentPosition = y + z*imageStack.getHeight();
                    switch ((int) left_right[currentPosition]) {
                        //Compute from left to right (the position)
                        case 0:

                            for (int j = y * imageStack.getWidth() + posZ; j < (y * imageStack.getWidth()) + backgroundLimits[currentPosition]  + posZ; j++) {
                                meanIntensity += data[j];
                            }

                            meanIntensity /= backgroundLimits[currentPosition];
                            break;
                        //Compute from right to left (the position)
                        case 1:

                            for (int j = (int) ((y * imageStack.getWidth()) + backgroundLimits[currentPosition] + posZ); j < (y+1) * imageStack.getWidth() + posZ; j++) {
                                meanIntensity += data[j];
                            }

                            meanIntensity /= (imageStack.getWidth() - backgroundLimits[currentPosition]);
                            break;
                        default:
                            System.err.println("Error");
                            break;
                    }

                    for (int j = y * imageStack.getWidth() + posZ; j < (y + 1) * imageStack.getWidth() + posZ; j++) {
                        data[j] -= meanIntensity;
                    }

//Debug Only
//                data[((int) (y * imageStack.getWidth() + backgroundLimits[currentPosition]))] = -1;
                }
            }
        });
    }


    /**
     * Normalize With the Mean intensity for each Row
     * @param imageStack The imae stack to normalize
     * @param withMean Use the mean value instead of the interval
     */
    private void normalize(RXImage imageStack, boolean withMean){
        if ( withMean ){
            this.normalize(this.mean,imageStack);
        }else{
            this.normalize(this.posObject,this.posType,imageStack);
        }
    }

    /**
     * Normalize With the Mean intensity for each Row
     * @param mean The array containing the mean intensity for each row
     * @param imageStack The imae stack to normalize
     */
    private void normalize(float[] mean, RXImage imageStack){
        imageStack.computeData(data->{
            for ( int z = 0 ; z < imageStack.getSize() ; z++ ) {
                int posZ  = z * imageStack.getWidth()*imageStack.getHeight();
                for ( int y = 0 ; y < imageStack.getHeight() ; y++ ) {
                    int currentPosition = y + z*imageStack.getHeight();
                    for (int j = y * imageStack.getWidth() + posZ; j < (y + 1) * imageStack.getWidth() + posZ; j++) {
                        data[j] /= mean[currentPosition];
                    }
                }
            }
        });
    }

    /**
     * Normalize With the Position detected as the object border
     * @param backgroundLimits The array containing the position of the supposed object
     * @param left_right Is the position at the right or the left
     * @param imageStack The image stack to normalize
     */
    private void normalize(float[] backgroundLimits, float[] left_right, RXImage imageStack){
        imageStack.computeData(data->{
            float meanIntensity = 1;
            int currentPosition = 0;
            for ( int z = 0 ; z < imageStack.getSize() ; z++ ) {
//                float[] absorptionImage = (float[]) imageStack.getPixels(z+1);
                int posZ = z*imageStack.getHeight()*imageStack.getWidth();
                for ( int y = 0 ; y < imageStack.getHeight() ; y++ ) {
                    meanIntensity = 1;
                    currentPosition = y + z*imageStack.getHeight();
                    switch ((int) left_right[currentPosition]) {
                        //Compute from left to right (the position)
                        case 0:

                            for (int j = y * imageStack.getWidth() + posZ; j < (y * imageStack.getWidth()) + backgroundLimits[currentPosition]  + posZ; j++) {
                                meanIntensity += data[j];
                            }

                            meanIntensity /= backgroundLimits[currentPosition];
                            break;
                        //Compute from right to left (the position)
                        case 1:

                            for (int j = (int) ((y * imageStack.getWidth()) + backgroundLimits[currentPosition] + posZ); j < (y+1) * imageStack.getWidth() + posZ; j++) {
                                meanIntensity += data[j];
                            }

                            meanIntensity /= (imageStack.getWidth() - backgroundLimits[currentPosition]);

                            break;
                        default:
                            System.err.println("Error");
                            break;
                    }

                    for (int j = y * imageStack.getWidth() + posZ; j < (y + 1) * imageStack.getWidth() + posZ; j++) {
                        data[j] /= meanIntensity;
                    }

//Debug Only
//                data[((int) (y * imageStack.getWidth() + backgroundLimits[currentPosition]))] = -1000;
                }
            }
        });
    }

    public void setVariationCoefficient(float variationCoefficient) {
        this.variationCoefficient = variationCoefficient;
    }

    public void setNumberOfForeseenPixel(float numberOfForeseenPixel) {
        this.numberOfForeseenPixel = numberOfForeseenPixel;
    }

    public void setComputeRigth(boolean computeRigth) {
        this.computeRigth = computeRigth;
    }

    public void setComputeLeft(boolean computeLeft) {
        this.computeLeft = computeLeft;
    }


    public float[] getPosObject() {
        return posObject;
    }

    public void setPosObject(float[] posObject) {
        this.posObject = posObject;
    }

    public float[] getPosType() {
        return posType;
    }

    public void setPosType(float[] posType) {
        this.posType = posType;
    }

    public void setUseMobileMean(boolean useMobileMean) {
        this.useMobileMean = useMobileMean;
    }

    public void setMobileMeanSize(int mobileMeanSize) {
        this.mobileMeanSize = mobileMeanSize;
    }
}
