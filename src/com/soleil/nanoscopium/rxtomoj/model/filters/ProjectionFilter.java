/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.filters;

import java.util.ResourceBundle;

/**
 * Created by antoine bergamaschi on 09/01/14.
 * Be aware that maximum and minimum are expressed like :
 * 0 1 2 3 4 5 4 3 2 1
 * with 0 the center of the screen and 5 the right and left border minimum can be 2 near 0 and maximum 2 at the extreme right
 */
public class ProjectionFilter {
    public enum WindowType{
        HAMMING("hamming_window"),GAUSSIAN("gaussian_window");

        private String resource = "";

        private WindowType(String resource){
            this.resource = resource;
        }

        public String getResource(){
             return resource;
        }

        public String getDescription(){
            return ResourceBundle.getBundle("RXTomoJ").getString(this.resource);
        }

        public static boolean contains(WindowType type){
            for ( WindowType windowType : WindowType.values() ){
                if ( windowType.equals(type) ){
                    return true;
                }
            }
            return false;
        }
    }

    public enum FilterType{
        RAMP("ramp_filter"), HILBERT("hilbert_filter");

        private String resource = "";

        private FilterType(String resource){
            this.resource = resource;
        }

        public String getResource(){
            return resource;
        }

        public String getDescription(){
            return ResourceBundle.getBundle("RXTomoJ").getString(this.resource);
        }

        public static boolean contains(FilterType type){
            for ( FilterType filterType : FilterType.values() ){
                if ( filterType.equals(type) ){
                    return true;
                }
            }
            return false;
        }
    }


    private boolean withWindow = false;


    private FilterType currentFilterType = FilterType.RAMP;
    private WindowType currentWindowType = WindowType.HAMMING;

    private float windowDimension  = 300.0f;

    //Coefficient of decrease in the window ( for Hamming window )
    private float windowCoef = 0.54f;


    public ProjectionFilter(){
        super();
    }

    public ProjectionFilter(FilterType filterType){
        this(filterType,WindowType.HAMMING,250.0f);
    }

    public ProjectionFilter(FilterType filterType, WindowType windowType, float windowDimension){
        this(filterType,windowType,windowDimension,0.54f);
    }

    public ProjectionFilter(FilterType filterType, WindowType windowType, float windowDimension, float windowCoef){
        this.setCurrentFilterType(filterType);
        this.setCurrentWindowType(windowType);
        this.windowDimension = windowDimension;
        this.windowCoef = windowCoef;
        this.withWindow = true;
    }

    public ProjectionFilter(ProjectionFilter projectionFilter){
        this();
        this.clone(projectionFilter);
    }


    private void clone(ProjectionFilter projectionFilter){
        this.setCurrentFilterType(projectionFilter.getCurrentFilterType());
        this.setCurrentWindowType(projectionFilter.getCurrentWindowType());
        this.setWindowDimension(projectionFilter.getWindowDimension());
        this.setWindowCoef(projectionFilter.getWindowCoef());
        this.setWithWindow(projectionFilter.isWithWindow());
    }
    /**
     * Compute the weigth for the specified position
     * This position should be in the range of the weighting function.
     * If not 0 is returned
     * @param position float, the position in the range of the weighting function
     * @return weight
     */
    public float getWeight(float position){
        float weight = 0.0f;

        if ( Math.abs(position) < windowDimension ){
            if ( this.withWindow ){
                weight = applyWindow(Math.abs(position)) * applyFilter(position,1);
            }else{
                weight = applyFilter(position,1);
            }
        }
        return weight;
    }


    /**
     * Compute the weigth for the specified position
     * This position should be in the range of the weighting function.
     * If not 0 is returned
     * @param position The position in the range of the weighting function
     * @param value The value to be modified by the filters functions
     * @return new_value
     */
    public float doFilter(float position, float value){
        float new_value = 0.0f;
//        if ( Math.abs(position) < windowDimension ){
            if ( this.withWindow ){
                new_value = applyWindow(Math.abs(position)) * applyFilter(position,value);
            }else{
                new_value = applyFilter(position,value);
            }
//        }
        return new_value;
    }

    /**
     * Compute the weigth for the specified position
     * This position should be in the range of the weighting function.
     * If not 0 is returned
     * @param position The position in the range of the weighting function
     * @param value The Array of value to filter where the first is real part and second imaginary
     */
    public void doFilter(float position, float[] value){
        float windowValue = 0.0f;
//        if ( Math.abs(position) < windowDimension ){
            if ( this.withWindow ){
                windowValue = applyWindow(position);
                applyFilter(position,value);

                value[0] *= windowValue;
                value[1] *= windowValue;

            }else{
                applyFilter(position,value);
            }
//        }
    }

    /**
     * Compute the weight for the specified position
     * This position should be in the range of the weighting function.
     * If not 0 is returned
     * @param x int
     * @param y int
     * @param value The Array of value to filter where the first is real part and second imaginary
     */
    public void doFilter(int x, int y, float[] value){
        float position = 0;

        if (Math.signum(x * y) != 0) {
            position = (float) Math.sqrt(y * y + x * x) * Math.signum(x * y);
        } else if (x != 0) {
            position = (float) Math.sqrt(y * y + x * x) * Math.signum(x);
        } else if (y != 0) {
            position = (float) Math.sqrt(y * y + x * x) * Math.signum(y);
        } else {
            position = 0;
        }

        doFilter(position,value);
    }

    /**
     * Compute the specified window filters function on this position
     * @param position The position in the range of the weighting function
     * @return weight
     */
    private float applyWindow(float position){
        float weight = 0.0f;
        switch (this.currentWindowType){
            case HAMMING:
                position = Math.abs(position);
                //Hamming window
                weight = (float) (this.windowCoef + (this.windowCoef-1)*Math.cos((2*Math.PI*position)/this.windowDimension));
                if ( !Float.isFinite(weight)  ){
                    weight = 0;
                }
                break;
            case GAUSSIAN:
                if ( Math.abs(position) < windowDimension ){
                    weight = 1;
                }else{
                    weight = 0;
                }
                break;
            default:
                System.err.println("Unknown window type : " + this.currentWindowType);
                break;
        }
        return weight;
    }


    /**
     * Compute the specified window filters function on this position
     * @param position float The position in the range of the weighting function
     * @param type WindowType The window to apply
     * @return weight
     */
    private float applyWindow(float position, WindowType type){
        float weight = 0.0f;
        switch (type){
            case HAMMING:
                position = Math.abs(position);
                //Hamming window
                weight = (float) (this.windowCoef + (this.windowCoef-1)*Math.cos((2*Math.PI*position)/this.windowDimension));
                break;
            case GAUSSIAN:
                if ( Math.abs(position) < windowDimension ){
                    weight = 1;
                }else{
                    weight = 0;
                }
                break;
            default:
                System.err.println("Unknown window type : " + this.currentWindowType);
                break;
        }
        return weight;
    }

    /**
     * Compute the specified filter function output on this position
     * @param position The position in the range of the weighting function
     * @param value The value to be modified by the filters functions
     * @return ponderated_value
     */
    private float applyFilter(float position, float value){
        float ponderated_value = 0.0f;
        switch (this.currentFilterType){
            //Classical ramp filter
            case RAMP:
                ponderated_value = value * Math.abs(position);
                break;
            //Classical Hilbert Filter
            case HILBERT:
                //Warning Hilbert is a Imaginary filter
                ponderated_value = (float) (-(1.0d/2.0d*Math.PI) * Math.signum(position) * value);
                break;
            default:
                System.err.println("Unknown filter type : " + this.currentWindowType);
                break;
        }
        return ponderated_value;
    }

    /**
     * Compute the specified filter function output on this position for real and imaginary parts calls applyFilter
     * @param position The position in the range of the weighting function
     * @param value The value to be modified by the filters functions where the first is real part and second imaginary
     */
    private void applyFilter(float position, float[] value){
        switch (this.currentFilterType){
            //Classical ramp filter
            case RAMP:
                value[0] = applyFilter(position,value[0]);
                value[1] = applyFilter(position,value[1]);
                break;
            //Classical Hilbert Filter
            case HILBERT:
                //Warning Hilbert is a Imaginary filter
//                value[0] = applyFilter(position,value[0]);
//                value[1] = applyFilter(position,value[1]);
                float tmp_val1 = applyFilter(position,value[0]);
                float tmp_val2 = applyFilter(position,value[1]);
                value[0] = -tmp_val2;
                value[1] = tmp_val1;
                break;
            default:
                System.err.println("Unknown filter type : " + this.currentWindowType);
                break;
        }
    }

    /**
     * Compute the specified filter function output on this position for real and imaginary parts calls applyFilter
     * @param x int
     * @param y int
     * @param position float
     * @param value The value to be modified by the filters functions where the first is real part and second imaginary
     */
    private void applyFilter(int x, int y, float position, float[] value){
        switch (this.currentFilterType){
            //Classical ramp filter
            case RAMP:
                applyFilter(position,value);
                break;
            //Classical Hilbert Filter
            case HILBERT:
                //Warning Hilbert is a Imaginary filter
                applyFilter(position,value);
                break;
            default:
                System.err.println("Unknown filter type : " + this.currentWindowType);
                break;
        }
    }

    /**
     * Compute Image statistics on image and modify the image array to reduce background
     * @param image float[], array of float representing the image to modify
     * @param k float, the coefficient multipliying sigma for divergence
     */
    public void backgroundFilter(float[] image, float k){
        double mean = 0,std=0,min;
        int i;

        //Compute basic statistique on image
        for ( i = 0 ; i < image.length ; i++ ){
            mean += image[i];
        }
        mean /= image.length;

        //Standard Deviation
        for ( i = 0 ; i < image.length ; i++ ){
            std += Math.pow(mean - image[i], 2);
        }

        std /= image.length;

        //min = mean - k*std because in transmission scanning noise is obligatory less than signal
        min =  mean - k*std;

        //Background substraction
        for ( i = 0 ; i < image.length ; i++ ){
            if ( image[i] < min ){
                //Set the Background values to 0
                image[i] = 0;
            }
        }
        System.out.println("mean :"+mean+" std :"+std+" min : "+min);
    }



    public boolean isWithWindow() {
        return withWindow;
    }

    public void setWithWindow(boolean withWindow) {
        this.withWindow = withWindow;
    }

    public FilterType getCurrentFilterType() {
        return currentFilterType;
    }

    public void setCurrentFilterType(FilterType currentFilterType) {
        if ( FilterType.contains(currentFilterType)){
            this.currentFilterType = currentFilterType;
        }else{
            System.err.println("Unknown filters type : " + currentFilterType);
        }
    }

    public WindowType getCurrentWindowType() {
        return currentWindowType;
    }

    public void setCurrentWindowType(WindowType currentWindowType) {
        if ( WindowType.contains(currentWindowType) ){
            this.currentWindowType = currentWindowType;
        }else{
            System.err.println("Unknown window type : " + currentWindowType);
        }
    }

    public float getWindowDimension() {
        return windowDimension;
    }

    public void setWindowDimension(float windowDimension) {
        this.windowDimension = windowDimension;
    }

    public float getWindowCoef() {
        return windowCoef;
    }

    public void setWindowCoef(float windowCoef) {
        this.windowCoef = windowCoef;
    }

    @Override
    public String toString() {
        String toString = "Projection Filter parameters :: ";
        toString += "\n\tFilter type :: "+currentFilterType.toString();
        toString += "\n\tUse windowing :: "+withWindow;
        if ( withWindow ){
            toString += "\n\t\tWindow type :: "+currentWindowType.toString();
            toString += "\n\t\tdimension :: "+windowDimension;
            toString += "\n\t\tcoefficient :: "+windowCoef;
        }
        return toString;
    }
}
