/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model;

import java.util.ArrayList;

/**
 * History and parameters regristrement class.
 * Created by bergamaschi on 03/02/14.
 */
public class AnalyseFactory {

    @Deprecated
    public final static String[] MOTORS={"motor X","motor Y","motor Z","Motor rotation"};
    @Deprecated
    public final static int ABSORPTION = 0,DARKFIELD = 1, PHASECONTRASTE = 2,FLUORESCENCE = 3;

    private ArrayList<CustomAnalyse> analyses = new ArrayList<>();


    public AnalyseFactory(){}

    /**
     * Create a new Instance of Analyse
     * @return Analyse
     */
    public CustomAnalyse createNewAnalyses(){
        CustomAnalyse analyse;

        //Save the old analysis
        SessionObject.save();

        if ( analyses.size() > 0 ){
            analyse = new CustomAnalyse(analyses.get(analyses.size()-1));
        }
        else{
            analyse = new CustomAnalyse();
        }

        analyses.add(analyse);

        return analyse;
    }

    /**
     * Get the Current Analyse
     * @return Analyse
     */
    public CustomAnalyse getCurrent(){
        if ( analyses.size() == 0 ){
            return createNewAnalyses();
        }
        return analyses.get(analyses.size()-1);
    }


    @Deprecated
    public void updateSAI(String saiName){
        if ( this.getCurrent().getSAInames().contains(saiName) ){
            this.getCurrent().getSAInames().remove(saiName);
        }else{
            //TODO warning Here only One SAI can be Set once
            System.err.println("warning Here only One SAI can be Set once");
            if ( this.getCurrent().getSAInames().size() > 0){
                this.getCurrent().setSAInames(new ArrayList<String>());
            }
            this.getCurrent().getSAInames().add(saiName);
        }
    }

//    public void updateSPC(String spcName){
//        if ( this.getCurrent().getSAInames().contains(spcName) ){
//            this.getCurrent().getSAInames().remove(spcName);
//        }else{
//            this.getCurrent().getSAInames().add(spcName);
//        }
//    }

    public ArrayList<CustomAnalyse> getAnalyses() {
        return analyses;
    }

    public void setAnalyses(ArrayList<CustomAnalyse> analyses) {
        this.analyses = analyses;
    }

}
