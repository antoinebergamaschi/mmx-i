/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.computationFunction;


import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStack;
import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStackFactory;
import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.model.CustomAnalyse;
import com.soleil.nanoscopium.rxtomoj.model.SessionObject;
import com.soleil.nanoscopium.rxtomoj.model.data.ReductionInfo;
import com.soleil.nanoscopium.rxtomoj.model.filters.HotSpot_Detection;
import com.soleil.nanoscopium.rxtomoj.model.reconstructionFunction.BasicReconstructionFunction;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.MotorType;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.PixelType;
import com.soleil.nanoscopium.rxtomoj.model.utils.RXTomoJThreadFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.IntStream;


/**
 * Created with IntelliJ IDEA.
 * This class implements Basic computation fonction used in the child class
 *
 * @author : antoine bergamaschi
 *         Date: 24/11/13
 *         Time: 18:48
 *
 * Implementing a new Function by ovewritting every function of BasicComputationFunction and by redifinning if necessary initialize / flush / finalization functions
 */
public abstract class BasicComputationFunction extends BasicReadFunction {

    protected float[][] array;
    protected Hdf5VirtualStack hdf5VirtualStackSAI;

    protected StackType typeOfStack = StackType.ABSORPTION_REF;

    protected float[] saiData;
    protected float[] motorX;
    protected float[] motorY;
    protected float[] rotation;

    protected CustomAnalyse customAnalyse;
    protected SessionObject sessionObject;

    /**
     * The Array containing the Spot Mask
     */
    protected int[] spotMap;

    /**
     * The Roi inserted in the roiSpot, where the computation should be performed
     */
    protected int[][] computationRoi;

    /**
     * Pixel to be selected on the spot Mask
     * This Array Should Always be filled before computation
     */
    protected ArrayList<PixelType> pixelID = new ArrayList<>();

    protected boolean isAverage = false;

    protected float spotCenterX;
    protected float spotCenterY;


    /**
     * Get the current Stack created or contained by a BasicComputationFunction Object
     *
     * @return RXImage
     */
    public RXImage getStack() {
        return this.stack;
    }


    /**
     * Retrieve the SAI values. If this values are set as average, just get the arrays from the hdf5
     * else if the SAI data is a spectrum like values, get the Mean value.
     * @param position The position where to retrieve the first SAIA value
     * @param width The number of SAIA values to retrieve
     * @return float[]
     */
    public float[] getSAIValues(int position, int width){
        float[] result;
        int x,z,currentPosition;
        if (hdf5VirtualStackSAI != null) {
            result = new float[width];
            if (isAverage) {
                hdf5VirtualStackSAI.getPixels(position, saiData);

                System.arraycopy(saiData,0,result,0,result.length);

            } else {

                for (z = position; z < position+width; z++) {
                    currentPosition = z-position;
                    hdf5VirtualStackSAI.getPixels(z, saiData);

                    for (x = 0; x < saiData.length; x++) {
                        result[currentPosition] += saiData[x];
                    }

                    result[currentPosition] /= (float) saiData.length;
                }
            }


            //Divide the SAIA value by the coef linked to white absorbtion
//            IntStream.range(0,result.length).parallel().forEach(k->result[k] /= computeSpot.getCoefSTI());
        }else{
            result = new float[width];
            Arrays.fill(result,1.0f);
        }

        return result;
    }



    /**
     * Flush the stored array in the finalStack which will be stored in the current Stack
     */
    @Override
    public void flush(){
        float[] finalStack = new float[array.length*array[0].length];

        for ( int i = 0 ; i < array.length;i++){
            System.arraycopy(array[i], 0, finalStack, i * array[0].length, array[i].length);
        }

        this.stack.setData(finalStack);

        array = null;
    }

    @Override
    protected void computeData(final float[] data, int width, int height, int size, int position, int slice) {

        //Remove unes
        for ( int z = 0; z < size ; z++ ){
            for ( int y = 0 ; y < height ; y++){
                for ( int x = 0 ; x < width ; x++ ){
                    if ( x >= computationRoi[0][0] && x < computationRoi[0][1] &&
                            y >= computationRoi[1][0] && y < computationRoi[1][1]){
                        if ( !filterPixelType(x+y*width) ){
                            data[x+y*width+z*width*height] = 0;
                        }
                    }
                }
            }
        }

        //Start the Thread
        RXTomoJThreadFactory.createThreadBasicComputation(this, data, width, height, size, position, slice);
    }

    @Override
    protected void computeSpectrumData(float[] data, int width, int size, int position, int slice) {
        //Start the Thread
        RXTomoJThreadFactory.createThreadBasicComputation(this, data, width, 1, size, position, slice);
    }

    /**
     * Warning this function must not be overwritten
     * Function firing the computation
     */
    public void _compute(final float[] data, int width, int height, int size, int position, int slice){
        _beforeComputation();
        if (!isProcessClosed()){
            compute(data, width, height, size, position, slice);
        }
        _afterComputation();
    }

    /**
     * Function called before computation is fired
     */
    public void _beforeComputation(){

    }

    /**
     * Function called after computation has been performed
     */
    public void _afterComputation(){
        incrementProgress();
        fireProgressFunction();
    }

    /**
     * Abstract function fired by the BasicComputation Function after retrieving the data
     * @param data The data to process
     * @param width The width of the data to process array
     * @param height The Height of the data to process array
     * @param size The Size of the data to process array
     * @param position The current position in the final scan map
     * @param slice The position in the Z order
     */
    public abstract void compute(final float[] data, int width, int height, int size, int position, int slice);

    /**
     * used for initiate the PixelIDs
     * @return The PixelType filtered before computation is processed
     */
    protected abstract ArrayList<PixelType> getPixelType();

    protected abstract int[][] getComputationRoi();

    /**
     * This function must be called at the end of the computation process ( when the stack is already created ).
     * It can be then use to post process the stack.
     */
    @Override
    protected void finalization(){
        this.stack = RXTomoJ.getInstance().getModelController().getComputationController().regrid(this.stack,this.typeOfStack);
        if ( hdf5VirtualStackSAI != null ){
            Hdf5VirtualStackFactory.clear(hdf5VirtualStackSAI.getFileName(), hdf5VirtualStackSAI.getDatasetName());
        }
    }

    @Override
    public void initialize(){
        sessionObject = SessionObject.getCurrentSessionObject();
        customAnalyse = sessionObject.getAnalyseFactory().getCurrent();
        //TODO initialize Data depending on Session and no more store this
    }

    @Override
    public void initialize(int dims_length, int stack_width, int stack_height, int stack_size, boolean initAll){
        pixelID.addAll(getPixelType());
        computationRoi = getComputationRoi();

        if ( computationRoi != null ) {

            //Enlarge the roiSpot if necessary
            if (roiSpot[0][0] > computationRoi[0][0]) {
                roiSpot[0][0] = computationRoi[0][0];
            }
            if (roiSpot[0][1] < computationRoi[0][1]) {
                roiSpot[0][1] = computationRoi[0][1];
            }
            if (roiSpot[1][0] > computationRoi[1][0]) {
                roiSpot[1][0] = computationRoi[1][0];
            }
            if (roiSpot[1][1] < computationRoi[1][1]) {
                roiSpot[1][1] = computationRoi[1][1];
            }
        }else{
            computationRoi = new int[2][2];
            computationRoi[0][1] = roiSpot[0][1];
            computationRoi[0][0] = roiSpot[0][0];
            computationRoi[1][1] = roiSpot[1][1];
            computationRoi[1][0] = roiSpot[1][0];
        }

        //Transform the computationROI to have relative position from the roiSpot
        computationRoi[0][1] = roiSpot[0][1]-computationRoi[0][0];
        computationRoi[0][0] -= roiSpot[0][0];
        computationRoi[1][1] = roiSpot[1][1]-computationRoi[1][0];
        computationRoi[1][0] -= roiSpot[1][0];

        //Initialize the SpotMap
        initializeSpotMap();

//        if ( initAll ) {
            //Initialize the SAI vitualStack
//            initSAI(dims_length);

//        }

        this.stack = new RXImage(stack_width, stack_height, stack_size);
        this.array = new float[stack_size][stack_height*stack_width];
    }

    /**
     * Initialize the Spot map use as a mask to retrieve the pixel on which computation has to be done in each modalities
     */
    public void initializeSpotMap(){
        RXImage imageStack = RXTomoJ.getInstance().getModelController().getDataController().getComputedStack(StackType.SPOT_MAP);
        //TODO warning this will bug if the spot has the same size but is not on the same place
        if ( imageStack != null ){

            //Crop Data inside the whole 2D detector
            float[] pixels = imageStack.copyData();
            float[] cropped = ComputationUtils.cropInArray(pixels,roiSpot[0][0],roiSpot[1][0],0,imageStack.getWidth(),imageStack.getHeight(),
                    imageStack.getSize(),roiSpot[0][1]-roiSpot[0][0],roiSpot[1][1]-roiSpot[1][0],1);

            spotMap = new int[cropped.length];

            float[] center = ComputationUtils.computeGravityCenter(cropped,roiSpot[0][1]-roiSpot[0][0],roiSpot[1][1]-roiSpot[1][0]);

            spotCenterX = center[0];
            spotCenterY = center[1];

            IntStream.range(0,cropped.length).parallel().forEach(i->{
                //If the pixel ha a value identifiable in the PixelType arrayList, add it in the spotMap
                if ( pixelID.contains(PixelType.getPixelType((int) cropped[i])) ){
                    spotMap[i] = (int) cropped[i];
                }
            });
        }
        else{
            System.err.println("No Map Spot Found");
        }
    }

    //TODO CHANGE ORIENTATION ALSO ADD parameters to retrieve the selected SAIA
    public void initSAI(int imgDims){
        //Initialize the SAI vitualStack
        if (customAnalyse.getSAInames() != null && customAnalyse.getSAInames().size() > 0) {
            System.err.println("Warning default SAI is takken : 0 " + this.getClass().toString());
            //WARNING CHANGE IN ACQUISITION NOW SAI IS SET AS AVERAGED 1 value Per channel
            hdf5VirtualStackSAI = Hdf5VirtualStackFactory.createVirtualStack(sessionObject.getPathToHdf5(), customAnalyse.getSAInames().get(0), true);
//            hdf5VirtualStackSAI = new Hdf5VirtualStack(sessionObject.getPathToHdf5(), customAnalyse.getSAInames().get(0), true);
            //To be more accurate 3D image div should give 3D data sai if average or 4D data sai if not same for 2D
            if ( hdf5VirtualStackSAI.getType().getMatrixDimension() - imgDims == 0){
                isAverage = true;
                saiData = new float[hdf5VirtualStackSAI.getWidth()];
            }else {
                saiData = new float[hdf5VirtualStackSAI.getWidth() * hdf5VirtualStackSAI.getHeigth()];
            }
            //Set -1 to initialy get the 0 position when calling next
            hdf5VirtualStackSAI.setCurrentPosition(-1);
        }
    }

    /**
     * Init Motors Data
     * @param sessionObject The sessionObject to use as base
     * @param customAnalyse The CustomAnalyse from this sessionObject to use for analyse
     * @param stackType The current Stack Type to reconstructe
     * @return {PositionX, PositionY, PositionR} Motors position X and Y cropped and inserted in the good direction
     */
    public static RXImage[] initMotors(SessionObject sessionObject, CustomAnalyse customAnalyse, StackType stackType){
        RXImage[] motors = {new RXImage(),new RXImage(),new RXImage()};

        ReductionInfo reductionInfo = customAnalyse.getParameterStorage().getReductionInfo();
//        ComputeSpot computeSpot = customAnalyse.getParameterStorage().getComputeSpot().get(stackType);

        //Init Dims
        int[][] roi = reductionInfo.getReductionROI();
        int w = (roi[0][1] - roi[0][0]);
        int h = (roi[1][1] - roi[1][0]);
        int s = (roi[2][1] - roi[2][0]);

        motors[0].setDims(new int[]{w,h,s});
        motors[1].setDims(new int[]{w,h,s});
        motors[2].setDims(new int[]{w,h,s});

        //Fill with 0
        motors[0].setData(new float[motors[0].getGlobalSize()]);
        motors[1].setData(new float[motors[0].getGlobalSize()]);
//        test[2].setData(new float[test[0].getGlobalSize()]);
//        test[1].setDims(new int[]{w,h,s});

        for (MotorType motorType : customAnalyse.getMotorPath().keySet()){
            switch (motorType){
                case MOTOR_X :
                    //Shift with motor X
                    if ( customAnalyse.getMotorPath().get(MotorType.MOTOR_X).length() > 0 ){

                        Hdf5VirtualStack hdf5VirtualStack = Hdf5VirtualStackFactory.createVirtualStack(sessionObject.getPathToHdf5(),customAnalyse.getMotorPath().get(MotorType.MOTOR_X),false);
                        float[] tx = hdf5VirtualStack.getData();

                        int wh = h * w;

                        float[] motorX = new float[wh*(roi[2][1] - roi[2][0])];

                        //Crop tx to fit in the roiReduction
                        int yprime = 0;
                        for ( int z = roi[2][0] ; z < roi[2][1] ; z++ ){
                            for ( int y = roi[1][0] ; y < roi[1][1] ; y++ ){
                                System.arraycopy(tx,roi[0][0] + y*hdf5VirtualStack.getWidth() + z*hdf5VirtualStack.getWidth()*hdf5VirtualStack.getHeigth() ,motorX,yprime*w,w);
                                yprime++;
                            }
                        }

//                        test[0] = new RXImage(w,h,s);
                        motors[0].setData(motorX);

                        //Convert orientation
                        motors[0] = RXTomoJ.getInstance().getModelController().getComputationController().setStackOrientation(motors[0], customAnalyse.getDimension(), ComputationUtils.OrientationType.XYZ);

                        Hdf5VirtualStackFactory.clear(sessionObject.getPathToHdf5(),customAnalyse.getMotorPath().get(MotorType.MOTOR_X));

                        tx = null;
                    }
                    break;
                case MOTOR_Y :
                    //Warning for the Y motor we need to orientate the array in the right direction
                    if ( customAnalyse.getMotorPath().get(MotorType.MOTOR_Y).length() > 0 ){

                        Hdf5VirtualStack hdf5VirtualStack = Hdf5VirtualStackFactory.createVirtualStack(sessionObject.getPathToHdf5(),customAnalyse.getMotorPath().get(MotorType.MOTOR_Y),false);
                        float[] tx = hdf5VirtualStack.getData();

                        int wh = h * w;
                        float[] motorY = new float[wh*(roi[2][1] - roi[2][0])];

                        //Crop tx to fit in the roiReduction
                        int yprime = 0;
                        for ( int z = roi[2][0] ; z < roi[2][1] ; z++ ){
                            for ( int y = roi[1][0] ; y < roi[1][1] ; y++ ){
                                System.arraycopy(tx,roi[0][0] + y*hdf5VirtualStack.getWidth() + z*hdf5VirtualStack.getWidth()*hdf5VirtualStack.getHeigth() ,motorY,yprime*w,w);
                                yprime++;
                            }
                        }

//                        test[1] = new RXImage(w,h,s);
                        motors[1].setData(motorY);

                        //Convert orientation
                        motors[1] = RXTomoJ.getInstance().getModelController().getComputationController().setStackOrientation(motors[1], customAnalyse.getDimension(),ComputationUtils.OrientationType.XYZ);

                        Hdf5VirtualStackFactory.clear(sessionObject.getPathToHdf5(),customAnalyse.getMotorPath().get(MotorType.MOTOR_Y));

                        tx = null;
                    }
                    break;
                case MOTOR_R :
                    //Warning for the Y motor we need to orientate the array in the right direction
                    if ( customAnalyse.getMotorPath().get(MotorType.MOTOR_R).length() > 0 ){

                        Hdf5VirtualStack hdf5VirtualStack = Hdf5VirtualStackFactory.createVirtualStack(sessionObject.getPathToHdf5(),customAnalyse.getMotorPath().get(MotorType.MOTOR_R),false);
                        float[] tx = hdf5VirtualStack.getData();

                        int wh = h * w;
                        float[] motorY = new float[wh*(roi[2][1] - roi[2][0])];

                        //Test the Dimension of the R motors (sometimes only 1D Data is used for 2D projection)
                        if ( tx.length != motorY.length ){
                            System.err.println("Error :: Data length and expected length are not equals :: "+BasicComputationFunction.class.toString()+" l.436");
                            break;
                        }

                        //Crop tx to fit in the roiReduction
                        int yprime = 0;
                        for ( int z = roi[2][0] ; z < roi[2][1] ; z++ ){
                            for ( int y = roi[1][0] ; y < roi[1][1] ; y++ ){
                                System.arraycopy(tx,roi[0][0] + y*hdf5VirtualStack.getWidth() + z*hdf5VirtualStack.getWidth()*hdf5VirtualStack.getHeigth() ,motorY,yprime*w,w);
                                yprime++;
                            }
                        }

//                        test[1] = new RXImage(w,h,s);
                        motors[2].setData(motorY);

                        //Convert orientation
                        motors[2] = RXTomoJ.getInstance().getModelController().getComputationController().setStackOrientation(motors[2], customAnalyse.getDimension(),ComputationUtils.OrientationType.XYZ);

                        Hdf5VirtualStackFactory.clear(sessionObject.getPathToHdf5(),customAnalyse.getMotorPath().get(MotorType.MOTOR_R));

                        tx = null;
                    }
                    break;
                default:
                    System.err.println("Error Unknown motors :: " + BasicReconstructionFunction.class.toString());
                    break;
            }
        }

        return motors;
    }


    /**
     * Initialize the Motors used in the Finalize function for regridding. Set also the correct orientation.
     * @param sessionObject
     * @param stackType
     * @return
     */
    public static RXImage[] initMotors(SessionObject sessionObject, StackType stackType){
        CustomAnalyse customAnalyse =  sessionObject.getAnalyseFactory().getCurrent();
        return initMotors(sessionObject,customAnalyse,stackType);
    }

    public static void hotSpotFilter(final float[] data,final int[] spotMap, int width, int height, int size,
                                     int maxLoop, int spreadCoef,int vois,boolean keepZero,
                                     final ArrayList<PixelType> pixelTypes){

        HotSpot_Detection.run3D(data, spotMap, width, height, size ,
                maxLoop,spreadCoef, vois, keepZero, pixelTypes);
    }

    protected boolean testPixelType(int x, int y){
        return pixelID.contains(PixelType.getPixelType(spotMap[computationRoi[0][0] + x + (computationRoi[1][0] + y) * (roiSpot[0][1] - roiSpot[0][0])]));
    }

    private boolean filterPixelType(int x, int y){
        return pixelID.contains(PixelType.getPixelType(spotMap[ x +  y*(roiSpot[0][1] - roiSpot[0][0])]));
    }

    private boolean filterPixelType(int pos){
        return pixelID.contains(PixelType.getPixelType(spotMap[pos]));
    }

}


