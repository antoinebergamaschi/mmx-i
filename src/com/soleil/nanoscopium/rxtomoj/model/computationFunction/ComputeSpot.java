/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.computationFunction;


import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.model.data.ReductionInfo;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.PixelType;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;
import com.soleil.nanoscopium.rxtomoj.model.utils.RXTomoJThreadFactory;

import java.util.ArrayList;
import java.util.stream.IntStream;

/**
 * Created by antoine bergamaschi on 16/01/14.
 */
public class ComputeSpot extends BasicComputationFunction {
    //Default Parameter of the different Algo
    private float minimalPixelValue = 1.0f;
    private int minimalPixelNumber = 3;
//    private boolean eliminatedPixelToZero = true;
    final private int[][] computedRoiSpot = {{Integer.MAX_VALUE,Integer.MIN_VALUE},
        {Integer.MAX_VALUE,Integer.MIN_VALUE}};

    private int[] map;
    private float[] mapSum;
    final private int[][] roiNormalization = new int[2][2];
    final private int[][] roiC = new int[2][2];

    private boolean is2Ddetector = true;

    /**
     * Maximum dimension of the SpotMap
     * Only 2D detector
     */
    final private int[] maxDimension = new int[2];

    /**
     * percent of minimum presence of a pixel to be taken has account for the spot
     */
    private float spotFilter = 0.9f;

    /**
     * Minimum pixel value to be taken in the computation of the spot
     */
    private float spotMinimumValue = 1.0f;


    /**
     * Do not use this constructor
     */
    public ComputeSpot() {
        super();
        this.typeOfStack = StackType.ABSORPTION;
    }

    @Override
    protected ArrayList<PixelType> getPixelType() {
        ArrayList<PixelType> p = new ArrayList<>();
        p.add(PixelType.OTHER);
        return p;
    }

    @Override
    protected int[][] getComputationRoi() {
        //No default range
        return null;
    }

    /**
     * Basic Function, get the  roiSpot and return the asked image.
     * Set the Spot outside of the Spot to zero
     * @param data    float[], the raw data as returned from hdf5 file
     * @param width   int, the width of the raw spot image (retrieved in hdf5 metadata)
     * @param height  int, the height of the raw spot image (retrieved in hdf5 metadata)
     * @param size    int, the size of the raw spot image which is equal to a dimension (size or height of the desired image )(retrieved in hdf5 metadata)
     * @param roiSpot int[][], the resulting roiSpot
     */
    private void findSpotROI(float[] data, int width, int height, int size, int[][] roiSpot) {
        //Search for the Spot Roy ( Critical step )
        int[][] currentRoiSpot = this.findSpotROI(data, 0, width, 0, height, 0, size);

        synchronized (roiSpot) {
//            if (currentRoiSpot[0][0] < roiSpot[0][0]) {
//                roiSpot[0][0] = currentRoiSpot[0][0];
//            }
//            if (currentRoiSpot[0][1] > roiSpot[0][1]) {
//                roiSpot[0][1] = currentRoiSpot[0][1];
//            }
//            if (currentRoiSpot[1][0] < roiSpot[1][0]) {
//                roiSpot[1][0] = currentRoiSpot[1][0];
//            }
//            if (currentRoiSpot[1][1] > roiSpot[1][1]) {
//                roiSpot[1][1] = currentRoiSpot[1][1];
//            }

            //Compute 2 Roi and 1 normalization parameters
            //roiC is use to compute the "path" to find the computedRoi thus roiC sum each of his component,
            //roiC is latter normalize by roiNormalize. roiC/normlize should always be outside of the range of the computedRoi,
            //Only in the case of Strange ROI behavior this parameters his used
            if (currentRoiSpot[0][0] < roiSpot[0][0]) {
                roiNormalization[0][0]++;
                roiC[0][0] += currentRoiSpot[0][0];
                roiSpot[0][0] = currentRoiSpot[0][0];
            }
            if (currentRoiSpot[0][1] > roiSpot[0][1]) {
                roiNormalization[0][1]++;
                roiC[0][1] += currentRoiSpot[0][1];
                roiSpot[0][1] = currentRoiSpot[0][1];
            }
            if (currentRoiSpot[1][0] < roiSpot[1][0]) {
                roiNormalization[1][0]++;
                roiC[1][0] += currentRoiSpot[1][0];
                roiSpot[1][0] = currentRoiSpot[1][0];
            }
            if (currentRoiSpot[1][1] > roiSpot[1][1]) {
                roiNormalization[1][1]++;
                roiC[1][1] += currentRoiSpot[1][1];
                roiSpot[1][1] = currentRoiSpot[1][1];
            }
        }
    }



    /**
     * Basic Roi spot retrieve function.
     * Search for contiguous pixel ( more than the specified minimalPixelNumber )  with
     * a value superior to the minimalPixelValue.
     *
     * @param data the 1D data to browse
     * @param xMin start of the X ROI position in the data
     * @param xMax end of the X ROI position in the data
     * @param yMin start of the Y ROI position in the data
     * @param yMax end of the Y ROI position in the data
     * @param zMin start of the Z ROI position in the data
     * @param zMax end of the Z ROI position in the data
     * @return int[][] the roi defined for each x,y,z dimension.
     */
    private int[][] findSpotROI(float[] data, int xMin, int xMax, int yMin, int yMax, int zMin, int zMax) {
        int[][] positionRoiSpot = new int[3][2];

        //Initialize the roy position
        positionRoiSpot[0][0] = Integer.MAX_VALUE;
        positionRoiSpot[0][1] = Integer.MIN_VALUE;
        positionRoiSpot[1][0] = Integer.MAX_VALUE;
        positionRoiSpot[1][1] = Integer.MIN_VALUE;
        positionRoiSpot[2][0] = Integer.MAX_VALUE;
        positionRoiSpot[2][1] = Integer.MIN_VALUE;

        //Find the HotSpot
        for (int z = zMin; z < zMax; z++) {
            int x_pos_min = Integer.MAX_VALUE;
            int x_pos_max = Integer.MIN_VALUE;
            int y_pos_min = Integer.MAX_VALUE;
            int y_pos_max = Integer.MIN_VALUE;
            int z_pos_min = Integer.MAX_VALUE;
            int z_pos_max = Integer.MIN_VALUE;
            for (int y = yMin; y < yMax; y++) {
                int[] xposStart = {-1, -1, -1};
                for (int x = xMin; x < xMax; x++) {
                    //Find a non negative pixel
                    if (data[x + y * xMax + z * xMax * yMax] > this.minimalPixelValue) {
                        //Initialize the start position
                        if (xposStart[0] == -1) {
                            xposStart[0] = x;
                            xposStart[1] = y;
                            xposStart[2] = z;
                        }
                    } else if (xposStart[0] > 0 && (x - xposStart[0]) > this.minimalPixelNumber) {
//                        if ( xposStart[0] > 0 && (xposStart[0] - x) > minimalPixelNumber ){
                        //If there is enough contiguous pixel
                        if (xposStart[0] < x_pos_min) {
                            x_pos_min = xposStart[0];
                        }
                        if (x > x_pos_max) {
                            x_pos_max = x;
                        }

                        if (xposStart[1] < y_pos_min) {
                            y_pos_min = xposStart[1];
                        }
                        if (y > y_pos_max) {
                            y_pos_max = y;
                        }
                        if (xposStart[2] < z_pos_min) {
                            z_pos_min = xposStart[2];
                        }
                        if (z > z_pos_max) {
                            z_pos_max = z;
                        }

                        //Reinitialize xposStart and End
                        xposStart[0] = -1;
                        xposStart[1] = -1;
                        xposStart[2] = -1;
                    }
                    //No contiguous pixel reset selection
                    else {
//                        if ( eliminatedPixelToZero ){
                        //set to 0 the no coutiguous pixels
                        if (xposStart[0] > 0) {
                            for (int k = xposStart[0]; k <= x; k++) {
                                int testY = 1;
                                int contiguous = 0;
                                while (testY <= this.minimalPixelNumber && y + testY < yMax) {
                                    if (data[x + (y + testY) * xMax + z * xMax * yMax] > this.minimalPixelValue) {
                                        contiguous++;
                                    } else {
                                        break;
                                    }
                                    testY++;
                                }
                                testY = 1;
                                while (testY <= this.minimalPixelNumber && y - testY >= yMin) {
                                    if (data[x + (y - testY) * xMax + z * xMax * yMax] > this.minimalPixelValue) {
                                        contiguous++;
                                    } else {
                                        break;
                                    }
                                    testY++;
                                }
                            }
                        }
                        //Reinitialize selection
                        xposStart[0] = -1;
                        xposStart[1] = -1;
                        xposStart[2] = -1;
                    }
                }
            }

            //Save the coordonate
            if (x_pos_min < positionRoiSpot[0][0]) {
                positionRoiSpot[0][0] = x_pos_min;
            }
            if (x_pos_max > positionRoiSpot[0][1]) {
                positionRoiSpot[0][1] = x_pos_max;
            }

            if (y_pos_min < positionRoiSpot[1][0]) {
                positionRoiSpot[1][0] = y_pos_min;
            }
            if (y_pos_max > positionRoiSpot[1][1]) {
                positionRoiSpot[1][1] = y_pos_max;
            }
            if (z_pos_min < positionRoiSpot[2][0]) {
                positionRoiSpot[2][0] = z_pos_min;
            }
            if (z_pos_max > positionRoiSpot[2][1]) {
                positionRoiSpot[2][1] = z_pos_max;
            }
        }

        return positionRoiSpot;
    }

    /**
     * Basic search for the hotSpot.
     * Search for contiguous pixel ( more than the specified minimalPixelNumber )  with
     * a value superior to the minimalPixelValue.
     *
     * @param data the 1D data to browse
     * @param xMin start of the X ROI position in the data
     * @param xMax end of the X ROI position in the data
     * @param yMin start of the Y ROI position in the data
     * @param yMax end of the Y ROI position in the data
     * @param zMin start of the Z ROI position in the data
     * @param zMax end of the Z ROI position in the data
     * @return int[][] the roi defined for each x,y,z dimension.
     */
    @Deprecated
    private int[][] searchSpotCenter(float[] data, int xMin, int xMax, int yMin, int yMax, int zMin, int zMax) {
        int[][] positionRoiSpot = new int[3][2];

        //Initialize the roy position
        positionRoiSpot[0][0] = Integer.MAX_VALUE;
        positionRoiSpot[0][1] = Integer.MIN_VALUE;
        positionRoiSpot[1][0] = Integer.MAX_VALUE;
        positionRoiSpot[1][1] = Integer.MIN_VALUE;
        positionRoiSpot[2][0] = Integer.MAX_VALUE;
        positionRoiSpot[2][1] = Integer.MIN_VALUE;

        //Find the HotSpot
        for (int z = zMin; z < zMax; z++) {
            int x_pos_min = Integer.MAX_VALUE;
            int x_pos_max = Integer.MIN_VALUE;
            int y_pos_min = Integer.MAX_VALUE;
            int y_pos_max = Integer.MIN_VALUE;
            int z_pos_min = Integer.MAX_VALUE;
            int z_pos_max = Integer.MIN_VALUE;
            for (int y = yMin; y < yMax; y++) {
                int[] xposStart = {-1, -1, -1};
                for (int x = xMin; x < xMax; x++) {
                    //Find a non negative pixel
                    if (data[x + y * xMax + z * xMax * yMax] > this.minimalPixelValue) {
                        //Initialize the start position
                        if (xposStart[0] == -1) {
                            xposStart[0] = x;
                            xposStart[1] = y;
                            xposStart[2] = z;
                        }
                    } else if (xposStart[0] > 0 && (x - xposStart[0]) > this.minimalPixelNumber) {
//                        if ( xposStart[0] > 0 && (xposStart[0] - x) > minimalPixelNumber ){
                        //If there is enough contiguous pixel
                        if (xposStart[0] < x_pos_min) {
                            x_pos_min = xposStart[0];
                        }
                        if (x > x_pos_max) {
                            x_pos_max = x;
                        }

                        if (xposStart[1] < y_pos_min) {
                            y_pos_min = xposStart[1];
                        }
                        if (y > y_pos_max) {
                            y_pos_max = y;
                        }
                        if (xposStart[2] < z_pos_min) {
                            z_pos_min = xposStart[2];
                        }
                        if (z > z_pos_max) {
                            z_pos_max = z;
                        }

                        //Reinitialize xposStart and End
                        xposStart[0] = -1;
                        xposStart[1] = -1;
                        xposStart[2] = -1;
                    }
                    //No contiguous pixel reset selection
                    else {
//                        if ( eliminatedPixelToZero ){
                        //set to 0 the no coutiguous pixels
                        if (xposStart[0] > 0) {
                            for (int k = xposStart[0]; k <= x; k++) {
                                int testY = 1;
                                int contiguous = 0;
                                while (testY <= this.minimalPixelNumber && y + testY < yMax) {
                                    if (data[x + (y + testY) * xMax + z * xMax * yMax] > this.minimalPixelValue) {
                                        contiguous++;
                                    } else {
                                        break;
                                    }
                                    testY++;
                                }
                                testY = 1;
                                while (testY <= this.minimalPixelNumber && y - testY >= yMin) {
                                    if (data[x + (y - testY) * xMax + z * xMax * yMax] > this.minimalPixelValue) {
                                        contiguous++;
                                    } else {
                                        break;
                                    }
                                    testY++;
                                }
                                if (contiguous < this.minimalPixelNumber) {
                                    data[k + y * xMax + z * xMax * yMax] = 0;
                                }
                            }
                        } else {
                            data[x + y * xMax + z * xMax * yMax] = 0;
                        }
//                        }
                        //Reinitialize selection
                        xposStart[0] = -1;
                        xposStart[1] = -1;
                        xposStart[2] = -1;
                    }
                }
            }

            //Save the coordonate
            if (x_pos_min < positionRoiSpot[0][0]) {
                positionRoiSpot[0][0] = x_pos_min;
            }
            if (x_pos_max > positionRoiSpot[0][1]) {
                positionRoiSpot[0][1] = x_pos_max;
            }

            if (y_pos_min < positionRoiSpot[1][0]) {
                positionRoiSpot[1][0] = y_pos_min;
            }
            if (y_pos_max > positionRoiSpot[1][1]) {
                positionRoiSpot[1][1] = y_pos_max;
            }
            if (z_pos_min < positionRoiSpot[2][0]) {
                positionRoiSpot[2][0] = z_pos_min;
            }
            if (z_pos_max > positionRoiSpot[2][1]) {
                positionRoiSpot[2][1] = z_pos_max;
            }
        }

        return positionRoiSpot;
    }



    /**
     * Create a Map containing information about the each pixel.
     * Information may be of there kind. either PIXEL_OTHER, PIXEL_SPOT, PIXEL_HOT or PIXEL_SPOT_AND_HOT
     * @param count The count of presence of each pixel
     * @param roiSpot The Spot ROI information
     * @return The constructed MAP
     */
    private float[] createPixelMap(final float[] count, int[][] roiSpot){
        //Warning here this.roiSpot is the roi Selection in the image,
        //In case of computeSpot this roiSpot correspond to the whole image.

        float max = 0;
        float[] result = new float[map.length];

        boolean containsHotSpot = true;
        int tryAgain = 0;

        //Cannot go more than 3 Times
        while (containsHotSpot && tryAgain < 3) {
            containsHotSpot = false;
            tryAgain++;

            //Find the highest pixel value in the roiSpot
            for (int y = roiSpot[1][0]; y < roiSpot[1][1]; y++) {
                for (int x = roiSpot[0][0]; x < roiSpot[0][1]; x++) {
                    if (count[x + y * this.roiSpot[0][1]] > max) {
                        max = count[x + y * this.roiSpot[0][1]];
                    }
                }
            }

            //The minimum count value for a pixel to be marked has being in the spot
            final float minDev = max * spotFilter;

            //For each pixel, check the surrounding and assign the value of it
            IntStream.range(0, count.length).parallel().forEach(i -> {
                int numbOfPixel = 0;
                //If the pixel is less represented than a pixel from the spot
                if (count[i] < minDev) {
                    result[i] = PixelType.OTHER.value();
                } else {
                    //Test surrending Pixel If there is no other pixel this pixel is a HotSpot
                    if (i + 1 < count.length && count[i + 1] >= minDev) {
                        numbOfPixel++;
                    }
                    if (numbOfPixel == 0 && i - 1 >= 0 && count[i - 1] >= minDev) {
                        numbOfPixel++;
                    }
                    if (numbOfPixel == 0 && i + this.roiSpot[0][1] < map.length &&
                            count[i + this.roiSpot[0][1]] >= minDev) {
                        numbOfPixel++;
                    }
                    if (numbOfPixel == 0 && i - this.roiSpot[0][1] >= 0 &&
                            count[i - this.roiSpot[0][1]] >= minDev) {
                        numbOfPixel++;
                    }

                    //If this pixel as at least one neighbour
                    if (numbOfPixel > 0) {
                        result[i] = PixelType.SPOT.value();
                    } else {
                        //Else this is a HOT pixel
                        result[i] = PixelType.HOT.value();
                    }
                }
            });

            //Second loop to change pixel outside the ROI spot to HOT pixels
            for (int y = 0; y < this.roiSpot[1][1]; y++) {
                for (int x = 0; x < this.roiSpot[0][1]; x++) {
                    int i = x + y * this.roiSpot[0][1];
                    //If pixel inside the ROI
                    if (x >= roiSpot[0][0] && x < roiSpot[0][1] &&
                            y >= roiSpot[1][0] && y < roiSpot[1][1]) {
                        if (result[i] == PixelType.HOT.value()) {
                            result[i] = PixelType.SPOT_AND_HOT.value();
                            //Redo
                            containsHotSpot = true;
                        }
                    }
                    //Else for pixel outside of the current spot but having value sup to minDev
                    else {
                        if ( result[i] == PixelType.SPOT.value() || result[i] == PixelType.HOT.value() ) {
                            int numbOfPixel = 0;

//                        Check neighbour if near a SPOT pixel identified as spot else HOT
                            if (i + 1 < count.length && result[i + 1] == PixelType.SPOT.value()) {
                                numbOfPixel++;
                            }
                            if ( i - 1 >= 0 && result[i - 1] == PixelType.SPOT.value()) {
                                numbOfPixel++;
                            }
                            if ( i + this.roiSpot[0][1] < map.length &&
                                    result[i + this.roiSpot[0][1]] == PixelType.SPOT.value()) {
                                numbOfPixel++;
                            }
                            if ( i - this.roiSpot[0][1] >= 0 &&
                                    result[i - this.roiSpot[0][1]] == PixelType.SPOT.value()) {
                                numbOfPixel++;
                            }

                            if (numbOfPixel > 1) {
                                result[i] = PixelType.SPOT.value();
                            } else {
                                result[i] = PixelType.HOT.value();
                            }
                        }
                    }
                }
            }

//            if ( containsHotSpot ){
                //Tmp Roi
                int[][] tmpROI = new int[2][2];
                tmpROI[0][0] = Integer.MAX_VALUE;
                tmpROI[0][1] = Integer.MIN_VALUE;
                tmpROI[1][0] = Integer.MAX_VALUE;
                tmpROI[1][1] = Integer.MIN_VALUE;

                //Third Loop over the whole image to update the ROI
                //Update the ROI
                for (int y = this.roiSpot[1][0]; y < this.roiSpot[1][1]; y++) {
                    for (int x = this.roiSpot[0][0]; x < this.roiSpot[0][1]; x++) {
                        int i = x+y*this.roiSpot[0][1];
                        if (result[i] == PixelType.SPOT.value() ) {
                            if ( x < tmpROI[0][0] ){
                                tmpROI[0][0] = x;
                            }
                            if ( y < tmpROI[1][0] ){
                                tmpROI[1][0] = y;
                            }
                            if ( x > tmpROI[0][1]){
                                tmpROI[0][1] = x;
                            }
                            if ( y > tmpROI[1][1]){
                                tmpROI[1][1] = y;
                            }
                        }
                    }
                }

//                if (tmpROI[0][0] > roiSpot[0][0]) {
                    roiSpot[0][0] = tmpROI[0][0];
//                }
//                if (tmpROI[0][1] <= roiSpot[0][1]) {
                    roiSpot[0][1] = tmpROI[0][1]+1;
//                }
//                if (tmpROI[1][0] > roiSpot[1][0]) {
                    roiSpot[1][0] = tmpROI[1][0];
//                }
//                if (tmpROI[1][1] <= roiSpot[1][1]) {
                    roiSpot[1][1] = tmpROI[1][1]+1;
//                }
            }

        //Final check
        //Remove Other pixel (background) from spot.
        //Find the highest pixel value in the roiSpot
        for ( int k = 0; k < 3 ; k++ ) {
            for (int y = roiSpot[1][0]; y < roiSpot[1][1]; y++) {
                for (int x = roiSpot[0][0]; x < roiSpot[0][1]; x++) {
                    int i = x + y * this.roiSpot[0][1];
                    int numbOfPixel = 0;
                    //If the roi spot contains a Other values
                    if (result[i] == PixelType.OTHER.value()) {
                        //If this pixel is near 2 Spot or Hot/Spot pixel
                        //Test surrending Pixel If there is no other pixel this pixel is a HotSpot
                        if (i + 1 < count.length && (result[i + 1] == PixelType.SPOT.value() ||
                                result[i + 1] == PixelType.SPOT_AND_HOT.value())) {
                            numbOfPixel++;
                        }
                        if ( i - 1 >= 0 && (result[i - 1] == PixelType.SPOT.value() ||
                                result[i - 1] == PixelType.SPOT_AND_HOT.value())) {
                            numbOfPixel++;
                        }
                        if (i + this.roiSpot[0][1] < map.length &&
                                (result[i + this.roiSpot[0][1]] == PixelType.SPOT.value() ||
                                        result[i + this.roiSpot[0][1]] == PixelType.SPOT_AND_HOT.value())) {
                            numbOfPixel++;
                        }
                        if (i - this.roiSpot[0][1] >= 0 &&
                                (result[i - this.roiSpot[0][1]] == PixelType.SPOT.value() ||
                                        result[i - this.roiSpot[0][1]] == PixelType.SPOT_AND_HOT.value())) {
                            numbOfPixel++;
                        }

                        if (numbOfPixel > 2) {
                            result[i] = PixelType.SPOT_AND_HOT.value();
                        }
                    }
                }
            }
        }

        return result;
    }


    /**
     * Same as searchSpotCenter, but set to 0 the selected contiguous pixel.
     *
     * @param data the 1D data to browse
     * @param xMin start of the X ROI position in the data
     * @param xMax end of the X ROI position in the data
     * @param yMin start of the Y ROI position in the data
     * @param yMax end of the Y ROI position in the data
     * @param zMin start of the Z ROI position in the data
     * @param zMax end of the Z ROI position in the data
     * @return int[][] the roi defined for each x,y,z dimension.
     */
    @Deprecated
    private int[][] searchSpotCenterDarkField(float[] data, int xMin, int xMax, int yMin, int yMax, int zMin, int zMax) {
        int[][] positionRoiSpot = new int[3][2];

        //Initialize the roy position
        positionRoiSpot[0][0] = Integer.MAX_VALUE;
        positionRoiSpot[0][1] = Integer.MIN_VALUE;
        positionRoiSpot[1][0] = Integer.MAX_VALUE;
        positionRoiSpot[1][1] = Integer.MIN_VALUE;
        positionRoiSpot[2][0] = Integer.MAX_VALUE;
        positionRoiSpot[2][1] = Integer.MIN_VALUE;

        //Find the HotSpot
        for (int z = zMin; z < zMax; z++) {
            int x_pos_min = Integer.MAX_VALUE;
            int x_pos_max = Integer.MIN_VALUE;
            int y_pos_min = Integer.MAX_VALUE;
            int y_pos_max = Integer.MIN_VALUE;
            int z_pos_min = Integer.MAX_VALUE;
            int z_pos_max = Integer.MIN_VALUE;
            for (int y = yMin; y < yMax; y++) {
                int[] xposStart = {-1, -1, -1};
                for (int x = xMin; x < xMax; x++) {
                    //Find a non negative pixel
                    if (data[x + y * xMax + z * xMax * yMax] > this.minimalPixelValue) {
                        //Initialize the start position
                        if (xposStart[0] == -1) {
                            xposStart[0] = x;
                            xposStart[1] = y;
                            xposStart[2] = z;
                        }
                    } else if (xposStart[0] > 0 && (x - xposStart[0]) > this.minimalPixelNumber) {
//                        if ( xposStart[0] > 0 && (xposStart[0] - x) > minimalPixelNumber ){
                        //If there is enough contiguous pixel
                        if (xposStart[0] < x_pos_min) {
                            x_pos_min = xposStart[0];
                        }
                        if (x > x_pos_max) {
                            x_pos_max = x;
                        }

                        if (xposStart[1] < y_pos_min) {
                            y_pos_min = xposStart[1];
                        }
                        if (y > y_pos_max) {
                            y_pos_max = y;
                        }
                        if (xposStart[2] < z_pos_min) {
                            z_pos_min = xposStart[2];
                        }
                        if (z > z_pos_max) {
                            z_pos_max = z;
                        }

                        //Set contiguous pixel to 0
                        for (int k = xposStart[0]; k <= x; k++) {
                            data[k + y * xMax + z * xMax * yMax] = 0;
                        }

                        //Reinitialize xposStart and End
                        xposStart[0] = -1;
                        xposStart[1] = -1;
                        xposStart[2] = -1;
                    }
                    //No contiguous pixel reset selection
                    else {
                        //Reinitialize selection
                        xposStart[0] = -1;
                        xposStart[1] = -1;
                        xposStart[2] = -1;
                    }
                }
            }
            //Save the coordonate
            if (x_pos_min < positionRoiSpot[0][0]) {
                positionRoiSpot[0][0] = x_pos_min;
            }
            if (x_pos_max > positionRoiSpot[0][1]) {
                positionRoiSpot[0][1] = x_pos_max;
            }

            if (y_pos_min < positionRoiSpot[1][0]) {
                positionRoiSpot[1][0] = y_pos_min;
            }
            if (y_pos_max > positionRoiSpot[1][1]) {
                positionRoiSpot[1][1] = y_pos_max;
            }
            if (z_pos_min < positionRoiSpot[2][0]) {
                positionRoiSpot[2][0] = z_pos_min;
            }
            if (z_pos_max > positionRoiSpot[2][1]) {
                positionRoiSpot[2][1] = z_pos_max;
            }
        }

        for (int i = 0; i < 3; i++) {
            positionRoiSpot[i][0] -= (positionRoiSpot[i][1] - positionRoiSpot[i][0]) * 0.25f;
            positionRoiSpot[i][1] += (positionRoiSpot[i][1] - positionRoiSpot[i][0]) * 0.25f;

            if (positionRoiSpot[i][0] < 0) {
                positionRoiSpot[i][0] = 0;
            }
            if (positionRoiSpot[i][1] < 0) {
                positionRoiSpot[i][0] = 0;
            }
        }
        return positionRoiSpot;
    }


    @Override
    public void initialize() {
        super.initialize();
    }

    @Override
    public void initialize(int dims_length, int stack_width, int stack_height, int stack_size, boolean initAll) {
        ReductionInfo reductionInfo = RXTomoJ.getInstance().getModelController().getDataController().getReductionInfo();

        //Only One as to be used
        this.minimalPixelValue = reductionInfo.getMinimalPixelValue();
        this.spotMinimumValue = reductionInfo.getMinimalPixelValue();

        this.minimalPixelNumber = reductionInfo.getMinimalPixelNumber();
        this.spotFilter = reductionInfo.getSpotFilter();

        this.stack = new RXImage(roiSpot[0][1], roiSpot[1][1], 1);
        this.array = new float[1][roiSpot[1][1] * roiSpot[0][1]];

        //Create the map for re-computing the spotMap
        map = new int[roiSpot[1][1] * roiSpot[0][1]];
        mapSum = new float[roiSpot[1][1] * roiSpot[0][1]];

        //Enforce the sampling paramters ( which is normaly setup in the first init )
        sampling = reductionInfo.getPixelSpacing();

        //Init the sampling
        sampling_X = sampling;
        sampling_Y = sampling;
        sampling_Z = sampling;

        if ( sampling_X > stack_width){
            sampling_X = 1;
            System.out.println("Warning :: Sampling X modified to 1");
        }

        if ( sampling_Y > stack_height ){
            sampling_Y = 1;
            System.out.println("Warning :: Sampling Y modified to 1");
        }

        if ( sampling_Z > stack_size ){
            sampling_Z = 1;
            System.out.println("Warning :: Sampling Z modified to 1");
        }

        //If this is the firstStep computation allow to update Model
        maxDimension[0] = roiSpot[0][1];
        maxDimension[1] = roiSpot[1][1];

        //Init the computation ROI with the same dimension and value as roiSpot
        computationRoi = new int[2][2];
        computationRoi[0][1] = roiSpot[0][1];
        computationRoi[0][0] = roiSpot[0][0];
        computationRoi[1][1] = roiSpot[1][1];
        computationRoi[1][0] = roiSpot[1][0];
    }

    @Override
    protected void computeData(float[] data, int width, int height, int size, int position, int slice) {
        //Start the Thread
        RXTomoJThreadFactory.createThreadBasicComputation(this, data, width, height, size, position, slice);
    }

    @Override
    public void compute(float[] data, int width, int height, int size, int position, int slice) {
        findSpotROI(data, width, height, size, computedRoiSpot);

        //Create the Sum detector image
        for (int x = 0; x < width * height; x++) {
            for ( int z = 1 ; z < size ; z++ ) {
                if (  data[x+z*width*height] > spotMinimumValue ) {
                    array[0][x] += data[x+z*width*height];
                    mapSum[x]++;
                }
            }
        }
    }

    @Override
    public void finalization() {
        //Test if computedRoiSpot is within the range of the normalized roiC ( this should never appends )
        roiC[0][0] = roiC[0][0]/roiNormalization[0][0];
        roiC[0][1] = roiC[0][1]/roiNormalization[0][1];
        roiC[1][0] = roiC[1][0]/roiNormalization[1][0];
        roiC[1][1] = roiC[1][1]/roiNormalization[1][1];

        if (roiC[0][0] > computedRoiSpot[0][0]) {
            computedRoiSpot[0][0] = roiC[0][0];
        }
        if (roiC[0][1] < computedRoiSpot[0][1]) {
            computedRoiSpot[0][1] = roiC[0][1];
        }
        if (roiC[1][0] > computedRoiSpot[1][0]) {
            computedRoiSpot[1][0] = roiC[1][0];
        }
        if (roiC[1][1] < computedRoiSpot[1][1]) {
            computedRoiSpot[1][1] = roiC[1][1];
        }

        //Based On the computedRoiSpot create a Map with value 1==Spot and 2==HotSpot 3==HotSpot/Spot
        mapSum  = createPixelMap(mapSum,computedRoiSpot);

        this.stack.setData(mapSum);
    }


    public int[][] getComputedRoiSpot(){
        return this.computedRoiSpot;
    }

    /**
     * Get the Detector Map to use as mask for defining HotPixel Position
     * @return The Computed Map or Null if not computed
     */
    public float[] getMap(){
        return this.mapSum;
    }

    /**
     * Get the Dimension of the 2D detector
     * @return The dimension of the 2D detector
     */
    public int[] getMaxDimension() {
        return maxDimension;
    }

    public int[][] getReconstructionFullDimension(){
        return this.roiReduction;
    }

    @Override
    public String getFunctionName() {
        return "Compute Spot";
    }

    @Override
    public float getErrorValue() {
        return -1;
    }

    public boolean is2Ddetector() {
        return is2Ddetector;
    }

    public void setIs2Ddetector(boolean is2Ddetector) {
        this.is2Ddetector = is2Ddetector;
    }
}

