/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.computationFunction;

import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.util.RXUtils;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.PixelType;
import com.soleil.nanoscopium.rxtomoj.model.data.DarkFieldInfo;
import com.soleil.nanoscopium.rxtomoj.model.utils.FittingUtils;
import javafx.beans.binding.When;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : antoine bergamaschi
 *         Date: 25/11/13
 *         Time: 10:54
 */
public class ComputeDarkField extends BasicComputationFunction {

    public enum OrientationFitFunction{
        DEFAULT,HALF;
    }


    private OrientationFitFunction fitFunction;
    private boolean doOrientationComputation = false;
    private int[][] ranges_carre;
    private float[][][] darkFields;
    private DarkFieldInfo darkFieldInfo;
    private float[] realSpot;

    private int numberOfSegment = 32;
    private int[] maxRange;

    private float[][] orientation;

    /**
     * The mask Used to segment the DarkField pattern in order to compute orientation
     * This should be initialize every time the ComputeDarkField is used
     */
    private int[] segmentationMask;

    private HashMap<String,RXImage> darkFieldImage;

    private RXImage orientationMap;

    public ComputeDarkField(){
        super();
        this.typeOfStack = ComputationUtils.StackType.DARKFIELD;
    }


    /**
     * Integrate the DarkField in a circular Area.
     * @param data The Area where the computation is performed
     * @param width The width of the Area
     * @param height the Height of the Area
     * @param size The size of the Area
     * @param dark The Resulting array where the integration value is summed
     * @param range The Distance of the four radius describing an ellipse
     * @param centerX The X center of the integration pattern
     * @param centerY The Y center of the integration pattern
     */
    private void darkFieldComputationCircle(final float[] data, int width, int height, int size, float[] dark, int[] range, float centerX, float centerY){
        //Compute a minimum and maximum radius
        //Ellipse Equation
        for ( int z = 0 ; z < size ; z++ ){
            for ( int y = 0 ; y < height ; y++ ){
                float Min_ypos = ((y-centerY)*(y-centerY))/(range[2]);
                float Max_ypos = ((y-centerY)*(y-centerY))/(range[3]);
                for( int x = 0 ; x < width; x++){
                    float Min_xpos = ((x-centerX)*(x-centerX))/(range[0]);
                    float Max_xpos = ((x-centerX)*(x-centerX))/(range[1]);
                    if ( Max_ypos+Max_xpos <= 1 && Min_ypos+Min_xpos > 1 ){
                        dark[z] += data[x + y * width + z * width * height];
                    }
                }
            }
        }
    }

    /**
     * TO TEST ONLY
     */
//    public static void darkFieldComputationCircleT(final float[] data, int width, int height, int size, float[] dark, int[] range, float centerX, float centerY){
//        //Compute a minimum and maximum radius
//        //Ellipse Equation
//        for ( int z = 0 ; z < size ; z++ ){
//            for ( int y = 0 ; y < height ; y++ ){
//                float Min_ypos = ((y-centerY)*(y-centerY))/(range[2]);
//                float Max_ypos = ((y-centerY)*(y-centerY))/(range[3]);
//                for( int x = 0 ; x < width; x++){
//                    float Min_xpos = ((x-centerX)*(x-centerX))/(range[0]);
//                    float Max_xpos = ((x-centerX)*(x-centerX))/(range[1]);
//                    if ( Max_ypos+Max_xpos <= 1 && Min_ypos+Min_xpos > 1 ){
////                        dark[z] += data[x+y*width+z*width*height];
//                        dark[x+y*width+z*width*height] += 1;
//                    }
//                }
//            }
//        }
//    }

    /**
     * Integrate the DarkField in a rectangular Area.
     * @param data The Area where the computation is performed
     * @param width The width of the Area
     * @param height the Height of the Area
     * @param size The size of the Area
     * @param dark The Resulting array where the integration value is summed
     * @param range The Distance of the four radius describing a rectangle
     * @param centerX The X center of the integration pattern
     * @param centerY The Y center of the integration pattern
     */
    private void darkFieldComputationSquare(final float[] data, int width, int height, int size, float[] dark, int[] range , float centerX, float centerY){
        //Compute a minimum and maximum radius
        for ( int z = 0 ; z < size ; z++ ){
            for ( int y = 0 ; y < height ; y++ ){
                float ypos =  Math.abs(y-centerY);
                if ( ypos <= range[3] ) {
                    for (int x = 0; x < width; x++) {
                        float xpos = Math.abs(x - centerX);
                        if (xpos <= range[1]) {
                            if ((xpos <= range[1] && xpos > range[0]) || (ypos > range[2] && ypos <= range[3])) {
                                dark[z] += data[x+y*width+z*width*height];
                            }
                        }
                    }
                }
            }
        }
    }

//    /**
//     * Test Only
//     * @param data
//     * @param width
//     * @param height
//     * @param size
//     * @param dark
//     * @param range
//     * @param centerX
//     * @param centerY
//     */
//    public static void darkFieldComputationSquareT(final float[] data, int width, int height, int size, float[] dark, int[] range , float centerX, float centerY){
//        //Compute a minimum and maximum radius
//        for ( int z = 0 ; z < size ; z++ ){
//            for ( int y = 0 ; y < height ; y++ ){
//                float ypos =  Math.abs(y-centerY);
//                if ( ypos <= range[3] ) {
//                    for (int x = 0; x < width; x++) {
//                        float xpos = Math.abs(x - centerX);
//                        if (xpos <= range[1]) {
//                            if ((xpos <= range[1] && xpos > range[0]) || (ypos > range[2] && ypos <= range[3])) {
//                                dark[x + y * width + z * width * height] += 1;
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }

    /**
     * Integrate the orientation signal of DarkField using always a circle geometry.
     * @param data The Area where the computation is performed
     * @param width The width of the Area
     * @param height the Height of the Area
     * @param size The size of the Area
     * @param dark The Resulting array where the integration value is summed
     * @param range The Distance of the four radius describing an circle ( cannot be an ellipse )
     * @param centerX The X center of the integration pattern
     * @param centerY The Y center of the integration pattern
     */
    private void darkFieldComputationOrientation(final float[] data, int width, int height, int size,
                                                 float[] dark, int[] range, float centerX, float centerY){


        float[] segments = new float[numberOfSegment];
        float[] minSegment = null;
        float[] X = null;

        //Depend on the fitFunction to apply
        if ( fitFunction == OrientationFitFunction.HALF ) {
            minSegment = new float[numberOfSegment/2];
            X = new float[minSegment.length];
        }else if (fitFunction == OrientationFitFunction.DEFAULT){
            X = new float[segments.length];
        }else{
            System.err.println("Unknown parameter :: l.192  "+this.getClass().toGenericString());
        }


        for (int i = 0; i < X.length; i++) {
            X[i] = (float) (i*((2*Math.PI)/X.length));
        }

        //Init the FittingUtils, disable the mobile mean normalization
        FittingUtils fittingUtils = new FittingUtils();
        fittingUtils.setUseMobileMean(false);

        for ( int z = 0 ; z < size ; z++ ){
            Arrays.fill(segments,0);
            for ( int y = 0 ; y < height ; y++ ){
                float Min_ypos = ((y-centerY)*(y-centerY))/(range[2]);
                float Max_ypos = ((y-centerY)*(y-centerY))/(range[3]);
                for( int x = 0 ; x < width; x++){
                    float Min_xpos = ((x-centerX)*(x-centerX))/(range[0]);
                    float Max_xpos = ((x-centerX)*(x-centerX))/(range[1]);
                    if ( Max_ypos+Max_xpos <= 1 && Min_ypos+Min_xpos > 1 ){
                        segments[segmentationMask[x+y*width]] += data[x+y*width+z*width*height];
                    }
                }
            }

            //Depend on the fit function to apply
            if ( fitFunction == OrientationFitFunction.HALF ) {
                System.arraycopy(segments,0,minSegment,0,minSegment.length);
                //From each segment compute the Sine and retrieve the Phase angle
                fittingUtils.fitCurveOnFullLengthSine(X, minSegment, 1);
                dark[z] = (float) fittingUtils.getSineParameters()[2];

                System.arraycopy(segments,4,minSegment,0,minSegment.length);
                fittingUtils.fitCurveOnFullLengthSine(X, minSegment, 1);
                dark[z] += (float) fittingUtils.getSineParameters()[2];
                dark[z] /= 2;
            }else if (fitFunction == OrientationFitFunction.DEFAULT){
//                System.arraycopy(segments,0,minSegment,0,segments.length);
                //From each segment compute the Sine and retrieve the Phase angle
                fittingUtils.fitCurveOnFullLengthSine(X, segments, 2);
                dark[z] = (float) fittingUtils.getSineParameters()[2];
            }else{
                System.err.println("Unknown parameter :: l.232  "+this.getClass().toGenericString());
            }
        }

    }


    /**
     * Integrate the orientation signal of DarkField using always a circle geometry.
     * @param data The Area where the computation is performed
     * @param width The width of the Area
     * @param height the Height of the Area
     * @param size The size of the Area
     * @param dark The Resulting array where the integration value is summed
     * @param range The Distance of the four radius describing an circle ( cannot be an ellipse )
     * @param centerX The X center of the integration pattern
     * @param centerY The Y center of the integration pattern
     */
    private void darkFieldComputationOrientationT(final float[] data, int width, int height, int size,
                                                 float[] dark, int[] range, float centerX, float centerY){

        RXImage rxImage = new RXImage(numberOfSegment,1,size);

        for ( int z = 0 ; z < size ; z++ ){
            float[] segments = new float[numberOfSegment];
            for ( int y = 0 ; y < height ; y++ ){
                float Min_ypos = ((y-centerY)*(y-centerY))/(range[2]);
                float Max_ypos = ((y-centerY)*(y-centerY))/(range[3]);
                for( int x = 0 ; x < width; x++){
                    float Min_xpos = ((x-centerX)*(x-centerX))/(range[0]);
                    float Max_xpos = ((x-centerX)*(x-centerX))/(range[1]);
                    if ( Max_ypos+Max_xpos <= 1 && Min_ypos+Min_xpos > 1 ){
                        segments[segmentationMask[x+y*width]] += data[x+y*width+z*width*height];
                    }
                }
            }
            rxImage.setSlice(segments,z);
        }
        //From each segment compute the Sine and retrieve the Phase angle
        RXUtils.RXImageToImagePlus(rxImage).show();

        while (true){
            continue;
        }
    }


    /**
     * Create a Mask for integration of DarkField orientation.
     * @param width The width of the mask to create
     * @param height The height of the mask to create
     * @param numberOfSegment The number of segment defining each slice of the orientation
     * @param centerX The X center of the pattern
     * @param centerY The Y center of the pattern
     * @return The Mask, the value of each pixel of the mask correspond to one of the segment ( 0 for first segment and so on )
     */
    public static int[] createOrientationMask(int width, int height, int numberOfSegment, float centerX, float centerY){
        int[] mask = new int[width*height];
        for ( int y = 0 ; y < height ; y++ ){
            for( int x = 0 ; x < width; x++){
                //Compute the angle
                double theta = Math.atan2((x-centerX),(y-centerY));

                if ( theta < 0 ){
                    theta += Math.PI*2;
                }

                mask[x+y*width] = (int) ((theta/(2*Math.PI))*numberOfSegment);
            }
        }
        return mask;
    }

    @Override
    public void compute(final float[] data, int width, int height, int size, int position, int slice) {
        int i, z, j, x, k, wh = width * height;

        if ( reductionInfo.isDoFilter_diffusion() ) {
            hotSpotFilter(data, spotMap, width, height, size, reductionInfo.getNumberOfIteration_diffusion(),
                    reductionInfo.getSpreadCoefficient_diffusion(), reductionInfo.getRadius_diffusion(), false, getPixelType());
        }

        int positionC = 0;
        int secondCounter = 0;
        float[] tmp_data;

        //If computation of orientation
        if ( doOrientationComputation ) {
            tmp_data = this.orientation[slice];
            float[] orientation = new float[size];
            //darkFieldComputationOrientationT(data, width, height, size, orientation, maxRange, spotCenterX, spotCenterY);
            //Add computation of DarkField orientation
            //DarkField orientation is computed on the whole range of darkfield
            darkFieldComputationOrientation(data, width, height, size, orientation, maxRange, spotCenterX, spotCenterY);

            for (i = 0; i < size; i++) {

                //Sampling loop
                for (j = i * sampling; j < (i + 1) * (sampling); j++) {
                    for (k = 0; k < sampling; k++) {
                        //If the position is contained in the tmp_data ( special case for height inferior to sampling )
                        if (position + j + (size * sampling) * k < tmp_data.length) {
                            tmp_data[position + j + positionC + (this.stack.getWidth() * sampling) * k] = orientation[i];
                        }
                    }
                }

                secondCounter++;
                //Test second counter in case of multi line processing
                if ((secondCounter * sampling) >= this.stack.getWidth() - 1) {
                    secondCounter = 0;
                    positionC += this.stack.getWidth() * (sampling - 1);
                }
            }
        }

        //For every DarkField Ranges
        for (int m =  0 ; m < darkFields.length; m++ ) {
            tmp_data = darkFields[m][slice];

            float[] dark = new float[size];

            switch (darkFieldInfo.getIntegrationType()){
                case CIRCLE:
                    darkFieldComputationCircle(data, width, height, size, dark, ranges_carre[m], spotCenterX, spotCenterY);
                    break;
                case SQUARE:
                    darkFieldComputationSquare(data, width, height, size, dark, ranges_carre[m], spotCenterX, spotCenterY);
                    break;
            }


            positionC = 0;
            secondCounter = 0;

            for (i = 0; i < size; i++) {

                //Sampling loop
                for (j = i * sampling; j < (i + 1) * (sampling); j++) {
                    for (k = 0; k < sampling; k++) {
                        //If the position is contained in the tmp_data ( special case for height inferior to sampling )
                        if (position + j + (size * sampling) * k < tmp_data.length) {
                            tmp_data[position + j + positionC + (this.stack.getWidth() * sampling) * k] = dark[i];
                        }
                    }
                }

                secondCounter++;
                //Test second counter in case of multi line processing
                if ((secondCounter * sampling) >= this.stack.getWidth() - 1) {
                    secondCounter = 0;
                    positionC += this.stack.getWidth() * (sampling - 1);
                }
            }
        }

    }

    //TODO
    public static int[] computeWholeDarkFieldRange(int[][] ranges){
        int[] result = new int[]{Integer.MAX_VALUE,Integer.MIN_VALUE};

        return result;
    }

    @Override
    public void flush() {
        int index = 0;
        darkFieldImage = new HashMap<>();
        for(String id : darkFieldInfo.getDarkArea().keySet() ){
            RXImage rxImage = new RXImage(this.stack.getWidth(),this.stack.getHeight(),this.stack.getSize());

            float[] finalStack = new float[rxImage.getGlobalSize()];

            for ( int i = 0 ; i < darkFields[index].length; i++){
                System.arraycopy(darkFields[index][i], 0, finalStack, i * darkFields[index][0].length, darkFields[index][i].length);
            }

            rxImage.setData(finalStack);

            darkFieldImage.put(id, rxImage);
            index++;
        }

        if ( doOrientationComputation ){
            orientationMap = new RXImage(this.stack.getWidth(),this.stack.getHeight(),this.stack.getSize());
            for ( int i = 0 ; i < orientation.length; i++){
    //            System.arraycopy(darkFields[index][i], 0, finalStack, i * darkFields[index][0].length, darkFields[index][i].length);
                orientationMap.setSlice(orientation[i], i);
            }
        }
    }

    @Override
    public void finalization() {
        for (String id : darkFieldImage.keySet()) {
            darkFieldImage.replace(id, RXTomoJ.getInstance().getModelController().getComputationController().regrid(darkFieldImage.get(id),this.typeOfStack));
        }

        if ( orientationMap != null && doOrientationComputation ){
            orientationMap = RXTomoJ.getInstance().getModelController().getComputationController().regrid(orientationMap,this.typeOfStack);
        }
    }


    @Override
    protected ArrayList<PixelType> getPixelType() {
        ArrayList<PixelType> p = new ArrayList<>();
        p.add(PixelType.OTHER);
        return p;
    }

    @Override
    protected int[][] getComputationRoi() {
        DarkFieldInfo darkFieldInfo = customAnalyse.getParameterStorage().getDarkFieldInfo();
        int maxX = 0;
        int maxY = 0;
        //Retrieve the largest Area
        for (int[] values : darkFieldInfo.getDarkArea().values()) {
            if ( values[1] > maxX ){
                maxX = values[1];
            }

            if ( values[3] > maxY ){
                maxY = values[3];
            }
        }

        //Compute the computationROI based on the range
        int centerX = ((roiSpot[0][1]-roiSpot[0][0])/2)+roiSpot[0][0];
        int centerY = ((roiSpot[1][1]-roiSpot[1][0])/2)+roiSpot[1][0];

        int[][] r = new int[2][2];

        r[0][0] = centerX - maxX;
        r[0][1] = centerX + maxX;
        r[1][0] = centerY - maxY;
        r[1][1] = centerY + maxY;

        //Test if r is positive (value exeding max dimension not tested)
        if ( r[0][0] < 0 ){
            r[0][0] = 0;
        }
        if ( r[1][0] < 0 ){
            r[1][0] = 0;
        }

        return r;
    }

    @Override
    public void initialize(int dims_length, int stack_width, int stack_height, int stack_size, boolean initAll) {
        super.initialize(dims_length, stack_width, stack_height, stack_size, initAll);
        darkFieldInfo = customAnalyse.getParameterStorage().getDarkFieldInfo();
        fitFunction = darkFieldInfo.getFitFunction();
        numberOfSegment = darkFieldInfo.getNumberOfSegment();
        doOrientationComputation = darkFieldInfo.isDoOrientationComputation();

        darkFields = new float[darkFieldInfo.getDarkArea().size()][this.stack.getSize()][this.stack.getHeight()*this.stack.getWidth()];
        ranges_carre = new int[darkFieldInfo.getDarkArea().size()][4];
        int i = 0;

        //Init the range depending on the integration Type
        switch (darkFieldInfo.getIntegrationType()){
            case CIRCLE:
                //Init Ranges
                for (int[] values : darkFieldInfo.getDarkArea().values()) {
                    ranges_carre[i] = new int[]{values[0]*values[0], values[1]*values[1], values[2]*values[2], values[3]*values[3]};
                    i++;
                }
                break;
            case SQUARE:
                //Init Ranges
                for (int[] values : darkFieldInfo.getDarkArea().values()) {
                    ranges_carre[i] = new int[]{values[0], values[1], values[2], values[3]};
                    i++;
                }
                break;
        }
        realSpot = (RXTomoJ.getInstance().getModelController().getDataController().getComputedStack(ComputationUtils.StackType.SPOT_MAP)).copyData();

        //Init the Range of the Segmentation ( which is by default the entire darkField Range
        maxRange = new int[4];
        //From X -> to X
        maxRange[0] = Integer.MAX_VALUE;
        maxRange[1] = Integer.MIN_VALUE;

        //From Y -> to Y
        maxRange[2] = Integer.MAX_VALUE;
        maxRange[3] = Integer.MIN_VALUE;

        for (int[] values : darkFieldInfo.getDarkArea().values()) {
            if ( maxRange[0] > values[0] ){
                maxRange[0] = values[0];
            }

            if ( maxRange[1] < values[1] ){
                maxRange[1] = values[1];
            }


            if ( maxRange[2] > values[2] ){
                maxRange[2] = values[2];
            }

            if ( maxRange[3] < values[3] ){
                maxRange[3] = values[3];
            }
        }

        if ( doOrientationComputation ) {
            //Warning here the Mask size do not correspond to the Spot Size mask
            //Init The Segmentation mask for orientation computation
            segmentationMask = createOrientationMask(roiSpot[0][1] - roiSpot[0][0], roiSpot[1][1] - roiSpot[1][0], numberOfSegment, spotCenterX, spotCenterY);

            orientation = new float[this.stack.getSize()][this.stack.getHeight() * this.stack.getWidth()];
        }
    }


    public HashMap<String, RXImage> getDarkFieldImage(){
        return this.darkFieldImage;
    }

    @Override
    public String getFunctionName() {
        return "DarkField";
    }

    @Override
    public float getErrorValue() {
        return -1;
    }

    public RXImage getOrientationMap() {
        return orientationMap;
    }
}
