/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.computationFunction;


import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by bergamaschi on 27/05/2014.
 */
public class ComputeModalities extends BasicComputationFunction {

    private ArrayList<BasicComputationFunction> functions = new ArrayList<>();

    @Override
    public void initialize() {
        functions.parallelStream().forEach(f -> {
            f.setReductionInfo(reductionInfo);
            f.initialize();
            //TODO Warning sharing memory is not perfect, different modalities may have not the same memory requirement
            ComputeModalities.this.shareMemory(f);
        });
    }

    @Override
    public void initialize(int dims_length, int stack_width, int stack_height, int stack_size, boolean initAll) {
        functions.parallelStream().forEach(f->f.initialize(dims_length, stack_width, stack_height, stack_size, initAll));
    }

    @Override
    public void computeData(float[] data, int width, int height, int size, int position, int slice) {
        if ( !isProcessClosed() ) {
            //Create the absorbtion image based on the data takken from the Hdf5 reads
            functions.parallelStream().forEach(f -> f.computeData(Arrays.copyOf(data, data.length), width, height, size, position, slice));
        }

        incrementProgress();
        fireProgressFunction();
    }

    @Override
    public void compute(float[] data, int width, int height, int size, int position, int slice) {
        System.out.println("This should not be implemented");
    }

    @Override
    public void flush() {
        functions.parallelStream().forEach(f -> f.flush());
    }

    @Override
    protected void finalization() {
        functions.parallelStream().forEach(f->f.finalization());
    }


    @Override
    protected ArrayList<ComputationUtils.PixelType> getPixelType() {
        ArrayList<ComputationUtils.PixelType> p = new ArrayList<>();
        p.add(ComputationUtils.PixelType.OTHER);
        return p;
    }

    @Override
    protected int[][] getComputationRoi() {
        //No default range
        return null;
    }

    public ArrayList<BasicComputationFunction> getFunctions(){
        return functions;
    }


    @Override
    public String getFunctionName() {
        return "Multi-Modalities";
    }

    @Override
    public float getErrorValue() {
        return -1;
    }
}
