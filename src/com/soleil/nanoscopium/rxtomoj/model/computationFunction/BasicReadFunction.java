/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.computationFunction;


import com.soleil.nanoscopium.hdf5Opener.stream.Hdf5VirtualStream;
import com.soleil.nanoscopium.hdf5Opener.stream.StreamInterface;
import com.soleil.nanoscopium.hdf5Opener.stream.StreamObject;
import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.model.data.ReductionInfo;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import com.soleil.nanoscopium.rxtomoj.model.utils.ProgressFunction;
import com.soleil.nanoscopium.rxtomoj.model.utils.ProgressFunctionListener;
import com.soleil.nanoscopium.rxtomoj.model.utils.RXTomoJThreadFactory;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by bergamaschi on 16/06/2014.
 */
//TODO add a MultiThread layer
public abstract class BasicReadFunction implements StreamInterface, ProgressFunction{

    //Progress function parameters
    private ArrayList<ProgressFunctionListener> progressFunctionListeners = new ArrayList<>();

    //Position in the numberOfWorkToDo
    private float workCompleted = 0.0f;

    //Number of Work  to do
    private float numberOfWorks = 1.0f;
    private boolean closeProcess = false;

    /**
     * Contains Every parameters Needed by the BasicComputation function to work
     * This parameters can be either set from initialization or added to customize the reduction
     */
    protected ReductionInfo reductionInfo;

    /**
     * Roi of the scan image to proceed
     */
    protected int[][] roiReduction = new int[3][2];

    /**
     * Roi of the 2D detector containing data to be used for computation
     * This is computed as the sum of each modalities ROI
     */
    protected int[][] roiSpot = new int[2][2];

    /**
     * Storage ?
     */
    protected RXImage stack;

    protected long[] neededMemory = new long[1];
    protected static final long[] currentMemory = new long[1];

    protected int numberOfDataRead = 5;

    /**
     * The number of line read in each iteration of reading process
     * This number should be coherent with the memory available for the JVM
     */
    protected int numberOfLine = -1;

    /**
     * Sampling in the 2D dimension for 3D image and 1D for 2D images
     */
    protected int sampling = 1;

    /**
     * If sampling is not adaptable for each direction
     */
    protected int sampling_X = 1;
    protected int sampling_Y = 1;
    protected int sampling_Z = 1;


    //Finalization and ComputeSpotMap parameters have to be set before computation start

    /**
     * IF only finalization function should be called
     */
    private boolean finalization = true;

    /**
     * Stack width by the compute spot reconstruction width
     */
    protected int stack_width = 0;
    /**
     * Stack Height by the compute spot reconstruction height
     */
    protected int stack_height = 0;
    /**
     * Stack Size defined by the compute spot reconstruction Size
     */
    protected int stack_size = 0;

    /**
     * Size of the last dimension of the current readed Hdf5
     */
    protected int spectrum_size = 0;

    @Override
    public void runCodeOnDataset(Hdf5VirtualStream hdf5VirtualStream) {
        //Initialize CustomAnalyse and SessionObject
        _initialize();

        if ( hdf5VirtualStream.isSpectrumDataset() ){
            _spectrumRunCode(hdf5VirtualStream);
        }else{
            _imageRunCode(hdf5VirtualStream);
        }

        if ( finalization ){
            this.finalization();
        }
    }

    /**
     * Specific Function dedicated for spectrum images
     * @param hdf5VirtualStream The VirtualStack being streamed
     */
    private void _spectrumRunCode(Hdf5VirtualStream hdf5VirtualStream){
        //Init the roiReductionSize
        if ( !((ComputeSpectrum)this).isSumSpectra() ){
            this.stack_width = roiReduction[0][1] - roiReduction[0][0];
            this.stack_height = roiReduction[1][1] - roiReduction[1][0];
            this.stack_size = roiReduction[2][1] - roiReduction[2][0];
        }
        //Initialize Spot Case
        else{
            //Compute the ROI FULL SIZE
            long[] dims = hdf5VirtualStream.getFormatedDims();

            roiReduction[0][0] = 0;
            roiReduction[0][1] = (int) dims[0];
            roiReduction[1][0] = 0;
            roiReduction[1][1] = (int) dims[1];
            roiReduction[2][0] = 0;
            roiReduction[2][1] = (int) dims[2];
            this.stack_width = roiReduction[0][1];
            this.stack_height = roiReduction[1][1];
            this.stack_size = roiReduction[2][1];
        }

        long[] dims = hdf5VirtualStream.getDims();

        //Init the spectrum_size
        spectrum_size = (int) dims[dims.length-1];

        //TODO add a special Test for Spectrum
        //Test if the Computation can be performed
        if ( !testBeforeComputation(dims) ){
            return;
        }

        switch (dims.length) {
            //1D resulting image
            //Warning due to the overlay condition this is very likely.
            case 2:
                retrieve1Dspectrum(hdf5VirtualStream);
                break;
            //2D resulting Image
            case 3:
                retrieve2Dspectrum(hdf5VirtualStream);
                break;
            //3D resulting Image
            case 4:
                retrieve3Dspectrum(hdf5VirtualStream);
                break;
            default:
                System.err.println("Spectrum dimension is uncorrect : " + dims.length);
                return;
        }
    }

    /**
     * Specific Function dedicated for 2D detectors images
     * @param hdf5VirtualStream The VirtualStack being streamed
     */
    private void _imageRunCode(Hdf5VirtualStream hdf5VirtualStream){
        //Normal initialization
        if ( !(this instanceof ComputeSpot) ){
            this.stack_width = roiReduction[0][1] - roiReduction[0][0];
            this.stack_height = roiReduction[1][1] - roiReduction[1][0];
            this.stack_size = roiReduction[2][1] - roiReduction[2][0];
        }
        //Initialize Spot Case
        else{
            //Compute the ROI FULL SIZE
            long[] dims = hdf5VirtualStream.getFormatedDims();

            roiReduction[0][0] = 0;
            roiReduction[0][1] = (int) dims[0];
            roiReduction[1][0] = 0;
            roiReduction[1][1] = (int) dims[1];
            roiReduction[2][0] = 0;
            roiReduction[2][1] = (int) dims[2];
            this.stack_width = roiReduction[0][1];
            this.stack_height = roiReduction[1][1];
            this.stack_size = roiReduction[2][1];

            //Init ROI spot as Full size
            long[] dims2 = hdf5VirtualStream.getSpotMapDims();
            //Avoid case where image is not issued from 2D detector
            if ( dims2.length == 2 ) {
                roiSpot[0][0] = 0;
                roiSpot[0][1] = (int) dims2[0];
                roiSpot[1][0] = 0;
                roiSpot[1][1] = (int) dims2[1];
            }
        }

        long[] dims = hdf5VirtualStream.getDims();

            //Test if the Computation can be performed
        if ( !testBeforeComputation(dims) ){
            return;
        }

        //Warning Only work for scanning transmission purpose where 2D image represent a point in a new 2D image
        switch (dims.length) {
            case 2:
                //Store fact that 2D detector is Used
                if ( this instanceof ComputeSpot ){
                    ((ComputeSpot)this).setIs2Ddetector(false);
                }

                System.err.println("Special Case compute from 1D image");
                //Set Basic ComputeSpot and regridding
//                computeSpot.setRoiSpot(new Rectangle(0,0,10,10));
                roiSpot = new int[2][2];
                roiSpot[0][1] = 10;
                roiSpot[1][1] = 10;
                finalization = false;
                retrieveBasicImage(hdf5VirtualStream);

                this.stack = RXTomoJ.getInstance().getModelController().getComputationController().regrid(this.stack, ComputationUtils.StackType.ABSORPTION);
//
//                if ( hdf5VirtualStackSAI != null ){
//                    Hdf5VirtualStackFactory.clear(hdf5VirtualStackSAI.getFileName(), hdf5VirtualStackSAI.getDatasetName());
//                }
                return;
            //1D resulting image
            //Warning due to the overlay condition this is very likely.
            case 3:
                retrieve1Dimage(hdf5VirtualStream);
                break;
            //2D resulting Image
            case 4:
                retrieve2Dimage(hdf5VirtualStream);
                break;
            //3D resulting Image
            case 5:
                retrieve3Dimage(hdf5VirtualStream);
                break;
            default:
                System.err.println("Image dimension is uncorrect : " + dims.length);
                return;
        }

    }


    @Override
    public void retrieve3Dimage(Hdf5VirtualStream hdf5VirtualStream) {
        final long[] dims = hdf5VirtualStream.getDims();

        long[] offset = new long[dims.length], stride = new long[dims.length], count = new long[dims.length],
                block = new long[dims.length], dimsm = new long[1];


        this.workCompleted = 0;

        //Initialization
        this._initialize(dims.length - 2, stack_width, stack_height, stack_size, finalization);

        this.numberOfWorks = (float) (((double)stack_height / (sampling_Y * numberOfLine))*Math.ceil((double)stack_size / (double)sampling_Z));
//        this.numberOfWorks = (stack_height*stack_size) / (sampling_Y*sampling_Z*numberOfLine) + 1;

        final int widthROI = roiSpot[0][1] - roiSpot[0][0];
        final int heightROI = roiSpot[1][1] - roiSpot[1][0];

        this.setMemoryNeeded(widthROI * heightROI * (stack_width/sampling_X));

        //The size of the retrieved data for each iteration
        dimsm[0] = widthROI * heightROI * (stack_width/sampling_X) * numberOfLine;

        float[] data = new float[(int) (dimsm[0])];


        count[0] = 1;
        count[1] = numberOfLine;
        count[2] = stack_width/sampling_X;
        count[3] = 1;
        count[4] = 1;

        block[0] = 1;
        block[1] = 1;
        block[2] = 1;
        block[3] = heightROI;
        block[4] = widthROI;

        stride[0] = sampling_Z;
        stride[1] = sampling_Y;
        stride[2] = sampling_X;
        stride[3] = dims[3] - heightROI;
        stride[4] = dims[4] - widthROI;


        //TODO 2D case to debug ( where 2D is in the Z orientation )
        if ( stride[3] == 0 ){
            stride[3] = 1;
        }
        if ( stride[4] == 0){
            stride[4] = 1;
        }

        //TODO set an option to control the virtual stack number of arrays parameters
        hdf5VirtualStream.iniStream(count, block, offset, stride, dimsm);
        hdf5VirtualStream.initStream(numberOfDataRead, (int) dimsm[0], (long) this.numberOfWorks);


        offset[2] = roiReduction[0][0];
        offset[3] = roiSpot[1][0];
        offset[4] = roiSpot[0][0];

        //First Loop initialization of the hdf5VirtualStream
        for (long z = 0; z < stack_size; z+=sampling_Z) {
            //Create image matrix to add to the stack for each z
            offset[0] = roiReduction[2][0] + z;
            for ( long y = 0; y < stack_height; y += (sampling_Y*numberOfLine)) {
                //Define offset ( position in the file array )
                offset[1] = roiReduction[1][0] + y;

                //Add the StreamObject to the queue
                hdf5VirtualStream.addToStream(new StreamObject(offset));
            }
        }

        //Begin the Stream
        hdf5VirtualStream.stream();

        for (long z = 0; z < stack_size; z+=sampling_Z) {
            for (long y = 0; y < stack_height; y += (sampling_Y * numberOfLine)) {
                if ( !isProcessClosed() ) {
                    hdf5VirtualStream.getData(data);

                    this._computeData(data, widthROI, heightROI, (int) (y * stack_width), (int) z);
                }
            }
        }

//        hdf5VirtualStream.stop();
        RXTomoJThreadFactory.terminateBasicComputation();

        this.flush();

        this.workCompleted = this.numberOfWorks;
    }

    @Override
    public void retrieve2Dimage(Hdf5VirtualStream hdf5VirtualStream) {
        final long[] dims = hdf5VirtualStream.getDims();

        long[] offset = new long[dims.length], stride = new long[dims.length], count = new long[dims.length],
                block = new long[dims.length], dimsm = new long[1];

        this._initialize(dims.length - 2, stack_width, stack_height, 1, finalization);

        int widthROI = roiSpot[0][1] - roiSpot[0][0];
        int heightROI = roiSpot[1][1] - roiSpot[1][0];

        this.setMemoryNeeded(widthROI * heightROI * (stack_width/sampling_X));

        this.workCompleted = 0;
        this.numberOfWorks = stack_height/(sampling_Y*numberOfLine) + 1;

        dimsm[0] = widthROI * heightROI * (stack_width/sampling_X) * numberOfLine;

        final float[] data = new float[(int) dimsm[0]];

        //If necessary more data can be retrieved
        count[0] = numberOfLine;
        count[1] = stack_width/sampling_X;
        count[2] = 1;
        count[3] = 1;

        block[0] = 1;
        block[1] = 1;
        block[2] = heightROI;
        block[3] = widthROI;

        stride[0] = sampling_Y;
        stride[1] = sampling_X;
        stride[2] = dims[2];
        stride[3] = dims[3];

        //TODO set an option to control the virtual stack number of arrays parameters
        hdf5VirtualStream.iniStream(count, block, offset, stride, dimsm);
        hdf5VirtualStream.initStream(numberOfDataRead, (int) dimsm[0], (long) this.numberOfWorks);

        //Begin the Stream
        hdf5VirtualStream.stream();

        //Create receptive memspace
        offset[1] = roiReduction[0][0];
        offset[2] = roiSpot[1][0];
        offset[3] = roiSpot[0][0];

        //First Loop initialization of the hdf5VirtualStream
        for (long y = 0; y < stack_height; y += sampling_Y*numberOfLine) {

            //Define offset ( position in the file array )
            offset[0] = roiReduction[1][0] + y;

            //Add the StreamObject to the queue
            hdf5VirtualStream.addToStream(new StreamObject(offset));
        }

        for (long y = 0; y < stack_height; y += sampling_Y*numberOfLine) {
            if ( !isProcessClosed() ) {
                hdf5VirtualStream.getData(data);

                this._computeData(data, widthROI, heightROI, (int) (y * stack_width), 0);
            }
        }

//        hdf5VirtualStream.stop();
        RXTomoJThreadFactory.terminateBasicComputation();

        //Only 1 image to set since this image is supposed to be 2D
        this.flush();

        this.workCompleted = this.numberOfWorks;
    }

    @Override
    public void retrieve1Dimage(Hdf5VirtualStream hdf5VirtualStream) {
        System.err.println("Function retrieve1Dimage not implemented Yet");
    }

    @Override
    public void retrieve3Dspectrum(Hdf5VirtualStream hdf5VirtualStream) {

        final long[] dims = hdf5VirtualStream.getDims();

        long[] offset = new long[dims.length], stride = new long[dims.length], count = new long[dims.length],
                block = new long[dims.length], dimsm = new long[1];


        //Initialization
        //ERROR
        this._initialize(dims.length - 3, stack_width, stack_height, stack_size, false);

        this.workCompleted = 0;
        this.numberOfWorks = (float) (((double)stack_height/(sampling_Y*numberOfLine)) * Math.ceil(((double)stack_size/sampling_Z)));
//        this.numberOfWorks = (stack_height*stack_size) / (sampling_Z*sampling_Y*numberOfLine) + 1;


        //The size of the retrieved data for each iteration
        dimsm[0] = (stack_width/sampling_X)*dims[3] * numberOfLine;

        float[] data = new float[(int) (dimsm[0])];


        count[0] = 1;
        count[1] = numberOfLine;
        count[2] = stack_width/sampling_X;
        count[3] = 1;

        block[0] = 1;
        block[1] = 1;
        block[2] = 1;
        block[3] = dims[3];

        stride[0] = sampling_Z;
        stride[1] = sampling_Y;
        stride[2] = sampling_X;
        stride[3] = 1;


        //TODO 2D case to debug ( where 2D is in the Z orientation )
        if ( stride[3] == 0 ){
            stride[3] = 1;
        }

        //TODO set an option to control the virtual stack number of arrays parameters
        hdf5VirtualStream.iniStream(count, block, offset, stride, dimsm);
        hdf5VirtualStream.initStream(numberOfDataRead, (int) dimsm[0], (long) this.numberOfWorks);

        //Begin the Stream
        hdf5VirtualStream.stream();

        offset[2] = roiReduction[0][0];
        offset[3] = 0;

        //First Loop initialization of the hdf5VirtualStream
        for (long z = 0; z < stack_size; z+=sampling_Z) {
            //Create image matrix to add to the stack for each z
            offset[0] = roiReduction[2][0] + z;

            for ( long y = 0; y < stack_height; y+=(sampling_Y*numberOfLine)) {
                if ( !isProcessClosed() ) {
                    //Define offset ( position in the file array )
                    offset[1] = roiReduction[1][0] + y;

                    //Add the StreamObject to the queue
                    hdf5VirtualStream.addToStream(new StreamObject(offset));
                }
            }
        }

        for (long z = 0; z < stack_size; z+=sampling_Z) {
//            final int zf = (int) z;
//            System.out.println("Z position :: " + z);
            for ( long y = 0; y < stack_height; y+=(sampling_Y*numberOfLine)) {
                hdf5VirtualStream.getData(data);

                this._computeSpecrumData(data, (int) dims[3], (int) (y * stack_width), (int) z);
            }
        }


        RXTomoJThreadFactory.terminateBasicComputation();
        this.flush();

        this.workCompleted = this.numberOfWorks;
    }

    @Override
    public void retrieve2Dspectrum(Hdf5VirtualStream hdf5VirtualStream) {
        final long[] dims = hdf5VirtualStream.getDims();

        long[] offset = new long[dims.length], stride = new long[dims.length], count = new long[dims.length],
                block = new long[dims.length], dimsm = new long[1];

        this._initialize(dims.length - 3, stack_width, stack_height, 1, false);

        this.workCompleted = 0;
        this.numberOfWorks = stack_height/(sampling_Y*numberOfLine) + 1;

        dimsm[0] = dims[2] * (stack_width/sampling_X)*numberOfLine;

        final float[] data = new float[(int) dimsm[0]];

        //If necessary more data can be retrieved
        count[0] = numberOfLine;
        count[1] = stack_width/sampling_X;
        count[2] = 1;

        block[0] = 1;
        block[1] = 1;
        block[2] = dims[2];

        stride[0] = sampling_Y;
        stride[1] = sampling_X;
        stride[2] = 1;

        //TODO set an option to control the virtual stack number of arrays parameters
        hdf5VirtualStream.iniStream(count, block, offset, stride, dimsm);
        hdf5VirtualStream.initStream(5, (int) dimsm[0], (long) this.numberOfWorks-1);

        //Begin the Stream
        hdf5VirtualStream.stream();

        //Create receptive memspace
        offset[1] = roiReduction[0][0];
        offset[2] = 0;

        //First Loop initialization of the hdf5VirtualStream
        for (long y = 0; y < stack_height; y += sampling_Y*numberOfLine) {

            //Define offset ( position in the file array )
            offset[0] = roiReduction[1][0] + y;

            //Add the StreamObject to the queue
            hdf5VirtualStream.addToStream(new StreamObject(offset));
        }


        for (long y = 0; y < stack_height; y += sampling_Y*numberOfLine) {
            if ( !isProcessClosed() ) {
                hdf5VirtualStream.getData(data);

                this._computeSpecrumData(data, (int) dims[2], (int) (y * stack_width), 0);
            }
        }


        RXTomoJThreadFactory.terminateBasicComputation();
        //Only 1 image to set since this image is supposed to be 2D
        this.flush();

        this.workCompleted = this.numberOfWorks;
    }

    @Override
    public void retrieve1Dspectrum(Hdf5VirtualStream hdf5VirtualStream) {
        System.err.println("Function retrieve1Dspectrum not implemented Yet");
    }

    public void retrieveBasicImage(Hdf5VirtualStream hdf5VirtualStream){
        final long[] dims = hdf5VirtualStream.getDims();

        long[] offset = new long[dims.length], stride = new long[dims.length], count = new long[dims.length],
                block = new long[dims.length], dimsm = new long[1];

        if ( dims.length == 3 ) {
            this._initialize(dims.length - 1, (int) dims[2], (int) dims[1], (int) dims[0], finalization);
        }
        else{
            this._initialize(dims.length, (int) dims[1], (int) dims[0], 1, finalization);
        }

        this.setMemoryNeeded(this.stack.getWidth());

        this.workCompleted = 0;
        this.numberOfWorks = dims[0]/(sampling*numberOfLine);
        dimsm[0]=1;
        for (int i = 0 ; i < dims.length; i++){
            dimsm[0]*=dims[i];
        }

        final float[] data = new float[(int) dimsm[0]];

        Arrays.fill(count,1);

        //Read the entire block
        for ( int i = 0 ; i < dims.length ; i++){
            block[i] = dims[i];
        }

        //TODO set an option to control the virtual stack number of arrays parameters
        hdf5VirtualStream.iniStream(count, block, offset, null, dimsm);
        hdf5VirtualStream.initStream(5, (int) dimsm[0], (long) this.numberOfWorks);

        //Begin the Stream
        hdf5VirtualStream.stream();

        //Add the StreamObject to the queue
        //Read every things
        hdf5VirtualStream.addToStream(new StreamObject(offset));

        hdf5VirtualStream.getData(data);


//        //IF there is an SAI
//        if ( hdf5VirtualStackSAI != null ){
//            for ( int y = 0  ; y < hdf5VirtualStackSAI.getNSlice()-1; y++){
//                float[] SAIA = getSAIValues(y, (int) hdf5VirtualStackSAI.getGlobalDimension()[1]);
//                for ( int i = 0 ; i < SAIA.length; i++ ){
//                    data[i + y * SAIA.length] = (float) -Math.log(data[i + y * SAIA.length]/SAIA[i]);
//                }
//            }
//        }

        //No computation is performed
        this.stack.setData(data);

        fireProgressFunction();

        this.workCompleted = this.numberOfWorks;
    }

    private void _initialize() {
        //If the reductionInfo has not been set already
        if ( this.reductionInfo == null ) {
            reductionInfo = RXTomoJ.getInstance().getModelController().getDataController().getReductionInfo();
        }

        this.roiReduction = reductionInfo.getReductionROI();
        this.numberOfLine = reductionInfo.getNumberOfLine();

        if ( !(this instanceof ComputeSpot) ){
            this.roiSpot = reductionInfo.getTransmissionROI();
        }

        //Case of using low resolution
        if ( reductionInfo.isUseLowResolution() ){
            this.sampling = reductionInfo.getPixelSpacing();
        }

        //Call the abstract function
        initialize();
    }

    private void _initialize(int dims_length, int stack_width, int stack_height, int stack_size, boolean initAll) {
        //Init the sampling
        sampling_X = sampling;
        sampling_Y = sampling;
        sampling_Z = sampling;

        if ( sampling_X > stack_width){
            sampling_X = 1;
            System.out.println("Warning :: Sampling X modified to 1");
        }

        if ( sampling_Y > stack_height ){
            sampling_Y = 1;
            System.out.println("Warning :: Sampling Y modified to 1");
        }

        if ( sampling_Z > stack_size ){
            sampling_Z = 1;
            System.out.println("Warning :: Sampling Z modified to 1");
        }

        initialize(dims_length,stack_width,stack_height,stack_size,initAll);

    }

    /**
     * Data is send part by part to allow child class to multi-thread the computation process
     * @param data The data retrieve from one read in the Hdf5 file
     * @param width The width of the retrieved data
     * @param height The height of the retrieved data
     * @param position The Position in the whole Hdf5 dataset
     * @param slice The Z position in the whole dataset
     */
    private void _computeData(float[] data, int width, int height, int position, int slice) {
        for ( int y = 0 ; y < numberOfLine ; y++ ) {

            //For each Line Add a Memory offset
            this.addMemory();


            //If the Thread is Running and Memory is available
            if (  RXTomoJThreadFactory.isRunning() && RXTomoJThreadFactory.checkMemory(currentMemory[0]) >= 0){

                //Add a length Test
                int length = width * height * (stack_width/sampling_X);
                if ( length > data.length ){
                    length = data.length;
                }
                float[] tmp_data = new float[length];
                System.arraycopy(data,y * (width * height * (stack_width/sampling_X)),tmp_data,0,length);

                computeData(tmp_data, width, height, (stack_width / sampling_X), position + y * (stack_width / sampling_X), slice);
            }
            //Bug with the memory
            else{
                return;
            }
        }
    }

    /**
     * Data is send part by part to allow child class to multi-thread the computation process
     * @param data The data retrieve from one read in the Hdf5 file
     * @param width The width of the retrieved data
     * @param position The Position in the whole Hdf5 dataset
     * @param slice The Z position in the whole dataset
     */
    private void _computeSpecrumData(float[] data, int width, int position, int slice) {
        for ( int y = 0 ; y < numberOfLine ; y++ ) {

            //For each Line Add a Memory offset
            this.addMemory();


            //If the Thread is Running and Memory is available
            if (  RXTomoJThreadFactory.isRunning() && RXTomoJThreadFactory.checkMemory(currentMemory[0]) >= 0){

                //Add a length Test
                int length = width * (stack_width/sampling_X);
                if ( length > data.length ){
                    length = data.length;
                }
                //Transfert the Data in a new Pool of data
                final float[] tmp_data = new float[length];
                System.arraycopy(data,y * (width * (stack_width / sampling_X)), tmp_data, 0, length);

                computeSpectrumData(tmp_data, width, (stack_width / sampling_X), position + y * (stack_width / sampling_X), slice);
            }
            //Bug with the memory
            else{
                System.err.println("Error Memory");
                return;
            }
        }
    }


    //Function Call be the readFunction to overwrite

    /**
     * Initialize the basic data to performs the read function
     */
    protected abstract void initialize();

    /**
     * Initialize the basic data to performs computation or other thing along with the read operation
     * @param dims_length The RANK of the dataset
     * @param stack_width The width of the resulting stack
     * @param stack_height The height of the resulting stack
     * @param stack_size The Size of the resulting stack
     * @param initAll TO REMOVE
     */
    protected abstract void initialize(int dims_length, int stack_width, int stack_height, int stack_size, boolean initAll);

    /**
     * This is the basic computation Function. This function must be called to construct each slide of the resulting stack.
     * Normally this function is used to fill the array tmp_data.
     * @param data The data retrieve from one read in the Hdf5 file
     * @param width The width of the retrieved data
     * @param height The height of the retrieved data
     * @param size The dimension of the data in the Z direction ( Warning this may correspond to a whole line or more )
     * @param position The Position in the whole Hdf5 dataset
     * @param slice The Z position in the whole dataset
     */
    protected abstract void computeData(float[] data, int width, int height, int size, int position, int slice);

    /**
     * This is the basic computation Function. This function must be called to construct each slide of the resulting stack.
     * Normally this function is used to fill the array tmp_data.
     * @param data The data retrieve from one read in the Hdf5 file
     * @param width The width of the retrieved data
     * @param size The dimension of the data in the Z direction ( Warning this may correspond to a whole line or more )
     * @param position The Position in the whole Hdf5 dataset
     * @param slice The Z position in the whole dataset
     */
    protected abstract void computeSpectrumData(float[] data, int width, int size, int position, int slice);


    /**
     * Flush the stored array in the finalStack which will be stored in the current Stack
     */
    protected abstract void flush();


    /**
     * This function must be called at the end of the computation process ( when the stack is already created ).
     * It can be then use to post process the stack.
     */
    protected abstract void finalization();

    /**
     * Need to throw Custom error
     * Test if the data which will be read respect some defined proportion
     * @param dims The dimension of the Data to read
     * @return true if the computation can proceed
     */
    //TODO Re-write Test
    private boolean testBeforeComputation(long[] dims){

        System.err.println("Change the Test Before Computation l.788 "+this.getClass().toGenericString());

        boolean bol = true;
        int groupOfLines = numberOfLine*sampling;
        long numberOfDataPerLine = 0;
        String errorMessage = "";
        int error = 0;
        //Before launching test if sampling / number of line is consistent with memory and dimension of the data
//        if ( !computeSpotMap ) {
//            RXImage imageStack = (RXImage) RXTomoJ.getInstance().getModelController().getDataController().getStorageData(StackType.SPOT_MAP);
//            if (imageStack == null ||
//                    this.computeSpot.toRec().getHeight() != imageStack.getHeight() ||
//                    this.computeSpot.toRec().getWidth() != imageStack.getWidth()) {
//
//                error = 6;
//            }
//        }
//
//        if ( this.computeSpot == null || (this.computeSpot != null && this.computeSpot.getHotSpot_detection() == null) ){
//            error = 7;
//        }
        //Warning Only work for scanning transmission purpose where 2D image represent a point in a new 2D image
//        switch (dims.length) {
//            //1D resulting image
//            case 2:
////                System.err.println("1D Data not supported here");
////                error = 1;
//            break;
//            //Warning due to the overlay condition this is very likely.
//            case 3:
//                System.err.println("1D Data not supported here");
//                error = 1;
//            break;
//            //2D resulting Image
//            case 4 :
//                if (dims[0] % groupOfLines != 0) {
//                    error = 2;
//                    errorMessage += " "+dims[0]+" % "+groupOfLines+" != 0 ";
//                }
//                numberOfDataPerLine = (dims[3]*dims[2]*dims[1])/sampling;
//                break;
//            //3D resulting Image
//            case 5:
//                if (dims[1] % groupOfLines != 0) {
//                    error = 3;
//                    errorMessage += " "+dims[1]+" % "+groupOfLines+" != 0 ";
//                }
//                numberOfDataPerLine = (dims[4]*dims[3]*dims[2])/sampling;
//                break;
//            default:
//                System.err.println("Image dimension is uncorrect : " + dims.length);
//                error = 4;
//        }
//
//        if ( numberOfDataPerLine*groupOfLines < 0 || numberOfDataPerLine*groupOfLines > Integer.MAX_VALUE ){
//            error = 8;
//        }
//
//        //Test memory, get the current available memory
//            MemoryMXBean memoryMXBean = ManagementFactory.getMemoryMXBean();
//            MemoryUsage memoryUsage = memoryMXBean.getHeapMemoryUsage();
//            long memoryToBeUse = (long) (memoryUsage.getMax() * 0.75);
//
////            long maximumDimension = memoryToBeUse / Float.BYTES;
//
//            System.out.println("Maximum Dimension / Estimation :: " + (memoryToBeUse/1000000.0f) + " Mio  / "+ ((numberOfLine*numberOfDataPerLine*Float.BYTES)/1000000.0f)+" Mio.");
////        Number of lines should always be proportional with the groupOfLines which will be readied simultaneously
//
////        Size of memory to be read simultaneously
//        if  (memoryToBeUse < (numberOfLine*numberOfDataPerLine*Float.BYTES* numberOfDataRead) ){
//            error = 5;
//            errorMessage += " "+(memoryToBeUse/1000000.0f) + " Mio / "+ ((numberOfLine*numberOfDataPerLine*Float.BYTES* numberOfDataRead)/1000000.0f)+" Mio.";
//        }
//
//        //If there is a View
//        if ( RXTomoJ.getInstance().getView() != null ) {
//            switch (error) {
//                case 1:
//                    DialogMessage.showMessage_ID("rxTomoJModelController_error_testBeforeReading_dimension_1", errorMessage, "JAVA_ERROR", DialogMessage.ERROR);
//                    System.err.println(RXTomoJ.getInstance().getView().getRessourceValue("rxTomoJModelController_error_testBeforeReading_dimension_1"));
//                    bol = false;
//                    break;
//                case 2:
//                    DialogMessage.showMessage_ID("rxTomoJModelController_error_testBeforeReading_dimension_2", errorMessage, "JAVA_ERROR", DialogMessage.ERROR);
//                    System.err.println(RXTomoJ.getInstance().getView().getRessourceValue("rxTomoJModelController_error_testBeforeReading_dimension_2"));
//                    bol = false;
//                    break;
//                case 3:
//                    DialogMessage.showMessage_ID("rxTomoJModelController_error_testBeforeReading_dimension_3", errorMessage, "JAVA_ERROR", DialogMessage.ERROR);
//                    System.err.println(RXTomoJ.getInstance().getView().getRessourceValue("rxTomoJModelController_error_testBeforeReading_dimension_3"));
//                    bol = false;
//                    break;
//                case 4:
//                    DialogMessage.showMessage_ID("rxTomoJModelController_error_testBeforeReading_dimension", errorMessage, "JAVA_ERROR", DialogMessage.ERROR);
//                    System.err.println(RXTomoJ.getInstance().getView().getRessourceValue("rxTomoJModelController_error_testBeforeReading_dimension"));
//                    bol = false;
//                    break;
//                case 5:
//                    DialogMessage.showMessage_ID("rxTomoJModelController_error_testBeforeReading_memory", errorMessage, "JAVA_ERROR", DialogMessage.ERROR);
//                    System.err.println(RXTomoJ.getInstance().getView().getRessourceValue("rxTomoJModelController_error_testBeforeReading_memory"));
//                    bol = false;
//                    break;
//                case 6:
//                    DialogMessage.showMessage_ID("rxTomoJModelController_error_testBeforeReading_spotMap", errorMessage, "JAVA_ERROR", DialogMessage.ERROR);
//                    System.err.println(RXTomoJ.getInstance().getView().getRessourceValue("rxTomoJModelController_error_testBeforeReading_spotMap"));
//                    bol = false;
//                    break;
//                case 7:
//                    DialogMessage.showMessage_ID("rxTomoJModelController_error_testBeforeReading_computeSpot", errorMessage, "JAVA_ERROR", DialogMessage.ERROR);
//                    System.err.println(RXTomoJ.getInstance().getView().getRessourceValue("rxTomoJModelController_error_testBeforeReading_computeSpot"));
//                    bol = false;
//                    break;
//                case 8:
//                    DialogMessage.showMessage_ID("rxTomoJModelController_error_testBeforeReading_largeArray", errorMessage, "JAVA_ERROR", DialogMessage.ERROR);
//                    System.err.println(RXTomoJ.getInstance().getView().getRessourceValue("rxTomoJModelController_error_testBeforeReading_largeArray"));
//                    bol = false;
//                    break;
//                default:
//                    break;
//            }
//
//        }else if ( error > 0){
//            bol = false;
//        }
//
//        if ( !bol ){
//            workCompleted = numberOfWorks = 1;
//        }

        return bol;
    }



    /**
     * Get the number of dimension of this Dataset, used to switch on the corresponding computation function
     * @param dimension An array containing the dimension in order z,y,x
     * @return the number of dimension of the resulting stack after processing
     */
    public int getDatasetDimension(long[] dimension){
        return dimension.length;
    }

    //Manage works monitoring

    @Override
    public float getWorkDone() {
        return this.workCompleted / this.numberOfWorks;
    }

    @Override
    public String getWorkDoneString() {
        return "Number Of Iteration  : " + (int) this.workCompleted + " / " + (int) this.numberOfWorks;
    }

    @Override
    public void abort() {
        this.closeProcess = true;
    }

    @Override
    public void setNumberOfWorks(float numberOfWorks) {
        this.numberOfWorks = numberOfWorks;
    }


    @Override
    public void hardStop() {
        this.closeProcess = true;
        this.workCompleted = this.numberOfWorks;
    }

    protected void endWorks(){
        this.workCompleted = this.numberOfWorks = 1;
    }

    //Manage Memory usage

    /**
     * Set the Memory need to complet 1 call in the read procedure
     * @param size The float data length
     */
    public void setMemoryNeeded(long size){
        neededMemory[0] =  size*Float.SIZE;
    }

    public long[] getMemoryNeeded(){
        return neededMemory;
    }

    public void setMemoryNeeded(long[] neededMemory){
        this.neededMemory = neededMemory;
    }

    /**
     *
     */
    public void shareMemory(BasicReadFunction... sharedWith){
        for ( BasicReadFunction b : sharedWith ){
            b.setMemoryNeeded(this.getMemoryNeeded());
        }
    }


    /**
     * Clear memory usage of one read step
     */
    public void clearMemory(){
        currentMemory[0] -= neededMemory[0];
    }

    /**
     * Add memory usage of one read step
     */
    public void addMemory(){
        currentMemory[0] += neededMemory[0];
    }

    /**
     * This function must be called before computation.
     * Use a custome reductionInfo( different from the one stored in the Model)
     * this function must be only used in the scope of the ComputationController
     * @param reductionInfo The reductionInfo to use ( instead of the model one )
     */
    public void setReductionInfo(ReductionInfo reductionInfo) {
        this.reductionInfo = reductionInfo;
        //Init the Object
        _initialize();
    }

    //Manage Progress Function listeners
    public void addProgressListener(ProgressFunctionListener functionListener){
        progressFunctionListeners.add(functionListener);
    }

    public void removeProgressListener(ProgressFunctionListener functionListener) {
        progressFunctionListeners.remove(functionListener);
    }

    public void removeAllProgressListener(){
        progressFunctionListeners.clear();
    }

    /**
     * Increment the workComplete counter by 1
     */
    protected void incrementProgress(){
        this.workCompleted++;
    }

    /**
     * @return True if the process is closed
     */
    protected boolean isProcessClosed(){
        return this.closeProcess;
    }

    public void fireProgressFunction() {
        progressFunctionListeners.forEach(l -> l.progress(getWorkDone()));
    }

    public void setFinalization(boolean finalization) {
        this.finalization = finalization;
    }
}
