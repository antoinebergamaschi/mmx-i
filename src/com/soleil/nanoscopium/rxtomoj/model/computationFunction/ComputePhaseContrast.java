/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.computationFunction;

import cern.colt.matrix.io.MatrixInfo;
import cern.colt.matrix.tfcomplex.impl.DenseFComplexMatrix1D;
import cern.colt.matrix.tfcomplex.impl.DenseFComplexMatrix2D;
import cern.colt.matrix.tfloat.FloatMatrix2D;
import cern.colt.matrix.tfloat.impl.DenseFloatMatrix1D;
import cern.colt.matrix.tfloat.impl.DenseFloatMatrix2D;
import com.soleil.nanoscopium.hdf5Opener.Hdf5Handler;
import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.model.ShiftImage.MirroringType;
import com.soleil.nanoscopium.rxtomoj.model.data.PhaseContrastInfo;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.PixelType;
import com.soleil.nanoscopium.rxtomoj.model.utils.RXTomoJThreadFactory;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * This Class set the function used to retrieve PhaseContrast from the data in a hdf5 file opened with
 * {@link Hdf5Handler}.
 *
 * @author : antoine bergamaschi
 *         Date: 23/11/13
 *         Time: 10:59
 */
public class ComputePhaseContrast extends BasicComputationFunction {

    private RXImage imageStackGradientX;
    private RXImage imageStackGradientY;

    private MirroringType real,imaginary;
    private PhaseRetrievalAlgo phaseRetrievalAlgo;
    private PhaseComputationTechniques phaseComputationTechniques;
    private boolean useMirroring;
    private boolean doXnormalization;
    private boolean doYnormalization;

//    final private double[] minimalStepValue = new double[1];
//    final private double[] expectedErrorPerPixel = new double[1];
//    final private int[] maximumNumberOfIteration = new int[1];

    private final static float[] error = new float[1];

    public enum PhaseRetrievalAlgo {
        REALSPACE_JACOBI,REALSPACE_GAUSSIAN,FOURRIER,EXPERIMENTAL;

        public RXImage phaseRetrieval(RXImage phaseX, RXImage phaseY,final double minimalStepValue,
                                      final double expectedErrorPerPixel, final int maximumNumberOfIteration){
            RXImage result = null;
            switch (this){
                case REALSPACE_JACOBI:
                    result = _SouthWellJacobi(phaseX,phaseY,minimalStepValue,expectedErrorPerPixel,maximumNumberOfIteration);
                    break;
                case REALSPACE_GAUSSIAN:
                    result = _SouthWellGausian(phaseX, phaseY,minimalStepValue,expectedErrorPerPixel,maximumNumberOfIteration);
                    break;
                case FOURRIER:
                    if ( phaseX.getHeight() == 1 ){
                        result = phaseContrastReconstruction1D(phaseX,phaseY);
                    }else{
                        result = phaseContrastReconstruction(phaseX, phaseY);
                    }
                    break;
                case EXPERIMENTAL:
                    result = _ModalEstimation(phaseX,phaseY);
                    break;
                default:
                    System.err.println("Unknwon PhaseRetrievalAlgo "+this);
                    break;
            }

            return result;
        }

        public boolean isFourrier(){
            switch (this){
                case FOURRIER:
                    return true;
                case REALSPACE_GAUSSIAN:
                case REALSPACE_JACOBI:
                case EXPERIMENTAL:
                    return false;
                default:
                    return false;
            }
        }
    }

    public enum PhaseComputationTechniques {
        CENTEROFGRAVITY,INTENSITYMAP;

        public void phaseComputation(float[] data, int width, int height, int size, float[] centerX, float[] centerY,float posCenterX, float posCenterY){
            switch (this){
                case CENTEROFGRAVITY:
                    createGradientFromGravityCenter(data, width, height, size, centerX, centerY, posCenterX, posCenterY);
                    break;
                case INTENSITYMAP:
                    createGradientFromIntensity(data, width, height, size, centerX, centerY, posCenterX, posCenterY);
                    break;
                default:
                    System.err.println("Unknwon PhaseComputationTechniques "+this);
                    break;
            }
        }
    }



    //EXPERIMENTAL MODAL ESTIMATION

    /**
     * 2 dimensional Legendre Polynomials
     */
    public enum LegendrePolynomial{
        M0,M1,M2,M3,M4,M5,M6,M7,M8,M9;

        private static double gN = -1;
        private static double dN = -1;

        public int getOrder(){
            switch (this){
                case M0:
                    return 0;
                case M1:
                    return 1;
                case M2:
                    return 2;
                case M3:
                    return 3;
                case M4:
                    return 4;
                case M5:
                    return 5;
                case M6:
                    return 6;
                case M7:
                    return 7;
                case M8:
                    return 8;
                case M9:
                    return 9;
            }
            return -1;
        }

        public static LegendrePolynomial getLegendreOrder(int i){
            switch (i){
                case 0:
                    return M0;
                case 1:
                    return M1;
                case 2:
                    return M2;
                case 3:
                    return M3;
                case 4:
                    return M4;
                case 5:
                    return M5;
                case 6:
                    return M6;
                case 7:
                    return M7;
                case 8:
                    return M8;
                case 9:
                    return M9;
            }
            return null;
        }

        /**
         * @param x The Slope measured at position x
         * @param y The Slope measured at position y
         * @return The result of the Legendre polynom of any order between 0 and 9
         */
        public double F(double x, double y){
            switch (this){
                case M0:
                    return 1;
                case M1:
                    return x;
                case M2:
                    return y;
                case M3:
                    return 3*(x*x)-dN;
                case M4:
                    return 3*(y*y)-dN;
                case M5:
                    return x*y;
                case M6:
                    return (3*(x*x)-dN)*y;
                case M7:
                    return (3*(y*y)-dN)*x;
                case M8:
                    return (5*(x*x)-gN)*x;
                case M9:
                    return (5*(y*y)-gN)*y;
            }
            return -1;
        }

        /**
         * @param x The Slope measured at position x
         * @param y The Slope measured at position y
         * @return The result of the derivate in x Legendre polynom of any order between 0 and 9
         */
        public double dFx(double x, double y){
            switch (this){
                case M0:
                    return 0;
                case M1:
                    return 1;
                case M2:
                    return 0;
                case M3:
                    return 6*x;
                case M4:
                    return 0;
                case M5:
                    return y;
                case M6:
                    return 6*x*y;
                case M7:
                    return 3*(y*y)-dN;
                case M8:
                    return 15*(x*x)-gN;
                case M9:
                    return 0;
            }
            return -1;
        }

        /**
         * @param x The Slope measured at position x
         * @param y The Slope measured at position y
         * @return The result of the derivate in y Legendre polynom of any order between 0 and 9
         */
        public double dFy(double x, double y){
            switch (this){
                case M0:
                    return 0;
                case M1:
                    return 0;
                case M2:
                    return 1;
                case M3:
                    return 0;
                case M4:
                    return 6*y;
                case M5:
                    return x;
                case M6:
                    return 3*(x*x)-dN;
                case M7:
                    return 6*x*y;
                case M8:
                    return 0;
                case M9:
                    return 15*(y*y)-gN;
            }
            return -1;
        }

        /**
         * @return The result the normalization constante for each Legendre polynoms betwwen 0-9
         */
        public double n(){
            switch (this){
                case M0:
                    return 1;
                case M1:
                case M2:
                    return 3/dN;
                case M3:
                case M4:
                    return 5/((3*dN*gN)-(5*(dN*dN)));
                case M5:
                    return 9/(dN*dN);
                case M6:
                case M7:
                    return 15/((3*dN*dN*gN)-(5*dN*dN*dN));
                case M8:
                case M9:
                    return 7/((6*gN*gN*dN)+(25*(dN-gN)*dN*dN));
            }
            return -1;
        }

        private static void initConstant(double numericalAperture, int width, int height) {
            dN = (numericalAperture / 2) * (numericalAperture / 2) * (1 - (1 / ((width * height) * (width * height))));
            gN = (3 * (numericalAperture / 2) * (numericalAperture / 2)) * (1 - (7 / (3 * (width * height) * (width * height))));
        }



    }

    public ComputePhaseContrast(){
        super();
        this.typeOfStack = ComputationUtils.StackType.PHASECONTRAST;
    }

    /**
     * Create the Gradient image based on the intensity distribution instead of the center of gravity ( thus normalized by the total intensity)
     * Warning Boundary condition sum = 0 on row and columns are applied
     * @param data The spot dataset array
     * @param width the width of the array
     * @param height The height of the array
     * @param size The size of the array ( each plane X/Y correspond to a pixel in the final array )
     * @param centerX The array filled be this function containing the X center of gravity values
     * @param centerY The array filled be this function containing the Y center of gravity values
     */
    private static void createGradientFromIntensity(float[] data, int width, int height, int size, float[] centerX, float[] centerY, float posCenterX, float posCenterY){

            int z, y, x, wh = width * height;
            float total;

            int posYStart = 0;
            int posXStart = 0;
            int posYend = height;
            int posXend = width;

            int normH = (int) ((height / 2) - posCenterY);
            int normW = (int) ((width / 2) - posCenterX);
            if (normH > 0) {
                posYStart = normH;
            } else {
                posYend = height+normH;
            }

            if (normW > 0) {
                posXStart = normW;
            } else {
                posXend = width+normW;
            }


//      Without normalization
        for (z = 0; z < size; z++) {
            total = 0.0f;
            for (y = posYStart; y < posYend; y++) {
                for (x = posXStart; x < posXend; x++) {
                    total += data[x + y * width + z * wh];
                    if (x < posCenterX) {
                        centerX[z] -= data[x + y * width + z * wh];
                    } else {
                        centerX[z] += data[x + y * width + z * wh];
                    }
                    if (y < posCenterY) {
                        centerY[z] += data[x + y * width + z * wh];
                    } else {
                        centerY[z] -= data[x + y * width + z * wh];
                    }
                }
            }
            if ( total == 0 ){
                total = 1;
            }

            centerX[z] /= total;
            centerY[z] /= total;
        }
    }


    /**
     * Create the Gradient image based on the intensity distribution instead of the center of gravity ( thus normalized by the total intensity)
     * Warning Boundary condition sum = 0 on row and columns are applied
     * @param data The spot dataset array
     * @param width the width of the array
     * @param height The height of the array
     * @param size The size of the array ( each plane X/Y correspond to a pixel in the final array )
     * @param centerX The array filled be this function containing the X center of gravity values
     * @param centerY The array filled be this function containing the Y center of gravity values
     */
    private static void createGradientFromGravityCenter(float[] data, int width, int height, int size, float[] centerX, float[] centerY, float posCenterX, float posCenterY){

        int z, y, x, wh = width * height;
        float total;

        int posYStart = 0;
        int posXStart = 0;
        int posYend = height;
        int posXend = width;

        int normH = (int) ((height / 2) - posCenterY);
        int normW = (int) ((width / 2) - posCenterX);
        if (normH > 0) {
            posYStart = normH;
        } else {
            posYend = height+normH;
        }

        if (normW > 0) {
            posXStart = normW;
        } else {
            posXend = width+normW;
        }


        //Without normalization
        for (z = 0; z < size; z++) {
            float[] data_h = new float[posYend-posYStart];
            float[] data_w = new float[posXend-posXStart];
            total = 0.0f;
            for (y = posYStart; y < posYend; y++) {
                //Make the summation for each line
                for (x = posXStart; x < posXend; x++) {
                    data_h[y-posYStart] += data[x + y * width + z * wh];
                }
                centerY[z] += (y-posYStart+1)*data_h[y-posYStart];
                total += data_h[y-posYStart];
            }
            if ( total == 0 ){
                total = 1;
            }

            //warning Here to be consistent with data Y is positive in top and neg in bottom
            centerY[z] /= total;
            centerY[z] *= -1;

            total = 0.0f;
            for (x = posXStart; x < posXend; x++) {
                for (y = posYStart; y < posYend; y++) {
                    data_w[x-posXStart] += data[x + y * width + z * wh];
                }
                centerX[z] += (x-posXStart+1)*data_w[x-posXStart];
                total += data_w[x-posXStart];
            }

            if ( total == 0 ){
                total = 1;
            }

            centerX[z] /= total;
        }
    }

    /**
     * In the case of the Phase gradient it is physically impossible to have a mean value strictly superior to 0.
     * The only condition is that the object has to be isolated ( an appropriate mirroring can be applied )
     * @param gradientX the vertical phase gradient of the image
     * @param gradientY the horizontal phase gradient of the image
     */
    /**
     * In the case of the Phase gradient it is physically impossible to have a mean value strictly superior to 0.
     * The only condition is that the object has to be isolated ( an appropriate mirroring can be applied )
     * @param gradientX the vertical phase gradient of the image
     * @param gradientY the horizontal phase gradient of the image
     * @param width the width of the array
     * @param height The height of the array
     * @param size The size of the array
     */
    public void testZeroCondition(float[] gradientX, float[] gradientY, int width, int height , int size){
        int z,y,x;
        float[] meanX=new float[height*size],meanY = new float[width*size];
        for (z=0;z<size;z++){
            for (y=0;y<height;y++){
                for(x=0;x<width;x++){
                    meanY[x] += gradientY[x + y * width + z * height * width];
                    meanX[y] += gradientX[x + y * width + z * height * width];
                }
//                meanX[y + z*height] /= width;
            }
//            for(x=0;x<width;x++) {
//                meanY[x + z*width] /= height;
//            }
        }
//        System.err.print(true);
//
//        for (z=0;z<size;z++){
//            for (y=0;y<height;y++){
//                for(x=0;x<width;x++){
//                    gradientY[x + y * width + z * height * width] -= meanY[x + z * width];
//                    gradientX[x + y * width + z * height * width] -= meanX[y + z * height];
//                }
//            }
//        }

    }


    /**
     * Test call to phaseContrastReconstruction private function
     *
     * @param phaseX the reconstructed gradient in the X direction
     * @param phaseY the reconstructed gradient in the Y direction
     * @return The resulting image.
     */
    public RXImage phaseContrastReconstructionT(RXImage phaseX, RXImage phaseY) {
        this.stack = new RXImage(phaseX.getWidth(), phaseX.getHeight(),phaseX.getSize());
        this.phaseContrastReconstruction(phaseX, phaseY);
        return this.stack;
    }


    private static RXImage _ModalEstimation(RXImage phaseX, RXImage phaseY){
        //3,5,6,8,10
        int modulation = 10;

        double[] tmpMode = new double[modulation];
        double[] mode = new double[modulation];
        LegendrePolynomial.initConstant(1,phaseX.getWidth(),phaseX.getHeight());

        double xy = phaseX.getWidth()*phaseX.getHeight();
        double error = Double.MAX_VALUE;
        final float[] dataX = phaseX.copyData();
        final float[] dataY = phaseY.copyData();
        while( error > 0.000_000_001 ) {
            error = 0;
//            for (int i = 1; i < mode.length; i++) {
//                tmpMode[i] = 0;
//                for (int y = 0; y < phaseX.getHeight(); y++) {
//                    for (int x = 0; x < phaseX.getWidth(); x++) {
//                        double currentValX = 0;
//                        double currentValY = 0;
//                        for (int k = 0; k < mode.length; k++) {
//                            currentValX += mode[k] * LegendrePolynomial.getLegendreOrder(k).n() * LegendrePolynomial.getLegendreOrder(k).dFx(dataX[x + y * phaseX.getWidth()], dataY[x + y * phaseY.getWidth()]);
//                            currentValY += mode[k] * LegendrePolynomial.getLegendreOrder(k).n() * LegendrePolynomial.getLegendreOrder(k).dFy(dataX[x + y * phaseX.getWidth()], dataY[x + y * phaseY.getWidth()]);
//                        }
//                        if (LegendrePolynomial.getLegendreOrder(i).n() * LegendrePolynomial.getLegendreOrder(i).dFx(dataX[x + y * phaseX.getWidth()], dataY[x + y * phaseY.getWidth()]) != 0 ) {
//                            tmpMode[i] += ((dataX[x + y * phaseX.getWidth()] - currentValX) / (LegendrePolynomial.getLegendreOrder(i).n() * LegendrePolynomial.getLegendreOrder(i).dFx(dataX[x + y * phaseX.getWidth()], dataY[x + y * phaseY.getWidth()]))) / (xy * 2);
//                        }
//                        if (LegendrePolynomial.getLegendreOrder(i).n() * LegendrePolynomial.getLegendreOrder(i).dFy(dataX[x + y * phaseX.getWidth()], dataY[x + y * phaseY.getWidth()]) != 0 ) {
//                            tmpMode[i] += ((dataY[x + y * phaseX.getWidth()] - currentValY) / (LegendrePolynomial.getLegendreOrder(i).n() * LegendrePolynomial.getLegendreOrder(i).dFy(dataX[x + y * phaseX.getWidth()], dataY[x + y * phaseY.getWidth()]))) / (xy * 2);
//                        }
//                    }
//                }
//                mode[i] += tmpMode[i];
//            }

            for (int i = 1; i < mode.length; i++) {
                tmpMode[i] = 0;
                for (int y = 0; y < phaseX.getHeight(); y++) {
                    for (int x = 0; x < phaseX.getWidth(); x++) {
                        double currentValX = 0;
                        double currentValY = 0;
                        for (int k = 0; k < mode.length; k++) {
                            currentValX += mode[k] * LegendrePolynomial.getLegendreOrder(k).n() * LegendrePolynomial.getLegendreOrder(k).dFx(x,y);
                            currentValY += mode[k] * LegendrePolynomial.getLegendreOrder(k).n() * LegendrePolynomial.getLegendreOrder(k).dFy(x,y);
                        }
                        if (LegendrePolynomial.getLegendreOrder(i).n() * LegendrePolynomial.getLegendreOrder(i).dFx(x,y) != 0 ) {
                            tmpMode[i] += ((dataX[x + y * phaseX.getWidth()] - currentValX) / (LegendrePolynomial.getLegendreOrder(i).n() * LegendrePolynomial.getLegendreOrder(i).dFx(x,y))) / (xy * 2);
                        }
                        if (LegendrePolynomial.getLegendreOrder(i).n() * LegendrePolynomial.getLegendreOrder(i).dFy(x,y) != 0 ) {
                            tmpMode[i] += ((dataY[x + y * phaseX.getWidth()] - currentValY) / (LegendrePolynomial.getLegendreOrder(i).n() * LegendrePolynomial.getLegendreOrder(i).dFy(x,y))) / (xy * 2);
                        }
                    }
                }
//                tmpMode[i]*=0.1;
                mode[i] += 0.1*tmpMode[i];
            }

            for (int i = 0; i < mode.length; i++) {
                error += Math.abs(tmpMode[i]);
            }


            for ( int i = 0; i < mode.length ; i++ ) {
                mode[i] = mode[i] + 0.1*tmpMode[i];
//                System.out.println(i+" :: "+mode[i]);
            }

            error = 0;
            for (int y = 0; y < phaseX.getHeight(); y++) {
                for (int x = 0; x < phaseX.getWidth(); x++) {
                    double v = 0;
                    double h = 0;
                    for ( int i = 0; i < mode.length ; i++ ) {
                        v +=  mode[i] * LegendrePolynomial.getLegendreOrder(i).n() * LegendrePolynomial.getLegendreOrder(i).dFx(x,y);
                        h +=  mode[i] * LegendrePolynomial.getLegendreOrder(i).n() * LegendrePolynomial.getLegendreOrder(i).dFy(x,y);
                    }
                    error += Math.abs(dataX[x + y * phaseX.getWidth()]-v);
                    error += Math.abs(dataY[x + y * phaseX.getWidth()]-h);
                }
            }

            System.out.println("Error :: "+error);
        }


        for ( int i = 0; i < mode.length ; i++ ) {
            System.out.println(i+" :: "+mode[i]);
        }

        RXImage phase = new RXImage(phaseX.getWidth(),phaseX.getHeight(),1);
        float[] result = new float[dataX.length];

        for (int y = 0; y < phaseX.getHeight(); y++) {
            for (int x = 0; x < phaseX.getWidth(); x++) {
                for ( int i = 0; i < mode.length ; i++ ) {
                    result[x + y * phaseX.getWidth()] +=  mode[i] * LegendrePolynomial.getLegendreOrder(i).n() * LegendrePolynomial.getLegendreOrder(i).F(dataX[x + y * phaseX.getWidth()], dataY[x + y * phaseY.getWidth()]);
                }
            }
        }

        phase.setData(result);

        return phase;
    }

     /**
     * Southwell, W. H. « Wave-front estimation from wave-front slope measurements ».
     * Journal of the Optical Society of America 70, nᵒ 8 (1 août 1980): 998‑1006. doi:10.1364/JOSA.70.000998.
     * Zonal estimation
     * @param phaseX Phase Gradient X
     * @param phaseY Phase Gradient Y
     * @param minimalStepValue The minimal Step value between two iteration
     * @param expectedErrorPerPixel The expected error per pixel
     * @param maximumNumberOfIteration The maximal number of iteration
     * @return RXImage the computed PhaseContrast Image
     */
    private static RXImage _SouthWellJacobi(RXImage phaseX, RXImage phaseY,final double minimalStepValue,
                                            final double expectedErrorPerPixel, final int maximumNumberOfIteration){
        //Compute the
        RXImage result = new RXImage(phaseX.getWidth(),phaseX.getHeight(),phaseX.getSize());

        final float[] resultData = new float[result.getGlobalSize()];

        final float[] dataX = phaseX.copyData();
        final float[] dataY = phaseY.copyData();

        final int width = result.getWidth();
        final int height = result.getHeight();


        for ( int z = 0; z < result.getSize() ; z++ ) {
            final int posZ = z * result.getHeight() * result.getWidth();

            RXTomoJThreadFactory.createThreadLambdaComputation(() -> {
                final float[] phase = new float[result.getWidth()*result.getHeight()];
                final float[] tmp = new float[result.getWidth()*result.getHeight()];
                final float[] conv = new float[]{Float.MAX_VALUE, Float.MAX_VALUE};
                int n = 0;

                //TODO define the H value which is the spacing between 2 pixels
                //Set h
                final float h = 1f;
                //Coef to average data
                final float[] g = new float[1];
//               Mean Value of the current phase
                final float[] pa = new float[1];
//              Linear derivate
                final float[] b = new float[1];


//                final double stop = 0.00001;
//                final double stop1 = 0.00001;

                final double s = result.getHeight()*result.getWidth();
                //Use the Jacobi algorithm
                final float w = 0.9f;

                while ((conv[0] > minimalStepValue || conv[1]/s > expectedErrorPerPixel) && n < maximumNumberOfIteration) {
                    conv[0] = conv[1];
                    conv[1] = 0;

                    for (int y = 0; y < height; y++) {
                        for (int x = 0; x < width; x++) {

                            g[0] = g(x, y, width, height);
                            pa[0] = PA(x, y, 0, tmp, width, height);
                            b[0] = S(x, y,0, dataX, dataY, width, height) * h;

                            pa[0] /= g[0];
                            b[0] /= g[0];

                            conv[1] += Math.abs(pa[0] + b[0] - tmp[x + y * width]);
                            phase[x + y * width] = tmp[x + y * width] + (w * (pa[0] + b[0] - tmp[x + y * width]));
                        }
                    }

                    //Update TMP at each projection
                    System.arraycopy(phase, 0, tmp, 0, phase.length);

                    conv[0] = Math.abs(conv[0] - conv[1]);
//                    conv[0] = conv[1]/(result.getHeight()*result.getWidth());

                    if (!Float.isFinite(conv[0])) {
                        conv[0] = Float.MAX_VALUE;
                    }

//                    error[0] = conv[0];
                    System.out.println("Error :: " + conv[0] +" \\ "+ conv[1]+"\t n :: " + n);

                    n++;
                }

                System.arraycopy(tmp,0,resultData,posZ,tmp.length);
            });
        }

        RXTomoJThreadFactory.terminateLambdaThread();

//        MONO THREAD

//        //Set h
//        float h = 1f;
//        //Compute the
//        RXImage result = new RXImage(phaseX.getWidth(),phaseX.getHeight(),phaseX.getSize());
//
//        final float[] tmp = new float[result.getGlobalSize()];
////        final float[] phase = new float[result.getWidth()*result.getHeight()];
//
//        Arrays.fill(tmp,0);
//
//        final float[] dataX = phaseX.copyData();
//        final float[] dataY = phaseY.copyData();
//
//        //Coef to average data
//        final float[] g = new float[1];
////        Mean Value of the current phase
//        final float[] pa = new float[1];
////        Linear derivate
//        final float[] b = new float[1];
//
////        final float[] conv = new float[2];
////        int n ;
//        double stop = 0.00001;
//        System.out.println("Stop condition :: "+stop);
//
//        //Use the Jacobi algorithm
//        float w = 0.9f;
//
////        final int[] posZ = new int[1];
//        final int width = result.getWidth();
//        final int height = result.getHeight();

//        Date date = new Date();
//        for ( int z = 0; z < result.getSize() ; z++ ) {
//            conv[0] = Float.MAX_VALUE;
//            conv[1] = Float.MAX_VALUE;
//            n = 0;
//            posZ[0] = z * result.getHeight() * result.getWidth();
//
//
//            while (conv[0] > stop && n < 100000) {
//                conv[0] = conv[1];
//                conv[1] = 0;
//
//                for (int y = 0; y < result.getHeight(); y++) {
//                    for (int x = 0; x < result.getWidth(); x++) {
//
//                        g[0] = g(x, y, result.getWidth(), result.getHeight());
//                        pa[0] = PA(x, y, posZ[0], tmp, result.getWidth(), result.getHeight());
//                        b[0] = S(x, y,posZ[0], dataX, dataY, result.getWidth(), result.getHeight()) * h;
//
//                        pa[0] /= g[0];
//                        b[0] /= g[0];
//
//                        conv[1] += Math.abs(pa[0] + b[0] - tmp[x + y * result.getWidth()+ posZ[0]]);
//                        phase[x + y * result.getWidth()] = tmp[x + y * result.getWidth() + posZ[0]] + (w * (pa[0] + b[0] - tmp[x + y * result.getWidth() + posZ[0]]));
//                    }
//                }
//
//                //Update TMP at each projection
//                System.arraycopy(phase, 0, tmp, posZ[0], phase.length);
//
//                conv[0] = Math.abs(conv[0] - conv[1]);
//
//                if ( !Float.isFinite(conv[0]) ){
//                    conv[0] = Float.MAX_VALUE;
//                }
//
//                error[0] = conv[0];
//
////                System.out.println("Error :: " + conv[0] + "\t n :: " + n);
//
//                n++;
//            }
//        }

//        System.out.println("Time :: "+ TimeUnit.MILLISECONDS.toSeconds(new Date().getTime()-date.getTime()));
        result.setData(resultData);

        return result;
    }

    /**
     * Southwell, W. H. « Wave-front estimation from wave-front slope measurements ». Journal of the Optical Society of America 70, nᵒ 8 (1 août 1980): 998‑1006. doi:10.1364/JOSA.70.000998.
     * @param phaseX Phase Gradient X
     * @param phaseY Phase Gradient Y
     * @param minimalStepValue The minimal Step value between two iteration
     * @param expectedErrorPerPixel The expected error per pixel
     * @param maximumNumberOfIteration The maximal number of iteration
     * @return RXImage the computed PhaseContrast Image
     */
    private static RXImage _SouthWellGausian(RXImage phaseX, RXImage phaseY,final double minimalStepValue,
                                             final double expectedErrorPerPixel, final int maximumNumberOfIteration){
        RXImage result = new RXImage(phaseX.getWidth(),phaseX.getHeight(),phaseX.getSize());

        final float[] resultData = new float[result.getGlobalSize()];

        final float[] dataX = phaseX.copyData();
        final float[] dataY = phaseY.copyData();

        for ( int z = 0; z < result.getSize() ; z++ ) {
            final int posZ = z * result.getHeight() * result.getWidth();

            RXTomoJThreadFactory.createThreadLambdaComputation(() -> {
                final float[] conv = new float[]{Float.MAX_VALUE, Float.MAX_VALUE};
                final float[] phase = new float[result.getHeight()*result.getWidth()];
                final float[] tmp = new float[result.getHeight()*result.getWidth()];

                final float[] dataXtmp = new float[result.getHeight()*result.getWidth()];
                final float[] dataYtmp = new float[result.getHeight()*result.getWidth()];

//                final float w = (float) (2.0f/(1+Math.sin(Math.PI/(result.getGlobalSize()))));
                final float w = 0.9f;

                final double stop = 0.00001;
                final double stop1 = 0.0001;

                final double s = result.getHeight()*result.getWidth();

                //Coef to average data
                final float[] g = new float[1];
                //Mean Value of the current phase
                final float[] pa = new float[1];
                //Linear derivate
                final float[] b = new float[1];

                System.arraycopy(dataX,posZ,dataXtmp,0,dataXtmp.length);
                System.arraycopy(dataY,posZ,dataYtmp,0,dataYtmp.length);

                final float h = 1;

                int n = 0;

                while ((conv[0] > minimalStepValue || conv[1]/s > expectedErrorPerPixel) && n < maximumNumberOfIteration) {

                    conv[0] = conv[1];
                    conv[1] = 0;

                    //TODO what is the best
//                for (int y = 1; y < result.getHeight() - 1; y++) {
//                    for (int x = 1; x < result.getWidth() - 1; x++) {
                    for (int y = 0; y < result.getHeight(); y++) {
                        for (int x = 0; x < result.getWidth(); x++) {
                            g[0] = g(x, y, result.getWidth(), result.getHeight());
                            pa[0] = PA(x, y, 0, tmp, result.getWidth(), result.getHeight());
                            //TODO the array of this may be computed first
                            b[0] = S(x, y, 0, dataXtmp, dataYtmp, result.getWidth(), result.getHeight()) * h;
//                            b[0] = S2(x, y, 0, dataXtmp, dataYtmp, result.getWidth(), result.getHeight()) * h;

                            pa[0] /= g[0];
                            b[0] /= g[0];

                            conv[1] += Math.abs(pa[0] + b[0] - tmp[x + y * result.getWidth()]);
                            phase[x + y * result.getWidth()] = tmp[x + y * result.getWidth()] + (w * (pa[0] + b[0] - tmp[x + y * result.getWidth()]));

                            //Update TMP at each step
                            tmp[x + y * result.getWidth()] = phase[x + y * result.getWidth()];
                        }
                    }

                    conv[0] = Math.abs(conv[0] - conv[1]);
//                    conv[0] = conv[1]/(result.getHeight()*result.getWidth());

                    if (!Float.isFinite(conv[0])) {
                        conv[0] = Float.MAX_VALUE;
                    }

//                    error[0] = conv[0];
                    System.out.println("Error :: " + conv[0] +" \\ "+ conv[1]+"\t n :: " + n);

                    n++;
                }

                System.arraycopy(tmp,0,resultData,posZ,tmp.length);
            });
        }

        RXTomoJThreadFactory.terminateLambdaThread();


        //MONO THREAD

        //Set h
//        float h = 1f;
        //Compute the
//        RXImage result = new RXImage(phaseX.getWidth(),phaseX.getHeight(),phaseX.getSize());

//        final float[] resultData = new float[result.getGlobalSize()];
//        final float[] tmp = new float[result.getGlobalSize()];
//        final float[] phase = new float[result.getGlobalSize()];

//        Arrays.fill(tmp,0);

//        //Coef to average data
//        final float[] g = new float[1];
//        //Mean Value of the current phase
//        final float[] pa = new float[1];
//        //Linear derivate
//        final float[] b = new float[1];

//        float[] dataX = phaseX.copyData();
//        float[] dataY = phaseY.copyData();

//        float[] conv;
//        int n;
//        double stop = 0.00001;
//        System.out.println("Stop condition :: "+stop);

//        float w = (float) (2.0f/(1+Math.sin(Math.PI/(result.getGlobalSize()))))*0.5f;

//        int posZ;

//        for ( int z = 0; z < result.getSize() ; z++ ){
//            conv = new float[]{Float.MAX_VALUE,Float.MAX_VALUE};
//            n = 0;
//            posZ = z*result.getHeight()*result.getWidth();
//
//
//            while (conv[0] > stop && n < 100000) {
//                conv[0] = conv[1];
//                conv[1] = 0;
//
//                //TODO what is the best
////                for (int y = 1; y < result.getHeight() - 1; y++) {
////                    for (int x = 1; x < result.getWidth() - 1; x++) {
//                for (int y = 0; y < result.getHeight(); y++) {
//                    for (int x = 0; x < result.getWidth(); x++) {
//                        g[0] = g(x, y, result.getWidth(), result.getHeight());
//                        pa[0] = PA(x, y, posZ, tmp, result.getWidth(), result.getHeight());
//                        b[0] = S(x, y, posZ, dataX, dataY, result.getWidth(), result.getHeight()) * h;
//
//                        pa[0] /= g[0];
//                        b[0] /= g[0];
//
//                        conv[1] += Math.abs(pa[0] + b[0] - tmp[x + y * result.getWidth() + posZ]);
//                        phase[x + y * result.getWidth()] = tmp[x + y * result.getWidth() + posZ] + (w * (pa[0] + b[0] - tmp[x + y * result.getWidth()+ posZ]));
//
//                        //Update TMP at each step
//                        tmp[x + y * result.getWidth() + posZ] = phase[x + y * result.getWidth()];
//                    }
//                }
//
//                conv[0] = Math.abs(conv[0] - conv[1]);
//
//                if ( !Float.isFinite(conv[0]) ){
//                    conv[0] = Float.MAX_VALUE;
//                }
//
//                error[0] = conv[0];
//                System.out.println("Error :: " + conv[0] + "\t n :: " + n);
//
//                n++;
//            }
//
//        }
        result.setData(resultData);

        return result;
    }

    /**
     * Get the normalization factor for this position
     * @param x The X coordinate position
     * @param y The Y coordinate position
     * @param width The width
     * @param height The height
     * @return The normalization factor of this position
     */
    private static float g(int x, int y, int width, int height){
        float g = 0;
        if ( (x == 0 && y == 0) || (x == (width-1) && y == (height-1)) ){
            g=2;
        }
        else if ( x == 0 || y == 0 ||  x == (width-1) || y == (height-1) ){
            g=3;
        }else{
            g=4;
        }
        return g;
    }

    /**
     * Compute nearest-neighbor phase sum
     * @param x The x position
     * @param y The y position
     * @param pos The current Z position (z*width*height)
     * @param phase The phase array
     * @param width The width of the phase array
     * @param height The height of the phase array
     * @return The nearest-neighbor phase sum
     */
    private static float PA(int x, int y, int pos, float[] phase, int width, int height){
        return (px(x,y*width+pos,phase,width)+py(x,y,pos,phase,width,height));
    }

    private static float px(int x, int pos, float[] phase, int width){
        float[] val = new float[2];
        //Left Border Case
        if ( (x-1) < 0){
            //Take the inverse of the first value
            val[0] = 0;
//            val[0] = -phase[x+pos];
        }else{
            val[0] = phase[(x-1)+pos];
        }
        //Right Border Case
        if ( (x+1) >= width ){
            //Take the inverse of the last value
            val[1] = 0;
//            val[1] = -phase[x+pos];
        }else{
            val[1] = phase[(x+1)+pos];
        }

        return val[1]+val[0];
    }

    private static float py(int x, int y, int pos, float[] phase, int width, int height){
        float[] val = new float[2];
        //Top Border Case
        if ( (y-1) < 0){
            //Take the inverse of the first value
            val[0] = 0;
//            val[0] = -phase[x+y*width+pos];
        }else{
            val[0] = phase[x+(y-1)*width+pos];
        }
        //Right Border Case
        if ( (y+1) >= height){
            val[1] = 0;
//            val[1] = -phase[x+y*width+pos];
        }else{
            val[1] = phase[x+(y+1)*width+pos];
        }

        return val[1]+val[0];
    }

    /**
     * Slope measurement constant
     * @param x The x position
     * @param y The y position
     * @param pos The current Z position (z*width*height)
     * @param phaseX The X direction slope array
     * @param phaseY The Y direction slope array
     * @param width The width of the slope array
     * @param height The height of the slope array
     * @return The contante slope measure for this position
     */
    private static float S(int x, int y, int pos, float[] phaseX, float[] phaseY, int width, int height){
        return (fx(x, y * width + pos, phaseX, width)+fy(x,y,pos,phaseY,width,height))/2.0f;
    }

    /**
     * Slope measurement constant
     * @param x The x position
     * @param y The y position
     * @param pos The current Z position (z*width*height)
     * @param phaseX The X direction slope array
     * @param phaseY The Y direction slope array
     * @param width The width of the slope array
     * @param height The height of the slope array
     * @return The contante slope measure for this position
     */
    private static float S2(int x, int y, int pos, float[] phaseX, float[] phaseY, int width, int height){
        float tmp = 0;
        if ( x == 1 || x == width - 2){
            tmp = 12*(phaseX[(x-1)+y * width + pos] + phaseX[(x+1)+y * width + pos]);
//            tmp = 12*fx(x, y * width + pos, phaseX, width);
        }
        else if ( x > 1 && x < width - 2 ){
            tmp = fx2(x, y * width + pos, phaseX, width);
        }

        if( y == 1 || y == height - 2){
            tmp = 12*(phaseY[x+((y-1)*width)+pos] + phaseY[x+((y+1)*width)+pos]);
//            tmp += 12*fy(x,y,pos,phaseY,width,height);
        }
        else if ( y > 1 && y < height - 2){
            tmp += fy2(x, y, pos, phaseY, width,height);
        }
        return tmp/24;
    }

    private static float fx(int x, int pos, float[] phaseX, int width){
        float[] val = new float[2];
        //Left Border Case
        if ( (x-1) < 0){
            //Take the inverse of the first value
            val[0] += - phaseX[pos];
        }else{
            val[0] += phaseX[(x-1)+pos];
        }

//        if ( (x-2) < 0){
//            //Take the inverse of the first value
//            val[0] += - phaseX[posY];
//        }else{
//            val[0] += phaseX[(x-1)+posY];
//        }
//
//        //Right Border Case
//        if ( (x+2) >= width){
//            //Take the inverse of the last value
//            val[1] += - phaseX[width+posY-1];
//        }else{
//            val[1] += phaseX[(x+2)+posY];
//        }

        //Right Border Case
        if ( (x+1)>=width){
            //Take the inverse of the last value
            val[1] += - phaseX[width+pos-1];
        }else{
            val[1] += phaseX[(x+1)+pos];
        }

        return val[1]-val[0];
    }

    private static float fx2(int x, int pos, float[] phaseX, int width){
        float[] val = new float[2];
        //Left Border Case
        if ( (x-1) < 0){
            //Take the inverse of the first value
            val[0] += - phaseX[pos];
        }else{
            val[0] += phaseX[(x-1)+pos];
        }

//        if ( (x-2) < 0){
//            //Take the inverse of the first value
//            val[0] += - phaseX[posY];
//        }else{
//            val[0] += phaseX[(x-1)+posY];
//        }
//
//        //Right Border Case
//        if ( (x+2) >= width){
//            //Take the inverse of the last value
//            val[1] += - phaseX[width+posY-1];
//        }else{
//            val[1] += phaseX[(x+2)+posY];
//        }

        //Right Border Case
        if ( (x+1)>=width){
            //Take the inverse of the last value
            val[1] += - phaseX[width+pos-1];
        }else{
            val[1] += phaseX[(x+1)+pos];
        }

        return (11*val[1]+phaseX[(x+2)+pos])-(11*val[0]+phaseX[(x-2)+pos]);
    }


    private static float fy(int x, int y, int pos, float[] phaseY, int width, int height){
        float[] val = new float[2];
        //Top Border Case
        if ( (y-1) < 0){
            //Take the inverse of the first value
            val[0] += -phaseY[x];
        }else{
            val[0] += phaseY[x+(y-1)*width+pos];
        }

//        if ( (y-2) < 0){
//            Take the inverse of the first value
//            val[0] += -phaseY[x];
//        }else{
//            val[0] += phaseY[x+(y-2)*width];
//        }

        //Right Border Case
        if ( (y+1)>=height){
            val[1] += -phaseY[x+(height-1)*width+pos];
        }else{
            val[1] += phaseY[x+(y+1)*width+pos];
        }

//        if ( (y+2)>=height){
//            val[1] += -phaseY[x+(height-1)*width];
//        }else{
//            val[1] += phaseY[x+(y+2)*width];
//        }

        return val[1]-val[0];
    }

    private static float fy2(int x, int y, int pos, float[] phaseY, int width, int height){
        float[] val = new float[2];
        //Top Border Case
        if ( (y-1) < 0){
            //Take the inverse of the first value
            val[0] += -phaseY[x];
        }else{
            val[0] += phaseY[x+(y-1)*width+pos];
        }

//        if ( (y-2) < 0){
//            Take the inverse of the first value
//            val[0] += -phaseY[x];
//        }else{
//            val[0] += phaseY[x+(y-2)*width];
//        }

        //Right Border Case
        if ( (y+1)>=height){
            val[1] += -phaseY[x+(height-1)*width+pos];
        }else{
            val[1] += phaseY[x+(y+1)*width+pos];
        }

//        if ( (y+2)>=height){
//            val[1] += -phaseY[x+(height-1)*width];
//        }else{
//            val[1] += phaseY[x+(y+2)*width];
//        }

        return (11*val[1]+phaseY[x+(y+2)*width+pos])-(11*val[0]+phaseY[x+(y-2)*width+pos]);
    }

    /**
     * Test call to phaseContrastReconstruction(FFT) private function
     * NOT IMPLEMENTED
     * @param phaseX the reconstructed gradient in the X direction
     * @param phaseY the reconstructed gradient in the Y direction
     * @return The resulting image.
     */
    public RXImage phaseContrastReconstructionIterativeFFT(RXImage phaseX, RXImage phaseY) {
        float[] currentDataX = phaseX.copyData();
        float[] currentDataY = phaseY.copyData();

        final float[] dataX = ComputationUtils.padDataWithZeroCenter(phaseX, phaseX.getWidth() * 2, phaseX.getHeight() * 2);
        final float[] dataY = ComputationUtils.padDataWithZeroCenter(phaseY, phaseY.getWidth() * 2, phaseY.getHeight() * 2);

        RXImage newPhaseX = new RXImage(phaseX.getWidth() * 2, phaseX.getHeight() * 2, phaseX.getSize());
        RXImage newPhaseY = new RXImage(phaseX.getWidth() * 2, phaseX.getHeight() * 2, phaseX.getSize());

        RXImage result = new RXImage();
        newPhaseX.setData(dataX);
        newPhaseY.setData(dataY);


        for ( int i = 0 ; i < 100 ; i++) {
//            result = pri(newPhaseX, newPhaseY);
            result = phaseContrastReconstruction(newPhaseX,newPhaseY);

            //Get the linear Gradient of the resulting stack in X and Y direction
            result.computeData(data -> {
                for (int y = 0; y < newPhaseX.getHeight(); y++) {
                    for (int x = 1; x < newPhaseX.getWidth() - 1; x++) {
                        dataX[x + y * newPhaseX.getWidth()] = data[x + 1 + y * newPhaseX.getWidth()] - data[x - 1 + y * newPhaseX.getWidth()];
                    }
                }
            });

            //Get the linear Gradient of the resulting stack in X and Y direction
            result.computeData(data -> {
                for (int x = 0; x < newPhaseX.getWidth(); x++) {
                    for (int y = 1; y < newPhaseX.getHeight() - 1; y++) {
                        dataY[x + y * newPhaseY.getWidth()] = data[x + (y + 1) * newPhaseX.getWidth()] - data[x + (y - 1) * newPhaseX.getWidth()];
                    }
                }
            });

            double error = 0;


//      Compute Error
            for (int y = 0; y < phaseX.getHeight(); y++) {
                for (int x = 0; x < phaseX.getWidth(); x++) {
                    error += Math.pow(currentDataX[x + y * phaseX.getWidth()] - (dataX[(x + newPhaseX.getWidth() / 4) + (y + newPhaseX.getHeight() / 4) * newPhaseX.getWidth()]), 2);
                }
            }
            System.out.println("Error : " + error + " Iteration : "+i);
//            RXUtils.RXImageToImagePlus(newPhaseY).show();

            //Replace the Center of the Gradient with the real one
            for (int y = 0; y < phaseX.getHeight(); y++) {
                for (int x = 0; x < phaseX.getWidth(); x++) {
                    dataX[(x + newPhaseX.getWidth() / 4) + (y + newPhaseX.getHeight() / 4) * newPhaseX.getWidth()] = currentDataX[x + y * phaseX.getWidth()];
                    dataY[(x + newPhaseX.getWidth() / 4) + (y + newPhaseX.getHeight() / 4) * newPhaseY.getWidth()] = currentDataY[x + y * phaseX.getWidth()];
                }
            }

//            RXUtils.RXImageToImagePlus(newPhaseY).show();
        }

        return result;

    }

    private RXImage pri(RXImage phaseX, RXImage phaseY){
        DenseFloatMatrix2D phaseXD = new DenseFloatMatrix2D(phaseX.getHeight(),phaseX.getWidth());
        DenseFloatMatrix2D phaseYD = new DenseFloatMatrix2D(phaseY.getHeight(),phaseY.getWidth());
//        DenseFloatMatrix2D p = new DenseFloatMatrix2D(1,1);
//        p.getFft2();
//        p.fft2();

//        DenseFComplexMatrix2D denseFComplexMatrix2D = new DenseFComplexMatrix2D(phaseX.getHeight(), phaseX.getWidth());
        float u, v;
        float re, im;
        int row,col;
        RXImage result = new RXImage(phaseX.getWidth(), phaseX.getHeight(),1);


        //Warning here FFT cannot take 1D arguments
        if ( phaseXD.rows() > 1 && phaseXD.columns() > 1) {
//            float[] pixel  = new float[this.stack.getGlobalSize()];
            for (int z = 0; z < phaseX.getSize(); z++) {

                float[] phaseXPixels = phaseX.copySlice(z);
                float[] phaseYPixels = phaseY.copySlice(z);

//                float[] phaseXPixels = (float[]) phaseX.getPixels(z + 1);
//                float[] phaseYPixels = (float[]) phaseY.getPixels(z + 1);

                for (row = 0; row < phaseXD.rows(); row++) {
                    for (col = 0; col < phaseXD.columns(); col++) {
                        re = phaseXPixels[col + row * phaseXD.columns()];
                        im = phaseYPixels[col + row * phaseXD.columns()];
                        //Warning here value cannot be NAN
                        if ( !Float.isFinite(re) ){
                            re = 0;
//                            System.err.print(true);
                        }

                        if ( !Float.isFinite(im) ){
                            im = 0;
                        }
                        phaseXD.setQuick(row, col, re );
                        phaseYD.setQuick(row, col, im );
                    }
                }

                DenseFComplexMatrix2D dX = phaseXD.getFft2();
                DenseFComplexMatrix2D dY = phaseYD.getFft2();
//
                for (row = 0; row < dX.rows(); row++) {
                    for (col = 0; col < dX.columns(); col++) {
                        float[] xVal = dX.getQuick(row, col);
                        float[] yVal = dY.getQuick(row, col);

                        v = row;
                        u = col;

//                        if (row > dX.rows() / 2) {
//                            v -= dX.rows();
//                        }
//                        if (col > dX.columns() / 2) {
//                            u -= dX.columns();
//                        }
////
                        if (u == 0 && v == 0) {
                            dX.setQuick(row, col, 0, 0);
                        } else {
//                            v /= (float) (dX.rows()) / 2.0f;
//                            u /= (float) (dX.columns()) / 2.0f;

                            float nVal = (xVal[0]*v + yVal[0]*u)/ (v * v + u * u);

                            dX.setQuick(row, col, nVal, 0);
                        }
                    }
                }

                dX.ifft2(true);

                System.err.println("Warning Data is Cropped here");
                result.setSlice((float[]) dX.getRealPart().elements(), z);
            }
        }

        return result;
    }

    /**
     * Test call to phaseContrastReconstruction(FFT) private function
     * NOT IMPLEMENTED
     * @param phaseX the reconstructed gradient in the X direction
     * @param phaseY the reconstructed gradient in the Y direction
     * @return The resulting image.
     */
    public RXImage phaseContrastReconstructionT(RXImage phaseX, RXImage phaseY, int height, int width) {
        this.stack = new RXImage(width,height,phaseX.getSize());
        this.phaseContrastReconstruction(phaseX,phaseY);
        return this.stack;

    }

    /**
     * Phase reconstruction first implementation, integration in the Fourier Space.
     * @see <a href="http://www.opticsinfobase.org/oe/abstract.cfm?uri=oe-15-3-1175">C. Kottler, C. David, F. Pfeiffer, and O. Bunk, "A two-directional approach for grating based differential phase contrast imaging using hard x-rays," Opt. Express 15, 1175-1181 (2007) </a>
     *
     * @param phaseX the reconstructed gradient in the X direction
     * @param phaseY the reconstructed gradient in the Y direction
     */
    private static RXImage phaseContrastReconstruction(RXImage phaseX, RXImage phaseY) {
        DenseFComplexMatrix2D denseFComplexMatrix2D = new DenseFComplexMatrix2D(phaseX.getHeight(), phaseX.getWidth());
        float u, v;
        float re, im;
        int row,col;

        RXImage result = new RXImage(phaseX.getWidth(),phaseX.getHeight(),phaseX.getSize());
        //Warning here FFT cannot take 1D arguments
        if ( denseFComplexMatrix2D.rows() > 1 && denseFComplexMatrix2D.columns() > 1) {
//            float[] pixel  = new float[this.stack.getGlobalSize()];
            for (int z = 0; z < phaseX.getSize(); z++) {

                float[] phaseXPixels = phaseX.copySlice(z);
                float[] phaseYPixels = phaseY.copySlice(z);

//                float[] phaseXPixels = (float[]) phaseX.getPixels(z + 1);
//                float[] phaseYPixels = (float[]) phaseY.getPixels(z + 1);

                for (row = 0; row < denseFComplexMatrix2D.rows(); row++) {
                    for (col = 0; col < denseFComplexMatrix2D.columns(); col++) {
                        re = phaseXPixels[col + row * denseFComplexMatrix2D.columns()];
                        im = phaseYPixels[col + row * denseFComplexMatrix2D.columns()];
                        //Warning here value cannot be NAN
                        if ( !Float.isFinite(re) ){
                            re = 0;
//                            System.err.print(true);
                        }

                        if ( !Float.isFinite(im) ){
                            im = 0;
                        }
                        denseFComplexMatrix2D.setQuick(row, col, re, im);
                    }
                }

                denseFComplexMatrix2D.fft2();

                for (row = 0; row < denseFComplexMatrix2D.rows(); row++) {
                    for (col = 0; col < denseFComplexMatrix2D.columns(); col++) {
                        float[] value = denseFComplexMatrix2D.getQuick(row, col);
                        float[] new_value = new float[2];

                        v = row;
                        u = col;

                        if (row > denseFComplexMatrix2D.rows() / 2) {
                            v -= denseFComplexMatrix2D.rows();
                        }
                        if (col > denseFComplexMatrix2D.columns() / 2) {
                            u -= denseFComplexMatrix2D.columns();
                        }

                        if (u == 0 && v == 0) {
                            denseFComplexMatrix2D.setQuick(row, col, new_value);
                        } else {
                            v /= (float) (denseFComplexMatrix2D.rows()) / 2.0f;
                            u /= (float) (denseFComplexMatrix2D.columns()) / 2.0f;

                            new_value[0] = (value[0] * v - value[1] * u) / (((v * v + u * u) * -1) * (float) (2.0d * Math.PI));
                            new_value[1] = (value[0] * u + value[1] * v) / (((v * v + u * u) * -1) * (float) (2.0d * Math.PI));


                            denseFComplexMatrix2D.setQuick(row, col, new_value);
                        }
                    }
                }

                denseFComplexMatrix2D.ifft2(true);
                FloatMatrix2D floatMatrix2D = denseFComplexMatrix2D.getRealPart();
                FloatMatrix2D floatMatrix2D1 = denseFComplexMatrix2D.getImaginaryPart();

                result.setSlice((float[]) floatMatrix2D.elements(), z);
            }
        }

        return result;
    }

    /**
     * Phase reconstruction first implementation, integration in the Fourier Space.
     * @see <a href="http://www.opticsinfobase.org/oe/abstract.cfm?uri=oe-15-3-1175">C. Kottler, C. David, F. Pfeiffer, and O. Bunk, "A two-directional approach for grating based differential phase contrast imaging using hard x-rays," Opt. Express 15, 1175-1181 (2007) </a>
     *
     * @param phaseX the reconstructed gradient in the X direction
     * @param phaseY the reconstructed gradient in the Y direction
     */
    private static RXImage phaseContrastReconstruction1D(RXImage phaseX, RXImage phaseY) {
//        DenseFComplexMatrix1D denseFComplexMatrix1D = new DenseFComplexMatrix1D(phaseX.getWidth());
        float u, v;
        float re, im;
        int row,col;

        RXImage result = new RXImage(phaseX.getWidth(),phaseX.getHeight(),phaseX.getSize());
        //Warning here FFT cannot take 1D arguments

        for (int z = 0; z < phaseX.getSize(); z++) {

            float[] phaseXPixels = phaseX.copySlice(z);
            float[] phaseYPixels = phaseY.copySlice(z);

            DenseFloatMatrix1D real = new DenseFloatMatrix1D(phaseX.getWidth());
            real.assign(phaseXPixels);

            DenseFloatMatrix1D img = new DenseFloatMatrix1D(phaseX.getWidth());
            img.assign(phaseYPixels);

            DenseFComplexMatrix1D denseFComplexMatrix1D = new DenseFComplexMatrix1D(phaseX.getWidth());
            denseFComplexMatrix1D.assignReal(real);
            denseFComplexMatrix1D.assignImaginary(img);

            denseFComplexMatrix1D.fft();

            for (col = 0; col < denseFComplexMatrix1D.size(); col++) {
                float[] value = denseFComplexMatrix1D.getQuick(col);
                float[] new_value = new float[2];

                u = col;

                if (col > denseFComplexMatrix1D.size() / 2) {
                    u -= denseFComplexMatrix1D.size();
                }

                if (u == 0 ) {
                    denseFComplexMatrix1D.setQuick( col, new_value);
                } else {
//                            v /= (float) (denseFComplexMatrix2D.rows()) / 2.0f;
                    u /= (float) (denseFComplexMatrix1D.size()) / 2.0f;

                    new_value[0] = ( - value[1] * u) / (((u * u) * -1) * (float) (2.0d * Math.PI));
                    new_value[1] = (value[0] * u) / (((u * u) * -1) * (float) (2.0d * Math.PI));
//                    float tmp_val1 = (float) (-(1.0d/2.0d*Math.PI) * Math.signum(u) * value[0]);
//                    float tmp_val2 = (float) (-(1.0d/2.0d*Math.PI) * Math.signum(u) * value[1]);
//                    new_value[0] = -tmp_val2;
//                    new_value[1] = tmp_val1;

                    denseFComplexMatrix1D.setQuick(col, new_value);
                }
            }

            denseFComplexMatrix1D.ifft(true);

            result.setSlice((float[]) denseFComplexMatrix1D.getRealPart().elements(), z);
        }

        return result;
    }

    /**
     * Normalize by the mean of each ROW
     * @param image The gradient image to normalize
     */
    private void xNormalization(RXImage image){
        image.computeData(data->{
            final float[] meanX = new float[1];
            final int[] pos = new int[1];
            for ( int z = 0 ; z < image.getSize() ; z++ ) {
                for (int y = 0; y < image.getHeight(); y++) {
                    meanX[0] = 0;
                    pos[0] = y*image.getWidth()+z*image.getWidth()*image.getHeight();
//                    pos[1] = y+z*imageStackGradientX.getHeight();
                    for (int x = 0; x < image.getWidth(); x++) {
                        meanX[0] += data[x+pos[0]];
                    }

                    meanX[0]/=image.getWidth();

                    for (int x = 0; x < image.getWidth(); x++) {
                        data[x+pos[0]] -= meanX[0];
                    }
                }
            }
        });
    }

    /**
     * Normalize by the mean of each Col (change now apply on each row also )
     * Because normalization here do avoid beam drift (background may be removed with latter computation)
     * @param image The gradient image to normalize
     */
    private void yNormalization(RXImage image){
        image.computeData(data->{
            final float[] meanX = new float[1];
            final int[] pos = new int[1];
            for ( int z = 0 ; z < image.getSize() ; z++ ) {
                for (int y = 0; y < image.getHeight(); y++) {
                    meanX[0] = 0;
                    pos[0] = y*image.getWidth()+z*image.getWidth()*image.getHeight();
//                    pos[1] = y+z*imageStackGradientX.getHeight();
                    for (int x = 0; x < image.getWidth(); x++) {
                        meanX[0] += data[x+pos[0]];
                    }

                    meanX[0]/=image.getWidth();

                    for (int x = 0; x < image.getWidth(); x++) {
                        data[x+pos[0]] -= meanX[0];
                    }
                }
            }
        });

//        image.computeData(data->{
//            final float[] meanY = new float[1];
//            final int[] pos = new int[1];
//            for ( int z = 0 ; z < image.getSize() ; z++ ) {
//                for (int x = 0; x < image.getWidth(); x++) {
//                    meanY[0] = 0;
//                    pos[0] = x+z*image.getWidth()*image.getHeight();
//
//                    for (int y = 0; y < image.getHeight(); y++) {
//                        meanY[0] += data[y*image.getWidth()+pos[0]];
//                    }
//
//                    meanY[0]/=image.getHeight();
//
//                    for (int y = 0; y < image.getHeight(); y++) {
//                        data[y*image.getWidth()+pos[0]] -= meanY[0];
//                    }
//                }
//            }
//        });
    }

    /**
     * Normalize by the spot center
     * @param xGrave The gradient image to normalize
     */
    private void defaultNormalizationX(RXImage xGrave){
        xGrave.computeData(data->{
            for( int i = 0 ; i < data.length ; i++ ){
                data[i] -= spotCenterX;
            }
        });
    }

    /**
     * Normalize by the spot center
     * @param yGrave The gradient image to normalize
     */
    private void defaultNormalizationY(RXImage yGrave){
        yGrave.computeData(data->{
            for( int i = 0 ; i < data.length ; i++ ){
                data[i] += spotCenterY;
            }
        });
    }

    @Override
    protected ArrayList<PixelType> getPixelType() {
        ArrayList<PixelType> p = new ArrayList<>();
        p.add(PixelType.SPOT);
        return p;
    }

    @Override
    protected int[][] getComputationRoi() {
        //no default range
        return null;
    }

    @Override
    public void initialize(int dims_length, int stack_width, int stack_height, int stack_size, boolean initAll) {
        super.initialize(dims_length, stack_width, stack_height, stack_size, initAll);

        //Use the whole Spot
//        Arrays.fill(this.spotMap, 1);

        PhaseContrastInfo phaseContrastInfo = RXTomoJ.getInstance().getModelController().getDataController().getPhaseContrastInfo();

        real = phaseContrastInfo.getReal();
        imaginary = phaseContrastInfo.getImaginary();

        phaseComputationTechniques = phaseContrastInfo.getPhaseComputationTechniques();
        phaseRetrievalAlgo = phaseContrastInfo.getPhaseRetrievalAlgo();

        useMirroring = phaseContrastInfo.isUseMirroring();

        doXnormalization = phaseContrastInfo.isNormalizeX();
        doYnormalization = phaseContrastInfo.isNormalizeY();


//        minimalStepValue[0] = phaseContrastInfo.getMinimalStepValue();
//        expectedErrorPerPixel[0] = phaseContrastInfo.getExpectedErrorPerPixel();
//        maximumNumberOfIteration[0] = phaseContrastInfo.getMaximumNumberOfIteration();

        this.imageStackGradientX = new RXImage(stack_width, stack_height, stack_size);
        this.imageStackGradientY = new RXImage(stack_width, stack_height, stack_size);

        this.array = new float[stack_size*2][stack_height*stack_width];

//        Arrays.fill(this.spotMap,1);

    }

    @Override
    public void compute(float[] data, int width, int height, int size, int position, int slice) {
        int i,j,k,z;

        float[] gradientX = this.array[2*slice];
        float[] gradientY = this.array[(2*slice)+1];

//        System.err.println("Warning HotSpot Filter is disabled "+this.getClass().toGenericString());
//        this.computeSpot.run3DHotSpotFilter(data,spotMap,size);
        if ( reductionInfo.isDoFilter() ) {
            hotSpotFilter(data, spotMap, width, height, size, reductionInfo.getNumberOfIteration(),
                    reductionInfo.getSpreadCoefficient(), reductionInfo.getRadius(), false, getPixelType());
        }

        int positionC = 0;
        int secondCounter = 0;

        float[] centerX = new float[size];
        float[] centerY = new float[size];

        phaseComputationTechniques.phaseComputation(data, width, height, size, centerX, centerY, spotCenterX, spotCenterY);

        for (i = 0; i < size; i++) {

            //Sampling loop
            for (j = i * sampling; j < (i + 1) * (sampling); j++) {
                for (k = 0; k < sampling; k++) {
                    //If the position is contained in the tmp_data ( special case for height inferior to sampling )
                    if (position + j + (size * sampling) * k < gradientX.length) {
                        gradientX[position + j + positionC + (this.stack.getWidth() * sampling) * k] = centerX[i];
                        gradientY[position + j + positionC + (this.stack.getWidth() * sampling) * k] = centerY[i];
                    }
                }
            }

            secondCounter++;
            //Test second counter in case of multi line processing
            if ((secondCounter * sampling) >= this.stack.getWidth() - 1) {
                secondCounter = 0;
                positionC += this.stack.getWidth() * (sampling - 1);
            }

        }

    }


    @Override
    public void flush() {
        for ( int i = 0 ; i < imageStackGradientX.getSize() ; i++ ){
            this.imageStackGradientX.setSlice(this.array[2*i],i);
            this.imageStackGradientY.setSlice(this.array[(2*i)+1],i);
        }
        array = null;
    }

    @Override
    public void finalization() {

        //Normalize
        if ( doXnormalization && imageStackGradientX.getWidth() > 1 ){
            xNormalization(imageStackGradientX);
        }else{
            //Normalize by the spot Center
            defaultNormalizationX(imageStackGradientX);
        }

        if ( doYnormalization && imageStackGradientY.getHeight() > 1 ){
            yNormalization(imageStackGradientY);
        }else{
            //Normalize by the spot center
            defaultNormalizationY(imageStackGradientY);
        }

        //Regrid data
        imageStackGradientX = RXTomoJ.getInstance().getModelController().getComputationController().regrid(imageStackGradientX,this.typeOfStack);
        imageStackGradientY = RXTomoJ.getInstance().getModelController().getComputationController().regrid(imageStackGradientY,this.typeOfStack);
    }

    @Override
    public void setMemoryNeeded(long size) {
        neededMemory[0] = size*Float.SIZE*2;
    }


    @Override
    public String getFunctionName() {
        return "Phase contrast";
    }

    @Override
    public float getErrorValue() {
        return error[0];
    }


    public RXImage getImageStackGradientX() {
        return imageStackGradientX;
    }

    public RXImage getImageStackGradientY() {
        return imageStackGradientY;
    }
}
