/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.computationFunction;


import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.PixelType;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : antoine bergamaschi
 *         Date: 24/11/13
 *         Time: 18:17
 */
public class ComputeAbsorption extends BasicComputationFunction {

    public ComputeAbsorption(){
        super();
        this.typeOfStack = StackType.ABSORPTION;
    }

    /**
     * Basic Function, get the  roiSpot and return the asked image.
     * @param data The raw data as returned from hdf5 file
     * @param width The width of the raw spot image (retrieved in hdf5 metadata)
     * @param height The height of the raw spot image (retrieved in hdf5 metadata)
     * @param size The size of the raw spot image which is equal to a dimension (size or height of the desired image )(retrieved in hdf5 metadata)
     * @param position The current y  position in the desired image
     * @param slice The Slice current position
     */
    @Override
    public void compute(float[] data, int width, int height, int size, int position, int slice) {
        float[] tmp_data = array[slice];


        //Iteration var
        int x, k, j, z, y, wh = width * height,positionC=0,secondCounter=0;
        float tmp_sumPixel;

        if ( reductionInfo.isDoFilter() ) {
            hotSpotFilter(data, spotMap, width, height, size, reductionInfo.getNumberOfIteration(),
                    reductionInfo.getSpreadCoefficient(), reductionInfo.getRadius(), false, getPixelType());
        }
        for (z = 0; z < size; z++) {
            tmp_sumPixel = 0;
            for ( x = 0 ; x < width ; x++ ){
                for ( y = 0 ; y < height; y++){
//                    if (  testPixelType(x,y) ){
                        tmp_sumPixel += data[x+y*width+z*wh];
//                    }
                }
            }

            //Function to fill scan map depending on the Sampling
            //Warning here as data is considered as 1D there could be edge problems
            for ( j = z*sampling ; j < (z+1)*(sampling) ; j++ ) {
                //Warning here data are overwritted on each level
                for ( k = 0 ; k < sampling ; k++) {
                    //If the position is contained in the tmp_data ( special case for height inferior to sampling )
                    if ( position + j + positionC + (this.stack.getWidth()) * k < tmp_data.length ) {
                        tmp_data[position + j + positionC + (this.stack.getWidth()) * k] = tmp_sumPixel;
                    }
                }
            }

            secondCounter++;
            //Test second counter in case of multi line processing
            if ( (secondCounter*sampling) >= this.stack.getWidth()-1  ){
                secondCounter = 0;
                positionC += this.stack.getWidth()*(sampling-1);
            }

        }
    }

    //TODO Add a function to fill the scan map with the sampling parameters
//    protected void fillMap(float[] data, int position){
//        int k;
//        float[] tmp_data=new float[1];
//        for( int i = 0 ; i < data.length ; i++ ){
//            if ( sampling_X > 1 ){
//                for ( k = 0 ; k < sampling_X ; k++) {
//                    if ( position + i + (this.stack.getWidth()) * k < tmp_data.length ) {
//                        tmp_data[position + i + (this.stack.getWidth()) * k] = data[i];
//                    }
//                }
//            }
//            if ( sampling_Y > 1){
//
//            }
//            if ( sampling_Z > 1){
//
//            }
//        }
//    }

    @Override
    protected ArrayList<PixelType> getPixelType() {
        ArrayList<PixelType> p = new ArrayList<>();
        p.add(PixelType.SPOT);
        return p;
    }

    @Override
    protected int[][] getComputationRoi() {
        //Return Void no default value
        return null;
    }

    @Override
    public void initialize(int dims_length, int stack_width, int stack_height, int stack_size, boolean initAll) {
        super.initialize(dims_length, stack_width, stack_height, stack_size, initAll);
    }

    @Override
    public String getFunctionName() {
        return "Absorption";
    }

    @Override
    public float getErrorValue() {
        return -1;
    }
}
