/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.computationFunction;

import com.soleil.nanoscopium.hdf5Opener.stream.Hdf5VirtualStream;
import com.soleil.nanoscopium.hdf5Opener.stream.StreamObject;
import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.RXSpectrum;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.model.fluorescence.FluorescenceElement;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import com.soleil.nanoscopium.rxtomoj.model.utils.RXTomoJThreadFactory;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * Created by bergamaschi on 10/02/14.
 */
public class ComputeSpectrum extends BasicComputationFunction {

    private ArrayList<FluorescenceElement> elements = new ArrayList<>();
    private ArrayList<RXImage> elementImages = new ArrayList<>();

    private RXSpectrum spectrum = new RXSpectrum();
    float[] sumSpectra;
    float[][][] fluoByElement;
    float[][] roiByElement;


    private boolean isSumSpectra = false;

    public ComputeSpectrum(){
        super();
        this.typeOfStack = ComputationUtils.StackType.FLUORESCENCE;
    }

    @Override
    protected ArrayList<ComputationUtils.PixelType> getPixelType() {
        ArrayList<ComputationUtils.PixelType> p = new ArrayList<>();
        p.add(ComputationUtils.PixelType.ALL);
        return p;
    }

    @Override
    protected int[][] getComputationRoi() {
        //No default range
        return null;
    }

    @Override
    public void compute(final float[] data, int width, int height, int size, int position, int slice) {
        if (isSumSpectra){
            createSumSpectra(data,width,size);
        }else{
            createFluorescenceImage(data,width,height,size,position,slice);
        }
    }

//    @Override
//    protected void computeSpectrumData(float[] data, int width, int size, int position, int slice) {
//        computeSpectrumData(data, width, size, position, slice);
//    }

    /**
     * Create a Fluorescence Map for a distinct elements
     * @param data the input 1D array
     * @param widthOfData the width of the image  (data)
     * @param size the size of the image (data)
     */
    private void createSumSpectra(float[] data, int widthOfData, int size) {
        int z, x, index=0;
        int[] zw = new int[1];
        for (z = 0; z < size; z++) {
            zw[0] = z * widthOfData;
            for (x = 0; x < widthOfData; x++) {
                sumSpectra[x] += data[x+zw[0]];
            }
        }
    }

    /**
     * Create a Fluorescence Map for a distinct elements
     * @param data the input 1D array
     * @param widthOfData the width of the image  (data)
     * @param size the size of the image (data)
     * @param position the start position in the data
     * @param fluo the 1D array containing the sum of every fluorescence elements
     * @param fluoByElement the 2D array containing one 1D array for each element
     */
    private void createFluorescenceImage(float[] data, int widthOfData, int size, int position, float[] fluo, float[][] fluoByElement) {
        int z, x, index=0;
        //Do only for one element
//        FluorescenceElement element = elements.get(0);

        for (z = 0; z < size; z++) {
            int zw = z * widthOfData;
            index = 0;
            for ( FluorescenceElement element : elements ) {
                for (x = (int) (zw + element.getRoiPosition()[0]); x < zw + element.getRoiPosition()[1]; x++) {
                    fluo[position + z] += data[x];
                    fluoByElement[index][position + z] += data[x];
                }
                index++;
            }
        }
    }

    /**
     * Create a Fluorescence Map for a distinct elements
     * @param data The input 1D array
     * @param width The width of the image  (data)
     * @param height The width of the image  (data)
     * @param size The size of the image (data)
     * @param slice The Slice current position
     * @param position the start position in the data
     */
    private void createFluorescenceImage(final float[] data, int width, int height, int size, int position, int slice) {
        int z, x, index=0;
        for (z = 0; z < size; z++) {
            int zw = z * width;
            //Basic Summation
            for ( index = 0 ; index < roiByElement.length ; index++ ) {
                for (x = (int) (zw + roiByElement[index][0]); x < zw + roiByElement[index][1]; x++) {
                    fluoByElement[slice][index][position + z] += data[x];
                }
            }
        }
    }


    public ArrayList<FluorescenceElement> getElements() {
        return elements;
    }

    public void setElements(ArrayList<FluorescenceElement> elements) {
        this.elements = elements;
    }


    public ArrayList<RXImage> getElementImages() {
        return elementImages;
    }

    public void setElementImages(ArrayList<RXImage> elementImages) {
        this.elementImages = elementImages;
    }

    @Override
    public void initialize(int dims_length, int stack_width, int stack_heigth, int stack_size, boolean initAll) {
        super.initialize(dims_length,stack_width,stack_heigth,stack_size, initAll);

        if ( isSumSpectra ){
            this.sumSpectra = new float[(int) spectrum_size];
            spectrum.setDims(new int[]{(int) spectrum_size, 1});
            spectrum.setData(sumSpectra);
        }else{
            //Remove the array parameters has its unused by the fluorescence Computation function
            this.array = null;
            this.spotMap = null;
            this.stack = null;

    //      Initialize the elments based on the currently saved fluorescence Elements
            elements = new ArrayList<>(customAnalyse.getParameterStorage().getFluorescenceInfo().getElements());
            elementImages = new ArrayList<>();

            roiByElement = new float[elements.size()][2];

            int index = 0;
            for ( FluorescenceElement element : elements ){
                elementImages.add(new RXImage(stack_width, stack_heigth, stack_size));
    //            for (int k = 0; k < element.getRoiPosition().length; k++ ) {
                    System.arraycopy(element.getRoiPosition(), 0, roiByElement[index], 0, roiByElement[index].length);
    //            }
                index++;
            }

            fluoByElement = new float[stack_size][elementImages.size()][stack_width*stack_heigth];
        }
    }

    @Override
    public void flush() {
        if ( isSumSpectra ){
            //Spectrum data are directly computed...
//            spectrum.computeData(data->{
//                for ( int i = 0 ; i < data.length ; i++ ){
//                    data[i] += sumSpectra[i];
//                }
//            });
        }else {
            int index;
            for (int z = 0; z < this.elementImages.get(0).getSize(); z++) {
                index = 0;
                for (RXImage imageStack : elementImages) {
                    imageStack.setSlice(Arrays.copyOf(fluoByElement[z][index], fluoByElement[z][index].length), z);
                    index++;
                }
            }
        }
    }

    @Override
    public void finalization() {
        if ( !isSumSpectra ) {
//            if (withShift) {
                int index = 0;
                //Set the current ShiftImage to use the Fluorescence Value
//                this.shiftImage.useInterpolation_fluorescence();
                for (RXImage imageStack : elementImages) {
                    elementImages.set(index,RXTomoJ.getInstance().getModelController().getComputationController().regrid(imageStack,this.typeOfStack));
//                    elementImages.set(index, shiftImage(imageStack));
                    index++;
                }
//            }
        }
    }

    @Override
    public int getDatasetDimension(long[] dimension) {
        //Spectrum are retrieved here
        return dimension.length+1;
    }

    @Override
    public String getFunctionName() {
        return "Fluorescence";
    }

    @Override
    public float getErrorValue() {
        return -1;
    }


    public void setIsSumSpectra(boolean isSumSpectra) {
        this.isSumSpectra = isSumSpectra;
    }

    public boolean isSumSpectra(){
        return isSumSpectra;
    }

    public RXSpectrum getSpectrum() {
        return spectrum;
    }
}
