/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.reconstructionFunction;

import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.util.RXUtils;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.model.SessionObject;
import com.soleil.nanoscopium.rxtomoj.model.data.ReconstructionInfo;
import com.soleil.nanoscopium.rxtomoj.model.filters.ProjectionFilter;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import com.soleil.nanoscopium.rxtomoj.model.utils.DirectRayTracing;
import com.soleil.nanoscopium.rxtomoj.model.utils.ProgressFunction;
import com.soleil.nanoscopium.rxtomoj.model.utils.ProgressFunctionListener;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

/**
 * Created by Bergamaschi on 01/04/2014.
 * TODO set a way to lock the uses of this class during a computation process
 * TODO Add Sampling support by increasing the size of the reconstructed Stack
 */
public abstract class BasicReconstructionFunction implements ProgressFunction {

    //TODO Only for projectionMethod purpose
    public enum ProjectionMethod {
        DEFAULT_RAYTRACING, RAYTRACING, DEFAULT;

        /**
         * Compute the projection of the volume array given in parameter,
         * and the given theta angle
         * @param volume The array from which the projection is computed
         * @param theta The angle of the projection ( in euler formulation )
         * @param measuredProjection The measured projection if any ( or null )
         * @param error
         * @param relaxationCoef The relaxation coefficient to apply if measuredProjection is not null
         * @param projectionWidth
         * @param projectionHeight
         * @param centerOfRotationX
         * @param reconstructionWidth
         * @param reconstructionSize
         * @param reconstructionCenterOfRotationX
         * @param reconstructionCenterOfRotationZ
         * @param sampling1
         * @param sampling2
         * @return The projection of a plane with the angle theta of the volume array
         */
        public float[] projection(final float[] volume, double theta,final float[] measuredProjection,
                                  final double[] error, float relaxationCoef,
                                  int projectionWidth, int projectionHeight, double centerOfRotationX,
                                  int reconstructionWidth, int reconstructionHeight, int reconstructionSize,
                                  double reconstructionCenterOfRotationX, double reconstructionCenterOfRotationZ,
                                  double sampling1, double sampling2){

            boolean doSoustraction = false;
            if ( measuredProjection != null ){
                doSoustraction = true;
            }

            switch (this){
                case DEFAULT:
                    return BasicReconstructionFunction.projection(volume,theta,measuredProjection,
                                error,relaxationCoef,doSoustraction,
                                projectionWidth,projectionHeight,centerOfRotationX,
                                reconstructionWidth,reconstructionHeight, reconstructionSize,
                                reconstructionCenterOfRotationX,reconstructionCenterOfRotationZ,
                                sampling1,sampling2);
                case RAYTRACING:
                    return BasicReconstructionFunction._projection3(volume, theta, measuredProjection,
                                error, relaxationCoef, doSoustraction,
                                projectionWidth, projectionHeight, centerOfRotationX,
                                reconstructionWidth, reconstructionHeight, reconstructionSize,
                                reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ);
                case DEFAULT_RAYTRACING:
                    return BasicReconstructionFunction._projection4(volume, theta, measuredProjection,
                                error, relaxationCoef, doSoustraction,
                                projectionWidth, projectionHeight, centerOfRotationX,
                                reconstructionWidth, reconstructionHeight, reconstructionSize,
                                reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ,
                                sampling1);
            }
            return null;
        }


        /**
         * Compute the backprojection of the given projection inside a volume array,
         * with the given angle of rotation theta ( in euler formulation )
         * @param projection The projection to backproject in the volume
         * @param theta The angle with which the projection is backprojected
         * @param volume The filled volume with the projection
         * @param mapCounts The volume containing the number of time each pixel have been updated.
         * @param reconstructionWidth
         * @param reconstructionHeight
         * @param reconstructionSize
         * @param reconstructionCenterOfRotationX
         * @param reconstructionCenterOfRotationZ
         * @param projectionWidth
         * @param projectionHeight
         * @param centerOfRotationX
         * @param sampling1
         * @param sampling2
         */
        public void backprojection(final float[] projection, double theta,
                                   final float[] volume, final float[] mapCounts,
                                   int reconstructionWidth, int reconstructionHeight, int reconstructionSize,
                                   double reconstructionCenterOfRotationX, double reconstructionCenterOfRotationZ,
                                   int projectionWidth, int projectionHeight,
                                   double centerOfRotationX,
                                   double sampling1, double sampling2){
            switch (this){
                case DEFAULT_RAYTRACING:
                    _backProjection(projection, theta,
                            volume, mapCounts,
                            reconstructionWidth, reconstructionHeight, reconstructionSize,
                            reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ,
                            projectionWidth, projectionHeight,
                            centerOfRotationX,
                            sampling1);
                    break;
                case RAYTRACING:
                    _backProjection3(projection, theta,
                            volume, mapCounts,
                            reconstructionWidth, reconstructionHeight, reconstructionSize,
                            reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ,
                            projectionWidth, projectionHeight,
                            centerOfRotationX);
                    break;
                case DEFAULT:
                    _backProjection4(projection, theta,
                            volume, mapCounts,
                            reconstructionWidth, reconstructionHeight, reconstructionSize,
                            reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ,
                            projectionWidth, projectionHeight,
                            centerOfRotationX,
                            sampling1);
                    break;
            }
        }
    }


    private enum Projection{
        RAYTRACING,DEFAULT;

        /**
         *
         * @param yPosition
         * @param maxLenght
         * @param reconstruction
         * @param countingMap
         * @param projectionPosition
         * @param theta
         * @param sampling
         * @param reconstructionWidth
         * @param reconstructionHeight
         * @param reconstructionSize
         * @param initPosition
         * @param doFirstAddition
         * @return
         */
        public double projection(int yPosition,double maxLenght, float[] reconstruction, float[] countingMap,
                                 int projectionPosition, double theta, double sampling,
                                 int reconstructionWidth, int reconstructionHeight, int reconstructionSize,
                                 double[] initPosition, boolean doFirstAddition){
            switch (this){
                case RAYTRACING:
                    return BasicReconstructionFunction._projectionSum3(yPosition, maxLenght, reconstruction, countingMap,
                            projectionPosition, theta,
                            reconstructionWidth, reconstructionHeight, reconstructionSize,
                            initPosition, doFirstAddition);
                case DEFAULT:
                    return BasicReconstructionFunction._projectionSum(yPosition, maxLenght, reconstruction, countingMap,
                            projectionPosition, theta, sampling,
                            reconstructionWidth, reconstructionHeight, reconstructionSize,
                            initPosition, doFirstAddition);
            }
            return 0;
        }

        /**
         *
         * @param yPosition
         * @param maxLenght
         * @param value
         * @param reconstruction
         * @param countingMap
         * @param theta
         * @param sampling
         * @param reconstructionWidth
         * @param reconstructionHeight
         * @param reconstructionSize
         * @param initPosition
         * @param doFirst
         */
        public void backprojection(int yPosition, double[] maxLenght, double value, float[] reconstruction, float[] countingMap,
                                   double theta, double sampling,
                                   int reconstructionWidth, int reconstructionHeight, int reconstructionSize,
                                   double[] initPosition, boolean doFirst){
            switch (this){
                case RAYTRACING:
                    BasicReconstructionFunction._backProjectSum3(yPosition, maxLenght, value,
                            reconstruction, countingMap,
                            theta,
                            reconstructionWidth, reconstructionHeight, reconstructionSize,
                            initPosition,doFirst);
                    break;
                case DEFAULT:
                    BasicReconstructionFunction._backProjectSum(yPosition, maxLenght, value,
                            reconstruction, countingMap,
                            theta, sampling,
                            reconstructionWidth, reconstructionHeight, reconstructionSize,
                            initPosition, doFirst);
                    break;
            }
        }
    }

    protected ArrayList<ProgressFunctionListener> progressFunctionListeners = new ArrayList<>();

    protected double totalCovorageAngle = 360.0d;

    protected float centerOfRotationX = 0.0f;
    protected float centerOfRotationY = 0.0f;
    protected float centerOfRotationZ = 0.0f;

    protected int reconstructionWidth  =  0;
    protected int reconstructionHeight  = 0;
    protected int reconstructionSize  = 0;

    protected int projectionWidth = 0;
    protected int projectionHeight = 0;
    protected int projectionSize = 0;

    protected ProjectionFilter projectionFilter = new ProjectionFilter();


    protected int[] projectionROI = new int[6];

    private double reconstructionCenterOfRotationX=0.0d,reconstructionCenterOfRotationY=0.0d,reconstructionCenterOfRotationZ=0.0d;
//    private float y,x;

    //Position in the numberOfWorkToDo
    protected float workCompleted = 0.0f;

    //Number of Work to be completed
    protected float numberOfWorks = 1.0f;

    protected boolean closeProcess = false;

//    protected float error_counter = 0;

    protected double numberOfLine = 3;
    protected double numberOfStepByPixel = 3;

    protected ProjectionMethod projectionMethod = ProjectionMethod.RAYTRACING;

    protected double[] thetaProjection;

    protected BasicReconstructionFunction(){
        ReconstructionInfo reconstructionInfo = RXTomoJ.getInstance().getModelController().getDataController().getReconstructionInfo();

        this.numberOfLine = reconstructionInfo.getNumberOfLine();
        this.centerOfRotationX = (float) reconstructionInfo.getCenterOfRotationX();
        this.projectionMethod = reconstructionInfo.getProjectionMethod();
    }

    protected void copy(BasicReconstructionFunction basic){
        ReconstructionInfo reconstructionInfo = RXTomoJ.getInstance().getModelController().getDataController().getReconstructionInfo();

        this.numberOfLine = reconstructionInfo.getNumberOfLine();
        this.centerOfRotationX = (float) reconstructionInfo.getCenterOfRotationX();
//        this.numberOfStepByPixel = reconstructionInfo.getNumberOfStepByPixel();
        this.projectionMethod = reconstructionInfo.getProjectionMethod();

        this.centerOfRotationY = basic.centerOfRotationY;
        this.centerOfRotationZ = basic.centerOfRotationZ;
        this.projectionFilter = new ProjectionFilter(basic.getProjectionFilter());

    }

    /**
     * Basic retro projection
     * Data should be passed with the projections in the Z directions
     * @param imageStack the stackImage containing each projection to reconstruct
     * @return RXImage the resulting reconstruction
     */
    public float[] retroProjection(RXImage imageStack){
        final float[] results = new float[reconstructionHeight * reconstructionWidth * reconstructionSize];

        //Debug control
        Date date = new Date();

//        RXImage imageStackResults = new RXImage(reconstructionWidth,reconstructionHeight,reconstructionSize);
//        imageStackResults.setData(results);


        final float[] mapCount = new float[results.length];
        int i = 0;
        for ( int k = projectionROI[4] ; k < projectionROI[5] ; k++ ){
            backProjection(imageStack.copySlice(k), thetaProjection[i], results, mapCount);
            workCompleted++;

            fireProgressFunction();
            i++;
//            System.out.println(k+" :: Cumulate Reconstruction Time :: " + (new Date().getTime() - date.getTime())/1000.0d);
        }
        ComputationUtils.arrayDivide(results, mapCount);


//        workCompleted = numberOfWorks;
        System.out.println(" Reconstruction Time :: " + TimeUnit.MILLISECONDS.toSeconds(new Date().getTime() - date.getTime()));

        //Debug Display Weighting
        RXImage rxImage = new RXImage(reconstructionWidth,reconstructionHeight,reconstructionSize);
        rxImage.setData(mapCount);
        RXUtils.RXImageToImagePlus(rxImage).show();

        return results;
    }

    /**
     * Compute the Back propagation for the given projection and the reconstruction table.
     * Multi Threaded in the Y order
     * @param projection 1D/2D array containing a projection data
     * @param theta the angle of rotation around the rotation axis ( 1 axis rotation only ) here Y
     * @param reconstruction  2D/3D array containing reconstruction data
     * @param countingMap 2D/3D array containing an integers corresponding to the number of time the pixel has been passed thought
     */
    public void backProjection(float[] projection, double theta,final float[] reconstruction,final float[] countingMap) {
        projectionMethod.backprojection(projection, theta, reconstruction, countingMap,
                reconstructionWidth, reconstructionHeight, reconstructionSize,
                reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ,
                projectionWidth, projectionHeight,
                centerOfRotationX,
                numberOfLine, numberOfStepByPixel);
    }

    /**
     *
     * @param reconstruction the currently reconstructed array
     * @param theta The angle of rotation
     * @param realProjection the real projection data
     * @param relaxationCoef the relaxation coefficient
     * @return float[] the projection for this angle theta.
     */
    public float[] projection(float[] reconstruction, double theta, float[] realProjection, double[] error,float relaxationCoef){
        return projectionMethod.projection(reconstruction,theta,realProjection,error,relaxationCoef,
                projectionWidth,projectionHeight,
                centerOfRotationX,
                reconstructionWidth,reconstructionHeight,reconstructionSize,
                reconstructionCenterOfRotationX,reconstructionCenterOfRotationZ,
                numberOfLine,numberOfStepByPixel);
    }

    /**
     * Compute the Back propagation for the given projection and the reconstruction table.
     * Multi Threaded in the Z order
     * @param projection 1D array containing a projection data
     * @param theta the angle of rotation around the rotation axis ( 1 axis rotation only )
     * @param reconstruction  1D array containing reconstruction data
     * @param countingMap 1D array containing an integers corresponding to the number of time the pixel has been passed thought
     */
    @Deprecated
    private void backProjection2D_old(float[] projection, double theta,final float[] reconstruction,final float[] countingMap) {
        //Theta is set as perpendicular projection
        double[] angle = new double[3];
        angle[1] = -theta;

        //Construct the rotation matrix
        double[][] rotationMatrix = ComputationUtils.computeRotationMatrix(angle);

        //MULTI THREAD (THREAD SAFE IMPLEMENTATION)
        IntStream.range(0, reconstructionSize).parallel().forEach(p -> {
            double r, value;
            double x, y, z;
            //Normally should trace for X/Y vector a Z line through the volume
            for (z = p; z < (p + 1); z += 1) {
                //For every x/y in the reconstruction
                for (y = 0; y < reconstructionHeight; y += 1) {
                    for (x = 0; x < reconstructionWidth; x += 1) {

                        double[] pos = ComputationUtils.computeRotatedCoordinate(x - reconstructionCenterOfRotationX, y - reconstructionCenterOfRotationY, z - reconstructionCenterOfRotationZ, rotationMatrix);

                        pos[0] += centerOfRotationX;
                        pos[1] += centerOfRotationY;

                        if (pos[0] >= projectionROI[0] && pos[0] < projectionROI[1] &&
                                pos[1] >= projectionROI[2] && pos[1] < projectionROI[3]) {
                            //Linear extrapolation
//                            value = projection[((int)pos[0] + (int)pos[1] * projectionWidth)];
//                            value = projection[((int) (pos[0] + pos[1] * projectionWidth))];
                            value = ComputationUtils.getBilinearInterpolationValue(pos[0], pos[1], projectionWidth, projection);
//                            if ( !Double.isFinite(value) || value2 != value ){
//                                value = ComputationUtils.getBilinearInterpolationValue(pos[0],pos[1],projectionWidth,projection);
//                            }

                            //Eliminate the external Pixel ( Sup to the limite of geometrical reconstruction )
                            if (!Double.isNaN(value)) {
                                reconstruction[(int) (x + (y * reconstructionWidth) + (z * reconstructionWidth * reconstructionHeight))] += (float) value;
                                countingMap[(int) (x + (y * reconstructionWidth) + (z * reconstructionWidth * reconstructionHeight))] += 1;
                            }
                        }
                    }
                }
            }
        });

//        ComputationUtils.arrayDivide(reconstruction, countingMap);
    }





    /**
     * Compute the Back propagation for the given projection and the reconstruction table.
     * Multi Threaded in the Z order
     * @param projection 1D array containing a projection data
     * @param theta the angle of rotation around the rotation axis ( 1 axis rotation only )
     * @param reconstruction  1D array containing reconstruction data
     * @param countingMap 1D array containing an integers corresponding to the number of time the pixel has been passed thought
     */
    private void _backProjection2(float[] projection, double theta,final float[] reconstruction,final float[] countingMap) {
        //Theta is set as perpendicular projection
        double[] angle = {0.0d,theta,0.0d};

        //Construct the rotation matrix
        double[][] rotationMatrix = ComputationUtils.computeRotationMatrix(angle);

        //MULTI THREAD (THREAD SAFE IMPLEMENTATION)
        IntStream.range(0, reconstructionHeight).parallel().forEach(p -> {
            double r, value;
            double x, y, z;
            //Normally should trace for X/Y vector a Z line through the volume
            for (y = p; y < (p + 1); y++) {
                //For every x/y in the reconstruction
                for (z = 0; z < reconstructionSize; z++) {
                    for (x = 0; x < reconstructionWidth; x++) {
//                        currentDistance[0] = Math.sqrt(x*x+z*z);
                        //Compute start position
                        double[] pos = ComputationUtils.computeRotatedCoordinate(x - reconstructionCenterOfRotationX, y - reconstructionCenterOfRotationY, z - reconstructionCenterOfRotationZ, rotationMatrix);

                        pos[0] += centerOfRotationX;
                        pos[1] += centerOfRotationX;

                        if (pos[0] >= projectionROI[0] && pos[0] < projectionROI[1] &&
                                pos[1] >= projectionROI[2] && pos[1] < projectionROI[3]) {
                            //Linear extrapolation
//                            value = projection[((int)pos[0] + (int)pos[1] * projectionWidth)];
//                            value = projection[((int) (pos[0] + pos[1] * projectionWidth))];
                            value = ComputationUtils.getBilinearInterpolationValue(pos[0], pos[1], projectionWidth, projection);
//                            if ( !Double.isFinite(value) || value2 != value ){
//                                value = ComputationUtils.getBilinearInterpolationValue(pos[0],pos[1],projectionWidth,projection);
//                            }

                            //Eliminate the external Pixel ( Sup to the limite of geometrical reconstruction )
                            if (!Double.isNaN(value)) {
                                double weight = ComputationUtils.computeRayLenght(pos[0] - reconstructionCenterOfRotationX, pos[1] - reconstructionCenterOfRotationZ, theta + (Math.PI / 2.0d), -reconstructionCenterOfRotationX, -reconstructionCenterOfRotationZ, reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ);
                                reconstruction[(int) (x + (y * reconstructionWidth) + (z * reconstructionWidth * reconstructionHeight))] += (float) value / weight;
                                countingMap[(int) (x + (y * reconstructionWidth) + (z * reconstructionWidth * reconstructionHeight))] += 1;
                            }
                        }
                    }
                }
            }
        });

//        ComputationUtils.arrayDivide(reconstruction, countingMap);
    }

    /**
     * Compute the Back propagation for the given projection and the reconstruction table.
     * Multi Threaded in the Z order
     * @param projection 1D array containing a projection data
     * @param theta the angle of rotation around the rotation axis ( 1 axis rotation only )
     * @param reconstruction  1D array containing reconstruction data
     * @param countingMap 1D array containing an integers corresponding to the number of time the pixel has been passed thought
     */
    @Deprecated
    private void _backProjection(final float[] projection, double theta, final float[] reconstruction, final float[] countingMap) {
        final double cosTheta = ComputationUtils.cosR(theta, ComputationUtils.DEFAULT_ROUDING);
        final double sinTheta = -ComputationUtils.sinR(theta, ComputationUtils.DEFAULT_ROUDING);
        final double step = Math.sqrt(cosTheta * cosTheta + sinTheta * sinTheta)/(numberOfLine);
//        final double step = 1.0d/(numberOfLine);
        final float [] weightF = new float[projectionWidth*projectionHeight];

//        for( int y = 0 ; y < projectionHeight ; y++ ) {
//            for (int x = 0; x < projectionWidth; x++) {
////                double weight = ComputationUtils.computeRayLenghtInCircle(x, y, theta + Math.PI / 2, projectionWidth/2.0d, projectionHeight/2.0d, projectionWidth/2.0d);
//                double weight = ComputationUtils.computeRayLenght(0, 0, theta + Math.PI / 2, -reconstructionCenterOfRotationX, -reconstructionCenterOfRotationZ, reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ);
//
//
//                if ( weight == 0 ){
//                    weightF[x + y * projectionWidth] = Float.MAX_VALUE;
//                }else{
//                    weightF[x + y * projectionWidth] = (float) weight;
//                }
//            }
//        }

        //For each slice ( parallelle tomography case)
//        for ( int z = 0 ; z < projectionHeight ; z++ ){
        IntStream.range(0,projectionHeight).parallel().forEach(z -> {
            final double[] currentDistanceFromCenter = {0};
            final int currentYposition = z * reconstructionWidth;

            final double[] nextPosition = new double[2];
//
            nextPosition[0] = cosTheta * (step / 2);
            nextPosition[1] = sinTheta * (step / 2);
//
            int i = 0;

            //Retrieve the center of rotation in the reconstruction
            while (currentDistanceFromCenter[0] < projectionWidth) {
                int positionX1 = (int) (currentDistanceFromCenter[0]);

                //Compute start position
                double xStart = cosTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationX);
                double zStart = sinTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationZ);

//                double weight = 1;
                double weight = ComputationUtils.computeRayLenght(xStart - reconstructionCenterOfRotationX, zStart - reconstructionCenterOfRotationZ, theta + (Math.PI / 2.0d), -reconstructionCenterOfRotationX, -reconstructionCenterOfRotationZ, reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ);
//                double weight = weightF[positionX1 + z * projectionWidth];
//                if (weight < 1) {
//                    weight = 1;
//                }
                double[] sum = new double[1];

//                weight *= numberOfStepByPixel*numberOfLine;
//                double weight = 1;
//                double weight = numberOfLine*numberOfStepByPixel;
                if (xStart > 0 && zStart > 0 && xStart < reconstructionWidth && zStart < reconstructionSize) {
//                    backProjectSum(xStart, zStart, currentYposition, nextPosition, weight/2, theta, projection[positionX1 + z * projectionWidth]/weight, reconstruction, counts, true);
//                    backProjectSum(xStart, zStart, currentYposition, nextPosition, weight/2, theta + Math.PI, projection[positionX1 + z * projectionWidth]/weight, reconstruction, counts, false);
//                    reconstruction[(int)xStart + currentYposition + ((int)zStart)*reconstructionWidth*reconstructionHeight] += 1;
//                    backProjectSum(xStart, zStart, currentYposition, nextPosition, weight, theta, projection[positionX1 + z * projectionWidth] / (numberOfStepByPixel), reconstruction, countingMap, true);
//                    backProjectSum(xStart, zStart, currentYposition, nextPosition, weight, theta + Math.PI, projection[positionX1 + z * projectionWidth] / (numberOfStepByPixel), reconstruction, countingMap, false);
                    backProjectSum(xStart, zStart, currentYposition, nextPosition, sum, theta, projection[positionX1 + z * projectionWidth] / (weight * numberOfStepByPixel * numberOfLine), reconstruction, countingMap, true);
                    backProjectSum(xStart, zStart, currentYposition, nextPosition, sum, theta + Math.PI,  projection[positionX1 + z * projectionWidth] / (weight * numberOfStepByPixel * numberOfLine), reconstruction, countingMap, false);
                }


                i++;
                //Compute the next position
                currentDistanceFromCenter[0] = i * step;
            }
        });
    }

    private static void _backProjection(final float[] projection, double theta,
                                        final float[] reconstruction, final float[] countingMap,
                                        int reconstructionWidth, int reconstructionHeight, int reconstructionSize,
                                        double reconstructionCenterOfRotationX, double reconstructionCenterOfRotationZ,
                                        int projectionWidth, int projectionHeight,
                                        double centerOfRotationX,
                                        double sampling1) {
        final double cosTheta = Math.cos(theta);
        final double sinTheta = -Math.sin(theta);

//        final double cosTheta = ComputationUtils.cosR(theta, ComputationUtils.DEFAULT_ROUDING);
//        final double sinTheta = -ComputationUtils.sinR(theta, ComputationUtils.DEFAULT_ROUDING);
        final double step = Math.sqrt(cosTheta * cosTheta + sinTheta * sinTheta)/(sampling1);
        final double fullstep = Math.sqrt(cosTheta * cosTheta + sinTheta * sinTheta);
//        final double step = 1.0d/(numberOfLine);
        final float [] weightF = new float[projectionWidth*projectionHeight];

//        for( int y = 0 ; y < projectionHeight ; y++ ) {
//            for (int x = 0; x < projectionWidth; x++) {
////                double weight = ComputationUtils.computeRayLenghtInCircle(x, y, theta + Math.PI / 2, projectionWidth/2.0d, projectionHeight/2.0d, projectionWidth/2.0d);
//                double weight = ComputationUtils.computeRayLenght(0, 0, theta + Math.PI / 2, -reconstructionCenterOfRotationX, -reconstructionCenterOfRotationZ, reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ);
//
//
//                if ( weight == 0 ){
//                    weightF[x + y * projectionWidth] = Float.MAX_VALUE;
//                }else{
//                    weightF[x + y * projectionWidth] = (float) weight;
//                }
//            }
//        }

        //For each slice ( parallelle tomography case)
//        for ( int z = 0 ; z < projectionHeight ; z++ ){
        IntStream.range(0,projectionHeight).parallel().forEach(z -> {
            final double[] currentDistanceFromCenter = {0};
            final int currentYposition = z * reconstructionWidth;
            final double[] weight = new double[1];
            final double[] position = new double[2];

            final double[] nextPosition = new double[2];
//
            nextPosition[0] = cosTheta * (step / 2);
            nextPosition[1] = sinTheta * (step / 2);
//
            int i = 0;

            //Retrieve the center of rotation in the reconstruction
            while (currentDistanceFromCenter[0] < projectionWidth-1) {
                int positionX1 = (int) (currentDistanceFromCenter[0]);
                for ( int h = 0; h < sampling1 ; h++ ) {


                    //Compute start position
                    position[0] = cosTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationX);
                    position[1] = sinTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationZ);

                    //Compute the dimension of the orthogonal line
                    weight[0] = ComputationUtils.computeRayLenght(position[0] - centerOfRotationX, position[1] - centerOfRotationX, theta + (Math.PI / 2.0d), -reconstructionCenterOfRotationX, -reconstructionCenterOfRotationZ, reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ)/2;
//                    weight[0] = 1;
//
                    double[] sum = new double[1];

                    if (position[0] >= 0 && position[1] >= 0 && position[0] < reconstructionWidth && position[1] < reconstructionSize) {
//                    backProjectSum(xStart, zStart, currentYposition, nextPosition, weight/2, theta, projection[positionX1 + z * projectionWidth]/weight, reconstruction, counts, true);
//                    backProjectSum(xStart, zStart, currentYposition, nextPosition, weight/2, theta + Math.PI, projection[positionX1 + z * projectionWidth]/weight, reconstruction, counts, false);
//                        reconstruction[(int)position[0] + currentYposition + ((int)position[1])*reconstructionWidth*reconstructionHeight] += 1;
//                    backProjectSum(xStart, zStart, currentYposition, nextPosition, weight, theta, projection[positionX1 + z * projectionWidth] / (numberOfStepByPixel), reconstruction, countingMap, true);
//                    backProjectSum(xStart, zStart, currentYposition, nextPosition, weight, theta + Math.PI, projection[positionX1 + z * projectionWidth] / (numberOfStepByPixel), reconstruction, countingMap, false);
//                    backProjectSum(xStart, zStart, currentYposition, nextPosition, sum, theta, projection[positionX1 + z * projectionWidth] / (weight * numberOfStepByPixel * numberOfLine), reconstruction, countingMap, true);
//                    backProjectSum(xStart, zStart, currentYposition, nextPosition, sum, theta + Math.PI,  projection[positionX1 + z * projectionWidth] / (weight * numberOfStepByPixel * numberOfLine), reconstruction, countingMap, false);

                        Projection.RAYTRACING.backprojection(currentYposition, weight, projection[positionX1 + z * projectionWidth],
                                reconstruction, countingMap,
                                theta + Math.PI / 2, -1.0d,
                                reconstructionWidth, reconstructionHeight, reconstructionSize,
                                position, true);

                        Projection.RAYTRACING.backprojection(currentYposition, weight, projection[positionX1 + z * projectionWidth],
                                reconstruction, countingMap,
                                theta + 3 * Math.PI / 2, -1.0d,
                                reconstructionWidth, reconstructionHeight, reconstructionSize,
                                position, true);


//                            Projection.RAYTRACING.backprojection(currentYposition, weight, 1,
//                                    reconstruction, countingMap,
//                                    theta + Math.PI / 2, -1.0d,
//                                    reconstructionWidth, reconstructionHeight, reconstructionSize,
//                                    position, true);

//                        Projection.RAYTRACING.backprojection(currentYposition, weight, 1,
//                                reconstruction, countingMap,
//                                theta + 3 * Math.PI / 2, -1.0d,
//                                reconstructionWidth, reconstructionHeight, reconstructionSize,
//                                position, false);


                    }
                    currentDistanceFromCenter[0]+=step;

                }
                i++;
                //Compute the next position
                currentDistanceFromCenter[0] = i * fullstep;
            }
        });
    }

    /**
     * Compute the Back propagation for the given projection and the reconstruction table.
     * Multi Threaded in the Z order
     * @param projection 1D array containing a projection data
     * @param theta the angle of rotation around the rotation axis ( 1 axis rotation only )
     * @param reconstruction  1D array containing reconstruction data
     * @param countingMap 1D array containing an integers corresponding to the number of time the pixel has been passed thought
     */
    @Deprecated
    private void _backProjection3(final float[] projection, double theta, final float[] reconstruction, final float[] countingMap) {
        final double cosTheta = ComputationUtils.cosR(theta, ComputationUtils.DEFAULT_ROUDING);
        final double sinTheta = -ComputationUtils.sinR(theta, ComputationUtils.DEFAULT_ROUDING);
        final double step = Math.sqrt(cosTheta * cosTheta + sinTheta * sinTheta);

        //For each slice ( parallelle tomography case)
        IntStream.range(0,projectionHeight).parallel().forEach(z -> {
            final double[] currentDistanceFromCenter = {0};
            final int currentYposition = z * reconstructionWidth;

            final double[] nextPosition = new double[2];
//
            nextPosition[0] = cosTheta * (step / 2);
            nextPosition[1] = sinTheta * (step / 2);
//
//            int i = 0;

            //Compute start position
            double xStart = cosTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationX);
            double zStart = sinTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationZ);

            DirectRayTracing directRayTracing = new DirectRayTracing();

            directRayTracing.init(theta, new double[]{xStart, zStart});


            double[] pos = directRayTracing.getCurrentPosition();
            int[] ints = directRayTracing.getIntegerPosition();
            double[] stepSize = directRayTracing.getComputedStep();
            final double[] valueToSpread = new double[1];
            final double[] maxLenght = new double[1];

            //Retrieve the center of rotation in the reconstruction
            while (currentDistanceFromCenter[0] < projectionWidth) {
                int positionX1 = (int) (currentDistanceFromCenter[0]);

                maxLenght[0] = ComputationUtils.computeRayLenght(ints[0] - reconstructionCenterOfRotationX, ints[1] - reconstructionCenterOfRotationZ, theta + (Math.PI / 2.0d), -reconstructionCenterOfRotationX, -reconstructionCenterOfRotationZ, reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ);

                if (ints[0] >= 0 && ints[1] >= 0 && ints[0] < reconstructionWidth && ints[1] < reconstructionSize) {
                    if (stepSize[0] > step) {
                        stepSize[0] = step;
                    }
                    valueToSpread[0] = projection[positionX1 + z * projectionWidth] * (stepSize[0] / step);
                    backProjectSum(ints[0], ints[1], currentYposition, nextPosition, maxLenght, theta, valueToSpread[0], reconstruction, countingMap, false);
                    backProjectSum(ints[0], ints[1], currentYposition, nextPosition, maxLenght, theta + Math.PI, valueToSpread[0], reconstruction, countingMap, false);
//                    backProjectSum(pos[0], pos[1], currentYposition, nextPosition, maxLenght, theta, projection[positionX1 + z * projectionWidth], reconstruction, countingMap, false);
//                    backProjectSum(pos[0], pos[1], currentYposition, nextPosition, maxLenght, theta + Math.PI,  projection[positionX1 + z * projectionWidth], reconstruction, countingMap, false);
//                    reconstruction[ints[0] + currentYposition + ints[1]*reconstructionWidth*reconstructionHeight] += stepSize[0];
                }


                //Compute the next position
                currentDistanceFromCenter[0] += stepSize[0];
                directRayTracing.getNextPosition();
            }
        });
    }


    /**
     * Ray Tracing back-projection function.
     * @param projection
     * @param theta
     * @param reconstruction
     * @param countingMap
     * @param reconstructionWidth
     * @param reconstructionHeight
     * @param reconstructionSize
     * @param reconstructionCenterOfRotationX
     * @param reconstructionCenterOfRotationZ
     * @param projectionWidth
     * @param projectionHeight
     * @param centerOfRotationX
     */
    private static void _backProjection3(final float[] projection, double theta,
                                         final float[] reconstruction, final float[] countingMap,
                                         int reconstructionWidth, int reconstructionHeight, int reconstructionSize,
                                         double reconstructionCenterOfRotationX, double reconstructionCenterOfRotationZ,
                                         int projectionWidth, int projectionHeight,
                                         double centerOfRotationX) {

        final double cosTheta = Math.cos(theta);
        final double sinTheta = -Math.sin(theta);

        //For each slice ( parallelle tomography case)
        IntStream.range(0,projectionHeight).parallel().forEach(z -> {
            final double[] currentDistanceFromCenter = {0};
            final int currentYposition = z * reconstructionWidth;

            //Compute start position
            double xStart = cosTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationX);
            double zStart = sinTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationZ);

            DirectRayTracing directRayTracing = new DirectRayTracing();

//            directRayTracing.init(theta, new double[]{xStart, zStart});
            directRayTracing.init(theta, new double[]{ComputationUtils.round(xStart,100), ComputationUtils.round(zStart,100)});

            final double step = directRayTracing.getComputedStep()[0];
            directRayTracing.setMaximalDistance(step);
            double[] pos = directRayTracing.getCurrentPosition();
            int[] ints = directRayTracing.getIntegerPosition();
            double[] stepSize = directRayTracing.getComputedStep();
            final double[] valueToSpread = new double[1];
            final double[] maxLenght = new double[1];


            double remainingFrac = 0;
            double testy = 0;

            //Retrieve the center of rotation in the reconstruction
            while (currentDistanceFromCenter[0] < projectionWidth) {
                //Convert the X/Y position to projection position
                int positionX1 = (int) (currentDistanceFromCenter[0]);

                maxLenght[0] = ComputationUtils.computeRayLenght(ints[0] - reconstructionCenterOfRotationX, ints[1] - reconstructionCenterOfRotationZ, theta + (Math.PI / 2.0d), -reconstructionCenterOfRotationX, -reconstructionCenterOfRotationZ, reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ);

                //If the integer position are withing the reconstruction limits
//                if (ints[0] >= 0 && ints[1] >= 0 && ints[0] < reconstructionWidth && ints[1] < reconstructionSize) {
//                    //The value to back-project
////                    valueToSpread[0] = (projection[positionX1 + z * projectionWidth]/maxLenght[0]) * (stepSize[0]);
//                    valueToSpread[0] = (projection[positionX1 + z * projectionWidth]) * (stepSize[0]);
//
//
//                    Projection.RAYTRACING.backprojection(currentYposition, maxLenght, valueToSpread[0],
//                            reconstruction, countingMap,
//                            theta + Math.PI / 2, -1.0d,
//                            reconstructionWidth, reconstructionHeight, reconstructionSize,
//                            pos, true);
//
//                    Projection.RAYTRACING.backprojection(currentYposition, maxLenght, valueToSpread[0],
//                            reconstruction, countingMap,
//                            theta + (3 * Math.PI / 2), -1.0d,
//                            reconstructionWidth, reconstructionHeight, reconstructionSize,
//                            pos, false);
//                }


                double factor = ComputationUtils.round(stepSize[0],10000);
                testy += factor;
//              step
                int decalage = 0;
                //While case to check position in the projection width
                //If the step is still in the same pixel
                if (testy < 1) {
                    //Add the full summation
                    valueToSpread[0] += projection[positionX1 + z * projectionWidth]*factor;
                    //increment the remaining fraction
                    remainingFrac = testy;
                } else {
                    while ( testy >= 1 && positionX1 + decalage < projectionWidth) {
                        valueToSpread[0] += projection[positionX1 + decalage + z * projectionWidth]*(1 - remainingFrac);

                        Projection.RAYTRACING.backprojection(currentYposition, maxLenght, valueToSpread[0],
                                reconstruction, countingMap,
                                theta + Math.PI / 2, -1.0d,
                                reconstructionWidth, reconstructionHeight, reconstructionSize,
                                pos, false);

                        Projection.RAYTRACING.backprojection(currentYposition, maxLenght, valueToSpread[0],
                                reconstruction, countingMap,
                                theta + (3 * Math.PI / 2), -1.0d,
                                reconstructionWidth, reconstructionHeight, reconstructionSize,
                                pos, false);

                        valueToSpread[0] = 0;
                        testy -= 1;
                        decalage++;
                        if ( testy >= 1 ){
                            remainingFrac = 0;
                        }else{
                            remainingFrac = testy;
                        }
                    }
                    if (positionX1 + decalage < projectionWidth) {
                        valueToSpread[0] += projection[positionX1 + decalage + z * projectionWidth]*remainingFrac;
                    }
                }

                //Compute the next position
                currentDistanceFromCenter[0] += factor;
                directRayTracing.getNextPosition();
            }
        });
    }


    /**
     * Compute the Back propagation for the given projection and the reconstruction table.
     * Multi Threaded in the Z order
     * @param projection 1D array containing a projection data
     * @param theta the angle of rotation around the rotation axis ( 1 axis rotation only )
     * @param reconstruction  1D array containing reconstruction data
     * @param countingMap 1D array containing an integers corresponding to the number of time the pixel has been passed thought
     */
    @Deprecated
    private void _backProjection4(final float[] projection, double theta, final float[] reconstruction, final float[] countingMap) {
        final double cosTheta = ComputationUtils.cosR(theta, ComputationUtils.DEFAULT_ROUDING);
        final double sinTheta = -ComputationUtils.sinR(theta, ComputationUtils.DEFAULT_ROUDING);
        final double step = 1/numberOfLine;

        //For each slice ( parallelle tomography case)
        IntStream.range(0,projectionHeight).parallel().forEach(z -> {
            final double[] currentDistanceFromCenter = {0};
            final int currentYposition = z * reconstructionWidth;

            final double[] nextPosition = new double[2];
//
            nextPosition[0] = cosTheta * (step / 2);
            nextPosition[1] = sinTheta * (step / 2);
//
            int i = 0;

            //Retrieve the center of rotation in the reconstruction
            while (currentDistanceFromCenter[0] < projectionWidth) {
                int positionX1 = (int) (currentDistanceFromCenter[0]);

                //Compute start position
                double xStart = cosTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationX);
                double zStart = sinTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationZ);

//                double weight = 1;
//                double weight = ComputationUtils.computeRayLenght(xStart - reconstructionCenterOfRotationX, zStart - reconstructionCenterOfRotationZ, theta + (Math.PI / 2.0d), -reconstructionCenterOfRotationX, -reconstructionCenterOfRotationZ, reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ);

                double[] sum = new double[1];

                if (xStart > 0 && zStart > 0 && xStart < reconstructionWidth && zStart < reconstructionSize) {
                    backProjectSum(xStart, zStart, currentYposition, nextPosition, sum, theta, projection[positionX1 + z * projectionWidth] / numberOfLine, reconstruction, countingMap, true);
                    backProjectSum(xStart, zStart, currentYposition, nextPosition, sum, theta + Math.PI, projection[positionX1 + z * projectionWidth] / numberOfLine, reconstruction, countingMap, false);
                }


                i++;
                //Compute the next position
                currentDistanceFromCenter[0] = i * step;
            }
        });
    }


    private static void _backProjection4(final float[] projection, double theta,
                                         final float[] reconstruction, final float[] countingMap,
                                         int reconstructionWidth, int reconstructionHeight, int reconstructionSize,
                                         double reconstructionCenterOfRotationX, double reconstructionCenterOfRotationZ,
                                         int projectionWidth, int projectionHeight,
                                         double centerOfRotationX,
                                         double sampling1) {
        final double cosTheta = ComputationUtils.cosR(theta, ComputationUtils.DEFAULT_ROUDING);
        final double sinTheta = -ComputationUtils.sinR(theta, ComputationUtils.DEFAULT_ROUDING);
        final double step = 1/sampling1;

        //For each slice ( parallelle tomography case)
        IntStream.range(0,projectionHeight).parallel().forEach(z -> {
            final double[] currentDistanceFromCenter = {0};
            final int currentYposition = z * reconstructionWidth;
            final double[] position = new double[2];
            final double[] nextPosition = new double[2];
            final double[] weight = new double[1];
//
            nextPosition[0] = cosTheta * (step / 2);
            nextPosition[1] = sinTheta * (step / 2);
//
            int i = 0;

            //Retrieve the center of rotation in the reconstruction
            while (currentDistanceFromCenter[0] < projectionWidth) {
                int positionX1 = (int) (currentDistanceFromCenter[0]);

                //Compute start position
                position[0] = cosTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationX);
                position[1] = sinTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationZ);

//                double weight = 1;
                weight[0] = ComputationUtils.computeRayLenght(position[0] - reconstructionCenterOfRotationX, position[1] - reconstructionCenterOfRotationZ, theta + (Math.PI / 2.0d), -reconstructionCenterOfRotationX, -reconstructionCenterOfRotationZ, reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ);

//                double[] sum = new double[1];

                if (position[0] > 0 && position[1] > 0 && position[0] < reconstructionWidth && position[1] < reconstructionSize) {
//                    backProjectSum(xStart, zStart, currentYposition, nextPosition, sum, theta, projection[positionX1 + z * projectionWidth] / numberOfLine, reconstruction, countingMap, true);
//                    backProjectSum(xStart, zStart, currentYposition, nextPosition, sum, theta + Math.PI, projection[positionX1 + z * projectionWidth] / numberOfLine, reconstruction, countingMap, false);

                    Projection.RAYTRACING.backprojection(currentYposition,weight,projection[positionX1 + z * projectionWidth],
                            reconstruction,countingMap,
                            theta + Math.PI/2,-1.0d,
                            reconstructionWidth,reconstructionHeight,reconstructionSize,
                            position,true);

                    Projection.RAYTRACING.backprojection(currentYposition,weight,projection[positionX1 + z * projectionWidth],
                            reconstruction,countingMap,
                            theta + 3*Math.PI/2,-1.0d,
                            reconstructionWidth,reconstructionHeight,reconstructionSize,
                            position,false);
                }


                i++;
                //Compute the next position
                currentDistanceFromCenter[0] = i * step;
            }
        });
    }


    private void backProjectSum(double xStart, double zStart, int yPosition, double[] toAdd, double[] weight, double theta, double value, final float[] reconstruction, final float[] countingMap, boolean doFirstAddition){
        final double[] initPosition = new double[]{xStart,zStart};
//        final double step = 1/numberOfStepByPixel;
        final double cosTheta = ComputationUtils.cosR(theta + Math.PI / 2, ComputationUtils.DEFAULT_ROUDING);
        final double sinTheta = -ComputationUtils.sinR(theta + Math.PI / 2, ComputationUtils.DEFAULT_ROUDING);
        final double step = Math.sqrt(cosTheta*cosTheta+sinTheta*sinTheta)/numberOfStepByPixel;


//        _backProjectSum(yPosition, weight, value, reconstruction, countingMap, step, cosTheta, sinTheta, initPosition, doFirstAddition);
        _backProjectSum3(yPosition, new double[]{step * numberOfStepByPixel}, value, reconstruction, countingMap, theta + (Math.PI / 2), initPosition, doFirstAddition);
    }

    /**
     * Compute the Sum projection along an axis
     * @param yPosition The Y position in the final reconstruction
     * @param value The Value to spread in the volume
     * @param reconstruction The Final reconstruction volume
     * @param countingMap the Final reconstruction counting volume
     * @param step The length of the step along the projection path
     * @param xDirection The direction of the projection path in the X dimension
     * @param yDirection The direction of the projection path in the Y dimension (Z in the reconstruction dimension)
     * @param initPosition The initial position of the projection in the reconstruction volume
     * @param doFirstAddition Boolean added to performs a slight shift in the forward direction
     * @return The sum value along this path
     */
    private void _backProjectSum(int yPosition, double[] maxLenght, double value, float[] reconstruction, float[] countingMap,
                                 double step, double xDirection, double yDirection,
                                 double[] initPosition, boolean doFirstAddition){
        double x = initPosition[0];
        double z = initPosition[1];
        double currentDistanceFromCenter = 0;
        int[] currentPosition = {(int) x,(int) z};

        if (doFirstAddition && currentPosition[0] < reconstructionWidth &&
                currentPosition[1] < reconstructionSize && currentPosition[0] >= 0 && currentPosition[1] >= 0) {
            //add a % of the value depending on the number of thread called to the sum projection
            currentDistanceFromCenter += step;
        }

        //Go through the stack with the thread
        loop:
        while (x >= 0 && z >= 0 && x < reconstructionWidth && z < reconstructionSize ) {
//            int i = 0;

            //Compute start position
            x = xDirection * ( currentDistanceFromCenter ) + initPosition[0];
            z = yDirection * ( currentDistanceFromCenter ) + initPosition[1];

            currentPosition[0] = (int) x;
            currentPosition[1] = (int) z;

            if  ( x >= 0 && z >= 0 && x < reconstructionWidth && z < reconstructionSize ) {
//                if ( (xStart-reconstructionCenterOfRotationX)*(xStart-reconstructionCenterOfRotationX)+(zStart-reconstructionCenterOfRotationY)*(zStart-reconstructionCenterOfRotationY) < projectionWidth/2*projectionWidth/2 ) {
                //add a % of the value depending on the number of thread called to the sum projection
//                  reconstruction[currentX + yPosition + currentY * reconstructionWidth * reconstructionHeight]+=value;
                reconstruction[currentPosition[0] + yPosition + currentPosition[1] * reconstructionWidth * reconstructionHeight] += value;
                countingMap[currentPosition[0] + yPosition + currentPosition[1] * reconstructionWidth * reconstructionHeight] += step;
//                }
                maxLenght[0] += value;
            }

            currentDistanceFromCenter += step;
        }
    }


    private static void _backProjectSum(int yPosition, double[] maxLenght, double value, float[] reconstruction, float[] countingMap,
                                        double theta, double sampling,
                                        int reconstructionWidth, int reconstructionHeight, int reconstructionSize,
                                        double[] initPosition, boolean doFirstAddition){

        double xDirection = ComputationUtils.cosR(theta,ComputationUtils.DEFAULT_ROUDING);
        double yDirection = -ComputationUtils.sinR(theta, ComputationUtils.DEFAULT_ROUDING);
        double step = Math.sqrt(xDirection*xDirection+yDirection*yDirection)/sampling;
        double x = initPosition[0];
        double z = initPosition[1];
        double currentDistanceFromCenter = 0;
        int[] currentPosition = {(int) x,(int) z};

        if (doFirstAddition && currentPosition[0] < reconstructionWidth &&
                currentPosition[1] < reconstructionSize && currentPosition[0] >= 0 && currentPosition[1] >= 0) {
            //add a % of the value depending on the number of thread called to the sum projection
            currentDistanceFromCenter += step;
        }

        //Go through the stack with the thread
        loop:
        while (x >= 0 && z >= 0 && x < reconstructionWidth && z < reconstructionSize ) {
//            int i = 0;

            //Compute start position
            x = xDirection * ( currentDistanceFromCenter ) + initPosition[0];
            z = yDirection * ( currentDistanceFromCenter ) + initPosition[1];

            currentPosition[0] = (int) x;
            currentPosition[1] = (int) z;

            if  ( x >= 0 && z >= 0 && x < reconstructionWidth && z < reconstructionSize ) {
//                if ( (xStart-reconstructionCenterOfRotationX)*(xStart-reconstructionCenterOfRotationX)+(zStart-reconstructionCenterOfRotationY)*(zStart-reconstructionCenterOfRotationY) < projectionWidth/2*projectionWidth/2 ) {
                //add a % of the value depending on the number of thread called to the sum projection
//                  reconstruction[currentX + yPosition + currentY * reconstructionWidth * reconstructionHeight]+=value;
                reconstruction[currentPosition[0] + yPosition + currentPosition[1] * reconstructionWidth * reconstructionHeight] += value;
                countingMap[currentPosition[0] + yPosition + currentPosition[1] * reconstructionWidth * reconstructionHeight] += step;
//                }
                maxLenght[0] += value;
            }

            currentDistanceFromCenter += step;
        }
    }


    /**
     * Compute the Sum projection along an axis
     * @param yPosition The Y position in the final reconstruction
     * @param value The Value to spread in the volume
     * @param reconstruction The Final reconstruction volume
     * @param countingMap the Final reconstruction counting volume
     * @param theta The direction of the projection path in the X dimension
     * @param initPosition The initial position of the projection in the reconstruction volume
     * @param doFirstAddition Boolean added to performs a slight shift in the forward direction
     * @return The sum value along this path
     */
    @Deprecated
    private void _backProjectSum3(int yPosition, double[] maxLenght, double value, float[] reconstruction, float[] countingMap,
                                 double theta,
                                 double[] initPosition, boolean doFirstAddition){
//        double x = initPosition[0];
//        double z = initPosition[1];
//        final double[] currentDistanceFromCenter = {0};
//        int[] currentPosition = {(int) x,(int) z};

//        if (doFirstAddition && currentPosition[0] < reconstructionWidth &&
//                currentPosition[1] < reconstructionSize && currentPosition[0] >= 0 && currentPosition[1] >= 0) {
            //add a % of the value depending on the number of thread called to the sum projection
//            currentDistanceFromCenter[0] += step;
//        }

        DirectRayTracing directRayTracing = new DirectRayTracing();

        directRayTracing.init(theta, new double[]{initPosition[0], initPosition[1]});


        double[] pos = directRayTracing.getCurrentPosition();
        int[] ints = directRayTracing.getIntegerPosition();
        double[] stepSize = directRayTracing.getComputedStep();

        //Retrieve the center of rotation in the reconstruction
        while (ints[0] >= 0 && ints[1] >= 0 && ints[0] < reconstructionWidth && ints[1] < reconstructionSize) {

            if ( stepSize[0] > maxLenght[0] ){
                stepSize[0] = maxLenght[0];
            }
//            if (ints[0] >= 0 && ints[1] >= 0 && ints[0] < reconstructionWidth && ints[1] < reconstructionSize) {
//                reconstruction[ints[0] + yPosition + ints[1] * reconstructionWidth * reconstructionHeight] += value*(stepSize[0]/maxLenght[0]);
                reconstruction[ints[0] + yPosition + ints[1] * reconstructionWidth * reconstructionHeight] += value*(stepSize[0]/maxLenght[0]);
                countingMap[ints[0] + yPosition + ints[1] * reconstructionWidth * reconstructionHeight]++;
//            }


            //Compute the next position
            directRayTracing.getNextPosition();
        }
    }


    private static void _backProjectSum3(int yPosition, double[] maxLenght, double value, float[] reconstruction, float[] countingMap,
                                  double theta,
                                  int reconstructionWidth, int reconstructionHeight, int reconstructionSize,
                                  double[] initPosition, boolean doFirstAddition){

        DirectRayTracing directRayTracing = new DirectRayTracing();

        directRayTracing.init(theta, initPosition);
        final double step = directRayTracing.getComputedStep()[0];
        directRayTracing.setMaximalDistance(step);
//        double[] pos = directRayTracing.getCurrentPosition();
        int[] ints = directRayTracing.getIntegerPosition();
        double[] stepSize = directRayTracing.getComputedStep();

        //TODO Bug first pixel is not taken, and 1 pixel space due to the orientation modification
        if (doFirstAddition) {
            directRayTracing.getNextPosition();
        }

        int[] savePosition = new int[256*256];
        int l = 0;
        double sum = 0;

        //Retrieve the center of rotation in the reconstruction
        while (ints[0] >= 0 && ints[1] >= 0 && ints[0] < reconstructionWidth && ints[1] < reconstructionSize) {

            stepSize[0] = ComputationUtils.round(stepSize[0],10000);

            reconstruction[ints[0] + yPosition + ints[1] * reconstructionWidth * reconstructionHeight] += value*stepSize[0];

            savePosition[l]  = ints[0] + yPosition + ints[1] * reconstructionWidth * reconstructionHeight;

            countingMap[ints[0] + yPosition + ints[1] * reconstructionWidth * reconstructionHeight] += stepSize[0];
//            countingMap[ints[0] + yPosition + ints[1] * reconstructionWidth * reconstructionHeight] ++;

            sum += stepSize[0];

            l++;

            if ( l >= savePosition.length-1 ){
                break;
            }

            //Compute the next position
            directRayTracing.getNextPosition();
        }


        for ( int k = 0 ; k < l ; k++ ){
            reconstruction[savePosition[k]] /= sum;
        }
    }

    private void backProjectSumArea(double x, double z, int yPosition, double[] toAdd, double weight, double theta, double value, final float[] reconstruction, final float[] countingMap, boolean doFirstAddtion){
        double[] initPosition = {x,z};
        double currentDistanceFromCenter = 0;
        int[] currentPosition = {(int) x,(int) z};
        int[] oldPosition = {(int)x,(int)z};
        final double step = 1/numberOfStepByPixel;
        theta += Math.PI/2.0d;

        if ( doFirstAddtion && currentPosition[0] < reconstructionWidth &&
                currentPosition[1] < reconstructionSize && currentPosition[0] >= 0 && currentPosition[1] >= 0) {
            //add a % of the value depending on the number of thread called to the sum projection
//            countingMap[oldX + yPosition * reconstructionWidth + oldY*reconstructionWidth*reconstructionHeight] += computeTriangleRayLine(x,z,x+step[0],z+step[1], theta)/weight;
//            reconstruction[oldX + yPosition * reconstructionWidth + oldY * reconstructionWidth * reconstructionHeight] += value*computeTriangleRayLine(x,x,x+step[0],z+step[1], theta)/weight;
            currentDistanceFromCenter += step;
        }

        final double xDirection = ComputationUtils.cosR(theta + Math.PI / 2, ComputationUtils.DEFAULT_ROUDING);
        final double yDirection = -ComputationUtils.sinR(theta + Math.PI / 2, ComputationUtils.DEFAULT_ROUDING);

        //Go through the stack with the thread
        loop:
        while ( x < reconstructionWidth && z < reconstructionSize ){
            double count = 0;
            while (currentPosition[0] == oldPosition[0] && currentPosition[1] == oldPosition[1]){
                x = xDirection * ( currentDistanceFromCenter ) + initPosition[0];
                z = yDirection * ( currentDistanceFromCenter ) + initPosition[1];

                currentPosition[0] = (int) x;
                currentPosition[1] = (int) z;

                count += step;
                if ( currentPosition[0] < 0 || currentPosition[1] < 0 ||
                        currentPosition[0] >= reconstructionWidth || currentPosition[1] >= reconstructionSize ){
                    break loop;
                }
            }

            System.arraycopy(currentPosition,0,oldPosition,0,2);

            //add a % of the value depending on the number of thread called to the sum projection
            reconstruction[currentPosition[0] + yPosition * reconstructionWidth + currentPosition[1]*reconstructionWidth*reconstructionHeight] += value*ComputationUtils.computeTriangleRayLine(x,z,x+toAdd[0],z+toAdd[1],theta)/weight;
            countingMap[currentPosition[0] + yPosition * reconstructionWidth + currentPosition[1]*reconstructionWidth*reconstructionHeight] += ComputationUtils.computeTriangleRayLine(x,z,x+toAdd[0],z+toAdd[1],theta)/weight;
        }
    }


    /**
     * Compute the Back propagation for the given projection and the reconstruction table.
     * Multi Threaded in the Z order
     * @param projection 1D array containing a projection data
     * @param theta the angle of rotation around the rotation axis ( 1 axis rotation only )
     * @param reconstruction  1D array containing reconstruction data
     * @param countingMap 1D array containing an integers corresponding to the number of time the pixel has been passed thought
     */
    @Deprecated
    private void backProjection1D(float[] projection, double theta,final float[] reconstruction,final float[] countingMap) {
        //Theta is set as perpendicular projection
        double[] angle = new double[3];
        angle[0] = theta;

        //Construct the rotation matrix
        double[][] rotationMatrix = ComputationUtils.computeRotationMatrix(angle);

        //MULTI THREAD (THREAD SAFE IMPLEMENTATION)
        IntStream.range(0, reconstructionHeight).parallel().forEach(p -> {
            double r, value;
            double x, y, z;
            //For every x/y in the reconstruction
            for (y = 0; y < (p + 1); y += 1) {
                for (x = 0; x < reconstructionWidth; x += 1) {
                    double[] pos = ComputationUtils.computeRotatedCoordinate(x - reconstructionCenterOfRotationX, y - reconstructionCenterOfRotationY, 0, rotationMatrix);
                    pos[0] += centerOfRotationX;

                    if (pos[0] >= projectionROI[0] && pos[0] < projectionROI[1]) {
                        //Linear extrapolation
                        value = ComputationUtils.getLinearInterpolationValue(pos[0], 0, projectionWidth, projection);

                        //Eliminate the external Pixel ( Sup to the limite of geometrical reconstruction )
                        if (!Double.isNaN(value) && value != 0.0f) {
                            reconstruction[(int) (x + (y * reconstructionWidth))] += (float) value;
                            countingMap[(int) (x + (y * reconstructionWidth))] += 1;
                        }
                    }
                }
            }
        });
    }

    @Deprecated
    public void backProjection(float[] projection, double theta, float[] reconstruction){
        backProjection(projection, theta, reconstruction, new float[reconstruction.length]);
    }


    /**
     *
     * @param reconstruction the currently reconstructed array
     * @param theta The angle of rotation
     * @param realProjection the real projection data
     * @param relaxationCoef the relaxation coefficient
     * @param withSousTraction if the subtraction between the real and computed projection has to be done
     * @return float[] the projection for this angle theta.
     */
    //TODO change projection according to Cedric, interpolation should be avoided ( can )
    @Deprecated
    public float[] projectionOLD(float[] reconstruction, double theta, float[] realProjection,float relaxationCoef ,boolean withSousTraction){
        final float[] computedProjection =  new float[projectionWidth*projectionHeight];
        final float [] weighting = new float[projectionWidth*projectionHeight];

        int i;

        final double[] angle = {theta,0,0};

        //Construct the rotation matrix
        final double[][] rotationMatrix = ComputationUtils.computeRotationMatrix(angle);

        IntStream.range(0,reconstructionSize).parallel().forEach( z -> {
            double y,x,value;
            double[] pos;

            for (y = 0; y < reconstructionHeight; y += 1) {
                for (x = 0; x < reconstructionWidth; x += 1) {
                    //Compute the bilinear interpolation
                    value = ComputationUtils.getBilinearInterpolationValue(x,y, z * reconstructionWidth * reconstructionHeight, reconstructionWidth, reconstructionHeight ,reconstruction);
                    if (!Double.isNaN(value)) {
                        //Compute the coordinate in the projection
                        pos = ComputationUtils.computeRotatedCoordinate(x - reconstructionCenterOfRotationX, y - reconstructionCenterOfRotationY, z - reconstructionCenterOfRotationZ, rotationMatrix);

//                        double r = pos[0];
//                        double c = pos[2]+this.centerOfRotationY;
//                        r += this.centerOfRotationX;
                        double r = pos[0]+reconstructionCenterOfRotationX;
                        double c = pos[2]+reconstructionCenterOfRotationY;
//                        r += this.centerOfRotationX;
                        if (r >= 0 && r < projectionWidth && c >= 0 && c < projectionHeight) {
                            //Add the computed intensity value to the reconstructed Projection
                            //TODO application of a Hamming Window required to store a new array(List) for each projection
//                            computedProjections[z][(int) r] += (r-(int)r)*value;
                            if ( value > 0.0d ) {
                                ComputationUtils.setBilinearInterpolationValue(r, c, projectionWidth, (float) value, computedProjection);
                                //Add this pixel has passed though by the the incident ray
                                ComputationUtils.setBilinearInterpolationValue(r, c, projectionWidth, 1.0f, weighting);
                            }
                        }

                    }
                }
            }
        });

//        ComputationUtils.arrayDivide(computedProjection,weighting);

        //Divide by the length path through the object
        if ( withSousTraction ){
            for ( i = 0 ; i < computedProjection.length ; i++ ){
//            //Weighting on the computedProjection
                computedProjection[i] = realProjection[i] - computedProjection[i];

                if ( weighting[i] > 0.0f){
                    if ( weighting[i] < 1.0f ){
                        computedProjection[i] /= weighting[i]+1;
                    }else{
                        computedProjection[i] /= weighting[i];
                    }
                }

//                error_counter += computedProjection[i]*computedProjection[i];
                computedProjection[i] *= relaxationCoef;
            }
        }

        return computedProjection;
    }


    /**
     * @param reconstruction the currently reconstructed array
     * @param theta The angle of rotation
     * @param realProjection the real projection data
     * @param relaxationCoef the relaxation coefficient
     * @param withSousTraction if the subtraction between the real and computed projection has to be done
     * @return float[] the projection for this angle theta.
     */
    private static float[] projection(final float[] reconstruction, double theta, final float[] realProjection,
                                     final double[] error, float relaxationCoef, boolean withSousTraction,
                                     int projectionWidth, int projectionHeight, double centerOfRotationX,
                                     int reconstructionWidth, int reconstructionHeight, int reconstructionSize,
                                     double reconstructionCenterOfRotationX, double reconstructionCenterOfRotationZ,
                                     double sampling1, double sampling2){

        final float[] computedProjection =  new float[projectionWidth*projectionHeight];
        final float [] countingMap = new float[projectionWidth*projectionHeight];
        final float [] weightF = new float[projectionWidth*projectionHeight];
        final double cosTheta = ComputationUtils.cosR(theta,ComputationUtils.DEFAULT_ROUDING);
        final double sinTheta = -ComputationUtils.sinR(theta, ComputationUtils.DEFAULT_ROUDING);
        final double step = Math.sqrt(cosTheta*cosTheta+sinTheta*sinTheta)/sampling1;
//        final double step = 1/numberOfLine;
//        for( int y = 0 ; y < projectionHeight ; y++ ) {
//            for (int x = 0; x < projectionWidth; x++) {
////                double weight = ComputationUtils.computeRayLenghtInCircle(x, y, theta + Math.PI / 2, projectionWidth/2.0d, projectionHeight/2.0d, projectionWidth/2.0d);
//                double weight = ComputationUtils.computeRayLenght(x, 0, theta + Math.PI/2, -reconstructionCenterOfRotationX, -reconstructionCenterOfRotationZ, reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ);
//
//                if ( weight == 0 ){
//                    weightF[x + y * projectionWidth] = Float.MAX_VALUE;
//                }else{
//                    weightF[x + y * projectionWidth] = (float) weight;
//                }
//            }
//        }

        //For each slice ( parallelle tomography case)
        IntStream.range(0,projectionHeight).parallel().forEach(z -> {
            final double[] currentDistanceFromCenter = {0};
            final double[] sum = new double[2];
            final double[] position = new double[2];
            final int currentYposition = z * reconstructionWidth;

            final double[] nextPosition = new double[2];
//
            nextPosition[0] = cosTheta * (step / 2);
            nextPosition[1] = sinTheta * (step / 2);
//
            int i = 0;
            //Retrieve the center of rotation in the reconstruction
            while (currentDistanceFromCenter[0] < projectionWidth) {
                int positionX1 = (int) (currentDistanceFromCenter[0]);
                int projectionPosition = (positionX1 + z * projectionWidth);
                Arrays.fill(sum, 0);

                //Compute start position
                position[0] = cosTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationX);
                position[1] = sinTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationZ);

                double weight = ComputationUtils.computeRayLenght(position[0] - reconstructionCenterOfRotationX, position[1] - reconstructionCenterOfRotationZ, theta + (Math.PI / 2.0d), -reconstructionCenterOfRotationX, -reconstructionCenterOfRotationZ, reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ);
                weightF[positionX1 + z * projectionWidth] = (float)weight;

                if (position[0] >= 0 && position[1] >= 0 && position[0] < reconstructionWidth && position[1] < reconstructionSize) {
                    sum[0] += Projection.DEFAULT.projection(currentYposition,weight,reconstruction,countingMap,
                            projectionPosition,theta, sampling2,
                            reconstructionWidth,reconstructionHeight, reconstructionSize,
                            position, false);

                    sum[0] += Projection.DEFAULT.projection(currentYposition,weight,reconstruction,countingMap,
                            projectionPosition,theta + Math.PI, sampling2,
                            reconstructionWidth,reconstructionHeight, reconstructionSize,
                            position, false);

                }
                computedProjection[projectionPosition] += sum[0]/sampling1;
                countingMap[projectionPosition] /= sampling1;

                i++;
                //Compute the next position
                currentDistanceFromCenter[0] = i * step;
            }

        });

        if ( withSousTraction ) {
            BasicReconstructionFunction.computeIterationAdjustement(realProjection,computedProjection,
                    weightF,countingMap,
                    relaxationCoef,error);
        }

        return computedProjection;
    }


    private static void computeIterationAdjustement(float[] realProjection, float[] computedProjection,
                                                    float[] weightReal, float[] weightComputed,
                                                    float relaxationCoef, double[] error) {
        float realVal = 0;
        float computedVal = 0;

        for (int i = 0; i < computedProjection.length; i++) {
            if (weightReal[i] == 0) {
                realVal = 0;
            } else {
                realVal = realProjection[i] / weightReal[i];
            }

            if (weightComputed[i] == 0) {
                computedVal = 0;
            } else {
                computedVal = computedProjection[i] / weightComputed[i];
            }

            computedProjection[i] = (realVal) - computedVal;

            //Error Counter
            error[0] += Math.abs(computedProjection[i]);

            computedProjection[i] *= relaxationCoef;
        }

        error[0] /= realProjection.length;
    }

    /**
     * @param reconstruction the currently reconstructed array
     * @param theta The angle of rotation
     * @param realProjection the real projection data
     * @param relaxationCoef the relaxation coefficient
     * @param withSousTraction if the subtraction between the real and computed projection has to be done
     * @return float[] the projection for this angle theta.
     */
    @Deprecated
    private float[] _projection(final float[] reconstruction, double theta, final float[] realProjection, float relaxationCoef, boolean withSousTraction){
        final float[] computedProjection =  new float[projectionWidth*projectionHeight];
        final float [] countingMap = new float[projectionWidth*projectionHeight];
        final float [] weightF = new float[projectionWidth*projectionHeight];
        final double cosTheta = ComputationUtils.cosR(theta,ComputationUtils.DEFAULT_ROUDING);
        final double sinTheta = -ComputationUtils.sinR(theta, ComputationUtils.DEFAULT_ROUDING);
        final double step = Math.sqrt(cosTheta*cosTheta+sinTheta*sinTheta)/numberOfLine;
//        final double step = 1/numberOfLine;
//        for( int y = 0 ; y < projectionHeight ; y++ ) {
//            for (int x = 0; x < projectionWidth; x++) {
////                double weight = ComputationUtils.computeRayLenghtInCircle(x, y, theta + Math.PI / 2, projectionWidth/2.0d, projectionHeight/2.0d, projectionWidth/2.0d);
//                double weight = ComputationUtils.computeRayLenght(x, 0, theta + Math.PI/2, -reconstructionCenterOfRotationX, -reconstructionCenterOfRotationZ, reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ);
//
//                if ( weight == 0 ){
//                    weightF[x + y * projectionWidth] = Float.MAX_VALUE;
//                }else{
//                    weightF[x + y * projectionWidth] = (float) weight;
//                }
//            }
//        }

        //For each slice ( parallelle tomography case)
        IntStream.range(0,projectionHeight).parallel().forEach(z -> {
            final double[] currentDistanceFromCenter = {0};
            final double[] sum = new double[2];
            final int currentYposition = z * reconstructionWidth;

            final double[] nextPosition = new double[2];
//
            nextPosition[0] = cosTheta * (step / 2);
            nextPosition[1] = sinTheta * (step / 2);
//
            int i = 0;
            //Retrieve the center of rotation in the reconstruction
            while (currentDistanceFromCenter[0] < projectionWidth) {
                int positionX1 = (int) (currentDistanceFromCenter[0]);
                int projectionPosition = (positionX1 + z * projectionWidth);
                Arrays.fill(sum, 0);

                //Compute start position
                double xStart = cosTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationX);
                double zStart = sinTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationZ);

//                double weight = ComputationUtils.computeRayLenght(0, 0, theta + Math.PI/2, -reconstructionCenterOfRotationX, -reconstructionCenterOfRotationZ, reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ);
//                double weight = 1;
                double weight = ComputationUtils.computeRayLenght(xStart - reconstructionCenterOfRotationX, zStart - reconstructionCenterOfRotationZ, theta + (Math.PI / 2.0d), -reconstructionCenterOfRotationX, -reconstructionCenterOfRotationZ, reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ);
                weightF[positionX1 + z * projectionWidth] = (float)weight;
//                double weight = ComputationUtils.computeRayLenghtInCircle(xStart, zStart, theta + Math.PI / 2, reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ, projectionWidth/2.0d);
//                double weight = weightF[positionX1 + z * projectionWidth];

                if (xStart >= 0 && zStart >= 0 && xStart < reconstructionWidth && zStart < reconstructionSize) {
                    sum[0] += projectionSum(xStart, zStart, weight, currentYposition, theta, reconstruction, countingMap, projectionPosition, true);
                    sum[0] += projectionSum(xStart, zStart, weight, currentYposition, theta + Math.PI, reconstruction, countingMap, projectionPosition, false);
//                        sum[0] += projectionSumArea(xStart, zStart, currentYposition, theta, reconstruction, nextPosition, true);
//                        sum[0] += projectionSumArea(xStart, zStart, currentYposition, theta + Math.PI, reconstruction, nextPosition, false);
                }
                computedProjection[projectionPosition] += sum[0]/numberOfLine;
                countingMap[projectionPosition] /= numberOfLine;

                i++;
                //Compute the next position
                currentDistanceFromCenter[0] = i * step;
            }

        });

        if ( withSousTraction ) {
            //Warning Here CountingMap seems to Bug so we change with the WeightF factor ( but thats not correct )
//            ComputationUtils.arrayDivide(computedProjection, countingMap);
//            ComputationUtils.arrayDivide(computedProjection, weightF);
            float realVal = 0;
            float computedVal = 0;

            for (int i = 0; i < computedProjection.length; i++) {
//                computedProjection[i] = ((realProjection[i] / weightF[i]) - computedProjection[i]) * countingMap[i];
//                computedProjection[i] = ((realProjection[i] / weightF[i]) - computedProjection[i]) * weightF[i];


                if ( weightF[i] == 0 ){
                    realVal = 0;
                }else{
                    realVal = realProjection[i]/weightF[i];
                }

                if ( countingMap[i] == 0){
                    computedVal = 0;
                }else{
                    computedVal = computedProjection[i]/countingMap[i];
                }

                computedProjection[i] = (realVal) - computedVal;

                //Error Counter
//                error_counter += Math.abs(computedProjection[i]);

                computedProjection[i] *= relaxationCoef;

            }
//            error_counter /= computedProjection.length;
        }

        return computedProjection;
    }


    /**
     * @param reconstruction the currently reconstructed array
     * @param theta The angle of rotation
     * @param realProjection the real projection data
     * @param relaxationCoef the relaxation coefficient
     * @param withSousTraction if the subtraction between the real and computed projection has to be done
     * @return float[] the projection for this angle theta.
     */
    @Deprecated
    private float[] _projection3(final float[] reconstruction, double theta, final float[] realProjection, float relaxationCoef, boolean withSousTraction){
        final float[] computedProjection =  new float[projectionWidth*projectionHeight];
        final float [] countingMap = new float[projectionWidth*projectionHeight];
        final float [] weightF = new float[projectionWidth*projectionHeight];
//        final double cosTheta = ComputationUtils.cosR(theta,ComputationUtils.DEFAULT_ROUDING);
//        final double sinTheta = -ComputationUtils.sinR(theta, ComputationUtils.DEFAULT_ROUDING);
        final double cosTheta = ComputationUtils.round(Math.cos(theta));
        final double sinTheta = ComputationUtils.round(-Math.sin(theta));
        final double step = Math.sqrt(cosTheta * cosTheta + sinTheta * sinTheta);


        //For each slice ( parallelle tomography case)
        IntStream.range(0,projectionHeight).parallel().forEach(z -> {
            final double[] currentDistanceFromCenter = {0};
            final int currentYposition = z * reconstructionWidth;
            final double[] sum = new double[2];
            final double[] nextPosition = new double[2];

            //Compute start position
            double xStart = cosTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationX);
            double zStart = sinTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationZ);

            DirectRayTracing directRayTracing = new DirectRayTracing();

            directRayTracing.init(theta, new double[]{ComputationUtils.round(xStart), ComputationUtils.round(zStart)});


            double[] pos = directRayTracing.getCurrentPosition();
            int[] ints = directRayTracing.getIntegerPosition();
            double[] stepSize = directRayTracing.getComputedStep();

            final double[] maxLength = new double[1];

            //Retrieve the center of rotation in the reconstruction
            while (currentDistanceFromCenter[0] < projectionWidth) {
                Arrays.fill(sum, 0);
                int positionX1 = (int)(currentDistanceFromCenter[0]);
                int projectionPosition = (positionX1 + z * projectionWidth);

                if ( stepSize[0] > step ){
                    stepSize[0] = step;
                }

                if  ( projectionPosition < projectionWidth*projectionHeight && projectionPosition >= 0 ){
                    maxLength[0] = ComputationUtils.computeRayLenght(pos[0] - reconstructionCenterOfRotationX, pos[1] - reconstructionCenterOfRotationZ, theta + (Math.PI / 2.0d), -reconstructionCenterOfRotationX, -reconstructionCenterOfRotationZ, reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ);

                    if (ints[0] >= 0 && ints[1] >= 0 && ints[0] < reconstructionWidth && ints[1] < reconstructionSize) {

                        sum[0] += projectionSum(ints[0], ints[1], maxLength[0], currentYposition, theta, reconstruction, countingMap, projectionPosition, false);
                        sum[0] += projectionSum(ints[0], ints[1], maxLength[0], currentYposition, theta + Math.PI, reconstruction, countingMap, projectionPosition, false);

    //                    sum[0] += projectionSum(pos[0], pos[1], maxLenght[0], currentYposition, theta, reconstruction, countingMap, projectionPosition, false);
    //                    sum[0] += projectionSum(pos[0], pos[1], maxLenght[0], currentYposition, theta + Math.PI, reconstruction, countingMap, projectionPosition, false);
                    }

//                if  ( projectionPosition < projectionWidth*projectionHeight  ){
                    computedProjection[projectionPosition] += (sum[0]*(stepSize[0]/cosTheta))/maxLength[0];
                }

                //Compute the next position
                currentDistanceFromCenter[0] += stepSize[0];

                directRayTracing.getNextPosition();
            }
        });

        if ( withSousTraction ) {
//            ComputationUtils.arrayDivide(computedProjection, countingMap);
            float realVal = 0;
            float computedVal = 0;
            for (int i = 0; i < computedProjection.length; i++) {

//                computedProjection[i] = ((realProjection[i] / weightF[i]) - computedProjection[i]);
//                computedProjection[i] = (realProjection[i] - computedProjection[i])/weightF[i];
                if ( weightF[i] == 0 ){
                    realVal = 0;
//                    computedProjection[i] = 0;
                }else{
                    realVal = realProjection[i]/weightF[i];
//                    computedProjection[i] = (realProjection[i] - computedProjection[i])/weightF[i];
                }

                if ( countingMap[i] == 0){
                    computedVal = 0;
                }else{
                    computedVal = computedProjection[i]/countingMap[i];
                }

                computedProjection[i] = (realVal) - computedVal;

                //Error Counter
//                error_counter += Math.abs(computedProjection[i]);

                computedProjection[i] *= relaxationCoef;

            }
//            error_counter /= computedProjection.length;
        }

        return computedProjection;
    }


    /**
     * The Ray tracing projection algorithm.
     * @param reconstruction
     * @param theta
     * @param realProjection
     * @param error
     * @param relaxationCoef
     * @param withSousTraction
     * @param projectionWidth
     * @param projectionHeight
     * @param centerOfRotationX
     * @param reconstructionWidth
     * @param reconstructionHeight
     * @param reconstructionSize
     * @param reconstructionCenterOfRotationX
     * @param reconstructionCenterOfRotationZ
     * @return
     */
    private static float[] _projection3(final float[] reconstruction, double theta, final float[] realProjection,
                                        final double[] error, float relaxationCoef, boolean withSousTraction,
                                        int projectionWidth, int projectionHeight, double centerOfRotationX,
                                        int reconstructionWidth, int reconstructionHeight, int reconstructionSize,
                                        double reconstructionCenterOfRotationX, double reconstructionCenterOfRotationZ){
        final float[] computedProjection =  new float[projectionWidth*projectionHeight];
        final float [] countingMap = new float[projectionWidth*projectionHeight];
        final float [] weightF = new float[projectionWidth*projectionHeight];

        final double cosTheta = Math.cos(theta);
        final double sinTheta = -Math.sin(theta);


        //For each slice ( parallelle tomography case)
//        for ( int z = 0 ; z < projectionHeight ; z++ ) {
//            System.out.println(step);
        IntStream.range(0,projectionHeight).parallel().forEach(z -> {
            final double[] currentDistanceFromCenter = {0};
            final int currentYposition = z * reconstructionWidth;
            final double[] sum = new double[2];

            //Compute start position
            double xStart = cosTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationX);
            double zStart = sinTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationZ);

            DirectRayTracing directRayTracing = new DirectRayTracing();

            directRayTracing.init(theta, new double[]{ComputationUtils.round(xStart,100), ComputationUtils.round(zStart,100)});
//            directRayTracing.init(theta, new double[]{xStart, zStart});

            double[] pos = directRayTracing.getCurrentPosition();
            double[] stepSize = directRayTracing.getComputedStep();

            final double[] maxLength = new double[1];

            double remainingFrac = 0;
            double testy = 0;
            //Retrieve the center of rotation in the reconstruction
            while (currentDistanceFromCenter[0] < projectionWidth) {
//            while (posInt < projectionWidth) {
                Arrays.fill(sum, 0);

                //Convert the X/Y position to projection position
                int projectionPosition = (int) (currentDistanceFromCenter[0] + z * projectionWidth);
//                int projectionPosition =  (posInt + z * projectionWidth);

                if (projectionPosition < projectionWidth * projectionHeight && projectionPosition >= 0) {
                    maxLength[0] = ComputationUtils.computeRayLenght(pos[0] - reconstructionCenterOfRotationX, pos[1] - reconstructionCenterOfRotationZ, theta + (Math.PI / 2.0d), -reconstructionCenterOfRotationX, -reconstructionCenterOfRotationZ, reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ);

                    sum[0] += Projection.RAYTRACING.projection(currentYposition, maxLength[0], reconstruction, countingMap,
                            projectionPosition, theta + Math.PI / 2, 0,
                            reconstructionWidth, reconstructionHeight, reconstructionSize,
                            pos, true);

                    sum[0] += Projection.RAYTRACING.projection(currentYposition, maxLength[0], reconstruction, countingMap,
                            projectionPosition, theta + 3 * Math.PI / 2, 0,
                            reconstructionWidth, reconstructionHeight, reconstructionSize,
                            pos, false);
                }

                double factor = ComputationUtils.round(stepSize[0],10000);
                testy += factor;
//              step
                int decalage = 0;
                //While case to check position in the projection width
                    //If the step is still in the same pixel
                if (testy < 1) {
                    //Add the full summation
                    computedProjection[projectionPosition] += sum[0]*(factor);
                    //increment the remaining fraction
                    remainingFrac = testy;
                } else {
                    while ( testy >= 1 && currentDistanceFromCenter[0] + decalage < projectionWidth ) {
                        computedProjection[projectionPosition+decalage] += sum[0] * (1 - remainingFrac);
                        testy -= 1;
                        decalage++;
                        if ( testy >= 1 ){
                            remainingFrac = 0;
                        }else{
                            remainingFrac = testy;
                        }
                    }
                    if (currentDistanceFromCenter[0] + decalage < projectionWidth) {
                        computedProjection[projectionPosition + decalage] += sum[0] * remainingFrac;
                    }
                }

                currentDistanceFromCenter[0] += factor;
                directRayTracing.getNextPosition();
            }
        });

        if ( withSousTraction ) {
            BasicReconstructionFunction.computeIterationAdjustement(realProjection,computedProjection,
                    weightF,countingMap,relaxationCoef,error);
        }

        return computedProjection;
    }

    /**
     * @param reconstruction the currently reconstructed array
     * @param theta The angle of rotation
     * @param realProjection the real projection data
     * @param relaxationCoef the relaxation coefficient
     * @param withSousTraction if the subtraction between the real and computed projection has to be done
     * @return float[] the projection for this angle theta.
     */
    @Deprecated
    private float[] _projection4(final float[] reconstruction, double theta, final float[] realProjection, float relaxationCoef, boolean withSousTraction){
        final float[] computedProjection =  new float[projectionWidth*projectionHeight];
        final float [] countingMap = new float[projectionWidth*projectionHeight];
        final float [] weightF = new float[projectionWidth*projectionHeight];
//        final double cosTheta = ComputationUtils.cosR(theta,ComputationUtils.DEFAULT_ROUDING);
//        final double sinTheta = -ComputationUtils.sinR(theta, ComputationUtils.DEFAULT_ROUDING);
        final double cosTheta = ComputationUtils.round(Math.cos(theta));
        final double sinTheta = ComputationUtils.round(-Math.sin(theta));
        final double step = 1/numberOfLine;



        //For each slice ( parallelle tomography case)
        IntStream.range(0,projectionHeight).parallel().forEach(z -> {
            final double[] currentDistanceFromCenter = {0};
            final double[] sum = new double[2];
            final int currentYposition = z * reconstructionWidth;

            final double[] nextPosition = new double[2];
//
            nextPosition[0] = cosTheta * (step / 2);
            nextPosition[1] = sinTheta * (step / 2);
//
            int i = 0;
            //Retrieve the center of rotation in the reconstruction
            while (currentDistanceFromCenter[0] < projectionWidth) {
                int positionX1 = (int) (currentDistanceFromCenter[0]);
                int projectionPosition = (positionX1 + z * projectionWidth);
                Arrays.fill(sum, 0);

                //Compute start position
                double xStart = cosTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationX);
                double zStart = sinTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationZ);

//                double weight = ComputationUtils.computeRayLenght(xStart - reconstructionCenterOfRotationX, zStart - reconstructionCenterOfRotationZ, theta + (Math.PI / 2.0d), -reconstructionCenterOfRotationX, -reconstructionCenterOfRotationZ, reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ);
                double weight = ComputationUtils.computeRayLenghtInCircle(xStart - reconstructionCenterOfRotationX, zStart - reconstructionCenterOfRotationZ, theta + (Math.PI / 2.0d), 0, 0 ,projectionWidth);
                weightF[positionX1 + z * projectionWidth] = (float)weight;

                if (xStart >= 0 && zStart >= 0 && xStart < reconstructionWidth && zStart < reconstructionSize) {
                    sum[0] += projectionSum(xStart, zStart, weight, currentYposition, theta, reconstruction, countingMap, projectionPosition, true);
                    sum[0] += projectionSum(xStart, zStart, weight, currentYposition, theta + Math.PI, reconstruction, countingMap, projectionPosition, false);
                }


                computedProjection[projectionPosition] += sum[0]/(numberOfLine);
//                computedProjection[projectionPosition] += sum[0]/(weight*numberOfLine);
                countingMap[projectionPosition] /= numberOfLine;

                i++;
                //Compute the next position
                currentDistanceFromCenter[0] = i * step;
            }
        });

        if ( withSousTraction ) {
//            ComputationUtils.arrayDivide(computedProjection, countingMap);
            float realVal = 0;
            float computedVal = 0;
            for (int i = 0; i < computedProjection.length; i++) {

//                computedProjection[i] = ((realProjection[i] / weightF[i]) - computedProjection[i]);
//                computedProjection[i] = (realProjection[i] - computedProjection[i])/weightF[i];
                if ( weightF[i] == 0 ){
                    realVal = 0;
//                    computedProjection[i] = 0;
                }else{
                    realVal = realProjection[i]/weightF[i];
//                    computedProjection[i] = (realProjection[i] - computedProjection[i])/weightF[i];
                }

                if ( countingMap[i] == 0){
                    computedVal = 0;
                }else{
                    computedVal = computedProjection[i]/countingMap[i];
                }

                computedProjection[i] = (realVal) - computedVal;


                //Error Counter
//                error_counter += Math.abs(computedProjection[i]);

                computedProjection[i] *= relaxationCoef;

            }
//            error_counter /= computedProjection.length;
        }

        return computedProjection;
    }

    private static float[] _projection4(final float[] reconstruction, double theta, final float[] realProjection,
                                 final double[] error, float relaxationCoef, boolean withSousTraction,
                                 int projectionWidth, int projectionHeight, double centerOfRotationX,
                                 int reconstructionWidth, int reconstructionHeight, int reconstructionSize,
                                 double reconstructionCenterOfRotationX, double reconstructionCenterOfRotationZ,
                                 double sampling1){
        final float[] computedProjection =  new float[projectionWidth*projectionHeight];
        final float [] countingMap = new float[projectionWidth*projectionHeight];
        final float [] weightF = new float[projectionWidth*projectionHeight];
//        final double cosTheta = ComputationUtils.cosR(theta,ComputationUtils.DEFAULT_ROUDING);
//        final double sinTheta = -ComputationUtils.sinR(theta, ComputationUtils.DEFAULT_ROUDING);
//        final double cosTheta = ComputationUtils.round(Math.cos(theta));
//        final double sinTheta = ComputationUtils.round(-Math.sin(theta));
        final double cosTheta = Math.cos(theta);
        final double sinTheta = -Math.sin(theta);
        //+1 because 0 is part of the next pixel
        final double step = 1/(sampling1);



        //For each slice ( parallelle tomography case)
        IntStream.range(0,projectionHeight).parallel().forEach(z -> {
            final double[] currentDistanceFromCenter = {0};
            final double[] sum = new double[2];
            final double[] position = new double[2];
            final int currentYposition = z * reconstructionWidth;

            final double[] nextPosition = new double[2];
//
            nextPosition[0] = cosTheta * (step / 2);
            nextPosition[1] = sinTheta * (step / 2);
//
            int i = 0;
            //Retrieve the center of rotation in the reconstruction
            while (currentDistanceFromCenter[0] < projectionWidth-1) {
                int positionX1 = (int) (currentDistanceFromCenter[0]);
                int projectionPosition = (positionX1 + z * projectionWidth);
                Arrays.fill(sum, 0);
                for ( int h = 0; h < sampling1-1 ; h++ ) {
                    currentDistanceFromCenter[0] += step;

                    //Compute start position
                    position[0] = cosTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationX);
                    position[1] = sinTheta * (currentDistanceFromCenter[0] - centerOfRotationX) + (reconstructionCenterOfRotationZ);

//                double weight = ComputationUtils.computeRayLenght(xStart - reconstructionCenterOfRotationX, zStart - reconstructionCenterOfRotationZ, theta + (Math.PI / 2.0d), -reconstructionCenterOfRotationX, -reconstructionCenterOfRotationZ, reconstructionCenterOfRotationX, reconstructionCenterOfRotationZ);
                    double weight = ComputationUtils.computeRayLenghtInCircle(position[0] - reconstructionCenterOfRotationX, position[1] - reconstructionCenterOfRotationZ, theta + (Math.PI / 2.0d), 0, 0, projectionWidth) / 2;
                    weightF[positionX1 + z * projectionWidth] = (float) weight;

                    if (position[0] >= 0 && position[1] >= 0 && position[0] < reconstructionWidth && position[1] < reconstructionSize) {
//                    sum[0] += projectionSum(xStart, zStart, weight, currentYposition, theta, reconstruction, countingMap, projectionPosition, true);
//                    sum[0] += projectionSum(xStart, zStart, weight, currentYposition, theta + Math.PI, reconstruction, countingMap, projectionPosition, false);

                        sum[0] += Projection.RAYTRACING.projection(currentYposition, weight, reconstruction, countingMap,
                                projectionPosition, theta + Math.PI / 2, 0,
                                reconstructionWidth, reconstructionHeight, reconstructionSize,
                                position, true);

                        sum[0] += Projection.RAYTRACING.projection(currentYposition, weight, reconstruction, countingMap,
                                projectionPosition, theta + 3 * Math.PI / 2, 0,
                                reconstructionWidth, reconstructionHeight, reconstructionSize,
                                position, true);

                    }


//                computedProjection[projectionPosition] += sum[0]/(weight*numberOfLine);
                    countingMap[projectionPosition] /= sampling1;


                }

                computedProjection[projectionPosition] += sum[0] / (sampling1-1);
                i++;
                //Compute the next position
                currentDistanceFromCenter[0] = i;
            }
        });

        if ( withSousTraction ) {
            BasicReconstructionFunction.computeIterationAdjustement(realProjection,computedProjection,
                    weightF,countingMap,relaxationCoef,error);
        }

        return computedProjection;
    }

    /**
     * Compute the Sum projection along an axis.
     * Warning this function is very long to execute due to the
     * computation at each step of the area under the ray
     * @param xStart The X start position of the Axis
     * @param zStart The Y start position of the Axis
     * @param yPosition The z position of the Axis
     * @param theta The angle maid between the reconstruction X/Y plan and the X/Z projection plan
     * @param reconstruction The reconstruction array
     * @param doFirstAddition If the First addition has to be maid ( if the sum is maid in the two direction compute the first addition only once )
     * @return The Sum value along this theta + PI/2 axis
     */
    private double projectionSumArea(double xStart, double zStart, int yPosition, double theta, float[] reconstruction, double[] toAdd, boolean doFirstAddition){
        double sum = 0;
        int oldX = (int) xStart;
        int oldY = (int) zStart;
        double count = 0;
        final double[] initPosition = new double[]{xStart,zStart};
        final double step = 1/numberOfStepByPixel;
        final double cosTheta = Math.cos(theta + Math.PI / 2);
        final double sinTheta = Math.sin(theta + Math.PI / 2);
        double currentDistanceFromCenter = 0;

        if (doFirstAddition && oldX < reconstructionWidth && oldY < reconstructionSize && oldX >= 0 && oldY >= 0) {
//            double totalArea = ComputationUtils.computeTriangleRayLine(xStart, zStart, (xStart + toAdd[0]), (zStart + toAdd[1]), theta + (Math.PI / 2));
//            totalArea += ComputationUtils.computeTriangleRayLine(xStart, zStart, (xStart - toAdd[0]), (zStart - toAdd[1]), theta + (Math.PI / 2));
//            //add a % of the value depending on the number of thread called to the sum projection
//            sum += reconstruction[oldX + yPosition + oldY * reconstructionWidth * reconstructionHeight]*totalArea;
            count += step;
        }

        //Go through the stack with the thread
        loop:
        while (xStart <= reconstructionWidth && zStart <= reconstructionSize) {
            int currentX = oldX;
            int currentY = oldY;
            while (currentX == oldX && currentY == oldY) {
//                xStart += cosTheta;
//                zStart += sinTheta;
                //Compute start position
                xStart = cosTheta * ( currentDistanceFromCenter + count ) + initPosition[0];
                zStart = sinTheta * ( currentDistanceFromCenter + count ) + initPosition[1];

                currentX = (int) xStart;
                currentY = (int) zStart;


                count += step;


                if (currentX < 0 || currentY < 0 || currentX >= reconstructionWidth || currentY >= reconstructionSize) {
                    break loop;
                }
            }

            oldX = currentX;
            oldY = currentY;

            //add a % of the value depending on the number of thread called to the sum projection
            double totalArea = ComputationUtils.computeTriangleRayLine(xStart, zStart, (xStart + toAdd[0]), (zStart + toAdd[1]), theta + (Math.PI / 2));
            totalArea += ComputationUtils.computeTriangleRayLine(xStart, zStart, (xStart - toAdd[0]), (zStart - toAdd[1]), theta + (Math.PI / 2));

            //add a % of the value depending on the number of thread called to the sum projection
            sum += reconstruction[currentX + yPosition + currentY * reconstructionWidth * reconstructionHeight]*totalArea;

            currentDistanceFromCenter += count;
            //Warning count as to be updated last !
            count = 0;
        }

        return sum;
    }

    /**
     * Compute the Sum projection along an axis
     * @param xStart The X start position of the Axis
     * @param zStart The Y start position of the Axis
     * @param yPosition The z position of the Axis
     * @param theta The angle maid between the reconstruction X/Y plan and the X/Z projection plan
     * @param reconstruction The reconstruction array
     * @param doFirstAddition If the First addition has to be maid ( if the sum is maid in the two direction compute the first addition only once )
     * @return The Sum value along this theta + PI/2 axis
     */
    @Deprecated
    private double projectionSum(double xStart, double zStart, double maxLenght, int yPosition, double theta, float[] reconstruction, float[] countingMap, int projectionPosition, boolean doFirstAddition){

        double[] initPosition = new double[]{xStart,zStart};
        double cosTheta = ComputationUtils.cosR(theta + Math.PI / 2,ComputationUtils.DEFAULT_ROUDING);
        double sinTheta = -ComputationUtils.sinR(theta + Math.PI / 2, ComputationUtils.DEFAULT_ROUDING);
//        double step = 1/numberOfStepByPixel;
        final double step = Math.sqrt(cosTheta*cosTheta+sinTheta*sinTheta)/numberOfStepByPixel;

//        return _projectionSum(yPosition, maxLenght, reconstruction, countingMap, projectionPosition, step, cosTheta, sinTheta, initPosition, doFirstAddition);
        return _projectionSum3(yPosition, step*numberOfStepByPixel, reconstruction, countingMap, projectionPosition, theta + Math.PI / 2, initPosition, doFirstAddition);
    }


    /**
     * Compute the Sum projection along an axis
     * @param yPosition The Y position in the final reconstruction
     * @param reconstruction The Final reconstruction volume
     * @param countingMap the Final reconstruction counting volume
     * @param projectionPosition The current position where the sum will be computed
     * @param step The length of the step along the projection path
     * @param xDirection The direction of the projection path in the X dimension
     * @param yDirection The direction of the projection path in the Y dimension (Z in the reconstruction dimension)
     * @param initPosition The initial position of the projection in the reconstruction volume
     * @param doFirstAddition Boolean added to performs a slight shift in the forward direction
     * @return The sum value along this path
     */
    @Deprecated
    private double _projectionSum(int yPosition,double maxLenght, float[] reconstruction, float[] countingMap,
                                  int projectionPosition, double step, double xDirection, double yDirection,
                                  double[] initPosition, boolean doFirstAddition){
        double sum = 0;
        double x = initPosition[0];
        double z = initPosition[1];
        double currentDistanceFromCenter = 0;
        int[] currentPosition = {(int) x,(int) z};

        if (doFirstAddition && currentPosition[0] < reconstructionWidth &&
                currentPosition[1] < reconstructionSize && currentPosition[0] >= 0 && currentPosition[1] >= 0) {
            //add a % of the value depending on the number of thread called to the sum projection
            currentDistanceFromCenter += step;
        }

        //Go through the stack with the thread
        loop:
        while (x >= 0 && z >= 0 && x < reconstructionWidth && z < reconstructionSize) {

            //Compute start position
            x = xDirection * ( currentDistanceFromCenter ) + initPosition[0];
            z = yDirection * ( currentDistanceFromCenter ) + initPosition[1];

            currentPosition[0] = (int) x;
            currentPosition[1] = (int) z;

            if  (x >= 0 && z >= 0 && x < reconstructionWidth && z < reconstructionSize) {
                //add a % of the value depending on the number of thread called to the sum projection
//                sum += ComputationUtils.getLinearInterpolationValue(x,yPosition + currentPosition[1] * reconstructionWidth * reconstructionHeight,reconstructionWidth,reconstruction);
//                sum += ComputationUtils.getBilinearInterpolationValueZ(x,z,yPosition,reconstructionWidth,reconstructionHeight,reconstructionSize,reconstruction)/(numberOfStepByPixel*numberOfLine);
//                sum += ComputationUtils.getBilinearInterpolationValue(x, yPosition, currentPosition[1] * reconstructionWidth * reconstructionHeight, reconstructionHeight, reconstructionWidth, reconstruction);
                sum += reconstruction[currentPosition[0] + yPosition + currentPosition[1] * reconstructionWidth * reconstructionHeight]/(numberOfStepByPixel*numberOfLine);
                countingMap[projectionPosition] += step;
            }

            currentDistanceFromCenter += step;
        }

        return sum;
    }


    /**
     * Compute the Sum projection along an axis
     * @param yPosition The Y position in the final reconstruction
     * @param reconstruction The Final reconstruction volume
     * @param countingMap the Final reconstruction counting volume
     * @param projectionPosition The current position where the sum will be computed
     * @param initPosition The initial position of the projection in the reconstruction volume
     * @param doFirstAddition Boolean added to performs a slight shift in the forward direction
     * @return The sum value along this path
     */
    private static double _projectionSum(int yPosition,double maxLenght, float[] reconstruction, float[] countingMap,
                                         int projectionPosition, double theta, double sampling,
                                         int reconstructionWidth, int reconstructionHeight, int reconstructionSize,
                                         double[] initPosition, boolean doFirstAddition){
        double sum = 0;

        double xDirection = ComputationUtils.cosR(theta + Math.PI / 2,ComputationUtils.DEFAULT_ROUDING);
        double yDirection = -ComputationUtils.sinR(theta + Math.PI / 2, ComputationUtils.DEFAULT_ROUDING);
        double step = Math.sqrt(xDirection*xDirection+yDirection*yDirection)/sampling;
        double x = initPosition[0];
        double z = initPosition[1];
        double currentDistanceFromCenter = 0;
        int[] currentPosition = {(int) x,(int) z};

        if (doFirstAddition && currentPosition[0] < reconstructionWidth &&
                currentPosition[1] < reconstructionSize && currentPosition[0] >= 0 && currentPosition[1] >= 0) {
            //add a % of the value depending on the number of thread called to the sum projection
            currentDistanceFromCenter += step;
        }

        //Go through the stack with the thread
        loop:
        while (x >= 0 && z >= 0 && x < reconstructionWidth && z < reconstructionSize) {

            //Compute start position
            x = xDirection * ( currentDistanceFromCenter ) + initPosition[0];
            z = yDirection * ( currentDistanceFromCenter ) + initPosition[1];

            currentPosition[0] = (int) x;
            currentPosition[1] = (int) z;

            if  (x >= 0 && z >= 0 && x < reconstructionWidth && z < reconstructionSize) {
                //add a % of the value depending on the number of thread called to the sum projection
//                sum += ComputationUtils.getLinearInterpolationValue(x,yPosition + currentPosition[1] * reconstructionWidth * reconstructionHeight,reconstructionWidth,reconstruction);
//                sum += ComputationUtils.getBilinearInterpolationValueZ(x,z,yPosition,reconstructionWidth,reconstructionHeight,reconstructionSize,reconstruction)/(numberOfStepByPixel*numberOfLine);
//                sum += ComputationUtils.getBilinearInterpolationValue(x, yPosition, currentPosition[1] * reconstructionWidth * reconstructionHeight, reconstructionHeight, reconstructionWidth, reconstruction);
                sum += reconstruction[currentPosition[0] + yPosition + currentPosition[1] * reconstructionWidth * reconstructionHeight]/sampling;
                countingMap[projectionPosition] += step;
            }

            currentDistanceFromCenter += step;
        }

        return sum;
    }


    /**
     * Compute the Sum projection along an axis
     * @param yPosition The Y position in the final reconstruction
     * @param reconstruction The Final reconstruction volume
     * @param countingMap the Final reconstruction counting volume
     * @param projectionPosition The current position where the sum will be computed
     * @param theta The direction of the projection path in the X dimension
     * @param initPosition The initial position of the projection in the reconstruction volume
     * @param doFirstAddition Boolean added to performs a slight shift in the forward direction
     * @return The sum value along this path
     */
    @Deprecated
    private double _projectionSum3(int yPosition,double maxLenght, float[] reconstruction, float[] countingMap,
                                  int projectionPosition, double theta,
                                  double[] initPosition, boolean doFirstAddition){
        double sum = 0;

        DirectRayTracing directRayTracing = new DirectRayTracing();

        directRayTracing.init(theta, new double[]{initPosition[0], initPosition[1]});


        double[] pos = directRayTracing.getCurrentPosition();
        int[] ints = directRayTracing.getIntegerPosition();
        double[] stepSize = directRayTracing.getComputedStep();

        //Retrieve the center of rotation in the reconstruction
        while (ints[0] >= 0 && ints[1] >= 0 && ints[0] < reconstructionWidth && ints[1] < reconstructionSize) {

            if (  stepSize[0] > maxLenght ){
                stepSize[0] = maxLenght;
            }

            sum += reconstruction[ints[0] + yPosition + ints[1] * reconstructionWidth * reconstructionHeight]*(stepSize[0]/maxLenght);
//            sum += ComputationUtils.getBilinearInterpolationValueZ(pos[0], pos[1], yPosition, reconstructionWidth, reconstructionHeight, reconstructionSize, reconstruction) *(stepSize[0]/maxLenght);
//            sum += ComputationUtils.getBilinearInterpolationValue(pos[0], yPosition, ints[1] * reconstructionWidth * reconstructionHeight, reconstructionHeight, reconstructionWidth, reconstruction)*(stepSize[0]/maxLenght);
            countingMap[projectionPosition] += (stepSize[0]/maxLenght);

            //Compute the next position
            directRayTracing.getNextPosition();
        }

        return sum;
    }


    /**
     * Compute the Sum projection along an axis
     * @param yPosition The Y position in the final reconstruction
     * @param reconstruction The Final reconstruction volume
     * @param countingMap the Final reconstruction counting volume
     * @param projectionPosition The current position where the sum will be computed
     * @param theta The direction of the projection path in the X dimension
     * @param initPosition The initial position of the projection in the reconstruction volume
     * @param doFirstAddition Boolean added to performs a slight shift in the forward direction
     * @return The sum value along this path
     */
    private static double _projectionSum3(int yPosition,double maxLenght, float[] reconstruction, float[] countingMap,
                                   int projectionPosition, double theta,
                                   int reconstructionWidth, int reconstructionHeight, int reconstructionSize,
                                   double[] initPosition, boolean doFirstAddition){
        double sum = 0;

        DirectRayTracing directRayTracing = new DirectRayTracing();
        directRayTracing.init(theta, new double[]{initPosition[0], initPosition[1]});
        final double step = directRayTracing.getComputedStep()[0];
        directRayTracing.setMaximalDistance(step);

        //Why two ?
        if ( doFirstAddition ){
            directRayTracing.getNextPosition();
        }

//        double[] pos = directRayTracing.getCurrentPosition();
        int[] ints = directRayTracing.getIntegerPosition();
        double[] stepSize = directRayTracing.getComputedStep();


        //Retrieve the center of rotation in the reconstruction
        while (ints[0] >= 0 && ints[1] >= 0 && ints[0] < reconstructionWidth && ints[1] < reconstructionSize) {
//            sum += reconstruction[ints[0] + yPosition + ints[1] * reconstructionWidth * reconstructionHeight];
            sum += reconstruction[ints[0] + yPosition + ints[1] * reconstructionWidth * reconstructionHeight]*(stepSize[0]);

            //Compute the next position
            directRayTracing.getNextPosition();
        }

        return sum;
    }


    @Deprecated
    private double projectionSum2(double xStart, double zStart, int yPosition, double theta, float[] reconstruction, boolean doFirstAddtion){
        double sum = 0;

        int oldX = (int) Math.floor(xStart);
        int oldY = (int) Math.floor(zStart);

        if (doFirstAddtion && oldX < reconstructionWidth && oldY < reconstructionSize && oldX >= 0 && oldY >= 0) {
            //add a % of the value depending on the number of thread called to the sum projection
            sum += reconstruction[oldX + yPosition + oldY * reconstructionWidth * reconstructionHeight];
        }


        final double cosTheta = Math.cos(theta + Math.PI / 2)/numberOfStepByPixel;
        final double sinTheta = Math.sin(theta + Math.PI / 2)/numberOfStepByPixel;
        double count = 0;
        //Go through the stack with the thread
        loop:
        while (xStart <= reconstructionWidth && zStart <= reconstructionSize) {
            int currentX = oldX;
            int currentY = oldY;
            count = 0;
            while (currentX == oldX && currentY == oldY) {
                xStart += cosTheta;
                zStart += sinTheta;
                currentX = (int) Math.floor(xStart);
                currentY = (int) Math.floor(zStart);
                count++;
                if (currentX < 0 || currentY < 0 || currentX >= reconstructionWidth || currentY >= reconstructionSize) {
                    break loop;
                }
            }

            oldX = currentX;
            oldY = currentY;

            if( count > numberOfStepByPixel){
                count = numberOfStepByPixel;
            }

            //add a % of the value depending on the number of thread called to the sum projection
            sum += reconstruction[currentX + yPosition + currentY * reconstructionWidth * reconstructionHeight];
        }

        return sum;
    }


    /**
     *
     * @param reconstruction the currently reconstructed array
     * @param theta The angle of rotation
     * @param realProjection the real projection data
     * @param relaxationCoef the relaxation coefficient
     * @param withSousTraction if the subtraction between the real and computed projection has to be done
     * @return float[] the projection for this angle theta.
     */
    //TODO change projection according to Cedric, interpolation should be avoided ( can )
    @Deprecated
    private float[] projection2D_2(float[] reconstruction, double theta, float[] realProjection,float relaxationCoef ,boolean withSousTraction){
        final float[] computedProjection =  new float[projectionWidth*projectionHeight];
        final float [] weighting = new float[projectionWidth*projectionHeight];

        int i,sizeFactor=1;
        //Warning here -theta
//        final double[] angle = {-theta,0,0};
        final double[] angle = {0,-theta,0};
        //Construct the rotation matrix
        final double[][] rotationMatrix = ComputationUtils.computeRotationMatrix(angle);

        int newWidth = sizeFactor*reconstructionWidth;
        int newHeight = sizeFactor*reconstructionHeight;
        int newSize = reconstructionSize;

        float centerX = newWidth/2.0f;
        float centerY = newHeight/2.0f;
        float centerZ = newSize/2.0f;

        float[] reconstruction_rotated = new float[newWidth*newSize*newHeight];

        //Rotate the whole volume
        IntStream.range(0,newSize).parallel().forEach( z -> {
            int y,x;
            for (y = 0; y < newHeight; y++) {
                for (x = 0; x < newWidth; x++) {
                    double[] pos = ComputationUtils.computeRotatedCoordinate(x - centerX , y - centerY, z - centerZ, rotationMatrix);

                    pos[0] += reconstructionCenterOfRotationX - 0.5;
                    pos[1] += reconstructionCenterOfRotationY;
                    pos[2] += reconstructionCenterOfRotationZ;
                    float value = Float.NaN;

                    if ((pos[0] >= 0 && pos[1] >= 0  && pos[2] >= 0 ) &&
                        (pos[0] < reconstructionWidth && pos[1] < reconstructionHeight && pos[2] < reconstructionSize )) {
                        value = (float) ComputationUtils.getBilinearInterpolationValue(pos[0],  pos[1],  (int)pos[2] * reconstructionHeight * reconstructionWidth, reconstructionHeight, reconstructionWidth, reconstruction);
                    }
                    reconstruction_rotated[( (x + (y * newWidth) + (z * newHeight * newWidth)))] = value;
                }
            }
        });


        int posX = (int) ((newWidth-reconstructionWidth)/2.0d);
        int posY = (int) ((newHeight-reconstructionHeight)/2.0d);
        int posZ = (int) ((newSize-reconstructionSize)/2.0d);

        //Compute the projection inside the rotated volume
        IntStream.range(0,projectionHeight).parallel().forEach( l -> {
            int g,h;
            int currentY = (l+posY)*newWidth;
            for ( h=0; h < projectionWidth; h++){
                int pos = (h+posX) + currentY;
                for ( g = 0 ; g < newSize ; g++){
                    int positionZ = pos + (g*newWidth*newHeight);
                    if ( Float.isFinite(reconstruction_rotated[positionZ]) ){
                        weighting[h + l * projectionWidth]++;
                        computedProjection[h+l*projectionWidth] += reconstruction_rotated[positionZ];
                    }
                }
            }
        });

//        System.err.println("Warrning Array Divider to Remove");
//        ComputationUtils.arrayDivide(computedProjection,weighting);

//        //Divide by the length path through the object
        if ( withSousTraction ) {
            for (i = 0; i < computedProjection.length; i++) {
//
                computedProjection[i] = realProjection[i] - computedProjection[i];

                //Error Counter
//                error_counter += computedProjection[i] * computedProjection[i];

                //Weighting on the computedProjection
//                if ( weighting[i] > 0.0f){
//                    if ( weighting[i] < 1.0f ){
//                        computedProjection[i] /= weighting[i]+1;
//                    }else{
//                        computedProjection[i] /= weighting[i];
//                    }
//                }

                computedProjection[i] *= relaxationCoef;

            }
//            error_counter /= computedProjection.length;
        }

        return computedProjection;
    }

    /**
     *
     * @param reconstruction the currently reconstructed array
     * @param theta The angle of rotation
     * @param realProjection the real projection data
     * @param relaxationCoef the relaxation coefficient
     * @param withSousTraction if the subtraction between the real and computed projection has to be done
     * @return float[] the projection for this angle theta.
     */
    //TODO change projection according to Cedric, interpolation should be avoided ( can )
    @Deprecated
    private float[] projection1D(float[] reconstruction, double theta, float[] realProjection,float relaxationCoef ,boolean withSousTraction){
        final float[] computedProjection =  new float[projectionWidth*projectionHeight];
        final float [] weighting = new float[projectionWidth*projectionHeight];

        int i,sizeFactor=2;
        //Warning here -theta
        final double[] angle = {-theta,0,0};

        //Construct the rotation matrix
        final double[][] rotationMatrix = ComputationUtils.computeRotationMatrix(angle);

        int newWidth = sizeFactor*reconstructionWidth;
        int newHeight = sizeFactor*reconstructionHeight;
        int newSize = reconstructionSize;

        float centerX = newWidth/2.0f;
        float centerY = newHeight/2.0f;
        float centerZ = newSize/2.0f;

        float[] reconstruction_rotated = new float[newWidth*newSize*newHeight];

        IntStream.range(0,newSize).parallel().forEach( z -> {
            int y,x;
            for (y = 0; y < newHeight; y++) {
                for (x = 0; x < newWidth; x++) {
                    double[] pos = ComputationUtils.computeRotatedCoordinate(x - centerX , y - centerY, 0, rotationMatrix);

                    pos[0] += reconstructionCenterOfRotationX;
                    pos[1] += reconstructionCenterOfRotationY;
//                    pos[2] += reconstructionCenterOfRotationZ;
                    float value = Float.NaN;

                    if ((pos[0] >= 0 && pos[1] >= 0 ) &&
                            (pos[0] < reconstructionWidth && pos[1] < reconstructionHeight) ) {
//                        value = reconstruction[((int) (pos[0] + pos[1] * reconstructionWidth))];
                        value = (float) ComputationUtils.getBilinearInterpolationValue(pos[0],  pos[1], reconstructionWidth, reconstruction);
//                        if ( value < 1){
//                            value = (float) ComputationUtils.getBilinearInterpolationValue(pos[0],  pos[1], reconstructionWidth, reconstruction);
//                        }
                    }
                    reconstruction_rotated[x + (y * newWidth)] = value;
                }
            }
        });

//        new Thread(()->new ImagePlus("Test",ComputationUtils.createStackFromArray(reconstruction_rotated,newWidth,newHeight,newSize)).show()).start();


        int posX = (int) ((newWidth-reconstructionWidth)/2.0d);
        int posY = (int) ((newHeight-reconstructionHeight)/2.0d);

        IntStream.range(0,projectionWidth).parallel().forEach( l -> {
            int g,h;
            int pos = l+posX;
            for ( h = 0 ; h < newHeight ; h++){
                int positionY = pos + (h*newWidth);
                if ( Float.isFinite(reconstruction_rotated[positionY]) ){
                    weighting[l]++;
                    computedProjection[l] += reconstruction_rotated[positionY];
                }
            }
        });


//        //Divide by the length path through the object
        if ( withSousTraction ) {
            for (i = 0; i < computedProjection.length; i++) {
//
                computedProjection[i] = realProjection[i] - computedProjection[i];

                //Error Counter
//                error_counter += computedProjection[i] * computedProjection[i];

                //Weighting on the computedProjection
                if ( weighting[i] > 0.0f){
                    if ( weighting[i] < 1.0f ){
                        computedProjection[i] /= weighting[i]+1;
                    }else{
                        computedProjection[i] /= weighting[i];
                    }
                }

                computedProjection[i] *= relaxationCoef;

            }
//            error_counter /= computedProjection.length;
        }
        return computedProjection;
    }

    public float getCenterOfRotationX() {
        return centerOfRotationX;
    }

    public void setCenterOfRotationX(float centerOfRotationX) {
//        System.out.println("ReconstructionAlgebric -- Update Center of Rotation X : "+ centerOfRotationX);
        this.centerOfRotationX = centerOfRotationX;
    }

    public void setCenterOfRotationY(float centerOfRotationY) {
//        System.out.println("ReconstructionAlgebric -- Update Center of Rotation Y : "+ centerOfRotationY);
        this.centerOfRotationY = centerOfRotationY;
    }

    public float getCenterOfRotationY() {
        return centerOfRotationY;
    }

    public float getCenterOfRotationZ() {
        return centerOfRotationZ;
    }

    public void setCenterOfRotationZ(float centerOfRotationZ) {
//        System.out.println("ReconstructionAlgebric -- Update Center of Rotation Z : "+ centerOfRotationZ);
        this.centerOfRotationZ = centerOfRotationZ;
    }

    public ProjectionFilter getProjectionFilter() {
        return projectionFilter;
    }

    public void setProjectionFilter(ProjectionFilter projectionFilter) {
        this.projectionFilter = projectionFilter;
    }


    public void initReconstructionDimension(RXImage projection){
//            this.sampling = SessionObject.getCurrentSessionObject().getAnalyseFactory().getCurrent().getReconstructionSampling();

        projectionWidth = projection.getWidth();
        projectionHeight = projection.getHeight();
        projectionSize = projection.getSize();

        this.setCenterOfRotationY(projectionHeight/2.0f);
        this.setCenterOfRotationZ(projectionSize/2.0f);

        //By default set projectionROI if there is inconsistancy as all size
        if ( projectionROI[0] >= projectionROI[1] ){
            projectionROI[0] = 0;
            projectionROI[1] = projectionWidth;
        }
        if ( projectionROI[2] >= projectionROI[3] ){
            projectionROI[2] = 0;
            projectionROI[3] = projectionHeight;
        }
        //Used to select projection
        if ( projectionROI[4] >= projectionROI[5] ){
            projectionROI[4] = 0;
            projectionROI[5] = projectionSize;
        }

        reconstructionWidth = projectionROI[1] - projectionROI[0];
        reconstructionHeight = projectionROI[3] - projectionROI[2];
        reconstructionSize =  projectionROI[1] - projectionROI[0];

        reconstructionCenterOfRotationX=(reconstructionWidth)/2.0d;
        reconstructionCenterOfRotationY=(reconstructionHeight)/2.0d;
        reconstructionCenterOfRotationZ=(reconstructionSize)/2.0d;


        //Init the angle of rotation
        RXImage angle = RXTomoJ.getInstance().getModelController().getDataController().getComputedStack(StackType.MOTORS_ROTATION);

        thetaProjection = new double[projectionROI[5] - projectionROI[4]];
        if ( angle != null ) {
            int i = 0;
            //First Value of each proj will be the angle of proj
            for (int k = projectionROI[4]; k < projectionROI[5] ; k++) {
                thetaProjection[i] = Math.toRadians(angle.copySlice(k)[0]);
                i++;
            }
        }else{
            int i = 0;
            //First Value of each proj will be the angle of proj
            for (int k = projectionROI[4]; k < projectionROI[5]; k++) {
                thetaProjection[i] = Math.toRadians(totalCovorageAngle/projectionSize)*k;
                i++;
            }
        }
    }

    //TEST ONLY
    public void initDimension(RXImage reconstruction, int numberOfProjection){
//        this.sampling = SessionObject.getCurrentSessionObject().getAnalyseFactory().getCurrent().getReconstructionSampling();

        projectionWidth = reconstruction.getWidth();
        projectionHeight = reconstruction.getHeight();
//        projectionSize = reconstruction.getWidth();

        this.setCenterOfRotationY(projectionHeight/2.0f);
        this.setCenterOfRotationZ(projectionSize/2.0f);

        //By default set projectionROI if there is inconsistancy as all size
        if ( projectionROI[0] >= projectionROI[1] ){
            projectionROI[0] = 0;
            projectionROI[1] = projectionWidth;
        }
        if ( projectionROI[2] >= projectionROI[3] ){
            projectionROI[2] = 0;
            projectionROI[3] = projectionHeight;
        }
        //Used to select projection
        if ( projectionROI[4] >= projectionROI[5] ){
            projectionROI[4] = 0;
            projectionROI[5] = numberOfProjection;
        }

        reconstructionWidth = reconstruction.getWidth();
        reconstructionHeight = reconstruction.getHeight();
        reconstructionSize =  reconstruction.getSize();

        reconstructionCenterOfRotationX=(reconstructionWidth)/2.0d;
        reconstructionCenterOfRotationY=(reconstructionHeight)/2.0d;
        reconstructionCenterOfRotationZ=(reconstructionSize)/2.0d;
    }

    protected void setResultStack(RXImage resultStack, float[] result){
        float[] tmp_result;
        for ( int z = 0 ; z < reconstructionSize ; z++ ){
            tmp_result = new float[reconstructionHeight*reconstructionWidth];
            System.arraycopy(result,z*reconstructionWidth*reconstructionHeight,tmp_result,0,reconstructionHeight*reconstructionWidth);
            resultStack.setSlice(tmp_result, z);
        }
    }

    private void setRXImage(RXImage projection){
        initReconstructionDimension(projection);
    }

    @Override
    public void setNumberOfWorks(float numberOfWorks) {
        this.numberOfWorks = numberOfWorks;
    }

    @Override
    public float getWorkDone() {
        return workCompleted/numberOfWorks;
    }

    @Override
    public String getWorkDoneString() {
        return "Number Of Iteration  : " + (int) this.workCompleted + " / " + (int) this.numberOfWorks;
    }

    @Override
    public void hardStop() {
        this.closeProcess = true;
        this.workCompleted = this.numberOfWorks;
    }

    public void abort() {
        this.closeProcess = true;
    }

    public int[] getProjectionROI() {
        return projectionROI;
    }

    public void setProjectionROI(int[] projectionROI) {
        System.arraycopy(projectionROI,0,this.projectionROI,0, projectionROI.length);
    }


    public double getTotalCovorageAngle() {
        return totalCovorageAngle;
    }

    public void setTotalCovorageAngle(double totalCovorageAngle) {
        this.totalCovorageAngle = totalCovorageAngle;
    }

    public void addProgressListener(ProgressFunctionListener functionListener){
        progressFunctionListeners.add(functionListener);
    }

    public void removeProgressListener(ProgressFunctionListener functionListener) {
        progressFunctionListeners.remove(functionListener);
    }

    public void removeAllProgressListener(){
        progressFunctionListeners.clear();
    }

    public void fireProgressFunction() {
        progressFunctionListeners.forEach(l -> l.progress(getWorkDone()));
    }


    @Override
    public String toString() {
        String toString = "Reconstruction Function parameters :: ";

        toString += "\n\tProjection method :: "+ projectionMethod.toString();
        toString += "\n\tCenter of rotation (X,Y):: ("+centerOfRotationX+" ,"+centerOfRotationY+")";
        toString += "\n\tAngle coverage :: "+totalCovorageAngle;
        toString += "\n"+projectionFilter.toString();
        toString += "\n\tOversampling numberOfLine :: "+numberOfLine;
        toString += "\n\tOversampling numberOfStepByPixel :: "+numberOfStepByPixel;

        return toString;
    }
}
