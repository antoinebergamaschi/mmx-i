/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.reconstructionFunction;

import cern.colt.matrix.tfcomplex.impl.DenseFComplexMatrix1D;
import cern.colt.matrix.tfcomplex.impl.DenseFComplexMatrix2D;
import cern.colt.matrix.tfloat.impl.DenseFloatMatrix1D;
import cern.colt.matrix.tfloat.impl.DenseFloatMatrix2D;
import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.util.RXUtils;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;

import java.util.Date;
import java.util.stream.IntStream;

/**
 * Created by bergamaschi on 01/04/2014.
 */
public class ReconstructionFilteredBack extends BasicReconstructionFunction {

    private boolean doFilter = true;

    public ReconstructionFilteredBack(){
        super();
    }

//    @Deprecated
//    public ReconstructionFilteredBack(ReconstructionFilteredBack reconstructionFilteredBack) {
//        super.copy(reconstructionFilteredBack);
//        this.copy(reconstructionFilteredBack);
//    }

    @Deprecated
    private void copy(ReconstructionFilteredBack reconstructionFilteredBack){
        super.copy(reconstructionFilteredBack);
        this.doFilter = reconstructionFilteredBack.isDoFilter();
    }

    /**
     * Filter and Back projection the imageStack given in parameter.
     * @param imageStack the stackImage containing each projection to reconstruct
     * @return RXImage the resulting reconstruction
     */
    public RXImage filteredBackProjection(RXImage imageStack){
        this.numberOfWorks = imageStack.getSize();
        if ( doFilter ) {
            if (imageStack.getHeight() > 1) {
                return filteredBackProjection2D(imageStack);
            } else {
                return filteredBackProjection1D(imageStack);
            }
        }else{
            return retroProjections(imageStack);
        }
    }

    public RXImage filterData(RXImage image){
        if ( doFilter ) {
            if (image.getHeight() > 1) {
                return filterProjection2D(image);
            } else {
                return filterProjection1D(image);
            }
        }
        return image;
    }


    /**
     * Filter and Back projection the imageStack given in parameter.
     * @param imageStack the stackImage containing each projection to reconstruct
     * @return RXImage the resulting reconstruction
     */
    private RXImage filterProjection2D(RXImage imageStack){
        this.setNumberOfWorks(imageStack.getSize());

        //TODO this process is not takken in the computation time of the progressFunction
        RXImage filteredStack = new RXImage(imageStack.getWidth(),imageStack.getHeight(),imageStack.getSize());
        this.numberOfWorks += imageStack.getSize();

        IntStream.range(0,imageStack.getSize()).parallel().forEach( k -> {

            if ( !closeProcess ) {
                float position, value;
                int u, v, up, vp;
                float[] pixels, values;
                pixels = imageStack.copySlice(k);

                //TEST padding
                int width = 2*imageStack.getWidth()+1;
                int height = 2*imageStack.getHeight()+1;

                float[] padded = ComputationUtils.padDataWithZero(pixels,width,height,imageStack.getWidth(),imageStack.getHeight());
//                ComputationUtils.centerFFT(padded,width,heigth);

                DenseFloatMatrix2D data = new DenseFloatMatrix2D(height, width);
                data.assign(padded);

                DenseFComplexMatrix2D projection = new DenseFComplexMatrix2D(data);
                projection.fft2();

                for (v = 0; v < projection.rows(); v++) {
                    for (u = 0; u < projection.columns(); u++) {
                        up = u;
                        vp = v;
                        if (v > projection.rows() / 2) {
                            vp -= projection.rows();
                        }
                        if (u > projection.columns() / 2) {
                            up -= projection.columns();
                        }

                        values = projection.getQuick(v, u);

//                        projectionFilter.doFilter(position, values);
                        projectionFilter.doFilter(up, vp, values);

                        projection.setQuick(v, u, values);
                    }
                }

                projection.ifft2(true);

                float[] result = (float[]) projection.getRealPart().elements();
//                ComputationUtils.centerFFT(result,width,heigth);

                float[] crop = new float[imageStack.getHeight()*imageStack.getWidth()];

                for ( int y = 0 ; y < imageStack.getHeight() ; y++){
                    System.arraycopy(result,(y+height/2)*width + width/2,crop,y*imageStack.getWidth(),imageStack.getWidth());
                }

                filteredStack.setSlice(crop, k);
            }

            workCompleted++;
        });

        return filteredStack;
    }


    /**
     * Filter and Back projection the imageStack given in parameter.
     * @param imageStack the stackImage containing each projection to reconstruct
     * @return RXImage the resulting reconstruction
     */
    private RXImage filterProjection1D(RXImage imageStack){
        this.setNumberOfWorks(imageStack.getSize());

        //TODO this process is not takken in the computation time of the progressFunction
        RXImage filteredStack = new RXImage(imageStack.getWidth(),imageStack.getHeight(),imageStack.getSize());
        this.numberOfWorks += imageStack.getSize();

        IntStream.range(0,imageStack.getSize()).parallel().forEach( k -> {
            if ( !closeProcess ) {
                float position, value;
                int u, v;
                float[] pixels, values;
                pixels = imageStack.copySlice(k);

//                DenseFloatMatrix1D data = new DenseFloatMatrix1D(pixels);
//                DenseFComplexMatrix1D projection = new DenseFComplexMatrix1D(data);


                //TEST
                int width = 2*imageStack.getWidth()+1;

                float[] padded = ComputationUtils.padDataWithZero(pixels,width,1,imageStack.getWidth(),imageStack.getHeight());

                DenseFloatMatrix1D data = new DenseFloatMatrix1D(width);
                data.assign(padded);

                DenseFComplexMatrix1D projection = new DenseFComplexMatrix1D(data);

//            Compute the Fourrier transform of the current projection without imaginary parts
                projection.fft();

                for (v = 0; v < projection.size(); v++) {
                    position = v;

                    if (v > projection.size() / 2) {
                        position -= projection.size();
                    }

                    values = projection.getQuick(v);

                    projectionFilter.doFilter(position, values);

                    projection.setQuick(v, values);

                }

                //FFT-1
                projection.ifft(true);

                //crop Data
                float[] resultCrop = ComputationUtils.cropInArray((float[]) projection.getRealPart().elements(),0,0,0,width,imageStack.getHeight(),
                        1, imageStack.getWidth(),imageStack.getHeight(),1);

                //Create a new Array of pixel containing only the FFT RealPart
                filteredStack.setSlice(resultCrop, k + 1);
            }

            workCompleted++;
        });

        return filteredStack;
    }

    /**
     * Filter and Back projection the imageStack given in parameter.
     * @param imageStack the stackImage containing each projection to reconstruct
     * @return RXImage the resulting reconstruction
     */
    private RXImage filteredBackProjection2D(RXImage imageStack){
        this.setNumberOfWorks(imageStack.getSize());

        //TODO this process is not takken in the computation time of the progressFunction
        RXImage filteredStack = new RXImage(imageStack.getWidth(),imageStack.getHeight(),imageStack.getSize());
//        ArrayList<Float> weightValuesToSee = new ArrayList<>();
//        float[] weightValuesToSee = new float[500];
        this.numberOfWorks += imageStack.getSize();

        IntStream.range(0,imageStack.getSize()).parallel().forEach( k -> {

            if ( !closeProcess ) {
                float position, value;
                int u, v, up, vp;
                float[] pixels, values;
                pixels = imageStack.copySlice(k);

                //TEST
                int width = 2*imageStack.getWidth()+1;
                int heigth = 2*imageStack.getHeight()+1;

                float[] padded = ComputationUtils.padDataWithZero(pixels,width,heigth,imageStack.getWidth(),imageStack.getHeight());
//                ComputationUtils.centerFFT(padded,width,heigth);

                DenseFloatMatrix2D data = new DenseFloatMatrix2D(heigth, width);
                data.assign(padded);

                DenseFComplexMatrix2D projection = new DenseFComplexMatrix2D(data);
                projection.fft2();
                //TEST

                //Compute the Fourrier transform of the current projection without imaginary parts
                //2D datas
//                DenseFloatMatrix2D data = new DenseFloatMatrix2D(imageStack.getHeight(), imageStack.getWidth());
//                data.assign(pixels);
//
//                DenseFComplexMatrix2D projection = new DenseFComplexMatrix2D(data);
//                projection.fft2();


                for (v = 0; v < projection.rows(); v++) {
                    for (u = 0; u < projection.columns(); u++) {
                        up = u;
                        vp = v;
                        if (v > projection.rows() / 2) {
                            vp -= projection.rows();
                        }
                        if (u > projection.columns() / 2) {
                            up -= projection.columns();
                        }
//                        if (v > projection.rows()/2) {
//                            vp -= projection.rows()/2;
//                        }
//                        if (u > projection.columns() / 2) {
//                            up -= projection.columns()/2;
//                        }
//                        if (Math.signum(up * vp) != 0) {
//                            position = (float) Math.sqrt(vp * vp + up * up) * Math.signum(up * vp);
//                        } else if (up != 0) {
//                            position = (float) Math.sqrt(vp * vp + up * up) * Math.signum(up);
//                        } else if (vp != 0) {
//                            position = (float) Math.sqrt(vp * vp + up * up) * Math.signum(vp);
//                        } else {
//                            position = 0;
//                        }

                        values = projection.getQuick(v, u);

//                        projectionFilter.doFilter(position, values);
                        projectionFilter.doFilter(up, vp, values);

                        projection.setQuick(v, u, values);
                    }
                }

                //FFT-1
                //            projectionFFT.ifft(true);
                projection.ifft2(true);

                float[] result = (float[]) projection.getRealPart().elements();
//                ComputationUtils.centerFFT(result,width,heigth);

                float[] crop = new float[imageStack.getHeight()*imageStack.getWidth()];

                for ( int y = 0 ; y < imageStack.getHeight() ; y++){
                    System.arraycopy(result,y*width,crop,y*imageStack.getWidth(),imageStack.getWidth());
                }

//                RXImage imageStack1 = new RXImage(width,heigth,1);
//                imageStack1.setPixels(projection.getRealPart().elements(),1);
//                new ImagePlus("test",imageStack1).show();
                //Create a new Array of pixel containing only the FFT RealPart
                //            for (int t = 0; t < pixels.length; t++) {
                //                reconstructedPixel[t] = projectionFFT.elements()[t * 2];
                //            }
                //            filteredStack.setPixels(reconstructedPixel,k+1);
//                filteredStack.setPixels(projection.getRealPart().elements(), k + 1);
//                filteredStack.setPixels(result, k + 1);
                filteredStack.setSlice(crop, k);

            }

            workCompleted++;
        });

//        workCompleted = numberOfWorks;

//        new ImagePlus("test FilteredBack projection",filteredStack.duplicate()).show();

        //Retro  projection of the filtered projection
        return this.retroProjections(filteredStack);
    }


    /**
     * Filter and Back projection the imageStack given in parameter.
     * @param imageStack the stackImage containing each projection to reconstruct
     * @return RXImage the resulting reconstruction
     */
    private RXImage filteredBackProjection1D(RXImage imageStack){
        this.setNumberOfWorks(imageStack.getSize());

        //TODO this process is not takken in the computation time of the progressFunction
        RXImage filteredStack = new RXImage(imageStack.getWidth(),imageStack.getHeight(),imageStack.getSize());
//        ArrayList<Float> weightValuesToSee = new ArrayList<>();
//        float[] weightValuesToSee = new float[500];
        this.numberOfWorks += imageStack.getSize();

        IntStream.range(0,imageStack.getSize()).parallel().forEach( k -> {
            if ( !closeProcess ) {
                float position, value;
                int u, v;
                float[] pixels, values;
                pixels = imageStack.copySlice(k);

//                DenseFloatMatrix1D data = new DenseFloatMatrix1D(pixels);
//                DenseFComplexMatrix1D projection = new DenseFComplexMatrix1D(data);


                //TEST
                int width = 2*imageStack.getWidth()+1;

                float[] padded = ComputationUtils.padDataWithZero(pixels,width,1,imageStack.getWidth(),imageStack.getHeight());

                DenseFloatMatrix1D data = new DenseFloatMatrix1D(width);
                data.assign(padded);

                DenseFComplexMatrix1D projection = new DenseFComplexMatrix1D(data);
//                projection.fft2();
                //TEST

//            Compute the Fourrier transform of the current projection without imaginary parts
                projection.fft();

                for (v = 0; v < projection.size(); v++) {
                    position = v;

                    if (v > projection.size() / 2) {
                        position -= projection.size();
                    }

                    values = projection.getQuick(v);

                    projectionFilter.doFilter(position, values);

                    projection.setQuick(v, values);

                }

                //FFT-1
                projection.ifft(true);

                //crop Data
                float[] resultCrop = ComputationUtils.cropInArray((float[]) projection.getRealPart().elements(),0,0,0,width,imageStack.getHeight(),
                        1, imageStack.getWidth(),imageStack.getHeight(),1);

                //Create a new Array of pixel containing only the FFT RealPart
                filteredStack.setSlice(resultCrop, k + 1);
            }

            workCompleted++;
        });


//        new ImagePlus("test",filteredStack.duplicate()).show();
        //Retro  projection of the filtered projection
        return this.retroProjections(filteredStack);
    }



    /**
     * Basic retro projection
     * @param imageStack the stackImage containing each projection to reconstruct
     * @return RXImage the resulting reconstruction
     */
    public RXImage retroProjections(RXImage imageStack){
        float[] result = super.retroProjection(imageStack);

        RXImage rxImage = new RXImage(reconstructionWidth,reconstructionHeight,reconstructionSize);
        rxImage.setData(result);

        return rxImage;
    }

    public boolean isDoFilter() {
        return doFilter;
    }

    public void setDoFilter(boolean doFilter) {
        this.doFilter = doFilter;
    }

    @Override
    public String getFunctionName() {
        return "Filtered reconstruction";
    }

    @Override
    public float getErrorValue() {
        return -1;
    }
}
