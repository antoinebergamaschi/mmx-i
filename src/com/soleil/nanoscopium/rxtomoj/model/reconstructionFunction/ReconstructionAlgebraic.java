/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.reconstructionFunction;

import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.util.RXUtils;
import com.soleil.nanoscopium.rxtomoj.model.filters.ProjectionFilter;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import ij.ImagePlus;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by bergamaschi on 17/12/13.
 * TODO set the 3D reconstruction ( only work on 2D images )
 */
//TODO add a projectionMethod function to compare a projection and backprojection
public class ReconstructionAlgebraic extends BasicReconstructionFunction {

    private int ARTiteration = 10;

    private float ARTrelaxation = 0.1f;

    private boolean doBGART = false;

    private float k = 2.0f;
    public ReconstructionAlgebraic(){
        super();
    }

    @Deprecated
    public ReconstructionAlgebraic(ReconstructionAlgebraic reconstructionAlgebraic) {
        super.copy(reconstructionAlgebraic);
        this.copy(reconstructionAlgebraic);
    }

    @Deprecated
    private void copy(ReconstructionAlgebraic reconstructionAlgebraic){
        super.copy(reconstructionAlgebraic);

        this.setK(reconstructionAlgebraic.getK());
        this.setDoBGART(reconstructionAlgebraic.isDoBGART());
        this.setARTrelaxation(reconstructionAlgebraic.getARTrelaxation());
        this.setARTiteration(reconstructionAlgebraic.getARTiteration());
    }

    /**
     * ART implementation. This is an very early version
     * @param imageStack the stackImage containing each projection to reconstruct
     * @return RXImage the resulting reconstruction
     */
    public RXImage ART(RXImage imageStack){
//        float[] results = new float[reconstructionWidth*reconstructionHeight*reconstructionSize];
        final float[] count = new float[reconstructionWidth*reconstructionHeight*reconstructionSize];
        this.numberOfWorks = ARTiteration*projectionSize + projectionSize;

//        double increments = Math.toRadians(totalCovorageAngle/imageStack.getSize());
        final float[] results = retroProjection(imageStack);
        final double[] error = new double[1];

        //For the number of incrementation given
        for ( int loop = 0; loop < ARTiteration ; loop++ ){
//            int positionZ = projectionROI[4];
            error[0] = 0;
            //Reset count
            Arrays.fill(count, 0);

//            ARTrelaxation = 1.0f/(loop+1.0f);
            System.out.println("Relaxation :: "+ARTrelaxation);
//            error_counter = 0;

            //For every Theta increment
            //For every Theta increment
            int i = 0;
            for ( int positionZ = projectionROI[4]; positionZ < projectionROI[5] ; positionZ++ ){
//                double theta = positionZ*increments;

                float[] projection = imageStack.copySlice(positionZ);

                //Get the projection and compare it to the real one
                float[] computedProjection = projection(results, thetaProjection[i], projection, error, ARTrelaxation);

                //Apply the correction on the computed reconstruction
                backProjection(computedProjection, -thetaProjection[i], results, count);

                workCompleted++;
                fireProgressFunction();
                i++;
            }

            ComputationUtils.setMinimumValue(results,0,0);
            System.out.println("Boucle :: "+loop+" Error Counter == "+error[0]);

            if ( closeProcess ){
                System.err.println("Process Closed");
                break;
            }

            if ( doBGART ) {
                //Eliminate Background
                projectionFilter.backgroundFilter(results, k);
            }

            System.out.println("New Loop");
        }

        workCompleted = numberOfWorks;

        RXImage imageStackResults = new RXImage(reconstructionWidth,reconstructionHeight,reconstructionSize);
        imageStackResults.setData(Arrays.copyOf(results, results.length));
//        setResultStack(imageStackResults, results);

//        System.out.println("Time ART :: "+(new Date().getTime() - start.getTime()));

        return imageStackResults;
    }


    /**
     * ART implementation. This is an very early version
     * @param imageStack the stackImage containing each projection to reconstruct
     * @return RXImage the resulting reconstruction
     */
    @Deprecated
    public RXImage BGART(RXImage imageStack){
        float[] results = new float[reconstructionWidth*reconstructionHeight*reconstructionSize];
        this.numberOfWorks = ARTiteration*imageStack.getSize();

        double increments = Math.toRadians(1);
        ProjectionFilter projectionFilter =  new ProjectionFilter();
        double[] error = new double[1];

        //For the number of incrementation given
        //TODO set this parameter or compute is as an minimum  criteria
        for ( int loop = 0; loop < ARTiteration ; loop++ ){
            int positionZ = projectionROI[4];
            error[0] = 0;
            //For every Theta increment
            for ( double theta =  projectionROI[4]*increments ; theta <= projectionROI[5]*increments ; theta+=increments ){
//                if ( positionZ <= imageStack.getSize() ) {
                    float[] projection = imageStack.copySlice(positionZ);
                    //Get the projection and compare it to the real one
                    float[] computedProjection = projection(results, theta, projection, error, ARTrelaxation);


                    //Apply the correction on the computed reconstruction
                    backProjection(computedProjection, theta, results);
                    positionZ++;
//                }
            }

            //Eliminate Background
            projectionFilter.backgroundFilter(results, 2.0f);

            System.out.println("New Loop : "+loop);
        }


        RXImage imageStackResults = new RXImage(reconstructionWidth,reconstructionHeight,reconstructionSize);
        setResultStack(imageStackResults, results);
        return imageStackResults;
    }


    /**
     * SIRT implementation. SIRT is the Same as ART but the reconstuction is update when every projection have been computed
     * @param imageStack the stackImage containing each projection to reconstruct
     * @return RXImage the resulting reconstruction
     */
    public RXImage SIRT(RXImage imageStack){
//        float[] results = new float[reconstructionWidth*reconstructionHeight*reconstructionSize];
        this.numberOfWorks += ARTiteration*imageStack.getSize()*2;

        double increments = Math.toRadians(totalCovorageAngle/imageStack.getSize());
        float[][] projections = new float[projectionROI[5]-projectionROI[4]][projectionWidth*projectionHeight];

        final double[] error = new double[1];
        final float[] results = retroProjection(imageStack);
        //For the number of incrementation given
        //TODO set this parameter or compute is as an minimum  criteria
        for ( int loop = 0; loop < ARTiteration ; loop++ ){
            float[] countingMap = new float[reconstructionWidth*reconstructionHeight*reconstructionSize];
            error[0] = 0;
            int k = 0;
            //For every Theta increment
            for ( int positionZ = projectionROI[4]; positionZ < projectionROI[5] ; positionZ++ ){
//                double theta = positionZ*increments;
                float[] projection = imageStack.copySlice(positionZ);

                //Get the projection and compare it to the real one
                System.arraycopy(projection(results, thetaProjection[k], projection, error, ARTrelaxation), 0, projections[k], 0, projections[k].length);
                k++;

                workCompleted++;
                fireProgressFunction();
            }

            k = 0;
            for ( int positionZ = projectionROI[4]; positionZ < projectionROI[5] ; positionZ++ ){
//                double theta = positionZ*increments;
                //Apply the correction on the computed reconstruction
                backProjection(projections[k], thetaProjection[k], results, countingMap);

//                ComputationUtils.setMinimumValue(results, 0, 0);
                k++;
                workCompleted++;
                fireProgressFunction();
            }

//            ComputationUtils.arrayDivide(results,countingMap);
//            System.out.println("New Loop : " + loop);
            System.out.println("Boucle :: "+loop+" Error Counter == "+error[0]);
        }


        RXImage imageStackResults = new RXImage(reconstructionWidth,reconstructionHeight,reconstructionSize);
//        setResultStack(imageStackResults, results);
        imageStackResults.setData(results);
        return imageStackResults;
    }

    //TODO make it work



    public int getARTiteration() {
        return ARTiteration;
    }


    public void setARTiteration(int ARTiteration) {
        this.ARTiteration = ARTiteration;
    }

    public float getARTrelaxation() {
        return ARTrelaxation;
    }

    public void setARTrelaxation(float ARTrelaxation) {
        this.ARTrelaxation = ARTrelaxation;
    }

    public double testInterpolation(double x, double y, int width, float[] pixels){
        double result;
//        result  = this.getLinearInterpolationValue(x,pixels);
        result  = ComputationUtils.getBilinearInterpolationValue(x, y, width, pixels);
        return result;
    }


    //    }
//        return computedProjection;
//
//        }
//            computedProjection[i] /= weighting[i]+1;
//        for ( i = 0 ; i < computedProjection.length ; i++ ){
//
//        }
//            }
//                y+=dirY;
//                x+=dirX;
//                }
//                    weighting[i]++;
//                    computedProjection[i] += value;
//                if ( !Double.isNaN(value) ){
////                value = reconstruction[(int)x + (int)y*(int)width];
//                value = ComputationUtils.getBilinearInterpolationValue(x, y, (int) width, reconstruction);
////                value = reconstruction[(int)pos[0] + (int)pos[1]*(int)width];
////                value = this.getBilinearInterpolationValue(pos[0],pos[1], (int) width,reconstruction);
//
////                pos[1] += dirY;
////                pos[0] += dirX;
//            while ( x >= 0 && y >= 0 &&  x < (width-1) && y < (width-1)){
////            while ( pos[0] >= 0 && pos[1] >= 0 &&  pos[0] < (width-1) && pos[1] < (width-1)){
////            while ( pos[0] >= -width2 && pos[1] >= -width2 &&  pos[0] < (width2-1) && pos[1] < (width2-1)){
//
//
//
////            }
////                pos[k] *= i;
////                pos[k] += width2;
////            for ( k = 0 ; k < pos.length ; k++ ){
//
////            double[] pos = computeRotatedCoordinate(x, y, 0, rotationMatrix);
//
//            y=(i)*Math.sin(theta) ;
//            x=(i)*Math.cos(theta) ;
//            //Initialize the first position
//        for ( i = 0 ; i < computedProjection.length ; i++ ){
//
//        double dirY = Math.abs(Math.sin(theta));
//        double dirX = Math.abs(Math.cos(theta));
//        // so direction is sum of Math.cos(theta)
//        // posX = r * Math.cos(theta)
//        //Normally
//
//        double y,x;
//        //Test virtually grow of the image points
//
//        double[][] rotationMatrix = ComputationUtils.computeRotationMatrix(angle);
//        //Construct the rotation matrix
//        angle[1] = -theta;
//        double[] angle = new double[3];
//
//
//        double width = Math.sqrt(reconstruction.length),width2=width/2.0d;
//        //Warning Here width suppose a square reconstruction
//
//        double value;
//        int i,k;
////        int y,x,i;
//        //For loop variable
//
//        int [] weighting = new int[realProjection.length];
//        float[] computedProjection =  new float[realProjection.length];
//    public float[] projection2(float[] reconstruction, double theta, float[] realProjection ){

    public boolean isDoBGART() {
        return doBGART;
    }

    public void setDoBGART(boolean doBGART) {
        this.doBGART = doBGART;
    }

    public float getK() {
        return k;
    }

    public void setK(float k) {
        this.k = k;
    }

    @Override
    public String getFunctionName() {
        return "Algebraic reconstruction";
    }

    @Override
    public float getErrorValue() {
        return -1;
    }
}
