/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model;


import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStackFactory;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoInit.SortedPanel;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by antoine bergamaschi on 14/01/14.
 * Singleton
 * Classe used to save information entered by the user about a Hdf5. One file should be created for each project.
 * After Creating one file, user do not have to look for the hdf5 file but can directly open this file.
 */

@XmlRootElement
public class SessionObject {

    public enum DataType{
        IMAGE,SPECTRUM,MOTOR,XBPM;
    }

    private static SessionObject instance = null;
    private String pathToSessionObject = null;
    //If there is a currentSessionObject and the Hdf5Path is Set,
    //Then isOpen = true
    private static boolean isOpen = false;

    //Interface parameters
    private int coordinateDisplay = RXTomoJViewController.POSITION_CARTESIAN;
    private ArrayList<StackType> automatedComputation = StackType.getBasicType();

    private String pathToHdf5 = "";
    private String pathToSavingFolder = "";

    private HashMap<DataType,ArrayList<String>> dataset = new HashMap<>();
    //Warper class used to fill HashMap dataset because Unmarshal didn't work on HashMap with ArrayList
    private HashMap<DataType,String[]> sessionDataset = new HashMap<>();

    private AnalyseFactory analyseFactory = new AnalyseFactory();


    //Constructor
    private SessionObject(){
        super();
    }

    public AnalyseFactory getAnalyseFactory() {
        return analyseFactory;
    }

    public void setAnalyseFactory(AnalyseFactory analyseFactory) {
        this.analyseFactory = analyseFactory;
    }


    public HashMap<DataType,ArrayList<String>> getDataset(){
        return dataset;
    }

    public void setSessionDataset(HashMap<DataType, String[]> sessionDataset) {
        this.sessionDataset = sessionDataset;
        //Fill dataset
        this.dataset = new HashMap<>();
        for ( DataType type : sessionDataset.keySet() ){
            List t = Arrays.asList((String[])sessionDataset.get(type));
            dataset.put(type, new ArrayList<>(t));
        }

    }

    public HashMap<DataType, String[]> getSessionDataset() {
        //Fill sessionDataset
        this.sessionDataset = new HashMap<>();

        for ( DataType type : dataset.keySet() ){
            String[] t = new String[dataset.get(type).size()];
            dataset.get(type).toArray(t);
            sessionDataset.put(type, t);
        }

        return sessionDataset;
    }

    public void setPathToSessionObject(String path){
        this.pathToSessionObject = path;
    }

    public String getPathToHdf5() {
        return pathToHdf5;
    }

    public void setPathToHdf5(String pathToHdf5) {
        this.pathToHdf5 = pathToHdf5;
    }

    public String getPathToSavingFolder() {
        return pathToSavingFolder;
    }

    public void setPathToSavingFolder(String pathToSavingFolder) {
        this.pathToSavingFolder = pathToSavingFolder;
    }

    public int getCoordinateDisplay() {
        return coordinateDisplay;
    }

    public void setCoordinateDisplay(int coordinateDisplay) {
        this.coordinateDisplay = coordinateDisplay;
    }

    public ArrayList<StackType> getAutomatedComputation() {
        return automatedComputation;
    }

    public void setAutomatedComputation(ArrayList<StackType> automatedComputation) {
        this.automatedComputation = automatedComputation;
    }

    /**
     * Basic method used to retrieve the current SessionObject which is always supposed unique
     * @return SessionObject, the current SessionObject
     * @see SessionObject
     */
    public static SessionObject getCurrentSessionObject(){
        isOpen = false;
        if ( instance != null ){
            if ( instance.getPathToHdf5().length() > 0){
                isOpen = true;
            }
            return instance;
        }else{
            instance = new SessionObject();
            return instance;
        }
    }

    /**
     * Factory to create a new SessionObject from scratch
     * @return SessionObject
     */
    public static SessionObject createSessionObject(){
        isOpen = false;
        instance = new SessionObject();
        isOpen = true;
        return instance;
    }


    public static boolean isOpen(){
        return isOpen;
    }

    /**
     * Close the current Session object and release associated resources
     * @return String, the path to the closed SessionObject
     */
    public static String closeCurrentSessionObject(){
        String pathToOldSession = null;
        if ( isOpen && instance != null ){
            pathToOldSession = SessionObject.getCurrentSessionObject().pathToSessionObject;
            SessionObject.save();

            //Free ressources linked with this SessionObject
            SessionObject.releaseResources();
        }
        isOpen = false;
        instance = null;
        return pathToOldSession;
    }

    /**
     * Clean the current Session object from every stored parameters
     */
    private static void releaseResources(){
        if ( instance != null ){
            instance.setAnalyseFactory(new AnalyseFactory());
            instance.getAutomatedComputation().clear();
            instance.getSessionDataset().clear();
            instance.setPathToSessionObject(null);
            instance.setPathToHdf5(null);
            instance.setPathToSavingFolder(null);
        }
    }

    //TODO verify the content of the retrieved session object, did this contains real values?
    /**
     * Get an instance of SessionObject with the path to a XML sessionObject file
     * @param pathToFile String, path to the sessionObject XML file
     */
    public static void open(String pathToFile){
        open(new File(pathToFile));
    }

    /**
     * Get an instance of SessionObject with a XML sessionObject file
     * @param file File, the XML session Object file
     */
    public static void open(File file){
        SessionObject sessionObject = null;

        try {
            JAXBContext jaxbContext  =  JAXBContext.newInstance(SessionObject.class);

            Unmarshaller jaxUnmarshaller = jaxbContext.createUnmarshaller();
//            Schema schema = jaxUnmarshaller.getSchema();
            sessionObject = (SessionObject) jaxUnmarshaller.unmarshal(file);

        } catch (JAXBException e) {
            e.printStackTrace();
        }

        //Create Path to SessionObject
        sessionObject.setPathToSessionObject(file.getAbsolutePath());

        //Create the new Path to the Saving folder
        sessionObject.setPathToSavingFolder(file.getParentFile().getAbsolutePath());

        instance = sessionObject;
        isOpen = true;
    }


    /**
     * Get an instance of SessionObject with the path to a XML sessionObject file
     * @param pathToFile String, path to the sessionObject XML file
     */
    public static SessionObject getSession(String pathToFile){
        return  getSession(new File(pathToFile));
    }

    /**
     * Get an instance of SessionObject with a XML sessionObject file
     * @param file File, the XML session Object file
     */
    public static SessionObject getSession(File file){
        SessionObject sessionObject = null;

        try {
            JAXBContext jaxbContext  =  JAXBContext.newInstance(SessionObject.class);

            Unmarshaller jaxUnmarshaller = jaxbContext.createUnmarshaller();
//            Schema schema = jaxUnmarshaller.getSchema();
            sessionObject = (SessionObject) jaxUnmarshaller.unmarshal(file);

        } catch (JAXBException e) {
            e.printStackTrace();
        }

        //Create Path to SessionObject
        sessionObject.setPathToSessionObject(file.getAbsolutePath());

        //Create the new Path to the Saving folder
        sessionObject.setPathToSavingFolder(file.getParentFile().getAbsolutePath());

        return sessionObject;
    }


    /**
     * Write the current SessionObject on the disk
     */
    public static void save(){
        SessionObject sessionObject = SessionObject.getCurrentSessionObject();
        if ( sessionObject.pathToSessionObject != null ){
            sessionObject.writteXML(null);
        }
    }

    private void writteXML(String fileName){
        File file;
        try{
            //If null save in the private path use to get this SessionObject
            if ( fileName == null ){
                file = new File(this.pathToSessionObject);
            }
            //Else remove the file is one in this path and save the new SessionObject  .xml file
            else{
                file  = new File(fileName);
            }

            if ( canWrite() ){
                //Create Path to SessionObject
                this.instance.setPathToSessionObject(file.getAbsolutePath());

                //Create the new Path to the Saving folder
                this.instance.setPathToSavingFolder(file.getParentFile().getAbsolutePath());

                JAXBContext jaxbContext = JAXBContext.newInstance(SessionObject.class);
                Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

                // output pretty printed
                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                //
                //            jaxbMarshaller.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION, "sessionObject.xsd");

                jaxbMarshaller.marshal(this.instance, file);

                System.out.println("Save in : "+this.pathToSessionObject);

                //Reset hasBeenUpdated
            }else{
                System.err.println("Cannot write XML"+this.getClass().toString());
            }

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public boolean canWrite(){
        if ( this.pathToSessionObject != null ){
            File file = new File(this.pathToSessionObject);
            if ( file.exists() ){
                //If this is a file, the file will be overrided
                if ( file.isFile() && file.canWrite() ){
                    return true;
                }
            }
            //If this is a new file, it should be set in a Directory with right to write permission
            else if ( file.getParentFile().exists() && file.getParentFile().isDirectory() && file.getParentFile().canWrite() ){
                return true;
            }
        }
        return false;
    }


    public String getPathToSessionObject() {
        return pathToSessionObject;
    }
}

