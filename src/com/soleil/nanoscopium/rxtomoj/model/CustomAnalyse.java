/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model;

import com.soleil.nanoscopium.rxtomoj.model.data.DataStorage;
import com.soleil.nanoscopium.rxtomoj.model.data.ParameterStorage;
import com.soleil.nanoscopium.rxtomoj.model.data.VolatileStorage;
import com.soleil.nanoscopium.rxtomoj.model.filters.BackgroundSubtraction;

import java.util.ArrayList;
import java.util.HashMap;

import static com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.MotorType;
import static com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.OrientationType;

/**
 * Created by bergamaschi on 03/02/14.
 */
public class CustomAnalyse {

    /**
     * Every ComputeSpot, ReconstructionFunction (algebraic and Fourrier) and
     */
    private ParameterStorage parameterStorage = new ParameterStorage();

    /**
     * Save the path to the data already saved on disk
     */
    private DataStorage dataStorage = new DataStorage();

    /**
     * Save of the data in volatile memory
     */
    private VolatileStorage volatileStorage;

//    private BackgroundSubtraction backgroundSubtraction = new BackgroundSubtraction();

    private String datasetName = "";

//    private ShiftImage shiftImage = new ShiftImage();
    private ArrayList<String> SAInames = new ArrayList<>();
    private OrientationType dimension = OrientationType.XYZ;
    private HashMap<MotorType,String> motorPath = new HashMap<>();

    /**
     * Down or oversampling for reconstruction processus
     */
    private float reconstructionSampling = 1;

    /**
     * Test if the shift has to be done or not
     */
    private boolean useSinusoidalShift = true;

    public CustomAnalyse (){
        volatileStorage = new VolatileStorage();
    }

    /**
     * Do a Hard copy of this customAnalyse
     * @param customAnalyse the Old custom analyse to be replaced
     */
    public CustomAnalyse (CustomAnalyse customAnalyse){
        super();
        //Copy the CustomAnalyse
        this.setSAInames(new ArrayList<>(customAnalyse.getSAInames()));
//        this.setBackgroundSubtraction(new BackgroundSubtraction(customAnalyse.getBackgroundSubtraction()));
        this.setParameterStorage(new ParameterStorage(customAnalyse.getParameterStorage()));
//        this.setShiftImage(new ShiftImage(customAnalyse.getShiftImage()));
        this.setMotorPath(new HashMap<>(customAnalyse.getMotorPath()));
        this.setDataStorage(new DataStorage(customAnalyse.getDataStorage()));


        this.datasetName  =  customAnalyse.getDatasetName();
        this.volatileStorage = new VolatileStorage();
        this.dimension = customAnalyse.getDimension();
    }

    public ArrayList<String> getSAInames() {
        return SAInames;
    }

    public void setSAInames(ArrayList<String> SAInames) {
        this.SAInames = SAInames;
    }

    public String getDatasetName() {
        return datasetName;
    }

    public void setDatasetName(String datasetName) {
        this.datasetName = datasetName;
    }

    public HashMap<MotorType, String> getMotorPath() {
        return motorPath;
    }

    public void setMotorPath(HashMap<MotorType, String> motorPath) {
        //Add Warning/Errors message when the resource is not found
//        for ( MotorType motorType : motorPath.keySet() ){
//
//        }
        this.motorPath = motorPath;
    }

    public float getReconstructionSampling() {
        return reconstructionSampling;
    }


    public void setReconstructionSampling(float reconstructionSampling) {
        this.reconstructionSampling = reconstructionSampling;
    }

//    public BackgroundSubtraction getBackgroundSubtraction() {
//        return backgroundSubtraction;
//    }

//    public void setBackgroundSubtraction(BackgroundSubtraction backgroundSubtraction) {
//        this.backgroundSubtraction = backgroundSubtraction;
//    }

    public ParameterStorage getParameterStorage() {
        return parameterStorage;
    }

    public void setParameterStorage(ParameterStorage parameterStorage) {
        this.parameterStorage = parameterStorage;
    }

    public VolatileStorage getVolatileStorage(){
        return volatileStorage;
    }

    public boolean isUseSinusoidalShift() {
        return useSinusoidalShift;
    }

    public void setUseSinusoidalShift(boolean useSinusoidalShift) {
        this.useSinusoidalShift = useSinusoidalShift;
    }

    public OrientationType getDimension() {
        return dimension;
    }

    public void setDimension(OrientationType dimension) {
        this.dimension = dimension;
    }

    public DataStorage getDataStorage() {
        return dataStorage;
    }

    public void setDataStorage(DataStorage dataStorage) {
        this.dataStorage = dataStorage;
    }

}

