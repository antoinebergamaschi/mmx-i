/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.utils;

import com.soleil.nanoscopium.rxtomoj.model.data.StackData;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by bergamaschi on 26/11/2015.
 */
public class StackDataStorageAdaptater extends XmlAdapter<MapStackDataElements[], ConcurrentHashMap<StackType,ArrayList<StackData>>> {
    @Override
    public ConcurrentHashMap<StackType, ArrayList<StackData>> unmarshal(MapStackDataElements[] v) throws Exception {
        ConcurrentHashMap<StackType,ArrayList<StackData>> map = new ConcurrentHashMap<>();
        for ( MapStackDataElements mapStackDataElements : v){
            map.put(mapStackDataElements.key,mapStackDataElements.value);
        }

        return map;
    }

    @Override
    public MapStackDataElements[] marshal(ConcurrentHashMap<StackType, ArrayList<StackData>> v) throws Exception {
        MapStackDataElements[] mapStackDataElementses = new MapStackDataElements[v.size()];
        int i = 0;
        for ( Map.Entry<StackType,ArrayList<StackData>> entry : v.entrySet() ){
            mapStackDataElementses[i] = new MapStackDataElements(entry.getKey(),entry.getValue());
            i++;
        }

        return mapStackDataElementses;
    }
}

class MapStackDataElements{
    @XmlElement
    public StackType key;
    @XmlElement
    public ArrayList<StackData> value;

    public MapStackDataElements(){}

    public MapStackDataElements(StackType key,ArrayList<StackData> value){
        this.key = key;
        this.value = value;
    }
}
