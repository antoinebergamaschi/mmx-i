/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.utils;

import com.soleil.nanoscopium.rxtomoj.model.computationFunction.BasicComputationFunction;

/**
 * Created by bergamaschi on 11/07/14.
 */
public class ThreadBasicComputation extends Thread {
        private BasicComputationFunction function = null;
        private float[] data;
        private int width;
        private int height;
        private int size;
        private int position;
        private int slice;

        protected ThreadBasicComputation(BasicComputationFunction function,final float[] data, int width, int height, int size, int position, int slice){
            super("ThreadBasicComputation-"+slice+"-"+position);
            this.function = function;
            this.data = data;
            this.width = width;
            this.height = height;
            this.size = size;
            this.position = position;
            this.slice = slice;
        }

        @Override
        public void run() {
            this.function._compute(data, width,  height, size, position, slice);
            //Realize the data table
            data = null;
            this.function.clearMemory();
        }

}
