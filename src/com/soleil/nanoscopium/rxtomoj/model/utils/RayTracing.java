/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.utils;

/**
 * Created by bergamaschi on 27/09/2015.
 */
public abstract class RayTracing {
    enum Direction{
        NORTH_EST, NORTH_WEST, SOUTH_WEST, SOUTH_EST, NORTH, SOUTH, EST, WEST
    }

    final protected double[] currentPosition = new double[2];
    final protected int[] integerPosition = new int[2];
    protected double coefDirector;
    protected double ordinateToOrigin;
    final protected double[] computedStep = new double[1];
    final protected double[] origin = new double[2];

    protected Direction direction;

    /**
     * Get the Next Position of the current RayTracing Object
     * This function must Compute the NextPosition according to Theta and also compute the next Step
     * in the same direction ( thus calling nextPosition correctly compute the computedStep at each Position )
     */
    public abstract void getNextPosition();

    /**
     * Get the Previous Position of the current RayTracing Object
     */
    public abstract void getPreviousPosition();

    public void init(double theta, double[] currentPosition){

        //Compute Coef director and ordinate to origin
//        coefDirector = ComputationUtils.tanR(theta,ComputationUtils.DEFAULT_ROUDING);

        coefDirector = Math.tan(-theta);

        initDirection(theta);

        if ( currentPosition != null ) {
            System.arraycopy(currentPosition,0,this.currentPosition,0,2);

//            integerPosition[0] = (int) currentPosition[0];
//            integerPosition[1] = (int) currentPosition[1];

            switch (direction){
                case NORTH_EST:
                    integerPosition[0] = (int) Math.ceil(currentPosition[0]);
                    integerPosition[1] = (int) Math.floor(currentPosition[1]);
                    break;
                case SOUTH_EST:
                    integerPosition[0] = (int) Math.ceil(currentPosition[0]);
                    integerPosition[1] = (int) Math.ceil(currentPosition[1]);
                    break;
                case NORTH_WEST:
                    integerPosition[0] = (int) Math.floor(currentPosition[0]);
                    integerPosition[1] = (int) Math.floor(currentPosition[1]);
                    break;
                case SOUTH_WEST:
                    integerPosition[0] = (int) Math.floor(currentPosition[0]);
                    integerPosition[1] = (int) Math.ceil(currentPosition[1]);
                    break;
                case NORTH:
                    integerPosition[0] = (int) Math.floor(currentPosition[0]);
                    integerPosition[1] = (int) Math.floor(currentPosition[1]);
                    break;
                case WEST:
                case SOUTH:
                case EST:
                    integerPosition[0] = (int) Math.ceil(currentPosition[0]);
                    integerPosition[1] = (int) Math.ceil(currentPosition[1]);
                    break;
            }

            ordinateToOrigin = 0;

            this.origin[0] = integerPosition[0];
            this.origin[1] = integerPosition[1];

//            System.arraycopy(currentPosition,0,this.origin,0,2);

            initFirstStep();
        }

    }


    private void initDirection(double theta){
        double tmpTheta = theta;

        //Init the Square position
        if ( tmpTheta / (2*Math.PI) > 1 ){
            tmpTheta -= (int)(tmpTheta/(2*Math.PI))*(2*Math.PI);
        }

        //Add uncertainty ? in rad
        double uncertainty = 0.001;

        if ( tmpTheta < 0 + uncertainty && tmpTheta > 0 - uncertainty ){
            direction = Direction.EST;
        }else if (tmpTheta < Math.PI/2 + uncertainty && tmpTheta > Math.PI/2 - uncertainty ){
            direction = Direction.NORTH;
        }else if ( tmpTheta < Math.PI + uncertainty && tmpTheta > Math.PI - uncertainty){
            direction = Direction.WEST;
        }else if ( tmpTheta < 3*Math.PI/2 + uncertainty && tmpTheta > 3*Math.PI/2 - uncertainty){
            direction = Direction.SOUTH;
        }
//        if ( tmpTheta == 0 ){
//            direction = Direction.EST;
//        }else if (tmpTheta == Math.PI/2 ){
//            direction = Direction.NORTH;
//        }else if ( tmpTheta == Math.PI ){
//            direction = Direction.WEST;
//        }else if ( tmpTheta == 3*Math.PI/2 ){
//            direction = Direction.SOUTH;
//        }
        else if ( tmpTheta > 0 && tmpTheta < Math.PI/2 ){
            direction = Direction.NORTH_EST;
        }else if ( tmpTheta > Math.PI/2 && tmpTheta <= Math.PI ){
            direction = Direction.NORTH_WEST;
        }else if ( tmpTheta > Math.PI && tmpTheta <= 3*(Math.PI/2) ){
            direction = Direction.SOUTH_WEST;
        }else{
            direction = Direction.SOUTH_EST;
        }
    }

    abstract void initFirstStep();

    public double[] getComputedStep(){
        return this.computedStep;
    }

    public double[] getCurrentPosition(){
        return currentPosition;
    }

    public int[] getIntegerPosition(){
        return integerPosition;
    }


    
}
