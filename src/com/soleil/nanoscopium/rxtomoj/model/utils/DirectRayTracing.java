/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.utils;

import java.util.Arrays;

/**
 * Created by bergamaschi on 27/09/2015.
 */
public class DirectRayTracing extends RayTracing {
    private double ecart = 1;
    private double minimalDistance = 0.001;

    public void setMaximalDistance(double maximalDistance) {
        this.maximalDistance = maximalDistance;
    }

    private double maximalDistance = Math.sqrt(2);
    private boolean isFirst = false;

    @Override
    public void getNextPosition() {

        if ( isFirst ){
            //Test
            computedStep[0] = 1;
            isFirst = false;
            return;
        }

        //Set the 0 position on the integer current position
        currentPosition[0] -= origin[0];
        currentPosition[1] -= origin[1];

//        currentPosition[0] = integerPosition[0] - origin[0];
//        currentPosition[1] = integerPosition[1] - origin[1];
        double[] computedPosition = computeNextPosition();

        //Compute Step
        computedStep[0] = Math.sqrt((currentPosition[0] - computedPosition[0]) * (currentPosition[0] - computedPosition[0]) + (currentPosition[1] - computedPosition[1]) * (currentPosition[1] - computedPosition[1]));
//        computedStep[0] = Math.sqrt(pos[0]*pos[0] + pos[1]*pos[1]);


        if( computedStep[0] <= minimalDistance ){
            ecart+=0.1;
            //Reset Ori
            currentPosition[0] += origin[0];
            currentPosition[1] += origin[1];

            getNextPosition();
        }
        else if ( computedStep[0] > maximalDistance ){
            computedStep[0] = maximalDistance;
            addPosition(computedPosition);
        }
        else{
            addPosition(computedPosition);
            //Reset ecart
            ecart = 1;
        }



        //Shift is used here because the ordinate repaire change with the orientation
        //Shift the Integer position
        switch (direction){
            case NORTH_EST:
                integerPosition[0] = (int) Math.ceil(currentPosition[0]);
                integerPosition[1] = (int) Math.floor(currentPosition[1]);
                break;
            case SOUTH_EST:
                integerPosition[0] = (int) Math.ceil(currentPosition[0]);
                integerPosition[1] = (int) Math.ceil(currentPosition[1]);
                break;
            case NORTH_WEST:
                integerPosition[0] = (int) Math.floor(currentPosition[0]);
                integerPosition[1] = (int) Math.floor(currentPosition[1]);
                break;
            case SOUTH_WEST:
                integerPosition[0] = (int) Math.floor(currentPosition[0]);
                integerPosition[1] = (int) Math.ceil(currentPosition[1]);
                break;
            case NORTH:
                integerPosition[0] = (int) Math.floor(currentPosition[0]);
                integerPosition[1] = (int) Math.floor(currentPosition[1]);
                break;
            case WEST:
            case SOUTH:
            case EST:
                integerPosition[0] = (int) Math.ceil(currentPosition[0]);
                integerPosition[1] = (int) Math.ceil(currentPosition[1]);
                break;
        }
    }

    @Override
    public void getPreviousPosition() {
//        double[] pos = ComputationUtils.computeIntersectionLineInSquare(Math.ceil(currentPosition[0])-currentPosition[0], Math.ceil(currentPosition[1])-currentPosition[1], theta, square[0], square[1], square[2], square[3]);
//        currentPosition[0] += pos[2];
//        currentPosition[1] += pos[3];
    }

    @Override
    void initFirstStep() {
        double[] tmpCurrentPosition = new double[2];
        tmpCurrentPosition[0] = currentPosition[0];
        tmpCurrentPosition[1] = currentPosition[1];

        currentPosition[0] = 0;
        currentPosition[1] = 0;

        double[] computedPosition = computeNextPosition();

        //Compute Step
        computedStep[0] = Math.sqrt((computedPosition[0] * computedPosition[0]) + (computedPosition[1] * computedPosition[1]));
//        computedStep[0] = 1;
        //Rest CurrentPosition
        currentPosition[0] = tmpCurrentPosition[0];
        currentPosition[1] = tmpCurrentPosition[1];

//        switch (direction){
//            case NORTH_EST:
//                integerPosition[0] = (int) Math.ceil(currentPosition[0]);
//                integerPosition[1] = (int) Math.floor(currentPosition[1]);
//                break;
//            case SOUTH_EST:
//                integerPosition[0] = (int) Math.ceil(currentPosition[0]);
//                integerPosition[1] = (int) Math.ceil(currentPosition[1]);
//                break;
//            case NORTH_WEST:
//                integerPosition[0] = (int) Math.floor(currentPosition[0]);
//                integerPosition[1] = (int) Math.floor(currentPosition[1]);
//                break;
//            case SOUTH_WEST:
//                integerPosition[0] = (int) Math.floor(currentPosition[0]);
//                integerPosition[1] = (int) Math.ceil(currentPosition[1]);
//                break;
//            case NORTH:
//                integerPosition[0] = (int) Math.floor(currentPosition[0]);
//                integerPosition[1] = (int) Math.floor(currentPosition[1]);
//                break;
//            case WEST:
//            case SOUTH:
//            case EST:
//                integerPosition[0] = (int) Math.ceil(currentPosition[0]);
//                integerPosition[1] = (int) Math.ceil(currentPosition[1]);
//                break;
//        }
    }

    private void addPosition(double[] posToAdd){
        currentPosition[0] = posToAdd[0]+origin[0];
        currentPosition[1] = posToAdd[1]+origin[1];

//        currentPosition[0] = ComputationUtils.round(currentPosition[0]);
//        currentPosition[1] = ComputationUtils.round(currentPosition[1]);
    }


    private double[] computeNextPosition(){
        double[] nextPosition = new double[2];
        double[] tmpNext = {Double.POSITIVE_INFINITY,Double.POSITIVE_INFINITY};

        double ka = (currentPosition[1]);
        double kb = (currentPosition[0]);

        double kb1 = (int) (kb + ecart);
        double kb2 = (int) (kb - ecart);

        double ka1 = (int) (ka - ecart);
        double ka2 = (int) (ka + ecart);

        switch (direction){
            case NORTH:
                if ( Math.abs(ka) > Math.abs(currentPosition[1]) ){
                    nextPosition = ComputationUtils.computeIntersectionLineH(coefDirector, ordinateToOrigin, ka);
                }else{
                    nextPosition = ComputationUtils.computeIntersectionLineH(coefDirector, ordinateToOrigin, ka1);
                }
                break;
            case SOUTH:
                if ( Math.abs(ka) > Math.abs(currentPosition[1]) ){
                    nextPosition = ComputationUtils.computeIntersectionLineH(coefDirector, ordinateToOrigin, ka);
                }else{
                    nextPosition = ComputationUtils.computeIntersectionLineH(coefDirector, ordinateToOrigin, ka2);
                }
                break;
            case EST:
                if ( Math.abs(kb) > Math.abs(currentPosition[0]) ){
                    nextPosition = ComputationUtils.computeIntersectionLineV(coefDirector, ordinateToOrigin, kb);
                }else{
                    nextPosition = ComputationUtils.computeIntersectionLineV(coefDirector, ordinateToOrigin, kb1);
                }
                break;
            case WEST:
                if ( Math.abs(kb) > Math.abs(currentPosition[0]) ){
                    nextPosition = ComputationUtils.computeIntersectionLineV(coefDirector, ordinateToOrigin, kb);
                }else{
                    nextPosition = ComputationUtils.computeIntersectionLineV(coefDirector, ordinateToOrigin, kb2);
                }
                break;
            case NORTH_EST:
                if ( Math.abs(ka) > Math.abs(currentPosition[1]) ){
                    tmpNext = ComputationUtils.computeIntersectionLineH(coefDirector, ordinateToOrigin, ka);
                }else{
                    tmpNext = ComputationUtils.computeIntersectionLineH(coefDirector, ordinateToOrigin, ka1);
                }

                if ( Math.abs(kb) > Math.abs(currentPosition[0]) ){
                    nextPosition = ComputationUtils.computeIntersectionLineV(coefDirector, ordinateToOrigin, kb);
                }else{
                    nextPosition = ComputationUtils.computeIntersectionLineV(coefDirector, ordinateToOrigin, kb1);
                }
                break;
            case NORTH_WEST:
                if ( Math.abs(ka) > Math.abs(currentPosition[1]) ){
                    tmpNext = ComputationUtils.computeIntersectionLineH(coefDirector, ordinateToOrigin, ka);
                }else{
                    tmpNext = ComputationUtils.computeIntersectionLineH(coefDirector, ordinateToOrigin, ka1);
                }

                if ( Math.abs(kb) > Math.abs(currentPosition[0]) ){
                    nextPosition = ComputationUtils.computeIntersectionLineV(coefDirector, ordinateToOrigin, kb);
                }else{
                    nextPosition = ComputationUtils.computeIntersectionLineV(coefDirector, ordinateToOrigin, kb2);
                }

                break;
            case SOUTH_EST:
                if ( Math.abs(ka) > Math.abs(currentPosition[1]) ){
                    tmpNext = ComputationUtils.computeIntersectionLineH(coefDirector, ordinateToOrigin, ka);
                }else{
                    tmpNext = ComputationUtils.computeIntersectionLineH(coefDirector, ordinateToOrigin, ka2);
                }

                if ( Math.abs(kb) > Math.abs(currentPosition[0]) ){
                    nextPosition = ComputationUtils.computeIntersectionLineV(coefDirector, ordinateToOrigin, kb);
                }else{
                    nextPosition = ComputationUtils.computeIntersectionLineV(coefDirector, ordinateToOrigin, kb1);
                }

                break;
            case SOUTH_WEST:
                if ( Math.abs(ka) > Math.abs(currentPosition[1]) ){
                    tmpNext = ComputationUtils.computeIntersectionLineH(coefDirector, ordinateToOrigin, ka);
                }else{
                    tmpNext = ComputationUtils.computeIntersectionLineH(coefDirector, ordinateToOrigin, ka2);
                }

                if ( Math.abs(kb) > Math.abs(currentPosition[0]) ){
                    nextPosition = ComputationUtils.computeIntersectionLineV(coefDirector, ordinateToOrigin, kb);
                }else{
                    nextPosition = ComputationUtils.computeIntersectionLineV(coefDirector, ordinateToOrigin, kb2);
                }

                break;
        }

        if ( tmpNext[0]*tmpNext[0] + tmpNext[1]*tmpNext[1] < nextPosition[0]*nextPosition[0] + nextPosition[1]*nextPosition[1] ){
            nextPosition = tmpNext;
        }

        return nextPosition;
    }
}
