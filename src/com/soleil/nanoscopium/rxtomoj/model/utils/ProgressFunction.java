/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.utils;

/**
 * Created by bergamaschi on 08/04/2014.
 */
public interface ProgressFunction {
    /**
     * get the work that has already be done
     * @return float, number between 0-1  representing the percent of work done
     */
    float getWorkDone();

    /**
     * Get the name of the currently monitored function
     * @return String, the name of the monitored function
     */
    String getFunctionName();

    /**
     * Get the error value in case of following iterative function
     * @return float, the currently computed error in an iterative process
     */
    float getErrorValue();

    /**
     * Same as getWorkDone but with a nice strung output
     * @return String
     */
    String getWorkDoneString();


    void setNumberOfWorks(float numberOfWorks);

    /**
    * Stop the current ProgressFunction, abort all process
    */
    void abort();

    /**
     * Stop the current ProgressFunction
     */
    void hardStop();

}
