/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.utils;

import ij.measure.ResultsTable;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Antoine Bergamaschi
 */
@Deprecated
public class SaveData {


    public static void saveRawData(float[] data, String name, String path){
//        new ImageJ();
        ResultsTable resultsTable = new ResultsTable();
       for ( int k = 0; k < data.length ; k++ ){
           resultsTable.incrementCounter();
           resultsTable.addValue(name,data[k]);
       }

        try {
//            resultsTable.show("test");
            resultsTable.saveAs(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveRawData(double[] data, String name, String path){
//        new ImageJ();
        ResultsTable resultsTable = new ResultsTable();
        for ( int k = 0; k < data.length ; k++ ){
            resultsTable.incrementCounter();
            resultsTable.addValue(name,data[k]);
        }

        try {
//            resultsTable.show("test");
            resultsTable.saveAs(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void saveRawData(ArrayList<double[]> data, ArrayList<String> name, String path){
        int maxLength = -1;
        ResultsTable resultsTable = new ResultsTable();

        for ( int k = 0; k < data.size() ; k++ ){
            double[] array = data.get(k);
            if ( array.length > maxLength ){
                maxLength = array.length;
            }
        }

        for ( int i = 0; i < maxLength ; i++ ){
            resultsTable.incrementCounter();
            for ( int k = 0; k < data.size() ; k++ ) {
                double[] array = data.get(k);
                String currentName = name.get(k);
                if ( i < array.length ) {
                    resultsTable.addValue(currentName, array[i]);
                }else{
                    resultsTable.addValue(currentName, 0);
                }
            }
        }

        try {
            resultsTable.saveAs(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
