/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.utils;

import com.soleil.nanoscopium.rxtomoj.model.filters.HotSpot_Detection;

/**
 * Created by antoine bergamaschi on 03/03/14.
 */
@Deprecated
public class ThreadHotSpot implements Runnable {

    private HotSpot_Detection hotSpot_detection = null;
    float[] data = null;
    private int position;

    public ThreadHotSpot(HotSpot_Detection hotSpot_detection, int position,float[] data){
        this.hotSpot_detection = hotSpot_detection;
        this.position = position;
        this.data = data;
    }

    @Override
    public void run() {
        this.hotSpot_detection.run(position,data);
    }
}
