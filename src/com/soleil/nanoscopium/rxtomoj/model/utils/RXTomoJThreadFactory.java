/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.utils;

import com.soleil.nanoscopium.rxtomoj.model.computationFunction.BasicComputationFunction;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryUsage;
import java.util.ArrayList;
import java.util.concurrent.*;

/**
 * Created by antoine bergamaschi on 03/03/14.
 */
public class RXTomoJThreadFactory {
    private static ExecutorService basicComputationExecutorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    private static ExecutorService lambdaThreadExecutorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    private static boolean isBasicComputationExecutorWorking = true;
    private static boolean isLambdaThreadExecutorWorking = true;

    final private static ArrayList<Future<?>> listOfBasicComputationFuture = new ArrayList<>();
    final private static ArrayList<Future<?>> listOfLambdaThreadFuture = new ArrayList<>();

    //Do not instantiate this Class
    private RXTomoJThreadFactory(){

    }

    /**
     * Create the Basic Computation Thread with the correcponding parameters.
     * Use the current ExecutorService is not shutDown.
     * @param function
     * @param data
     * @param width
     * @param height
     * @param size
     * @param position
     * @param slice
     * @return
     */
    public static boolean createThreadBasicComputation(BasicComputationFunction function,final float[] data, int width, int height, int size, int position, int slice){
        if (isBasicComputationExecutorWorking) {
            //If the basicComputationExecutorService is shutDown
            if (basicComputationExecutorService.isShutdown() || basicComputationExecutorService.isTerminated() ) {
                basicComputationExecutorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
            }

            listOfBasicComputationFuture.add(basicComputationExecutorService.submit(new ThreadBasicComputation(function, data, width, height, size, position, slice)));
        }
        return true;
//        return listOfJobs.add(new ThreadBasicComputation(function, data, width, height,size,position,slice));
    }

    /**
     * Create the Basic Computation Thread with the correcponding parameters.
     * Use the current ExecutorService is not shutDown.
     * @param function The Lambda function to embed and launch in a new Thread
     * @return True if the Thread has correctly been added to the ThreadPool
     */
    public static boolean createThreadLambdaComputation(LambdaThreadFunction function){
        if (isBasicComputationExecutorWorking) {
            //If the basicComputationExecutorService is shutDown
            if (lambdaThreadExecutorService.isShutdown() || lambdaThreadExecutorService.isTerminated() ) {
                lambdaThreadExecutorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
            }
            return listOfLambdaThreadFuture.add(lambdaThreadExecutorService.submit(new ThreadLambdaComputation(function)));
        }
        return false;
    }



    /**
     * Avoid ShutDown of the Executor service by calling Future jobs
     * @param listOfJobs
     */
    public static void exec(ArrayList<Thread> listOfJobs) {
        int i;
        Future[]  future = new Future[listOfJobs.size()];

        isBasicComputationExecutorWorking = true;

        for ( i = 0 ; i < listOfJobs.size() ; i++ ){
            future[i] = basicComputationExecutorService.submit(listOfJobs.get(i));
        }

        i=0;
        while (i < future.length){
            if ( future[i].isDone() ){
                i++;
            }else{
                try {
                    TimeUnit.MILLISECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Compute if the thread can be safely launch depending on the remaining available memory
     * @param memoryTobeUsed The amount of memory to be used by the next thread
     * @return The time waited before the memory have been made available
     */
    public static long checkMemory(long memoryTobeUsed){
        //Check memory
        MemoryUsage heap = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage();
        long[] time = new long[1];
        //Take less than the max heap size to give breath to other process
        while ((heap.getMax()*0.8)-heap.getUsed() < memoryTobeUsed ){
            try {
                heap = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage();
                TimeUnit.SECONDS.sleep(1);
                System.gc();
                if ( time[0] == TimeUnit.MINUTES.toMillis(3) ){
                    return -1;
                }else{
                    time[0] += TimeUnit.SECONDS.toMillis(1);
                }
//                System.err.println("No more mem");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return time[0];
    }


    public static boolean isRunning(){
        return isBasicComputationExecutorWorking;
    }

    /**
     * Safely shut down the current ThreadPool
     */
    public static void terminateBasicComputation(){
        basicComputationExecutorService.shutdown();
        isBasicComputationExecutorWorking = false;


        int size = listOfBasicComputationFuture.size();
        while ( size > 0 ){
            try {
                listOfBasicComputationFuture.get(0).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }finally {
                listOfBasicComputationFuture.remove(0);
                size--;
            }

        }


        basicComputationExecutorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        isBasicComputationExecutorWorking = true;
    }

    /**
     * Safely shut down the current ThreadPool
     */
    public static void terminateLambdaThread(){
        lambdaThreadExecutorService.shutdown();
        isLambdaThreadExecutorWorking = false;


        int size = listOfLambdaThreadFuture.size();
        while ( size > 0 ){
            try {
                listOfLambdaThreadFuture.get(0).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }finally {
                listOfLambdaThreadFuture.remove(0);
                size--;
            }

        }

        lambdaThreadExecutorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        isLambdaThreadExecutorWorking = true;
    }
}
