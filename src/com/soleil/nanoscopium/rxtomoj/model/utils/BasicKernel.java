/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.utils;

/**
 * Created by bergamaschi on 28/09/2015.
 */
public enum BasicKernel {
    MEAN,MEDIAN;

    float[] kernel;
    int[] dims;

    BasicKernel(){

    }

    public float compute(int posX, int posY, int posZ, float[] data, int widht, int height, int size){
        float val = 0;
        float div = 0;
        for ( int z = 0; z < dims[2] ; z++ ){
            for ( int y = 0; y < dims[1] ; y++ ){
                for (int x = 0 ; x < dims[0] ; x++ ){
                    int xP = x-dims[0]/2;
                    int yP = y-dims[1]/2;
                    int zP = z-dims[2]/2;
                    if ( xP+posX < widht && xP+posX >= 0 &&
                            yP+posY < height && yP+posY >= 0 &&
                            zP+posZ < size && zP+posZ >= 0  ) {
                        val += data[posX + xP + (posY + yP) * widht + (posZ + zP) * widht * height]*kernel[x+y*dims[0]+z*dims[0]*dims[1]];
                        div += kernel[x+y*dims[0]+z*dims[0]*dims[1]];
                    }
                }
            }
        }

        return val/div;
    }

    public void setKernel(float[] kernel, int widht, int height, int size){
        this.kernel = kernel;
        this.dims = new int[]{widht,height,size};
    }
}
