/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.utils;


/**
 * Utils to fits curve on function
 *
 * @author Antoine Bergamaschi
 * Created by bergamaschi on 28/11/2014.
 */
//TODO change the function to works with unlimited number of parameters
//TODO Add a Enum class to used different type of function
public class FittingUtils {
    private double fitOnSineValue = Double.MAX_VALUE;
    private double[] sineParameters = new double[4];
    private boolean isUpdated = false;

    public enum FitFunction{
        SINE
    }


    /**
     * Parameters for the mobile Mean filter on to apply on the Raw center of gravity values
     */
    private boolean useMobileMean = true;
    private int mobileWindow = 16;

    public FittingUtils(){

    }

//    public FittingUtils(float[] valueY){
//        if ( valueY != null ) {
//            sineParameters = new double[4];
//            this.computeBasicParameters(valueY, sineParameters);
//        }
//    }


    public FittingUtils(FittingUtils fittingUtils){
        this.copy(fittingUtils);
    }

    private void copy(FittingUtils old){
        this.sineParameters = new double[old.getSineParameters().length];
        System.arraycopy(old.getSineParameters(),0,this.sineParameters,0,this.sineParameters.length);

        this.setMobileWindow(old.getMobileWindow());
        this.setUseMobileMean(old.getUseMobileMean());
        this.setFitOnSineValue(old.getFitOnSineValue());
        this.isUpdated = old.isUpdated();
    }

    /**
     * Fit a sine on the given set of points, with the function : a + b*sin(x*c + d)
     * @param realValueX float[], the X value ( can be Z,Y,X in real stack ) of the center of mass
     * @param realValueY  float[], the center of mass for each X
     * @return double[] the parameters of the fitted sine function
     */
    public double[] fitCurveOnSine(float[] realValueX, float[] realValueY){
        return fitCurveOnSine(realValueX,realValueY,true);
    }

    /**
     * Fit a sine on the given set of points, with the function : a + b*sin(x*c + d)
     * @param realValueX float[], the X value ( can be Z,Y,X in real stack ) of the center of mass
     * @param realValueY  float[], the center of mass for each X
     * @param withBasicParameters True if the basic sine parameters have to be computed or false if already or null
     *                            parameters are used.
     * @return double[] the parameters of the fitted sine function
     */
    public double[] fitCurveOnSine(float[] realValueX, float[] realValueY, boolean withBasicParameters){
        //Mobile Mean on the Y value;
        if ( useMobileMean ){
            return this.fitCurve(realValueX, ComputationUtils.computeMobileMean(realValueY, this.mobileWindow),withBasicParameters);
        }else{
            return this.fitCurve(realValueX,realValueY,withBasicParameters);
        }
    }


    /**
     * Fit a sine on the given set of points, with the function : a + b*sin(x*c + d)
     * @param realValueX float[], the X value ( can be Z,Y,X in real stack ) of the center of mass
     * @param realValueY  float[], the center of mass for each X
     * @param fixeAngularStep Allow to fixe the angular step in the Sine fit function
     * @param withBasicParameters True if the basic sine parameters have to be computed or false if already or null
     *                            parameters are used.
     * @return double[] the parameters of the fitted sine function
     */
    public double[] fitCurveOnSine(float[] realValueX, float[] realValueY, double fixeAngularStep, boolean withBasicParameters){
        //Mobile Mean on the Y value;
        if ( useMobileMean ){
            return this.fitCurveWithFixeAngularStep(realValueX, ComputationUtils.computeMobileMean(realValueY, this.mobileWindow),withBasicParameters,fixeAngularStep);
        }else{
            return this.fitCurveWithFixeAngularStep(realValueX,realValueY,withBasicParameters,fixeAngularStep);
        }
    }


    /**
     * Fit a sine on the given set of points, with the function : a + b*sin(x*c + d)
     * @param realValueX float[], the X value ( can be Z,Y,X in real stack ) of the center of mass
     * @param realValueY  float[], the center of mass for each X
     * @param length The number of PI to fit
     * @return double[] the parameters of the fitted sine function
     */
    public double[] fitCurveOnFullLengthSine(float[] realValueX, float[] realValueY, double length){
        //Mobile Mean on the Y value;
        if ( useMobileMean ){
            return this.fitCurveWithoutLengthModification(realValueX, ComputationUtils.computeMobileMean(realValueY, this.mobileWindow),length);
        }else{
            return this.fitCurveWithoutLengthModification(realValueX, realValueY, length);
        }
    }


    /**
     * Fit a sine on the given set of points, with the function : a + b*sin(x*c + d)
     * @param realValueX X values
     * @param valueToFit Y values
     * @return The parameters of the fitted sine function
     */
    private double[] fitCurve(float[] realValueX, float[] valueToFit){
        return fitCurve(realValueX,valueToFit,true);
    }

    /**
     * Fit a sine on the given set of points, with the function : a + b*sin(x*c + d)
     * @param realValueX X values.
     * @param valueToFit Y values.
     * @param withBasicParameters True if the basic sine parameters have to be computed or false if already or null
     *                            parameters are used.
     * @return The parameters of the fitted sine function
     */
    private double[] fitCurve(float[] realValueX, float[] valueToFit, boolean withBasicParameters){
        double[] parameters = new double[4];
        if ( withBasicParameters ) {
            parameters[0] = 1;
            parameters[1] = 1;
            parameters[2] = 0;
            parameters[3] = 0;
            computeBasicParameters(valueToFit, parameters);
        }else{
            System.arraycopy(this.sineParameters,0,parameters,0,parameters.length);
            //Parameters 1 represents the angular advance
//            parameters[1] = (2*Math.PI)/500;
        }

        double[] param = new double[8];
        double standard = Double.MAX_VALUE,tmp_standard;
        int numberOfRepetitionFirst = 0,tmp_param;
        boolean overlooping;

        while ( numberOfRepetitionFirst < 5 && standard > 0.00001d ){
            int numberOfRepetitionSeconde = 1000;
            double delta = 100;

//            parameters = new double[4];
//            if ( numberOfRepetitionFirst != 0 ){
//                delta  = parameters[(numberOfRepetitionFirst%4)]/10;
////                parameters[(numberOfRepetitionFirst%4)] += delta;
//            }

            tmp_standard  =  Double.MAX_VALUE;
            overlooping   = true;
            //Test the best parameter to Change
            while ( numberOfRepetitionSeconde > 0 && tmp_standard > 0.00001d && overlooping ){
                tmp_standard = computeSine(parameters[0],parameters[1], parameters[2],parameters[3],realValueX,valueToFit);

                param[0] = computeSine(parameters[0]+delta,parameters[1], parameters[2],parameters[3],realValueX,valueToFit);
                tmp_param = 0;
                param[1] = computeSine(parameters[0]-delta,parameters[1], parameters[2],parameters[3],realValueX,valueToFit);
                if ( param[tmp_param] > param[1] ){
                    tmp_param = 1;
                }
                param[2] = computeSine(parameters[0],parameters[1]+delta, parameters[2],parameters[3],realValueX,valueToFit);
                if ( param[tmp_param] > param[2] ){
                    tmp_param = 2;
                }
                param[3] = computeSine(parameters[0],parameters[1]-delta, parameters[2],parameters[3],realValueX,valueToFit);
                if ( param[tmp_param] > param[3] ){
                    tmp_param = 3;
                }
                param[4] = computeSine(parameters[0],parameters[1], parameters[2]+delta,parameters[3],realValueX,valueToFit);
                if ( param[tmp_param] > param[4] ){
                    tmp_param = 4;
                }
                param[5] = computeSine(parameters[0],parameters[1], parameters[2]-delta,parameters[3],realValueX,valueToFit);
                if ( param[tmp_param] > param[5] ){
                    tmp_param = 5;
                }
                param[6] = computeSine(parameters[0],parameters[1], parameters[2],parameters[3]+delta,realValueX,valueToFit);
                if ( param[tmp_param] > param[6] ){
                    tmp_param = 6;
                }
                param[7] = computeSine(parameters[0],parameters[1], parameters[2],parameters[3]-delta,realValueX,valueToFit);
                if ( param[tmp_param] > param[7] ){
                    tmp_param = 7;
                }


                if ( param[tmp_param] < tmp_standard ){
                    switch (tmp_param){
                        case 0:
                            tmp_standard = whilecase(parameters,0,delta,1,tmp_standard,realValueX,valueToFit);
                            break;
                        case  1:
                            tmp_standard = whilecase(parameters,0,delta,-1,tmp_standard,realValueX,valueToFit);
                            break;
                        case 2:
                            tmp_standard = whilecase(parameters,1,delta,1,tmp_standard,realValueX,valueToFit);
                            break;
                        case  3:
                            tmp_standard = whilecase(parameters,1,delta,-1,tmp_standard,realValueX,valueToFit);
                            break;
                        case  4:
                            tmp_standard = whilecase(parameters,2,delta,1,tmp_standard,realValueX,valueToFit);
                            break;
                        case 5:
                            tmp_standard = whilecase(parameters,2,delta,-1,tmp_standard,realValueX,valueToFit);
                            break;
                        case 6:
                            tmp_standard = whilecase(parameters,3,delta,1,tmp_standard,realValueX,valueToFit);
                            break;
                        case 7:
                            tmp_standard = whilecase(parameters,3,delta,-1,tmp_standard,realValueX,valueToFit);
                            break;

                    }
                }else{
                    delta /= 2;
                    if ( delta < 0.00001 ){
                        overlooping = false;
                    }
                }


                numberOfRepetitionSeconde--;
//                System.out.println("a = "+parameters[0]+" b = "+parameters[1]+" c = "+ parameters[2]+" d = "+parameters[3]);
//                System.out.println("fit = "+fit+" numberOfRepetition = "+numberOfRepetitionSeconde);
            }

            //If the fit is the current Best
            if ( tmp_standard < standard  ){
                standard = tmp_standard;
//                this.sineParameters = parameters;
            }

            numberOfRepetitionFirst++;
        }

//        if ( fitOnSineValue > standard ) {
        this.fitOnSineValue = standard;
        this.sineParameters = parameters;
        System.out.println("fit = " + fitOnSineValue);
        System.out.println("a = " + this.sineParameters[0] + " b = " + this.sineParameters[1] + " c = " + this.sineParameters[2] + " d = " + this.sineParameters[3]);
//        }else{
//            System.err.println("Fitted value not good enough");
//        }

        return  parameters;
    }

    /**
     * Fit a sine on the given set of points, with the function : a + b*sin(x*c + d)
     * @param realValueX X values.
     * @param valueToFit Y values.
     * @param withBasicParameters True if the basic sine parameters have to be computed or false if already or null
     *                            parameters are used.
     * @return The parameters of the fitted sine function
     */
    private double[] fitCurveWithFixeAngularStep(float[] realValueX, float[] valueToFit,
                                                 boolean withBasicParameters, double angularStep){
        double[] parameters = new double[4];
        if ( withBasicParameters ) {
            parameters[0] = 1;
            parameters[1] = 1;
            parameters[2] = 0;
            parameters[3] = 0;
            computeBasicParameters(valueToFit, parameters);
        }else{
            System.arraycopy(this.sineParameters,0,parameters,0,parameters.length);
            //Parameters 1 represents the angular advance
            parameters[1] = angularStep;
        }

        double[] param = new double[8];
        double standard = Double.MAX_VALUE,tmp_standard;
        int numberOfRepetitionFirst = 0,tmp_param;
        boolean overlooping;

        while ( numberOfRepetitionFirst < 5 && standard > 0.00001d ){
            int numberOfRepetitionSeconde = 1000;
            double delta = 100;

            tmp_standard  =  Double.MAX_VALUE;
            overlooping   = true;
            //Test the best parameter to Change
            while ( numberOfRepetitionSeconde > 0 && tmp_standard > 0.00001d && overlooping ){
                tmp_standard = computeSine(parameters[0],parameters[1], parameters[2],parameters[3],realValueX,valueToFit);

                param[0] = computeSine(parameters[0]+delta,parameters[1], parameters[2],parameters[3],realValueX,valueToFit);
                tmp_param = 0;
                param[1] = computeSine(parameters[0]-delta,parameters[1], parameters[2],parameters[3],realValueX,valueToFit);
                if ( param[tmp_param] > param[1] ){
                    tmp_param = 1;
                }
                param[4] = computeSine(parameters[0],parameters[1], parameters[2]+delta,parameters[3],realValueX,valueToFit);
                if ( param[tmp_param] > param[4] ){
                    tmp_param = 4;
                }
                param[5] = computeSine(parameters[0],parameters[1], parameters[2]-delta,parameters[3],realValueX,valueToFit);
                if ( param[tmp_param] > param[5] ){
                    tmp_param = 5;
                }
                param[6] = computeSine(parameters[0],parameters[1], parameters[2],parameters[3]+delta,realValueX,valueToFit);
                if ( param[tmp_param] > param[6] ){
                    tmp_param = 6;
                }
                param[7] = computeSine(parameters[0],parameters[1], parameters[2],parameters[3]-delta,realValueX,valueToFit);
                if ( param[tmp_param] > param[7] ){
                    tmp_param = 7;
                }


                if ( param[tmp_param] < tmp_standard ){
                    switch (tmp_param){
                        case 0:
                            tmp_standard = whilecase(parameters,0,delta,1,tmp_standard,realValueX,valueToFit);
                            break;
                        case  1:
                            tmp_standard = whilecase(parameters,0,delta,-1,tmp_standard,realValueX,valueToFit);
                            break;
                        case 2:
                            tmp_standard = whilecase(parameters,1,delta,1,tmp_standard,realValueX,valueToFit);
                            break;
                        case  3:
                            tmp_standard = whilecase(parameters,1,delta,-1,tmp_standard,realValueX,valueToFit);
                            break;
                        case  4:
                            tmp_standard = whilecase(parameters,2,delta,1,tmp_standard,realValueX,valueToFit);
                            break;
                        case 5:
                            tmp_standard = whilecase(parameters,2,delta,-1,tmp_standard,realValueX,valueToFit);
                            break;
                        case 6:
                            tmp_standard = whilecase(parameters,3,delta,1,tmp_standard,realValueX,valueToFit);
                            break;
                        case 7:
                            tmp_standard = whilecase(parameters,3,delta,-1,tmp_standard,realValueX,valueToFit);
                            break;

                    }
                }else{
                    delta /= 2;
                    if ( delta < 0.00001 ){
                        overlooping = false;
                    }
                }


                numberOfRepetitionSeconde--;
            }

            //If the fit is the current Best
            if ( tmp_standard < standard  ){
                standard = tmp_standard;
//                this.sineParameters = parameters;
            }

            numberOfRepetitionFirst++;
        }


        this.fitOnSineValue = standard;
        this.sineParameters = parameters;
        System.out.println("fit = " + fitOnSineValue);
        System.out.println("a = " + this.sineParameters[0] + " b = " + this.sineParameters[1] + " c = " +
                this.sineParameters[2] + " d = " + this.sineParameters[3]);

        return  parameters;
    }

    /**
     * Fit a sine on the given set of points, with the function : sin(x + c)
     * This function is a  very restricted version of Sine fit, it should be used only in specific cases
     * @param realValueX X values
     * @param valueToFit Y values
     * @param length The number of PI to fit
     * @return The parameters of the fitted sine function
     */
    private double[] fitCurveWithoutLengthModification(float[] realValueX, float[] valueToFit, double length){
        double[] parameters = new double[4];
        parameters[0] = 1;
        parameters[1] = 1;
        parameters[2] = 0;
        parameters[3] = 0;

        computeBasicParameters(valueToFit,parameters);
        parameters[1] = length;

        double[] param = new double[8];
        double standard = Double.MAX_VALUE,tmp_standard;
        int numberOfRepetitionFirst = 0,tmp_param;
        boolean overlooping;

//        while ( numberOfRepetitionFirst < 100 && standard > 0.00001d ){
        while ( numberOfRepetitionFirst < 1 && standard > 0.00001d ){
            int numberOfRepetitionSeconde = 10;
            double delta = Math.PI;

            tmp_standard  =  Double.MAX_VALUE;
            overlooping   = true;
            //Test the best parameter to Change
            while ( numberOfRepetitionSeconde > 0 && tmp_standard > 0.00001d && overlooping ){
                tmp_standard = computeSine(parameters[0],parameters[1], parameters[2],parameters[3],realValueX,valueToFit);

                tmp_param = 4;
                param[4] = computeSine(parameters[0],parameters[1], parameters[2]+delta,parameters[3],realValueX,valueToFit);
                if ( param[tmp_param] > param[4] ){
                    tmp_param = 4;
                }
                param[5] = computeSine(parameters[0],parameters[1], parameters[2]-delta,parameters[3],realValueX,valueToFit);
                if ( param[tmp_param] > param[5] ){
                    tmp_param = 5;
                }


                if ( param[tmp_param] < tmp_standard ){
                    switch (tmp_param){
                        case  4:
                            tmp_standard = whilecase(parameters,2,delta,1,tmp_standard,realValueX,valueToFit);
                            break;
                        case 5:
                            tmp_standard = whilecase(parameters,2,delta,-1,tmp_standard,realValueX,valueToFit);
                            break;

                    }
                }else{
                    delta /= 2;
                    if ( delta < 0.00001 ){
                        overlooping = false;
                    }
                }

                if ( Math.abs(parameters[2]/(2*Math.PI)) > 1 ) {
                    parameters[2] = parameters[2] / ((parameters[2] / (2 * Math.PI)));
                }
                else if ( parameters[2] < 0 ){
                    parameters[2] += 2*Math.PI;
                }


                numberOfRepetitionSeconde--;
            }

            //If the fit is the current Best
            if ( tmp_standard < standard  ){
                standard = tmp_standard;
                this.sineParameters = parameters;
            }

            numberOfRepetitionFirst++;
        }

        return  parameters;
    }

    private double whilecase(double[] parameters,int param, double delta,int sign, double tmp_standard, float[] valuesX, float[] valuesY ){
        parameters[param] = parameters[param]+(delta*sign);
        double test_standard = computeSine(parameters[0],parameters[1], parameters[2],parameters[3],valuesX,valuesY);

        //Warning Change here > or < regarding the score function
        while ( test_standard < tmp_standard ){
            parameters[param] = parameters[param]+(delta*sign);
            tmp_standard = test_standard;
            test_standard = computeSine(parameters[0],parameters[1], parameters[2],parameters[3],valuesX,valuesY);
        }
        //Remove last modification
        parameters[param] = parameters[param]-(delta*sign);
        tmp_standard = computeSine(parameters[0],parameters[1], parameters[2],parameters[3],valuesX,valuesY);

        return tmp_standard;
    }

    private double computeSine(double a, double b, double c, double d, float[] valueX, float[] valueY){
        double n = valueX.length;
        double square = 0;
        double tmp;
        for ( int i = 0 ; i < n ; i++){
            tmp = computeSine(a,b,c,d,(double)valueX[i])-valueY[i];
            square += (tmp*tmp)/n;
        }
        return square;
    }


    /**
     * Compute the basic parameter of the sine function based on classic heuristics. return the computed value
     * in the parameters array given in function entry.
     //     * @param valueX float[], the X value ( can be Z,Y,X in real stack ) of the center of mass
     * @param valueY  float[], the center of mass for each X
     * @param parameters  double[], the sine parameters to be computed
     */
    private void computeBasicParameters(float[] valueY,double[] parameters){
        int minPos = 0,maxPos = 0, pos;
        double y;
        for ( pos = 0 ; pos < valueY.length ; pos++ ){
            y = (double)valueY[pos];
            if ( y < valueY[minPos] ){
                minPos = pos;
            }else if( y > valueY[maxPos] ){
                maxPos = pos;
            }

            parameters[3] += y/(double)valueY.length;
        }

        //Amplitude parameter
        parameters[0] = (valueY[maxPos] - valueY[minPos])/2;

        //Phase length parameter
        parameters[1] = (Math.PI) / Math.abs(maxPos-minPos);

        if ( parameters[1] < 0 && parameters[0] < 0){
            parameters[2] += maxPos;
        }else if(parameters[1] < 0 | parameters[0] < 0){
            parameters[2] += minPos;
        }else{
            parameters[2] += maxPos;
        }

        isUpdated = true;
//        System.out.println("Basic Parameter : a = "+parameters[0]+" b = "+parameters[1]+" c = "+ parameters[2]+" d = "+parameters[3]);
    }

    public double computeSine(double a, double b, double c, double d, double x){
        return  (d + a * Math.sin(x*b + c));
    }

    public float[] getFittedValue(float[] x){
        double[] sineParameters = this.getSineParameters();
        float[] fittedValue = new float[x.length];
        for (int i = 0; i < x.length; i++) {
            fittedValue[i] = (float) (this.computeSine(sineParameters[0], sineParameters[1], sineParameters[2], sineParameters[3], (double) x[i]));
        }
        return fittedValue;
    }

    public double[] getSineParameters() {
        return sineParameters;
    }

    public void setSineParameters(double[] sineParameters) {
        this.sineParameters = sineParameters;
    }

    public double getFitOnSineValue() {
        return fitOnSineValue;
    }

    public void setFitOnSineValue(double fitOnSineValue) {
        this.fitOnSineValue = fitOnSineValue;
    }

    public void setUseMobileMean(boolean useMobileMean){
        this.useMobileMean = useMobileMean;
    }

    public boolean getUseMobileMean(){
        return this.useMobileMean;
    }

    public int getMobileWindow() {
        return this.mobileWindow;
    }

    public void setMobileWindow(int mobileWindow){
        this.mobileWindow = mobileWindow;
    }

    public boolean isUpdated(){
        return isUpdated;
    }
}
