/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.model.utils;

import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStack;
import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStackFactory;
import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.RXSpectrum;
import com.soleil.nanoscopium.rxtomoj.model.computationFunction.ComputeSpot;

import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.stream.IntStream;

/**
 * Created by bergamaschi on 07/02/14.
 */
public class ComputationUtils {

//    public final static int XYZ = 0, XZY = 1, ZYX = 2;

    public final static double DEFAULT_ROUDING = Math.pow(10,-3);

    public enum StackType{
        ABSORPTION("Absorption", "absorption_description"), DARKFIELD("Darkfield","darkfield_description"),
        PHASECONTRAST("PhaseContrast","phasecontrast_description"),PHASE_GRAD_X("GradX","phasecontrast_description"),PHASE_GRAD_Y("GradY","phasecontrast_description"),

        BP_ABSORPTION("BP_absorbtion"), BP_DARKFIELD("BP_darkfield"),BP_PHASECONTRAST("BP_phaseContrast"),FBP_FLUORESCENCE("FBP_fluo"),
        FBP_ABSORPTION("FBP_absorbtion"),FBP_DARKFIELD("FBP_darkfield"),FBP_PHASECONTRAST("FBP_phaseContrast"),BP_FLUORESCENCE("BP_fluo"),
        ART_ABSORPTION("ART_absorbtion"),ART_DARKFIELD("ART_darkfield"),ART_PHASECONTRAST("ART_phaseContrast"),ART_FLUORESCENCE("ART_fluo"),
        SART_ABSORPTION("SART_absorbtion"),SART_DARKFIELD("SART_darkfield"),SART_PHASECONTRAST("SART_phaseContrast"),SART_FLUORESCENCE("SART_fluo"),
        BGART_ABSORPTION("BGART_absorbtion"),BGART_DARKFIELD("BGART_darkfield"),BGART_PHASECONTRAST("BGART_phaseContrast"),BGART_FLUORESCENCE("SART_fluo"),

        ABSORPTION_REF("Reference"),SPOT_MAP("Spot_Map"),FLUORESCENCE("Fluorescence","fluorescence_description"),
        SUM_SPECTRA("Spectrum"),
        PRECOMPUTED_SHIFT("Absorbtion_shift"), PRECOMPUTED_BACKGROUND_NORMALIZED("Absorbtion_normalized"),
        MOTORS_POSITION("Motors"),MOTORS_ROTATION("Rotation Position"),

        //Volatil storage only
        MOTOR_X_VOLATILE("volatile_X_Motors"),MOTOR_Y_VOLATILE("volatile Y Motors"),MOTOR_R_VOLATILE("volatile R Motors"),
        CENTER_OF_MASS("FITTING_UTILS"),LINE_POSITIONS("FITTING_UTILS"),COMPUTED_FIT("FITTING_UTILS"),
        COMPUTED_BACKGROUND_LIMITS("FITTING_UTILS"),COMPUTED_BACKGROUND_TYPES("FITTING_UTILS"),SHIFT_DATA("FITTING_UTILS");

        /**
         * The Filename created depending on the current StackType
         */
        private String saveName = "";

        /**
         * ??
         */
        private String resource = "";

        /**
         * An additional string that will be added to the FileName ( used for multi-path data such as fluorescence or darkfield )
         */
        @Deprecated
        private String additionalName = null;

        //ONLY for fluo
        //Implement a new version
        /**
         * The Spectrum name in the Hdf5 File ( used to separate differente fluo channel )
         * This information is mandatory to retrieve Spectrum data
         */
        @Deprecated
        private String spc = "";

        /**
         * If this stack is represented with a MultiPath Object,
         * Contains the name where is store this StackType in the MultiPath Object
         * Else is empty
         */
        @Deprecated
        private String otherName = "";


        StackType(String saveName){
            this(saveName,"default_modalities_description");
        }

        StackType(String saveName, String resource){
            this.saveName = saveName;
            this.resource = resource;
        }

        public String getSaveName(){
//            switch (this){
//                case FLUORESCENCE:
//                    if ( additionalName != null ){
//                        return this.saveName+"_"+this.additionalName+"_"+this.spc.substring(this.spc.lastIndexOf("/") + 1);
//                    }
//                case SUM_SPECTRA:
//                    return this.saveName + "_"+this.spc.substring(this.spc.lastIndexOf("/") + 1);
//                case DARKFIELD:
//                    if ( additionalName != null ){
//                        return  this.saveName +"_"+this.additionalName;
//                    }else{
//                        return  this.saveName;
//                    }
//                default:
//                    return this.saveName;
//            }
            return this.saveName;
        }

        public boolean isBasicType(){
            if ( ABSORPTION == this || DARKFIELD == this ||PHASECONTRAST == this || FLUORESCENCE == this ||
                    PHASE_GRAD_X == this || PHASE_GRAD_Y == this){
                return true;
            }
            return false;
        }

        public static boolean isBasicType(StackType type){
            if ( ABSORPTION == type || DARKFIELD == type ||PHASECONTRAST == type || FLUORESCENCE == type ){
                return true;
            }
            return false;
        }

//        boolean isInitialType(){
//            if ( ABSORPTION == this || DARKFIELD == this || PHASECONTRAST == this){
//                return true;
//            }
//            return false;
//        }

        public final static ArrayList<StackType> getBasicType(){
            ArrayList<StackType> s = new ArrayList<>();
            s.add(ABSORPTION);
            s.add(DARKFIELD);
            s.add(PHASECONTRAST);
            s.add(FLUORESCENCE);
            return s;
        }

        @Deprecated
        public void setAdditionalName(String name){
            this.additionalName = name;
        }

        public final static StackType getReference(){
            return ABSORPTION_REF;
        }

        @Deprecated
        public void setSpc(String spc){
            if ( FLUORESCENCE  ==  this || SUM_SPECTRA == this) {
                this.spc = spc;
            }
        }

        @Deprecated
        public void setOtherName(String otherName){
//            if ( FLUORESCENCE == this  ){
                this.otherName = otherName;
//            }
        }

        @Deprecated
        public String getSpc(){
            if ( FLUORESCENCE  ==  this || SUM_SPECTRA == this) {
                return this.spc;
            }
            return null;
        }

        @Deprecated
        public String getOtherName(){
            return this.otherName;
        }

    }

    private boolean lock_computeModalities = false;

    public enum MotorType{
        MOTOR_X("MOTOR_X_name"),MOTOR_Y("MOTOR_Y_name"),MOTOR_Z("MOTOR_Z_name"),MOTOR_R("MOTOR_R_name");

        private String name = "";

        MotorType(String name){
            this.name = ResourceBundle.getBundle("RXTomoJ").getString(name);
        }

        public String getName(){
            return this.name;
        }

        public final static String[] getNames(){
            String[] names = new String[MotorType.values().length];
            int i = 0;
            for ( MotorType motorType : MotorType.values()){
                names[i] = motorType.getName();
                i++;
            }
            return names;
        }

        public StackType getVolatilStackEnum(){
            switch (this){
                case MOTOR_R:
                    return StackType.MOTOR_R_VOLATILE;
                case MOTOR_X:
                    return StackType.MOTOR_X_VOLATILE;
                case MOTOR_Y:
                    return StackType.MOTOR_Y_VOLATILE;
                default:
                    return null;
            }
        }

    }

    public enum ReconstructionType{
        ART,FBP;
    }


    public enum OrientationType{
        XYZ("XYZ_description"),XZY("XZY_description"),ZYX("ZYX_description");

        private String resource;

        private OrientationType(String resource){
            this.resource = resource;
        }

        public String getDescription(){
            return ResourceBundle.getBundle("RXTomoJ").getString(this.resource);
        }
    }

    public enum PixelType{
        ALL(4),OTHER(0),SPOT(1),HOT(2),SPOT_AND_HOT(3);

        int value;

        PixelType(int value){
            this.value = value;
        }

        /**
         * Get the PixelType corresponding to this value or ALL if not existing
         * @param value The value corresponding to one of the PixelType
         * @return The PixelType corresponding
         */
        public static PixelType getPixelType(int value){
            for(PixelType pixelType : PixelType.values() ){
                if ( pixelType.value == value ){
                    return pixelType;
                }
            }
            return ALL;
        }

        public int value(){
            return value;
        }

        public static boolean isDarkFieldPixel(int value){
            PixelType p = getPixelType(value);
            return isDarkFieldPixel(p);
        }

        public static boolean isDarkFieldPixel(PixelType p){
            switch (p){
                case SPOT:
                case HOT:
                case SPOT_AND_HOT:
                    return false;
            }
            return true;
        }

        public static boolean isTransmissionPixel(int value){
            PixelType p = getPixelType(value);
            return isTransmissionPixel(p);
        }

        public static boolean isTransmissionPixel(PixelType p){
            switch (p){
                case OTHER:
                case HOT:
                case SPOT_AND_HOT:
                    return false;
            }
            return true;
        }
    }

    /**
     * Divide by 1/x
     * @param toInvert The stack to invert
     */
    public static void invertStack(RXImage toInvert){
        toInvert.computeData((data)->{
            for(int i = 0 ; i < (data).length ; i++){
                (data)[i]+=1;
                if ( (data)[i]==0 ) {
                    (data)[i] = 1;
                }else{
                    (data)[i] = 1 / (data)[i];
                }
            }
        });
    }

    /**
     * Compute the Center of Gravity of this 2D float data
     * @param data The Data on which the computation is performed
     * @param width The Width of the data to compute ON
     * @param height The Height of the data to compute ON
     * @return {x center, y center}
     */
    public static float[] computeGravityCenter(float[] data, int width, int height){
        float[] gravity = new float[2];
        float[] sumIntensity = new float[width+1];
        float[] sumPonderated = new float[width+1];
        float dividWidth = height;
        float dividHeight = width;

        for ( int y = 0 ; y < height ; y++ ){
            sumPonderated[width] = 0;
            sumIntensity[width] = 0;
            for( int x = 0 ; x < width ; x++ ){
                sumPonderated[width] += data[x+y*width]*(y+1);
                sumIntensity[width] += data[x+y*width];

                sumPonderated[x] += data[x+y*width]*(x+1);
                sumIntensity[x] += data[x+y*width];
            }
            if ( sumIntensity[width] != 0 ) {
                gravity[1] += sumPonderated[width] / sumIntensity[width];
            }else{
                dividWidth--;
            }
        }

        for( int x = 0 ; x < width ; x++ ){
            if ( sumIntensity[x] != 0 ) {
                gravity[0] += sumPonderated[x] / sumIntensity[x];
            }else{
                dividHeight--;
            }
        }


        gravity[0] /= dividHeight;
        gravity[1] /= dividWidth;

        return gravity;
    }


    /**
     * Compute the Center of Gravity of each Row
     * @param image The Data on which the computation is performed
     * @return {Gravity center of each row}
     */
    public static float[] computeGravityCenter1D(RXImage image){
        float[] gravityCenter = new float[image.getSize()*image.getHeight()], pixels;
        int z, y, x;
        float sumIntensity;

        final int[] positionImage = new int[2];

        pixels = image.copyData();
        for (z = 0; z < image.getSize(); z++) {
            positionImage[0] = z*image.getWidth()*image.getHeight();
            for (y = 0; y < image.getHeight(); y++) {
                positionImage[1] = positionImage[0]+y*image.getWidth();
                sumIntensity = 0;
                int centerGravityPos = z*image.getHeight() + y;
                for (x = 0; x < image.getWidth(); x++) {
                    sumIntensity += pixels[x + positionImage[1]] * (x + 1);
                    gravityCenter[centerGravityPos] += pixels[x + positionImage[1]];
                }
                gravityCenter[centerGravityPos] = sumIntensity / gravityCenter[centerGravityPos] - 1;
            }
        }

        return gravityCenter;
    }

    /**
     * Get the linear interpolated value of x. If no valid interpolation return NAN value
     * @param x double, the value to interpolate
     * @param pixels float[], the 1D table in which the value is interpolated
     * @return float, the pixel value of the interpolated x
     */
    public static double getLinearInterpolationValue(double x, float[] pixels) {
        return getLinearInterpolationValue(x,0,pixels.length,pixels);
    }

    /**
     * Get the linear interpolated value of x. If no valid interpolation return NAN value
     * @param x The value to interpolate
     * @param position Current integer position
     * @param width The width of the pixels array
     * @param pixels The 1D table in which the value is interpolated
     * @return float, the pixel value of the interpolated x
     */
    public static double getLinearInterpolationValue(double x, int position, int width, float[] pixels) {
        int x1 = (int) Math.floor(x);
        int x2 = (int) Math.ceil(x);

        double remainingFrac_x2 = x - x1;
        double remainingFrac_x1 = 1 - remainingFrac_x2;

        x1+= position;
        x2+=position;

        if ( x1 >= 0 && x1 < pixels.length){
            if ( x2 > x1 && (x2-position) < width && x2 < pixels.length ){
                return remainingFrac_x1*pixels[x1] + remainingFrac_x2*pixels[x2];
            }else{
                //No interpolation is done has only 1 value is known
                return pixels[x1];
            }
        }

        return Double.NaN;
    }


    /**
     * Apply a linear interpolated value in the pixels arrays. If no valid interpolation return NAN value
     * @param x The value to interpolate
     * @param width the width of the pixel array
     * @param value The value to dispatch
     * @param pixels The 1D table in which the value is interpolated
     */
    @Deprecated
    public static void setLinearInterpolationValue(double x,int width, float value, float[] pixels) {
        int leftPosition = (int)x,rightPosition = (int)x+1;
        double remainingFrac = x - (int)x;

        if ( remainingFrac > 0.5){
            remainingFrac = 1-remainingFrac;
        }else if ( remainingFrac == 0.0d ){
            remainingFrac = 1;
        }


        if ( (leftPosition < 0 | rightPosition < 0) | (leftPosition >= pixels.length) ){
//            System.err.println(" Non valid Position cannot be interpolated : "+leftPosition+" , "+rightPosition);
        }else if( x >= 1 && ((int)x)%(width-1) == 0){
            pixels[leftPosition] = value;
        }
        else{
            pixels[leftPosition] += value*remainingFrac;
            if ( rightPosition < pixels.length){
                pixels[rightPosition] += value*(1-remainingFrac);
            }
        }
    }

    /**
     * Apply a linear interpolated value in the pixels arrays. If no valid interpolation return NAN value
     * @param x The value to interpolate
     * @param pixels The 1D table in which the value is interpolated
     * @param value The value to dispatch
     */
    @Deprecated
    public static void setLinearInterpolationValue(double x, float value, float[] pixels) {
        int leftPosition = (int)x,rightPosition = (int)x+1;
        double remainingFrac = x - (int)x;

        if ( remainingFrac > 0.5){
            remainingFrac = 1-remainingFrac;
        }else if ( remainingFrac == 0.0d ){
            remainingFrac = 1;
        }

        if ( (leftPosition < 0 | rightPosition < 0) | (leftPosition >= pixels.length) ){
//            System.err.println(" Non valid Position cannot be interpolated : "+leftPosition+" , "+rightPosition);
        }
        else{
            pixels[leftPosition] += value*remainingFrac;
            if ( rightPosition < pixels.length){
                pixels[rightPosition] += value*(1-remainingFrac);
            }
        }
    }


    /**
     * Get the bilinear interpolation value of x,y. Take the center of gravity between the four pixels around the asked value
     * Beaware that y should be passed to int
     * @param x The x value to interpolate
     * @param y The y value to interpolate
     * @param width The width of the table
     * @param value The value to spread between the pixel
     * @param pixels The 2D table in which the value is interpolated
     */
    @Deprecated
    public static void setBilinearInterpolationValue(double x, double y, int width, float value, float[] pixels){
        setBilinearInterpolationValue(x,y,0,width,value,pixels);
    }

    @Deprecated
    public static void setBilinearInterpolationValue(double x, double y, double positionZ, int width, float value, float[] pixels){

        double positionTopLeft = ((int)x + (int)y * width + positionZ) + x - (int)x  , positionBottomLeft  = ((int)x + (int)(y + 1) * width + positionZ) + x - (int)x;
        double remainingFrac_y = y - (int)y;
        double remainingFrac_x = x - (int)x;

        if ( remainingFrac_x < 0.5 ){
            remainingFrac_x = 1 -remainingFrac_x;
        }

        if ( remainingFrac_y < 0.5 ){
            remainingFrac_y = 1 - remainingFrac_y;
        }
//        double valueX1,valueX2=Double.NaN;

//        //If x is on the right border of the plane then x+1 does not exist
//        //Then interpolation is made between this pixel and the pixel under this if it exist
        if ( x >= 1 && ((int)x)%(width-1) == 0 && positionTopLeft < pixels.length){
            pixels[((int) positionTopLeft)] += (float) (remainingFrac_y*value*remainingFrac_x);
            if ( positionBottomLeft < pixels.length ){
                pixels[((int) positionBottomLeft)] += (float) (value*(1-remainingFrac_y)*remainingFrac_x);
            }
        }
        //Else the pixel can be normally interpolate on the x plane
        else{
            setLinearInterpolationValue(positionTopLeft,width, (float) (value*remainingFrac_y), pixels);
            //If the pixel can be interpolated under the x plane
            if ( (x + (y+1)*width) < pixels.length  ){
                setLinearInterpolationValue(positionBottomLeft,width, (float) (value*(1-remainingFrac_y)), pixels);
            }else{
                setLinearInterpolationValue(positionTopLeft,width, (float) (value*(1-remainingFrac_y)), pixels);
            }
//        }
        //If every value exist return the weighting value between x plane and x+width plane
//        if ( !Double.isNaN(valueX1) && !Double.isNaN(valueX2) ){
//            return (float) (valueX1 + remainingFrac*(valueX2-valueX1));
        }
    }


    /**
     * Get the bilinear interpolation value of x,y. Take the center of gravity between the four pixels around the asked value
     * Beaware that y should be passed to int
     * @param x double, the x value to interpolate
     * @param y double, the y value to interpolate
     * @param width int, the width of the table
     * @param pixels float[], the 2D table in which the value is interpolated
     * @return flaot, the pixel value of the interpolated x,y
     */
    public static double getBilinearInterpolationValue(double x, double y, int width, float[] pixels){
        return getBilinearInterpolationValue(x,y,0,pixels.length/width,width,pixels);
    }

    /**
     * Get the bilinear interpolation value of x,y. Take the center of gravity between the four pixels around the asked value
     * Beaware that y should be passed to int
     * @param x double, the x value to interpolate
     * @param y double, the y value to interpolate
     * @param position the current in an 1D array
     * @param width int, the width of the table
     * @param height int, the height of the table
     * @param pixels float[], the 2D table in which the value is interpolated
     * @return flaot, the pixel value of the interpolated x,y
     */
    public static double getBilinearInterpolationValue(double x, double y, int position,int height, int width, float[] pixels){
        int y1 = (int) Math.floor(y);
        int y2 = (int) Math.ceil(y);

        double remainingFrac_y2 = y - y1;
        double remainingFrac_y1 = 1 - remainingFrac_y2;

        double valueY1=0,valueY2=0;


        valueY1 = getLinearInterpolationValue(x,(y1*width)+position,width,pixels);
        if ( y2 > y1 && y2 < height ) {
            valueY2 = getLinearInterpolationValue(x, (y2 * width) + position, width, pixels);
        }

        if ( remainingFrac_y1 == 1 ){
            return valueY1;
        }

        if ( !Double.isNaN(valueY1) && !Double.isNaN(valueY1) ){
            return valueY1*remainingFrac_y1 + valueY2*remainingFrac_y2;
        }
        return Double.NaN;
    }


    public static double getBilinearInterpolationValueZ(double x, double z, int positionY, int width,int height, int size, float[] pixels){
        int z1 = (int) Math.floor(z);
        int z2 = (int) Math.ceil(z);

        double remainingFrac_z2 = z - z1;
        double remainingFrac_z1 = 1 - remainingFrac_z2;

        double valueZ1=0,valueZ2=0;


        valueZ1 = getLinearInterpolationValue(x,(z1*width*height)+positionY,width,pixels);
        if ( z2 > z1 && z2 < size ) {
            valueZ2 = getLinearInterpolationValue(x, (z2 * width*height) + positionY, width, pixels);
        }

        if ( remainingFrac_z1 == 1 ){
            return valueZ1;
        }

        if ( !Double.isNaN(valueZ1) && !Double.isNaN(valueZ1) ){
            return valueZ1*remainingFrac_z1 + valueZ2*remainingFrac_z2;
        }
        return Double.NaN;
    }

    /**
     * Compute the Coordinate x,y,z in the cartesian coordinate with a rotation of phi/theta/psi
     * @param x double x position in the no-rotated plan
     * @param y double y position in the no-rotated plan
     * @param z double z position in the no-rotated plan
     * @param rotationMatrix double[][] , Euler matrix of rotation for phi,theta,psi in 3D plane
     * @return double[] , [x,y,z] position in the rotated plan
     */
    static public double[] computeRotatedCoordinate(double x, double y, double z, double[][] rotationMatrix){
        double [] newPosition = new double[3];

        for ( int pos = 0 ; pos < 3 ; pos++){
            newPosition[pos] = x*rotationMatrix[pos][0] + y*rotationMatrix[pos][1] + z*rotationMatrix[pos][2];
        }

        return newPosition;
    }

    /***
     * TODO warning this rotation works but is uncorrect for positive angle
     * Compute the matrix of rotation for the given euler_angle
     * @param euler_angle double[], [phi,theta,psi] in radian
     * @return double[][] , the appropriate matrix of rotation for this phi,theta,psi angle
     */
    static public double[][] computeRotationMatrix(double[] euler_angle){
        double [][] matrix=new double[3][3];

        double cphi = Math.cos(euler_angle[0]),
                sphi = Math.sin(euler_angle[0]),
                ctheta = Math.cos(euler_angle[1]),
                stheta = Math.sin(euler_angle[1]),
                cpsi = Math.cos(euler_angle[2]),
                spsi = Math.sin(euler_angle[2]);

        matrix[0][0] = ( cpsi * ctheta * cphi ) -spsi * sphi ;
        matrix[0][1] =  ( cpsi * ctheta * sphi ) +  (spsi * cphi );
        matrix[0][2] = -cpsi * stheta;
        matrix[1][0] = ( -spsi * ctheta * cphi ) -cpsi * sphi ;
        matrix[1][1] = ( -spsi * ctheta * sphi ) + ( cpsi * cphi );
        matrix[1][2] = ( spsi * stheta );
        matrix[2][0] = stheta * cphi;
        matrix[2][1] = stheta * sphi;
        matrix[2][2] =  ctheta;


        return matrix;
    }

    static public void arrayDivide(float[] arrayToDiv, int[] arrayDivider){
        //Copied from the Java8 code
        IntStream.range(0, arrayToDiv.length).parallel().forEach(i -> {
            if ( arrayDivider[i] != 0 ) {
                arrayToDiv[i] /= (float)arrayDivider[i];
            }
        });
    }

    /**
     * Warning Special divide operation. If arrayDivider[i] strictly inferior to 1 then divide by 1+arrayDivider[i]
     * @param arrayToDiv Array to divide
     * @param arrayDivider Array dividing
     */
    static public void arrayDivide(float[] arrayToDiv, float[] arrayDivider){
        //Copied from the Java8 code
        IntStream.range(0, arrayToDiv.length).parallel().forEach(i -> {
            if (arrayDivider[i] != 0.0f) {
                arrayToDiv[i] /= arrayDivider[i];
            }
        });
    }

    static public RXImage copyStack(RXImage stack){
        RXImage result = new RXImage(stack.getWidth(),stack.getHeight(),stack.getSize());
        result.setData(stack.copyData());
        return result;
    }

    public static double computeCoverage(double posX1, double posY1, double posX2, double posY2){
        double area = 0;
        double maxPosX = 1;
        double maxPosY = 1;
        double minPosX = 0;
        double minPosY = 0;
        //Case opposite side in X
        if ( posX1 == minPosX && posX2 == maxPosX ||
                posX1 == maxPosX && posX2 == minPosX){
            area = posY1*maxPosX + ((posY2-posY1)*maxPosX)/2;
        }
        //Case opposite side in Y
        else if ( posY1 == minPosY && posY2 == maxPosY ||
                posY1 == maxPosY && posY2 == minPosY ){
            area = posX1*maxPosY + ((posX2-posX1)*maxPosY)/2;
        }
        //Case adjacent side
        else {
            //Right Corner
            if ( posX1 > posX2 && posY1 >= posY2 ){
                area = ((posX1-posX2)*(posY1-posY2))/2;
            }
            //Left Corner
            else if ( posX1 > posX2 ){
                area = ((posX1-posX2)*(posY2-posY1))/2;
            }
            //Right Corner
            else if ( posY2 > posY1){
                area = ((posX2-posX1)*(posY2-posY1))/2;
            }
            //Left Corner
            else{
                area = ((posX2-posX1)*(posY1-posY2))/2;
            }
        }

        return area;

    }


    public static double computeCoverage(double posX1, double posY1, double posX2, double posY2, double[] dir){
        double area = 0;
        double maxPosX = 1;
        double maxPosY = 1;
        double minPosX = 0;
        double minPosY = 0;
        //Case opposite side in X
        if ( posX1 == minPosX && posX2 == maxPosX ||
                posX1 == maxPosX && posX2 == minPosX){
            area = Math.abs(posY1-dir[1] + ((posY2-posY1)*maxPosX)/2);
        }
        //Case opposite side in Y
        else if ( posY1 == minPosY && posY2 == maxPosY ||
                posY1 == maxPosY && posY2 == minPosY ){
            area = Math.abs(posX1-dir[0] + ((posX2-posX1)*maxPosX)/2);
        }
        //Case adjacent side
        else {
            area = Math.abs ((posY1-posY2)*(posX1-posX2))/2.0d;

            if ( ((dir[0] > posX1 && dir[0] > posX2) || (dir[1] > posY1 && dir[1] > posY2)) ||
                    ((dir[0] < posX1 && dir[0] < posX2) || (dir[1] < posY1 && dir[1] < posY2))
                    ){
                area = 1 - area;
            }
        }

        return area;

    }

    /**
     * Compute Area between of square ( of dimension 1 ) inside two lines with the same theta direction.
     * Make sure the coordinate X,y are between 0 and 1. Xend and Yend may be very large.
     * @param x A point X on the first line
     * @param y A point Y on the first line
     * @param xEnd A point X on the max line
     * @param yEnd A point Y on the max line
     * @param theta The angle of orientation
     * @return The Area between the two lines
     */
    public static double computeTriangleRayLine(double x, double y,double xEnd, double yEnd, double theta){
        double area = 0;
        double area1 = 0;
        double[] border = new double[]{xEnd - x,yEnd - y};
        double yBorder = yEnd - y;

        if ( border[0] > 0 ){
            border[0] = 1;
        }else{
            border[0] = 0;
        }

        if ( border[1] > 0 ){
            border[1] = 1;
        }else{
            border[1] = 0;
        }

        if ( xEnd > 0 && yEnd > 0 ){
            if ( xEnd-(int)x <= 1 && yEnd-(int)y <= 1) {
                xEnd = xEnd - (int) xEnd;
                yEnd = yEnd - (int) yEnd;

                double[] intersection = computeIntersectionLineInSquare(xEnd, yEnd, theta, 0, 0, 1, 1);
                area1 = computeCoverage(intersection[0], intersection[1], intersection[2], intersection[3], border);
            }
        }

        x = x - (int)x;
        y = y - (int)y;

        double[] intersection = computeIntersectionLineInSquare(x, y, theta, 0, 0, 1, 1);
        area = computeCoverage(intersection[0],intersection[1],intersection[2],intersection[3], border);


        return Math.abs(area-area1);
    }

    @Deprecated
    public static double computeTriangleRayLine(double x, double y,double xEnd, double yEnd, double theta,double minX, double minY, double maxX, double maxY){
        double area = 0;
        double area1 = 0;

        x = Math.ceil(x) - x;
        y = Math.ceil(y) - y;

        //Find the border where the maximal dimension point to
        double xBorder = xEnd - x;
        double yBorder = yEnd - y;

        if ( xBorder > 0.5 ){
            xBorder = 1;
        }else{
            xBorder = 0;
        }

        if ( yBorder > 0.5 ){
            yBorder = 1;
        }else{
            yBorder = 0;
        }

        //Compute the Area under the maximum dimension
        if ( (int)xEnd == (int)x && (int)yEnd == (int)y ){
            xEnd = Math.ceil(xEnd) - xEnd;
            yEnd = Math.ceil(yEnd) - yEnd;

            double[] intersection = computeIntersectionLineInSquare(xEnd,yEnd,theta,0,0,1,1);
            area1 = computeCoverage(intersection[0],intersection[1],intersection[2],intersection[3], new double[]{xBorder,yBorder});
        }

        double[] intersection = computeIntersectionLineInSquare(x,y,theta,0,0,1,1);
        area = computeCoverage(intersection[0], intersection[1], intersection[2], intersection[3], new double[]{xBorder, yBorder});

        //Return the subtraction of both
        return Math.abs(area-area1);
    }

    /**
     * Compute the length of the Ray inside the square
     * @param xstart The X position of a point on the line
     * @param ystart The Y position of a point on the line
     * @param theta The angle of direction of the line
     * @param minDistX The min X coordinate of the square
     * @param minDistY The min Y coordinate of the square
     * @param maxDistX The max X coordinate of the square
     * @param maxDistY The max Y coordinate of the square
     * @return The length
     */
    public static double computeRayLenght(double xstart,double ystart, double theta, double minDistX, double minDistY, double maxDistX, double maxDistY){
        double lenght = 0;
        double x1 = 0;
        double x2 = 0;

        double y1 = 0;
        double y2 = 0;

        if ( theta == 0 ){
            theta = Double.MIN_VALUE;
        }

        double coef = Math.abs(Math.tan(theta));

        x1 = ((maxDistY - ystart) / coef) + xstart;
        x2 = ((minDistY - ystart) / coef) + xstart;

        y1 = ((maxDistX - xstart) * coef) + ystart ;
        y2 = ((minDistX - xstart) * coef) + ystart ;


        if (x1 > maxDistX){
            x1 = maxDistX;
        }
        else if ( x1 < minDistX){
            x1 = minDistX;
        }

        if (x2 > maxDistX){
            x2 = maxDistX;
        }
        else if ( x2 < minDistX){
            x2 = minDistX;
        }

        if (y1 > maxDistX ){
            y1 = maxDistY;
        } else if (y1 < minDistY){
            y1 = minDistY;
        }

        if (y2 > maxDistY ){
            y2 = maxDistY;
        }
        else if ( y2 < minDistY){
            y2 = minDistY;
        }

        lenght = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));

        return lenght;
    }


    /**
     * Compute the length of the Ray inside the square
     * @param xstart The X position of a point on the line
     * @param ystart The Y position of a point on the line
     * @param theta The angle of direction of the line
     * @return The length
     */
    public static double computeRayLenghtInCircle(double xstart,double ystart, double theta, double centerX, double centerY, double radius){
        double lenght = 0;
//        if ( Math.abs(xstart) <= radius && Math.abs(ystart) <= radius) {

        double x1 = 0;
        double x2 = 0;

        double y1 = 0;
        double y2 = 0;

        if (theta == 0) {
            theta = Double.MIN_VALUE;
        }

//        double coef = Math.tan(theta);
        double coef = tanR(theta, ComputationUtils.DEFAULT_ROUDING);
//        coef = Double.MAX_VALUE;

        double yCoordinate = ystart - xstart * coef;

        double A = (coef * coef + 1);
        double B = 2 * coef * (yCoordinate - centerY) - 2 * centerX;
        double C = (yCoordinate - centerY) * (yCoordinate - centerY) - radius * radius + centerX * centerX;

        if (B * B - 4 * A * C > 0) {
            x1 = (-B - Math.sqrt(B * B - 4 * A * C)) / (2 * A);
            x2 = (-B + Math.sqrt(B * B - 4 * A * C)) / (2 * A);
            y1 = x1 * coef + yCoordinate;
            y2 = x2 * coef + yCoordinate;

        } else {
            return 0;
        }

        lenght = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));

        return lenght;
    }

    /**
     * Compute Intersection points between a line and a square.
     * @param xstart The X position of a point on the line
     * @param ystart The Y position of a point on the line
     * @param theta The angle of direction of the line
     * @param minDistX The min X coordinate of the square
     * @param minDistY The min Y coordinate of the square
     * @param maxDistX The max X coordinate of the square
     * @param maxDistY The max Y coordinate of the square
     * @return {x1,y1,x2,y2} the pairs of coordonate given the intersection with the square
     */
    public static double[] computeIntersectionLineInSquare(double xstart,double ystart, double theta, double minDistX, double minDistY, double maxDistX, double maxDistY){
        double x1 = 0;
        double x2 = 0;

        double y1 = 0;
        double y2 = 0;

//        if ( theta == 0 ){
//            theta = Double.MIN_VALUE;
//        }


//        if ( theta > Math.PI ){
//            theta *= -1;
//        }

//        double coef = ComputationUtils.tanR(-theta, ComputationUtils.DEFAULT_ROUDING);
        double coef = Math.abs(Math.tan(theta));
//        double coef = Math.tan(theta);

        x1 = ((maxDistY - ystart) / coef) + xstart;
        x2 = ((minDistY - ystart) / coef) + xstart;

        y1 = ((maxDistX - xstart) * coef) + ystart ;
        y2 = ((minDistX - xstart) * coef) + ystart ;


        if (x1 > maxDistX){
            x1 = maxDistX;
        }
        else if ( x1 < minDistX){
            x1 = minDistX;
        }

        if (x2 > maxDistX){
            x2 = maxDistX;
        }
        else if ( x2 < minDistX){
            x2 = minDistX;
        }

        if (y1 > maxDistX ){
            y1 = maxDistY;
        }
        else if ( y1 < minDistY){
            y1 = minDistY;
        }

        if (y2 > maxDistY ){
            y2 = maxDistY;
        }
        else if ( y2 < minDistY){
            y2 = minDistY;
        }

        return new double[]{x1,y1,x2,y2};
    }


    /**
     * Compute Intersection points between a line and a square.
     * @param coefLine The director coefficient of the first line
     * @param ordonateToOrigine The ordinate to origin component of the first line
     * @param coefLine2 The director coefficient of the second line
     * @param ordonateToOrigine2 The ordinate to origin component of the second line
     * @return {x1,y1,x2,y2} the pairs of coordonate given the intersection with the square
     */
    public static double[] computeIntersectionLine(double coefLine, double ordonateToOrigine, double coefLine2, double ordonateToOrigine2){
        double x = 0;
        double y = 0;

        x = (ordonateToOrigine2-ordonateToOrigine)/(coefLine-coefLine2);
        y = x*coefLine + ordonateToOrigine;

        return new double[]{x,y};
    }

    public static double[] computeIntersectionLineV(double coefLine, double ordonateToOrigine, double k){
//        double x = 0;
        double y = 0;

        y = k*coefLine + ordonateToOrigine;
//        y = (k-ordonateToOrigine)/(coefLine);
//        y = x*coefLine + ordonateToOrigine;

        return new double[]{k,y};
    }

    public static double[] computeIntersectionLineH(double coefLine, double ordonateToOrigine, double k){
//        double x = 0;
        double x = 0;

        x = (k/coefLine) + ordonateToOrigine;
//        y = (k-ordonateToOrigine)/(coefLine);
//        y = x*coefLine + ordonateToOrigine;

        return new double[]{x,k};
    }

    /**
     * Compute the Ordinate to Origin value of a Linear function of type y = ax+b
     * @param coefLine the director coef a
     * @param point A point of the line of function y = ax+b
     * @return The ordinate to origin b
     */
    public static double computeOrdinateToOrigin(double coefLine, double[] point){
        return point[1]-(coefLine*point[0]);
    }

    static public void divideRXImage(RXImage stack, float divider){
        stack.computeData(data -> {
            for (int i = 0; i < ((float[]) data).length; i++) {
                ((float[]) data)[i] /= divider;
            }
        });
    }

    public static void arrayBinaryInverse(int[] array){
        IntStream.range(0, array.length).parallel().forEach(i -> {
            if (array[i] > 0) {
                array[i] = 0;
            } else {
                array[i] = 1;
            }
        });
    }

    public static double round(double value, double precision ){
        value *= precision;
        return Math.round(value)/precision;
    }

    public static double round(double value ){
        return round(value,1000);
    }

    public static double cosR(double theta, double min){
        double tmpCosTheta = Math.cos(theta);

        if ( Math.abs(tmpCosTheta) < min){
            tmpCosTheta = 0;
        }

        return tmpCosTheta;
    }

    public static double sinR(double theta, double min){
        double tmpSinTheta = Math.sin(theta);

        if ( Math.abs(tmpSinTheta) < min){
            tmpSinTheta = 0;
        }

        return tmpSinTheta;
    }

    public static double tanR(double theta, double min){
        double tmpSinTheta = sinR(theta,DEFAULT_ROUDING)/cosR(theta,DEFAULT_ROUDING);


        if ( Double.isNaN(tmpSinTheta) || Double.isInfinite(tmpSinTheta) || Math.abs(tmpSinTheta) < min){
            tmpSinTheta = 100000;
        }

        tmpSinTheta *= 1000;
        tmpSinTheta = Math.round(tmpSinTheta)/1000;

        return tmpSinTheta;
    }

    /**
     * Compute the Centered FFT
     * @param fft_r The Real Part of the FFT
     * @param width The width of the FFT
     * @param height THe Height of the FFT
     */
    public static void centerFFT(float[] fft_r, int width, int height){
        for ( int y = 0; y < height ; y++){
            for ( int x = 0; x < width ; x++){
                fft_r[x + (y)*width] *= Math.pow(-1,x+y);
            }
        }
    }


    public static RXImage convertDimension(RXImage imageStack, OrientationType dimOriType, OrientationType dimFinalType){
        float[] result = new float[imageStack.getGlobalSize()],pixels;
        int[] pos,newDims = getDimension(imageStack,dimOriType,dimFinalType);
        RXImage resultStack = new RXImage(newDims[0],newDims[1],newDims[2]);

        pixels  =  imageStack.copyData();
        for ( int i = 0; i < imageStack.getSize(); i++){
            int positionZ = i*imageStack.getWidth()*imageStack.getHeight();
            for ( int j = 0; j < imageStack.getHeight(); j++){
                int positionY = j*imageStack.getWidth();
                for ( int k = 0; k < imageStack.getWidth(); k++){
                    pos = getDimension(k,j,i,dimOriType,dimFinalType);
                    result[pos[0]+pos[1]*newDims[0]+pos[2]*newDims[0]*newDims[1]]=pixels[k+positionY+positionZ];
                }
            }
        }

        resultStack.setData(result);

        return resultStack;
    }

    private static int[] getDimension(RXImage imageStack, OrientationType typeOri, OrientationType typeChanged){
//        int dimensionSize = this.getDimension(imageStack,dim);
        int[] result_Dims = new int[3];
        result_Dims[0] = imageStack.getWidth();
        result_Dims[1] = imageStack.getHeight();
        result_Dims[2] = imageStack.getSize();
        switch (typeOri){
            case XYZ:
                if ( OrientationType.XZY == typeChanged ){
                    result_Dims[0] = imageStack.getWidth();
                    result_Dims[1] = imageStack.getSize();
                    result_Dims[2] = imageStack.getHeight();
                }
                else if ( OrientationType.ZYX == typeChanged ){
                    result_Dims[0] = imageStack.getSize();
                    result_Dims[1] = imageStack.getHeight();
                    result_Dims[2] = imageStack.getWidth();
                }
                break;
            case XZY:
                if ( OrientationType.XYZ == typeChanged ){
                    result_Dims[0] = imageStack.getWidth();
                    result_Dims[1] = imageStack.getSize();
                    result_Dims[2] = imageStack.getHeight();
                }
                else if ( OrientationType.ZYX == typeChanged ){
                    result_Dims[0] = imageStack.getHeight();
                    result_Dims[1] = imageStack.getSize();
                    result_Dims[2] = imageStack.getWidth();
                }
                break;
            case ZYX:
                if ( OrientationType.XYZ == typeChanged ){
                    result_Dims[0] = imageStack.getSize();
                    result_Dims[1] = imageStack.getHeight();
                    result_Dims[2] = imageStack.getWidth();
                }
                else if ( OrientationType.XZY == typeChanged ){
                    result_Dims[0] = imageStack.getSize();
                    result_Dims[1] = imageStack.getWidth();
                    result_Dims[2] = imageStack.getHeight();
                }
                break;
        }
        return result_Dims;
    }

    private static int[] getDimension(int x, int y, int z, OrientationType fromDimType, OrientationType toDimType){
//        int dimensionSize = this.getDimension(imageStack,dim);
        int[] result_Dims = new int[3];
        result_Dims[0] = x;
        result_Dims[1] = y;
        result_Dims[2] = z;
        switch (fromDimType){
            case XYZ:
                if ( OrientationType.XZY == toDimType ){
                    result_Dims[0] = x;
                    result_Dims[1] = z;
                    result_Dims[2] = y;
                }
                else if ( OrientationType.ZYX == toDimType ){
                    result_Dims[0] = z;
                    result_Dims[1] = y;
                    result_Dims[2] = x;
                }
                break;
            case XZY:
                if ( OrientationType.XYZ == toDimType ){
                    result_Dims[0] = x;
                    result_Dims[1] = z;
                    result_Dims[2] = y;
                }
                else if ( OrientationType.ZYX == toDimType ){
                    result_Dims[0] = y;
                    result_Dims[1] = z;
                    result_Dims[2] = x;
                }
                break;
            case ZYX:
                if ( OrientationType.XYZ == toDimType ){
                    result_Dims[0] = z;
                    result_Dims[1] = y;
                    result_Dims[2] = x;
                }
                else if ( OrientationType.XZY == toDimType ){
                    result_Dims[0] = z;
                    result_Dims[1] = x;
                    result_Dims[2] = y;
                }
                break;
        }
        return result_Dims;
    }


    /**
     * Take the inverse Log of the current RXImage. If negative value then take 0.
     * @param imageStack The image to modify
     */
    public static void setAbsoptionCoef(RXImage imageStack){
        imageStack.computeData(data -> {
            for (int i = 0; i < (data).length; i++) {
                ( data)[i] = (float) -Math.log(data[i]);
                //Set Negative value to 0 has the coef cannot be negative
                if ( data[i] < 0 || !Float.isFinite( data[i])) {
                    data[i] = 0;
                }
            }
        });
    }

    public static void setMinimumValue(float[] array, float minimumValue, float replaceValue){
        for ( int k = 0 ; k < array.length; k ++){
            if ( array[k] < minimumValue ){
                array[k] = replaceValue;
            }
        }
    }

    private int getDimension(RXImage imageStack, int dim){
        int dimension = 0;

        if ( dim == 0){
            dimension = imageStack.getWidth();
        }else if (dim == 1){
            dimension = imageStack.getHeight();
        }else if ( dim == 2){
            dimension = imageStack.getSize();
        }

        return dimension;
    }


    /**
     * Compute Rotation of a 3D volume with the new dimension given in parameters
     * @param initArray The array to rotate
     * @param rotatedArray The resulting array
     * @param newWidth The rotatedArray width
     * @param newHeight The rotatedArray height
     * @param newSize The rotatedArray size
     * @param initWidth The initial array width
     * @param initHeight The initial array height
     * @param initSize The initial array size
     * @param reconstructionCenterOfRotationX The X center of rotation
     * @param reconstructionCenterOfRotationY The Y center of rotation
     * @param reconstructionCenterOfRotationZ The Z center of rotation
     * @param angle The euler angle of the rotation
     */
    public static void computeRotation3D(float[] initArray, float[] rotatedArray, int newWidth, int newHeight, int newSize, int initWidth, int initHeight, int initSize,
                                       float reconstructionCenterOfRotationX,float reconstructionCenterOfRotationY, float reconstructionCenterOfRotationZ,
                                       double[] angle){
        double[][] rotationMatrix = ComputationUtils.computeRotationMatrix(angle);

        float centerX = newWidth/2.0f;
        float centerY = newHeight/2.0f;
        float centerZ = newSize/2.0f;

        IntStream.range(0,newSize).parallel().forEach( z -> {
            int y,x;
            for (y = 0; y < newHeight; y++) {
                for (x = 0; x < newWidth; x++) {
                    double[] pos = ComputationUtils.computeRotatedCoordinate(x - centerX , y - centerY, z - centerZ, rotationMatrix);

                    pos[0] += reconstructionCenterOfRotationX;
                    pos[1] += reconstructionCenterOfRotationY;
                    pos[2] += reconstructionCenterOfRotationZ;
                    float value = Float.NaN;

                    if ((pos[0] >= 0 && pos[1] >= 0 && pos[2] >= 0) &&
                            (pos[0] < initWidth && pos[1] < initHeight && pos[2] < initSize)) {
                        value = (float) ComputationUtils.getBilinearInterpolationValue(pos[0],  pos[1],  (int)pos[2] * initWidth * initHeight, initHeight, initWidth, initArray);
                    }
                    rotatedArray[( (x + (y * newWidth) + (z * newHeight * newWidth)))] = value;
                }
            }
        });
    }

    /**
     * Compute Rotation of a 2D plane with the new dimension given in parameters
     * @param initArray The array to rotate
     * @param rotatedArray The resulting array
     * @param newWidth The rotatedArray width
     * @param newHeight The rotatedArray height
     * @param initWidth The initial array width
     * @param initHeight The initial array height
     * @param reconstructionCenterOfRotationX The X center of rotation
     * @param reconstructionCenterOfRotationY The Y center of rotation
     * @param angle The euler angle of the rotation
     */
    public static void computeRotation2D(float[] initArray, float[] rotatedArray, int newWidth, int newHeight, int initWidth, int initHeight,
                                       float reconstructionCenterOfRotationX,float reconstructionCenterOfRotationY,
                                       double[] angle){
        double[][] rotationMatrix = ComputationUtils.computeRotationMatrix(angle);

        float centerX = newWidth/2.0f;
        float centerY = newHeight/2.0f;

        IntStream.range(0,newHeight).parallel().forEach( y -> {
            int x;
            for (x = 0; x < newWidth; x++) {
                double[] pos = ComputationUtils.computeRotatedCoordinate(x - centerX , y - centerY, 0, rotationMatrix);

                pos[0] += reconstructionCenterOfRotationX;
                pos[1] += reconstructionCenterOfRotationY;
                float value = Float.NaN;

                if ((pos[0] >= 0 && pos[1] >= 0 ) &&
                        (pos[0] < initWidth && pos[1] < initHeight) ) {
                    value = (float) ComputationUtils.getBilinearInterpolationValue(pos[0],  pos[1], initWidth, initArray);
                }
                rotatedArray[x + (y * newWidth)] = value;
            }
        });
    }


    public static float[] padDataWithZero(RXImage imageStackToPad, int paddedWidth, int paddedHeight){
        float[] padded = new float[paddedHeight*paddedWidth*imageStackToPad.getSize()];
        float[] pixels = imageStackToPad.copyData();
        for ( int z = 0; z < imageStackToPad.getSize() ; z++) {
            for (int y = 0; y < imageStackToPad.getHeight(); y++) {
                System.arraycopy(pixels, y * imageStackToPad.getWidth() + z * imageStackToPad.getHeight() * imageStackToPad.getWidth(), padded, y*paddedWidth + z*paddedHeight*paddedWidth, imageStackToPad.getWidth());
            }
        }
        return padded;
    }

    public static float[] padDataWithZeroCenter(RXImage imageStackToPad, int paddedWidth, int paddedHeight){
        float[] padded = new float[paddedHeight*paddedWidth*imageStackToPad.getSize()];
        float[] pixels = imageStackToPad.copyData();
        for ( int z = 0; z < imageStackToPad.getSize() ; z++) {
            for (int y = 0; y < imageStackToPad.getHeight(); y++) {
                System.arraycopy(pixels,y * imageStackToPad.getWidth() + z * imageStackToPad.getHeight() * imageStackToPad.getWidth(), padded,paddedWidth/4 + (y+paddedHeight/4)*paddedWidth + z*paddedHeight*paddedWidth, imageStackToPad.getWidth());
            }
        }
        return padded;
    }

    public static float[] padDataWithCosineCenter(RXImage imageStackToPad, int paddedWidth, int paddedHeight){
        int beginW = (paddedWidth - imageStackToPad.getWidth())/2;
        int beginH = (paddedHeight - imageStackToPad.getHeight())/2;
        float[] padded = new float[paddedHeight*paddedWidth*imageStackToPad.getSize()];
        float[] pixels = imageStackToPad.copyData();
        for ( int z = 0; z < imageStackToPad.getSize() ; z++) {
            for (int y = 0; y < imageStackToPad.getHeight(); y++) {
                System.arraycopy(pixels, y * imageStackToPad.getWidth() + z * imageStackToPad.getHeight() * imageStackToPad.getWidth(), padded,beginW + (y+beginH) * paddedWidth + z*paddedHeight*paddedWidth, imageStackToPad.getWidth());
            }
        }

//        for ( int z = 0; z < imageStackToPad.getSize() ; z++) {
//            float[] pixels = (float[]) imageStackToPad.getPixels(z + 1);
//            for (int y = 0; y < imageStackToPad.getHeight(); y++) {
//                //Left - Middle
//                for ( int x = 0 ; x < beginW ; x++ ){
//                    padded[x + (y+beginH) * paddedWidth + z * paddedHeight * paddedWidth] = pixels[y*imageStackToPad.getWidth()];
//                }
//                //Right - Middle
//                for ( int x = beginW+imageStackToPad.getWidth(); x < paddedWidth ; x++){
//                    padded[x + (y+beginH) * paddedWidth + z * paddedHeight * paddedWidth] = pixels[y*imageStackToPad.getWidth() + imageStackToPad.getWidth()-1];
//                }
//            }
//            for ( int y = 0 ; y  < beginH ; y++ ){
//                //Left - Top
//                for ( int x = 0 ; x < beginW; x++ ){
//                    padded[x + y* paddedWidth + z * paddedHeight * paddedWidth] = pixels[0];
//                }
//                //Middle - Top
//                for ( int x = beginW ; x < beginW+imageStackToPad.getWidth() ; x++ ){
//                    padded[x + y* paddedWidth + z * paddedHeight * paddedWidth] = pixels[x-beginW];
//                }
//                //Right - Top
//                for ( int x = beginW+imageStackToPad.getWidth() ; x < paddedWidth; x++ ){
//                    padded[x + y* paddedWidth + z * paddedHeight * paddedWidth] = pixels[imageStackToPad.getWidth()-1];
//                }
//            }
//            for ( int y = beginH + imageStackToPad.getHeight() ; y  < paddedHeight ; y++ ){
//                //Left - Bottom
//                for ( int x = 0 ; x < beginW; x++ ){
//                    padded[x + y* paddedWidth + z * paddedHeight * paddedWidth] = pixels[(imageStackToPad.getHeight()-1)*imageStackToPad.getWidth()];
//                }
//                //Middle - Bottom
//                for ( int x = beginW ; x < beginW+imageStackToPad.getWidth() ; x++ ){
//                    padded[x + y*paddedWidth + z * paddedHeight * paddedWidth] = pixels[x-beginW+((imageStackToPad.getHeight()-1)*imageStackToPad.getWidth())];
//                }
//                //Right - Bottom
//                for ( int x = beginW+imageStackToPad.getWidth() ; x < paddedWidth; x++ ){
//                    padded[x + y* paddedWidth + z * paddedHeight * paddedWidth] = pixels[imageStackToPad.getHeight()*imageStackToPad.getWidth()-1];
//                }
//            }
//        }

//        float[] pixels = (float[]) imageStackToPad.getPixels(z + 1);
        for ( int z = 0; z < imageStackToPad.getSize() ; z++) {
            int posZ = z*imageStackToPad.getWidth()*imageStackToPad.getHeight();
            for (int y = 0; y < imageStackToPad.getHeight(); y++) {
                //Left - Middle
                double xf = 0;
                for ( int x = 0 ; x < beginW ; x++ ){
                    padded[x + (y+beginH) * paddedWidth + z * paddedHeight * paddedWidth] = (float) (Math.sin(xf * (Math.PI) / 2)*pixels[y*imageStackToPad.getWidth() + posZ]);
                    xf += 1.0d/beginW;
                }
                //Right - Middle
                xf = 0;
                for ( int x = beginW+imageStackToPad.getWidth(); x < paddedWidth ; x++){
                    padded[x + (y+beginH) * paddedWidth + z * paddedHeight * paddedWidth] = (float) (Math.cos(xf*(Math.PI/2))*pixels[y*imageStackToPad.getWidth() + posZ + imageStackToPad.getWidth()-1]);
                    xf += 1.0d/(paddedWidth-(beginW+imageStackToPad.getWidth()));
                }
            }
            double xf = 0;
            double yf = 0;
            for ( int y = 0 ; y  < beginH ; y++ ){
                //Left - Top
                for ( int x = 0 ; x < beginW; x++ ){
                    yf = 1 - ((Math.pow(x-beginW,2)+Math.pow(y-beginH,2))/(Math.pow(beginH/2.0d,2)+Math.pow(beginW/2.0d,2)));
                    if ( yf < 0 ){
                        yf = 0;
                    }
                    padded[x + y* paddedWidth + z * paddedHeight * paddedWidth] = (float)(Math.sin(yf * (Math.PI) / 2)*pixels[0 + posZ]);
//                    padded[x + y* paddedWidth + z * paddedHeight * paddedWidth] = pixels[0 + posZ];
                }
                //Middle - Top
                for ( int x = beginW ; x < beginW+imageStackToPad.getWidth() ; x++ ){
                    padded[x + y* paddedWidth + z * paddedHeight * paddedWidth] = (float) (Math.sin(xf * (Math.PI) / 2)*pixels[x-beginW+posZ]);
                }
                yf = 0;
                //Right - Top
                for ( int x = beginW+imageStackToPad.getWidth() ; x < paddedWidth; x++ ){
                    yf = 1 - (Math.pow(x-(beginW+imageStackToPad.getWidth()),2)+Math.pow(y-beginH,2))/(Math.pow(beginH/2.0d,2)+Math.pow(beginW/2.0d,2));
                    if ( yf < 0 ){
                        yf = 0;
                    }
                    padded[x + y* paddedWidth + z * paddedHeight * paddedWidth] = (float) (Math.sin(yf * (Math.PI) / 2)*pixels[posZ+imageStackToPad.getWidth()-1]);
//                    padded[x + y* paddedWidth + z * paddedHeight * paddedWidth] = pixels[posZ+imageStackToPad.getWidth()-1];
                }
                xf += 1.0d/beginH;
            }
            xf = 0;
            for ( int y = beginH + imageStackToPad.getHeight() ; y  < paddedHeight ; y++ ){
                //Left - Bottom
                for ( int x = 0 ; x < beginW; x++ ){

                    yf = ((Math.pow(x-beginW,2)+Math.pow(y-(beginH + imageStackToPad.getHeight()),2))/(Math.pow(beginH/2.0d,2)+Math.pow(beginW/2.0d,2)));
                    if ( yf > 1 ){
                        yf = 1;
                    }
                    padded[x + y* paddedWidth + z * paddedHeight * paddedWidth] = (float)(Math.cos(yf * (Math.PI / 2))*pixels[posZ+(imageStackToPad.getHeight()-1)*imageStackToPad.getWidth()]);
//                    padded[x + y* paddedWidth + z * paddedHeight * paddedWidth] = pixels[posZ+(imageStackToPad.getHeight()-1)*imageStackToPad.getWidth()];
                }
                //Middle - Bottom
                for ( int x = beginW ; x < beginW+imageStackToPad.getWidth() ; x++ ){
                    padded[x + y*paddedWidth + z * paddedHeight * paddedWidth] = (float) (Math.cos(xf*(Math.PI/2))*pixels[posZ+x-beginW+((imageStackToPad.getHeight()-1)*imageStackToPad.getWidth())]);
                }
                //Right - Bottom
                for ( int x = beginW+imageStackToPad.getWidth() ; x < paddedWidth; x++ ){
                    yf = (Math.pow(x-(beginW+imageStackToPad.getWidth()),2)+Math.pow(y-(beginH + imageStackToPad.getHeight()),2))/(Math.pow(beginH/2.0d,2)+Math.pow(beginW/2.0d,2));
                    if ( yf > 1 ){
                        yf = 1;
                    }

                    padded[x + y* paddedWidth + z * paddedHeight * paddedWidth] = (float) (Math.cos(yf*(Math.PI/2))*pixels[posZ+imageStackToPad.getHeight()*imageStackToPad.getWidth()-1]);
//                    padded[x + y* paddedWidth + z * paddedHeight * paddedWidth] = pixels[posZ+imageStackToPad.getHeight()*imageStackToPad.getWidth()-1];
                }
                xf += 1.0d/(paddedHeight-(beginH+imageStackToPad.getHeight()));
            }
        }

        return padded;
    }

    public static float[] padDataWithZero(float[] dataToPad, int paddedWidth, int paddedHeight, int width, int height){
        float[] padded = new float[paddedWidth*paddedHeight];
        for (int y = 0; y < height; y++) {
            System.arraycopy(dataToPad, y * width, padded, (y+paddedHeight/2) * paddedWidth + (paddedWidth/2), width);
        }
        return padded;
    }

    /**
     * Crop the given RXImage given the Parameters, this function is used as the inverse padding operation
     * @param imageStack The RXImage to crop
     * @param posBeginWidth The X position where the crop have to start
     * @param posBeginheight The Y position where the crop have to start
     * @param posBeginSize The Z position where the crop have to start
     * @param width The remaining width after crop
     * @param height The remaining height after crop
     * @param size The remaining size after crop
     * @return The cropped RXImage
     */
    public static RXImage cropInStack(RXImage imageStack,int posBeginWidth,int posBeginheight,int posBeginSize, int width, int height, int size){
        RXImage result = new RXImage(width,height,size);
        float[] pixels = imageStack.copyData();

        result.setData(cropInArray(pixels,posBeginWidth,posBeginheight,posBeginSize,imageStack.getWidth(),imageStack.getHeight(),imageStack.getSize(),width,height,size));

        return result;
    }

    /**
     * Crop the given Array given the Parameters, this function is used as the inverse padding operation
     * @param array The array to crop
     * @param posBeginWidth The X position where the crop have to start
     * @param posBeginheight The Y position where the crop have to start
     * @param posBeginSize The Z position where the crop have to start
     * @param width_init The Initial width of the array
     * @param height_init The Initial height of the array
     * @param size_init The Initial size of the array
     * @param width_res The remaining width after crop
     * @param height_res The remaining height after crop
     * @param size_res The remaining size after crop
     * @return The Cropped Array
     */
    public static float[] cropInArray(float[] array,int posBeginWidth,int posBeginheight,int posBeginSize,
                                      int width_init, int height_init, int size_init,
                                      int width_res, int height_res, int size_res){

        float[] result = new float[width_res*height_res*size_res];

        //For each Z position copy the array from one to the other
        for ( int z = posBeginSize; z < size_init && (z-posBeginSize) < size_res; z++){
            for (int y = posBeginheight; y < height_init && (y-posBeginheight) < height_res; y++) {
                System.arraycopy(array, posBeginWidth + y * width_init + z * width_init * height_init,
                        result, (y - posBeginheight) * width_res + (z - posBeginSize) * width_res * height_res, width_res);
            }
        }
        return result;
    }

    /**
     * Compute for each row a linear mobile mean
     * @param image The image to transform
     * @param windowSize The dimension of the window to use
     */
    public static void computeLinearMobileMean(RXImage image, int windowSize){
        //Windowing the image
        float[] data = image.copyData();
        float[] line = new float[image.getWidth()];
        for ( int z = 0; z < image.getSize() ; z++ ) {
            for (int h = 0; h < image.getHeight(); h++) {
                System.arraycopy(data, z*image.getHeight()*line.length + h * line.length, line, 0, line.length);
                line = ComputationUtils.computeMobileMean(line, 8);
                System.arraycopy(line, 0, data, z*image.getHeight()*line.length + h * line.length, line.length);
            }
        }

        //Finally copy back the data to the RXImage
        image.setData(data);
    }

    /**
     * Compute a Mobile Mean on the Given Spectrum
     * @param spectrum The spectrum array to process
     * @param windowSize The window size of the Spectrum
     * @return The new mean array
     */
    public static float[] computeMobileMean(float[] spectrum, int windowSize){
        float[] result = new float[spectrum.length];
        if ( windowSize > 3 ) {
            int border = 0, k, i;
            if (windowSize % 2 == 0) {
                border = windowSize / 2;
            } else {
                border = (windowSize - 1) / 2;
            }
            int counter = 0;
            for (k = 0; k < border; k++) {
                counter = 0;
                for (i = k + 1; i < k + border; i++) {
                    result[k] += spectrum[i];
                    counter++;
                }
                for (i = 0; i < k; i++) {
                    result[k] += spectrum[i];
                    counter++;
                }

                result[k] /= counter;
            }

            for (k = border; k < spectrum.length - border; k++) {
                for (i = k + 1; i < k + 1 + border; i++) {
                    result[k] += spectrum[i];
                }

                for (i = k - border; i < k; i++) {
                    result[k] += spectrum[i];
                }

                result[k] /= windowSize;
            }

            for (k = spectrum.length - border; k < spectrum.length; k++) {
                counter = 0;
                for (i = k + 1; i < spectrum.length; i++) {
                    result[k] += spectrum[i];
                    counter++;
                }

                for (i = k - border; i < k; i++) {
                    result[k] += spectrum[i];
                    counter++;
                }

                result[k] /= counter;
            }
        }else{
            System.arraycopy(spectrum,0,result,0,result.length);
        }
        return result;
    }


    public static float[] computeStatistic(float[] data){
        int[] s= new int[1];
        float[] result = new float[2];

        //Compute Mean
        for (s[0] = 0 ; s[0] < data.length ; s[0]++ ) {
            result[0] += data[s[0]]/(float)data.length;
        }

        //Compute Variance
        for (s[0] = 0 ; s[0] < data.length ; s[0]++ ) {
            result[1] += ((result[0] - data[s[0]])*(result[0]-data[s[0]]));
        }

        s = null;
        return result;
    }



    public static float[] computeStatistic(float[] data, boolean withUpdate){
        float[] meanSD = ComputationUtils.computeStatistic(data);

        float[] interval = {(float) (meanSD[0]-Math.sqrt(meanSD[1])), (float) (meanSD[0]+Math.sqrt(meanSD[1]))};

        int[] s= new int[1];
        float[] result = new float[2];

        //Compute Mean
        for (s[0] = 0 ; s[0] < data.length ; s[0]++ ) {
            if ( data[s[0]] <= interval[1] && data[s[0]] >= interval[0] ) {
                result[0] += data[s[0]] / (float) data.length;
            }
        }

        //Compute Variance
        for (s[0] = 0 ; s[0] < data.length ; s[0]++ ) {
            if ( data[s[0]] <= interval[1] && data[s[0]] >= interval[0] ) {
                result[1] += ((result[0] - data[s[0]]) * (result[0] - data[s[0]]));
            }
        }

        s = null;
        return result;
    }

    public static float[] computeStatistic(int[] data, boolean withUpdate){
        float[] meanSD = ComputationUtils.computeStatistic(data);

        float[] interval = {(float) (meanSD[0]-Math.sqrt(meanSD[1])), (float) (meanSD[0]+Math.sqrt(meanSD[1]))};

        int[] s= new int[1];
        float[] result = new float[2];

        //Compute Mean
        for (s[0] = 0 ; s[0] < data.length ; s[0]++ ) {
            if ( data[s[0]] <= interval[1] && data[s[0]] >= interval[0] ) {
                result[0] += data[s[0]] / (float) data.length;
            }
        }

        //Compute Variance
        for (s[0] = 0 ; s[0] < data.length ; s[0]++ ) {
            if ( data[s[0]] <= interval[1] && data[s[0]] >= interval[0] ) {
                result[1] += ((result[0] - data[s[0]]) * (result[0] - data[s[0]]));
            }
        }

        s = null;
        return result;
    }

    public static float[] computeStatistic(int[] data){
        int[] s= new int[1];
        float[] result = new float[2];

        //Compute Mean
        for (s[0] = 0 ; s[0] < data.length ; s[0]++ ) {
            result[0] += (float)data[s[0]]/(float)data.length;
        }

        //Compute Variance
        for (s[0] = 0 ; s[0] < data.length ; s[0]++ ) {
            result[1] += ((result[0] - data[s[0]])*(result[0]-data[s[0]]));
        }
        s = null;
        return result;
    }


    public static int findExtremeValue(double initPos, RXSpectrum spectrum, int frameSize){
        float[] data = spectrum.copyData();
        int i;
        int p = (int) Math.floor(initPos);
        int changeCount = 0;
        while ( p >= 0 && p < data.length ){
            float val1 = data[p];
            float val2 = 0;
            for ( i = 1 ; i < frameSize+1 ; i++ ){
                val2 += data[(int) (p+(i*Math.pow(-1,changeCount)))];
            }
            val2/=frameSize;

            if ( val1 - val2 < 0 ){
                p+=Math.pow(-1,changeCount);
            }else{
                //Change Way
                changeCount++;
                if ( changeCount > 1 ){
                    //Return the Max
                    return p;
                }
            }
        }
        return 0;
    }

    public static double[] convertHdfPositionToCartesianMap(int width, int height, int posInData){
        double[] position = new double[3];

        double xy = width*height;

        position[2] = (posInData/xy);
        position[1] = (posInData%xy)/(float)width;
        position[0] = (posInData%xy)%width;

        return position;
    }

    public static boolean arraysEquals(int[][] a, int[][] b){
        if ( a.length == b.length ){
            for ( int k = 0; k < a.length ; k++ ){
                if ( a[k].length == b[k].length ){
                    for ( int i = 0; i < a[k].length ; i++){
                        if ( a[k][i] != b[k][i] ){
                            return false;
                        }
                    }
                }else{
                    return false;
                }
            }
        }else{
            return false;
        }
        return true;
    }
}
