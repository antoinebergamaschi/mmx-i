/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj;

import com.soleil.nanoscopium.rxtomoj.controller.ModelController;
//import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJModelController;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import ij.ImageJ;
import ij.plugin.PlugIn;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : antoine bergamaschi
 *         Date: 23/11/13
 *         Time: 10:43
 */
public class RXTomoJ {
    private static RXTomoJ instance = null;
    private RXTomoJViewController view;
//    private RXTomoJModelController model;

    private ModelController  modelController;


//    private ImageJ imageJ;

    private RXTomoJ(){
        //debug only open a RXTomoJ whithout view
//        this.model = new RXTomoJModelController(true);
        this.modelController  = new ModelController();
    }

//    /**
//     * This method is called when the plugin is loaded.
//     * 'arg', which may be blank, is the argument specified
//     * for this plugin in IJ_Props.txt.
//     */
//    @Override
    public void run(String arg) {
        //Model should always be set before view because view may call Model element to build itself
//        this.model = new RXTomoJModelController();
        this.modelController  =  new ModelController();

        if(SwingUtilities.isEventDispatchThread()){
            System.out.println("use the curent Swing dispatcher Event");
            this.view = new RXTomoJViewController(this);
        }else{
            System.err.println("Use main function");
        }
        new ImageJ();
    }


    public static RXTomoJ getInstance(){
        if ( instance == null  ){
            instance = new RXTomoJ();
        }
        return instance;
    }

    public static void main(String[] args){
        SwingUtilities.invokeLater(() -> RXTomoJ.getInstance().run(null));
    }

//    public RXTomoJModelController getModel(){
//        return  this.model;
//    }

    public ModelController getModelController(){
        return  this.modelController;
    }

    public RXTomoJViewController getView(){
        return this.view;
    }

//    public ImageJ getImageJ(){
//        if ( this.imageJ == null ){
//            this.imageJ = new ImageJ();
//        }
//        return imageJ;
//    }

}
