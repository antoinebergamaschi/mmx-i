/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.controller;

import com.soleil.nanoscopium.rximage.RXData;
import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.RXSpectrum;
import com.soleil.nanoscopium.rximage.util.FilterRawFunction;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent.MMXIControllerEventID;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent.MMXIControllerEventType;
import com.soleil.nanoscopium.rxtomoj.event.MMXIEventData;
import com.soleil.nanoscopium.rxtomoj.model.CustomAnalyse;
import com.soleil.nanoscopium.rxtomoj.model.SessionObject;
import com.soleil.nanoscopium.rxtomoj.model.ShiftImage;
import com.soleil.nanoscopium.rxtomoj.model.computationFunction.ComputeDarkField.OrientationFitFunction;
import com.soleil.nanoscopium.rxtomoj.model.computationFunction.ComputePhaseContrast.PhaseComputationTechniques;
import com.soleil.nanoscopium.rxtomoj.model.computationFunction.ComputePhaseContrast.PhaseRetrievalAlgo;
import com.soleil.nanoscopium.rxtomoj.model.data.*;
import com.soleil.nanoscopium.rxtomoj.model.data.DarkFieldInfo.SpotGeometry;
import com.soleil.nanoscopium.rxtomoj.model.filters.BackgroundSubtraction;
import com.soleil.nanoscopium.rxtomoj.model.filters.HotSpot_Detection;
import com.soleil.nanoscopium.rxtomoj.model.filters.ProjectionFilter.WindowType;
import com.soleil.nanoscopium.rxtomoj.model.filters.ProjectionFilter.FilterType;
import com.soleil.nanoscopium.rxtomoj.model.fluorescence.FluorescenceElement;
import com.soleil.nanoscopium.rxtomoj.model.fluorescence.FluorescenceInfo;
import com.soleil.nanoscopium.rxtomoj.model.fluorescence.FluorescenceObject;
import com.soleil.nanoscopium.rxtomoj.model.reconstructionFunction.BasicReconstructionFunction;
import com.soleil.nanoscopium.rxtomoj.model.reconstructionFunction.ReconstructionAlgebraic;
import com.soleil.nanoscopium.rxtomoj.model.reconstructionFunction.ReconstructionFilteredBack;
import com.soleil.nanoscopium.rxtomoj.model.reconstructionFunction.BasicReconstructionFunction.ProjectionMethod;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import com.soleil.nanoscopium.rxtomoj.model.utils.FittingUtils;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;

import static com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.MotorType;
import static com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;
import static com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType.*;


/**
 * Created by antoine bergamaschi on 17/04/2015.
 */
public class DataController extends AbstractController{

    final public static int INCORRECT_ITERATION_VALUE = 0;
    final public static int INCORRECT_RELAXATION_VALUE = 1;
    final public static int INCORRECT_K_VALUE = 2;
    final public static int INCORRECT_STACKTYPE = 3;
    final public static int INCORRECT_FLUO_PATH = 4;
    final public static int INCORRECT_MODALITY_SELECTED = 5;
    final public static int INVALID_ROISPOT = 6;
    final public static int INCORRECT_FILTER_VALUE = 7;
    final public static int POSITIVE_VALUE_REQUIRED = 8;
    final public static int ERROR_NULL_VALUE = 9;
    final public static int ERROR_INCORRECT_SEGMENTATION = 10;

    /**
     * This Constructor should always be called with a non-null modelController
     *
     * @param modelController The modelController linked to this AbstractController
     */
    public DataController(ModelController modelController) {
        super(modelController);
    }


//    public DataController(ExecutorService executorService) {
//        super(executorService);
//    }


    /**
     * Manage the motors in the model, setting a null value for a motors will remove it from the computation model.
     * @param type the MotorType
     * @param path The path in the HDF5 to this motor dataset, if null this type is remove from the motors list
     */
    public void manageMotor(MotorType type, String path){
        //Validate Update Model with current Values
        HashMap<MotorType,String> motorTypeStringHashMap = _getCustomAnalyse().getMotorPath();
        HashMap<MotorType,String> c = (HashMap<MotorType, String>) motorTypeStringHashMap.clone();

        if( type != null ){
            //Test wheter the path exist
            if ( path != null ){
                //If the path already exist in the HashMap remove it
                if ( c.containsValue(path) ){
                    for ( MotorType motorType : c.keySet() ){
                        //If this is the same path and different motor type
                        if ( c.get(motorType).equalsIgnoreCase(path) && type != motorType ){
                            motorTypeStringHashMap.remove(motorType);
                            //Remove also from computed Stack
                            removeVolatileStack(motorType.getVolatilStackEnum());
                        }
                    }
                }

                //If the hash already contains the Key replace it
                if ( c.containsKey(type) ){
                    motorTypeStringHashMap.replace(type,path);
                    //Remove also from computed Stack
                    removeVolatileStack(type.getVolatilStackEnum());
                }
                //Else and a new type
                else{
                    motorTypeStringHashMap.put(type,path);
                }
            }
            //If null is the path given then remove the type
            else{
                motorTypeStringHashMap.remove(type);
                //Remove also from computed Stack
                removeVolatileStack(type.getVolatilStackEnum());
            }
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.MOTORS_CHANGED,null);
        }
        //If type is null then retrieve the path and remove it
        else{
            //Only if the path is not null
            if( path != null){
                if ( c.containsValue(path) ){
                    for ( MotorType motorType : c.keySet() ){
                        if ( c.get(motorType).equalsIgnoreCase(path) ){
                            motorTypeStringHashMap.remove(motorType);
                            //Remove also from computed Stack
                            removeVolatileStack(motorType.getVolatilStackEnum());
                            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.MOTORS_CHANGED,null);
                        }
                    }
                }
            }else{
                //Warning
                fireControllerEvent(MMXIControllerEventType.WARNING,MMXIControllerEventID.MOTORS_CHANGED,null);

            }

        }
    }


    /**
     * Store this stackData in the StoredData model.
     * Before entering the StoredData model, this stackData must be link to a valid file
     * @param stackData The StackData linking to a stored file
     */
    public void storeStack(StackData stackData){
        //Test nullity
        if ( stackData.getSystemPath() == null || !Files.exists(Paths.get(stackData.getSystemPath())) ){
            //Fire error
            MMXIEventData mmxiEventData = new MMXIEventData(ERROR_NULL_VALUE);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.STACKDATA_STORED,mmxiEventData);
            return;
        }

        ConcurrentHashMap<StackType, ArrayList<StackData>> stackDataStorage = this._getDataStorage().getStackDataStorage();
        //If the hash map do not contains the current stackType
        if ( !stackDataStorage.containsKey(stackData.getStackType()) ){
            ArrayList<StackData> arrayList = new ArrayList<>();
            arrayList.add(stackData);
            stackDataStorage.put(stackData.getStackType(),arrayList);
        }
        else{
            ArrayList<StackData> stackDatas = stackDataStorage.get(stackData.getStackType());
            //If there already was on instance of this stackData remove it
            if ( DataStorage.contains(stackDatas,stackData.getSystemPath()) ){
                DataStorage.remove(stackDatas,stackData.getSystemPath());
            }
            stackDatas.add(stackData);
        }
    }

    /**
     * Remove every StackData having this StackType identifier from the current analysis.
     * Files are not removed from the system are there are refered from ealier analysis
     * @param stackType The StackType identifier to remove every StackData
     */
    public void removeFromAnalyse(StackType stackType){
        this._getDataStorage().getStackDataStorage().remove(stackType);
    }

    /**
     * Remove This stackType from StoredData of the current analysis and thus also from file system.
     * Thus this file should be remove from every analysis perform earlier.
     * @param stackType The Type of the StackDatas to remove
     */
    public void releaseStack(StackType stackType){
        ArrayList<StackData> d = this._getDataStorage().getStackDataStorage().get(stackType);
        if ( d !=null ) {
            ArrayList<StackData> da = new ArrayList<>(d);
            for (StackData stackData : da) {
                releaseStack(stackData);
            }
        }
    }

    /**
     * Remove This StackData from StoredData of the current analysis and thus also from file system.
     * Thus this file should be remove from every analysis perform earlier.
     * @param stackData The StackData to remove
     */
    public void releaseStack(StackData stackData){
        //Remove from every DataStorage
        SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
        sessionController.removeStackData(stackData);

        //ReaLize and remove the stack from system files
        IOController ioController  = RXTomoJ.getInstance().getModelController().getIoController();
        ioController.removeStack(stackData.getSystemPath());

        //FireEvent
    }


    /**
     * Add or replace the stack in with this path and this typeOfstack
     * @param datasetName The dataset name of the spc to add/remove from the stored arrayList
     */
    @Deprecated
    public void manageSpectrum(String datasetName){
        ArrayList<FluorescenceObject> spc = this._getParameterStorage().getFluorescenceInfo().getFluoData();
        boolean isIn = false;
        for ( FluorescenceObject object : new ArrayList<>(spc)){
            if ( object.getPath().equalsIgnoreCase(datasetName)  ){
                spc.remove(object);
                isIn = true;
            }
        }

        if ( !isIn ){
            spc.add(new FluorescenceObject(datasetName));
        }
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.FLUORESCENCE_CHANGED,null);
    }

    /**
     * add the given Spectrum from the analyses flow
     * @param datasetName the name of the spectrum in the Hdf5 dataset
     */
    public void addSpectrum(String datasetName){
        ArrayList<FluorescenceObject> spc = this._getParameterStorage().getFluorescenceInfo().getFluoData();
        boolean isIn = false;
        for ( FluorescenceObject object : new ArrayList<>(spc)) {
            if (object.getPath().equalsIgnoreCase(datasetName)) {
                isIn = true;
            }
        }

        if ( !isIn ){
            spc.add(new FluorescenceObject(datasetName));
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.FLUORESCENCE_CHANGED,null);
        }
    }

    /**
     * Remove the given Spectrum from the analyses flow
     * @param datasetName the name of the spectrum in the Hdf5 dataset
     */
    public void removeSpectrum(String datasetName){
        //TODO warning remove all references linked to the fluorescence Object
        System.err.println("Error :: l.283 DataController");
        ArrayList<FluorescenceObject> spc = this._getParameterStorage().getFluorescenceInfo().getFluoData();
        for ( FluorescenceObject object : new ArrayList<>(spc)) {
            if (object.getPath().equalsIgnoreCase(datasetName)) {
                spc.remove(object);
                fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.FLUORESCENCE_CHANGED,null);
            }
        }
    }

    /**
     * Add or replace the stack in with this path and this typeOfstack
     * @param datasetName The dataset name of the spc to add/remove from the stored arrayList
     */
    public void manageXBPM(String datasetName){
        ArrayList<String> spc = RXTomoJ.getInstance().getModelController().getSessionController().getCustomAnalyse().getSAInames();
        //If the DatasetName is not null ( considered as correct )
        if ( datasetName != null ) {
            //If the spc already contains the datasetName
            if ( spc.contains(datasetName) ) {
                spc.remove(datasetName);
            }else {
                spc.add(datasetName);
            }
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.XBPM_CHANGED,null);
        }else{
            fireControllerEvent(MMXIControllerEventType.WARNING,MMXIControllerEventID.XBPM_CHANGED,null);
        }
    }



    /**
     * Add/replace RXImage in the volatileStorage class of custom analyses
     * Clear the VolatileStack from the other references to this StackType and add the new one
     * @param stackType The Stack type to retrieve
     * @param stack The stack to Store, or delete if null
     */
    public void storeVolatileStack(StackType stackType, RXImage stack){
        if ( stack != null && stackType != null ) {
            HashMap<StackData, RXData> computedStacks = RXTomoJ.getInstance().getModelController().getSessionController().getCustomAnalyse().getVolatileStorage().getComputedStack();
            StackData stackData = new StackData(stackType);

            //Clear the volatile storage from every StackData having this StackType
            for ( StackData stackData1 : new ArrayList<>(computedStacks.keySet()) ){
                if ( stackData1.getStackType() == stackType ){
                    computedStacks.remove(stackData1);
                }
            }

            //Finally add the new StackData
            computedStacks.put(stackData, ComputationUtils.copyStack(stack));
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.VOLATILE_STACK_CHANGED,null);
        }else{
            MMXIEventData mmxiEventData = new MMXIEventData(ERROR_NULL_VALUE);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.VOLATILE_STACK_CHANGED,mmxiEventData);
        }
    }

    /**
     * Add/replace RXImage in the volatileStorage class of custom analyses
     * @param stackType The Stack type to retrieve
     * @param stack The stack to Store, or delete if null
     */
    public void storeVolatileStack(StackData stackType, RXImage stack){
        if ( stack != null && stackType != null ) {
            HashMap<StackData, RXData> computedStacks = RXTomoJ.getInstance().getModelController().getSessionController().getCustomAnalyse().getVolatileStorage().getComputedStack();
            if (computedStacks.containsKey(stackType)) {
                computedStacks.replace(stackType, stack);
            } else {
                computedStacks.put(stackType, ComputationUtils.copyStack(stack));
            }
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.VOLATILE_STACK_CHANGED,null);
        }else{
            MMXIEventData mmxiEventData = new MMXIEventData(ERROR_NULL_VALUE);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.VOLATILE_STACK_CHANGED,mmxiEventData);
        }
    }



    /**
     * remove RXImage in the volatileStorage class of custom analyses
     * Clear the VolatileStack from the other references to this StackType
     * @param stackType The Stack type to retrieve
     */
    public void removeVolatileStack(StackType stackType){
        if ( stackType != null ) {
            HashMap<StackData, RXData> computedStacks = RXTomoJ.getInstance().getModelController().getSessionController().getCustomAnalyse().getVolatileStorage().getComputedStack();
            StackData stackData = new StackData();

            //Clear the volatile storage from every StackData having this StackType
            for ( StackData stackData1 : new ArrayList<>(computedStacks.keySet()) ){
                if ( stackData1.getStackType() == stackType ){
                    computedStacks.remove(stackData1);
                }
            }
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.VOLATILE_STACK_CHANGED,null);
        }else{
            MMXIEventData mmxiEventData = new MMXIEventData(ERROR_NULL_VALUE);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.VOLATILE_STACK_CHANGED,mmxiEventData);
        }
    }

    /**
     * remove RXImage in the volatileStorage class of custom analyses
     * @param stackType The Stack type to retrieve
     */
    public void removeVolatileStack(StackData stackType){
        if ( stackType != null ) {
            HashMap<StackData, RXData> computedStacks = RXTomoJ.getInstance().getModelController().getSessionController().getCustomAnalyse().getVolatileStorage().getComputedStack();

            computedStacks.remove(stackType);

            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.VOLATILE_STACK_CHANGED,null);
        }else{
            MMXIEventData mmxiEventData = new MMXIEventData(ERROR_NULL_VALUE);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.VOLATILE_STACK_CHANGED,mmxiEventData);
        }
    }

    /**
     * Get the Array list of basic stack type saved in the model and present on the filesystem
     * @return the {@link java.util.HashMap} of {StackType,String}
     */
    @Deprecated
    public ArrayList<StackType> getBasicStack(){
        ArrayList<StackType> basicStack = new ArrayList<>();

        this._getDataStorage().getStackPath().keySet().stream().filter(type -> type.isBasicType() &&
                new File(this._getDataStorage().getStackPath().get(type)).exists()).
                forEach(type -> basicStack.add(type));

        //For darkField Data and other...
        this._getDataStorage().getStackPaths().keySet().stream()
                .filter(stackType -> stackType.isBasicType())
                .forEach(stackType -> {
                    basicStack.add(stackType);
        });

        //For Fluo Data
        if ( this._getDataStorage().getFluoPath().keySet().size() > 0 ){
            basicStack.add(StackType.FLUORESCENCE);
        }

        return basicStack;
    }

    /**
     * Get the Array list of basic stack type saved in the model and present on the filesystem
     * @return the {@link java.util.HashMap} of {StackType,String}
     */
    public ArrayList<StackData> getBasicData(){
        ArrayList<StackData> basicStack = new ArrayList<>();

        this._getDataStorage().getStackDataStorage().keySet().stream().filter(type -> type.isBasicType()).
                forEach(type -> basicStack.addAll(this._getDataStorage().getStackDataStorage().get(type)));

        return basicStack;
    }



//    /**
//     * Test whenever a StackType is already computed as a stack
//     * @param type The stackType to test
//     * @return true is the stack of the type exist
//     */
//    public boolean stackExist(StackType type){
//        Set<StackType> stackTypes = this._getDataStorage().getStackPath().keySet();
//        return stackTypes.contains(type);
//    }

    /**
     * Retrieve the Path to the image stack corresponding to this type
     * @param type The type of stack to retrieve
     * @return The Path to the stack
     */
    public String getStackPath(StackType type){
        Set<StackType> stackTypes = this._getDataStorage().getStackPath().keySet();
        for ( StackType stackType : stackTypes){
            if (stackType.equals(type)){
                return this._getDataStorage().getStackPath().get(type);
            }
        }
        return "";
    }



    @Deprecated
    public RXData getStorageData(StackType type, String spc, String name){
        IOController ioController = RXTomoJ.getInstance().getModelController().getIoController();
        if ( type == StackType.FLUORESCENCE ){
            ConcurrentHashMap<String, MultiplePath> stringStringHashMap = this._getDataStorage().getFluoPath();
            RXData imageStack = null;
            if ( stringStringHashMap.containsKey(spc) && stringStringHashMap.get(spc).getMap().containsKey(name)  ){
                imageStack = ioController.laodStack(stringStringHashMap.get(spc).getMap().get(name), type);
            }else{
                System.err.println("No stack has been found for stack type"+type);
            }
            return imageStack;
        }else{
            return null;
        }
    }

    /**
     * Retrieve The first Image find marked with this StackType
     * @param type The stackType to retrieve
     * @return The Resulting Data
     */
    public ArrayList<RXData> getStackImages(StackType type){
        ConcurrentHashMap<StackType,ArrayList<StackData>> stackTypeArrayList = this._getDataStorage().getStackDataStorage();

        ArrayList<RXData> imageStacks = new ArrayList<>();
        if (stackTypeArrayList.containsKey(type)) {
            for ( StackData stackData : stackTypeArrayList.get(type) ){
                imageStacks.add(getStorageData(stackData));
            }
        } else {
            System.err.println("No stack has been found for stack type " + type);
        }
        return imageStacks;
    }

    /**
     * Retrieve this StackData from the fileSystem
     * @param stackData The StackData object containing information about the data being retrieved
     * @return The loaded data
     */
    public RXData getStorageData(StackData stackData){
        RXData imageStack = null;
        if ( stackData == null ){
            //Fire Null error
            //Fire error
            MMXIEventData mmxiEventData = new MMXIEventData(ERROR_NULL_VALUE);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.STACKDATA_STORED,mmxiEventData);
        }
        else {
            IOController ioController = RXTomoJ.getInstance().getModelController().getIoController();
            imageStack = ioController.laodStack(stackData.getSystemPath(), stackData.getStackType());
        }
        return imageStack;
    }


    /**
     * Get a Safe copy of the currently selected Motors and assigned motors type
     * @return The HashMap containing the motors Path in Hdf5 and type
     */
    public HashMap<MotorType, String> getSelectedMotors(){
        return new HashMap<>(RXTomoJ.getInstance().getModelController().getSessionController().getCustomAnalyse().getMotorPath());
    }

    //******************************************************************************************************************
    //******************************************************************************************************************
    //************************************* GLOBAL INFO DATACONTROLLER *************************************************
    //******************************************************************************************************************
    //******************************************************************************************************************

    /**
     * Add this stackType from the list of stack to reduce
     * @param stackType The StackType should contains the information to retrieve it
     */
    public void addToReduce(StackType stackType){

    }

    /**
     * Remove this stackType from the list of stack to reduce
     * @param stackType The StackType should contains the information to retrieve it
     */
    public void removeToReduce(StackType stackType){

    }

    /**
     * Add this stackType to the list of stack to reconstruct
     * @param toReconstruct The StackType to select every StackData to reconstruct
     */
    public void addToReconstruct(StackType toReconstruct){

    }

    /**
     * Add this stackType to the list of stack to reconstruct
     * @param toReconstruct The stackData to reconstruct
     */
    public void addToReconstruct(StackData toReconstruct){

    }

    /**
     * Remove this stackType from the list of stack to reconstruct
     * @param toReconstruct The StackType to select every StackData to remove from reconstruction process
     */
    public void removeToReconstruct(StackType toReconstruct){

    }

    /**
     * Remove this stackType from the list of stack to reconstruct
     * @param toReconstruct The stackData to reconstruct
     */
    public void removeToReconstruct(StackData toReconstruct){

    }

    //******************************************************************************************************************
    //******************************************************************************************************************
    //****************************** NORMALIZATION INFO DATACONTROLLER *************************************************
    //******************************************************************************************************************
    //******************************************************************************************************************

    /**
     * Set whenever the angular range should be used or also fitted by the sine function
     * @param bol True if the angular range is fixe ( else will be fitted )
     */
    public void setUsePrecomputedAngularStep(boolean bol){
        this._getNormalizationInfo().setUsePrecomputedAngularStep(bol);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.WOBBLE_PARAMETER_CHANGED,null);
    }

    /**
     * Update the angular range acquired in one rotation of the sample
     * @param range The range between ]0;2pi]
     */
    public void setAngularRange(double range){
        //Range should be between 0 and 2*PI ( even if normally 0 is impossible )
        if ( range > 0 && range < 2*Math.PI) {
            this._getNormalizationInfo().setAngularRange(range);
            fireControllerEvent(MMXIControllerEventType.UPDATE, MMXIControllerEventID.WOBBLE_PARAMETER_CHANGED,null);
        }else{
            MMXIEventData mmxiEventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.WOBBLE_PARAMETER_CHANGED,mmxiEventData);
        }
    }

    /**
     * Update the minimum value of shift before a real displacement is apply
     * @param minimumShiftValue Value typically between 0-1 (but can be greater than 1
     * @see NormalizationInfo
     */
    public void setMinimumShiftValue(float minimumShiftValue){
        if ( minimumShiftValue >= 0){
            this._getNormalizationInfo().setMinimumShift(minimumShiftValue);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.WOBBLE_PARAMETER_CHANGED,null);
        }else{
            MMXIEventData mmxiEventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.WOBBLE_PARAMETER_CHANGED,mmxiEventData);
        }
    }

    /**
     * Set the number of time the wobbling correction procedure will be apply.
     * @param numberOfIteration The number of time the wobbling correction is apply ( normaly between 2-3 )
     * @see NormalizationInfo
     */
    public void setWobblingIterationCorrectionNumber(int numberOfIteration){
        if ( numberOfIteration > 0){
            this._getNormalizationInfo().setRecursiveNumber(numberOfIteration);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.WOBBLE_PARAMETER_CHANGED,null);
        }else{
            MMXIEventData mmxiEventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.WOBBLE_PARAMETER_CHANGED,mmxiEventData);
        }
    }

    /**
     * Use or not a linear interpolation function to compute the displacement of each pixel
     * @param useInterpolation True is interpolation is used
     * @see NormalizationInfo
     */
    public void useWobblingInterpolation(boolean useInterpolation){
        this._getNormalizationInfo().setUseInterpolation(useInterpolation);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.WOBBLE_PARAMETER_CHANGED,null);
    }

    /**
     * Set the window size to use when computing the wobbling correction
     * @param windowSize The size must be between 1 and 16 ( more is not relevant )
     * @see NormalizationInfo
     */
    public void setWobblingWindowParameter(int windowSize){
        if ( windowSize > 0){
            this._getNormalizationInfo().setMobileWindow(windowSize);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.WOBBLE_PARAMETER_CHANGED,null);
        }else{
            MMXIEventData mmxiEventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.WOBBLE_PARAMETER_CHANGED,mmxiEventData);
        }
    }

    /**
     * If use the windowing in wobbling correction algorithm
     * @param useWindowing True if used
     * @see NormalizationInfo
     */
    public void useWobblingWindowParameter(boolean useWindowing){
        this._getNormalizationInfo().setUseMobileMean(useWindowing);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.WOBBLE_PARAMETER_CHANGED,null);
    }

    /**
     * Define if the wobble correction have to be performed during the computation process
     * @param useWobbleCorrection True if the wobble correction is included in the computation
     * @see NormalizationInfo
     */
    public void setUseWobbleCorrection(boolean useWobbleCorrection){
        this._getNormalizationInfo().setDoWobbleCorrection(useWobbleCorrection);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.WOBBLE_PARAMETER_CHANGED,null);
    }

    /**
     * Set the interpolation distance to use when regridding an image with the motors position
     * @param distance The maximal distance to search for neighbourg
     * @see NormalizationInfo
     */
    public void setRegriddingInterpolationDistance(int distance){
        if ( distance >= 0){
            this._getNormalizationInfo().setInterpolationDistance(distance);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.REGRIDDING_PARAMETER_CHANGED,null);
        }else{
            MMXIEventData mmxiEventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.REGRIDDING_PARAMETER_CHANGED,mmxiEventData);
        }
    }

    /**
     * Set the interpolation distance to use when regridding a fluorescence image with the motors position
     * @param distance The maximal distance to search for neighbourg ( often 0 here )
     * @see NormalizationInfo
     */
    public void setRegriddingInterpolationDistance_fluorescence(int distance){
        if ( distance >= 0){
            this._getNormalizationInfo().setInterpolationDistance_fluorescence(distance);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.REGRIDDING_PARAMETER_CHANGED,null);
        }else{

            MMXIEventData mmxiEventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.REGRIDDING_PARAMETER_CHANGED,mmxiEventData);
        }
    }

    /**
     * Set the distance in pixel in which the signal is integrated
     * @param distance The distance in pixel of integration
     * @see NormalizationInfo
     */
    public void setBackgroundIntegrationDistance(int distance){
        if ( distance > 0 ){
            if ( distance != this._getNormalizationInfo().getNumberOfForeseenPixel()) {
                this._getNormalizationInfo().setNumberOfForeseenPixel(distance);

                //Reset the precomputed background values
                resetBackgroundPrecomputedValues();

                fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.BACKGROUND_PARAMETER_CHANGED,null);
            }
        }else{
            MMXIEventData mmxiEventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.BACKGROUND_PARAMETER_CHANGED,mmxiEventData);
        }
    }

    /**
     * The algorithm will search for value from left to right
     * @param useLeft True if left computation is needed
     * @see NormalizationInfo
     */
    public void setBackgroundLeftComputation(boolean useLeft){
        if ( useLeft != this._getNormalizationInfo().isComputeLeft() ) {
            this._getNormalizationInfo().setComputeLeft(useLeft);

            //Reset the precomputed background values
            resetBackgroundPrecomputedValues();

            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.BACKGROUND_PARAMETER_CHANGED,null);
        }
    }

    /**
     * The algorithm will search for value from right to left
     * @param useRIght True if right computation is needed
     * @see NormalizationInfo
     */
    public void setBackgroundRightComputation(boolean useRIght){
        if ( useRIght != this._getNormalizationInfo().isComputeRigth() ) {
            this._getNormalizationInfo().setComputeRigth(useRIght);

            //Reset the precomputed background values
            resetBackgroundPrecomputedValues();

            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.BACKGROUND_PARAMETER_CHANGED,null);
        }
    }

    /**
     * The the integration value after which it can be supposed that we entered the sample
     * @param value The integration value separing background from sample ( normally between 0.2-2 ) depending on the intensity level
     * @see NormalizationInfo
     */
    public void setBackgroundMaximalIntegrationValue(float value){
        if ( value > 0 ){
            if( value != this._getNormalizationInfo().getVariationCoefficient()) {
                this._getNormalizationInfo().setVariationCoefficient(value);

                //Reset the precomputed background values
                resetBackgroundPrecomputedValues();

                fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.BACKGROUND_PARAMETER_CHANGED,null);
            }
        }else{
            MMXIEventData mmxiEventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.BACKGROUND_PARAMETER_CHANGED,mmxiEventData);
        }
    }

    /**
     * Remove data linked with the pre-computation of the Background limits
     * This function have to be called before recomputing. If not old data with always be takken.
     */
    public void resetBackgroundPrecomputedValues(){
        removeVolatileStack(COMPUTED_BACKGROUND_LIMITS);
        removeVolatileStack(COMPUTED_BACKGROUND_TYPES);
        removeVolatileStack(PRECOMPUTED_BACKGROUND_NORMALIZED);
        System.out.println("---------- Background Values have been reset ----------");
    }

    /**
     * Define if the Background correction is included in the computation process
     * @param useBackgroundCorrection True if included
     * @see NormalizationInfo
     */
    public void setUseBackgroundCorrection(boolean useBackgroundCorrection){
        this._getNormalizationInfo().setDoBackgroundNormalization(useBackgroundCorrection);
    }

    /**
     * Define if mobile mean have to be apply before computing the backgroud initialization algorithm.
     * @param useMobileMeanBackground True if mobile mean have to be used
     * @see NormalizationInfo
     */
    public void setUseMobileMeanBackground(boolean useMobileMeanBackground){
        this._getNormalizationInfo().setUseMobileMeanBackground(useMobileMeanBackground);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.BACKGROUND_PARAMETER_CHANGED,null);
    }

    /**
     * Define the dimension of the mobile mean to use
     * @param mobileMeanSize value between 8 and 32 by default
     * @see NormalizationInfo
     */
    public void setMobileMeanSizeBackground(int mobileMeanSize){
        if ( mobileMeanSize > 3 ) {
            this._getNormalizationInfo().setMobileMeanSizeBackground(mobileMeanSize);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.BACKGROUND_PARAMETER_CHANGED,null);
        }else{
            MMXIEventData mmxiEventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.BACKGROUND_PARAMETER_CHANGED,mmxiEventData);
        }
    }

    /**
     * Update the volatile stack used to compute the sinogram fit parameters
     * @param valueX The x value for each center of mass, stored in StackType.LINE_POSITION
     * @param centerOfMass The center of mass for each x value, stored in StackType.CENTER_OF_MASS
     */
    public void updateFitCurve(RXImage valueX, RXImage centerOfMass){
        //Save in Volatile Storage
        this.storeVolatileStack(StackType.CENTER_OF_MASS, centerOfMass);
        this.storeVolatileStack(StackType.LINE_POSITIONS, valueX);

        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.SHIFT_CHANGED,null);
    }


    //******************************************************************************************************************
    //******************************************************************************************************************
    //******************************** FLUORESCENCE DATACONTROLLER *****************************************************
    //******************************************************************************************************************
    //******************************************************************************************************************

    /**
     * Get a Safe copy of the currently saved FluorescenceElements
     * @return The list of fluorescence elements currently stored in the Session Object
     */
    public ArrayList<FluorescenceElement> getFluorescenceElements(){
        return new ArrayList<>(this._getParameterStorage().getFluorescenceInfo().getElements());
    }

    /**
     * Get a Safe copy of the currently saved FluorescenceInfo
     * @return The list of FluorescenceInfo currently stored in the Session Object
     */
    public FluorescenceInfo getFluorescenceInfo(){
        return new FluorescenceInfo(this._getParameterStorage().getFluorescenceInfo());
    }

    @Deprecated
    public void setCalibrationPeakChannel(float[] value){
        System.err.println("Not Working Here, Calibration Must Be set for each Spectrum L. 527 " + this.getClass());
        for (FluorescenceObject fluorescenceObject : this._getParameterStorage().getFluorescenceInfo().getFluoData()){
            fluorescenceObject.setCalibrationPeak_channel(value);
        }
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.FLUO_CALIBRATION_CHANGED,null);
    }

    public void setCalibrationPeakChannel(float value, int pos){
        System.err.println("Not Working Here, Calibration Must Be set for each Spectrum L. 535 " + this.getClass());
//        if ( this._getParameterStorage().getFluorescenceInfo().getFluoData().size() > 0 ) {
        for (FluorescenceObject fluorescenceObject : this._getParameterStorage().getFluorescenceInfo().getFluoData()){
            float[] channel = fluorescenceObject.getCalibrationPeak_channel();
            if (pos < channel.length && pos >= 0 && value > 0) {
                channel[pos] = value;
            }
        }
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.FLUO_CALIBRATION_CHANGED,null);
    }

    public void setCalibrationPeakEnergy(float[] value){
        for ( int i = 0 ; i < value.length ;  i++ ){
            setCalibrationPeakEnergy(value[i],i);
        }
//        RXTomoJ.getInstance().getModelController().getSessionController()._getCustomAnalyse().getFluorescenceInfo().setCalibrationPeak_energy(value);
//        fireModelUpdateEvent(new MMXIControllerEvent(MMXIControllerEventType.UPDATE, this, MMXIControllerEventID.FLUO_CALIBRATION_CHANGED,null));
    }

    public void setCalibrationPeakEnergy(float value, int pos){
        for (FluorescenceObject fluorescenceObject : this._getParameterStorage().getFluorescenceInfo().getFluoData()){
            float[] energy = fluorescenceObject.getCalibrationPeak_energy();
            if ( pos < energy.length && pos >= 0 && value > 0) {
                energy[pos] = value;
            }
        }
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.FLUO_CALIBRATION_CHANGED,null);
    }

    public void setFluorescenceData(String fluoPath, float[] meanSpectrum){
        RXSpectrum rxSpectrum = new RXSpectrum();
        rxSpectrum.setData(meanSpectrum);
        setFluorescenceData(fluoPath,rxSpectrum);
    }

    public void setFluorescenceData(String fluoPath, RXSpectrum meanSpectrum){
        FluorescenceObject object = this._getFluorescenceInfo().getFluoData(fluoPath);
        if ( object == null ){
            MMXIEventData mmxiEventData = new MMXIEventData(INCORRECT_FLUO_PATH);
            fireControllerEvent(MMXIControllerEventType.WARNING,MMXIControllerEventID.FLUO_MEANSPECTRUM_CHANGED,mmxiEventData);
        }else{
            object.setMeanSpectrum(meanSpectrum);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.FLUO_MEANSPECTRUM_CHANGED,null);
        }
    }

    public boolean fluorescenceDataContains(String fluoPath){
        return this.getFluorescenceInfo().getFluoData(fluoPath) != null;
    }

    public RXSpectrum getFluorescenceData(String fluoPath){
        FluorescenceObject object = this._getFluorescenceInfo().getFluoData(fluoPath);
        if ( object != null ){
            return new RXSpectrum(object.getMeanSpectrum());
        }
        return null;
    }

    public boolean manageFluorescenceElement(FluorescenceElement fluorescenceElement){
        boolean isAdd = addFluorescenceElement(fluorescenceElement);
        if ( !isAdd ){
            removeFluorescenceElement(fluorescenceElement);
        }
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.FLUO_ELEMENTS_CHANGED,null);
        return isAdd;
    }

    public FluorescenceElement getFluorescenceElement(String fluorescenceElementName){
        return this._getParameterStorage().getFluorescenceInfo().getElement(fluorescenceElementName);
    }

    private boolean addFluorescenceElement(FluorescenceElement fluorescenceElement){
        return this._getParameterStorage().getFluorescenceInfo().addElement(fluorescenceElement);
    }

    private void removeFluorescenceElement(FluorescenceElement fluorescenceElement){
        this._getParameterStorage().getFluorescenceInfo().removeElement(fluorescenceElement);
    }

    public void removeFluorescenceElement(String fluorescenceElementName){
        removeFluorescenceElement(getFluorescenceElement(fluorescenceElementName));
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.FLUO_ELEMENTS_CHANGED,null);
    }



    private FluorescenceObject getFluorescenceObject(String spc){
        return this._getParameterStorage().getFluorescenceInfo().getFluoData(spc);
    }

    public ArrayList<RXImage> getFluorescenceStack(){
        ConcurrentHashMap<String, MultiplePath> stringPathFluoHashMap = this._getDataStorage().getFluoPath();
        ArrayList<RXImage> imageStacks = new ArrayList<>();
        for ( String spc : stringPathFluoHashMap.keySet() ){
            for ( String name : stringPathFluoHashMap.get(spc).getMap().keySet() ){
                imageStacks.add((RXImage) getStorageData(StackType.FLUORESCENCE, spc, name));
            }
        }
        return imageStacks;
    }


    /**
     * Get The spectrum selected for computation
     * @return The List of selected Spectrum
     */
    public ArrayList<String> getSelectedSpectrum(){
        ArrayList<String> result  = new ArrayList<>();
        for ( FluorescenceObject object : this._getParameterStorage().getFluorescenceInfo().getFluoData()  ){
            result.add(object.getPath());
        }
        return result;
    }


    /**
     * Test if every Selected SumSpectra have their sum spectrum already computed
     * @return True if every the FluorescenceObject have a SumSpectra computed
     */
    public boolean isSumSpectrumComputed(){
        ArrayList<StackData> computedSpectrums = _getDataStorage().getStackDataStorage().get(SUM_SPECTRA);
        for ( FluorescenceObject object : this._getParameterStorage().getFluorescenceInfo().getFluoData()  ){
            if ( computedSpectrums != null ) {
                for (StackData stackData : computedSpectrums) {
                    //Path in FluorescenceObject is the Spectrum name
                    if (stackData.getSpc().equalsIgnoreCase(object.getPath())) {
                        object.setMeanSpectrum((RXSpectrum) getStorageData(stackData));
                    }
                }
                if (object.getMeanSpectrum() == null || object.getMeanSpectrum().isEmpty()) {
                    return false;
                }
            }
            else{
                return false;
            }
        }
        return true;
    }

    //******************************************************************************************************************
    //******************************************************************************************************************
    //******************************** PHASECONTRAST DATACONTROLLER ****************************************************
    //******************************************************************************************************************
    //******************************************************************************************************************


    /**
     * Set the Mirroring to apply when using a Fourier phase retrieval algorithm
     * @param real The mirroring type to apply to the real value
     * @param imaginary The mirroring type to apply to the imaginary value
     * @see PhaseContrastInfo
     */
    public void setPhaseContrastMirroring(ShiftImage.MirroringType real, ShiftImage.MirroringType imaginary){
        this.setPhaseContrastMirroring_real(real);
        this.setPhaseContrastMirroring_img(imaginary);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.PHASE_MIRRORING_CHANGED,null);
    }

    /**
     * Set the mirroring to the real value
     * @param real The mirroring type to apply to the real value
     * @see PhaseContrastInfo
     */
    private void setPhaseContrastMirroring_real(ShiftImage.MirroringType real){
        this._getPhaseContrastInfo().setReal(real);
    }

    /**
     * Set the mirroring to the imaginary value
     * @param imaginary The mirroring type to apply to the imaginary value
     * @see PhaseContrastInfo
     */
    private void setPhaseContrastMirroring_img(ShiftImage.MirroringType imaginary){
        this._getPhaseContrastInfo().setImaginary(imaginary);
    }

    /**
     * The reduction technique used, by default this is center of gravity. Intensity has been said to work better
     * for quantification?
     * @param phaseComputationTechnique The computation technique to retrieve the gradient value in X/Y direction
     * @see PhaseContrastInfo
     */
    public void setPhaseComputationTechnique(PhaseComputationTechniques phaseComputationTechnique){
        this._getPhaseContrastInfo().setPhaseComputationTechniques(phaseComputationTechnique);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.PHASE_COMPUTATION,null);
    }

    /**
     * The reconstruction technique used to retrieve the phase contrast. Can be Fourier or Southwell techniques.
     * @param phaseRetrievalAlgo The algorithm to use to compute the phase
     * @see PhaseContrastInfo
     */
    public void setPhaseRetrievalAlgo(PhaseRetrievalAlgo phaseRetrievalAlgo){
        this._getPhaseContrastInfo().setPhaseRetrievalAlgo(phaseRetrievalAlgo);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.RETRIEVAL_ALGO,null);
    }

    /**
     * Whenever the mirroring have to be use or not
     * @param useMirroring True if the mirroring has to be used
     * @see PhaseContrastInfo
     */
    public void setUseMirroring(boolean useMirroring){
        this._getPhaseContrastInfo().setUseMirroring(useMirroring);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.USE_MIRRORING,null);
    }

    /**
     * Whenever the normalizeX has to be computed or not.
     * The normalization consist of subtracting the mean value.
     * If False normalize by the center of the spot.
     * @param normalizeX True if the normalization is computed
     * @see PhaseContrastInfo
     */
    public void setGradientNormalizeX(boolean normalizeX){
        this._getPhaseContrastInfo().setNormalizeX(normalizeX);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.PHASE_COMPUTATION,null);
    }

    /**
     * Whenever the normalizeY has to be computed or not.
     * The normalization consist of subtracting the mean value.
     * If False normalize by the center of the spot.
     * @param gradientNormalizeY True if the normalization is computed
     * @see PhaseContrastInfo
     */
    public void setGradientNormalizeY(boolean gradientNormalizeY){
        this._getPhaseContrastInfo().setNormalizeY(gradientNormalizeY);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.PHASE_COMPUTATION,null);
    }

    /**
     * Maximum number of iteration should always be positive
     * Normal value should be between 1000 and 100.000
     * @param numberOfIteration The new maximum number of iteration
     * @see PhaseContrastInfo
     */
    public void setPhaseRetrievalMaximunNumberOfIteration(int numberOfIteration){
        if ( numberOfIteration > 0 ){
            this._getPhaseContrastInfo().setMaximumNumberOfIteration(numberOfIteration);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.PHASE_COMPUTATION,null);
        }else{
            MMXIEventData mmxiEventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.PHASE_COMPUTATION,mmxiEventData);
        }
    }

    /**
     * Expected error per pixel. This value should typically be between 0.01 and 0.00001
     * @param expectedError The new expected error per pixel
     * @see PhaseContrastInfo
     */
    public void setPhaseRetrievalExpectedError(double expectedError){
        if ( expectedError > 0 ){
            this._getPhaseContrastInfo().setExpectedErrorPerPixel(expectedError);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.PHASE_COMPUTATION,null);
        }else{
            MMXIEventData mmxiEventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.PHASE_COMPUTATION,mmxiEventData);
        }
    }

    /**
     * The minimal difference value between two step of the iterative procedure. Typical values are between
     * 0.0001 and 0.000001
     * @param minimumStepValue The new minimal step value between two iterative step
     * @see PhaseContrastInfo
     */
    public void setPhaseRetrievalMinimumStepValue(double minimumStepValue){
        if ( minimumStepValue > 0 ){
            this._getPhaseContrastInfo().setMinimalStepValue(minimumStepValue);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.PHASE_COMPUTATION,null);
        }else{
            MMXIEventData mmxiEventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.PHASE_COMPUTATION,mmxiEventData);
        }
    }


    //******************************************************************************************************************
    //******************************************************************************************************************
    //********************************** MISCELLIOUS DATACONTROLLER ****************************************************
    //******************************************************************************************************************
    //******************************************************************************************************************


    /**
     * Retrieve a ComputedStack from his StackType.
     * The computedStack can be in the Volatile or Permanent storage
     * @param type The StackType of the image to return
     * @return The First retrieved image ( if more than 1 ) or null
     */
    public RXImage getComputedStack(StackType type){
        HashMap<StackData, RXData> computedStacks = RXTomoJ.getInstance().getModelController().getSessionController().getCustomAnalyse().getVolatileStorage().getComputedStack();
        //First Search in the Computed Stack
        for ( StackData stackData : computedStacks.keySet() ){
            if ( stackData.getStackType() == type ){
                return (RXImage) computedStacks.get(stackData);
            }
        }

        //If not find in volatile storage
        //Then search in the saved Stack
        ArrayList<RXData> t = getStackImages(type);
        if ( t != null && t.size() > 0){
            storeVolatileStack(type, (RXImage) t.get(0));
            return (RXImage) t.get(0);
        }
        return null;
    }

    /**
     * Retrieve a ComputedStack from his StackType.
     * The computedStack can be in the Volatile or Permanent storage
     * @param stackData The StackType of the image to return
     * @return The First retrieved image ( if more than 1 ) or null
     */
    public RXImage getComputedStack(StackData stackData){
        HashMap<StackData, RXData> computedStacks = RXTomoJ.getInstance().getModelController().getSessionController().getCustomAnalyse().getVolatileStorage().getComputedStack();
        //First Search in the Computed Stack
        if ( computedStacks.containsKey(stackData) ){
            return (RXImage) computedStacks.get(stackData);
        }else{
            //Then search in the saved Stack
            RXImage t = (RXImage) getStorageData(stackData);
            if ( t != null ){
                storeVolatileStack(stackData.getStackType(), t);
            }
            return t;
        }
    }

    /**
     * Test whenever this OrientationType correspond to the one set as the initial one
     * @param type The OrientationType to test
     * @return True if type equals the default orientation type.
     */
    public boolean isCurrentOrientation(ComputationUtils.OrientationType type){
        return RXTomoJ.getInstance().getModelController().getSessionController().getCustomAnalyse().getDimension() == type;
    }


    /**
     * Set the orientation type of the retrieved data from the Hdf5
     * @param type The Orientation type to set
     */
    public void setCurrentOrientation(ComputationUtils.OrientationType type){
        RXTomoJ.getInstance().getModelController().getSessionController().getCustomAnalyse().setDimension(type);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.ORIENTATION_CHANGED,null);
    }

    /**
     * Retrieve the XBPM selected as active.
     * @return A List of the XBPM dataset name as String.
     */
    public ArrayList<String> getSelectedXBPM(){
        return new ArrayList<>(RXTomoJ.getInstance().getModelController().getSessionController().getCustomAnalyse().getSAInames());
    }

    /**
     * Get the name of the current active Image dataset, only 1 image dataset can be active once.
     * @return The dataset name as String.
     */
    public String getDatasetName(){
        return RXTomoJ.getInstance().getModelController().getSessionController().getCustomAnalyse().getDatasetName();
    }

    /**
     * Change the current active Image dataset.
     * @param data The Dataset name of the Image dataset to set.
     */
    public void setCurrentDataset(String data){
        if ( RXTomoJ.getInstance().getModelController().getSessionController().getDataset(SessionObject.DataType.IMAGE).contains(data) ) {
            RXTomoJ.getInstance().getModelController().getSessionController().getCustomAnalyse().setDatasetName(data);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.CURRENT_IMAGE_CHANGED,null);
        }else{
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.CURRENT_IMAGE_CHANGED,null);
        }
    }

    /**
     * Get the Default Image, Default image is used to computed reference Shift and Background substraction parameters
     * @return The Default RXImage.
     */
    public RXImage getDefaultRXImage(){
        return RXTomoJ.getInstance().getModelController().getComputationController().setStackOrientation(_getParameterStorage().getDefaultData(), ComputationUtils.OrientationType.XZY);
    }

    /**
     * Get the Default StackData representing the object to use as the default image
     * @return The default StackData
     */
    public StackData getDefaultModality(){
        return getParameterStorage().getDefaultData();
    }


    /**
     * Set the Default StackData representing the object to use as the default image
     */
    public void setDefaultStackType(StackData type){
        _getParameterStorage().setDefaultData(type);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.DEFAULT_MODALITY,null);
    }

    //******************************************************************************************************************
    //******************************************************************************************************************
    //********************************** REDUCTION INFO DATACONTROLLER *************************************************
    //******************************************************************************************************************
    //******************************************************************************************************************

    /**
     *
     * @param doDarkField
     * @return
     */
    public FilterRawFunction getFilterRawFunction(boolean doDarkField){
        return (data, width, height, size) -> {
            ReductionInfo reductionInfo =  _getReductionInfo();
            RXImage spot = getComputedStack(StackType.SPOT_MAP);

            HotSpot_Detection hotSpot_detection = new HotSpot_Detection();
            hotSpot_detection.setVois(reductionInfo.getRadius());
            hotSpot_detection.setSpreadCoef(reductionInfo.getSpreadCoefficient());
            hotSpot_detection.setMaxLoop(reductionInfo.getNumberOfIteration());
            if ( doDarkField ){
                hotSpot_detection.setKeepZero(true);
            }else{
                hotSpot_detection.setKeepZero(false);
            }

            hotSpot_detection.setHeigth(height);
            hotSpot_detection.setWidth(width);

            if ( doDarkField ) {
                //Filter the data with the Spot Map
                spot.computeData(l -> {
                    for (int k = 0; k < l.length; k++) {
                        if (!ComputationUtils.PixelType.isDarkFieldPixel((int) l[k])) {
                            data[k] = 0;
                        }
                    }
                });
            }else{
                //Filter the data with the Spot Map
                spot.computeData(l -> {
                    for (int k = 0; k < l.length; k++) {
                        if (ComputationUtils.PixelType.getPixelType((int) l[k]) != ComputationUtils.PixelType.SPOT ){
                            data[k] = 0;
                        }
                    }
                });
            }
            hotSpot_detection.run3D(data,1);
        };
    }

    /**
     * Update the ReductionInfo, change the spot ROI parameters,
     * @param roiSpot coordinate given as {Xmin,Xmax,Ymin,Ymax}
     */
    public void setRoiSpot(int[][] roiSpot){
        int[] maxSpotMapDimension = this._getReductionInfo().getMaxSpotDimension();
        //If ROI exist and within the spotMaps
        if ( roiSpot != null && roiSpot[0][0] >= 0 && roiSpot[0][1] < maxSpotMapDimension[0]
                && roiSpot[1][0] >= 0 && roiSpot[1][1] < maxSpotMapDimension[1]) {
            //Also test if the roiSpot is different ( avoid repetition )
            if ( !ComputationUtils.arraysEquals(roiSpot,this._getReductionInfo().getTransmissionROI())) {
                this._getReductionInfo().setTransmissionROI(roiSpot);
                fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.REDUCTIONINFO_TRANSMISSION_ROI,null);
            }
        }
        else{
            MMXIEventData eventData = new MMXIEventData(INVALID_ROISPOT);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.REDUCTIONINFO_TRANSMISSION_ROI,eventData);
        }
    }

    /**
     * Update the ReductionInfo, change the reduction ROI parameters,
     * this parameters allow to reduce only a fraction of the whole image
     * @param reductionRoi coordinate given as {Xmin,Xmax,Ymin,Ymax,Zmin,Zmax}
     */
    public void setReductionRoi(int[][] reductionRoi){
        System.out.println("Warning :: DATA not verified l.859");
        this._getReductionInfo().setReductionROI(reductionRoi);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.REDUCTIONINFO_REDUCTION_ROI,null);
    }

    /**
     * Do not use this function ( for software purpose only )
     * @param dim
     */
    public void setMaxSpotImageDimension(int[] dim){
        this._getReductionInfo().setMaxSpotDimension(dim);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.REDUCTIONINFO_SPOT_DIMENSION,null);
    }

    /**
     * Do not use this function ( for software purpose only )
     * @param dim
     */
    public void setMaxScanImageDimension(int[][] dim){
        System.out.println("Warning :: DATA not verified l.878");
        this._getReductionInfo().setMaxImageDimension(dim);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.REDUCTIONINFO_SPOT_DIMENSION,null);
    }

    /**
     * Set in the reduction info if the Dataset Image is Issue from a 2D detector
     * @param bo If the current Spot Map is issue from a 2D detector
     */
    public void set2DMap(boolean bo){
        System.out.println("Warning :: DATA not verified l.878");
        this._getReductionInfo().setIs2Ddetector(bo);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.REDUCTIONINFO_SPOT_2D,null);
    }

    /**
     * Use the low resolution mode for computing modalities
     * @param use True/False
     */
    public void useLowResolutionMode(boolean use){
        this._getReductionInfo().setUseLowResolution(use);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.LOW_RESOLUTION_MODE,null);
    }

    /**
     * Set the sampling option when reading the HDF file, sampling correspond to the number of pixel
     * separating each
     * @param sampling The sampling taken in account for the reference computation
     */
    public void setSamplingOption(int sampling){
        this._getReductionInfo().setPixelSpacing(sampling);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.SAMPLING_CHANGED,null);
    }


    /**
     * Change the number of line reading by each read call in the Hdf5 Opener
     * @param numberOfLineRead The ascked number of lines to read
     */
    public void setNumberOfLineRead(int numberOfLineRead){
        if ( numberOfLineRead > 0 ) {
            this._getReductionInfo().setNumberOfLine(numberOfLineRead);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.NUMBER_OF_LINE_READED,null);
        }else{
            MMXIEventData eventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.NUMBER_OF_LINE_READED,eventData);
        }
    }

    /**
     * Set the filter value apply when computing the spot Mask. This value must be between 0-1 exclusive.
     * This correspond to a % of redoudancy presence.
     * @param val Value between 0-1 exclusive
     */
    public void setSpotFilter(float val){
        if ( val > 0 && val < 1) {
            this._getReductionInfo().setSpotFilter(val);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.SPOT_FILTER_VALUE,null);
        }else{
            MMXIEventData eventData = new MMXIEventData(INCORRECT_FILTER_VALUE);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.SPOT_FILTER_VALUE,eventData);
        }
    }

    /**
     * Set the minimum value of a pixel to be consider as part of the SPOT
     * @param val The minimal value of a pixel belonging to the spot
     */
    public void setSpotMinimumValue(float val){
        if ( val > 0 ) {
            this._getReductionInfo().setMinimalPixelValue(val);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.SPOT_FILTER_VALUE,null);
        }else{
            MMXIEventData eventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.SPOT_FILTER_RANGE,eventData);

        }
    }

    //HotSpot Parameters

    public void setDoFilter(boolean doFilter){
        this._getReductionInfo().setDoFilter(doFilter);
    }

    public void setDoFilterForDiffusion(boolean doFilter){
        this._getReductionInfo().setDoFilter_diffusion(doFilter);
    }

    /**
     * Set the Maximum number of iteraction that the hot spot detection algo is allowed to do.
     * @param i The maximum number of iteration
     */
    public void setHotSpotDetectionIterationNumber(int i){
        if ( i > 0 ) {
            this._getReductionInfo().setNumberOfIteration(i);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.HOT_SPOT_ITERATION,null);
        }else{
            MMXIEventData eventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.HOT_SPOT_ITERATION,eventData);
        }
    }

    /**
     * Set the Hot spot algorithm kernel radius size
     * @param k The new kernel radius size
     */
    public void setHotSpotDetectionRadius(int k){
        if ( k > 0 ) {
            this._getReductionInfo().setRadius(k);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.HOT_SPOT_RADIUS,null);
        }else{
            MMXIEventData eventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.HOT_SPOT_RADIUS,eventData);
        }
    }

    /**
     * Set the Hot Spot detection algorithm the new spread coefficient
     * @param j The new spread coefficient
     */
    public void setHotSpotDetectionSpreadCoefficient(int j){
        if ( j > 0 ) {
            this._getReductionInfo().setSpreadCoefficient(j);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.HOT_SPOT_SPREADCOEF,null);
        }else{
            MMXIEventData eventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.HOT_SPOT_SPREADCOEF,eventData);
        }
    }


    /**
     * Set the Maximum number of iteraction that the hot spot detection algo is allowed to do.
     * @param i The maximum number of iteration
     */
    public void setHotSpotDetectionIterationNumberForDiffustion(int i){
        if ( i > 0 ) {
            this._getReductionInfo().setNumberOfIteration_diffusion(i);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.HOT_SPOT_ITERATION,null);
        }else{
            MMXIEventData eventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.HOT_SPOT_ITERATION,eventData);
        }
    }

    /**
     * Set the Hot spot algorithm kernel radius size
     * @param k The new kernel radius size
     */
    public void setHotSpotDetectionRadiusForDiffusion(int k){
        if ( k > 0 ) {
            this._getReductionInfo().setRadius_diffusion(k);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.HOT_SPOT_RADIUS,null);
        }else{
            MMXIEventData eventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.HOT_SPOT_RADIUS,eventData);
        }
    }

    /**
     * Set the Hot Spot detection algorithm the new spread coefficient
     * @param j The new spread coefficient
     */
    public void setHotSpotDetectionSpreadCoefficientForDiffusion(int j){
        if ( j > 0 ) {
            this._getReductionInfo().setSpreadCoefficient_diffusion(j);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.HOT_SPOT_SPREADCOEF,null);
        }else{
            MMXIEventData eventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.HOT_SPOT_SPREADCOEF,eventData);
        }
    }

    //******************************************************************************************************************
    //******************************************************************************************************************
    //********************************** RECONSTRUCTION INFO DATACONTROLLER ********************************************
    //******************************************************************************************************************
    //******************************************************************************************************************

    /**
     * Update the number of time each pixel is divided to produce smoother reconstruction image
     * @param sampling Integer between 1 and n
     */
    public void setReconstructionPixelSampling(int sampling){
        if ( sampling >  0 ) {
            _getReconstructionInfo().setNumberOfLine(sampling);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.RECONSTRUCTION_SAMPLING_CHANGED,null);
        }else{
            MMXIEventData eventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.RECONSTRUCTION_SAMPLING_CHANGED,eventData);
        }
    }

    /**
     * Update the projection/BackProjection method use to compute reconstruction
     * @param methode the ProjectionMethod to use
     */
    public void setProjectionMethode(ProjectionMethod methode ){
        this._getParameterStorage().getReconstructionInfo().setProjectionMethod(methode);
    }

    /**
     * Set the projection center of rotation
     * @param center {X,Y,...}
     */
    public void setProjectionCenterOfRotation(double... center){
        //First value is X
        if ( center.length > 0 ){
            //Test positivity
            if ( center[0] > 0 ) {
                this._getParameterStorage().getReconstructionInfo().setCenterOfRotationX(center[0]);
                fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.RECONSTRUCTION_CENTER_CHANGED,null);
            }else{
                MMXIEventData eventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
                fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.RECONSTRUCTION_CENTER_CHANGED,eventData);
            }
            //Second value is Y
            if ( center.length > 1 ){
                //Test positivity
                if ( center[1] > 0 ) {
                    this._getParameterStorage().getReconstructionInfo().setCenterOfRotationY(center[1]);
                    fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.RECONSTRUCTION_CENTER_CHANGED,null);
                }else{
                    MMXIEventData eventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
                    fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.RECONSTRUCTION_CENTER_CHANGED,eventData);
                }
            }
        }
    }


    /**
     * Set the Coeficient of rolaxation for the iterative computation functions
     * @param coef The multiplication coef apply at each iteration
     */
    public void setRelaxationCoef(double coef){
        if ( coef > 0){
            _getReconstructionInfo().setCoefOfRelaxation(coef);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.RECONSTRUCTION_RELAXATION_CHANGED,null);
        }else{
            MMXIEventData eventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.RECONSTRUCTION_RELAXATION_CHANGED,eventData);
        }
    }

    /**
     * Set the Coeficient of rolaxation for the iterative computation functions
     * @param iteration The number of time the algorithm is repeated
     */
    public void setNumberOfIteration(int iteration){
        if ( iteration > 0 ){
            _getReconstructionInfo().setNumberOfIteration(iteration);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.RECONSTRUCTION_ITERATION_CHANGED,null);
        }else{
            MMXIEventData eventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.RECONSTRUCTION_ITERATION_CHANGED,eventData);
        }
    }


    /**
     * Choose to use or not the Background normalization method
     * If used the k value must be valid
     * @param useBgNormalization True if used
     */
    public void setUseBgNormalization(boolean useBgNormalization){
        _getReconstructionInfo().setUseBgNormalization(useBgNormalization);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.RECONSTRUCTION_BGART_CHANGED,null);
    }

    /**
     * Choose to use or not the Background normalization method
     * If used the k value must be valid
     * @param k ]0,n]
     */
    public void setCoefK(float k){
        if ( k > 0 ) {
            _getReconstructionInfo().setK(k);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.RECONSTRUCTION_BGCoef_CHANGED,null);
        }else{
            MMXIEventData eventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.RECONSTRUCTION_BGCoef_CHANGED,eventData);
        }
    }

    /**
     * Choose to use or not the Background normalization method
     * If used the k value must be valid
     * @param usePositivityContrainst True if used
     */
    public void setUsePositivityContrainst(boolean usePositivityContrainst){
        _getReconstructionInfo().setUsePositivityContrainst(usePositivityContrainst);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.RECONSTRUCTION_POSITIVITY_CHANGED,null);
    }

    /**
     * Change the Filter type to apply when computing wieghted back projection
     * @param filterType The Fourrier filter to apply in case of WPB
     */
    public void setFilterType(FilterType filterType){
        _getReconstructionInfo().setFilterType(filterType);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.RECONSTRUCTION_FILTER_CHANGED,null);
    }

    /**
     * Change the Filter type to apply when computing wieghted back projection
     * @param windowType The windows to apply in addition to the FilterType
     */
    public void setWindowType(WindowType windowType){
        _getReconstructionInfo().setWindowType(windowType);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.RECONSTRUCTION_FILTER_CHANGED,null);
    }

    /**
     * Update the dimension of the windows to apply values outside are 0
     * @param dimension Dimension should be sup to 0
     */
    public void setWindowDimension(float dimension){
        if ( dimension > 0 ) {
            _getReconstructionInfo().setWindowDimension(dimension);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.RECONSTRUCTION_FILTER_CHANGED,null);
        }else{
            MMXIEventData eventData = new MMXIEventData(POSITIVE_VALUE_REQUIRED);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.RECONSTRUCTION_FILTER_CHANGED,eventData);
        }
    }

    /**
     * Choose to use or not the filter function for WBP
     * @param useFilter True if filter has to be used
     */
    public void setUseReconstructionFilter(boolean useFilter){
        _getReconstructionInfo().setUseFilter(useFilter);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.RECONSTRUCTION_FILTER_CHANGED,null);
    }


    //******************************************************************************************************************
    //******************************************************************************************************************
    //*************************************** PUBLIC DATA RETRIEVAL ****************************************************
    //******************************************************************************************************************
    //******************************************************************************************************************

    public ParameterStorage getParameterStorage(){
        return new ParameterStorage(RXTomoJ.getInstance().getModelController().getSessionController().getCustomAnalyse().getParameterStorage());
    }

    public PhaseContrastInfo getPhaseContrastInfo(){
        return this.getParameterStorage().getPhaseContrastInfo();
    }

    public ReductionInfo getReductionInfo(){
        return this.getParameterStorage().getReductionInfo();
    }

    public DarkFieldInfo getDarkFieldInfo(){
        return this.getParameterStorage().getDarkFieldInfo();
    }

    @Deprecated
    public ShiftImageInfo getShiftImageInfo(){
        return this.getParameterStorage().getShiftImageInfo();
    }

    public ReconstructionInfo getReconstructionInfo(){
        return this.getParameterStorage().getReconstructionInfo();
    }

    public NormalizationInfo getNormalizationInfo(){return this.getParameterStorage().getNormalizationInfo();}

    public GlobalInfo getGlobalInfo(){return this.getParameterStorage().getGlobalInfo();}

    public DataStorage getDataStorage(){
        return new DataStorage(RXTomoJ.getInstance().getModelController().getSessionController().getCustomAnalyse().getDataStorage());
    }

    //******************************************************************************************************************
    //******************************************************************************************************************
    //************************************** PRIVATE DATA RETRIEVAL ****************************************************
    //******************************************************************************************************************
    //******************************************************************************************************************

    private DataStorage _getDataStorage(){
        SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
        return sessionController.getCustomAnalyse().getDataStorage();
    }

    private ParameterStorage _getParameterStorage(){
        SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
        return sessionController.getCustomAnalyse().getParameterStorage();
    }

    private CustomAnalyse _getCustomAnalyse(){
        return RXTomoJ.getInstance().getModelController().getSessionController().getCustomAnalyse();
    }

    private FluorescenceInfo _getFluorescenceInfo(){
        return this._getParameterStorage().getFluorescenceInfo();
    }

    private PhaseContrastInfo _getPhaseContrastInfo(){
        return this._getParameterStorage().getPhaseContrastInfo();
    }

    private DarkFieldInfo _getDarkFieldInfo(){
        return this._getParameterStorage().getDarkFieldInfo();
    }

    @Deprecated
    private ShiftImageInfo _getShiftImageInfo(){
        return this._getParameterStorage().getShiftImageInfo();
    }

    private ReductionInfo _getReductionInfo(){
        return this._getParameterStorage().getReductionInfo();
    }

    private ReconstructionInfo _getReconstructionInfo(){
        return this._getParameterStorage().getReconstructionInfo();
    }

    private NormalizationInfo _getNormalizationInfo(){
        return this._getParameterStorage().getNormalizationInfo();
    }

    private GlobalInfo _getGlobalInfo(){return this._getParameterStorage().getGlobalInfo();}

    //******************************************************************************************************************
    //******************************************************************************************************************
    //************************************ DARKFIELD DATACONTROLLER ****************************************************
    //******************************************************************************************************************
    //******************************************************************************************************************

    //TODO change the ERROR and WARNING event of the DarkField controller, ERROR/Warning/Update should be send with the same DATA ID
    /**
     * Change the DarkField integration shape. The shape to use is directly linked to the spot shape
     * @param shape The new shape to use
     */
    public void setDarkFieldShape(SpotGeometry shape){
        this._getDarkFieldInfo().setIntegrationType(shape);
        //Fire Update
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.DARKFIELD_SHAPE_UPDATED,null);
    }

    public void addDarkFieldRange(String ID, int[] range){
        DarkFieldInfo darkFieldInfo = this._getDarkFieldInfo();
        //Test if the Name is not contained
        if ( darkFieldInfo.getDarkArea().containsKey(ID) ){
            //Fire Event
            fireControllerEvent(MMXIControllerEventType.WARNING,MMXIControllerEventID.DARKFIELD_INCORRECT_RANGE_ID,null);
            int i = 0;
            String t = ID;
            while (darkFieldInfo.getDarkArea().containsKey(t)) {
                t = ID+i;
                i++;
            }
            ID = t;
        }
        //Test if the range is correct
        if ( ! DarkFieldInfo.isCorrectRange(range) ){
            //Fire Event
            fireControllerEvent(MMXIControllerEventType.WARNING,MMXIControllerEventID.DARKFIELD_INCORRECT_RANGE,null);
            range = new int[]{0,1,0,1};
        }
        //Finally add the range
        darkFieldInfo.getDarkArea().put(ID,range);
        //Fire Update
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.DARKFIELD_RANGE_UPDATED,null);
    }

    /**
     * Symetrically update the darkField range corresponding to this ID
     * @param ID The ID of the darkfield range to modify
     * @param from The minimum distance between spot center and the darkfield area (half dimension)
     * @param to The maximal distance between the spot center and the darkfield area (half dimension)
     */
    public void updateDarkFieldRange(String ID, int from, int to){
        updateDarkFieldRange(ID,from,to,from,to);
    }

    /**
     * Update the darkField range corresponding to this ID.
     * Define a DarkField area where integration and/or orientation computation may be computed
     * @param ID The ID of the darkfield range to modify
     * @param fromX The minimum distance between spot center and the darkfield area in the first direction (half dimension)
     * @param toX The maximal distance between the spot center and the darkfield area in the first direction (half dimension)
     * @param fromY The minimum distance between spot center and the darkfield area in the second direction (half dimension)
     * @param toY The maximal distance between the spot center and the darkfield area in the second direction (half dimension)
     */
    public void updateDarkFieldRange(String ID, int fromX, int toX, int fromY, int toY){
        DarkFieldInfo darkFieldInfo = this._getDarkFieldInfo();
        int[] range = new int[]{fromX,toX,fromY,toY};
        if ( !darkFieldInfo.getDarkArea().containsKey(ID) ){
            //Fire Error
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.DARKFIELD_INCORRECT_RANGE_ID,null);
            return;
        }

        if ( ! DarkFieldInfo.isCorrectRange(range) ){
            //Fire Event
            fireControllerEvent(MMXIControllerEventType.WARNING,MMXIControllerEventID.DARKFIELD_INCORRECT_RANGE,null);
            range = new int[]{0,1,0,1};
        }

        darkFieldInfo.getDarkArea().replace(ID, range);
        //Fire Update
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.DARKFIELD_RANGE_UPDATED,null);
    }

    /**
     * Change the name of the oldID dark area to newID. If the newID already exist in the model,
     * add an integer at the end of it.
     * @param oldID The ID to change
     * @param newID The new ID to apply if possible
     * @return The newID
     */
    public String updateDarkFieldRangeID(String oldID, String newID){
        DarkFieldInfo darkFieldInfo = this._getDarkFieldInfo();
        if ( !darkFieldInfo.getDarkArea().containsKey(oldID) ){
            //Fire Error
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.DARKFIELD_INCORRECT_RANGE_ID,null);
            return "";
        }

        int[] range =  darkFieldInfo.getDarkArea().get(oldID);
        darkFieldInfo.getDarkArea().remove(oldID);

        if (darkFieldInfo.getDarkArea().containsKey(newID) ){
            //Fire Event
            fireControllerEvent(MMXIControllerEventType.WARNING,MMXIControllerEventID.DARKFIELD_INCORRECT_RANGE_ID,null);
            int i = 0;
            String t = newID;
            while (darkFieldInfo.getDarkArea().containsKey(t)) {
                t = newID+i;
                i++;
            }
            newID = t;
        }

        darkFieldInfo.getDarkArea().put(newID, range);
        //Fire Update
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.DARKFIELD_RANGE_UPDATED,null);

        return newID;
    }

    /**
     * Remove the dark area with this given ID.
     * @param ID The ID of the area to remove
     */
    public void removeDarkFieldRange(String ID){
        DarkFieldInfo darkFieldInfo = this._getDarkFieldInfo();
        if ( !darkFieldInfo.getDarkArea().containsKey(ID) ){
            //Fire Error
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.DARKFIELD_INCORRECT_RANGE_ID,null);
            return;
        }

        darkFieldInfo.getDarkArea().remove(ID);
        //Fire Update
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.DARKFIELD_RANGE_UPDATED,null);
    }

    /**
     * Remove all dark area
     */
    public void removeAllDarkFieldRange(){
        DarkFieldInfo darkFieldInfo = this._getDarkFieldInfo();
        darkFieldInfo.setDarkArea(new HashMap<>());
        //Fire Update
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.DARKFIELD_RANGE_UPDATED,null);
    }

    //TODO change the location of this function
    /**
     * Compute the maximum radius applicable without going beyond the detector
     * @return The max dimension
     */
    public int getDarkFieldMaxRadius(){
        int[] maxDimension = _getReductionInfo().getMaxSpotDimension();
        int[][] red = _getReductionInfo().getTransmissionROI();
        double[] center = new double[2];

        center[0] = ((red[0][1] - red[0][0])/2.0d)+red[0][0];
        center[1] = ((red[1][1] - red[1][0])/2.0d)+red[1][0];

        return (int) Math.min(maxDimension[0]-center[0],maxDimension[1]-center[1]);
    }

    /**
     * Update the number of segment used to compute the darkfield orientation.
     * Beaware that the number of segment given may not be acceptable. If HALF fit sine function is used,
     * only even segmentation number is allowed
     * @param numberOfSegment The new number of segment
     */
    public void setDarkFieldOrientaitonSegmentation(int numberOfSegment){
        if ( numberOfSegment <= 4 ){
            MMXIEventData eventData = new MMXIEventData(ERROR_INCORRECT_SEGMENTATION);
            //Fire Error
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.DARKFIELD_ORIENTATION,eventData);
            return;
        }

        _getDarkFieldInfo().setNumberOfSegment(numberOfSegment);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.DARKFIELD_ORIENTATION,null);
    }

    /**
     * Set the function used to compute the DarkField orientation {@link OrientationFitFunction}.
     * @param orientationFunction The new Fit function
     */
    public void setDarkFieldOrientationFunction(OrientationFitFunction orientationFunction){
        _getDarkFieldInfo().setFitFunction(orientationFunction);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.DARKFIELD_ORIENTATION,null);
    }

    /**
     *
     * @param compute True if computation have to be computed
     */
    public void computeDarkFieldOrientation(boolean compute){
        _getDarkFieldInfo().setDoOrientationComputation(compute);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.DARKFIELD_ORIENTATION,null);
    }

    //******************************************************************************************************************
    //******************************************************************************************************************
    //***************************************** Test Controller ********************************************************
    //******************************************************************************************************************
    //******************************************************************************************************************

    public boolean hasImage(){
        return getDatasetName() != null && getDatasetName().length() > 0;
    }

    public boolean hasFluorescence(){
        return _getFluorescenceInfo().getFluoData() != null && _getFluorescenceInfo().getFluoData().size() > 0;
    }

    public boolean hasMotors(){
        return _getCustomAnalyse().getMotorPath() != null && _getCustomAnalyse().getMotorPath().size() > 0;
    }

    public boolean hasSai(){
        return _getCustomAnalyse().getSAInames() != null && _getCustomAnalyse().getSAInames().size() > 0;
    }

    /**
     * Check if background normalization has been computed
     * @return True if computed
     */
    public boolean isBackgroundCalibrated(){
        return false;
    }

    /**
     * Check if wobling normalization has been computed
     * @return True if computed
     */
    public boolean isWobblingCalibrated(){
        return false;
    }

    public boolean isFluorescenceCalibrated(){
        System.out.println("Calibration have to be done for each Object : true returned");
        return true;
    }

    /**
     * Whenever the Initialization Computation has already been performed
     * @return True if init
     */
    public boolean isSpotInitialized(){
        //If a spot Map exist, then the Spot is Initialized
        return _getDataStorage().getStackDataStorage().containsKey(SPOT_MAP);
//        return _getDataStorage().getStackPath().containsKey(StackType.SPOT_MAP);
    }

    /**
     * If the Image dataset is issue from a 2D detector
     * @return True if true XD
     */
    public boolean isIssueFrom2DDetector(){
        return _getReductionInfo().is2Ddetector();
    }
}
