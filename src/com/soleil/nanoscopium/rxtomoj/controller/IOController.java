/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.controller;

import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStack;
import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStackFactory;
import com.soleil.nanoscopium.rximage.IO.RXImageIO_IJ;
import com.soleil.nanoscopium.rximage.RXData;
import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.RXSpectrum;
import com.soleil.nanoscopium.rximage.exception.RXImageException;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent.MMXIControllerEventID;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent.MMXIControllerEventType;
import com.soleil.nanoscopium.rxtomoj.event.MMXIEventData;
import com.soleil.nanoscopium.rxtomoj.model.SessionObject;
import com.soleil.nanoscopium.rxtomoj.model.computationFunction.BasicComputationFunction;
import com.soleil.nanoscopium.rxtomoj.model.data.MultiplePath;
import com.soleil.nanoscopium.rxtomoj.model.data.PathFluo;
import com.soleil.nanoscopium.rxtomoj.model.data.StackData;

import java.io.File;
import java.nio.file.Paths;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;

import static com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;
import static com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType.*;

/**
 * Created by antoine bergamaschi on 17/04/2015.
 */
public class IOController extends AbstractController {

    final public static int UNABLE_TO_OPEN_HDF = 0;
    final public static int NO_IMAGE_DATASET = 1;

    /**
     * This Constructor should always be called with a non-null modelController
     *
     * @param modelController The modelController linked to this AbstractController
     */
    public IOController(ModelController modelController) {
        super(modelController);
    }


    public void saveSpotMap(RXImage stack) {
        StackData data = new StackData(SPOT_MAP);
        saveStack(data, stack);
    }

    /**
     * @param typeOfStack              identifier of the stack which will be used for computation
     * @param basicComputationFunction the class containing the resulting imageStack reconstruction
     */
    public void saveStack(StackType typeOfStack, BasicComputationFunction basicComputationFunction) {
        StackData data = new StackData(typeOfStack);
        this.saveStack(data, basicComputationFunction.getStack());
    }

    /**
     * @param stackData identifier of the stack which will be used for computation
     * @param imageStack  the resulting imageStack reconstruction
     */
    public void saveStack(StackData stackData, RXData imageStack) {
        try {
            RXImageIO_IJ rxImageIO_ij = new RXImageIO_IJ();
            DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
            String pathToFolder = SessionObject.getCurrentSessionObject().getPathToSavingFolder();
            String path = "";
            //Only test the first pixel position
            if (imageStack != null) {

                int i = 0;
                do {
                    path = pathToFolder + File.separator + stackData.getSaveName() + "_" + i + ".tif";
                    i++;
                } while (new File(path).exists());

                switch (stackData.getStackType()) {
                    case SUM_SPECTRA:
                        rxImageIO_ij.save(Paths.get(path), (RXSpectrum) imageStack);
                        //Update the File path of the stack Data
                        stackData.setSystemPath(path);

                        //Save in the Current Analyses
                        dataController.storeStack(stackData);
                        break;
                    case FLUORESCENCE:
                    case DARKFIELD:
                    case ABSORPTION_REF:
                    case ABSORPTION:
                    case MOTORS_POSITION:
                    case MOTORS_ROTATION:
                    case PHASECONTRAST:
                    case SPOT_MAP:
                    case PHASE_GRAD_X:
                    case PHASE_GRAD_Y:
                        rxImageIO_ij.save(Paths.get(path), (RXImage) imageStack);
                        //Update the File path of the stack Data
                        stackData.setSystemPath(path);

                        //Save in the Current Analyses
                        dataController.storeStack(stackData);
                        break;
                    default:
                        rxImageIO_ij.save(Paths.get(path), (RXImage) imageStack);
                        //Update the File path of the stack Data
                        stackData.setSystemPath(path);
                        //This is not saved in the DataController

//                        System.err.println("Unkown Types l.102 " + this.getClass().toString());
                        break;
                }
            }
        } catch (RXImageException e) {

            String msg = e.toString();
            for (StackTraceElement element : e.getStackTrace()) {
                msg += "\n" + element.toString();
            }

            MMXIEventData eventData = new MMXIEventData(msg);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.SAVE_IMAGE,eventData);
            e.printStackTrace();
        }
    }


    public RXData laodStack(String path, StackType type) {
        try {
            RXImageIO_IJ rxImageIO_ij = new RXImageIO_IJ();

            switch (type) {
                case SUM_SPECTRA:
                    return rxImageIO_ij.load(Paths.get(path), true);
                case DARKFIELD:
                case ABSORPTION_REF:
                case ABSORPTION:
                case MOTORS_POSITION:
                case MOTORS_ROTATION:
                case PHASECONTRAST:
                case PHASE_GRAD_X:
                case PHASE_GRAD_Y:
                case FLUORESCENCE:
                case SPOT_MAP:
                    return rxImageIO_ij.load(Paths.get(path), false);
                default:
                    System.err.println("Unkown Types l.79 " + this.getClass().toString());
                    break;
            }
        } catch (RXImageException e) {

            String msg = e.toString();
            for (StackTraceElement element : e.getStackTrace()) {
                msg += "\n" + element.toString();
            }

            MMXIEventData eventData = new MMXIEventData(msg);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.LOAD_IMAGE,eventData);
            e.printStackTrace();
        }
        //Default return
        return null;
    }

    public void removeStack(StackType typeOfStack) {
        switch (typeOfStack) {
            case FLUORESCENCE:
                ConcurrentHashMap<String, MultiplePath> fluoMap = SessionObject.getCurrentSessionObject().getAnalyseFactory().getCurrent().getDataStorage().getFluoPath();
                if (fluoMap.containsKey(typeOfStack.getSpc())) {
                    new File(fluoMap.get(typeOfStack.getSpc()).getMap().get(typeOfStack.getOtherName())).delete();
                }
                break;
            case DARKFIELD:
                ConcurrentHashMap<StackType, MultiplePath> path = SessionObject.getCurrentSessionObject().getAnalyseFactory().getCurrent().getDataStorage().getStackPaths();
                if (path.containsKey(typeOfStack)) {
                    new File(path.get(typeOfStack).getMap().get(typeOfStack.getOtherName())).delete();
                }
                break;
            case ABSORPTION:
            case ABSORPTION_REF:
            case MOTORS_POSITION:
            case MOTORS_ROTATION:
            case PHASECONTRAST:
            case SPOT_MAP:
                ConcurrentHashMap<StackType, String> hashMap = SessionObject.getCurrentSessionObject().getAnalyseFactory().getCurrent().getDataStorage().getStackPath();
                if (hashMap.containsKey(typeOfStack)) {
                    new File(hashMap.get(typeOfStack)).delete();
                }
                break;
            default:
                System.err.println("Unkown Types l.145 " + this.getClass().toString());
                break;
        }
    }

    public void removeStack(String systemPath) {
        new File(systemPath).delete();
    }

    public void removeFluo(String spc, String name) {
        StackType f = FLUORESCENCE;
        f.setOtherName(name);
        f.setSpc(spc);
        removeStack(f);
    }

    public void removeDarkField(String name) {
        StackType f = DARKFIELD;
        f.setOtherName(name);
        removeStack(f);
    }

    public Hdf5VirtualStack openVirtualStack(String datasetName) {
        return openVirtualStack(RXTomoJ.getInstance().getModelController().getSessionController().getPathToHdf5(), datasetName, false);
    }

    public Hdf5VirtualStack openVirtualStack(String hdf5File, String datasetName) {
        return openVirtualStack(hdf5File, datasetName, false);
    }

    public Hdf5VirtualStack openVirtualStack(String datasetName, boolean isSpectrum) {
        return openVirtualStack(RXTomoJ.getInstance().getModelController().getSessionController().getPathToHdf5(), datasetName, isSpectrum);
    }

    public Hdf5VirtualStack openVirtualStack(String hdf5File, String datasetName, boolean isSpectrum) {
        Hdf5VirtualStack hdf5VirtualStack = null;
        if ( new File(hdf5File).exists() ) {
            hdf5VirtualStack = Hdf5VirtualStackFactory.createVirtualStack(hdf5File, datasetName, isSpectrum);
        }

        if ( hdf5VirtualStack == null ){
            MMXIEventData eventData = new MMXIEventData(UNABLE_TO_OPEN_HDF);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.LOAD_HDF,eventData);
        }
        return hdf5VirtualStack;
    }


    public Hdf5VirtualStack openDefaultRawImage(){
        Hdf5VirtualStack virtualStack = null;
        if ( RXTomoJ.getInstance().getModelController().getSessionController().getDataset(SessionObject.DataType.IMAGE).size() == 0 ){
            //Error no IMAGE
            MMXIEventData eventData = new MMXIEventData(NO_IMAGE_DATASET);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.LOAD_HDF,eventData);
        }else{
            virtualStack = openVirtualStack(RXTomoJ.getInstance().getModelController().getSessionController().getDataset(SessionObject.DataType.IMAGE).get(0));
        }

        return virtualStack;
    }
}
