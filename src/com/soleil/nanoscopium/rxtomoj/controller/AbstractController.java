/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.controller;

import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import com.soleil.nanoscopium.rxtomoj.event.MMXIEventData;

/**
 * Abstract class that every controller should extends.
 * ( except the MMXIController and ModelController )
 *
 * Created by bergamaschi on 27/01/2016.
 */
public abstract class AbstractController {
    private ModelController modelController;

    /**
     * This Constructor should always be called with a non-null modelController
     * @param modelController The modelController linked to this AbstractController
     */
    public AbstractController(ModelController modelController){
        if ( modelController != null ){
            this.modelController = modelController;
        }
        else{
            System.err.println("Error while initializing the AbstractController :: modelController should not be null");
        }
    }

    /**
     * Fire controller events
     * @param type The MMXIControllerEvent type to be fired
     * @param id THe MMXIControllerEventID of this MMXIControllerEvent type
     */
    public void fireControllerEvent(MMXIControllerEvent.MMXIControllerEventType type,
                                    MMXIControllerEvent.MMXIControllerEventID id,
                                    MMXIEventData data){
        modelController.fireModelUpdateEvent(new MMXIControllerEvent(type,modelController,id,data));
    }

    /**
     * Add this Runnable to the associated modelController executorService.
     * @param t The Runnable to add in the queue of the ExecutorService
     */
    public void addToQueue(Runnable t){
        modelController.executorService.submit(t);
    }
}
