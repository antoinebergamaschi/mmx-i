/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.controller;

import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.util.RXUtils;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent.MMXIControllerEventID;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent.MMXIControllerEventType;
import com.soleil.nanoscopium.rxtomoj.event.MMXIEventData;
import com.soleil.nanoscopium.rxtomoj.model.SessionObject;
import com.soleil.nanoscopium.rxtomoj.model.data.NormalizationInfo;
import com.soleil.nanoscopium.rxtomoj.model.data.ParameterStorage;
import com.soleil.nanoscopium.rxtomoj.model.data.ReconstructionInfo;
import com.soleil.nanoscopium.rxtomoj.model.data.StackData;
import com.soleil.nanoscopium.rxtomoj.model.filters.ProjectionFilter;
import com.soleil.nanoscopium.rxtomoj.model.filters.ProjectionFilter.FilterType;
import com.soleil.nanoscopium.rxtomoj.model.reconstructionFunction.BasicReconstructionFunction;
import com.soleil.nanoscopium.rxtomoj.model.reconstructionFunction.ReconstructionAlgebraic;
import com.soleil.nanoscopium.rxtomoj.model.reconstructionFunction.ReconstructionFilteredBack;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.OrientationType;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.ReconstructionType;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;
import com.soleil.nanoscopium.rxtomoj.model.utils.ProgressFunction;
import tomoj.tomography.TiltSeries;
import tomoj.tomography.TomoReconstruction2;
import tomoj.tomography.projectors.*;
import tomoj.utils.GPUDevice;

import java.util.UUID;
import java.util.concurrent.ExecutorService;

/**
 * Created by antoine bergamaschi on 17/04/2015.
 */
public class ReconstructionController extends AbstractController {

    final static int INITIALIZATION_FAILS = 0;
    final static int UNKNOWN_RECONSTRUCTION_TYPE = 1;
    final static int OPENCL_NODEVICE = 2;

    /**
     * This Constructor should always be called with a non-null modelController
     *
     * @param modelController The modelController linked to this AbstractController
     */
    public ReconstructionController(ModelController modelController) {
        super(modelController);
    }

//    public static BasicReconstructionFunction getReconstruction(ModelController mc, ReconstructionType type, StackType stackType){
//        DataController dataController  = mc.getDataController();
//        SessionController sessionController = mc.getSessionController();
//        BasicReconstructionFunction reconstruction = null;
//        ParameterStorage parameterStorage = sessionController.getCustomAnalyse().getParameterStorage();
//        switch (type){
//            case ART:
//                reconstruction = parameterStorage.getReconstructionAlgebric().get(stackType);
//                if ( reconstruction == null ){
//                    reconstruction = new ReconstructionAlgebraic();
//                    dataController.manageArtReconstruction(stackType, (ReconstructionAlgebraic) reconstruction);
//                }
//                break;
//            case FBP:
//                reconstruction = parameterStorage.getReconstructionFilteredBack().get(stackType);
//                if ( reconstruction == null ){
//                    reconstruction = new ReconstructionFilteredBack();
//                    dataController.manageFbpReconstruction(stackType, (ReconstructionFilteredBack) reconstruction);
//                }
//                break;
//            default:
//                System.err.println("Unknown Reconstruction Type (" + type + "):: " + ReconstructionController.class.toString());
//                break;
//        }
//        return reconstruction;
//    }

    @Deprecated
    public BasicReconstructionFunction getReconstruction(ReconstructionType type, StackType stackType){
//        DataController dataController  = RXTomoJ.getInstance().getModelController().getDataController();
//        SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
//        BasicReconstructionFunction reconstruction = null;
//        ParameterStorage parameterStorage = sessionController.getCustomAnalyse().getParameterStorage();
//        switch (type){
//            case ART:
//                reconstruction = parameterStorage.getReconstructionAlgebric().get(stackType);
//                if ( reconstruction == null ){
//                    reconstruction = new ReconstructionAlgebraic();
//                    dataController.manageArtReconstruction(stackType, (ReconstructionAlgebraic) reconstruction);
//                }
//                break;
//            case FBP:
//                reconstruction = parameterStorage.getReconstructionFilteredBack().get(stackType);
//                if ( reconstruction == null ){
//                    reconstruction = new ReconstructionFilteredBack();
//                    dataController.manageFbpReconstruction(stackType, (ReconstructionFilteredBack) reconstruction);
//                }
//                break;
//            default:
//                MMXIControllerEventType error = MMXIControllerEventType.ERROR;
//                error.setIdentifier(UNKNOWN_RECONSTRUCTION_TYPE);
//                fireModelUpdateEvent(new MMXIControllerEvent(error, this, MMXIControllerEvent.MMXIControllerEventID.RECONSTRUCTION_CHANGED));
//                System.err.println("Unknown Reconstruction Type ("+type+"):: "+this.getClass().toString());
//                break;
//        }
        return null;
    }

//    /**
//     * Create a back projected Image of this type of imageStack
//     * @param typeOfStack identifier of the stack which will be used for computation
//     * @return A ProgressFunction class which can be used to monitor the computation process
//     */
//    public static ProgressFunction filterdBackProjection(ModelController md, StackType typeOfStack){
//        return md.getReconstructionController().filterdBackProjection(typeOfStack);
//    }

    /**
     * Create a back projected Image of this type of imageStack
     * @param stackData identifier of the stack which will be used for computation
     * @return A ProgressFunction class which can be used to monitor the computation process
     */
    public ProgressFunction filterdBackProjection(StackData stackData){
        final UUID ID = UUID.randomUUID();


        ReconstructionInfo reconstructionInfo = RXTomoJ.getInstance().getModelController().getDataController().getReconstructionInfo();
        //FIRE an initialize progress Signal to setup the UUID and retrieve the corresponding Progress event
        MMXIEventData eventData = new MMXIEventData(ID);
        fireControllerEvent(MMXIControllerEventType.INITIALIZE, MMXIControllerEvent.MMXIControllerEventID.PROGRESS_FUNCTION,eventData);

        //Create a new Reconstruction Filteredback Object based on the currently saved in the Session Object
//        ReconstructionFilteredBack reconstructionFilteredBack = new ReconstructionFilteredBack((ReconstructionFilteredBack) this.getReconstruction(ReconstructionType.FBP, stackData));
        ReconstructionFilteredBack reconstructionFilteredBack = new ReconstructionFilteredBack();
        IOController ioController = RXTomoJ.getInstance().getModelController().getIoController();
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

        //Else performs the normal computation
        RXImage imageStack = initReconstructionStack(stackData);


        if  ( imageStack == null ){
            MMXIEventData mmxiEventData = new MMXIEventData(ID,INITIALIZATION_FAILS);
            fireControllerEvent(MMXIControllerEventType.ERROR, MMXIControllerEvent.MMXIControllerEventID.PROGRESS_FUNCTION,mmxiEventData);
            return reconstructionFilteredBack;
        }

        reconstructionFilteredBack.initReconstructionDimension(imageStack);
        reconstructionFilteredBack.setDoFilter(reconstructionInfo.isUseFilter());

        ProjectionFilter projectionFilter = new ProjectionFilter(reconstructionInfo.getFilterType(),
                reconstructionInfo.getWindowType(),reconstructionInfo.getWindowDimension());

        //If Hilbert filter, remove windowing even if selected
        if ( reconstructionInfo.getFilterType() == FilterType.HILBERT ){
            projectionFilter.setWithWindow(false);
        }

        reconstructionFilteredBack.setProjectionFilter(projectionFilter);


        addToQueue(() -> {

            //TODO change here
            GPUDevice device = GPUDevice.getBestDevice();
            if ( device == null ){
                MMXIEventData mmxiEventData = new MMXIEventData(ID,OPENCL_NODEVICE);
                fireControllerEvent(MMXIControllerEventType.ERROR, MMXIControllerEvent.MMXIControllerEventID.PROGRESS_FUNCTION,mmxiEventData);
            }

            TomoReconstruction2 reconstruction = new TomoReconstruction2(imageStack.getWidth(),imageStack.getHeight(),imageStack.getWidth());
            //Perform the filter function if any
            RXImage filterImageStack = reconstructionFilteredBack.filterData(imageStack);

            //Display the filtered image
            RXUtils.RXImageToImagePlus(filterImageStack).show();

            TiltSeries tiltSeries = new TiltSeries(RXUtils.RXImageToImagePlus(filterImageStack));

            //Test Angle
            RXImage rxAngle = dataController.getComputedStack(StackType.MOTOR_R_VOLATILE);
            if ( rxAngle != null ){
                float[] rot = rxAngle.copyData();

                double[] angles = new double[imageStack.getSize()];
                for ( int i = 0; i < imageStack.getSize() ; i++ ){
                    angles[i] = rot[i];
                }
                tiltSeries.setTiltAngles(angles);
            }else{
                double[] angles = new double[imageStack.getSize()];
                for ( int i = 0; i < imageStack.getSize() ; i++ ){
                    angles[i] = i*(360.0d/imageStack.getSize());
                }

                tiltSeries.setTiltAngles(angles);
            }

            tiltSeries.setNormalize(false);

            tiltSeries.setProjectionCenter(reconstructionInfo.getCenterOfRotationX(), imageStack.getHeight() / 2.0d);


            reconstruction.initCompletion();


            VoxelProjector3DGPU projector = new VoxelProjector3DGPU(tiltSeries,reconstruction,device,null);
            projector.initForBP(0, imageStack.getHeight(), 1);
            reconstruction.WBP(tiltSeries, projector, 0, imageStack.getHeight());


            projector.updateFromGPU(0,imageStack.getHeight());

            projector.releaseCL_Memory();

//            RXImage t = reconstructionFilteredBack.retroProjections(filterImageStack);

            StackData toSave = null;

            switch (stackData.getStackType()) {
                case ABSORPTION:
                    toSave = new StackData(StackType.FBP_ABSORPTION);
                    break;
                case DARKFIELD:
                    toSave = new StackData(StackType.FBP_DARKFIELD);
                    toSave.setAdditionalName(stackData.getAdditionalName());
                    break;
                case PHASECONTRAST:
                    toSave = new StackData(StackType.FBP_PHASECONTRAST);
                    break;
                case PHASE_GRAD_X:
                    toSave = new StackData(StackType.FBP_PHASECONTRAST);
                    break;
                case FLUORESCENCE:
                    toSave = new StackData(StackType.FBP_FLUORESCENCE);
                    toSave.setAdditionalName(stackData.getAdditionalName());
                    toSave.setSpc(stackData.getSpc());
                    break;
                default:
                    System.err.println("Error stackType unknown : " + stackData);
                    break;
            }

            ioController.saveStack(toSave, RXUtils.ImagePlusToRXImage(reconstruction));
//            ioController.saveStack(toSave, t);

            MMXIEventData eventData1 = new MMXIEventData(ID);
            fireControllerEvent(MMXIControllerEventType.FINISHED, MMXIControllerEventID.PROGRESS_FUNCTION,eventData1);
        });

        //Add the Listener on the Function
        reconstructionFilteredBack.addProgressListener(l -> {
            MMXIEventData mmxiEventData = new MMXIEventData(ID,l);
            fireControllerEvent(MMXIControllerEventType.UPDATE, MMXIControllerEvent.MMXIControllerEventID.PROGRESS_FUNCTION,mmxiEventData);
        });

        return reconstructionFilteredBack;
    }


//    /**
//     * Create a back projected Image of this type of imageStack
//     * @param typeOfStack identifier of the stack which will be used for computation
//     */
//    @Deprecated
//    public void backProjection(StackType typeOfStack){
//        RXImage imageStack = initReconstructionStack(typeOfStack);
//        IOController ioController = RXTomoJ.getInstance().getModelController().getIoController();
//
//        //Create a new Reconstruction Filteredback Object based on the currently saved in the Session Object
//        ReconstructionFilteredBack reconstructionFilteredBack = new ReconstructionFilteredBack((ReconstructionFilteredBack) this.getReconstruction(ReconstructionType.FBP,typeOfStack));
//        //TODO the way the center of rotation is retrieved
//        System.err.println("Warning Set Center is disabled " + this.getClass().toString());
////        reconstructionFilteredBack.setCenterOfRotationX(this.shiftImage.getOrdinateToOrigin());
//        reconstructionFilteredBack.initReconstructionDimension(imageStack);
//
//        switch (typeOfStack){
//            case ABSORPTION:
//                ioController.saveStack(new StackData(StackType.BP_ABSORPTION), reconstructionFilteredBack.retroProjections(imageStack));
//                break;
//            case  DARKFIELD:
//                ioController.saveStack(new StackData(StackType.BP_DARKFIELD), reconstructionFilteredBack.retroProjections(imageStack));
//                break;
//            case PHASECONTRAST:
//                ioController.saveStack(new StackData(StackType.BP_PHASECONTRAST), reconstructionFilteredBack.retroProjections(imageStack));
//                break;
//            default:
//                System.err.println("Error stackType unknown : "+typeOfStack);
//                return;
//        }
//    }

//    /**
//     * ART, algebraic tomographic reconstructionAlgebric
//     * @param typeOfStack identifier of the stack which will be used for computation
//     * @return A ProgressFunction class which can be used to monitor the computation process
//     */
//    public static ProgressFunction ART(ModelController mc, StackType typeOfStack){
//        return mc.getReconstructionController().ART(typeOfStack);
//    }

    /**
     * ART, algebraic tomographic reconstructionAlgebric
     * @param stackData identifier of the stack which will be used for computation
     * @return A ProgressFunction class which can be used to monitor the computation process
     */
    public ProgressFunction ART(StackData stackData){
        final UUID ID = UUID.randomUUID();

        //FIRE an initialize progress Signal to setup the UUID and retrieve the corresponding Progress event
        MMXIEventData eventData = new MMXIEventData(ID);
        fireControllerEvent(MMXIControllerEventType.INITIALIZE, MMXIControllerEvent.MMXIControllerEventID.PROGRESS_FUNCTION,eventData);

        //Create a new Reconstruction Algebric Object based on the currently saved in the Session Object
        ReconstructionAlgebraic reconstructionAlgebraic = new ReconstructionAlgebraic();
        IOController ioController = RXTomoJ.getInstance().getModelController().getIoController();
        ReconstructionInfo reconstructionInfo = RXTomoJ.getInstance().getModelController().getDataController().getReconstructionInfo();
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

        RXImage imageStack = initReconstructionStack(stackData);
        if  ( imageStack == null ){
            MMXIEventData mmxiEventData = new MMXIEventData(ID,INITIALIZATION_FAILS);
            fireControllerEvent(MMXIControllerEventType.ERROR, MMXIControllerEventID.PROGRESS_FUNCTION,mmxiEventData);
            return reconstructionAlgebraic;
        }

        reconstructionAlgebraic.initReconstructionDimension(imageStack);


        addToQueue(() -> {

            //TODO change here
            GPUDevice device = GPUDevice.getBestDevice();
            if (device == null) {
                MMXIEventData mmxiEventData = new MMXIEventData(ID,OPENCL_NODEVICE);
                fireControllerEvent(MMXIControllerEventType.ERROR, MMXIControllerEventID.PROGRESS_FUNCTION,mmxiEventData);
            }

            TomoReconstruction2 reconstruction = new TomoReconstruction2(imageStack.getWidth(), imageStack.getHeight(), imageStack.getWidth());

            //Display the image
            RXUtils.RXImageToImagePlus(imageStack).show();

            TiltSeries tiltSeries = new TiltSeries(RXUtils.RXImageToImagePlus(imageStack));

            //Test Angle
            RXImage rxAngle = dataController.getComputedStack(StackType.MOTOR_R_VOLATILE);
            if (rxAngle != null) {
                float[] rot = rxAngle.copyData();

                double[] angles = new double[imageStack.getSize()];
                for (int i = 0; i < imageStack.getSize(); i++) {
                    angles[i] = rot[i];
                }

                tiltSeries.setTiltAngles(angles);
            } else {
                double[] angles = new double[imageStack.getSize()];
                for (int i = 0; i < imageStack.getSize(); i++) {
                    //Warning here hard coded 360 => should be defined by users
                    angles[i] = i * (360.0d / imageStack.getSize());
                }

                tiltSeries.setTiltAngles(angles);
            }

            tiltSeries.setNormalize(false);

            tiltSeries.setProjectionCenter(reconstructionInfo.getCenterOfRotationX(), imageStack.getHeight() / 2.0d);

            reconstruction.initCompletion();


            if (!reconstructionInfo.isUseBgNormalization()) {
                VoxelProjector3DGPU projector = new VoxelProjector3DGPU(tiltSeries, reconstruction, device, null);
                projector.setPositivityConstraint(reconstructionInfo.isUsePositivityContrainst());
                projector.initForIterative(0, imageStack.getHeight(), 1, ProjectorGPU.ONE_KERNEL_PROJ_DIFF, false);

                reconstruction.OSSART(tiltSeries, projector, reconstructionInfo.getNumberOfIteration(),
                        reconstructionInfo.getCoefOfRelaxation(), 1);


                projector.updateFromGPU(0, imageStack.getHeight());

                projector.releaseCL_Memory();


                StackData toSave = null;

                switch (stackData.getStackType()) {
                    case ABSORPTION:
                        toSave = new StackData(StackType.ART_ABSORPTION);
//                        ioController.saveStack(new StackData(StackType.ART_ABSORPTION), RXUtils.ImagePlusToRXImage(reconstruction));
                        break;
                    case DARKFIELD:
                        toSave = new StackData(StackType.ART_DARKFIELD);
                        toSave.setAdditionalName(stackData.getAdditionalName());
//                        ioController.saveStack(new StackData(StackType.ART_DARKFIELD), RXUtils.ImagePlusToRXImage(reconstruction));
                        break;
                    case PHASECONTRAST:
                        toSave = new StackData(StackType.ART_PHASECONTRAST);
//                        ioController.saveStack(new StackData(StackType.ART_PHASECONTRAST), RXUtils.ImagePlusToRXImage(reconstruction));
                        break;
                    case FLUORESCENCE:
                        toSave = new StackData(StackType.ART_FLUORESCENCE);
                        toSave.setAdditionalName(stackData.getAdditionalName());
                        toSave.setSpc(stackData.getSpc());
//                        ioController.saveStack(new StackData(StackType.ART_FLUORESCENCE), RXUtils.ImagePlusToRXImage(reconstruction));
                        break;
                    default:
                        System.err.println("Error stackType unknown : " + stackData);
                        break;
                }

                ioController.saveStack(toSave, RXUtils.ImagePlusToRXImage(reconstruction));

            } else {
                BgARTVoxelProjector3DGPU projector = new BgARTVoxelProjector3DGPU(tiltSeries, reconstruction, device, null, reconstructionInfo.getK(),
                        false, false);
                projector.setPositivityConstraint(reconstructionInfo.isUsePositivityContrainst());
                projector.initForIterative(0, imageStack.getHeight(), 1, ProjectorGPU.ONE_KERNEL_PROJ_DIFF, false);
                reconstruction.OSSART(tiltSeries, projector, reconstructionInfo.getNumberOfIteration(),
                        reconstructionInfo.getCoefOfRelaxation(), 1);


                projector.updateFromGPU(0, imageStack.getHeight());
                projector.releaseCL_Memory();

                StackData toSave = null;

                switch (stackData.getStackType()) {
                    case ABSORPTION:
                        toSave = new StackData(StackType.BGART_ABSORPTION);
                        break;
                    case DARKFIELD:
                        toSave = new StackData(StackType.BGART_DARKFIELD);
                        toSave.setAdditionalName(stackData.getAdditionalName());
                        break;
                    case PHASECONTRAST:
                        toSave = new StackData(StackType.BGART_PHASECONTRAST);
                        break;
                    case FLUORESCENCE:
                        toSave = new StackData(StackType.BGART_FLUORESCENCE);
                        toSave.setAdditionalName(stackData.getAdditionalName());
                        toSave.setSpc(stackData.getSpc());
                        break;
                    default:
                        System.err.println("Error stackType unknown : " + stackData);
                        break;
                }

                ioController.saveStack(toSave, RXUtils.ImagePlusToRXImage(reconstruction));
            }

            MMXIEventData mmxiEventData = new MMXIEventData(ID);
            fireControllerEvent(MMXIControllerEventType.FINISHED, MMXIControllerEventID.PROGRESS_FUNCTION,mmxiEventData);
        });

        //Add the Listener on the Function
        reconstructionAlgebraic.addProgressListener(l -> {
            MMXIEventData mmxiEventData = new MMXIEventData(ID,l);
            fireControllerEvent(MMXIControllerEventType.UPDATE, MMXIControllerEventID.PROGRESS_FUNCTION,mmxiEventData);
        });


        return reconstructionAlgebraic;
    }

//
//    /**
//     * SIRT, simultaneous algebraic tomographic reconstructionAlgebric
//     * @param typeOfStack identifier of the stack which will be used for computation
//     */
//    public void SART(StackType typeOfStack){
//        RXImage imageStack = initReconstructionStack(typeOfStack);
//        IOController ioController = RXTomoJ.getInstance().getModelController().getIoController();
//
//        //Create a new Reconstruction Filteredback Object based on the currently saved in the Session Object
//        ReconstructionAlgebraic reconstructionAlgebraic = new ReconstructionAlgebraic((ReconstructionAlgebraic) this.getReconstruction(ReconstructionType.ART,typeOfStack));
//
//        //TODO the way the center of rotation is retrieved
//        System.err.println("Warning Set Center is disabled " + this.getClass().toString());
////        reconstructionAlgebric.setCenterOfRotationX(this.shiftImage.getOrdinateToOrigin());
//
//        reconstructionAlgebraic.initReconstructionDimension(imageStack);
//
//        switch (typeOfStack){
//            case ABSORPTION:
//                ioController.saveStack(StackType.SART_ABSORPTION, reconstructionAlgebraic.SIRT(imageStack));
//                break;
//            case  DARKFIELD:
//                ioController.saveStack(StackType.SART_DARKFIELD, reconstructionAlgebraic.SIRT(imageStack));
//                break;
//            case PHASECONTRAST:
//                ioController.saveStack(StackType.SART_PHASECONTRAST, reconstructionAlgebraic.SIRT(imageStack));
//                break;
//            default:
//                System.err.println("Error stackType unknown : "+typeOfStack);
//                return;
//        }
//    }

//    public static void setReconstructionCenterX(ModelController mc, double x){
//        mc.getReconstructionController().setReconstructionCenterX(x);
//    }
//
//    public void setReconstructionCenterX(double x){
//        for ( ReconstructionType type : ReconstructionType.values() ){
//            for ( StackType stackType : StackType.getBasicType() ) {
//                this.getReconstruction(type, stackType).setCenterOfRotationX((float) x);
//            }
//        }
//        MMXIControllerEventType error = MMXIControllerEventType.UPDATE;
//        fireModelUpdateEvent(new MMXIControllerEvent(error, this, MMXIControllerEvent.MMXIControllerEventID.RECONSTRUCTION_CENTER_CHANGED));
//    }

//    public static void setWindowDimension(ModelController mc, ReconstructionType reconstructionType, StackType type, float dimension){
//        mc.getReconstructionController().getReconstruction(reconstructionType,type).getProjectionFilter().setWindowDimension(dimension);
//    }

    public void setWindowDimension(ReconstructionType reconstructionType, StackType type, float dimension){
        this.getReconstruction(reconstructionType,type).getProjectionFilter().setWindowDimension(dimension);
    }

//    public static ProjectionFilter getProjectionFilter(ModelController mc, ReconstructionType reconstructionType, StackType type){
//        return mc.getReconstructionController().getProjectionFilter(reconstructionType,type);
//    }

    public ProjectionFilter getProjectionFilter(ReconstructionType reconstructionType, StackType type){
        return this.getReconstruction(reconstructionType, type).getProjectionFilter();
    }

    /**
     * Initialize the stack before calling the reconstruction function.
     * This initialization include Shifting from sinusoidal fit and background normalization
     * @param stackData The Stack Type to initialize
     * @return The image ready for reconstruction process
     */
    private RXImage initReconstructionStack(StackData stackData) {
        ComputationController computationController = RXTomoJ.getInstance().getModelController().getComputationController();
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
        ReconstructionInfo reconstructionInfo = dataController.getReconstructionInfo();

        NormalizationInfo normalizationInfo = dataController.getNormalizationInfo();
//        RXImage imageStack = computationController.setStackOrientation(stackType, OrientationType.XZY, false);


        //Test if Fitted Value is present in the ShiftImage Process ( if shiftImage is Used )
        if( SessionObject.getCurrentSessionObject().getAnalyseFactory().getCurrent().isUseSinusoidalShift() ){
            //TODO change this, check if the Shift function has been apply ( to find the center of rotation )
//            if (!dataController.getShiftImage().canShift()) {
//                DialogMessage.showMessage_ID("rxTomoJModelController_error_initReconstructionStack_shift", "", "JAVA_ERROR", DialogMessage.ERROR);
//                return null;
//            }
        }

        RXImage imageStack = null;
        if ( stackData.getStackType() != StackType.PHASECONTRAST ) {
            //Data should be first traited in XZY orientation ( projection in the height )
            imageStack = computationController.setStackOrientation(stackData, OrientationType.XZY, false);
            if (stackData.getStackType() == StackType.ABSORPTION ) {
                if ( normalizationInfo.isDoBackgroundNormalization() ) {
                    computationController._computeBackgroundNormalization(imageStack);
                    ComputationUtils.setAbsoptionCoef(imageStack);
                }
            }else if (stackData.getStackType() == StackType.DARKFIELD){
                if ( normalizationInfo.isDoBackgroundNormalization() ) {
                    computationController._computeBackgroundNormalization(imageStack);
                }
            }
            //Performs the wobble correction
            if ( normalizationInfo.isDoWobbleCorrection() ) {
                imageStack = computationController.wobbleCorrection(imageStack);
            }
        }
        else{
            //Add a special Case if Filtered / Hilbert and Phase reconstuction is choosen
            if ( reconstructionInfo.isUseFilter() && reconstructionInfo.getFilterType() == FilterType.HILBERT && stackData.getStackType() == StackType.PHASECONTRAST){
                imageStack = computationController.setStackOrientation(StackType.PHASE_GRAD_X, OrientationType.XZY, false);
                if ( normalizationInfo.isDoWobbleCorrection() ) {
                    imageStack = computationController.wobbleCorrection(imageStack);
                }
            }else {
                //Compute the Phase reconstruction
                //Data need to be XZY to be corrected but in XYZ for phase computation
                RXImage x = computationController.setStackOrientation(StackType.PHASE_GRAD_X, OrientationType.XZY, false);
                RXImage y = computationController.setStackOrientation(StackType.PHASE_GRAD_Y, OrientationType.XZY, false);
//                x = computationController.wobbleCorrection(x);
//                y = computationController.wobbleCorrection(y);

                imageStack = computationController.computePhaseRetrieval(x, y);
            }
        }



        System.out.println("WARNING :: COMMENTED LINE 416 :: "+this.getClass().toString());
//        RXImage result = dataController.getShiftImage().shiftImage(imageStack);
        RXImage result = imageStack;

        result = computationController.setStackOrientation(result, ComputationUtils.OrientationType.XZY, ComputationUtils.OrientationType.XYZ);

//        RXImage result2 = setStackOrientation(result, ComputationUtils.OrientationType.XZY,ComputationUtils.OrientationType.XYZ);

        //TODO make numerical simulation of padding impact
        //Test Pad data
//        int width = result2.getWidth()*2;
//        int height = result2.getHeight();
//        int size = result2.getSize();
//        float[] f_result = ComputationUtils.padDataWithCosineCenter(result2,width,height);
//        result = ComputationUtils.createStackFromArray(f_result,width,height,size);

        //Added because of the padding
//        this.setReconstructionCenterX(this.getShiftImage_().getCenterOfRotation()+(width/4.0f));
//        new ImagePlus("test",result.duplicate()).show();

        return result;
    }
}
