/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.controller;

import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerUpdated;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerUpdatedListener;
import javafx.application.Platform;

import javax.swing.*;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ExecutorService;

/**
 * Created by antoine bergamaschi on 17/04/2015.
 */
public abstract class MMXIController implements MMXIControllerUpdated{

    private static long updateEventLockerTime = 500;
    private static Date currentTimer = new Date();

    private List<MMXIControllerUpdatedListener> mmxiControllerUpdatedListeners = new ArrayList<>();
//    private List<MMXIControllerUpdatedListener> mmxiControllerSpecialListeners = new ArrayList<>();
    //Object[] should be [MMXIControllerEventID,MMXIControllerEventType]
    private Map<MMXIControllerUpdatedListener, Object[]> mmxiControllerSpecialListeners = new HashMap<>();
    final protected ExecutorService executorService;

    public MMXIController(final ExecutorService executorService){
        this.executorService = executorService;
    }

    @Override
    public void addMMXIControllerUpdatedListener(MMXIControllerUpdatedListener mmxiControllerUpdatedListener) {
        mmxiControllerUpdatedListeners.add(mmxiControllerUpdatedListener);
    }

    @Override
    public void removeMMXIControllerUpdatedListener() {
        mmxiControllerUpdatedListeners.clear();
    }

    @Override
    public void removeMMXIControllerUpdatedListener(MMXIControllerUpdatedListener mmxiControllerUpdatedListener) {
        mmxiControllerUpdatedListeners.remove(mmxiControllerUpdatedListener);
    }

    /**
     * The Special listener is used for one utilisation only. After one fire the object is destroyed by the Controller.
     * This Type of listener should be use for waiting a particular computation to finish before firing a new one.
     * @param eventID The Event ID to detect
     * @param eventType The Event Type to detect
     * @param mmxiControllerUpdatedListener The Listener to fire when this event ID and Type have been find
     */
    public void addMMXIControllerSpecialListener(MMXIControllerEvent.MMXIControllerEventID eventID,
                                                 MMXIControllerEvent.MMXIControllerEventType eventType,
                                                 MMXIControllerUpdatedListener mmxiControllerUpdatedListener ){
        mmxiControllerSpecialListeners.put(mmxiControllerUpdatedListener,new Object[]{eventID,eventType});
    }

    /**
     * Remove every special listeners linked with this EventType and EventID
     * @param eventID The Event ID to detect
     * @param eventType The Event Type to detect
     */
    public void removeMMXIControllerSpecialListener(MMXIControllerEvent.MMXIControllerEventID eventID,
                                                 MMXIControllerEvent.MMXIControllerEventType eventType){
        HashMap<MMXIControllerUpdatedListener,Object[]> specialListeners = new HashMap<>(mmxiControllerSpecialListeners);

        specialListeners.forEach((k, l) -> {
            if (eventID == l[0] &&
                    eventType == l[1]) {
                //Unref the listener
                mmxiControllerSpecialListeners.remove(k);
            }
        });
    }

    /**
     * Warning Here return will interfer with the Interface, so every call must pass throught Calls to SwingsUtilities
     * @param event
     */
    protected void fireModelUpdateEvent(MMXIControllerEvent event){
        final ArrayList<MMXIControllerUpdatedListener> updatedListeners = new ArrayList<>(mmxiControllerUpdatedListeners);
        final HashMap<MMXIControllerUpdatedListener,Object[]> specialListeners = new HashMap<>(mmxiControllerSpecialListeners);

        // Thread Service for JavaFX support
//        if ( Platform.isFxApplicationThread() ){
//            updatedListeners.forEach(k -> k.mmxiControllerUpdated(event));
//
//            specialListeners.forEach((k, l) -> {
//
//                if (event.getEventID() == l[0] &&
//                        event.getEventType() == l[1]) {
//                    k.mmxiControllerUpdated(event);
//                    //Unref the listener
//                    mmxiControllerSpecialListeners.remove(k);
//                }
//            });
//        }else{
//            Platform.runLater(() -> {
//                updatedListeners.forEach(k -> k.mmxiControllerUpdated(event));
//                specialListeners.forEach((k, l) -> {
//
//                    if (event.getEventID() == l[0] &&
//                            event.getEventType() == l[1]) {
//                        k.mmxiControllerUpdated(event);
//                        //Unref the listener
//                        mmxiControllerSpecialListeners.remove(k);
//                    }
//                });
//            });
//        }

        //Thread Service for Swing support
//        ArrayList<MMXIControllerUpdatedListener> copy = new ArrayList<>(mmxiControllerUpdatedListeners);

        if (SwingUtilities.isEventDispatchThread()) {

            updatedListeners.forEach(k -> {
                k.mmxiControllerUpdated(event);
            });


            //Special Listener Condition
            //Special Listener are only fired once then destroyed
            specialListeners.forEach((k, l) -> {
                if (event.getEventID() == l[0] &&
                        event.getEventType() == l[1]) {
                    k.mmxiControllerUpdated(event);
                    //Unref the listener
                    mmxiControllerSpecialListeners.remove(k);
                }
            });
        } else {
            if (event.getEventID() == MMXIControllerEvent.MMXIControllerEventID.PROGRESS_FUNCTION) {
                if (event.getEventType() == MMXIControllerEvent.MMXIControllerEventType.UPDATE) {
                    Date date = new Date();
                    if (updateEventLockerTime - (date.getTime() - currentTimer.getTime()) < 0) {
                        SwingUtilities.invokeLater(() -> updatedListeners.forEach(k -> k.mmxiControllerUpdated(event)));
//                    Save the Last occurences Of event
                        currentTimer.setTime(date.getTime());
                    }
                } else {
                    SwingUtilities.invokeLater(() -> updatedListeners.forEach(k -> k.mmxiControllerUpdated(event)));
                }
            } else {
                SwingUtilities.invokeLater(() ->{
                    updatedListeners.forEach(k -> k.mmxiControllerUpdated(event));
                    specialListeners.forEach((k, l) -> {

                        if (event.getEventID() == l[0] &&
                                event.getEventType() == l[1]) {
                            k.mmxiControllerUpdated(event);
                            //Unref the listener
                            mmxiControllerSpecialListeners.remove(k);
                        }
                    });
                });
            }
        }
    }

}
