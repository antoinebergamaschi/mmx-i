/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.controller;

import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStackFactory;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent.MMXIControllerEventID;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent.MMXIControllerEventType;
import com.soleil.nanoscopium.rxtomoj.event.MMXIEventData;
import com.soleil.nanoscopium.rxtomoj.model.AnalyseFactory;
import com.soleil.nanoscopium.rxtomoj.model.CustomAnalyse;
import com.soleil.nanoscopium.rxtomoj.model.SessionObject;
import com.soleil.nanoscopium.rxtomoj.model.SessionObject.DataType;
import com.soleil.nanoscopium.rxtomoj.model.data.StackData;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;

import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.ExecutorService;

import static com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;

/**
 * Created by antoine bergamaschi on 17/04/2015.
 */
public class SessionController extends AbstractController {
    public static int ERROR_INCORRECT_DATASETNAME = 0;
    public static int ERROR_UNKNWON_DATATYPE = 1;
    public static int ERROR_INCORRECT_HDF5 = 2;

    /**
     * This Constructor should always be called with a non-null modelController
     *
     * @param modelController The modelController linked to this AbstractController
     */
    public SessionController(ModelController modelController) {
        super(modelController);
    }

    public void openHdf5(String path){
        if (path != "") {
            if (isSessionOpen()) {
                if ( getPathToHdf5().equalsIgnoreCase(path)) {
                    //This file is already opened
                    //(Do nothing)
                } else {
                    closeSessionObject();
                    //Re-fire the Hdf5 opening
                    openHdf5(path);
                }
            }else{
                SessionObject.createSessionObject().setPathToHdf5(path);
                fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.HDF_OPENED,null);
            }
        }else{
            MMXIEventData mmxiEventData = new MMXIEventData(ERROR_INCORRECT_HDF5);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.HDF_OPENED,mmxiEventData);
        }
    }

    protected void initializeSessionObject(){
        SessionObject sessionObject = SessionObject.createSessionObject();
        //Create the new Session Object Analyse Factory
        sessionObject.setAnalyseFactory(new AnalyseFactory());
        sessionObject.getAnalyseFactory().createNewAnalyses();
    }

    @Deprecated
    public void manageDataset(DataType type, String value){
        ArrayList<String> a = new ArrayList<>();
        a.add(value);
        manageDataset(type, a);
    }

    @Deprecated
    public void manageDataset(DataType type, ArrayList<String> values) {
        if (values != null) {
            HashMap<DataType, ArrayList<String>> dataset = SessionObject.getCurrentSessionObject().getDataset();
            for (String value : values) {
                //If type is Null Every value is removed whatever there original Type
                if (type != null) {
                    if (!dataset.containsKey(type)) {
                        dataset.put(type, new ArrayList<>());
                    }

                    ArrayList<String> currentValue = dataset.get(type);
                    if (currentValue.contains(value)) {
                        currentValue.remove(value);
                        removeRefInData(type, value);
                    } else {
                        currentValue.add(value);
                    }
                    //Remove type
                    if (currentValue.size() == 0 || value == null) {
                        dataset.remove(type);
                    }
                }
                //Remove this value
                else {
                    for (DataType i : dataset.keySet()) {
                        if (dataset.get(i).contains(value)) {
                            dataset.get(i).remove(value);
                            removeRefInData(i, value);
                            break;
                        }
                    }
                }
            }
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.INIT_DATASET,null);
        }
    }

    /**
     * Add a datasetName to the list of a specify dataType
     * @param type The Datatype corresponding to the datasetName
     * @param datasetName The dataset name ( path in the Hdf5)
     */
    public void addRawDataset(DataType type, String datasetName){
        HashMap<DataType, ArrayList<String>> dataset = SessionObject.getCurrentSessionObject().getDataset();
        //If type is not null
        if (type != null) {
            if ( datasetName != null && datasetName.length() > 0) {
                //If the current HashMap do not contains the dataset already
                if (!dataset.containsKey(type)) {
                    dataset.put(type, new ArrayList<>());
                }
                ArrayList<String> currentValue = dataset.get(type);
                currentValue.add(datasetName);

                fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.INIT_DATASET,null);
            }else{
                //Error val is null or empty
                MMXIEventData mmxiEventData = new MMXIEventData(ERROR_INCORRECT_DATASETNAME);
                fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.INIT_DATASET,mmxiEventData);
            }
        }else{
            //Error type is null
            MMXIEventData mmxiEventData = new MMXIEventData(ERROR_UNKNWON_DATATYPE);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.INIT_DATASET,mmxiEventData);
        }
    }

    /**
     * remove a datasetName to the list of a specify dataType
     * @param type The Datatype corresponding to the datasetName
     * @param datasetName The dataset name ( path in the Hdf5)
     */
    public void removeRawDataset(DataType type, String datasetName){
        HashMap<DataType, ArrayList<String>> dataset = SessionObject.getCurrentSessionObject().getDataset();
        //If the current datasets contains this datatype
        if ( type != null && dataset.containsKey(type) ){
            //If the list of datasetNames contains this datasetName
            if ( datasetName != null && dataset.get(type).contains(datasetName) ){
                dataset.get(type).remove(datasetName);

                //Remove the dataset references in the stored model
                removeRefInData(type,datasetName);

                //If the datasetType is now empty remove it
                if ( dataset.get(type).size() == 0 ){
                    dataset.remove(type);
                }
            }
        }
    }

    /**
     * remove every datasetName to the list of a specify dataType
     */
    public void resetRawDataset(){
        HashMap<DataType, ArrayList<String>> dataset = SessionObject.getCurrentSessionObject().getDataset();
        //For every elements remove the references in the stored model
        for ( DataType type : dataset.keySet() ){
            for ( String datasetName : dataset.get(type) ){
                removeRefInData(type,datasetName);
            }
        }

        dataset.clear();
    }


    private void removeRefInData(DataType type, String val){
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
        switch (type){
            case IMAGE:
                //Warning Bug ? currentDataset can be null
                dataController.setCurrentDataset(null);
                break;
            case SPECTRUM:
                dataController.manageSpectrum(val);
                break;
            case MOTOR:
                dataController.manageMotor(null,val);
                break;
            case XBPM:
                dataController.manageXBPM(val);
                break;
        }
    }

    public ArrayList<String> getDataset(DataType type){
        if ( SessionObject.getCurrentSessionObject().getDataset().get(type)  !=  null  ){
            return new ArrayList<>(SessionObject.getCurrentSessionObject().getDataset().get(type));
        }else{
            return new ArrayList<>();
        }
    }

    protected CustomAnalyse getCustomAnalyse(){
        return SessionObject.getCurrentSessionObject().getAnalyseFactory().getCurrent();
    }


    protected CustomAnalyse createNewAnalyse(){
        CustomAnalyse customAnalyse = new CustomAnalyse(SessionObject.getCurrentSessionObject().getAnalyseFactory().createNewAnalyses());
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.ANALYSIS_UPDATED,null);
        return customAnalyse;
    }

    protected void removeAnalyse(int pos){
        SessionObject.getCurrentSessionObject().getAnalyseFactory().getAnalyses().remove(pos);
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.ANALYSIS_UPDATED,null);
    }

    public boolean isSessionOpen(){
        return SessionObject.isOpen();
    }

    public String getPathToHdf5(){
        return SessionObject.getCurrentSessionObject().getPathToHdf5();
    }

    /**
     * Remove this StackData from every analysis
     * @param toRemove The StackData to remove
     */
    public void removeStackData(StackData toRemove){
        for(CustomAnalyse analyse : getSessionObject().getAnalyseFactory().getAnalyses()){
            analyse.getDataStorage().getStackDataStorage().remove(toRemove);
        }
    }




    /**
     * Remove every modalities to automatically compute
     */
    public void removeModalities(){
        SessionObject.getCurrentSessionObject().setAutomatedComputation(new ArrayList<>());
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.AUTOMATED_MODALITIES_UPDATED,null);
    }

    /**
     * Manage the Modalities to compute automatically. Add or Remove this current modality
     * @param modality
     */
    public void manageModalities(Collection<ComputationUtils.StackType> modality){
        for ( ComputationUtils.StackType type : modality ) {
            manageModality(type);
        }
    }

    /**
     * Remove every modalities to automatically compute
     * @param modality
     */
    public void manageModality(ComputationUtils.StackType modality){
        //If contains
        if ( SessionObject.getCurrentSessionObject().getAutomatedComputation().contains(modality) ){
            removeModality(modality);
        }else{
            addModality(modality);
        }
    }

    /**
     * Set The modalities to compute automatically
     * @param modality The Modalities to add in the automated modality Collection
     */
    public void setModalities(Collection<StackData> modality){
        SessionObject.getCurrentSessionObject().setAutomatedComputation(new ArrayList<>());
        for ( StackData stackData : modality){
            addModality(stackData.getStackType());
        }

    }

    /**
     * Add The modalities as the Basic function to compute
     * @param automatedComputation The List of modalities to add view as StackType
     */
    public void addModalities(Collection<StackType> automatedComputation){
        for ( StackType type : automatedComputation ) {
            if ( !SessionObject.getCurrentSessionObject().getAutomatedComputation().contains(type) ){
                SessionObject.getCurrentSessionObject().getAutomatedComputation().add(type);
            }
        }
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.AUTOMATED_MODALITIES_UPDATED,null);
    }

    /**
     * Add this modality in the list of Basic function to compute
     * @param modality The StackType to add
     */
    private void addModality(ComputationUtils.StackType modality){
        Collection<StackType> s = new ArrayList<>();
        s.add(modality);
        this.addModalities(s);
    }

    /**
     * Remove This modality from the list of automatically computed function
     * @param modality The modality to remove ( a StackType )
     */
    private void removeModality(StackType modality){
        if ( SessionObject.getCurrentSessionObject().getAutomatedComputation().contains(modality) ){
            SessionObject.getCurrentSessionObject().getAutomatedComputation().remove(modality);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.AUTOMATED_MODALITIES_UPDATED,null);
        }
    }

    /**
     * Get a Safe copy of the current list of automatically computed function
     * @return The List of automatically computed function
     */
    public ArrayList<StackData> getModalities(){
        //Transform to the new StackData object
        ArrayList<StackData> stackDatas = new ArrayList<>();
        for ( StackType stackType : SessionObject.getCurrentSessionObject().getAutomatedComputation()){
            stackDatas.add(new StackData(stackType));
        }

        return stackDatas;
    }

    /**
     * Get a Safe copy of the current list of automatically computed function
     * @return The List of automatically computed function
     */
    public ArrayList<StackType> getStackTypeModalities(){
        return SessionObject.getCurrentSessionObject().getAutomatedComputation();
    }

    public boolean canSaveSessionObject(){
        return SessionObject.getCurrentSessionObject().canWrite();
    }

    public void saveSessionObject(){
        SessionObject.save();
        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.SESSION_SAVED,null);
    }

    public boolean setSessionObjectPath(String path){
        String oldPath =  SessionObject.getCurrentSessionObject().getPathToSessionObject();
        SessionObject.getCurrentSessionObject().setPathToSessionObject(path);
        if ( SessionObject.getCurrentSessionObject().canWrite() ){
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.SESSION_PATH_CHANGED,null);
            return true;
        }else{
            //Reverse Changes
            SessionObject.getCurrentSessionObject().setPathToSessionObject(oldPath);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.SESSION_PATH_CHANGED,null);
            return false;
        }
    }

    public void closeSessionObject(){
        //Clear the Session from the currently loaded Hdf5
        Hdf5VirtualStackFactory.clearAll();

        //Close the old Session object
        SessionObject.closeCurrentSessionObject();

        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.SESSION_CLOSED,null);
    }

    public void openSessionObject(String path){
        //Close Current Session Object
        this.closeSessionObject();

        //Open the new
        SessionObject.open(path);

        fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.SESSION_OPENED,null);
    }


    @Deprecated
    public int getSessionObjectDataType(String datasetName){
//        return SessionObject.getCurrentSessionObject().checkInListData(datasetName);
        DataType dataType = getDataType(datasetName);

        //If dataset exist in Session
        if ( dataType != null ) {
            switch (dataType) {
                case IMAGE:
                    return 0;
                case SPECTRUM:
                    return 1;
                case MOTOR:
                    return 3;
                case XBPM:
                    return 2;
            }
        }
        return -1;
    }

    /**
     * Return the DatatType of the DataSet Name if the datasetName exist in the Session else return null
     * @param datasetName The Dataset name to test
     * @return null if datasetName do not exist in the Session else the DataType
     */
    public DataType getDataType(String datasetName){
        for ( DataType type : DataType.values() ){
            if ( getDataset(type).contains(datasetName) ){
                return type;
            }
        }
        return null;
    }

    public SessionObject getSessionObject(){
        return SessionObject.getCurrentSessionObject();
    }
}
