/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.controller;

import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent.MMXIControllerEventID;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent.MMXIControllerEventType;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerUpdatedListener;

import java.util.concurrent.Executors;

/**
 * Created by antoine bergamaschi on 17/04/2015.
 */
public class ModelController extends MMXIController {
    private final AbstractController ioController = new IOController(this);
    private final AbstractController dataController = new DataController(this);
    private final AbstractController computationController = new ComputationController(this);
    private final AbstractController reconstructionController = new ReconstructionController(this);
    private final AbstractController sessionController = new SessionController(this);

    /**
     * Initialize every Controllers
     */
    public ModelController(){
        super(Executors.newSingleThreadExecutor(r -> new Thread(r,"MMXI_worker")));

        ((SessionController)sessionController).initializeSessionObject();

        //Fire Update Model finished initialization
        fireModelUpdateEvent(new MMXIControllerEvent(MMXIControllerEventType.UPDATE, this, MMXIControllerEventID.INIT_CONTROLLER_FINISHED, null));
    }

    public IOController getIoController() {
        return (IOController) ioController;
    }

    public DataController getDataController() {
        return (DataController) dataController;
    }

    public ComputationController getComputationController() {
        return (ComputationController) computationController;
    }

    public ReconstructionController getReconstructionController() {
        return (ReconstructionController) reconstructionController;
    }

    public SessionController getSessionController() {
        return (SessionController) sessionController;
    }
}
