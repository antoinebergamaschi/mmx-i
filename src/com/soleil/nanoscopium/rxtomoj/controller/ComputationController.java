/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.controller;

import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStack;
import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStackFactory;
import com.soleil.nanoscopium.hdf5Opener.stream.Hdf5VirtualStream;
import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.RXSpectrum;
import com.soleil.nanoscopium.rximage.util.RXUtils;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent.MMXIControllerEventID;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent.MMXIControllerEventType;
import com.soleil.nanoscopium.rxtomoj.event.MMXIEventData;
import com.soleil.nanoscopium.rxtomoj.model.SessionObject;
import com.soleil.nanoscopium.rxtomoj.model.ShiftImage;
import com.soleil.nanoscopium.rxtomoj.model.computationFunction.*;
import com.soleil.nanoscopium.rxtomoj.model.data.*;
import com.soleil.nanoscopium.rxtomoj.model.filters.BackgroundSubtraction;
import com.soleil.nanoscopium.rxtomoj.model.fluorescence.FluorescenceInfo;
import com.soleil.nanoscopium.rxtomoj.model.fluorescence.FluorescenceObject;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.OrientationType;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;
import com.soleil.nanoscopium.rxtomoj.model.utils.FittingUtils;
import com.soleil.nanoscopium.rxtomoj.model.utils.ProgressFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.ExecutorService;

import static com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent.*;
import static com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType.*;


/**
 * Created by antoine bergamaschi on 17/04/2015.
 */
public class ComputationController extends AbstractController {

    private boolean lock_computeModalities = false;

    //No Spectrum set in the Model
    final public static int WARNING_NOSPCPATH=0;
    //No Fluorescence Elements set in the Model
    final public static int WARNING_NOELEMENTS=1;
    //No Transmission Modality selected
    final  public static int WARNING_NOTRANSMISSION_SEL=2;
    final  public static int WARNING_NOIMAGE=3;

    final public static int COMPUTE_SUMSPECTRA = 0;
    final public static int SPOT_INITIALIZATION = 1;
    final public static int REGRID = 2;
    final public static int PHASE_RETRIEVAL = 3;

    final public static int ERROR_NULL_STACK = 0;
    final public static int ERROR_NULL_COMPUTESPOT = 1;
    final public static int ERROR_COMPUTATION_PARALLEL_EXEC = 2;
    final public static int ERROR_2D_REQUIRED = 3;

    /**
     * This Constructor should always be called with a non-null modelController
     *
     * @param modelController The modelController linked to this AbstractController
     */
    public ComputationController(ModelController modelController) {
        super(modelController);
    }


    /**
     * Update Spectrum compute the Sum spectrum of every SelectedSpectrum which are not already computed
     */
    public void updateSumSpectra(){
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

        ArrayList<String> selectedSpectrum = dataController.getSelectedSpectrum();
        ArrayList<StackData> computedSpectrums = dataController.getDataStorage().getStackDataStorage().get(SUM_SPECTRA);

        //If not null remove the already computed spectrums
//        if ( computedSpectrums != null ) {
//            for (StackData computedSpectrum : computedSpectrums) {
//                if (selectedSpectrum.contains(computedSpectrum.getSpc())) {
//                    selectedSpectrum.remove(computedSpectrum.getSpc());
//                }
//            }
//        }

        //Test if this file can be saved
        computeSumSpectra(null, selectedSpectrum);
    }

    /**
     * Compute the Sum spectra on the specifiyed ROI ( on the image )
     * @param roi The roi on which the Sum spectra will be performed ( roi on the image MAP )
     * @param spc The Spectrum path where Sum spectra will be computed
     */
    public void computeSumSpectra(int[][] roi, final ArrayList<String> spc){

//        for ( String spectrum : spc ){
//            computeSumSpectra(roi,spectrum);
//        }

        addToQueue(() -> {
            final UUID ID = UUID.randomUUID();
            MMXIEventData mmxiEventData1 = new MMXIEventData(ID,COMPUTE_SUMSPECTRA);
            fireControllerEvent(MMXIControllerEventType.INITIALIZE,MMXIControllerEventID.PROGRESS_FUNCTION,mmxiEventData1);
            spc.parallelStream().forEach(spectrumPath -> {

                //FIRE an initialize progress Signal to setup the UUID and retrieve the corresponding Progress event
                DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
                SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
                IOController ioController = RXTomoJ.getInstance().getModelController().getIoController();

                //Get the selected Elements
                final ComputeSpectrum computeSpectrum = new ComputeSpectrum();
                ReductionInfo reductionInfo = dataController.getReductionInfo();
                reductionInfo.setPixelSpacing(1);

//               If a Roi is given use it to reconstruct only a small part
                if ( roi != null ){
                    reductionInfo.setReductionROI(computeRegriddedROI(roi));
                }
                //Compute on the whole image if the ROI is not specified
                else{
                    reductionInfo.setReductionROI(reductionInfo.getMaxImageDimension());
                }

                //ComputeSpot have to be set by default
                computeSpectrum.setIsSumSpectra(true);

                computeSpectrum.setReductionInfo(reductionInfo);

                //Add the Listener on the Function
                computeSpectrum.addProgressListener(l -> {
                    //Fire Warning Event
                    MMXIEventData mmxiEventData = new MMXIEventData(ID,l);
                    fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.PROGRESS_FUNCTION,mmxiEventData);
                });

                HashMap<String, RXSpectrum> result = new HashMap<>();
                //If Spectrum have been selected
                if (RXTomoJ.getInstance().getModelController().getDataController().fluorescenceDataContains(spectrumPath)) {
                    Hdf5VirtualStack hdf5VirtualStack = Hdf5VirtualStackFactory.createVirtualStack(sessionController.getPathToHdf5(), spectrumPath, true);

                    Hdf5VirtualStream hdf5VirtualStream = new Hdf5VirtualStream(hdf5VirtualStack, computeSpectrum);
                    //Re-initialize the Hdf5VirtualStack
                    hdf5VirtualStack.updateID();
                    //Close and destroy if needed the Hdf5VirtualStack
                    hdf5VirtualStream.close();

                    StackData stackData = new StackData(SUM_SPECTRA);
                    stackData.setSpc(spectrumPath);

                    //Retreive the old StackData
                    ArrayList<StackData> stackDatas = dataController.getDataStorage().getStackDataStorage().get(SUM_SPECTRA);
                    if ( DataStorage.containsSPC(stackDatas,spectrumPath) ){
                        dataController.releaseStack(DataStorage.getFromSpectrum(stackDatas,spectrumPath));
                    }

                    ioController.saveStack(stackData, computeSpectrum.getSpectrum());

                    //Update Model
                    RXTomoJ.getInstance().getModelController().getDataController().setFluorescenceData(spectrumPath, computeSpectrum.getSpectrum());

                    result.put(spectrumPath, computeSpectrum.getSpectrum());
                } else {
                    //Fire Warning Event
                    MMXIEventData mmxiEventData = new MMXIEventData(ID,WARNING_NOSPCPATH);
                    fireControllerEvent(MMXIControllerEventType.WARNING,MMXIControllerEventID.PROGRESS_FUNCTION,mmxiEventData);
                }

//                MMXIEventData mmxiEventData = new MMXIEventData(ID,COMPUTE_SUMSPECTRA,result);
//                fireControllerEvent(MMXIControllerEventType.FINISHED,MMXIControllerEventID.PROGRESS_FUNCTION,mmxiEventData);
                computeSpectrum.hardStop();
            });
            MMXIEventData mmxiEventData = new MMXIEventData(ID,COMPUTE_SUMSPECTRA,null);
            fireControllerEvent(MMXIControllerEventType.FINISHED,MMXIControllerEventID.PROGRESS_FUNCTION,mmxiEventData);
        });
    }

    /**
     * Compute the Sum spectra on the specifiyed ROI ( on the image )
     * @param roi The roi on which the Sum spectra will be performed ( roi on the image MAP )
     * @param spc The Spectrum path where Sum spectra will be computed
     */
    public void computeSumSpectra(int[][] roi, final String spc){
        final UUID ID = UUID.randomUUID();

        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
        SessionController sessionController  =  RXTomoJ.getInstance().getModelController().getSessionController();
        IOController ioController = RXTomoJ.getInstance().getModelController().getIoController();

        //Get the selected Elements
        final ComputeSpectrum computeSpectrum = new ComputeSpectrum();
        ReductionInfo reductionInfo = dataController.getReductionInfo();
        reductionInfo.setPixelSpacing(1);
        //If a Roi is given use it to reconstruct only a small part
//        if ( roi != null ){
//            reductionInfo.setReductionROI(computeRegriddedROI(roi));
//        }
//
        //ComputeSpot have to be set by default
        computeSpectrum.setIsSumSpectra(true);

        computeSpectrum.setReductionInfo(reductionInfo);

        //Add the Listener on the Function
        computeSpectrum.addProgressListener(l->{
            //Fire Warning Event
            MMXIEventData mmxiEventData = new MMXIEventData(ID,l);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.PROGRESS_FUNCTION,mmxiEventData);
        });

        addToQueue(() -> {
            HashMap<String, RXSpectrum> result = new HashMap<>();
            //If Spectrum have been selected
            if (RXTomoJ.getInstance().getModelController().getDataController().fluorescenceDataContains(spc)) {
                Hdf5VirtualStack hdf5VirtualStack = Hdf5VirtualStackFactory.createVirtualStack(sessionController.getPathToHdf5(), spc, true);

                Hdf5VirtualStream hdf5VirtualStream = new Hdf5VirtualStream(hdf5VirtualStack, computeSpectrum);
                //Re-initialize the Hdf5VirtualStack
                hdf5VirtualStack.updateID();
                //Close and destroy if needed the Hdf5VirtualStack
                hdf5VirtualStream.close();

                StackData stackData = new StackData(SUM_SPECTRA);
                stackData.setSpc(spc);

                //Retreive the old StackData
                ArrayList<StackData> stackDatas = dataController.getDataStorage().getStackDataStorage().get(SUM_SPECTRA);
                if (DataStorage.containsSPC(stackDatas, spc)) {
                    dataController.releaseStack(DataStorage.getFromSpectrum(stackDatas, spc));
                }

                ioController.saveStack(stackData, computeSpectrum.getSpectrum());

                //Update Model
                RXTomoJ.getInstance().getModelController().getDataController().setFluorescenceData(spc, computeSpectrum.getSpectrum());

                result.put(spc, computeSpectrum.getSpectrum());
            } else {
                //Fire Warning Event
                MMXIEventData mmxiEventData = new MMXIEventData(ID,WARNING_NOSPCPATH);
                fireControllerEvent(MMXIControllerEventType.WARNING,MMXIControllerEventID.PROGRESS_FUNCTION,mmxiEventData);
            }

            MMXIEventData mmxiEventData = new MMXIEventData(ID,COMPUTE_SUMSPECTRA,result);
            computeSpectrum.hardStop();
            fireControllerEvent(MMXIControllerEventType.FINISHED,MMXIControllerEventID.PROGRESS_FUNCTION,mmxiEventData);
        });
    }


    /**
     * Initialize The Spot Map and Basic ReductionInfo
     *
     * This function should always be called before computation of other modalities.
     * In fact the spotMap image created in this function will be used in every other computation modalities.
     */
    public void spotMapInitialization(){
        final UUID ID = UUID.randomUUID();

        //FIRE an initialize progress Signal to setup the UUID and retrieve the corresponding Progress event
        MMXIEventData mmxiEventData1 = new MMXIEventData(ID,SPOT_INITIALIZATION);
        fireControllerEvent(MMXIControllerEventType.INITIALIZE,MMXIControllerEventID.PROGRESS_FUNCTION,mmxiEventData1);

        final ComputeSpot computeSpot = new ComputeSpot();

        final SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
        final IOController ioController = RXTomoJ.getInstance().getModelController().getIoController();
        final DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

        //Add the Listener on the Function
        computeSpot.addProgressListener(l->{
            MMXIEventData mmxiEventData = new MMXIEventData(ID,SPOT_INITIALIZATION,l);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.PROGRESS_FUNCTION,mmxiEventData);
        });

        //Launch the Computation Thread
        addToQueue(() -> {
            //Test if a DatasetName exist
            if (  sessionController.getCustomAnalyse().getDatasetName() != null && sessionController.getCustomAnalyse().getDatasetName().length() > 0) {

                //First Iteration looking for the Roi spot
                Hdf5VirtualStack hdf5VirtualStack = Hdf5VirtualStackFactory.createVirtualStack(sessionController.getPathToHdf5(), sessionController.getCustomAnalyse().getDatasetName(), false);

                Hdf5VirtualStream hdf5VirtualStream = new Hdf5VirtualStream(hdf5VirtualStack, computeSpot);
                //Re-initialize the Hdf5VirtualStack
                hdf5VirtualStack.updateID();
                //Close and destroy if needed the Hdf5VirtualStack
                hdf5VirtualStream.close();

                //Update the Maximum Spot Map dimension in case of 2D detectors only
                dataController.setMaxSpotImageDimension(computeSpot.getMaxDimension());

                //Update the ROI spot position in the MAP
                dataController.setRoiSpot(computeSpot.getComputedRoiSpot());
                dataController.setReductionRoi(computeSpot.getReconstructionFullDimension());
                dataController.setMaxScanImageDimension(computeSpot.getReconstructionFullDimension());
                dataController.set2DMap(computeSpot.is2Ddetector());

                //Release the old Image saved in the model
                dataController.removeFromAnalyse(SPOT_MAP);

                //Save the SpotMap
                ioController.saveSpotMap(computeSpot.getStack());

                MMXIEventData mmxiEventData = new MMXIEventData(ID,SPOT_INITIALIZATION);
                fireControllerEvent(MMXIControllerEventType.FINISHED,MMXIControllerEventID.PROGRESS_FUNCTION,mmxiEventData);

                sessionController.saveSessionObject();
            }else{
                //Fire Warning Event
                MMXIEventData mmxiEventData = new MMXIEventData(ID,WARNING_NOIMAGE);
                fireControllerEvent(MMXIControllerEventType.WARNING,MMXIControllerEventID.PROGRESS_FUNCTION,mmxiEventData);
            }
        });
    }

    public ProgressFunction computeModalities(){
        final UUID ID = UUID.randomUUID();
        //Create the data object
        MMXIEventData eventData = new MMXIEventData(ID);
        //FIRE an initialize progress Signal to setup the UUID and retrieve the corresponding Progress event
        fireControllerEvent(MMXIControllerEventType.INITIALIZE,MMXIControllerEventID.PROGRESS_FUNCTION,eventData);

        //Init the computation object and controller to save latter
        ComputeModalities computeModalities = new ComputeModalities();
        SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
        IOController ioController = RXTomoJ.getInstance().getModelController().getIoController();
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

//        if ( lock_computeModalities ){
//            computeModalities.hardStop();
//            MMXIControllerEventType err = MMXIControllerEventType.ERROR;
//            err.setID(ID);
//            err.setIdentifier(ERROR_COMPUTATION_PARALLEL_EXEC);
//            fireModelUpdateEvent(new MMXIControllerEvent(err, this, MMXIControllerEventID.PROGRESS_FUNCTION));
//
//            //Then Fire Finish to close the process
//            MMXIControllerEventType f = MMXIControllerEventType.FINISHED;
//            f.setID(ID);
//            fireModelUpdateEvent(new MMXIControllerEvent(f,this,MMXIControllerEventID.PROGRESS_FUNCTION));
//        }
        if ( sessionController.getStackTypeModalities().size() > 0  ) {

            if ( sessionController.getStackTypeModalities().contains(FLUORESCENCE)){
                //Start the computation of Fluorescence with a new UUID
                RXTomoJ.getInstance().getModelController().getComputationController().imageCreation(StackType.FLUORESCENCE);
            }

//            lock_computeModalities = true;

            if ( sessionController.getStackTypeModalities().size() > 1 && sessionController.getStackTypeModalities().contains(FLUORESCENCE) ||
                    sessionController.getStackTypeModalities().size() > 0 && !sessionController.getStackTypeModalities().contains(FLUORESCENCE)) {
                //Add the Listener on the Function
                computeModalities.addProgressListener(l -> {
                    //Create the data object
                    MMXIEventData eventDataUpdate = new MMXIEventData(ID,l);
                    fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.PROGRESS_FUNCTION,eventDataUpdate);
                });


                addToQueue(() -> {
                    //Init The Modalities
                    ArrayList<BasicComputationFunction> functions = computeModalities.getFunctions();
                    for (StackData type : sessionController.getModalities()) {
                        switch (type.getStackType()) {
                            case ABSORPTION:
                                ComputeAbsorption computeAbsorption = new ComputeAbsorption();
//                            computeAbsorption.setComputeSpot(type);
                                functions.add(computeAbsorption);
                                System.out.println("Compute Absobtion");
                                break;
                            case DARKFIELD:
                                ComputeDarkField computeDarkField = new ComputeDarkField();
//                            computeDarkField.setComputeSpot(type);
                                functions.add(computeDarkField);
                                System.out.println("Compute DarkField");
                                break;
                            case PHASECONTRAST:
                                ComputePhaseContrast computePhaseContrast = new ComputePhaseContrast();
//                            computePhaseContrast.setComputeSpot(type);
                                functions.add(computePhaseContrast);
                                System.out.println("Compute Phase");
                                break;
                        }
                    }


                    Hdf5VirtualStack hdf5VirtualStack = Hdf5VirtualStackFactory.createVirtualStack(sessionController.getPathToHdf5(), sessionController.getCustomAnalyse().getDatasetName(), false);

                    Hdf5VirtualStream hdf5VirtualStream = new Hdf5VirtualStream(hdf5VirtualStack, computeModalities);
                    //Re-initialize the Hdf5VirtualStack
                    hdf5VirtualStack.updateID();
                    //Close and destroy if needed the Hdf5VirtualStack
                    hdf5VirtualStream.close();

                    //Save every Stack
                    for (BasicComputationFunction basicComputationFunction : functions) {
                        if (basicComputationFunction instanceof ComputeAbsorption) {
                            //Release the old Image saved in the model
                            dataController.removeFromAnalyse(ABSORPTION);

                            ioController.saveStack(ABSORPTION, basicComputationFunction);
                        } else if (basicComputationFunction instanceof ComputeDarkField) {
                            //Release the old Image saved in the model
                            dataController.removeFromAnalyse(DARKFIELD);

                            for (String name : ((ComputeDarkField) basicComputationFunction).getDarkFieldImage().keySet()) {
                                StackData data = new StackData(DARKFIELD);
                                data.setOtherName(name);
                                data.setAdditionalName(name);
                                ioController.saveStack(data, ((ComputeDarkField) basicComputationFunction).getDarkFieldImage().get(name));
                            }

                            //TODO here save the Orientation MAP
                            if ( dataController.getDarkFieldInfo().isDoOrientationComputation() ) {
                                RXImage map = ((ComputeDarkField) basicComputationFunction).getOrientationMap();
                                RXUtils.RXImageToImagePlus(map).show();
                            }

                        } else if (basicComputationFunction instanceof ComputePhaseContrast) {
                            //Release the old Image saved in the model
                            dataController.removeFromAnalyse(PHASE_GRAD_Y);
                            dataController.removeFromAnalyse(PHASE_GRAD_X);

                            //            Set the stack in the Model
                            ioController.saveStack(new StackData(PHASE_GRAD_X), ((ComputePhaseContrast) basicComputationFunction).getImageStackGradientX());
                            ioController.saveStack(new StackData(PHASE_GRAD_Y), ((ComputePhaseContrast)basicComputationFunction).getImageStackGradientY());

                            computePhaseRetrieval();
                        }
                    }


                    sessionController.createNewAnalyse();

                    //Create the data object
                    MMXIEventData eventDataUpdate = new MMXIEventData(ID);
                    fireControllerEvent(MMXIControllerEventType.FINISHED,MMXIControllerEventID.PROGRESS_FUNCTION,eventDataUpdate);

                });
            }
            //Close this computation as ther is no other data then Fluorescence
            else{
                //Create the data object
                MMXIEventData eventDataUpdate = new MMXIEventData(ID);
                //Then Fire Finish to close the process
                fireControllerEvent(MMXIControllerEventType.FINISHED,MMXIControllerEventID.PROGRESS_FUNCTION,eventDataUpdate);
            }
        }
        else {
            //Create the data object
            MMXIEventData eventDataUpdate = new MMXIEventData(ID,WARNING_NOTRANSMISSION_SEL);
            fireControllerEvent(MMXIControllerEventType.WARNING,MMXIControllerEventID.PROGRESS_FUNCTION,eventDataUpdate);

            //Close the computation
            computeModalities.hardStop();

            //Create the data object
            MMXIEventData eventDataUpdate2 = new MMXIEventData(ID);
            //Then Fire Finish to close the process
            fireControllerEvent(MMXIControllerEventType.FINISHED,MMXIControllerEventID.PROGRESS_FUNCTION,eventDataUpdate2);
        }

        return computeModalities;
    }



    /**
     * Compute and store reconstructed image in the model
     * @param typeOfStack identifier of the stack which will be used for computation
     * @return The progress function use to follow the current execution
     */
    public ProgressFunction imageCreation(StackType typeOfStack){
        return imageCreation(typeOfStack, 1);
    }


    /**
     * Compute and store reconstructed image in the model
     * @param typeOfStack identifier of the stack which will be used for computation
     * @param additionalName Name to add in the saving file
     * @return The progress function use to follow the current execution
     */
    public ProgressFunction imageCreation(StackType typeOfStack, String additionalName){
        typeOfStack.setAdditionalName(additionalName);
        return imageCreation(typeOfStack,1);
    }

    /**
     * Compute and store reconstructed image in the model
     * @param typeOfStack identifier of the stack which will be used for computation
     * @param additionalName Name to add in the saving file
     * @param sampling Sampling to apply when reading the image in the Hdf5 file
     * @return The progress function use to follow the current execution
     */
    public ProgressFunction imageCreation(StackType typeOfStack,int sampling, String additionalName){
        typeOfStack.setAdditionalName(additionalName);
        return imageCreation(typeOfStack,sampling);
    }

    /**
     * Compute and store reconstructed image in the model
     * @param typeOfStack identifier of the stack which will be used for computation
     * @param sampling Sampling to apply when reading the image in the Hdf5 file
     * @return The progress function use to follow the current execution
     */
    public ProgressFunction imageCreation(StackType typeOfStack, int sampling){

        final UUID ID = UUID.randomUUID();
        //Create the data object
        MMXIEventData eventData = new MMXIEventData(ID);
        //FIRE an initialize progress Signal to setup the UUID and retrieve the corresponding Progress event
        fireControllerEvent(MMXIControllerEventType.INITIALIZE,MMXIControllerEventID.PROGRESS_FUNCTION,eventData);



        BasicComputationFunction progressFunction = null;

        switch (typeOfStack){
            case ABSORPTION_REF:
                progressFunction = this.computeAbsorption_REF(sampling, ID);
                break;
            case ABSORPTION:
                progressFunction = this.computeAbsorption(sampling, ID);
                break;
            case DARKFIELD:
                progressFunction = this.computeDarkField(sampling, ID);
                break;
            case PHASECONTRAST:
                progressFunction = this.computePhaseContrast(sampling, ID);
                break;
            case FLUORESCENCE:
                //Error Fluorescence do not support sampling yet
                progressFunction = this.computeFluorescence(sampling, ID);
                break;
            case SUM_SPECTRA:
                //Error Fluorescence do not support sampling yet
                progressFunction = this.computeSumSpectra(sampling,ID);
                break;
        }

        //Add the Listener on the Function
        progressFunction.addProgressListener(l -> {
            //Create the data object
            MMXIEventData eventDataUpdate = new MMXIEventData(ID,l);
            fireControllerEvent(MMXIControllerEventType.UPDATE,MMXIControllerEventID.PROGRESS_FUNCTION,eventDataUpdate);
        });

        return progressFunction;
    }

    private BasicComputationFunction computeAbsorption_REF(){
        return this.computeAbsorption_REF(1);
    }

    private BasicComputationFunction computeAbsorption_REF(int sampling){
        return computeAbsorption_REF(sampling, UUID.randomUUID());
    }

    /**
     * Compute the Raw Absorption Image, this image is name AbsorptionRef and feature only the raw data without normalization
     * of any kinds.
     * @param ID The Unique ID of this function Thread
     * @return
     */
    private BasicComputationFunction computeAbsorption_REF(int sampling, final UUID ID){
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
        IOController ioController =  RXTomoJ.getInstance().getModelController().getIoController();
        SessionController sessionController  = RXTomoJ.getInstance().getModelController().getSessionController();
        ReductionInfo reductionInfo = dataController.getReductionInfo();

        //Use the Low resolution mode
        reductionInfo.setUseLowResolution(true);

        //Reset the ReductionROI
        reductionInfo.setReductionROI(reductionInfo.getMaxImageDimension());

        //Remove the use of Filter to speed up
        reductionInfo.setDoFilter(false);
        reductionInfo.setDoFilter_diffusion(false);

        //Reset Stack from memory
        dataController.releaseStack(ABSORPTION_REF);

        ComputeAbsorption computeAbsorption = new ComputeAbsorption();

        //Do not perform the finalization step
        computeAbsorption.setFinalization(false);

        //Set the ReductionInfo to not use the Model one
        computeAbsorption.setReductionInfo(reductionInfo);


        addToQueue(() -> {
            Hdf5VirtualStack hdf5VirtualStack = Hdf5VirtualStackFactory.createVirtualStack(sessionController.getPathToHdf5(), sessionController.getCustomAnalyse().getDatasetName(), false);

            Hdf5VirtualStream hdf5VirtualStream = new Hdf5VirtualStream(hdf5VirtualStack, computeAbsorption);
            //Re-initialize the Hdf5VirtualStack
            hdf5VirtualStack.updateID();

            //Close and destroy if needed the Hdf5VirtualStack
            hdf5VirtualStream.close();

            //Warning here a reading operation is done again !
            RXImage[] motors = BasicComputationFunction.initMotors(sessionController.getSessionObject(), StackType.ABSORPTION);
            RXImage motors1 = ShiftImage.initRegrid(motors);

            //Create the StackData object
            StackData stackData = new StackData(MOTORS_POSITION);

            //Release in the whole analysis
            dataController.releaseStack(MOTORS_POSITION);

            //Save the computed Motors Position
            ioController.saveStack(stackData, motors1);

            //Add a null test in RXImage
            if (motors[2].copyData() != null) {
                StackData stackData2 = new StackData(MOTORS_ROTATION);

                dataController.releaseStack(MOTORS_ROTATION);

                ioController.saveStack(stackData2, motors[2]);
                //Save the MOTOR R in volatil storage
                dataController.storeVolatileStack(MOTOR_R_VOLATILE, motors[2]);
            }

            //Store the Motors array in the volatile storage
            dataController.storeVolatileStack(MOTOR_X_VOLATILE, motors[0]);
            dataController.storeVolatileStack(MOTOR_Y_VOLATILE, motors[1]);

            //Manage the Volatil stack
            dataController.storeVolatileStack(ABSORPTION_REF, computeAbsorption.getStack());


            dataController.releaseStack(ABSORPTION_REF);

            //Set the stack in the Model
            ioController.saveStack(ABSORPTION_REF, computeAbsorption);

            //Save the old analysis and stay on the current
            sessionController.saveSessionObject();

            //Create the data object
            MMXIEventData data = new MMXIEventData(ID,ABSORPTION_REF);

            fireControllerEvent(MMXIControllerEventType.FINISHED,MMXIControllerEventID.PROGRESS_FUNCTION,data);
        });

        return computeAbsorption;
    }


    private BasicComputationFunction computeAbsorption(){
        return this.computeAbsorption(1);
    }

    private BasicComputationFunction computeAbsorption(int sampling){
        return computeAbsorption(sampling, UUID.randomUUID());
    }

    private BasicComputationFunction computeAbsorption(int sampling, UUID ID){
        SessionController sessionController  = RXTomoJ.getInstance().getModelController().getSessionController();
        IOController ioController =  RXTomoJ.getInstance().getModelController().getIoController();
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

        ComputeAbsorption computeAbsorption = new ComputeAbsorption();
//        computeAbsorption.setSampling(sampling);
//        ComputeSpot computeSpot = dataController.getComputeSpot(ABSORPTION);

        //Update the position to fit with the motors if any
//        computeSpot.setRoiReconstruction(computeRegriddedROI(computeSpot.getRoiReconstruction()));
//        computeAbsorption.setComputeSpot(computeSpot);

        addToQueue(() -> {

            Hdf5VirtualStack hdf5VirtualStack = Hdf5VirtualStackFactory.createVirtualStack(sessionController.getPathToHdf5(), sessionController.getCustomAnalyse().getDatasetName(), false);

            Hdf5VirtualStream hdf5VirtualStream = new Hdf5VirtualStream(hdf5VirtualStack, computeAbsorption);
            //Re-initialize the Hdf5VirtualStack
            hdf5VirtualStack.updateID();
            //Close and destroy if needed the Hdf5VirtualStack
            hdf5VirtualStream.close();

            //Release the old Image saved in the model
            dataController.removeFromAnalyse(ABSORPTION);

            //Set the stack in the Model
            ioController.saveStack(ABSORPTION, computeAbsorption);

            sessionController.createNewAnalyse();

            //Create the data object
            MMXIEventData data = new MMXIEventData(ID,ABSORPTION);

            fireControllerEvent(MMXIControllerEventType.FINISHED,MMXIControllerEventID.PROGRESS_FUNCTION,data);
        });

        return computeAbsorption;
    }

    private BasicComputationFunction computePhaseContrast(){
        return this.computePhaseContrast(1);
    }

    private BasicComputationFunction computePhaseContrast(int sampling){
        return computePhaseContrast(sampling, UUID.randomUUID());
    }

    private BasicComputationFunction computePhaseContrast(int sampling,UUID ID){
        SessionController sessionController  = RXTomoJ.getInstance().getModelController().getSessionController();
        IOController ioController =  RXTomoJ.getInstance().getModelController().getIoController();
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

        ComputePhaseContrast computePhaseContrast = new ComputePhaseContrast();
//        computePhaseContrast.setSampling(sampling);
//        computePhaseContrast.setComputeSpot(dataController.getComputeSpot(PHASECONTRAST));
//        ComputeSpot computeSpot = dataController.getComputeSpot(PHASECONTRAST);

        //Update the position to fit with the motors if any
//        computeSpot.setRoiReconstruction(computeRegriddedROI(computeSpot.getRoiReconstruction()));
//        computePhaseContrast.setComputeSpot(computeSpot);

        addToQueue(() -> {
            Hdf5VirtualStack hdf5VirtualStack = Hdf5VirtualStackFactory.createVirtualStack(sessionController.getPathToHdf5(), sessionController.getCustomAnalyse().getDatasetName(), false);

            Hdf5VirtualStream hdf5VirtualStream = new Hdf5VirtualStream(hdf5VirtualStack, computePhaseContrast);
            //Re-initialize the Hdf5VirtualStack
            hdf5VirtualStack.updateID();
            //Close and destroy if needed the Hdf5VirtualStack
            hdf5VirtualStream.close();


            //Release the old Image saved in the model
            dataController.removeFromAnalyse(PHASE_GRAD_Y);
            dataController.removeFromAnalyse(PHASE_GRAD_X);

//            Set the stack in the Model
            ioController.saveStack(new StackData(PHASE_GRAD_X), computePhaseContrast.getImageStackGradientX());
            ioController.saveStack(new StackData(PHASE_GRAD_Y), computePhaseContrast.getImageStackGradientX());

//            ioController.saveStack(PHASECONTRAST, computePhaseContrast);
            computePhaseRetrieval();
//            hdf5Handler.hdf5_close();
            sessionController.createNewAnalyse();


            //Create the data object
            MMXIEventData eventDataUpdate = new MMXIEventData(ID,PHASECONTRAST);
            fireControllerEvent(MMXIControllerEventType.FINISHED,MMXIControllerEventID.PROGRESS_FUNCTION,eventDataUpdate);
        });

        return computePhaseContrast;
    }

    private BasicComputationFunction computeDarkField(){
        return this.computeDarkField(1);
    }

    private BasicComputationFunction computeDarkField(int sampling){
        return computeDarkField(sampling, UUID.randomUUID());
    }

    private BasicComputationFunction computeDarkField(int sampling, UUID ID){
        SessionController sessionController  = RXTomoJ.getInstance().getModelController().getSessionController();
        IOController ioController =  RXTomoJ.getInstance().getModelController().getIoController();
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

        ComputeDarkField computeDarkField = new ComputeDarkField();
//        computeDarkField.setSampling(sampling);
//        ComputeSpot computeSpot = dataController.getComputeSpot(DARKFIELD);

        //Update the position to fit with the motors if any
//        computeSpot.setRoiReconstruction(computeRegriddedROI(computeSpot.getRoiReconstruction()));
//        computeDarkField.setComputeSpot(computeSpot);

        addToQueue(() -> {
            Hdf5VirtualStack hdf5VirtualStack = Hdf5VirtualStackFactory.createVirtualStack(sessionController.getPathToHdf5(), sessionController.getCustomAnalyse().getDatasetName(), false);

            Hdf5VirtualStream hdf5VirtualStream = new Hdf5VirtualStream(hdf5VirtualStack, computeDarkField);
            //Re-initialize the Hdf5VirtualStack
            hdf5VirtualStack.updateID();
            //Close and destroy if needed the Hdf5VirtualStack
            hdf5VirtualStream.close();

            //Release the old Image saved in the model
            dataController.removeFromAnalyse(DARKFIELD);

            //Set the stack in the Model
            for (String name : computeDarkField.getDarkFieldImage().keySet()) {
                StackData data = new StackData(DARKFIELD);
                data.setOtherName(name);
                data.setAdditionalName(name);

                ioController.saveStack(data, computeDarkField.getDarkFieldImage().get(name));
            }

            //TODO here save the Orientation MAP
            if ( dataController.getDarkFieldInfo().isDoOrientationComputation() ) {
                RXImage map = computeDarkField.getOrientationMap();
                RXUtils.RXImageToImagePlus(map).show();
            }

            sessionController.createNewAnalyse();


            //Create the data object
            MMXIEventData data = new MMXIEventData(ID,DARKFIELD);
            fireControllerEvent(MMXIControllerEventType.FINISHED,MMXIControllerEventID.PROGRESS_FUNCTION,data);
        });

        return computeDarkField;
    }


    private BasicComputationFunction computeFluorescence(){
        return this.computeFluorescence(1);
    }

    private BasicComputationFunction computeFluorescence(int sampling){
        return computeFluorescence(sampling, UUID.randomUUID());
    }

    private BasicComputationFunction computeFluorescence(int sampling,UUID ID){
        IOController ioController =  RXTomoJ.getInstance().getModelController().getIoController();
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
        SessionController sessionController  =  RXTomoJ.getInstance().getModelController().getSessionController();

        //Get the selected Elements
        ComputeSpectrum computeSpectrum = new ComputeSpectrum();

//        computeSpectrum.setSampling(sampling);
        //ComputeSpot have to be set by default
//        ComputeSpot computeSpot = dataController.getComputeSpot(ABSORPTION);

        //Update the position to fit with the motors if any
//        computeSpot.setRoiReconstruction(computeRegriddedROI(computeSpot.getRoiReconstruction()));
//        computeSpectrum.setComputeSpot(computeSpot);

        addToQueue(() -> {
            FluorescenceInfo fluorescenceInfo = sessionController.getCustomAnalyse().getParameterStorage().getFluorescenceInfo();
            //If Spectrum have been selected
            if (fluorescenceInfo.getFluoData().size() > 0) {

                //Release the old Image saved in the model
                dataController.removeFromAnalyse(FLUORESCENCE);

                //For every Selected spectrum
                for (FluorescenceObject spc : fluorescenceInfo.getFluoData()) {
                    //If Elements Have been set
                    if (fluorescenceInfo.getElements().size() > 0) {

                        Hdf5VirtualStack hdf5VirtualStack = Hdf5VirtualStackFactory.createVirtualStack(sessionController.getPathToHdf5(), spc.getPath(), true);

                        Hdf5VirtualStream hdf5VirtualStream = new Hdf5VirtualStream(hdf5VirtualStack, computeSpectrum);
                        //Re-initialize the Hdf5VirtualStack
                        hdf5VirtualStack.updateID();
                        //Close and destroy if needed the Hdf5VirtualStack
                        hdf5VirtualStream.close();

                        int index = 0;
                        //Warning here Elements Name are lost
                        for (RXImage imageStack : computeSpectrum.getElementImages()) {
                            StackData stackData = new StackData(FLUORESCENCE);
                            stackData.setSpc(spc.getPath());
                            stackData.setOtherName(computeSpectrum.getElements().get(index).getName());
                            stackData.setAdditionalName(computeSpectrum.getElements().get(index).getName());

                            ioController.saveStack(stackData, imageStack);
                            index++;
                        }
                    } else {
                        //Create the data object
                        MMXIEventData data = new MMXIEventData(ID,WARNING_NOELEMENTS);
                        fireControllerEvent(MMXIControllerEventType.WARNING,MMXIControllerEventID.PROGRESS_FUNCTION,data);
                    }
                }
                sessionController.createNewAnalyse();

            } else {
                //Create the data object
                MMXIEventData data = new MMXIEventData(ID,WARNING_NOSPCPATH);
                fireControllerEvent(MMXIControllerEventType.WARNING,MMXIControllerEventID.PROGRESS_FUNCTION,data);
            }

            //Create the data object
            MMXIEventData data = new MMXIEventData(ID,FLUORESCENCE);
            fireControllerEvent(MMXIControllerEventType.FINISHED,MMXIControllerEventID.PROGRESS_FUNCTION,data);
            computeSpectrum.hardStop();
        });

        return computeSpectrum;
    }

    private BasicComputationFunction computeSumSpectra(int sampling,UUID ID){
        IOController ioController =  RXTomoJ.getInstance().getModelController().getIoController();
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
        SessionController sessionController  =  RXTomoJ.getInstance().getModelController().getSessionController();

        //Get the selected Elements
        ComputeSpectrum computeSpectrum = new ComputeSpectrum();

        //ComputeSpot have to be set by default
        computeSpectrum.setIsSumSpectra(true);
//        computeSpectrum.setFinalization(false);

//        computeSpectrum.setSampling(sampling);
        //ComputeSpot have to be set by default
//        ComputeSpot computeSpot = dataController.getComputeSpot(ABSORPTION);

        //Update the position to fit with the motors if any
//        computeSpot.setRoiReconstruction(computeRegriddedROI(computeSpot.getRoiReconstruction()));
//        computeSpectrum.setComputeSpot(computeSpot);

        addToQueue(() -> {
            HashMap<String,RXSpectrum> result = new HashMap<>();

            FluorescenceInfo fluorescenceInfo = sessionController.getCustomAnalyse().getParameterStorage().getFluorescenceInfo();
            for (FluorescenceObject spc : fluorescenceInfo.getFluoData()) {
                //If Spectrum have been selected
                Hdf5VirtualStack hdf5VirtualStack = Hdf5VirtualStackFactory.createVirtualStack(sessionController.getPathToHdf5(), spc.getPath(), false);

                Hdf5VirtualStream hdf5VirtualStream = new Hdf5VirtualStream(hdf5VirtualStack, computeSpectrum);
                //Re-initialize the Hdf5VirtualStack
                hdf5VirtualStack.updateID();
                //Close and destroy if needed the Hdf5VirtualStack
                hdf5VirtualStream.close();

                //Release the old Image saved in the model
                dataController.removeFromAnalyse(SUM_SPECTRA);

                StackData stackData = new StackData(SUM_SPECTRA);
                stackData.setSpc(spc.getPath());
//                SUM_SPECTRA.setSpc(spc.getPath());
                ioController.saveStack(stackData, computeSpectrum.getSpectrum());


                result.put(spc.getPath(),computeSpectrum.getSpectrum());
            }


            //Create the data object
            MMXIEventData eventData = new MMXIEventData(ID,COMPUTE_SUMSPECTRA,result);
            fireControllerEvent(MMXIControllerEventType.FINISHED,MMXIControllerEventID.PROGRESS_FUNCTION,eventData);

            computeSpectrum.hardStop();
        });

        return computeSpectrum;
    }


    /**
     * Regrid function used to integrate motors from models, the RXImage used is by default ABSORPTION_REF
     * @param type The stackType to regrid, the stacktype is used to retrives the specified reconstruction size from computeSpot
     * @return RXImage The regridded version of ABSORPTION_REF
     */
    public RXImage regrid(StackType type){
//        SessionObject sessionObject = RXTomoJ.getInstance().getModelController().getSessionController().getSessionObject();
        RXImage shifted = RXTomoJ.getInstance().getModelController().getDataController().getComputedStack(StackType.ABSORPTION_REF);

        //If The image to Shift do not exist
        if ( shifted == null ){
            //Create the data object
            MMXIEventData eventData = new MMXIEventData(ERROR_NULL_STACK);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.REGRID_FUNCTION,eventData);
            //Return Error
            return null;
        }

        //Run the Regrid
        return regrid(shifted, type);
    }

    /**
     * Regrid function used to integrate motors from models, the RXImage used is the one given in parameter
     * @param toRegrid The image to regrid
     * @param type The stackType to regrid, the stacktype is used to retrives the specified reconstruction size from computeSpot
     * @return RXImage The regridded version of the given RXImage
     */
    public RXImage regrid(RXImage toRegrid, StackType type){
        SessionObject sessionObject = RXTomoJ.getInstance().getModelController().getSessionController().getSessionObject();
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

        //Make sure the regrided stack is in the right orientation XYZ
        toRegrid = setStackOrientation(toRegrid,OrientationType.XYZ);

        //Fire computation started
        fireControllerEvent(MMXIControllerEventType.INITIALIZE,MMXIControllerEventID.REGRID_FUNCTION,null);


        //TODO change here stop computation if there is no motors selected
        //If no motors selected
        if ( dataController.getSelectedMotors().size() == 0 ){
            return toRegrid;
        }

        RXImage[] motors = new RXImage[2];
        //Retrieves Motors in volatil storage
        if ( dataController.getComputedStack(MOTOR_X_VOLATILE) != null && dataController.getComputedStack(MOTOR_Y_VOLATILE) != null ) {
            motors[0] = dataController.getComputedStack(MOTOR_X_VOLATILE);
            motors[1] = dataController.getComputedStack(MOTOR_Y_VOLATILE);
            if ( motors[0].getGlobalSize() != toRegrid.getGlobalSize() || toRegrid.getGlobalSize() != motors[1].getGlobalSize()){
                //Else compute it
                motors = BasicComputationFunction.initMotors(sessionObject, type);
                //Manage the volatile storage
                dataController.storeVolatileStack(MOTOR_X_VOLATILE, motors[0]);
                dataController.storeVolatileStack(MOTOR_Y_VOLATILE, motors[1]);
            }
        }else{
            //Else compute it
            motors = BasicComputationFunction.initMotors(sessionObject, type);
            //Manage the volatile storage
            dataController.storeVolatileStack(MOTOR_X_VOLATILE, motors[0]);
            dataController.storeVolatileStack(MOTOR_Y_VOLATILE, motors[1]);
        }

        //Run the Regrid
        return regrid(toRegrid,motors[0],motors[1], type);
    }


    /**
     * Regrid function used to integrate motors given in parameters, the RXImage used is the one given in parameter
     * @param toRegrid The image to regrid
     * @param motorX The motor X data
     * @param motorY The motor Y data
     * @return RXImage The regridded version of the given RXImage
     */
    public RXImage regrid(RXImage toRegrid, RXImage motorX, RXImage motorY, StackType type){
        //Init ShiftImage base on ShiftImageInfo
        NormalizationInfo normalizationInfo = RXTomoJ.getInstance().getModelController().getDataController().getNormalizationInfo();
        RXImage rxImage = null;

        //Run the Regrid
        if ( type == FLUORESCENCE ) {
            rxImage = ShiftImage.regriding(toRegrid, motorX.copyData(), motorY.copyData(), normalizationInfo.getInterpolationDistance_fluorescence());
        }else{
            rxImage = ShiftImage.regriding(toRegrid, motorX.copyData(), motorY.copyData(), normalizationInfo.getInterpolationDistance());
        }

        MMXIEventData mmxiEventData = new MMXIEventData(REGRID);
        //Fire computation finished
        fireControllerEvent(MMXIControllerEventType.FINISHED,MMXIControllerEventID.REGRID_FUNCTION,mmxiEventData);

        return rxImage;
    }

    /**
     * Compute the phase based on the X and Y phase gradient
     * This Function may use the wobble / background correction function for normalization purpose
     *
     * The Phase computed is stored in {@link ComputationUtils.StackType} PHASECONTRAST
     */
    public void computePhaseRetrieval(){
        RXImage result = null;
        IOController ioController = RXTomoJ.getInstance().getModelController().getIoController();
        //Get the phaseContrast info object and data storage
        PhaseContrastInfo phaseContrastInfo = RXTomoJ.getInstance().getModelController().getDataController().getPhaseContrastInfo();
        DataStorage dataStorage = RXTomoJ.getInstance().getModelController().getDataController().getDataStorage();

        RXImage x = RXTomoJ.getInstance().getModelController().getDataController().getComputedStack(dataStorage.getStackDataStorage().get(PHASE_GRAD_X).get(0));
        RXImage y = RXTomoJ.getInstance().getModelController().getDataController().getComputedStack(dataStorage.getStackDataStorage().get(PHASE_GRAD_Y).get(0));
        //If the Gradient exist
        if ( x != null && y != null ){
            //Cannot compute phase using Southwell with data height should be > 4
            if ( x.getHeight() == 1 && !phaseContrastInfo.getPhaseRetrievalAlgo().isFourrier() ) {
                MMXIEventData mmxiEventData = new MMXIEventData(ERROR_2D_REQUIRED);
                //Fire computation finished
                fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.COMPUTE_PHASE_RETRIEVAL,mmxiEventData);
                return;
            }

            //Compute the phase
            result = computePhaseRetrieval(x,y);

        }else{
            MMXIEventData mmxiEventData = new MMXIEventData(ERROR_NULL_STACK);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.COMPUTE_PHASE_RETRIEVAL,mmxiEventData);
        }

        //Release the old Image saved in the model
        RXTomoJ.getInstance().getModelController().getDataController().removeFromAnalyse(PHASECONTRAST);

        ioController.saveStack(new StackData(PHASECONTRAST), result);
    }

    /**
     * Compute the phase with x as the phase gradient in the x direction and y the phase gradient in the y direction
     * Warning data given in parameters shoud be in XZY orientation for correction purpose. But for phase reconstruction
     * they should be in XYZ orientation.
     * @param x The phase gradient in the X direction in XZY orientation ( projection is Z )
     * @param y The phase gradient in the Y direction in XZY orientation ( projection is Z )
     * @return The computed phase image in XZY orientation
     */
    public RXImage computePhaseRetrieval(RXImage x, RXImage y){
        RXImage result = null;
        //If the Gradient exist
        if ( x != null && y != null ) {

            PhaseContrastInfo phaseContrastInfo = RXTomoJ.getInstance().getModelController().getDataController().getPhaseContrastInfo();
            ComputePhaseContrast.PhaseRetrievalAlgo p = phaseContrastInfo.getPhaseRetrievalAlgo();
            NormalizationInfo normalizationInfo = RXTomoJ.getInstance().getModelController().getDataController().getNormalizationInfo();

            if ( RXTomoJ.getInstance().getModelController().getDataController().getDefaultRXImage() != null ) {
                if (normalizationInfo.isDoWobbleCorrection()) {
                    RXImage k = wobbleCorrection(x);
                    RXImage l = wobbleCorrection(y);
                    //Test if computation result is ok
                    if (k != null && l != null) {
                        x = k;
                        y = l;
                    }
                }

                if (normalizationInfo.isDoBackgroundNormalization()) {
                    computeBackgroundSubtraction(x);
                    computeBackgroundSubtraction(y);
                }
            }

            //Debug only
//            RXTomoJ.getInstance().getModelController().getIoController().saveStack(new StackData(FLUORESCENCE),x);
//            RXTomoJ.getInstance().getModelController().getIoController().saveStack(new StackData(FLUORESCENCE),y);

            //Set orientation for computation
            x = setStackOrientation(x,OrientationType.XZY,OrientationType.XYZ);
            y = setStackOrientation(y,OrientationType.XZY,OrientationType.XYZ);

            //Set Mirroring
            if (phaseContrastInfo.isUseMirroring() && phaseContrastInfo.getPhaseRetrievalAlgo().isFourrier()) {
                RXImage mirrorX = ShiftImage.mirroring(x, phaseContrastInfo.getReal());
                RXImage mirrorY = ShiftImage.mirroring(y, phaseContrastInfo.getImaginary());

                result = ComputationUtils.cropInStack(p.phaseRetrieval(mirrorX, mirrorY,
                                phaseContrastInfo.getMinimalStepValue(),phaseContrastInfo.getExpectedErrorPerPixel(),
                                phaseContrastInfo.getMaximumNumberOfIteration()), x.getWidth(), x.getHeight(), 0,
                        x.getWidth(), x.getHeight(), x.getSize());
            } else {
                result = p.phaseRetrieval(x, y,phaseContrastInfo.getMinimalStepValue(),
                        phaseContrastInfo.getExpectedErrorPerPixel(),
                        phaseContrastInfo.getMaximumNumberOfIteration());

                result = setStackOrientation(result,OrientationType.XYZ,OrientationType.XZY);
            }
        }else{
            MMXIEventData mmxiEventData = new MMXIEventData(ERROR_NULL_STACK);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.COMPUTE_PHASE_RETRIEVAL,mmxiEventData);
        }

        return result;
    }

    /**
     * Warning Here only an estimate position is given
     * @param roi The Roi to reconstruct based on the uncorrected image
     * @return The Roi to reconstruct in the corrected image
     */
    public int[][] computeRegriddedROI(int[][] roi){
        int[] newRoi = new int[6];
        newRoi[0] = roi[0][0];
        newRoi[1] = roi[0][1];
        newRoi[2] = roi[1][0];
        newRoi[3] = roi[1][1];
        newRoi[4] = roi[2][0];
        newRoi[5] = roi[2][1];

        return computeRegriddedROI(newRoi);
    }

    /**
     * Warning Here only an estimate position is given
     * @param roi The Roi to reconstruct based on the uncorrected image
     * @return The Roi to reconstruct in the corrected image
     */
    public int[][] computeRegriddedROI(int[] roi){
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
        RXImage motors = dataController.getComputedStack(MOTORS_POSITION);
        int[][] result = new int[3][2];

        if ( motors == null ) {
            MMXIEventData mmxiEventData = new MMXIEventData(ERROR_NULL_STACK);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.REGRID_POSITION,mmxiEventData);
        }
        else{
            result = ShiftImage.getCorrectedReconstructionROI(roi,motors);
        }

        MMXIEventData mmxiEventData = new MMXIEventData(ERROR_NULL_STACK);
        fireControllerEvent(MMXIControllerEventType.FINISHED,MMXIControllerEventID.REGRID_POSITION,null);

        return result;
    }

    /**
     * Compute the background normalization of this image.
     * Store the resulting limits position ( which are the position where the object should be ) and the types ( left or right )
     * of the detection
     * @param imageStack The Image that will be normalized
     */
    protected void initBackgroundNormalization(RXImage imageStack){
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
        NormalizationInfo normalizationInfo = dataController.getNormalizationInfo();

        //Init the Background Object
        BackgroundSubtraction backgroundSubtraction = new BackgroundSubtraction(normalizationInfo);
        backgroundSubtraction.computeNormalization(imageStack);

        //Save as volatile Stack the backgrounds limits and position Type
        RXImage limits = new RXImage(1,imageStack.getHeight(),imageStack.getSize());
        RXImage types = new RXImage(1,imageStack.getHeight(),imageStack.getSize());
        limits.setData(backgroundSubtraction.getPosObject());
        types.setData(backgroundSubtraction.getPosType());

        dataController.storeVolatileStack(COMPUTED_BACKGROUND_LIMITS, limits);
        dataController.storeVolatileStack(COMPUTED_BACKGROUND_TYPES, types);
    }

    /**
     * Compute the Background normalization of the given image. Normalization consist of dividing the image with the
     * background value ( for each raw ).
     * @param imageStack The image to normalize
     */
    public void computeBackgroundNormalization(RXImage imageStack){
        addToQueue(() -> _computeBackgroundNormalization(imageStack));
    }

    /**
     * Private Function for internal use only
     *
     * Compute the Background normalization of the given image. Normalization consist of dividing the image with the
     * background value ( for each raw ).
     * @param imageStack The image to normalize
     */
    protected void _computeBackgroundNormalization(RXImage imageStack){
        //Test nullity of the first image stack
        if ( imageStack != null ) {
            DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

            RXImage limits = dataController.getComputedStack(COMPUTED_BACKGROUND_LIMITS);
            RXImage types = dataController.getComputedStack(COMPUTED_BACKGROUND_TYPES);

            if (limits == null || types == null) {
                //If data are not set compute them
                initBackgroundNormalization(imageStack);
            } else {
                NormalizationInfo normalizationInfo = dataController.getNormalizationInfo();

                //Init the Background Object
                BackgroundSubtraction backgroundSubtraction = new BackgroundSubtraction(normalizationInfo);

                backgroundSubtraction.setPosObject(limits.copyData());
                backgroundSubtraction.setPosType(types.copyData());

                backgroundSubtraction.computeNormalization(imageStack);
            }
            fireControllerEvent(MMXIControllerEventType.FINISHED,MMXIControllerEventID.COMPUTE_NORMALIZATION,null);
        }
        //The image stack given in input is NULL
        else{
            MMXIEventData mmxiEventData = new MMXIEventData(ERROR_NULL_STACK);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.COMPUTE_NORMALIZATION,mmxiEventData);
        }
    }

    /**
     * Compute the Background normalization of the given image. Normalization consist of subtracting the image with the
     * background value ( for each raw ).
     * @param imageStack The image to normalize
     */
    public void computeBackgroundSubtraction(RXImage imageStack){
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

        RXImage limits = dataController.getComputedStack(COMPUTED_BACKGROUND_LIMITS);
        RXImage types = dataController.getComputedStack(COMPUTED_BACKGROUND_TYPES);

        //Init the Limits if not already
        if ( limits == null || types == null ){
            initBackgroundNormalization(new RXImage(imageStack));
            limits = dataController.getComputedStack(COMPUTED_BACKGROUND_LIMITS);
            types = dataController.getComputedStack(COMPUTED_BACKGROUND_TYPES);
        }

        NormalizationInfo normalizationInfo = dataController.getNormalizationInfo();

        //Init the Background Object
        BackgroundSubtraction backgroundSubtraction = new BackgroundSubtraction(normalizationInfo);

        backgroundSubtraction.setPosObject(limits.copyData());
        backgroundSubtraction.setPosType(types.copyData());

        backgroundSubtraction.computeSubtraction(imageStack);
    }


    /**
     * Shift the corresponding stackToShift, with the shift value computed on the stackref.
     * This procedure can be called recursively by setting recursiveNumber superior to  0
     * @param stackToShift The shifting stack
     * @param stackRef The reference Stack
     * @return The unshifted image
     */
    public void shiftImageWithUpdate(StackType stackToShift,StackType stackRef){
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
        RXImage imageStack,imageStackRef;
        imageStack = dataController.getComputedStack(stackToShift);
        imageStackRef = dataController.getComputedStack(stackRef);
        this.shiftImageWithUpdate(imageStack, imageStackRef);
    }

    /**
     * Shift the corresponding stackToShift, with the shift value computed on the stackref.
     * This procedure can be called recursively by setting recursiveNumber superior to  0.
     * This Procedure update the shiftImage stored in the model
     * @param stackToShift The shifting stack
     * @param stackRef The reference Stack
     * @return The shifted imageStack
     */
    public void shiftImageWithUpdate(final RXImage stackToShift,final RXImage stackRef){
        if ( stackToShift == null || stackRef == null ){
            MMXIEventData mmxiEventData = new MMXIEventData(ERROR_NULL_STACK);
            fireControllerEvent(MMXIControllerEventType.ERROR,MMXIControllerEventID.COMPUTE_SINUS_SHIFT,mmxiEventData);
            return;
        }

        addToQueue(() -> {
            _shiftImageWithUpdate(stackToShift,stackRef);
        });
    }

    /**
     * Private Function for internal use only
     *
     * Shift the corresponding stackToShift, with the shift value computed on the stackref.
     * This procedure can be called recursively by setting recursiveNumber superior to  0.
     * This Procedure update the shiftImage stored in the model
     * @param stackToShift The shifting stack
     * @param stackRef The reference Stack
     * @return The shifted imageStack
     */
    protected RXImage _shiftImageWithUpdate(final RXImage stackToShift,final RXImage stackRef){
        RXImage unShiftedImage = null;
        if ( stackToShift != null && stackRef != null ) {
            DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
            NormalizationInfo normalizationInfo = dataController.getNormalizationInfo();

            //Initialize the FittingUtils Object
            FittingUtils fittingUtils = new FittingUtils();
            fittingUtils.setMobileWindow(normalizationInfo.getMobileWindow());
            fittingUtils.setUseMobileMean(normalizationInfo.isUseMobileMean());

            //Initialize the ShiftImage object
            ShiftImage shiftImage = new ShiftImage();
            shiftImage.setInterpolate(normalizationInfo.isUseInterpolation());
            shiftImage.setMinimumShift(normalizationInfo.getMinimumShift());
            shiftImage.setFittingUtils(fittingUtils);

            ShiftImage old = new ShiftImage();
            old.setInterpolate(normalizationInfo.isUseInterpolation());
            old.setMinimumShift(normalizationInfo.getMinimumShift());

            //If use Background Normalization before computing
            if (normalizationInfo.isDoBackgroundNormalization()) {
                _computeBackgroundNormalization(stackRef);
                ComputationUtils.setAbsoptionCoef(stackRef);
            }

            //Initialize the computed Center of gravity
            initializeShiftImage(stackRef);

            //Init ShiftImage with the stored values
//        shiftImage.setValues(dataController.getComputedStack(LINE_POSITIONS).copyData(), dataController.getComputedStack(CENTER_OF_MASS).copyData());

            double[] mod = new double[1];
            mod[0] = Double.MAX_VALUE;
            double[] mod1 = new double[1];
            mod1[0] = Double.MAX_VALUE;
            //angularStep =  ( angulareRange / number of points ) * number of repetition
            double angularStep = (normalizationInfo.getAngularRange() / stackToShift.getHeight()) ;//* stackToShift.getSize()
            boolean useNormalizedStep = normalizationInfo.isUsePrecomputedAngularStep();


            float testCoef = (float) Math.pow(10, -2);
            int recursivePos = 0;
            mod1[0] = 0;
            //Reset the tmpRef stack ( input should always be the unmodified stack )
            RXImage tmpRef = new RXImage(stackRef);

            //Reset the error numeric container
            mod[0] = Double.MAX_VALUE;

//            testCoef*=10;
            int recursiveNumber = normalizationInfo.getRecursiveNumber();
            while (recursiveNumber > 0 && mod[0] > Math.pow(10, -3)) {
                mod[0] = mod1[0];

                shiftImage.updateValues(tmpRef);

                //Update the shift to apply
                if (recursivePos == 0) {
                    //Compute with the precomputed Step
                    if (useNormalizedStep) {
                        shiftImage.fitValueOnSine(angularStep, true);
                    }
                    //Compute without the precomputed Step
                    else {
                        shiftImage.fitValueOnSine(true);
                    }
                } else {
                    //Compute with the precomputed Step
                    if (useNormalizedStep) {
                        shiftImage.fitValueOnSine(angularStep, false);
                    }
                    //Compute without the precomputed Step
                    else {
                        shiftImage.fitValueOnSine(false);
                    }
                }

                //Retrieve the square dif between real and computed values
                mod1[0] = shiftImage.getFittingUtils().getFitOnSineValue();

                //Shift the tmpRef
                tmpRef = shiftImage.shiftImage(tmpRef, testCoef);

                //Reduce progressivly the mobile mean size
//                if (shiftImage.getFittingUtils().getFitOnSineValue() < 1 && shiftImage.getFittingUtils().getUseMobileMean()) {
//                    shiftImage.getFittingUtils().setMobileWindow((int) (shiftImage.getFittingUtils().getMobileWindow() / 2.0d));
//                    if (shiftImage.getFittingUtils().getMobileWindow() < 4) {
//                        shiftImage.getFittingUtils().setUseMobileMean(false);
//                        System.out.println("Disable mobile mean");
//                    }
//                }

                mod[0] = Math.abs(mod[0] - mod1[0]);

                System.out.println(recursivePos + " -- Error ::" + mod[0]);
                recursiveNumber--;
                recursivePos++;
            }

            //Update the shift of unShifted Image
            old.setFittingUtils(shiftImage.getFittingUtils());

            //Warning setting this update lock the shifting
            old.setValues(dataController.getComputedStack(LINE_POSITIONS).copyData(), dataController.getComputedStack(CENTER_OF_MASS).copyData());
            unShiftedImage = old.shiftImage(stackToShift);

            //Update the reconstruction Function with the obtained
            dataController.setProjectionCenterOfRotation(old.getCenterOfRotation(), unShiftedImage.getHeight() / 2);

            //Store The Fitted Values
            float[] fittedValue = old.getFittingUtils().getFittedValue(dataController.getComputedStack(LINE_POSITIONS).copyData());
            RXImage f = new RXImage(fittedValue.length, 1, 1);
            f.setData(fittedValue);
            dataController.storeVolatileStack(COMPUTED_FIT, f);

            float[] shift = old.getShifting();
            RXImage k = new RXImage(shift.length, 1, 1);
            k.setData(shift);

            //Store the computed volatile stack
            dataController.storeVolatileStack(SHIFT_DATA, k);
            dataController.storeVolatileStack(StackType.PRECOMPUTED_SHIFT, unShiftedImage);

            fireControllerEvent(MMXIControllerEventType.FINISHED, MMXIControllerEventID.COMPUTE_SINUS_SHIFT,null);
        }
        //ERROR null stack
        else{
            MMXIEventData data = new MMXIEventData(ERROR_NULL_STACK);
            fireControllerEvent(MMXIControllerEventType.ERROR, MMXIControllerEventID.COMPUTE_SINUS_SHIFT,data);
        }
        return unShiftedImage;
    }

    /**
     * Initialize the Fitting utils
     * @param imageStack the RXImage
     */
    public void initializeShiftImage(RXImage imageStack){
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

        float[]  valuesX = new float[imageStack.getSize()*imageStack.getHeight()],centerOfMass;
        int  i;

        for ( i = 0; i < valuesX.length ; i++ ){
            valuesX[i] = i;
        }

        centerOfMass = ComputationUtils.computeGravityCenter1D(imageStack);

        RXImage com = new RXImage(centerOfMass.length,1,1);
        RXImage px = new RXImage(centerOfMass.length,1,1);

        com.setData(centerOfMass);
        px.setData(valuesX);

        //Update the stored X/Y values
        dataController.updateFitCurve(px, com);

        //Fire Event Finish Initialization
        fireControllerEvent(MMXIControllerEventType.FINISHED,MMXIControllerEventID.INIT_SINUS_SHIFT,null);
    }


    public RXImage wobbleCorrection(RXImage toCorrect){

        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
        RXImage shiftData = dataController.getComputedStack(SHIFT_DATA);


        if ( dataController.getComputedStack(SHIFT_DATA) == null ){
            //Try to compute the Shift_DATA ( this could contains eternal loop )
            if ( _shiftImageWithUpdate(dataController.getDefaultRXImage(),dataController.getDefaultRXImage()) != null ){
                wobbleCorrection(toCorrect);
            }
        }

        return ShiftImage.wobbleCorrection(toCorrect,shiftData);

    }

    /**
     * Reslice the given stackType based on the currently seted orientation and the given orientation.
     * @param stackData the ID of stackType to retrieve
     * @param orientationType The asked new orientation
     * @return The resulting reslice RXImage
     */
    public RXImage setStackOrientation(StackData stackData,OrientationType orientationType){
        if ( stackData != null ) {

            DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

            return setStackOrientation(dataController.getComputedStack(stackData), orientationType);
        }

        return null;
    }

    /**
     * Reslice the given stackType based on the currently seted orientation and the given orientation.
     * @param stackData the ID of stackType to retrieve
     * @param orientationType The asked new orientation
     * @param withUpdate DO NOT USE
     * @return The resulting reslice RXImage
     */
    public RXImage setStackOrientation(StackData stackData,OrientationType orientationType, boolean withUpdate){
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
        return setStackOrientation(dataController.getComputedStack(stackData),orientationType,withUpdate);
    }


    /**
     * Reslice the given stackType based on the currently seted orientation and the given orientation.
     * @param stackType the ID of stackType to retrieve
     * @param orientationType The asked new orientation
     * @return The resulting reslice RXImage
     */
    @Deprecated
    public RXImage setStackOrientation(StackType stackType,OrientationType orientationType){
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
        return setStackOrientation(dataController.getComputedStack(stackType),orientationType);
    }

    /**
     * Reslice the given stackType based on the currently seted orientation and the given orientation.
     * @param stackType the ID of stackType to retrieve
     * @param orientationType The asked new orientation
     * @param withUpdate DO NOT USE
     * @return The resulting reslice RXImage
     */
    @Deprecated
    public RXImage setStackOrientation(StackType stackType,OrientationType orientationType, boolean withUpdate){
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
        return setStackOrientation(dataController.getComputedStack(stackType),orientationType,withUpdate);
    }


    /**
     * Change the Stack orientation based on the {@link com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.OrientationType}.
     * @param imageStack The stack to reoriente
     * @param orientationType The wanted orientation type
     * @param withUpdate true if the current Orientation have to be modified
     * @return The resulting reslice RXImage
     */
    public RXImage setStackOrientation(RXImage imageStack,ComputationUtils.OrientationType orientationType, boolean withUpdate){
        ComputationUtils.OrientationType current = SessionObject.getCurrentSessionObject().getAnalyseFactory().getCurrent().getDimension();
        if ( imageStack == null ){
            return null;
        }
        if ( withUpdate ){
            SessionObject.getCurrentSessionObject().getAnalyseFactory().getCurrent().setDimension(orientationType);
        }
        return setStackOrientation(imageStack, current, orientationType);
    }

    /**
     * Reslice the given stackType based on the currently set orientation and the given orientation.
     * @param imageStack The image to reslice
     * @param orientationType The asked new orientation
     * @return The resulting reslice RXImage
     */
    public RXImage setStackOrientation(RXImage imageStack,OrientationType orientationType){
        ComputationUtils.OrientationType current = SessionObject.getCurrentSessionObject().getAnalyseFactory().getCurrent().getDimension();
        if ( imageStack == null ){
            return null;
        }
        return setStackOrientation(imageStack, current, orientationType);
    }


    public RXImage setStackOrientation(RXImage imageStack,OrientationType fromOrientationType, OrientationType toOrientationType){
        RXImage result = ComputationUtils.convertDimension(imageStack, fromOrientationType, toOrientationType);

        fireControllerEvent(MMXIControllerEventType.FINISHED,MMXIControllerEventID.COMPUTE_ORIENTATION,null);

        return result;
    }
}
