/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.controller;

import com.soleil.nanoscopium.hdf5Opener.Hdf5Handler;
import com.soleil.nanoscopium.hdf5Opener.Hdf5PlugIn;
import com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStack;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.RXTomoJComputationFrame;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoInit.DataHdf5;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoInit.Hdf5openerJFrame;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoInit.SortedPanel;
import com.soleil.nanoscopium.rxtomoj.gui.utils.DialogMessage;
import com.soleil.nanoscopium.rxtomoj.model.SessionObject;
import com.soleil.nanoscopium.rxtomoj.model.SessionObject.DataType;

import javax.swing.*;
import java.awt.*;
import java.net.URL;
import java.util.*;

/**
 * Created by antoine bergamaschi on 13/01/14.
 */
public class RXTomoJViewController {
    public final static int HDF5_FRAME=0,COMPUTATION_FRAME=1;
    public final static Class HDF5_FRAME_CLASS = Hdf5openerJFrame.class,
    COMPUTATION_FRAME_CLASS = RXTomoJComputationFrame.class;

    public final static int POSITION_CARTESIAN=0,POSITION_RAW=1;
    public final static int INNER_SPOT = 0,INNER_PREPROCESSING = 1,INNER_MODALITIES = 2,INNER_RECONSTRUCTION=4,INNER_POSTPROCESSING=3;

    private static int ID=0;
    private HashMap<Class,Integer> frameModel = new HashMap<>();

    private ArrayList<JFrame> openWindow = new ArrayList<>();
    private int currentFrame=COMPUTATION_FRAME;

    private RXTomoJ controller;
    private ResourceBundle ressource;
    private ResourceBundle icone;
    private boolean lockSessionUpdate = false;

    private String oldSessionPath = null;
    private int positionDisplay=POSITION_RAW;

    private HashMap<String,JFrame> innerFrameList = new HashMap<>();

    public RXTomoJViewController(RXTomoJ controller){
        super();
        //Init the dialogMessage Class
        DialogMessage.init(this);

        //Initialize JFrame Map order
        frameModel.put(RXTomoJViewController.HDF5_FRAME_CLASS,RXTomoJViewController.HDF5_FRAME);
        frameModel.put(RXTomoJViewController.COMPUTATION_FRAME_CLASS,RXTomoJViewController.COMPUTATION_FRAME);


        //Default bundle Resource is English
        this.ressource = ResourceBundle.getBundle("RXTomoJ", Locale.ENGLISH);
//        this.icone = ResourceBundle.getBundle("image");
        setUIFont (new javax.swing.plaf.FontUIResource(new Font("Arial",Font.BOLD, 12)));

        //Instantiate a new Hdf5openerJFrame which will be linked in the controller
        openFrame(RXTomoJViewController.COMPUTATION_FRAME);
        this.controller  = controller;


        RXTomoJ.getInstance().getModelController().addMMXIControllerUpdatedListener(l->{
            if ( l.getEventType() != MMXIControllerEvent.MMXIControllerEventType.ERROR ) {
//                System.out.println("An " + l.getEventType().toString() + " Occurs :: " + l.getEventID().toString() + " ... " + new Date().getTime());
                System.out.println(l.toString());
            }
            else{
                System.err.println(l.toString());
//                System.err.println("An " + l.getEventType().toString() + "( "+l.getEventType().getIdentifier()+" ) Occurs :: " + l.getEventID().toString() + " ... " + new Date().getTime());
            }

//            switch (l.getEventType() ){
//                case ERROR:
//                    System.out.println("An Error Occurs");
//                    break;
//                case WARNING:
//                    if ( l.getEventSource() instanceof ComputationController ){
//                        switch (l.getEventType().getIdentifier()){
//                            case ComputationController.WARNING_NOELEMENTS:
//                                System.out.println("Warning No elements");
//                                break;
//                            case ComputationController.WARNING_NOSPCPATH:
//                                System.out.println("Warning no spectrum");
//                                break;
//
//                            case ComputationController.WARNING_NOTRANSMISSION_SEL:
//                                System.out.println("No Transmission Modality selected");
//                            default:
//                                break;
//                        }
//                    }
//                    else{
//                        System.out.println("A Warning Occurs");
//                    }
//                    break;
//                case UPDATE:
//                    if ( l.getEventID() == MMXIControllerEvent.MMXIControllerEventID.PROGRESS_FUNCTION ){
//                        System.out.println(l.getEventType().getValue());
//                    }
//                    System.out.println("An Update Occurs :: "+l.getEventID().toString()+" ... "+new Date().getTime());
//                    break;
//            }
        });
    }

    public String getRessourceValue(String key){
        return this.ressource.getString(key);
    }

    public ImageIcon getRessourceIcon(String key){
      URL url = this.getClass().getResource("/image/"+key);
      return new ImageIcon(url);
    }

    public RXTomoJ getController(){
        return this.controller;
    }

    public int getPositionDisplay() {
        return positionDisplay;
    }

    /**
     * Get the specified JFrame
     * @param frameID the frame ID to retrieve
     * @return JFrame the frame retrieved
     * @see com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController
     */
    public JFrame getFrame(Class frameID){
        for ( Class className : frameModel.keySet() ){
            if ( className == frameID){
                return getInOpenWindow(className,true);
            }
        }
        return null;
    }


    public JFrame getCurrentFrame(){
        return getFrame(openWindow.get(currentFrame).getClass());
    }

    private JFrame getInOpenWindow(Class className,boolean requestFocus){
        for ( int i = 0 ; i < openWindow.size() ;i++ ){
            if ( openWindow.get(i).getClass() == className ){
                if ( requestFocus ){
                    openWindow.get(i).requestFocus();
                }
                this.currentFrame = i;
                return openWindow.get(i);
            }
        }
        return null;
    }

    private void removeWindow(Class className){
        for ( int i = 0 ; i < openWindow.size() ;i++ ){
            if ( openWindow.get(i).getClass() == className ){
                openWindow.remove(i);
                if ( openWindow.size() > 0 ){
                    this.currentFrame = 0;
                    openWindow.get(0).requestFocus();
                }
            }
        }
    }

    private JFrame createNewInstance(int frameID){
        JFrame jFrame = null;
        switch (frameID){
//            case RXTomoJViewController.COMPUTATION_FRAME:
//                jFrame =  new RXTomoComputationJFrame(this);
//                break;
            case RXTomoJViewController.HDF5_FRAME:
                jFrame =  new Hdf5openerJFrame(this);
                break;
            case RXTomoJViewController.COMPUTATION_FRAME:
                jFrame =  new RXTomoJComputationFrame(this);
               break;
            default:
                System.err.println("Unknown Frame ID : " + frameID+" in class "+this.getClass());
                break;
        }

        if ( jFrame !=null ){
            jFrame.requestFocus();
        }

        return jFrame;
    }

    private void fireUpdate(int frameID, JFrame frame){
        switch (frameID){
//            case RXTomoJViewController.COMPUTATION_FRAME:
//                ((RXTomoComputationJFrame)frame).update();
//                break;
            case RXTomoJViewController.HDF5_FRAME:
//                ((Hdf5openerJFrame)frame).update();
                break;
            case RXTomoJViewController.COMPUTATION_FRAME:
                ((RXTomoJComputationFrame)frame).update();
                break;
            default:
                System.err.println("Unknown Frame ID : " + frameID+" in class "+this.getClass());
                break;
        }
    }

    private void fireReset(int frameID, JFrame frame){
        switch (frameID){
//            case RXTomoJViewController.COMPUTATION_FRAME:
//                ((RXTomoComputationJFrame)frame).update();
//                break;
            case RXTomoJViewController.HDF5_FRAME:
//                ((Hdf5openerJFrame)frame).update();
                break;
            case RXTomoJViewController.COMPUTATION_FRAME:
                ((RXTomoJComputationFrame)frame).reset();
                break;
            default:
                System.err.println("Unknown Frame ID : " + frameID+" in class "+this.getClass());
                break;
        }
    }

    /**
     * Close the window and dispose of the ressource linked. SessionObject may be save between window change
     * @param frameToClose the JFrame to close
     */
    public void removeFrame(JFrame frameToClose){
        int type = frameModel.get(frameToClose.getClass());
        switch (type){
//            case RXTomoJViewController.COMPUTATION_FRAME:
//                frameToClose.dispose();
//                break;
            case RXTomoJViewController.HDF5_FRAME:
                //Yes or No close window
                removeWindow(frameToClose.getClass());
                frameToClose.dispose();
                break;
            case RXTomoJViewController.COMPUTATION_FRAME:
                frameToClose.dispose();
                break;
        }

        this.updateWindow(false);
    }

    public void openFrame(int frameID){
        for ( Class className : frameModel.keySet() ){
            if ( frameModel.get(className) == frameID){
                if(getInOpenWindow(className,true) == null ){
                    openWindow.add(createNewInstance(frameID));
                    this.currentFrame = openWindow.size()-1;
                }
            }
        }
    }

    /**
     * Update windows with the current SessionObject parameters
     * @param withReset , if a reset has to be fire in the innerFrame before update
     */
    public void updateWindow(boolean withReset){
        for ( Object frame : openWindow ){
            int type = frameModel.get(frame.getClass());
            if ( withReset ){
                this.fireReset(type, (JFrame) frame);
            }
            this.fireUpdate(type, (JFrame) frame);
        }
    }

    public void updateRoiSpot(){
        ((RXTomoJComputationFrame)this.getInOpenWindow(RXTomoJViewController.COMPUTATION_FRAME_CLASS,true)).updateRoi();
    }

    public void displayCurrentFrame(){

    }

    public void previousFrame(){

    }

    public void nextFrame(){

    }

    /**
     * Update the model by adding information in the sessionObject
     * @param collectionToAdd The basic Object representing datasetNames in the interface
     * @param PanelID The panel which should be updated with this data's
     */
    public void updateSessionObjectList( DataHdf5 collectionToAdd , int PanelID){
        //Do not update the Session object if the update come from the synchronisation of a sessionObject
        if ( !this.lockSessionUpdate){
//            SessionObject sessionObject = SessionObject.getCurrentSessionObject();
//            ArrayList<String> obj = null;

            switch (PanelID){
                case SortedPanel.IMAGE:
                    RXTomoJ.getInstance().getModelController().getSessionController().addRawDataset(DataType.IMAGE, collectionToAdd.toString());
                    break;
                case SortedPanel.MOTORS:
                    RXTomoJ.getInstance().getModelController().getSessionController().addRawDataset(DataType.MOTOR, collectionToAdd.toString());
//                    obj = sessionObject.getMotorsID();
                    break;
                case SortedPanel.SAI:
                    RXTomoJ.getInstance().getModelController().getSessionController().addRawDataset(DataType.XBPM, collectionToAdd.toString());
//                    obj = sessionObject.getSAI();
                    break;
                case SortedPanel.SPECTRE:
                    RXTomoJ.getInstance().getModelController().getSessionController().addRawDataset(DataType.SPECTRUM, collectionToAdd.toString());
//                    obj = sessionObject.getSpectrumID();
                    break;
                default:
                    System.err.println("Unknow Panel Type : "+PanelID+" in class "+this.getClass());
            }
        }
    }

    public void setLockSessionUpdate(boolean bol){
        this.lockSessionUpdate = bol;
    }

    //TODO implements this
    public boolean isRawDataView(){
        return true;
    }

    /**
     * Set the image in the displayed JPanelImage
     * @param hdf5VirtualStack The {@link com.soleil.nanoscopium.hdf5Opener.Hdf5VirtualData.Hdf5VirtualStack} containing the raw spot images
     */
    public void setRawSpotImage(Hdf5VirtualStack hdf5VirtualStack){
        ((RXTomoJComputationFrame)this.openWindow.get(this.currentFrame)).setRawSpotImage(hdf5VirtualStack);
    }

    public void openNewHdf5Frame(){
        Hdf5PlugIn hdf5PlugIn = new Hdf5PlugIn();
        hdf5PlugIn.run(null);

        if ( hdf5PlugIn.initialized() ){
           HashMap<String,Hdf5Handler.Dataset_Iterate_cb_o1.DatasetObject> datasets = hdf5PlugIn.getDatasetInformation();
            this.openFrame(RXTomoJViewController.HDF5_FRAME);
            //If there is a current Session Object
            if ( SessionObject.isOpen()){
                //If this session Object is not linked to the Hdf5
                if ( SessionObject.getCurrentSessionObject().getPathToHdf5() != "" && hdf5PlugIn.getPathToHdf5().compareToIgnoreCase(SessionObject.getCurrentSessionObject().getPathToHdf5()) != 0 ){
                    //Would you like to Save before
                    //Save Old Session Path if cancelation occurs
                    oldSessionPath = SessionObject.closeCurrentSessionObject();
                    //Initialize the Session Object
                    SessionObject.createSessionObject().setPathToHdf5(hdf5PlugIn.getPathToHdf5());
                }else{
                    //Do nothing Because this is the Same sessionObject
                }
            }else{
                SessionObject.createSessionObject().setPathToHdf5(hdf5PlugIn.getPathToHdf5());
            }

            ((Hdf5openerJFrame)this.getFrame(RXTomoJViewController.HDF5_FRAME_CLASS)).updateDataFromHdf5(datasets);
            hdf5PlugIn.closeHdf5File();

        }
        else{
            DialogMessage.showMessage(getRessourceValue("rxTomoJViewController_error_openHdf5_message"),getRessourceValue("rxTomoJViewController_error_openHdf5_title"),DialogMessage.ERROR);
        }
    }

//    @Deprecated
//    public int getSelectedAction(){
//        Frame  frame   =   this.getFrame(RXTomoJViewController.COMPUTATION_FRAME_CLASS);
//        int command = -1;
//        if ( frame != null ){
//            command = ((RXTomoJComputationFrame)frame).getCommand();
//        }
//        return command;
//    }

//    @Deprecated
//    public void showErrorMessage(int frameID,String message){
//        Frame frame = this.getFrame(frameID);
//        JOptionPane.showMessageDialog(frame,
//                message,
//                this.getRessourceValue("rxTomoJViewController_error_title"),
//                JOptionPane.ERROR_MESSAGE);
//    }

    public void fireAbsorption(){

    }


    public void setDisplayPosition(int displayPositionType ){
        ((RXTomoJComputationFrame)this.getInOpenWindow(RXTomoJViewController.COMPUTATION_FRAME_CLASS,false)).setDisplayPosition(displayPositionType);
        this.positionDisplay = displayPositionType;
    }

    private static void setUIFont(javax.swing.plaf.FontUIResource f)
    {
        java.util.Enumeration keys = UIManager.getDefaults().keys();
        while (keys.hasMoreElements())
        {
            Object key = keys.nextElement();
            Object value = UIManager.get(key);
            if (value instanceof javax.swing.plaf.FontUIResource)
            {
                UIManager.put(key, f);
            }
        }
    }

    public boolean getInnerFrame(String key){
        if ( this.innerFrameList.containsKey(key)){
            this.innerFrameList.get(key).requestFocus();
            return true;
        }
        return false;
    }

    public void addInnerFrame(String key, JFrame frame){
        if ( this.innerFrameList.containsKey(key)){
            this.innerFrameList.get(key).requestFocus();
        }
        else{
            this.innerFrameList.put(key,frame);
        }
    }

    public void removeInnerFrame(String key){
        if ( this.innerFrameList.containsKey(key)){
            this.innerFrameList.remove(key);
        }else{
            System.err.println("Error unknown Key : "+this.getClass().toString() );
        }
    }


    public String getOldSessionPath() {
        return oldSessionPath;
    }

    public void setOldSessionPath(String oldSessionPath) {
        this.oldSessionPath = oldSessionPath;
    }
}
