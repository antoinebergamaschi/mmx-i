/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.event;

import com.soleil.nanoscopium.rxtomoj.controller.MMXIController;

import java.util.UUID;

/**
 * Created by antoine bergamaschi on 17/04/2015.
 */
public class MMXIControllerEvent {

    /**
     * MMXI event type. Event type can be warning, error, update , finished or Initialize.
     * Warning correspond to a non-blocking error,
     * Error correspond to a blocking error,
     * Update correspond to a change in a the model,
     * Initialize correspond to the first step of a computation function,
     * Finished correspond to the final line of a computation function
     *
     * Warning this is a Static context this Identifier, msg , value, Object and ID should be remove from the enum and
     * place in the MMXIControllerEvent Object to be correctly stored.
     */
    public enum MMXIControllerEventType{
        WARNING,ERROR,UPDATE,FINISHED,INITIALIZE;

        @Override
        public String toString() {
            return  "MMXI Event Type : " + this.name();
        }
    }

    public enum MMXIControllerEventID{
        INIT_CONTROLLER_FINISHED,

        STACK_PATH,

        AUTOMATED_MODALITIES_UPDATED,SESSION_SAVED,SESSION_PATH_CHANGED,SESSION_OPENED,SESSION_CLOSED,BACKGROUND_UPDATED,
        SESSION_RESET,IMAGE_UPDATED,MOTOR_UPDATED,SPECTRUM_UPDATE,XBPM_UPDATED,ANALYSIS_UPDATED, INIT_DATASET,HDF_OPENED,

        REGRID_FUNCTION,

        STACKDATA_STORED,

        FLUO_CALIBRATION_CHANGED, FLUO_ELEMENTS_CHANGED, FLUO_MEANSPECTRUM_CHANGED, PHASE_MIRRORING_CHANGED, ORIENTATION_CHANGED,CURRENT_IMAGE_CHANGED,
        MOTORS_CHANGED, IMAGE_STACK_CHANGED, FLUORESCENCE_STACK_CHANGED, COMPUTESPOT_CHANGED, COMPUTESPOT_ROI_SPOT,COMPUTESPOT_ROI_RECONSTRUCTION,
        FLUORESCENCE_CHANGED, XBPM_CHANGED,VOLATILE_STACK_CHANGED,ART_STACK_CHANGED, FBP_STACK_CHANGED,

        SHIFT_CHANGED, WOBBLE_PARAMETER_CHANGED, REGRIDDING_PARAMETER_CHANGED, BACKGROUND_PARAMETER_CHANGED,


        DEFAULT_MODALITY,

        RECONSTRUCTION_ALGEBRAIC_CHANGED,RECONSTRUCTION_FILTERED_CHANGED,RECONSTRUCTION_CHANGED,RECONSTRUCTION_CENTER_CHANGED,
        RECONSTRUCTION_BASICINFO_CHANGED,RECONSTRUCTION_SAMPLING_CHANGED,RECONSTRUCTION_RELAXATION_CHANGED,
        RECONSTRUCTION_ITERATION_CHANGED,RECONSTRUCTION_BGART_CHANGED,RECONSTRUCTION_POSITIVITY_CHANGED,
        RECONSTRUCTION_BGCoef_CHANGED,RECONSTRUCTION_FILTER_CHANGED,

        REDUCTIONINFO_TRANSMISSION_ROI,REDUCTIONINFO_REDUCTION_ROI,REDUCTIONINFO_SPOT_DIMENSION,REDUCTIONINFO_SPOT_2D,
        LOW_RESOLUTION_MODE,SAMPLING_CHANGED,NUMBER_OF_LINE_READED,SPOT_FILTER_VALUE,SPOT_FILTER_RANGE,
        HOT_SPOT_ITERATION,HOT_SPOT_RADIUS,HOT_SPOT_SPREADCOEF,

        RETRIEVAL_ALGO,PHASE_COMPUTATION,USE_MIRRORING,

        DARKFIELD_INCORRECT_RANGE_ID,DARKFIELD_INCORRECT_RANGE,DARKFIELD_RANGE_UPDATED,DARKFIELD_SHAPE_UPDATED,
        DARKFIELD_ORIENTATION,

        PROGRESS_FUNCTION, INIT_SINUS_SHIFT, COMPUTE_SINUS_SHIFT, COMPUTE_ORIENTATION, COMPUTE_NORMALIZATION,
        REGRID_POSITION,COMPUTE_PHASE_RETRIEVAL,

        BASIC_IMAGE_DIMS_UPDATED,

        SAVE_IMAGE,LOAD_IMAGE,LOAD_HDF;

        @Override
        public String toString() {
            return "MMXI EVENT ID : "+this.name();
        }
    }

    private MMXIControllerEventType type = null;
    private MMXIController source = null;
    private MMXIControllerEventID id = null;
    private MMXIEventData data = null;

    public MMXIControllerEvent(MMXIControllerEventType type, MMXIController source,
                               MMXIControllerEventID id, MMXIEventData data){
        this.type = type;
        this.source = source;
        this.id = id;
        this.data = data;
    }

    public MMXIControllerEventType getEventType(){
        return this.type;
    }

    public MMXIControllerEventID getEventID(){
        return this.id;
    }

    public MMXIController getEventSource(){
        return this.source;
    }

    public MMXIEventData getEventData(){
        return this.data;
    }

    @Override
    public String toString() {
        String toreturn = "--------------------------------------------------------------------------------\n";
        toreturn += id.toString()+"\n";
        toreturn += type.toString()+"\n";
        //Data can be null
        if ( data != null ) {
            toreturn += data.toString() + "\n";
        }
        toreturn += source.toString();
        return toreturn;
    }
}
