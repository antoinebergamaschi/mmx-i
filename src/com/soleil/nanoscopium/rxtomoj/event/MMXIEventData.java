/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.event;

import java.util.UUID;

/**
 * MMXIEventData is the class containing every information relevant to understand and display EventData.
 * Object should not be accessible via setter, but initialized in a given constructor.
 * Created by bergamaschi on 02/02/2016.
 */
public class MMXIEventData {
    private int identifier = -1;
    private String msg = null;
    private float value  = -1;
    private Object object = null;
    private UUID ID = null;

    public MMXIEventData(){
    }

    public MMXIEventData(int identifier){
        this();
        this.identifier = identifier;
    }

    public MMXIEventData(int identifier, String msg){
        this(identifier);
        this.msg = msg;
    }

    public MMXIEventData(int identifier, String msg, float value){
        this(identifier,msg);
        this.value = value;
    }

    public MMXIEventData(int identifier, String msg, float value, UUID uuid){
        this(identifier,msg,value);
        this.ID = uuid;
    }

    public MMXIEventData(int identifier, String msg, float value, UUID uuid, Object o){
        this(identifier,msg,value,uuid);
        this.object = o;
    }

    public MMXIEventData(UUID uuid, Object o){
        this.ID = uuid;
        this.object = o;
    }

    public MMXIEventData(UUID uuid, int identifier){
        this.ID = uuid;
        this.identifier = identifier;
    }

    public MMXIEventData(UUID uuid, int identifier, Object o){
        this.ID = uuid;
        this.identifier = identifier;
        this.object = o;
    }

    public MMXIEventData(UUID uuid, int identifier, float value){
        this.ID = uuid;
        this.identifier = identifier;
        this.value = value;
    }

    public MMXIEventData(UUID uuid, float value){
        this.ID = uuid;
        this.value = value;
    }

    public MMXIEventData(UUID uuid){
        this.ID = uuid;
    }

    public MMXIEventData(String msg){
        this.msg = msg;
    }


    public UUID getID() {
        return ID;
    }

    public Object getObject() {
        return object;
    }

    public float getValue() {
        return value;
    }

    public String getMsg() {
        return msg;
    }

    public int getIdentifier() {
        return identifier;
    }

    @Override
    public String toString() {
        String toreturn = "MMXI Event Data : ";
        if ( identifier > 0 ){
            toreturn += "\n\t- identifier : "+identifier;
        }
        if ( value > 0 ){
            toreturn += "\n\t- value : "+value;
        }
        if ( ID != null ){
            toreturn += "\n\t- UUID : "+ID.toString();
        }
        if ( msg != null ){
            toreturn +="\n"+msg;
        }
        return toreturn;
    }
}
