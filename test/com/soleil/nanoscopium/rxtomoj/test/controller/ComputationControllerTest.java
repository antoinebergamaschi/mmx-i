/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.test.controller;

import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import com.soleil.nanoscopium.rxtomoj.test.Test_constant;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by bergamaschi on 05/10/2015.
 */
public class ComputationControllerTest {

    //OPEN The Test Session
    @BeforeClass
    public  static void init(){
        RXTomoJ.getInstance().getModelController().getSessionController().openSessionObject(Test_constant.path125);
    }

    @Test
    public void testRegrid(){
//        RXImage toRegrid = RXTomoJ.getInstance().getModelController().getDataController().getComputedStack(ComputationUtils.StackType.ABSORPTION);
//        RXTomoJ.getInstance().getModelController().getComputationController().regrid(toRegrid, ComputationUtils.StackType.ABSORPTION);
    }
}
