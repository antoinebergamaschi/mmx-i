/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.test.controller;

import org.junit.Test;

/**
 * Created by bergamaschi on 19/11/2014.
 */
@Deprecated
public class RXTomoJModelController_test {

    @Test
    public void testRXTomoJModelController(){
//        System.out.println("testRXTomoJModelController");
//        RXTomoJModelController rxTomoJModelController = new RXTomoJModelController();

        //Ouverture rapide
//        ComputeAbsorption computeAbsorption = new ComputeAbsorption();
//        ComputePhaseContrast computePhaseContrast = new ComputePhaseContrast();

        //Ouverture normal
//        Hdf5PlugIn hdf5PlugIn = new Hdf5PlugIn();
//        hdf5PlugIn.run(null);
//        Object[][] datasets = hdf5PlugIn.getDatasets();
//        hdf5PlugIn.computeOnDataset((String) datasets[0][63],computeAbsorbtion);
//        Hdf5Handler hdf5Handler = new Hdf5Handler();
//        hdf5Handler.hdf5_open(filename2);
//        this.hdf5Handler.hdf5_open(filename2);
//        Object[][] datasets = hdf5Handler.getDatasetWithDimensionInformation();
        //Unknow
//        float[] data5 = this.hdf5Handler.readRawData((String) datasets[0][5]);
        //SAI
//        float[] data1 = this.hdf5Handler.readRawData((String) datasets[0][24]);
//        float[] data2 =this.hdf5Handler.readRawData((String) datasets[0][25]);
//        float[] data3 = this.hdf5Handler.readRawData((String) datasets[0][26]);
//        float[] data3 = this.hdf5Handler.readRawData((String) datasets[0][26]);
//        float[] data4 =this.hdf5Handler.readRawData((String) datasets[0][27]);
//        Xpad
//        this.hdf5Handler.readDataset((String) datasets[0][30], computeAbsorbtion);
//        this.hdf5Handler.readDataset((String) datasets[0][30], computePhaseContrast);
        //Tx
//        float[] tx = hdf5Handler.readRawData((String) datasets[0][26]);
        //Tz
//        float[] ty = this.hdf5Handler.readRawData((String) datasets[0][29]);


//        this.hdf5Handler.readDataset((String) datasets[0][63],computeAbsorbtion);
//        this.hdf5Handler.readDataset((String) datasets[0][63],computePhaseContrast);


//        rxTomoJModelController.saveStacks();
//        this.hdf5Handler.hdf5_close();
//        ImagePlus imagePlusX = new ImagePlus(pathToIMG20);
//        RXImage stack   =  new ShiftImage().regriding(imagePlusX.getStack(),tx);
//        ImagePlus imagePlus =  new ImagePlus("test",stack);
//        FileSaver fileSaver  =  new FileSaver(imagePlus);
//        fileSaver.saveAsTiff();
//        ImagePlus imagePlusX = new ImagePlus(pathToIMG6);
//        ImagePlus imagePlusY = new ImagePlus(pathToIMG7);
//        computePhaseContrast.phaseContrastReconstruction(imagePlusX.getStack(),imagePlusY.getStack());

//        ImagePlus absobtion = new ImagePlus(pathToIMG16);
//        rxTomoJModelController.setStack(RXTomoJModelController.ABSORPTION, absobtion.getStack());
//        rxTomoJModelController.BGART(RXTomoJModelController.ABSORPTION);
//        rxTomoJModelController.saveStacks();
//        ImagePlus absobtion2 = new ImagePlus(pathToIMG17);
//        rxTomoJModelController.setStack(RXTomoJModelController.ABSORPTION, absobtion2.getStack());
//        rxTomoJModelController.BGART(RXTomoJModelController.ABSORPTION);
//        rxTomoJModelController.saveStacks();
//        ImagePlus absobtion3 = new ImagePlus(pathToIMG18);
//        rxTomoJModelController.setStack(RXTomoJModelController.ABSORPTION, absobtion3.getStack());
//        rxTomoJModelController.BGART(RXTomoJModelController.ABSORPTION);
//        rxTomoJModelController.saveStacks();
//        ImagePlus absobtion4 = new ImagePlus(pathToIMG19);
//        rxTomoJModelController.setStack(RXTomoJModelController.ABSORPTION, absobtion4.getStack());
//        rxTomoJModelController.BGART(RXTomoJModelController.ABSORPTION);
//        rxTomoJModelController.saveStacks();


//        rxTomoJModelController.setStack(RXTomoJModelController.ABSORPTION,computeAbsorbtion.getStack());
//          rxTomoJModelController.setStack(RXTomoJModelController.PHASECONTRAST,computePhaseContrast.getStack());
//        rxTomoJModelController.setStack(RXTomoJModelController.PHASECONTRAST,imagePlusX.getStack());
        //Create a result image
//        RXImage resultImage = new RXImage(500,1,500);
//        for ( int i = 1 ; i <= 500 ; i ++ ){
//            resultImage.setPixels(rxTomoJModelController.testProjectionBackprojectionProcess(RXTomoJModelController.ABSORPTION, i), i);
//        }
//
//        ImagePlus imagePlusResult = new ImagePlus("resultImage",resultImage);
//        FileSaver f = new FileSaver(imagePlusResult);
//        f.saveAsTiff();

//        rxTomoJModelController.ART(RXTomoJModelController.PHASECONTRAST);

//        rxTomoJModelController.saveStacks();
        //Over Fit the absorbtion image
//        rxTomoJModelController.updateFitCurve(RXTomoJModelController.ABSORPTION);
//        rxTomoJModelController.shiftImageWithUpdate(RXTomoJModelController.ABSORPTION, RXTomoJModelController.ABSORPTION, 1);
//        rxTomoJModelController.backProjection(RXTomoJModelController.ABSORPTION);
//        rxTomoJModelController.BGART(RXTomoJModelController.ABSORPTION);
//        rxTomoJModelController.filterdBackProjection(RXTomoJModelController.ABSORPTION);
        //Fit the abtened filters of the absorbtion on the phasecontrast
//        rxTomoJModelController.shiftImageOnRef(rxTomoJModelController.getStorageData(RXTomoJModelController.PHASECONTRAST),RXTomoJModelController.ABSORPTION);

//        rxTomoJModelController.ART(RXTomoJModelController.ABSORPTION);
//        rxTomoJModelController.SIRT(RXTomoJModelController.ABSORPTION);
//        rxTomoJModelController.saveStacks();
    }
}
