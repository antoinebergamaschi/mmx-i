/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.test;

import com.soleil.nanoscopium.hdf5Opener.Hdf5PlugIn;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.RXTomoJViewController;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoInit.Hdf5openerJFrame;
import org.junit.After;
import org.junit.Test;

/**
 * Created by antoine bergamaschi on 13/01/14.
 */
public class interfaceTest {

    @After
    public void after(){
        while (true){
            continue;
        }
    }

    @Test
    public void XXMI(){
//        new ImageJ();
        RXTomoJ.getInstance().main(null);
    }

    @Test
    public void HDF5_handler(){
        Hdf5PlugIn hdf5PlugIn = new Hdf5PlugIn();
        hdf5PlugIn.run(null);

//        Object[][] datasets = hdf5PlugIn.getDatasets();

        Hdf5openerJFrame hdf5openerJFrame = new Hdf5openerJFrame(new RXTomoJViewController(RXTomoJ.getInstance()));

        hdf5openerJFrame.updateDataFromHdf5(hdf5PlugIn.getDatasetInformation());
        hdf5PlugIn.closeHdf5File();
    }
}
