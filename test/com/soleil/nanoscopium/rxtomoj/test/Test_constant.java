/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.test;

/**
 * Created by bergamaschi on 01/08/2014.
 */
public class Test_constant {

    public static final String pathToMMXI = "./RX-tomoJ/";

    public static String reconstruction_lines_12 = "C:\\Users\\bergamaschi\\Documents\\image\\Simulation_lines\\12_45_1000\\Abs.tif";

    public static String pathResourceTest = pathToMMXI+"./test/resources/";
    public static String pathReconstructionImage = "image/reconstruction/";
    public static String pathPhaseContrastImage = "image/phaseContrast/";
    public static String pathFilterImage = "image/filter/";
    public static String pathDarkField = pathResourceTest+"./image/darkField/";
    public static String hdf5_path = "C:\\Users\\antoine bergamaschi\\Documents\\testHDF5\\";
    public static String hdf5_path2 = "C:\\Users\\bergamaschi\\Documents\\testHDF5\\";
    public static String hdf5_path3 = "D:\\run2\\";

//    public static String path = "C:\\Users\\antoine bergamaschi\\Documents\\output_image_test\\";
    public static String path = "D:\\SessionObject\\";
//    public static String path = "D:\\output_image_test\\";
//    public static String path = "/media/bergamaschi/cari/output_image_test/";

//    public static String pathReconstruction = "/media/bergamaschi/cari/testReconstruction/";
    public static String pathReconstruction = "C:\\Users\\antoine bergamaschi\\Documents\\testReconstruction\\";
//    public static String pathReconstruction = "C:\\Users\\bergamaschi\\Documents\\testReconstruction\\";
//    public static String pathReconstruction = "D:\\testReconstruction\\";
//
    public static String pathTrashTest = "./test/Trash/";

    public static String pathMoustic = "G:\\moustic.tif";
//    public static String pathSquares = "D:\\squares-1.tif";
    public static String pathSimpleForm = pathResourceTest+pathReconstructionImage+"simpleForm.tif";
    public static String pathSimpleFormProj = pathResourceTest+pathReconstructionImage+"simpleForm_proj.tif";

    public static String pathSquares = pathResourceTest+pathReconstructionImage+"square.tif";
    public static String pathSquares256 = pathResourceTest+pathReconstructionImage+"square_256.tif";
    public static String pathSquaresTube = pathResourceTest+pathReconstructionImage+"tube.tif";
    public static String pathSquaresTubeProj = pathResourceTest+pathReconstructionImage+"tube_proj.tif";
    public static String pathSquaresProj = pathResourceTest+pathReconstructionImage+"square_proj.tif";
    public static String pathSquares256Proj = pathResourceTest+pathReconstructionImage+"square_256_proj.tif";
    public static String pathSquares1D = pathResourceTest+pathReconstructionImage+"square1D.tif";
    public static String pathSquares1DProj = pathResourceTest+pathReconstructionImage+"square1D_proj.tif";
    public static String pathCubeTest = pathResourceTest+pathReconstructionImage+"cube.tif";
    public static String pathCubeTestProj = pathResourceTest+pathReconstructionImage+"cube_proj.tif";
    public static String pathSphereTest = pathResourceTest+pathReconstructionImage+"Sphere.tif";
    public static String pathSphereTestProj = pathResourceTest+pathReconstructionImage+"Sphere_proj.tif";
    public static String pathTest = pathResourceTest+pathReconstructionImage+"Test.tif";
    public static String pathTest1 = pathResourceTest+pathReconstructionImage+"Test1.tif";
    public static String pathTest2 = pathResourceTest+pathReconstructionImage+"Test2.tif";
    public static String pathTest3 = pathResourceTest+pathReconstructionImage+"Test3.tif";

    //Phase
    public static String pathMiteX = pathResourceTest+pathPhaseContrastImage+"X.tif";
    public static String pathMiteXFromIntensity = pathResourceTest+pathPhaseContrastImage+"X_intensity.tif";
    public static String pathMiteXcropped = pathResourceTest+pathPhaseContrastImage+"Xcropped.tif";
    public static String pathMiteXcropped_I = pathResourceTest+pathPhaseContrastImage+"Xcropped_I.tif";

    public static String pathMiteY = pathResourceTest+pathPhaseContrastImage+"Y.tif";
    public static String pathMiteYFromIntensity = pathResourceTest+pathPhaseContrastImage+"Y_intensity.tif";
    public static String pathMiteYcropped = pathResourceTest+pathPhaseContrastImage+"Ycropped.tif";
    public static String pathMiteYcropped_I = pathResourceTest+pathPhaseContrastImage+"Ycropped_I.tif";

    //Filter
    public static String pathFilterSpotMap = pathResourceTest+pathFilterImage+"Spot_Map.tif";
    //DarkField
    public static String pathOrientationImage2 = pathDarkField+"orientation2.tif";

    public static String pathLine12 = "C:\\Users\\bergamaschi\\Documents\\image\\Simulation_lines\\12.tif";
    public static String pathShiftSpot = "D:\\testSessionObject\\TestFilterFunction\\shiftSpot\\";

    public static String path281 = path +"281\\";
    public static String path79 = path +"79\\";
    public static String path117 = path +"117\\";
    public static String path156 = path +"156_window\\";
//    public static String path156 = path +"156\\";
    public static String path155 = path +"155\\";
    public static String path125 = path +"125\\";
    public static String path63 = path +"63\\";
    public static String path204 = path + "204\\";
    public static String path203 = path + "203\\";
    public static String path265 = path + "265\\";
    public static String path135 = path + "135\\";
    public static String path136 = path + "136\\";
    public static String path143 = path + "143\\";
    public static String path182 = path + "182\\";

    public static String pathToSessionObject156 = path156+"156.xml";
    public static String pathToSessionObject155 = path155+"155.xml";
    public static String pathToSessionObject125 = path125+"125.xml";
    public static String pathToSessionObject281 = path281+"281.xml";
    public static String pathToSessionObject117 = path117+"117.xml";
    public static String pathToSessionObject63 = path63+"63.xml";
    public static String pathToSessionObject204 = path204 + "204.xml";
    public static String pathToSessionObject203 = path203 + "203.xml";
    public static String pathToSessionObject265 = path265 + "265.xml";
    public static String pathToSessionObject135 = path135 + "135.xml";
    public static String pathToSessionObject136 = path136 + "136.xml";
    public static String pathToSessionObject143 = path143 + "143.xml";
    public static String pathToSessionObject182 = path182 + "182.xml";
}
