/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.test.model.filters;

import com.soleil.nanoscopium.rximage.IO.RXImageIO_IJ;
import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.exception.RXImageException;
import com.soleil.nanoscopium.rximage.util.RXUtils;
import com.soleil.nanoscopium.rxtomoj.model.filters.HotSpot_Detection;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import com.soleil.nanoscopium.rxtomoj.test.Test_constant;
import ij.ImageJ;
import org.junit.Test;

import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Created by bergamaschi on 30/11/2015.
 */
public class HotSpotFilter_test {

    @Test
    public void testFilter(){

        new ImageJ();

        RXImageIO_IJ rxImageIO_ij = new RXImageIO_IJ();
        RXImage image = null;
        RXImage old = null;
        try {
            image = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathFilterSpotMap),false);
            old = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathFilterSpotMap),false);
        } catch (RXImageException e) {
            e.printStackTrace();
        }

//        final float[] data = image.copyData();
        final int[] spotMap = new int[image.getGlobalSize()];

        //Create Spot map
        image.computeData(inside->{
            for ( int i = 0 ; i < inside.length ; i++ ){
                spotMap[i] = (int) inside[i];
            }
        });


//        for ( int k = 0 ; k < data.length ; k++ ){
//            if ( Math.random() > 0.95 ){
//                data[k] += Math.random()*Math.pow(10,Math.random()*3);
//            }
//        }

        RXUtils.RXImageToImagePlus(old).show();

        ArrayList<ComputationUtils.PixelType> pixelTypes = new ArrayList<>();
        pixelTypes.add(ComputationUtils.PixelType.SPOT);

        //add random noise in data
        image.computeData(inside->{
            for ( int k = 0 ; k < inside.length ; k++ ) {
                if (spotMap[k] == ComputationUtils.PixelType.SPOT.value() ){
                    if (Math.random() < 0.99) {
                        inside[k] += Math.random() * Math.pow(10, Math.random() * 3);
                    } else {
                        inside[k] += Math.random() * Math.pow(10, Math.random() * 5);
                    }
                }else{
                    if (Math.random() < 0.99) {
                        inside[k] += Math.random() * Math.pow(10, Math.random() * 2);
                    } else {
                        inside[k] += Math.random() * Math.pow(10, Math.random() * 10);
                    }
                }
            }
        });

        //First filter depending of the spot
        image.computeData(f->{
            for (int i = 0 ; i < f.length ; i++ ){
                boolean isIN = false;
                for ( int k = 0; k < pixelTypes.size() ; k++ ){
                    if ( spotMap[i] == pixelTypes.get(k).value()){
                        isIN = true;
                    }
                }
                if ( !isIN ){
                    f[i] = 0;
                }
            }
        });

        RXImage finalImage = new RXImage(image);

        final float[] data = finalImage.copyData();

        //Basic Abs Parameters
        HotSpot_Detection.run3D(data, spotMap, image.getWidth(), image.getHeight(), image.getSize(),
                4, 10, 2, false, pixelTypes);

        //Basic Dark Parameters
//        HotSpot_Detection.run3D(data, spotMap, image.getWidth(), image.getHeight(), image.getSize(),
//                20, 6, 4, false, pixelTypes);

        finalImage.setData(data);

        RXUtils.RXImageToImagePlus(image).show();
        RXUtils.RXImageToImagePlus(finalImage).show();

        while (true){
            continue;
        }
    }

}
