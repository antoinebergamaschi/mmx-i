/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.test.model.filters;

import com.soleil.nanoscopium.hdf5Opener.Hdf5Handler;
import com.soleil.nanoscopium.rxtomoj.model.computationFunction.ComputeSpot;
import com.soleil.nanoscopium.rxtomoj.model.filters.HotSpot_Detection;
import org.junit.Test;


/**
 * Created by bergamaschi on 01/08/2014.
 */
public class ProjectionFilter_test {
    private Hdf5Handler hdf5Handler;
    @Test
    public void filterFunction(){
//        ComputeSpot computeSpot = new ComputeSpot();
//        HotSpot_Detection h = computeSpot.getHotSpot_detection();
//        computeSpot.setDarkField(true);
//        this.hdf5Handler = new Hdf5Handler(filename4);
//        Object[][] datasets = this.hdf5Handler.getDatasetWithDimensionInformation();
//
////        Hdf5VirtualStack hdf5VirtualStack = new Hdf5VirtualStack(filename2,(String) datasets[0][63],false);
//        Hdf5VirtualStack hdf5VirtualStack = new Hdf5VirtualStack(filename4,(String) datasets[0][30],false);
//        computeSpot.applyFilter(data,hdf5VirtualStack.getWidth(),hdf5VirtualStack.getHeigth(),1);
        //Test PlugIn on a unique image
//        float[] data = hdf5VirtualStack.getPixels(0);
//        computeSpot.applyFilter(data,hdf5VirtualStack.getWidth(),hdf5VirtualStack.getHeigth(),1);
//
//        RXImage imageStack = new RXImage(hdf5VirtualStack.getWidth(),hdf5VirtualStack.getHeigth(),1);
//        imageStack.setPixels(data, 1);

        //Test PlugIn On a whole set of images
//        RXImage imageStack = new RXImage(hdf5VirtualStack.getWidth(),hdf5VirtualStack.getHeigth(), 1000);

//        ImagePlus imagePlus =  new ImagePlus("test");
//        h.setSpreadCoef(3);
//        h.setMaxLoop(1);
//        h.setSize(1);
//        Date date = new Date();
//        for ( long i = 1 ; i < 1000 ; i++ ){
//            if ( i > Integer.MAX_VALUE ){
//                break;
//            }
//            float[] data = hdf5VirtualStack.getPixels(i - 1);
//            computeSpot.applyFilter(data,hdf5VirtualStack.getWidth(),hdf5VirtualStack.getHeigth(),1);
//            imageStack.setPixels(data, (int) i);
//        }
//
//        System.out.println("Time : "+(new Date().getTime() -  date.getTime())/(1000.0d));
//
//        imagePlus.setStack(imageStack);
//        imagePlus.show();
//
//        FileSaver fileSaver = new FileSaver(imagePlus);
//        fileSaver.saveAsTiff();
//        h.run(imagePlus.getProcessor());
    }
}
