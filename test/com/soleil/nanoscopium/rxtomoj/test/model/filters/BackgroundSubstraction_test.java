/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.test.model.filters;

import com.soleil.nanoscopium.rximage.IO.RXImageIO_IJ;
import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.exception.RXImageException;
import com.soleil.nanoscopium.rximage.util.RXUtils;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.DataController;
import com.soleil.nanoscopium.rxtomoj.controller.SessionController;
import com.soleil.nanoscopium.rxtomoj.model.filters.BackgroundSubtraction;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;
import ij.ImageJ;
import com.soleil.nanoscopium.rxtomoj.test.Test_constant;
import org.junit.Test;

import java.nio.file.Paths;

/**
 * @author Antoine Bergamaschi
 */
public class BackgroundSubstraction_test {

    @Test
    public void testBackgroundNormalization(){
        System.out.println("testBackgroundNormalization");
        new ImageJ();
        SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

        sessionController.openSessionObject(Test_constant.pathToSessionObject204);

        BackgroundSubtraction backgroundSubtraction = new BackgroundSubtraction();
//        RXTomoJModelController rxTomoJModelController = RXTomoJ.getInstance().getModel();

        backgroundSubtraction.setNumberOfForeseenPixel(5);
        backgroundSubtraction.setVariationCoefficient(0.2f);
        RXImage imageStack = dataController.getComputedStack(StackType.ABSORPTION);
//        RXImage imageStack = new ImagePlus(Test_constant.pathMoustic).getRXImage();
        backgroundSubtraction.computeNormalization(imageStack, true, true, false);

        imageStack = dataController.getComputedStack(StackType.PHASECONTRAST);
//        RXImage imageStack = new ImagePlus(Test_constant.pathMoustic).getRXImage();
        backgroundSubtraction.computeNormalization(imageStack, true, true, true);
//        imageStack = rxTomoJModelController.getStorageData(RXTomoJModelController.StackType.DARKFIELD);

//        imageStack = new ImagePlus(Test_constant.path125+"X.tif").getRXImage();

//        backgroundSubtraction.initBackgroundNormalization(imageStack);
//        new ImagePlus("",imageStack).show();
        RXUtils.RXImageToImagePlus(imageStack).show();
        while (true){
            continue;
        }
    }

    @Test
    public void testBackgroundSubstraction(){
        System.out.println("testBackgroundSubstraction");
        new ImageJ();
        SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

        sessionController.openSessionObject(Test_constant.pathToSessionObject125);

        BackgroundSubtraction backgroundSubtraction = new BackgroundSubtraction();
//        RXTomoJModelController rxTomoJModelController = RXTomoJ.getInstance().getModel();

        RXImage imageStack = dataController.getComputedStack(StackType.ABSORPTION);
        backgroundSubtraction.computeNormalization(imageStack, true, true, false);
//        imageStack = rxTomoJModelController.getStorageData(RXTomoJModelController.StackType.DARKFIELD);


        RXImageIO_IJ rxImageIO_ij = new RXImageIO_IJ();
        try {
            imageStack = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.path125 + "Y6.tif"), false);
        } catch (RXImageException e) {
            e.printStackTrace();
        }

        backgroundSubtraction.computeSubtraction(imageStack);
//        new ImagePlus("",imageStack).show();

        RXUtils.RXImageToImagePlus(imageStack).show();
        while (true){
            continue;
        }
    }

}
