/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.test.model.reconstructionFunction;

import com.soleil.nanoscopium.rximage.IO.RXImageIO_IJ;
import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.exception.RXImageException;
import com.soleil.nanoscopium.rximage.util.RXUtils;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.*;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import com.soleil.nanoscopium.rxtomoj.gui.utils.BasicListener;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.SessionObject;
import com.soleil.nanoscopium.rxtomoj.model.data.ReductionInfo;
import com.soleil.nanoscopium.rxtomoj.model.data.StackData;
import com.soleil.nanoscopium.rxtomoj.model.filters.ProjectionFilter;
import com.soleil.nanoscopium.rxtomoj.model.reconstructionFunction.ReconstructionAlgebraic;
import com.soleil.nanoscopium.rxtomoj.model.reconstructionFunction.ReconstructionFilteredBack;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.ReconstructionType;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;
import com.soleil.nanoscopium.rxtomoj.model.utils.ProgressFunction;
import com.soleil.nanoscopium.rxtomoj.test.Test_constant;
import com.soleil.nanoscopium.rxtomoj.test.testClassType.ReconstructionTest;
import ij.ImageJ;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import tomoj.tomography.TiltSeries;
import tomoj.tomography.TomoReconstruction2;
import tomoj.tomography.filters.FFTWeighting;
import tomoj.tomography.filters.FFTWeightingGPU;
import tomoj.tomography.projectors.ProjectorGPU;
import tomoj.tomography.projectors.VoxelProjector3D;
import tomoj.tomography.projectors.VoxelProjector3DGPU;
import tomoj.utils.GPUDevice;


import javax.swing.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Created by bergamaschi on 01/08/2014.
 */
public class Reconstruction_test {

    @Test
    public void projection(){
        new ImageJ();
        ReconstructionFilteredBack filteredBack = new ReconstructionFilteredBack();
        filteredBack.setCenterOfRotationX(50f);
//        filteredBack.setCenterOfRotationX(75f);
//        filteredBack.setCenterOfRotationX(128f);

        int nProj = 360;
        double increment = 360.0d/nProj;
        RXImageIO_IJ rxImageIO_ij = new RXImageIO_IJ();
        RXImage test = null;
        try {
//            test = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathLine12),false);
//            test = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathSquares256),false);
//            test = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathCubeTest),false);
            test = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathSphereTest),false);
//            test = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathSimpleForm),false);
//            test = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.getPathSquares1D),false);
        } catch (RXImageException e) {
            e.printStackTrace();
        }

        RXImage result = new RXImage(test.getWidth(),test.getHeight(),nProj);

        filteredBack.initReconstructionDimension(test);

        System.out.println(filteredBack.toString());

        float[][] stack = new float[nProj][test.getWidth()*test.getHeight()];
        float[] realProj = new float[test.getWidth()*test.getHeight()];

        float[] stack_r = test.copyData();

        int i = 0;
        Date date = new Date();
        for ( int theta = 0; theta < nProj ; theta++) {
            System.out.println("i :: "+theta*increment) ;
//            float[] tmp = filteredBack.projection(stack_r, Math.toRadians(20), null, new double[]{0}, 1);
            float[] tmp = filteredBack.projection(stack_r, Math.toRadians(theta*increment), null, new double[]{0}, 1);
            System.arraycopy(tmp,0,stack[i],0,tmp.length);
            i++;
        }

        System.out.println("Time :: " + (new Date().getTime() - date.getTime()) / 1000.0f);

        for ( int k = 0; k < nProj ; k++){
            result.setSlice(stack[k],k);
        }

        RXUtils.RXImageToImagePlus(result).show();

        while (true){
            continue;
        }
    }


    @Test
    public void backProjection(){

        new ImageJ();
        ReconstructionFilteredBack filteredBack = new ReconstructionFilteredBack();
//        filteredBack.setCenterOfRotationX(49.5f);
        filteredBack.setCenterOfRotationX(50.0f);
//        filteredBack.setCenterOfRotationX(228);
//        filteredBack.setCenterOfRotationX(226.9f);
//        filteredBack.setCenterOfRotationX(128);
//        filteredBack.setCenterOfRotationX(75f);

        RXImageIO_IJ rxImageIO_ij = new RXImageIO_IJ();
        RXImage test = null;
        try {
//            test = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathSquares1DProj),false);
//            test = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathTest),false);
//            test = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathTest3),false);
//            test = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathSquares256Proj),false);
            test = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathSphereTestProj),false);
//            test = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathSimpleFormProj),false);
//            test = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathSquaresTubeProj),false);
//            test = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.reconstruction_lines_12),false);
        } catch (RXImageException e) {
            e.printStackTrace();
        }


//        filteredBack.setTotalCovorageAngle(180);
        filteredBack.initReconstructionDimension(test);
//        filteredBack.setProjectionFilter(new ProjectionFilter(ProjectionFilter.FilterType.HILBERT));


        System.out.println(filteredBack.toString());

        float[][] stack = new float[test.getSize()][test.getWidth()*test.getHeight()];
        for ( int i = 0 ; i < test.getSize() ; i++ ){
            stack[i] = test.copySlice(i);
        }
//

        Date date = new Date();

        float[] resultA = new float[test.getWidth()*test.getHeight()*test.getWidth()];
        float[] count = new float[test.getWidth()*test.getHeight()*test.getWidth()];
        resultA = filteredBack.retroProjection(test);
//        filteredBack.backProjection(stack[0], Math.toRadians(0), resultA, count);
//        filteredBack.backProjection(stack[0], Math.toRadians(2), resultA, count);
//        filteredBack.backProjection(stack[0], Math.toRadians(36), resultA, count);
//        filteredBack.backProjection(stack[0], Math.toRadians(0), resultA, count);
//        filteredBack.backProjection(stack[0], Math.PI/2, resultA, count);
//        filteredBack.backProjection(stack[136], -(3*Math.PI/4), resultA, count);
//        filteredBack.backProjection(stack[45], Math.toRadians(45), resultA, count);
//        filteredBack.backProjection(stack[180], -Math.PI, resultA, count);
//        filteredBack.backProjection(stack[181], Math.PI, resultA, count);

//        ComputationUtils.arrayDivide(resultA, count);

        RXImage result = new RXImage(test.getWidth(),test.getHeight(),test.getWidth());
        result.setData(resultA);

//        RXImage resultCount = new RXImage(projectionMethod.getWidth(),projectionMethod.getHeight(),projectionMethod.getWidth());
//        resultCount.setData(count);

        System.out.println("Time :: " + (new Date().getTime() - date.getTime()) / 1000.0f);

        RXUtils.RXImageToImagePlus(result).show();
//        RXUtils.RXImageToImagePlus(resultCount).show();

        while (true){
            continue;
        }
    }

    @Category(ReconstructionTest.class)
    @Test
    public void computeRayLenght(){
//        for ( double pos = -50 ; pos < 50; pos++ ) {
//            double l = ComputationUtils.computeRayLenght(-10, pos, Math.PI/4, -50, -50, 50, 50);
//            System.out.println(l);
//        }
        double centerX = 0;
        double centerY = 0;

        double dimensionX = 100;
        double dimensionY = 100;

        double minX = -dimensionX/2;
        double maxX = dimensionX/2;

        double minY = -dimensionY/2;
        double maxY = dimensionY/2;

        //Test with Center in the middle of the square

        double theta = 0;
        for ( int i = 0 ; i < 5 ; i++ ) {
            assertEquals("Area must be 0.125", dimensionX, ComputationUtils.computeRayLenght(centerX, centerY, theta, minX, minY, maxX, maxY), 0.00001);
            theta += Math.PI;
        }

        theta = Math.PI/4;
        for ( int i = 0 ; i < 10 ; i++ ) {
            assertEquals("Area must be 0.125", 141.42, ComputationUtils.computeRayLenght(centerX, centerY, theta, minX, minY, maxX, maxY), 0.01);
            theta += Math.PI/2;
        }

        theta = Math.PI/2;
        for ( int i = 0 ; i < 5 ; i++ ) {
            assertEquals("Area must be 0.125", dimensionY, ComputationUtils.computeRayLenght(centerX, centerY, theta, minX, minY, maxX, maxY), 0);
            theta += Math.PI;
        }

        theta = Math.PI/3;
        for ( int i = 0 ; i < 10 ; i++ ) {
            assertEquals("Area must be 0.125", 115.47 , ComputationUtils.computeRayLenght(centerX, centerY, theta, minX, minY, maxX, maxY), 0.01);
            theta += Math.PI/2;
        }

        //Test with the center out of the middle
        centerX = 17;
        centerY = -33;
        theta = Math.PI/3 + Math.PI/2;
        assertEquals("Area must be 0.125", 72.10 , ComputationUtils.computeRayLenght(centerX, centerY, theta, minX, minY, maxX, maxY), 0.01);

        centerX = -29;
        centerY = 50;
        assertEquals("Area must be 0.125", 24.25 , ComputationUtils.computeRayLenght(centerX, centerY, theta, minX, minY, maxX, maxY), 0.01);
    }


    @Category(ReconstructionTest.class)
    @Test
    public void computeRayLenghtInCircle(){
        double centerX = 0;
        double centerY = 0;

        double radius = 50;

        double centerCircleX = 0;
        double centerCircleY = 0;

        //Test with Center in the middle of the square

        double theta = 0;
        for ( int i = 0 ; i < 5 ; i++ ) {
            assertEquals("Computing Lenght in Circle", 2*radius, ComputationUtils.computeRayLenghtInCircle(centerX, centerY, theta, centerCircleX, centerCircleY, radius), 0.00001);
            theta += Math.PI;
        }

        theta = Math.PI/4;
        for ( int i = 0 ; i < 10 ; i++ ) {
            assertEquals("Computing Lenght in Circle", 2*radius, ComputationUtils.computeRayLenghtInCircle(centerX, centerY, theta, centerCircleX, centerCircleY, radius), 0.01);
            theta += Math.PI/2;
        }

        centerX = 10;
        centerY = 10;
        theta = 0;
        for ( int i = 0 ; i < 5 ; i++ ) {
            assertEquals("Computing Lenght in Circle", 97.979, ComputationUtils.computeRayLenghtInCircle(centerX, centerY, theta, centerCircleX, centerCircleY, radius), 0.001);
            theta += Math.PI;
        }


        centerX = -10;
        centerY = -10;
        theta = 0;
        for ( int i = 0 ; i < 5 ; i++ ) {
            assertEquals("Computing Lenght in Circle", 97.979, ComputationUtils.computeRayLenghtInCircle(centerX, centerY, theta, centerCircleX, centerCircleY, radius), 0.001);
            theta += Math.PI;
        }


        centerX = -40;
        centerY = 20;

        theta = 0;
        for ( int i = 0 ; i < 5 ; i++ ) {
            assertEquals("Computing Lenght in Circle", 91.65151, ComputationUtils.computeRayLenghtInCircle(centerX, centerY, theta, centerCircleX, centerCircleY, radius), 0.001);
            theta += Math.PI;
        }


        theta = Math.PI/3;
        for ( int i = 0 ; i < 5 ; i++ ) {
            assertEquals("Computing Lenght in Circle", 45.0413, ComputationUtils.computeRayLenghtInCircle(centerX, centerY, theta, centerCircleX, centerCircleY, radius), 0.001);
//            theta += Math.PI/2;
        }

        theta = Math.PI/3;
        centerX = -51;
        centerY = 20;
        for ( int i = 0 ; i < 5 ; i++ ) {
            assertEquals("Computing Lenght in Circle", 0, ComputationUtils.computeRayLenghtInCircle(centerX, centerY, theta, centerCircleX, centerCircleY, radius), 0.001);
//            theta += Math.PI/2;
        }

        centerCircleX = 25;
        centerCircleY = 25;
        theta = 0;
        centerX = -51;
        centerY = 25;
        for ( int i = 0 ; i < 5 ; i++ ) {
            assertEquals("Computing Lenght in Circle", 100, ComputationUtils.computeRayLenghtInCircle(centerX, centerY, theta, centerCircleX, centerCircleY, radius), 0.001);
//            theta += Math.PI/2;
        }

        centerCircleX = 50;
        centerCircleY = 50;
        theta = 0;
        centerX = -51;
        centerY = 10;
        for ( int i = 0 ; i < 5 ; i++ ) {
            assertEquals("Computing Lenght in Circle", 60, ComputationUtils.computeRayLenghtInCircle(centerX, centerY, theta, centerCircleX, centerCircleY, radius), 0.001);
//            theta += Math.PI/2;
        }
    }


    @Category(ReconstructionTest.class)
    @Test
    public void computeCoverage(){
        //Test Each adjacent Side
        assertEquals("Area must be 0.125", 0.125, ComputationUtils.computeCoverage(0, 0.5, 0.5, 1),0);
        assertEquals("Area must be 0.125", 0.125, ComputationUtils.computeCoverage(0, 0.5, 0.5, 0),0);
        assertEquals("Area must be 0.125", 0.125, ComputationUtils.computeCoverage(0.5, 0, 0, 0.5),0);
        assertEquals("Area must be 0.125", 0.125, ComputationUtils.computeCoverage(0.5, 0, 1, 0.5),0);
        assertEquals("Area must be 0.125", 0.125, ComputationUtils.computeCoverage(1, 0.5, 0.5, 0),0);
        assertEquals("Area must be 0.125", 0.125, ComputationUtils.computeCoverage(1, 0.5, 0.5, 1),0);
        assertEquals("Area must be 0.125", 0.125, ComputationUtils.computeCoverage(0.5, 1, 1, 0.5),0);

        //Test opposite Side Y
        assertEquals("Area must be 0.5", 0.5, ComputationUtils.computeCoverage(0, 0.5, 1, 0.5),0);
        assertEquals("Area must be 0", 0, ComputationUtils.computeCoverage(0, 0, 0, 1),0);
        assertEquals("Area must be 1", 1, ComputationUtils.computeCoverage(1, 0, 1, 1),0);
        assertEquals("Area must be 0.75", 0.75, ComputationUtils.computeCoverage(0.75, 0, 0.75, 1),0);
        assertEquals("Area must be 0.25", 0.25, ComputationUtils.computeCoverage(0.25, 0, 0.25, 1),0);
        assertEquals("Area must be 0.5", 0.5, ComputationUtils.computeCoverage(0, 0, 1, 1),0);

        //Test opposite Side X
        assertEquals("Area must be 0.25", 0.25, ComputationUtils.computeCoverage(0, 0.25, 1, 0.25),0);
        assertEquals("Area must be 0.375", 0.375, ComputationUtils.computeCoverage(0, 0.5, 1, 0.25),0);

        //Test particular cases
        assertEquals("Area must be 0.0", 0, ComputationUtils.computeCoverage(0, 0, 0, 0),0);
//        assertEquals("Area must be 0.0", 0, ComputationUtils.computeCoverage(-1, -0.5, 0, 0),0);
    }

    @Category(ReconstructionTest.class)
    @Test
    public void computeCoverageWithDirection(){
        double[] dirOut = {0,0};
        double[] dirIn = {1,1};
        double[] dirLeft = {0,1};
        double[] dirRight = {1,0};

        //Test opposite Side X
        assertEquals("Area must be 0.875", 0.875, ComputationUtils.computeCoverage(0, 0.5, 0.5, 0,dirIn),0);
        assertEquals("Area must be 0.875", 0.875, ComputationUtils.computeCoverage(0.5, 0, 0, 0.5,dirIn),0);

        assertEquals("Area must be 0.125", 0.125, ComputationUtils.computeCoverage(0, 0.5, 0.5, 0, dirOut), 0);
        assertEquals("Area must be 0.125", 0.125, ComputationUtils.computeCoverage(0.5, 0, 0, 0.5,dirOut),0);


        assertEquals("Area must be 0.875", 0.875, ComputationUtils.computeCoverage(0.5, 0, 1, 0.5,dirIn),0);
        assertEquals("Area must be 0.875", 0.875, ComputationUtils.computeCoverage(1, 0.5, 0.5, 0,dirIn),0);

        assertEquals("Area must be 0.875", 0.875, ComputationUtils.computeCoverage(0.5, 0, 1, 0.5,dirOut),0);
        assertEquals("Area must be 0.875", 0.875, ComputationUtils.computeCoverage(1, 0.5, 0.5, 0,dirOut),0);


        assertEquals("Area must be 0.125", 0.125, ComputationUtils.computeCoverage(1, 0.5, 0.5, 1,dirIn),0);
        assertEquals("Area must be 0.125", 0.125, ComputationUtils.computeCoverage(0.5, 1, 1, 0.5,dirIn),0);

        assertEquals("Area must be 0.875", 0.875, ComputationUtils.computeCoverage(1, 0.5, 0.5, 1, dirOut), 0);
        assertEquals("Area must be 0.875", 0.875, ComputationUtils.computeCoverage(0.5, 1, 1, 0.5,dirOut),0);


        assertEquals("Area must be 0.875", 0.875, ComputationUtils.computeCoverage(0.5, 1, 0, 0.5,dirIn),0);
        assertEquals("Area must be 0.875", 0.875, ComputationUtils.computeCoverage(0, 0.5, 0.5, 1,dirIn),0);

        assertEquals("Area must be 0.875", 0.875, ComputationUtils.computeCoverage(0.5, 1, 0, 0.5,dirOut),0);
        assertEquals("Area must be 0.875", 0.875, ComputationUtils.computeCoverage(0, 0.5, 0.5, 1,dirOut),0);


        //Test opposite Side X
        assertEquals("Area must be 0.75", 0.75, ComputationUtils.computeCoverage(0, 0.25, 1, 0.25, dirIn),0);
        assertEquals("Area must be 0.625", 0.625, ComputationUtils.computeCoverage(0, 0.5, 1, 0.25, dirIn),0);

        assertEquals("Area must be 0.25", 0.25, ComputationUtils.computeCoverage(0, 0.25, 1, 0.25, dirOut),0);
        assertEquals("Area must be 0.375", 0.375, ComputationUtils.computeCoverage(0, 0.5, 1, 0.25, dirOut),0);

        assertEquals("Area must be 0.75", 0.75, ComputationUtils.computeCoverage(0, 0.25, 1, 0.25, dirLeft),0);
        assertEquals("Area must be 0.625", 0.625, ComputationUtils.computeCoverage(0, 0.5, 1, 0.25, dirLeft),0);

        assertEquals("Area must be 0.25", 0.25, ComputationUtils.computeCoverage(0, 0.25, 1, 0.25, dirRight),0);
        assertEquals("Area must be 0.375", 0.375, ComputationUtils.computeCoverage(0, 0.5, 1, 0.25, dirRight),0);


        //Test opposite Side Y
        assertEquals("Area must be 0.5", 0.5, ComputationUtils.computeCoverage(0.5, 0, 0.5, 1, dirIn),0);
        assertEquals("Area must be 1", 1, ComputationUtils.computeCoverage(0, 0, 0, 1, dirIn),0);
        assertEquals("Area must be 0.25", 0.25, ComputationUtils.computeCoverage(0.75, 0, 0.75, 1, dirIn),0);

        assertEquals("Area must be 0.5", 0.5, ComputationUtils.computeCoverage(0.5, 0, 0.5, 1, dirOut),0);
        assertEquals("Area must be 0", 0, ComputationUtils.computeCoverage(0, 0, 0, 1, dirOut),0);
        assertEquals("Area must be 0.75", 0.75, ComputationUtils.computeCoverage(0.75, 0, 0.75, 1, dirOut),0);

        assertEquals("Area must be 0.5", 0.5, ComputationUtils.computeCoverage(0.5 , 0, 0.5, 1, dirRight),0);
        assertEquals("Area must be 1", 1, ComputationUtils.computeCoverage(0, 0, 0, 1, dirRight),0);
        assertEquals("Area must be 0.25", 0.25, ComputationUtils.computeCoverage(0.75, 0, 0.75, 1, dirRight),0);

        assertEquals("Area must be 0.5", 0.5, ComputationUtils.computeCoverage(0.5 , 0, 0.5, 1, dirLeft),0);
        assertEquals("Area must be 0", 0, ComputationUtils.computeCoverage(0, 0, 0, 1, dirLeft), 0);
        assertEquals("Area must be 0.75", 0.75, ComputationUtils.computeCoverage(0.75, 0, 0.75, 1, dirLeft), 0);

        //Special Case
//         assertEquals("Area must be 1", 1, ComputationUtils.computeCoverage( 1.001, 1.001, 0, 0, dirIn),0);
    }

    @Category(ReconstructionTest.class)
    @Test
    public void computeTriangleInSquare(){
        assertEquals("Area must be 0.5", 0.5, ComputationUtils.computeTriangleRayLine(0.5, 0.5, 10, 10, Math.PI/4), 0.00001);


        assertEquals("Area must be 0.5", 0.5, ComputationUtils.computeTriangleRayLine(0.3, 0.5, 10, 10, Math.PI), 0.00001);
        assertEquals("Area must be 0.7", 0.7, ComputationUtils.computeTriangleRayLine(0.5, 0.3, 10, 10, Math.PI), 0.00001);

        assertEquals("Area must be 0.2", 0.2, ComputationUtils.computeTriangleRayLine(0.3, 0.5, 0.7, 0.7, Math.PI), 0.00001);
        assertEquals("Area must be 0.2", 0.2, ComputationUtils.computeTriangleRayLine(0.5, 0.5, 0.7, 0.7, Math.PI / 2), 0.00001);

        assertEquals("Area must be 0.2", 0.1, ComputationUtils.computeTriangleRayLine(0.0, 0.6, 0.7, 0.7, Math.PI), 0.00001);
        assertEquals("Area must be 0.2", 0.1, ComputationUtils.computeTriangleRayLine(0.6, 0.0, 0.7, 0.7, Math.PI / 2), 0.00001);

        assertEquals("Area must be 0.2", 0.18, ComputationUtils.computeTriangleRayLine(0.5, 0.5, 0.5, 0.7, Math.PI / 4), 0.00001);
        assertEquals("Area must be 0.2", 0.18, ComputationUtils.computeTriangleRayLine(0.0, 0.0, 0.5, 0.7, Math.PI / 4), 0.00001);

        assertEquals("Area must be 0.5", 0.5, ComputationUtils.computeTriangleRayLine(0.0, 0.0, -0.5, -0.5, Math.PI / 4), 0.00001);
        assertEquals("Area must be 0.2", 0.5, ComputationUtils.computeTriangleRayLine(0.0, 0.0, 10, -0.7, Math.PI / 4), 0.00001);
    }


    @Category(ReconstructionTest.class)
    @Test
    public void computePosInSquare(){
        double theta = Math.toRadians(45);
        double[] trueValue = new double[]{10,10,-10,-10};

        assertArrayEquals("Incorrect position T1", trueValue, ComputationUtils.computeIntersectionLineInSquare(0, 0, theta, -10, -10, 10, 10), 0.00001);

        trueValue = new double[]{10,10,0,0};
        assertArrayEquals("Incorrect position T2", trueValue, ComputationUtils.computeIntersectionLineInSquare(0, 0, theta, 0, 0, 10, 10), 0.00001);
        assertArrayEquals("Incorrect position T3", trueValue, ComputationUtils.computeIntersectionLineInSquare(5, 5, theta, 0, 0, 10, 10), 0.00001);

        trueValue = new double[]{10,7.88675,0,2.113248};
        theta = Math.toRadians(30);
        assertArrayEquals("Incorrect position T4", trueValue, ComputationUtils.computeIntersectionLineInSquare(5, 5, theta, 0, 0, 10, 10), 0.00001);


        trueValue = new double[]{10,6.43,0,3.56};
        theta = Math.toRadians(196);
        assertArrayEquals("Incorrect position T5", trueValue, ComputationUtils.computeIntersectionLineInSquare(5, 5, theta, 0, 0, 10, 10), 0.01);

        trueValue = new double[]{6.81,10,3.18,0};
        theta = Math.toRadians(110);
        assertArrayEquals("Incorrect position T6", trueValue, ComputationUtils.computeIntersectionLineInSquare(5, 5, theta, 0, 0, 10, 10), 0.01);

        trueValue = new double[]{3.63,10,-3.63,-10};
        theta = Math.toRadians(110);
        assertArrayEquals("Incorrect position T7", trueValue, ComputationUtils.computeIntersectionLineInSquare(0, 0, theta, -10, -10, 10, 10), 0.01);

//        trueValue = new double[]{6.36,0,10,10};
        trueValue = new double[]{10,0,6.36,-10};
        theta = Math.toRadians(110);
        assertArrayEquals("Incorrect position T8", trueValue, ComputationUtils.computeIntersectionLineInSquare(10, 0, theta, -10, -10, 10, 10), 0.01);
    }


    @Test
    public void backProjection_withoutSession(){
//        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
//        ComputationController computationController = RXTomoJ.getInstance().getModelController().getComputationController();

        RXImageIO_IJ rxImageIO_ij = new RXImageIO_IJ();
        RXImage toReconstruct = null;
        try {
            toReconstruct = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.reconstruction_lines_12),false);
//            toReconstruct = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathSquares),false);
        } catch (RXImageException e) {
            e.printStackTrace();
        }
//        RXImage toReconstruct = new ImagePlus(Test_constant.pathSquares).getRXImage();
        new ImageJ();
        ReconstructionFilteredBack reconstructionFilteredBack = new ReconstructionFilteredBack();
        reconstructionFilteredBack.initReconstructionDimension(toReconstruct);
        reconstructionFilteredBack.setCenterOfRotationX(200);

//        reconstructionFilteredBack.setSampling(1f);
        ProjectionFilter projectionFilter = reconstructionFilteredBack.getProjectionFilter();

        projectionFilter.setWithWindow(false);
        projectionFilter.setCurrentWindowType(ProjectionFilter.WindowType.HAMMING);
        projectionFilter.setCurrentFilterType(ProjectionFilter.FilterType.RAMP);
        projectionFilter.setWindowDimension(2.5f);

        RXImage result = reconstructionFilteredBack.retroProjections(toReconstruct);

        RXUtils.RXImageToImagePlus(result).show();


        while (true){continue;}
    }

    @Test
    public void backProjection_withoutSession_underSampling(){
//        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
//        ComputationController computationController = RXTomoJ.getInstance().getModelController().getComputationController();

        RXImageIO_IJ rxImageIO_ij = new RXImageIO_IJ();
        RXImage toReconstruct = null;
        try {
//            toReconstruct = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.reconstruction_lines_12),false);
//            toReconstruct = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathTest1),false);
            toReconstruct = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathTest),false);
//            toReconstruct = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathSquares),false);
        } catch (RXImageException e) {
            e.printStackTrace();
        }



        int numberOfProj = toReconstruct.getSize();

        RXImage newProjToReconstruct = new RXImage(toReconstruct.getWidth(),toReconstruct.getHeight(),numberOfProj);

        int increment = toReconstruct.getSize()/numberOfProj;

        for( int i = 0 ; i < numberOfProj ; i++ ){
            newProjToReconstruct.setSlice(toReconstruct.copySlice(increment*i),i);
        }

//        RXImage toReconstruct = new ImagePlus(Test_constant.pathSquares).getRXImage();
        new ImageJ();
        ReconstructionFilteredBack reconstructionFilteredBack = new ReconstructionFilteredBack();
        reconstructionFilteredBack.initReconstructionDimension(newProjToReconstruct);

        reconstructionFilteredBack.setCenterOfRotationX(228);

//        reconstructionFilteredBack.setSampling(1f);
        ProjectionFilter projectionFilter = reconstructionFilteredBack.getProjectionFilter();

        projectionFilter.setWithWindow(false);
        projectionFilter.setCurrentWindowType(ProjectionFilter.WindowType.HAMMING);
        projectionFilter.setCurrentFilterType(ProjectionFilter.FilterType.RAMP);
        projectionFilter.setWindowDimension(2.5f);

        RXImage result = reconstructionFilteredBack.retroProjections(newProjToReconstruct);

        RXUtils.RXImageToImagePlus(result).show();


        while (true){continue;}
    }

    @Test
    public void art(){
        System.out.println("art");
        new ImageJ();
        RXTomoJ.getInstance().getModelController().getSessionController().openSessionObject(Test_constant.pathToSessionObject281);

//        RXTomoJ.getInstance().getModelController().getReconstructionController().setReconstructionCenterX(252.19959619140656f);

        ProgressFunction function = RXTomoJ.getInstance().getModelController().getReconstructionController().ART(new StackData(StackType.ABSORPTION));
        Utils.addToLoadBar(function, "Test ART", () -> {

        });
//        RXUtils.RXImageToImagePlus().show();
//        new ImagePlus("",result).show();

        while (true){continue;}
    }

    @Test
    public void art_tomo(){
        RXImageIO_IJ rxImageIO_ij = new RXImageIO_IJ();
        new ImageJ();
        RXImage toReconstruct = null;
        try {
//            toReconstruct = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathSquares256Proj),false);
//            toReconstruct = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathSquares1DProj),false);
//            toReconstruct = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathTest),false);
            toReconstruct = (RXImage) rxImageIO_ij.load(Paths.get("D:\\SessionObject\\t7\\X.tif"),false);
        } catch (RXImageException e) {
            e.printStackTrace();
        }


        ReconstructionFilteredBack reconstructionFilteredBack = new ReconstructionFilteredBack();


//        reconstructionFilteredBack.initReconstructionDimension(imageStack);
        reconstructionFilteredBack.setDoFilter(true);

        ProjectionFilter projectionFilter = new ProjectionFilter(ProjectionFilter.FilterType.HILBERT);

        reconstructionFilteredBack.setProjectionFilter(projectionFilter);

        GPUDevice device = GPUDevice.getBestDevice();
        TomoReconstruction2 reconstruction = new TomoReconstruction2(toReconstruct.getWidth(),toReconstruct.getHeight(),toReconstruct.getWidth());

        toReconstruct = reconstructionFilteredBack.filterData(toReconstruct);

        RXUtils.RXImageToImagePlus(toReconstruct).show();

        reconstruction.setDisplayIterationInfo(false);
        TiltSeries tiltSeries = new TiltSeries(RXUtils.RXImageToImagePlus(toReconstruct));

        tiltSeries.setNormalize(false);

        double[] angle = new double[toReconstruct.getSize()];
        for ( int i = 0; i < toReconstruct.getSize(); i++){
            angle[i] = i*(360.0d/toReconstruct.getSize());
        }

        tiltSeries.setTiltAngles(angle);
        tiltSeries.setProjectionCenter(228,toReconstruct.getHeight()/2.0d);


        reconstruction.initCompletion();

        reconstruction.show();

//        FFTWeightingGPU weighting = new FFTWeightingGPU(device,tiltSeries, 256);
        FFTWeightingGPU weighting = null;

        VoxelProjector3DGPU projector3DGPU = new VoxelProjector3DGPU(tiltSeries,reconstruction,device,weighting);
//        projector.setNbRaysPerPixels(3);
//        if (radioButtonRescaleData.isSelected()) projector.setScale(tiltSeries.getWidth() / reconstruction.getWidth());

        projector3DGPU.initForBP(0,toReconstruct.getHeight(),1);
        reconstruction.WBP(tiltSeries, projector3DGPU, 0, toReconstruct.getHeight());


//        projector3DGPU.initForIterative(0,toReconstruct.getHeight(),1, ProjectorGPU.ONE_KERNEL_PROJ_DIFF,false);
//        reconstruction.OSSART(tiltSeries, projector3DGPU, 3, 0.1, 1);

        projector3DGPU.updateFromGPU(0,toReconstruct.getHeight());

        projector3DGPU.releaseCL_Memory();

        reconstruction.updateAndRepaintWindow();

        while (true){continue;}
    }


    @Test
    public void art_withoutSession(){
        System.out.println("art");

        RXImageIO_IJ rxImageIO_ij = new RXImageIO_IJ();
        RXImage toReconstruct = null;
        try {
//            toReconstruct = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathSquares256Proj),false);
            toReconstruct = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathSimpleFormProj),false);
//            toReconstruct = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathTest),false);
//            toReconstruct = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathReconstruction + "proj2_test.tif"),false);
        } catch (RXImageException e) {
            e.printStackTrace();
        }

//        RXImage toReconstruct = new ImagePlus(Test_constant.pathReconstruction +"proj2_test.tif").getRXImage();

        new ImageJ();
        ReconstructionAlgebraic reconstructionAlgebraic = new ReconstructionAlgebraic();
        reconstructionAlgebraic.initReconstructionDimension(toReconstruct);
        ProjectionFilter projectionFilter = reconstructionAlgebraic.getProjectionFilter();

        reconstructionAlgebraic.setTotalCovorageAngle(360);

//        reconstructionAlgebraic.setSampling(1f);
        reconstructionAlgebraic.setARTrelaxation(0.1f);
        reconstructionAlgebraic.setARTiteration(10);

        reconstructionAlgebraic.setCenterOfRotationX(25);
//        reconstructionAlgebraic.setCenterOfRotationX(128);
//        reconstructionAlgebraic.setCenterOfRotationX(228);
//        reconstructionAlgebraic.setCenterOfRotationX(225.54f);

        RXImage result = reconstructionAlgebraic.ART(toReconstruct);

//        new ImagePlus("",result).show();
        RXUtils.RXImageToImagePlus(result).show();

        while (true){continue;}

    }


    @Test
    public void art_withoutSession_underSampling(){
        System.out.println("art");

        RXImageIO_IJ rxImageIO_ij = new RXImageIO_IJ();
        RXImage toReconstruct = null;
        try {
//            toReconstruct = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathSquares256Proj),false);
            toReconstruct = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathTest),false);
//            toReconstruct = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathReconstruction + "proj2_test.tif"),false);
        } catch (RXImageException e) {
            e.printStackTrace();
        }

//        RXImage toReconstruct = new ImagePlus(Test_constant.pathReconstruction +"proj2_test.tif").getRXImage();

        int numberOfProj = 25;

        RXImage newProjToReconstruct = new RXImage(toReconstruct.getWidth(),toReconstruct.getHeight(),numberOfProj);

        int increment = toReconstruct.getSize()/numberOfProj;

        for( int i = 0 ; i < numberOfProj ; i++ ){
            newProjToReconstruct.setSlice(toReconstruct.copySlice(increment*i),i);
        }

        new ImageJ();
        ReconstructionAlgebraic reconstructionAlgebraic = new ReconstructionAlgebraic();
        reconstructionAlgebraic.initReconstructionDimension(newProjToReconstruct);
        ProjectionFilter projectionFilter = reconstructionAlgebraic.getProjectionFilter();

//        reconstructionAlgebraic.setSampling(1f);
        reconstructionAlgebraic.setARTrelaxation(0.1f);
        reconstructionAlgebraic.setARTiteration(10);

//        reconstructionAlgebraic.setCenterOfRotationX(128);
        reconstructionAlgebraic.setCenterOfRotationX(228);


        RXImage result = reconstructionAlgebraic.ART(newProjToReconstruct);

//        new ImagePlus("",result).show();
        RXUtils.RXImageToImagePlus(result).show();

        while (true){continue;}

    }


    @Test
    public void sirt_withoutSession(){
        System.out.println("sirt");

        RXImageIO_IJ rxImageIO_ij = new RXImageIO_IJ();
        RXImage toReconstruct = null;
        try {
//            toReconstruct = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathSquares256Proj),false);
            toReconstruct = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathTest),false);
//            toReconstruct = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathReconstruction + "proj2_test.tif"),false);
        } catch (RXImageException e) {
            e.printStackTrace();
        }

//        RXImage toReconstruct = new ImagePlus(Test_constant.pathReconstruction +"proj2_test.tif").getRXImage();

        new ImageJ();
        ReconstructionAlgebraic reconstructionAlgebraic = new ReconstructionAlgebraic();
        reconstructionAlgebraic.initReconstructionDimension(toReconstruct);
        ProjectionFilter projectionFilter = reconstructionAlgebraic.getProjectionFilter();

//        reconstructionAlgebraic.setSampling(1f);
        reconstructionAlgebraic.setARTrelaxation(0.1f);
        reconstructionAlgebraic.setARTiteration(10);

//        reconstructionAlgebraic.setCenterOfRotationX(128);
        reconstructionAlgebraic.setCenterOfRotationX(228);

        RXImage result = reconstructionAlgebraic.SIRT(toReconstruct);

//        new ImagePlus("",result).show();
        RXUtils.RXImageToImagePlus(result).show();

        while (true){continue;}

    }



    @Category(ReconstructionTest.class)
    @Test
    public void testProjection_BackprojectionProcess(){
        new ImageJ();
        ArrayList<RXImage> imageStacks = new ArrayList<>();

        RXImageIO_IJ rxImageIO_ij = new RXImageIO_IJ();
        try {
//            imageStacks.add((RXImage) rxImageIO_ij.load(Paths.get(Test_constant.getPathSquares1D),false));
            imageStacks.add((RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathSquares256),false));
        } catch (RXImageException e) {
            e.printStackTrace();
        }

        ArrayList<Float> samplings = new ArrayList<>();
//        samplings.add(0.1f);
//        samplings.add(0.5f);
        samplings.add(1.0f);
//        samplings.add(1.5f);
//        samplings.add(2.0f);

        int nproj = 91;
        double increment = Math.toRadians(1);

        //Create a new Reconstruction Filteredback Object based on the currently saved in the Session Object
        ReconstructionFilteredBack reconstructionFilteredBack = new ReconstructionFilteredBack();
//        reconstructionFilteredBack.setCenterOfRotationX(49.5f);
//        reconstructionFilteredBack.setCenterOfRotationX(127.5f);
        reconstructionFilteredBack.setCenterOfRotationX(128f);
//        reconstructionFilteredBack.setCenterOfRotationY(25);

        int number = 0;
//        ArrayList<String> names = new ArrayList<>();
//        ArrayList<double[]> errors_array = new ArrayList<>();
        for ( RXImage imageStack :  imageStacks) {
            System.out.println("Number ::  " + number);
            for (float sampling : samplings) {
                System.out.println("Sampling :: " + sampling);

                reconstructionFilteredBack.initDimension(imageStack, 1);
//                reconstructionFilteredBack.setSampling(sampling);

//                double[] errors = new double[imageStack.getSize() + 2];
//                float[] projection = new float[imageStack.getWidth() * imageStack.getHeight()];
//                for (int z = 1; z <= imageStack.getSize(); z++) {
                float[] real_reconstruction = imageStack.copyData();
//                   double theta = Math.PI/4;
                float[] tmpProj = new float[(nproj+1) * imageStack.getWidth() * imageStack.getHeight() ];
                int k = 0;
                for (double theta = 0.0; theta < nproj*increment; theta += increment) {
                    double error = 0;
                    float[] test_reconstruction = new float[imageStack.getWidth() * imageStack.getHeight()* imageStack.getSize()];
                    float[] count = new float[test_reconstruction.length];
                    float[] tmp = reconstructionFilteredBack.projection(real_reconstruction,theta, null, new double[]{0}, 1);
                    reconstructionFilteredBack.backProjection(tmp, theta, test_reconstruction, count);

//                    ComputationUtils.arrayDivide(test_reconstruction, count);
                    System.out.println("Theta ::" + theta);
//                    RXImage rxImage = new RXImage(imageStack.getWidth(),imageStack.getHeight(),imageStack.getWidth());
//                    rxImage.setData(test_reconstruction);
//                    RXUtils.RXImageToImagePlus(rxImage).show();

                    float[] tmp2 = reconstructionFilteredBack.projection(test_reconstruction, theta, null, new double[]{0}, 1);

                    for ( int i = 0 ; i < tmp.length ; i++ ){
                        tmpProj[k*imageStack.getWidth()*imageStack.getHeight() + i] = tmp[i] - tmp2[i];
                        error += tmp[i] - tmp2[i];
                    }

                    System.out.println("Error :: "+error);
                    k++;
                }

                RXImage testReconstructionDif = new RXImage(imageStack.getWidth(),imageStack.getHeight(),nproj);
                testReconstructionDif.setData(tmpProj);
                new Thread(()->RXUtils.RXImageToImagePlus(testReconstructionDif).show()).start();
            }
        }
        while (true){continue;}
    }


    @Test
    public void reconstruct(){
        SessionObject.open(Test_constant.pathToSessionObject281);
        new ImageJ();

        ReconstructionController reconstructionController = RXTomoJ.getInstance().getModelController().getReconstructionController();

        ReconstructionFilteredBack reconstructionFilteredBack = (ReconstructionFilteredBack) reconstructionController.getReconstruction(ReconstructionType.FBP, StackType.ABSORPTION);
        ProjectionFilter projectionFilter = reconstructionFilteredBack.getProjectionFilter();

        projectionFilter.setWithWindow(true);
        projectionFilter.setCurrentWindowType(ProjectionFilter.WindowType.HAMMING);
        projectionFilter.setCurrentFilterType(ProjectionFilter.FilterType.RAMP);
        projectionFilter.setWindowDimension(150);

//        reconstructionFilteredBack.setCenterOfRotationX(225.35f);
//        reconstructionFilteredBack.setCenterOfRotationY(0);
//        reconstructionFilteredBack.setCenterOfRotationZ(0);
        Utils.openWaitingLoadBar(reconstructionController.filterdBackProjection(new StackData(StackType.ABSORPTION)),"");

        while (true){
            continue;
        }
    }
}

