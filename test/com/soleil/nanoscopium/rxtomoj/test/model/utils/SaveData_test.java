/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.test.model.utils;

import com.soleil.nanoscopium.rxtomoj.model.utils.SaveData;
import com.soleil.nanoscopium.rxtomoj.test.Test_constant;
import org.junit.Test;

import java.util.ArrayList;

/**
 * @author Antoine Bergamaschi
 */
public class SaveData_test {
    @Test
    public void testSaveRawData(){
        float[] test = {0.00005f,1,2,3};

        SaveData.saveRawData(test,"testSaveRawData", Test_constant.pathTrashTest+"testSaveRawData.txt");
    }

    @Test
    public void testSaveRawData2(){
        double[] test = {0.00005,1,2,3};
        double[] test2 = {3,2,1,0};
        String name = "Test1";
        String name2 = "Test2";

        ArrayList<double[]> testArray = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();

        testArray.add(test);
        testArray.add(test2);

        names.add(name);
        names.add(name2);

        SaveData.saveRawData(testArray,names, Test_constant.pathTrashTest+"testSaveRawData.txt");
    }
}
