/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.test.model.utils;

import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.util.RXUtils;
import com.soleil.nanoscopium.rxtomoj.model.utils.DirectRayTracing;
import ij.ImageJ;
import org.junit.Test;

/**
 * Created by bergamaschi on 27/09/2015.
 */
public class RayTracingTest {

    @Test
    public void testDirectRayTracing(){
        double theta = Math.toRadians(50);
        int widht = 1000,height = 1000;
        float[] volume = new float[widht*height];

        new ImageJ();

        DirectRayTracing directRayTracing = new DirectRayTracing();

        directRayTracing.init(theta, new double[]{500, 500});

        final double cosTheta = Math.cos(theta);
        final double sinTheta = Math.sin(theta);
//        double step = Math.sqrt(cosTheta * cosTheta + sinTheta * sinTheta);
        double step =directRayTracing.getComputedStep()[0];
//        step = Math.max(1/Math.cos(theta),1/Math.sin(theta));

        directRayTracing.setMaximalDistance(step);


        double[] pos = directRayTracing.getCurrentPosition();
        int[] Intpos = directRayTracing.getIntegerPosition();
        double[] stepSize = directRayTracing.getComputedStep();
//        volume[((int) (pos[0] * widht + pos[1] * height))] += 100;
        while (Intpos[0] < widht && pos[0] >= 0 &&
                Intpos[1] < height && pos[1] >= 0 ){
//            volume[(int)Math.round(pos[0]-0.5) + (int) (Math.round(pos[1]-0.5) * widht)] += stepSize[0];
            volume[Intpos[0] + Intpos[1] * widht] += stepSize[0]/step;

            System.out.println("Position x :: " + Intpos[0] + " / " + pos[0] + "\t Position Y =  " + Intpos[1] + " / " + pos[1] + " step :: " + stepSize[0]/step);
            if ( pos[0] == 261  ){
                System.out.println("Position x :: " + Intpos[0] + " / " + pos[0] + "\t Position Y =  " + Intpos[1] + " / " + pos[1] + " step :: " + stepSize[0]);
            }
            directRayTracing.getNextPosition();

        }
//        volume[Intpos[0] + Intpos[1] * widht] += stepSize[0];

        RXImage rxImage = new RXImage(widht,height,1);
        rxImage.setData(volume);

        RXUtils.RXImageToImagePlus(rxImage).show();

        while (true){
            continue;
        }
    }

    @Test
    public void testFullCircle(){
        int widht = 1920,height = 1200;
        float[] volume = new float[widht*height];

        new ImageJ();

        RXImage rxImage = new RXImage(widht, height, 1);
        rxImage.setData(volume);

        for ( int i = 0 ; i < 3600 ; i ++ ) {
            double theta = Math.toRadians(0.1)*i;

            DirectRayTracing directRayTracing = new DirectRayTracing();

            double step = 0;

            step = Math.max(Math.abs(1/Math.cos(theta)),Math.abs(1/Math.sin(theta)));

            directRayTracing.setMaximalDistance(step);
            directRayTracing.init(theta, new double[]{960, 600});


//            double[] pos = directRayTracing.getCurrentPosition();
            double[] stepSize = directRayTracing.getComputedStep();
            int[] Intpos = directRayTracing.getIntegerPosition();


            int[] oldPos = new int[2];
//        volume[((int) (pos[0] * widht + pos[1] * height))] += 100;
            while (Intpos[0] < widht && Intpos[0] >= 0 &&
                    Intpos[1] < height && Intpos[1] >= 0 ){
//            volume[(int)Math.round(pos[0]-0.5) + (int) (Math.round(pos[1]-0.5) * widht)] += stepSize[0];
                volume[Intpos[0] + Intpos[1] * widht] += stepSize[0];

                if ( Intpos[0] == oldPos[0] && Intpos[1] == oldPos[1] ){
                    System.out.println("Position x :: "+oldPos[0]+" Position Y =  "+oldPos[1]+ " step :: "+stepSize[0]/step);
                }

                System.arraycopy(Intpos,0,oldPos,0,2);




//                System.out.println("Position x :: "+Intpos[0]+" / "+pos[0]+"\t Position Y =  "+Intpos[1]+" / "+pos[1] + " step :: "+stepSize[0]);
                directRayTracing.getNextPosition();
            }

        }
        RXUtils.RXImageToImagePlus(rxImage).show();
        while (true){
            continue;
        }
    }
}
