/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.test.model.utils;

import cern.colt.matrix.tfcomplex.impl.DenseFComplexMatrix2D;
import cern.colt.matrix.tfloat.impl.DenseFloatMatrix2D;
import com.soleil.nanoscopium.rximage.IO.RXImageIO_IJ;
import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.exception.RXImageException;
import com.soleil.nanoscopium.rximage.util.RXUtils;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import ij.ImageJ;
import com.soleil.nanoscopium.rxtomoj.test.Test_constant;
import org.junit.Test;

import java.nio.file.Paths;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Created by bergamaschi on 01/08/2014.
 */
public class ComputationUtils_test {

    @Test
    public void testBilinearInterpolation(){
        float[] pixels = {2,0,4,
                          2,1,5,
                          0,1,6,
                //new Z
                        1,0,2,
                        4,3,9,
                        0,6,8};
        int width = 3;
        int height = 3;
        int size = 2;
        int position = 0;
        float x = 1f,y = 0f, z = 0.0f;
        assertEquals("Bilinear interpolation Test", 0.0f, ComputationUtils.getBilinearInterpolationValue(x, y, width, pixels), 0.001);
        x=0.5f;y=0f;
        assertEquals("Bilinear interpolation Test", 1f, ComputationUtils.getBilinearInterpolationValue(x, y, width, pixels), 0.001);
        x=1f;y=0.5f;
        assertEquals("Bilinear interpolation Test", 0.5f, ComputationUtils.getBilinearInterpolationValue(x, y, width, pixels), 0.001);
        x=1f;y=1f;
        assertEquals("Bilinear interpolation Test", 1f, ComputationUtils.getBilinearInterpolationValue(x, y, width, pixels), 0.001);
        x=0.5f;y=0.5f;
        assertEquals("Bilinear interpolation Test", 1.25f, ComputationUtils.getBilinearInterpolationValue(x, y, position ,height,width, pixels), 0.001);
        x=0.3f;y=2f;
        assertEquals("Bilinear interpolation Test", 0.3f, ComputationUtils.getBilinearInterpolationValue(x, y, position ,height,width, pixels), 0.001);
        x=0.3f;y=1f;
        assertEquals("Bilinear interpolation Test", 1.7f, ComputationUtils.getBilinearInterpolationValue(x, y, position ,height,width, pixels), 0.001);


        //Change position Function position
        position = width*height;
        x=1f;y=1f;
        assertEquals("Bilinear interpolation Test", 3f, ComputationUtils.getBilinearInterpolationValue(x, y, position ,height,width, pixels), 0.001);
        x=0.5f;y=0.5f;
        assertEquals("Bilinear interpolation Test", 2f, ComputationUtils.getBilinearInterpolationValue(x, y, position ,height,width, pixels), 0.001);
        x=1.5f;y=1.5f;
        assertEquals("Bilinear interpolation Test", 6.5f, ComputationUtils.getBilinearInterpolationValue(x, y, position ,height,width, pixels), 0.001);
        x=2f;y=0.5f;
        assertEquals("Bilinear interpolation Test", 5.5f, ComputationUtils.getBilinearInterpolationValue(x, y, position ,height,width, pixels), 0.001);

        //Test BilinearInterpolation Z
        position = width*height;
        x=1f;y=1f;z=0f;
        assertEquals("Bilinear interpolation Test", 1f, ComputationUtils.getBilinearInterpolationValueZ(x, z, (int) (y*width), width, height, size, pixels), 0.001);
        x=1f;y=1f;z=1f;
        assertEquals("Bilinear interpolation Test", 3f, ComputationUtils.getBilinearInterpolationValueZ(x, z, (int) (y*width), width, height, size, pixels), 0.001);
        x=0.5f;y=1f;z=0.5f;
        assertEquals("Bilinear interpolation Test", 2.5f, ComputationUtils.getBilinearInterpolationValueZ(x, z, (int) (y*width), width, height, size, pixels), 0.001);
        x=1.5f;y=2f;z=0.5f;
        assertEquals("Bilinear interpolation Test", 5.25f, ComputationUtils.getBilinearInterpolationValueZ(x, z, (int) (y*width), width, height, size, pixels), 0.001);
        x=0f;y=0f;z=0.5f;
        assertEquals("Bilinear interpolation Test", 1.5f, ComputationUtils.getBilinearInterpolationValueZ(x, z, (int) (y*width), width, height, size, pixels), 0.001);

    }

    @Test
    public void computeOrdinateToOrigin(){
        double coefDirectorLine = 1;
        double[] point = new double[]{1,1};

        assertEquals("Incorrect ordinate To origin", 0, ComputationUtils.computeOrdinateToOrigin(coefDirectorLine, point), 0.00001);

        point = new double[]{200,200};
        assertEquals("Incorrect ordinate To origin", 0, ComputationUtils.computeOrdinateToOrigin(coefDirectorLine, point), 0.00001);

        coefDirectorLine = -0.5;
        point = new double[]{2,0};
        assertEquals("Incorrect ordinate To origin", 1, ComputationUtils.computeOrdinateToOrigin(coefDirectorLine, point), 0.00001);
    }

    @Test
    public void testInterceptionLines(){
        double coefDirectorLine1 = 1;
        double ordinateToOriginLine1 = 0;

        double coefDirectorLine2 = 0;
        double ordinateToOriginLine2 = 1;

        double[] trueValue = new double[]{1,1};

        assertArrayEquals("Incorrect position T1", trueValue, ComputationUtils.computeIntersectionLine(coefDirectorLine1, ordinateToOriginLine1, coefDirectorLine2, ordinateToOriginLine2), 0.00001);


//        coefDirectorLine2 = 0;
//        ordinateToOriginLine2 = 1;
        coefDirectorLine1 = -1;
        trueValue = new double[]{-1,1};

        assertArrayEquals("Incorrect position T2", trueValue, ComputationUtils.computeIntersectionLine(coefDirectorLine1, ordinateToOriginLine1, coefDirectorLine2, ordinateToOriginLine2), 0.00001);


    }

    @Test
    public void testInterceptionLinesV(){
        double coefDirectorLine1 = 1;
        double ordinateToOriginLine1 = 0;

        double coefDirectorLine2 = 1;

        double[] trueValue = new double[]{1,1};

        assertArrayEquals("Incorrect position T1", trueValue, ComputationUtils.computeIntersectionLineV(coefDirectorLine1, ordinateToOriginLine1, coefDirectorLine2), 0.00001);

        coefDirectorLine2 = -1;
        trueValue = new double[]{-1,-1};
        assertArrayEquals("Incorrect position T2", trueValue, ComputationUtils.computeIntersectionLineV(coefDirectorLine1, ordinateToOriginLine1, coefDirectorLine2), 0.00001);

        coefDirectorLine1 = 3;
        coefDirectorLine2 = 1;
        trueValue = new double[]{1,0.33};
        assertArrayEquals("Incorrect position T3", trueValue, ComputationUtils.computeIntersectionLineV(coefDirectorLine1, ordinateToOriginLine1, coefDirectorLine2), 0.01);


        coefDirectorLine2 = -1;
        trueValue = new double[]{-1,-0.33};
        assertArrayEquals("Incorrect position T3", trueValue, ComputationUtils.computeIntersectionLineV(coefDirectorLine1, ordinateToOriginLine1, coefDirectorLine2), 0.01);

    }

    @Test
    public void testInterpolationPerf2(){
        System.out.println("Test Interpolation Perf 2 ");
        float[] pixels = {0,0,0,0};
        float x = 0.0f,y = -0.01f;
        int width = 2;
        float valueToSpread = 1.0f;
        ComputationUtils.setBilinearInterpolationValue(x, y, width, valueToSpread, pixels);

        System.out.println(pixels[0] + "," + pixels[1]+","+pixels[2]+","+pixels[3]);
        if ( pixels[0] == 0.25 && pixels[1] == 0.25 && pixels[2] == 0.25 && pixels[3] == 0.25){
            System.out.println("Result interpolation OK");
        }else{
            System.out.println("Result interpolation FALSE");
        }
    }



    @Test
    public void testCenterFFT(){
        System.out.println("testCenterFFT");
        new ImageJ();

        try{

            RXImageIO_IJ rxImageIO_ij = new RXImageIO_IJ();
            RXImage imageStack = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.path +"FFT_test.tif"),false);

            ComputationUtils.centerFFT(imageStack.copySlice(0),imageStack.getHeight(),imageStack.getWidth());

            DenseFComplexMatrix2D floatMatrix2D = new DenseFComplexMatrix2D(imageStack.getHeight(),imageStack.getWidth());
            floatMatrix2D.assignReal(new DenseFloatMatrix2D(imageStack.getHeight(),imageStack.getWidth()).assign(imageStack.copySlice(0)));
            floatMatrix2D.fft2();


            floatMatrix2D.ifft2(true);


            float[] real = (float[]) floatMatrix2D.getRealPart().elements();

            ComputationUtils.centerFFT(real, floatMatrix2D.columns(), floatMatrix2D.rows());

            RXImage result_stack = new RXImage(floatMatrix2D.columns(),floatMatrix2D.rows(),1);
            result_stack.setSlice(real, 0);

            RXUtils.RXImageToImagePlus(result_stack).show();
        }catch (RXImageException e){
            e.printStackTrace();
        }

        while (true){
            continue;
        }
    }

    @Test
    public void testReslice(){
        new ImageJ();

        RXImageIO_IJ rxImageIO_ij = new RXImageIO_IJ();
        RXImage test = null;
        try {
            test = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathReconstruction + "proj_test_2_01.tif"),false);
        } catch (RXImageException e) {
            e.printStackTrace();
        }

//        RXImage test = new ImagePlus(Test_constant.pathReconstruction +"proj_test_2_01.tif").getRXImage();
        RXImage result = ComputationUtils.convertDimension(test, ComputationUtils.OrientationType.XYZ, ComputationUtils.OrientationType.ZYX);

        RXUtils.RXImageToImagePlus(result).show();
        while (true){
            continue;
        }

    }


    @Test
    public void testPaddingData(){
        float[] dataToPad = {1.0f,1.0f,1.0f,1.0f};
        float[] result = ComputationUtils.padDataWithZero(dataToPad, 8, 8, 2, 2);

        while (true){
            continue;
        }
    }

    @Test
    public void testRotation3D(){

        RXImageIO_IJ rxImageIO_ij = new RXImageIO_IJ();
        RXImage test = null;
        try {
            test = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathReconstruction + "test3D_2.tif"),false);
        } catch (RXImageException e) {
            e.printStackTrace();
        }

        float[] init = test.copyData();
        int w = test.getWidth();
        int h = test.getHeight();
        int s = test.getSize();
        float[] rot = new float[w*h*s];

        ComputationUtils.computeRotation3D(init, rot, w, h, s, w, h, s, w / 2, h / 2, s / 2, new double[]{Math.PI, 0, 0});

        test.setData(rot);

        RXUtils.RXImageToImagePlus(test).show();
        while (true){
            continue;
        }

    }

    @Test
    public void testRotation2D(){
        RXImageIO_IJ rxImageIO_ij = new RXImageIO_IJ();
        RXImage test = null;
        try {
            test = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathReconstruction + "test2D.tif"),false);
        } catch (RXImageException e) {

        }

//        RXImage test = new ImagePlus(Test_constant.pathReconstruction +"test2D.tif").getRXImage();
        float[] init = test.copyData();
        int w = test.getWidth();
        int h = test.getHeight();
        int s = test.getSize();
        float[] rot = new float[w*h*s];

        ComputationUtils.computeRotation2D(init, rot, w, h, w, h, w / 2, h / 2, new double[]{Math.PI, 0, 0});

//        RXImage result = ComputationUtils.createStackFromArray(rot, w, h, s);
        test.setData(rot);

        RXUtils.RXImageToImagePlus(test).show();
//        new ImagePlus("test",result).show();
        while (true){
            continue;
        }

    }


    @Test
    public void getCenterOfMass(){
//        RXImageIO_IJ rxImageIO_ij = new RXImageIO_IJ();
//        RXImage imageStack = (RXImage) rxImageIO_ij.load(Paths.get("C:\\Users\\antoine bergamaschi\\Documents\\testGwendoline\\ref_C05_1.tif"),false);
//
//        float[] xCenter = new float[imageStack.getSize()];
//        float[] yCenter = new float[imageStack.getSize()];
//        float[] x = new float[imageStack.getSize()];
//        for ( int i =0 ; i < imageStack.getSize() ;  i++  ){
//            FloatProcessor imageProcessor  = (FloatProcessor) imageStack.getProcessor(i+1);
//            x[i] = i;
//            ImageStatistics imageStatistics  =  imageProcessor.getStatistics();
//            xCenter[i] = (float) imageStatistics.xCenterOfMass;
//            yCenter[i] = (float) imageStatistics.yCenterOfMass;
//        }
//
//
//        Plot plot = new Plot("Test Center Of mass","X","Center");
//        plot.setLimits(0,imageStack.getSize(),283,295);
//        plot.setColor(Color.blue);
//        plot.addPoints(x, xCenter, Plot.LINE);
////        plot.draw();
//        plot.setColor(Color.red);
//        plot.addPoints(x, yCenter, Plot.LINE);
////        plot.draw();
//
//        plot.show();
//        while (true){
//            continue;
//        }
    }
}
