/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.test.model;

import com.soleil.nanoscopium.rximage.IO.RXImageIO_IJ;
import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.exception.RXImageException;
import com.soleil.nanoscopium.rximage.util.RXUtils;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.ComputationController;
import com.soleil.nanoscopium.rxtomoj.controller.DataController;
import com.soleil.nanoscopium.rxtomoj.controller.SessionController;
import com.soleil.nanoscopium.rxtomoj.gui.RXTomoComput.MenuLoadSession;
import com.soleil.nanoscopium.rxtomoj.model.ShiftImage;
import com.soleil.nanoscopium.rxtomoj.model.computationFunction.BasicComputationFunction;
import com.soleil.nanoscopium.rxtomoj.model.utils.BasicKernel;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;
import com.soleil.nanoscopium.rxtomoj.test.testClassType.Hdf5_281_test;
import ij.ImageJ;
import com.soleil.nanoscopium.rxtomoj.test.Test_constant;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Created by bergamaschi on 01/08/2014.
 */
public class ShiftImage_test {

    private ShiftImage shiftImage;

    @Before
    public void testInit(){
        new ImageJ();
        System.out.println("testInit");
        shiftImage = new ShiftImage();
        //Add listener to MMXI
        RXTomoJ.getInstance().getModelController().addMMXIControllerUpdatedListener(l->{
            System.out.println(l.toString());
        });

    }

    @Test
    public void testMirroring(){
//        new ImageJ();
//        ShiftImage shiftImage = new ShiftImage();
//        ImagePlus imagePlus = new ImagePlus(Test_constant.pathToIMG23);
//
//        RXImage newImage = shiftImage.mirroring(imagePlus.getRXImage(),0,1,2,3);
////        RXImage newImage = shiftImage.mirroring(imagePlus.getRXImage(),5,2,7,0);
//        ImagePlus result = new ImagePlus("test",newImage);
//
//        result.show();

        ArrayList<StackType> test = new ArrayList<>();
        test.add(StackType.ABSORPTION);
        test.add(StackType.ABSORPTION);
        StackType t = StackType.ABSORPTION;
        t.setAdditionalName("tset");
        test.add(t);

        System.out.println((Math.sin(Math.pow(10, -6)*50*Math.pow(10,-3))*Math.pow(10,9)));
        System.out.println(Math.ceil(0.3));

        while (true){
            continue;
        }
    }

    @Test
    public void testCropping(){
//        new ImageJ();

        RXImageIO_IJ rxImageIO_ij = new RXImageIO_IJ();
        RXImage image = null;
        try {
            image = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.path156 + "X.tif"), false);
        } catch (RXImageException e) {
            e.printStackTrace();
        }

//        ImagePlus imagePlus = new ImagePlus(Test_constant.path156+"X.tif");
        RXImage result = shiftImage.changeMirroring(image, ShiftImage.MirroringType.MDI, ShiftImage.MirroringType.ASDI_X);

//        new ImagePlus("test",result).show();

        RXUtils.RXImageToImagePlus(result).show();
        while (true){
            continue;
        }
    }

    @Test
    public void testCorrelation(){
        System.out.println("testCorrelation");
        //Open Hdf5
//        this.hdf5Handler = new Hdf5Handler();
//        this.hdf5Handler.hdf5_open(filename6);
//
//        Object[][] datasets = this.hdf5Handler.getDatasetWithDimensionInformation();
//        this.hdf5Handler.hdf5_close();

        //Get Spect
//        Hdf5VirtualStack hdf5VirtualStack = new Hdf5VirtualStack(this.filename6,(String)datasets[0][30],true);
//        Hdf5VirtualStack hdf5VirtualStack2 = new Hdf5VirtualStack(this.filename6,(String)datasets[0][31],true);

        //Initialize mean
//        hdf5VirtualStack.initializeSpectrum();
//        hdf5VirtualStack2.initializeSpectrum();


        RXImageIO_IJ rxImageIO_ij = new RXImageIO_IJ();
        RXImage image1 = null;
        RXImage image2 = null;
        try {
            image1 = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathShiftSpot + "Spot_Map_1-1.tif"), false);
            image2 = (RXImage) rxImageIO_ij.load(Paths.get(Test_constant.pathShiftSpot + "Spot_Map_1-2.tif"), false);
        } catch (RXImageException e) {
            e.printStackTrace();
        }

        RXUtils.RXImageToImagePlus(image2).show();

        float[] data1 = image1.copyData();
        float[] data2 = image2.copyData();

        ShiftImage.correlationFFT2D(data1,data2,image1.getWidth(), image1.getHeight(),1);
//        shiftImage.displayFittedPlot(false);

        image2.setData(data2);

        RXUtils.RXImageToImagePlus(image1).show();
        RXUtils.RXImageToImagePlus(image2).show();

        //retrieve Sum Spectra
//        float[] sum1  = hdf5VirtualStack.getMeanSpectrum();
//        float[] sum2 = hdf5VirtualStack2.getMeanSpectrum();

//        float[] x = new float[sum1.length];
//        for ( int i = 0 ; i < x.length ; i++){
//            x[i] = i;
//        }

//        int shift = 16;
//        float[] sumShift = new float[sum2.length];
        //In the case - shift
//        System.arraycopy(sum2,shift,sumShift,0,sumShift.length-shift);

        //In the case + shift
//        System.arraycopy(sum2,0,sumShift,shift,sumShift.length-shift);

//        Plot plot = new Plot("test","x","y");
////
//        plot.setLineWidth(2);
//        plot.setLimits(0,x.length,0,0.05);
//        plot.setColor(Color.red);
//        plot.addPoints(x,sum1,Plot.LINE);
//        plot.setColor(Color.blue);
//        plot.addPoints(x,sumShift,Plot.LINE);
//        plot.setColor(Color.green);
//        plot.addPoints(x,sum2,Plot.LINE);
//        plot.show();

        while (true){
            continue;
        }

//        ShiftImage shiftImage = new ShiftImage();
//        shiftImage.correlationFFT1D(sum1,sum2);
    }
    
    @Test
    public void testFitSinusogram(){
        System.out.println("testFitSinusogram");

        SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
        ComputationController computationController  =  RXTomoJ.getInstance().getModelController().getComputationController();

        sessionController.openSessionObject(Test_constant.pathToSessionObject281);


        dataController.setMinimumShiftValue(0);
        dataController.setWobblingIterationCorrectionNumber(10);
        dataController.useWobblingInterpolation(true);

        dataController.setWobblingWindowParameter(16);
        dataController.useWobblingWindowParameter(true);
//        dataController.updateFittingUtils(16, true);
//        dataController.updateFitParameter(0, true, 10);


        RXImage shifted = dataController.getDefaultRXImage();
        computationController.computeBackgroundNormalization(shifted);
        ComputationUtils.setAbsoptionCoef(shifted);
        float[] copy = shifted.copyData();
        shifted.computeData(l->{
            BasicKernel mean = BasicKernel.MEAN;
            mean.setKernel(new float[]{1,1,1,1,1,
                                       1,1,1,1,1,
                                       1,1,1,1,1,
                                       1,1,1,1,1,
                                       1,1,1,1,1},5,5,1);
            for ( int z = 0; z < shifted.getSize() ; z++ ) {
                for (int y = 0; y < shifted.getHeight(); y++) {
                    for (int x = 0; x < shifted.getWidth(); x++) {
                        l[x+y*shifted.getWidth()+z*shifted.getHeight()*shifted.getWidth()] = mean.compute(x,y,z,copy,shifted.getWidth(),shifted.getHeight(),shifted.getSize());
                    }
                }
            }
        });


        RXImage seconde = dataController.getDefaultRXImage();
        computationController.computeBackgroundNormalization(seconde);
        ComputationUtils.setAbsoptionCoef(seconde);


        computationController.shiftImageWithUpdate(seconde, shifted);

        //TODO add detection of completion function then display resulting ShiftedImage
//        RXUtils.RXImageToImagePlus(unshifted).show();
        RXUtils.RXImageToImagePlus(shifted).show();

        while (true){
            continue;
        }

    }

    @Test
    public void testMobileMean(){
        System.out.println("testMobileMean");


        SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
        ComputationController computationController  =  RXTomoJ.getInstance().getModelController().getComputationController();

//        new ImageJ();
        sessionController.openSessionObject(Test_constant.pathToSessionObject281);

//        RXTomoJModelController rxTomoJModelController = RXTomoJ.getInstance().getModel();
        RXImage shifted = dataController.getComputedStack(StackType.ABSORPTION);
        computationController.computeBackgroundNormalization(shifted);
        ComputationUtils.setAbsoptionCoef(shifted);
//        dataController.getShiftImage().updateValues(shifted);
//        dataController.getShiftImage().getFittingUtils().fitCurveOnSine( dataController.getShiftImage().getRealValueX(), dataController.getShiftImage().getRealValueY());
//        RXImage unshifted = dataController.getShiftImage().shiftImage(shifted);
//        RXImage unshifted = rxTomoJModelController.shiftImageWithUpdate(shifted, rxTomoJModelController.getStorageData(RXTomoJModelController.StackType.ABSORPTION));

//        dataController.getShiftImage().displayFittedPlot(true);
//        new ImagePlus("",unshifted).show();
//        RXUtils.RXImageToImagePlus(unshifted).show();

        while (true){
            continue;
        }

    }


    @Category(Hdf5_281_test.class)
    @Test
    public void testRegriding(){
//        System.out.println("testRegriding");
        SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
        sessionController.openSessionObject(Test_constant.pathToSessionObject281);
        ComputationController computationController = RXTomoJ.getInstance().getModelController().getComputationController();

//        dataController.setNearestNeighbourDistance(new int[]{10,0});
        dataController.setRegriddingInterpolationDistance(10);
        dataController.setRegriddingInterpolationDistance_fluorescence(0);

        RXImage shifted = dataController.getComputedStack(StackType.ABSORPTION_REF);
        //Motor Position Have to be extracted
        RXImage regrid = computationController.regrid(shifted,StackType.ABSORPTION);

        //Remove show option when automatic computation
        RXUtils.RXImageToImagePlus(regrid).show();

        while (true){
            continue;
        }
    }

    @Category(Hdf5_281_test.class)
    @Test
    public void testRegridingWithSaving(){
        SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
        sessionController.openSessionObject(Test_constant.pathToSessionObject281);
        ComputationController computationController = RXTomoJ.getInstance().getModelController().getComputationController();

//        RXImage shifted = (RXImage) dataController.getStorageData(StackType.ABSORPTION_REF);
//        //Motor Position Have to be extracted
//        RXImage regrid = computationController.regrid(shifted,StackType.ABSORPTION);
//

        RXImage[] motors = BasicComputationFunction.initMotors(sessionController.getSessionObject(), StackType.ABSORPTION);
        RXImage motors1 = ShiftImage.initRegrid(motors);
        RXUtils.RXImageToImagePlus(motors1).show();


        int[][] bounds = ShiftImage.getCorrectedReconstructionROI(new int[]{10,100,0,1,0,1},motors1);

        System.out.println("Bounds ");
        for ( int i = 0 ; i < bounds.length ; i++){
            System.out.println("Bounds "+i+" = "+bounds[i]);
        }

        while (true){
            continue;
        }

    }

    @Category(Hdf5_281_test.class)
    @Test
    public void sinusogram(){
//        System.out.println("testShifting");
//        new ImageJ();

        SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();
        ComputationController computationController  = RXTomoJ.getInstance().getModelController().getComputationController();
        sessionController.openSessionObject(Test_constant.pathToSessionObject281);

//        RXTomoJModelController rxTomoJModelController = RXTomoJ.getInstance().getModel();
        computationController.shiftImageWithUpdate(dataController.getComputedStack(StackType.ABSORPTION), dataController.getComputedStack(StackType.ABSORPTION));
//        dataController.getShiftImage().displayFittedPlot(true);
        //TODO add detection of completion function then display resulting ShiftedImage

//        new ImagePlus("",unshifted).show();
//        RXUtils.RXImageToImagePlus(unshifted).show();
//        while (true){
//            continue;
//        }
    }
}
