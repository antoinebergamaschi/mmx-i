/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.test.model;

import com.soleil.nanoscopium.hdf5Opener.Hdf5Handler;
import com.soleil.nanoscopium.rxtomoj.model.SessionObject;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : antoine bergamaschi
 *         Date: 18/11/13
 *         Time: 22:16
 */
public class imageJtest {


    private Hdf5Handler hdf5Handler;


    @Test
    public void testOpenningGwendoline(){
//        ImagePlus imagePlus  =  new ImagePlus("C:\\Users\\antoine bergamaschi\\Documents\\testGwendoline\\ref_C05_1.tif");
//
//        new ImageJ();
//
        Math.sin(0.0000001);
//        ShiftImage shiftImage = new ShiftImage();
//        shiftImage.correlationFFT2D(imagePlus.getStack());
//
//        shiftImage.displayFittedPlot(false);
//
//
//        imagePlus.show();
        while (true){
            continue;
        }

    }



    @Test
    public void launchPlugIn(){
        System.out.println("launchPlugIn");
//        Hdf5PlugIn hdf5PlugIn = new Hdf5PlugIn();
//        hdf5PlugIn.run("test");
//        String[][] allGroupAndDataset = hdf5PlugIn.getHierarchy();
//        Object[][] datasets = hdf5PlugIn.getDatasets();
//
//        //Open the 5D dataset
//        hdf5PlugIn.computeOnDataset((String) datasets[0][63],new ComputePhaseContrast());

        //Ouverture normal
//        Hdf5PlugIn hdf5PlugIn = new Hdf5PlugIn();
//        hdf5PlugIn.run(null);
//        Object[][] datasets = hdf5PlugIn.getDatasets();
//
//        //Do not forget to close the file
//        hdf5PlugIn.closeHdf5File();

//        RXTomoJ rxTomoJ = new RXTomoJ();
//        rxTomoJ.run(null);
//        new RXTomoJ().run(null);
    }





    @Test
    public void testSessionObjectSaver(){
        System.out.println("testSessionObjectSaver");
        ArrayList<String> object =  new ArrayList<String>();
        object.add("1");
        object.add("2");
        object.add("3");
//        Writter
        SessionObject sessionObject  = SessionObject.getCurrentSessionObject();
        sessionObject.setPathToHdf5("testPath");
        sessionObject.setPathToSavingFolder("testPathToSavingFolder");
//        sessionObject.getAnalyseFactory().getCurrent().getParameterStorage.(new ComputeSpot());
//        sessionObject.setImageID(object);
//        sessionObject.writteXML(pathToSessionObject);
        SessionObject.save();
//        Loader
//        SessionObject.open(this.pathToSessionObject);
//        SessionObject sessionObject = SessionObject.getCurrentSessionObject();

    }








}
