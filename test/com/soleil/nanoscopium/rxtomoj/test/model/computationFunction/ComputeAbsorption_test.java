/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.test.model.computationFunction;

import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.ComputationController;
import com.soleil.nanoscopium.rxtomoj.controller.DataController;
import com.soleil.nanoscopium.rxtomoj.controller.SessionController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;
import com.soleil.nanoscopium.rxtomoj.test.Test_constant;
import org.junit.Test;

/**
 * Created by bergamaschi on 19/11/2014.
 */
public class ComputeAbsorption_test {
    @Test
    public void computingTestAbsorbtion() throws InterruptedException {
        System.out.println("computingTestAbsorbtion");

        SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
        DataController dataController =  RXTomoJ.getInstance().getModelController().getDataController();
        ComputationController computationController = RXTomoJ.getInstance().getModelController().getComputationController();

        sessionController.openSessionObject(Test_constant.pathToSessionObject203);

//        dataController.setSamplingOption(1,1);

        Utils.openWaitingLoadBar(computationController.imageCreation(StackType.ABSORPTION),"");

        while(true){
            Thread.sleep(100);
            continue;
        }
    }
}
