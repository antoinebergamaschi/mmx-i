/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.test.model.computationFunction;

import com.soleil.nanoscopium.hdf5Opener.Hdf5Handler;
import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.RXSpectrum;
import com.soleil.nanoscopium.rximage.util.RXUtils;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.ComputationController;
import com.soleil.nanoscopium.rxtomoj.controller.DataController;
import com.soleil.nanoscopium.rxtomoj.controller.SessionController;
import com.soleil.nanoscopium.rxtomoj.event.MMXIControllerEvent;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.SessionObject;
import com.soleil.nanoscopium.rxtomoj.model.computationFunction.ComputeSpectrum;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import com.soleil.nanoscopium.rxtomoj.test.Test_constant;
import ij.ImageJ;
import ij.measure.ResultsTable;
import org.junit.Test;

import java.util.HashMap;


/**
 * Created by antoine bergamaschi on 27/08/2014.
 */
public class ComputeSpectrum_test {


    @Test
    public void computeTestSpectrum() throws InterruptedException {
        System.out.println("computeTestSpectrum");
        SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
        DataController dataController =  RXTomoJ.getInstance().getModelController().getDataController();
        ComputationController computationController = RXTomoJ.getInstance().getModelController().getComputationController();
        sessionController.openSessionObject(Test_constant.pathToSessionObject182);

//        dataController.setSamplingOption(1,1);


        Utils.openWaitingLoadBar(computationController.imageCreation(ComputationUtils.StackType.FLUORESCENCE), "");


        while(true){
            Thread.sleep(100);
            continue;
        }
    }

    @Test
    public void computeTestSumSpectrum() throws InterruptedException {
        System.out.println("computeTestSpectrum");
        SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
        DataController dataController =  RXTomoJ.getInstance().getModelController().getDataController();
        ComputationController computationController = RXTomoJ.getInstance().getModelController().getComputationController();
        sessionController.openSessionObject(Test_constant.pathToSessionObject143);

//        dataController.setSamplingOption(1,1);

        int[][] roi = new int[3][2];
        roi[0][0] = 0;
        roi[0][1] = 867;
        roi[1][0] = 0;
        roi[1][1] = 494;
        roi[2][0] = 0;
        roi[2][1] = 1;

        Utils.openWaitingLoadBar(computationController.imageCreation(ComputationUtils.StackType.SUM_SPECTRA), "");

        RXTomoJ.getInstance().getModelController().addMMXIControllerUpdatedListener(l->{
            if ( l.getEventID() == MMXIControllerEvent.MMXIControllerEventID.PROGRESS_FUNCTION ){
                if ( l.getEventType() == MMXIControllerEvent.MMXIControllerEventType.FINISHED ){
                    if ( l.getEventData().getIdentifier() == ComputationController.COMPUTE_SUMSPECTRA ){
                        HashMap<String,RXSpectrum> spectrums = (HashMap<String,RXSpectrum>) l.getEventData().getObject();
                        ResultsTable resultsTable = new ResultsTable();
                        for ( String s : spectrums.keySet() ){
                            float[] sum1  = spectrums.get(s).copyData();
                            for ( int k = 0 ; k < sum1.length ; k++) {
                                resultsTable.incrementCounter();
                                resultsTable.addValue(s, sum1[k]);
                            }
                        }
                        resultsTable.show("test");
                    }
                }
            }
        });

        while(true){
            Thread.sleep(100);
            continue;
        }
    }

    @Test
    public void test(){
        new ImageJ();
        SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
        ComputationController computationController = RXTomoJ.getInstance().getModelController().getComputationController();
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

        sessionController.openSessionObject(Test_constant.pathToSessionObject63);
//        Thread thread4 = new Thread(() -> {
            ComputeSpectrum computeSpectrum = new ComputeSpectrum();
//            computeSpectrum.setComputeSpot(dataController.getComputeSpot(ComputationUtils.StackType.PHASECONTRAST));
//            this.setWaitingLoadBar(computeSpectrum);

            SessionObject sessionObject = SessionObject.getCurrentSessionObject();

            Hdf5Handler hdf5Handler = new Hdf5Handler(sessionObject.getPathToHdf5());
            System.err.println("Warning :: Only the first Fluorescence channel is computed");
//            hdf5Handler.readDataset(sessionObject.getAnalyseFactory().getCurrent().get().get(1), computeSpectrum);

            //Display result
//            ImagePlus imagePlus = new ImagePlus("Fluorescence", computeSpectrum.getElementImages());
//            imagePlus.show();

            int index = 0;
            for (RXImage imageStack : computeSpectrum.getElementImages()) {
                //Save the compute absorbtion image
                RXUtils.RXImageToImagePlus(imageStack).show();
//                ImagePlus imagePlus2 = new ImagePlus("Test" + index, imageStack);
//                imagePlus2.show();
                //                        FileSaver fs1 = new FileSaver(imagePlus2);
                //                        fs1.saveAsTiff();
                index++;
            }

            //                    sessionObject.getAnalyseFactory().createNewAnalyses();
//            this.removeWaitingLaodBar();
            System.err.println("Warning :: Fluorescence are not stored in the RXTomoJController  ! " + this.getClass().toString());
            System.err.println("Warning :: Fluorescence do not create a new Analyses  ! " + this.getClass().toString());
//        });
//        thread4.start();
        while(true){
            continue;
        }
    }


    @Test
    public void testRXComputation(){
        new ImageJ();
        SessionController sessionController =  RXTomoJ.getInstance().getModelController().getSessionController();
        ComputationController computationController = RXTomoJ.getInstance().getModelController().getComputationController();

        sessionController.openSessionObject(Test_constant.pathToSessionObject63);
        computationController.imageCreation(ComputationUtils.StackType.FLUORESCENCE);
        while(true){
            continue;
        }
    }


    @Test
    public void retrieveFluoInfo(){
        new ImageJ();
        SessionController sessionController =  RXTomoJ.getInstance().getModelController().getSessionController();
        DataController dataController = RXTomoJ.getInstance().getModelController().getDataController();

        sessionController.openSessionObject(Test_constant.pathToSessionObject63);
        for ( RXImage imageStack : dataController.getFluorescenceStack()){
            RXUtils.RXImageToImagePlus(imageStack).show();
        }
        while(true){
            continue;
        }
    }

    @Test
    public void computingTestSpectrum(){
//        System.out.println("computingTestSpectrum");
//        ComputeSpectrum computeSpectrum = new ComputeSpectrum();
//
//        ArrayList<FluorescenceElement> elements = new ArrayList<>();
//        FluorescenceElement element = new FluorescenceElement("Test 0");
//        float[] roi = {1048.0f,1079.0f};
//        element.setRoiPosition(roi);
//        elements.add(element);
//
//        FluorescenceElement element2 = new FluorescenceElement("Test 1");
//        float[] roi2 = {1062.0f,1092.0f};
//        element2.setRoiPosition(roi2);
//        elements.add(element2);
//
//        computeSpectrum.setElements(elements);
//
//        //Reading in the HDf5 File
//        Hdf5Handler hdf5Handler = new Hdf5Handler();
//        hdf5Handler.hdf5_open(filename5);
//        Object[][] datasets = hdf5Handler.getDatasetWithDimensionInformation();
//
//        hdf5Handler.readDataset((String) datasets[0][26],computeSpectrum);
//        hdf5Handler.hdf5_close();
//
//        //Save the compute absorbtion image
//        ImagePlus imagePlusAbsorption = new ImagePlus("Fluorescence", computeSpectrum.getStack());
//        FileSaver fs = new FileSaver(imagePlusAbsorption);
//        fs.saveAsTiff();
//        int index = 0;
//        for ( RXImage imageStack : computeSpectrum.getElementImages()){
//            //Save the compute absorbtion image
//            ImagePlus imagePlus = new ImagePlus("Test"+index, computeSpectrum.getElementImages().get(index));
//            FileSaver fs1 = new FileSaver(imagePlus);
//            fs1.saveAsTiff();
//            index++;
//        }
    }


//    @Test
//    public void saveSumSpectra(){
//        //Open Hdf5
//        Hdf5Handler hdf5Handler = new Hdf5Handler();
//        hdf5Handler.hdf5_open(filename6);
//
//        Object[][] datasets = hdf5Handler.getDatasetWithDimensionInformation();
//        hdf5Handler.hdf5_close();
//
//        //Get Spect
//        Hdf5VirtualStack hdf5VirtualStack = new Hdf5VirtualStack(this.filename6,(String)datasets[0][30],true);
//        Hdf5VirtualStack hdf5VirtualStack2 = new Hdf5VirtualStack(this.filename6,(String)datasets[0][31],true);
//
//        //Initialize mean
//        hdf5VirtualStack.initializeSpectrum();
//        hdf5VirtualStack2.initializeSpectrum();
//
//        //retrieve Sum Spectra
//        float[] sum1  = hdf5VirtualStack.getMeanSpectrum();
//        float[] sum2 = hdf5VirtualStack2.getMeanSpectrum();
//        //Save with imageJ
////        FileSaver fileSaver = new FileSaver()
//        ResultsTable resultsTable = new ResultsTable();
//        for ( int k = 0 ; k < sum1.length ; k++) {
//            resultsTable.incrementCounter();
//            resultsTable.addValue("Sum1", sum1[k]);
//            resultsTable.addValue("Sum2", sum2[k]);
//        }
//        resultsTable.show("test");
//        while (true){
//            continue;
//        }
//    }


//    @Test
//    public void testSpectrumShow(){
//        System.out.println("Test Spectrum Show");
//
//        Hdf5Handler hdf5Handler = new Hdf5Handler();
//        hdf5Handler.hdf5_open(filename2);
////        this.hdf5Handler.hdf5_open(filename2);
//        Object[][] datasets = hdf5Handler.getDatasetWithDimensionInformation();
//        hdf5Handler.hdf5_close();
//
//        Hdf5VirtualStack hdf5VirtualStack = new Hdf5VirtualStack(this.filename2,(String)datasets[0][26],true);
////        hdf5VirtualStack.setType(Hdf5VirtualStack.spectrum3D);
//        hdf5VirtualStack.getPixels(1);
//        Plot plot = new Plot("Test Plot","spectrum","intensity");
//        float[] data = hdf5VirtualStack.getPixels(167);
//        float[] xVal = new float[2048];
//        float maxVal = 0.0f;
//        for ( int i = 0 ; i < 2048 ; i++ ){
//            xVal[i] = i;
//            if ( maxVal < data[i] ){
//                maxVal = data[i];
//            }
//
//        }
//        plot.setLineWidth(2);
//        plot.setLimits(0,data.length,0,maxVal);
//        plot.setColor(Color.red);
//        plot.addPoints(xVal,data,Plot.LINE);
//
//        plot.show();
//        plot.setColor(Color.red);
//        plot.draw();
//
//        hdf5VirtualStack.destroy();
//        while(true){
//            continue;
//        }
//    }

//    @Test
//    public void openSpectrumFromImage(){
//        new ImageJ();
//        final ImagePlus imagePlus = new ImagePlus(pathToIMG4);
//        imagePlus.show();
//
//
//        this.hdf5Handler = new Hdf5Handler();
//        this.hdf5Handler.hdf5_open(filename2);
////        this.hdf5Handler.hdf5_open(filename2);
//        Object[][] datasets = this.hdf5Handler.getDatasetWithDimensionInformation();
//        this.hdf5Handler.hdf5_close();
//
//        final Hdf5VirtualStack hdf5VirtualStack = new Hdf5VirtualStack(this.filename2,(String)datasets[0][26],true);
////        hdf5VirtualStack.setType(Hdf5VirtualStack.spectrum3D);
//        imagePlus.getCanvas().addMouseListener(new MouseListener() {
//            @Override
//            public void mouseClicked(MouseEvent e) {
//
//            }
//
//            @Override
//            public void mousePressed(MouseEvent e) {
//
//            }
//
//            @Override
//            public void mouseReleased(MouseEvent e) {
//                //get Roi modification
//                Rectangle rec = imagePlus.getRoi().getBounds();
//                //Set the new Point in the plotWindow
//                float[] finalSpectre = new float[2048];
//                //Construct the image by getting every points
//                for ( int y = (int) rec.getY(); y < rec.getMaxY() ; y++ ){
//                    for ( int x = (int) rec.getX(); x < rec.getMaxX() ; x++ ){
//                        float[] data = hdf5VirtualStack.getPixels(x + y * imagePlus.getWidth());
//                        for ( int i = 0; i < data.length ; i++ ){
//                            finalSpectre[i] += data[i];
//                        }
//                    }
//                }
//
//                Plot plot = new Plot("Test Plot","spectrum","intensity");
////                float[] data = hdf5VirtualStack.getPixels(167);
//                float[] xVal = new float[2048];
//                float maxVal = 0.0f;
//                for ( int i = 0 ; i < 2048 ; i++ ){
//                    xVal[i] = i;
//                    if ( maxVal < finalSpectre[i] ){
//                        maxVal = finalSpectre[i];
//                    }
//
//                }
//                plot.setLineWidth(2);
//                plot.setLimits(0,finalSpectre.length,0,maxVal);
//                plot.setColor(Color.red);
//                plot.addPoints(xVal,finalSpectre,Plot.LINE);
//
//                plot.show();
//                plot.setColor(Color.red);
//                plot.draw();
////                plot.getImagePlus()
//            }
//
//            @Override
//            public void mouseEntered(MouseEvent e) {
//
//            }
//
//            @Override
//            public void mouseExited(MouseEvent e) {
//
//            }
//        });
//
//        while(true){
//            continue;
//        }
//    }

}
