/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.test.model.computationFunction;

import com.soleil.nanoscopium.rximage.IO.RXImageIO_IJ;
import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.exception.RXImageException;
import com.soleil.nanoscopium.rximage.util.RXUtils;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.ComputationController;
import com.soleil.nanoscopium.rxtomoj.controller.DataController;
import com.soleil.nanoscopium.rxtomoj.controller.SessionController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.ShiftImage;
import com.soleil.nanoscopium.rxtomoj.model.computationFunction.ComputePhaseContrast;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils.StackType;
import com.soleil.nanoscopium.rxtomoj.test.Test_constant;
import ij.ImageJ;
import ij.ImagePlus;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Paths;

/**
 * @author Antoine Bergamaschi
 */
public class ComputePhaseContrast_test {

    @Before
    public void init(){
        new ImageJ();
    }

    @Test
    public void computePhase(){
        System.out.println("computePhase");
        SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
        DataController dataController =  RXTomoJ.getInstance().getModelController().getDataController();
        ComputationController computationController = RXTomoJ.getInstance().getModelController().getComputationController();
        sessionController.openSessionObject(Test_constant.pathToSessionObject143);

//        dataController.setSamplingOption(1, 1);
        Utils.openWaitingLoadBar(computationController.imageCreation(StackType.PHASECONTRAST), "");
        while (true){
            continue;
        }
    }


    @Test
    public void testIterativeFourrierSpace(){
        RXImage x;
        RXImage y;
        try {
            x = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.path143+"X.tif"),false);
            y = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.path143+"Y.tif"),false);

            ComputePhaseContrast computePhaseContrast = new ComputePhaseContrast();
            RXImage result = computePhaseContrast.phaseContrastReconstructionIterativeFFT(x, y);
//            RXImage result = computePhaseContrast.southwellPhaseRecontructionWithDecorallation(x, y, 2);

            RXUtils.RXImageToImagePlus(result).show();

            while (true){
                continue;
            }

        } catch (RXImageException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testIterativeRealSpace(){
        RXImage x;
        RXImage y;
        try {
            x = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.pathMiteX),false);
            y = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.pathMiteY),false);
//            x = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.pathMiteXFromIntensity),false);
//            y = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.pathMiteYFromIntensity),false);
//            x = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.path143+"X1.tif"),false);
//            y = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.path143+"Y1.tif"),false);

//            ComputePhaseContrast computePhaseContrast = new ComputePhaseContrast();
//            RXImage result = ComputePhaseContrast.PhaseRetrievalAlgo.REALSPACE_JACOBI.phaseRetrieval(x, y,0.00001,0.00001,40000);
            RXImage result = ComputePhaseContrast.PhaseRetrievalAlgo.REALSPACE_GAUSSIAN.phaseRetrieval(x, y ,0.00001,0.00001,40000);

            RXUtils.RXImageToImagePlus(result).show();

            while (true){
                continue;
            }

        } catch (RXImageException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testIterativeExperimental(){
        RXImage x;
        RXImage y;
        try {
            x = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.pathMiteX),false);
            y = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.pathMiteY),false);
//            x = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.pathMiteXFromIntensity),false);
//            y = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.pathMiteYFromIntensity),false);
//            x = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.path143+"X1.tif"),false);
//            y = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.path143+"Y1.tif"),false);

//            ComputePhaseContrast computePhaseContrast = new ComputePhaseContrast();
            RXImage result = ComputePhaseContrast.PhaseRetrievalAlgo.EXPERIMENTAL.phaseRetrieval(x, y,0.00001,0.00001,40000);
//            RXImage result = ComputePhaseContrast.PhaseRetrievalAlgo.REALSPACE_GAUSSIAN.phaseRetrieval(x, y);

            RXUtils.RXImageToImagePlus(result).show();

            while (true){
                continue;
            }

        } catch (RXImageException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testFourrierSpace(){
        RXImage x;
        RXImage y;
        try {
//            x = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.pathMiteXFromIntensity),false);
//            y = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.pathMiteYFromIntensity),false);
            x = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.pathMiteXcropped),false);
            y = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.pathMiteYcropped),false);
//            x = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.path143+"X1.tif"),false);
//            y = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.path143+"Y1.tif"),false);
//            x = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.pathMiteXcropped_I),false);
//            y = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.pathMiteYcropped_I),false);

            //Add Mirroring
//            RXImage mirrorX = new ComputePhaseContrast().mirrorImage(x, ShiftImage.MirroringType.ASDI_X);
//            RXImage mirrorY =  new ComputePhaseContrast().mirrorImage(y, ShiftImage.MirroringType.ASDI_Y);
//            RXImage mirrorX = new ComputePhaseContrast().mirrorImage(x, ShiftImage.MirroringType.MDI);
//            RXImage mirrorY =  new ComputePhaseContrast().mirrorImage(y, ShiftImage.MirroringType.MDI);



//            RXImage result = ComputationUtils.cropInStack(ComputePhaseContrast.PhaseRetrievalAlgo.FOURRIER.phaseRetrieval(mirrorX, mirrorY),
//                    x.getWidth(),x.getHeight(),0,
//                    x.getWidth(),x.getHeight(),x.getSize());
//

            RXImage result = ComputePhaseContrast.PhaseRetrievalAlgo.FOURRIER.phaseRetrieval(x, y,0.00001,0.00001,40000);

            RXUtils.RXImageToImagePlus(result).show();

            while (true){
                continue;
            }

        } catch (RXImageException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testZeroCondition(){
        System.out.println("testZeroCondition");
        ImagePlus imagePlusY = new ImagePlus(Test_constant.path125+"X4.tif");
        ImagePlus imagePlusX = new ImagePlus(Test_constant.path125+"Y4.tif");

        new ComputePhaseContrast().testZeroCondition((float[])imagePlusX.getStack().getPixels(1),(float[])imagePlusY.getStack().getPixels(1),
                imagePlusX.getWidth(),imagePlusX.getHeight(),imagePlusX.getStackSize());

        while (true){
            continue;
        }
    }

    @Test
    public void computingTestPhaseContrast(){
        System.out.println("computingTestPhaseContrast");

        ComputePhaseContrast computePhaseContrast = new ComputePhaseContrast();


        ImagePlus imagePlusX = new ImagePlus(Test_constant.path125+"X2.tif");
        ImagePlus imagePlusY = new ImagePlus(Test_constant.path125+"Y2.tif");
        //Converte to RXImage format
        RXImage imagePlusX_rx = RXUtils.ImagePlusToRXImage(imagePlusX);
        RXImage imagePlusY_rx = RXUtils.ImagePlusToRXImage(imagePlusY);

        ShiftImage shiftImage = new ShiftImage();
//        RXImage orientX  = computationController.setStackOrientation(imagePlusX_rx, ComputationUtils.OrientationType.XZY);
//        RXImage orientY  = computationController.setStackOrientation(imagePlusY_rx, ComputationUtils.OrientationType.XZY);


        RXImage mirrorX = shiftImage.mirroring(imagePlusX_rx, ShiftImage.MirroringType.MDI);
        RXImage mirrorY = shiftImage.mirroring(imagePlusY_rx, ShiftImage.MirroringType.MDI);

        int newWidth = (mirrorX.getWidth() * 4) + 1;
        int newHeight = (mirrorY.getHeight() * 4) + 1;

        mirrorX.setData(ComputationUtils.padDataWithCosineCenter(mirrorX, newWidth, newHeight));
        mirrorY.setData(ComputationUtils.padDataWithCosineCenter(mirrorY, newWidth, newHeight));

        mirrorX.setDims(new int[]{newWidth, newHeight, mirrorX.getSize()});
        mirrorY.setDims(new int[]{newWidth, newHeight, mirrorY.getSize()});

        RXUtils.RXImageToImagePlus(mirrorX).show();
        RXUtils.RXImageToImagePlus(mirrorY).show();

//        ArrayList<RXImage> mirroredStack = new ArrayList<>(2);
//        mirroredStack.add(mirrorX);
//        mirroredStack.add(mirrorY);
//
//        RXImage re = computePhaseContrast.phaseContrastReconstructionT(mirroredStack.get(0), mirroredStack.get(1), mirrorX.getHeight(), mirrorX.getWidth());
//        ImagePlus imagePlusAbsorption = RXUtils.RXImageToImagePlus(re);
//        imagePlusAbsorption.setTitle("PhaseContrast");
//        imagePlusAbsorption.show();

        while(true){
            continue;
        }
    }
}
