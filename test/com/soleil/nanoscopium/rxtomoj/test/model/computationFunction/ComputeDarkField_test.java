/******************************************************************************
 * Copyright (c) 2013-2016 Synchrotron SOLEIL                                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated  documentation files                  *
 * (the "Software"), to deal in the Software without restriction,             *
 * including without limitation the rights to use, copy, modify, merge,       *
 * publish, distribute, sublicense, and/or sell copies of the Software,       *
 * and to permit persons to whom the Software is furnished to                 *
 * do so, subject to the following conditions:                                *
 *                                                                            *
 * The above copyright notice and this permission notice shall be             *
 * included in all copies or substantial portions of the Software.            *
 *                                                                            *
 * the software is provided "as is", without warranty of any kind, express or *
 * implied, including but not limited to the warranties of merchantability,   *
 * fitness for a particular purpose and noninfringement. in no event shall    *
 * the authors or copyright holders be liable for any claim,                  *
 * damages or other liability, whether in an action of contract,              *
 * tort or otherwise, arising from, out of or in connection with the software *
 * or the use or other dealings in the software.                              *
 ******************************************************************************/

package com.soleil.nanoscopium.rxtomoj.test.model.computationFunction;

import com.soleil.nanoscopium.rximage.IO.RXImageIO_IJ;
import com.soleil.nanoscopium.rximage.RXImage;
import com.soleil.nanoscopium.rximage.exception.RXImageException;
import com.soleil.nanoscopium.rximage.util.RXUtils;
import com.soleil.nanoscopium.rxtomoj.RXTomoJ;
import com.soleil.nanoscopium.rxtomoj.controller.ComputationController;
import com.soleil.nanoscopium.rxtomoj.controller.DataController;
import com.soleil.nanoscopium.rxtomoj.controller.SessionController;
import com.soleil.nanoscopium.rxtomoj.gui.utils.JPanelPlot;
import com.soleil.nanoscopium.rxtomoj.gui.utils.Utils;
import com.soleil.nanoscopium.rxtomoj.model.ShiftImage;
import com.soleil.nanoscopium.rxtomoj.model.computationFunction.ComputeDarkField;
import com.soleil.nanoscopium.rxtomoj.model.utils.ComputationUtils;
import com.soleil.nanoscopium.rxtomoj.model.utils.FittingUtils;
import com.soleil.nanoscopium.rxtomoj.test.Test_constant;
import ij.ImageJ;
import ij.gui.Plot;
import org.junit.Test;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Paths;

/**
 * Created by bergamaschi on 19/11/2014.
 */
public class ComputeDarkField_test {
    @Test
    public void computeTestDarkField() throws InterruptedException {
        System.out.println("computeTestDarkField");
        SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
        DataController dataController =  RXTomoJ.getInstance().getModelController().getDataController();
        ComputationController computationController = RXTomoJ.getInstance().getModelController().getComputationController();
        sessionController.openSessionObject(Test_constant.pathToSessionObject143);

//        dataController.setSamplingOption(1,1);

        Utils.openWaitingLoadBar(computationController.imageCreation(ComputationUtils.StackType.DARKFIELD),"");

        while(true){
            Thread.sleep(100);
            continue;
        }
    }


    @Test
    public void computePartialDarkField() throws InterruptedException {
        System.out.println("computeTestDarkField");
        SessionController sessionController = RXTomoJ.getInstance().getModelController().getSessionController();
        DataController dataController =  RXTomoJ.getInstance().getModelController().getDataController();
        ComputationController computationController = RXTomoJ.getInstance().getModelController().getComputationController();
        sessionController.openSessionObject(Test_constant.pathToSessionObject143);

//        dataController.setSamplingOption(1,1);
        dataController.removeAllDarkFieldRange();
        dataController.addDarkFieldRange("TestTu",new int[]{30,40});

        Utils.openWaitingLoadBar(computationController.imageCreation(ComputationUtils.StackType.DARKFIELD),"");

        while(true){
            Thread.sleep(100);
            continue;
        }
    }

    @Test
    public void testIntegrationSize(){
        float[] data = new float[10000];
        float[] result = new float[10000];
        int[] range = new int[]{10,25,20,50};
        int[] range2 = new int[]{range[0]*range[0],range[1]*range[1],range[2]*range[2],range[3]*range[3]};
        new ImageJ();
//        ComputeDarkField.darkFieldComputationCircleT(data, 100, 100, 1, result, range2, 50.0f, 50.0f);
        RXImage rxImage = new RXImage(100,100,1);
        rxImage.setData(result);

        RXUtils.RXImageToImagePlus(rxImage).show();
        while (true){
            continue;
        }
    }


    @Test
    public void testIntegrationSize2(){
        float[] data = new float[10000];
        float[] result = new float[10000];
        int[] range = new int[]{20,45,10,25};
        int[] range2 = new int[]{range[0],range[1],range[2],range[3]};
        new ImageJ();
//        ComputeDarkField.darkFieldComputationSquareT(data, 100, 100, 1, result, range2, 50.0f, 50.0f);
        RXImage rxImage = new RXImage(100,100,1);
        rxImage.setData(result);

        RXUtils.RXImageToImagePlus(rxImage).show();
        while (true){
            continue;
        }
    }

    @Test
    public void testCreateOrientationMask(){
        new ImageJ();
        int widht = 1000;
        int heihgt = 1000;
        float centerx = 500;
        float centery = 500;

        int numberOfSegment = 1024;

        int[] intRes = ComputeDarkField.createOrientationMask(widht,heihgt,numberOfSegment,centerx,centery);

        RXImage rxImage = new RXImage(widht,heihgt,1);

        float[] result = new float[intRes.length];
        for ( int i = 0 ; i < intRes.length ; i++ ){
            result[i] = intRes[i];
        }

        rxImage.setData(result);

        RXUtils.RXImageToImagePlus(rxImage).show();
        while (true){
            continue;
        }
    }

    @Test
    public void retrieveAngleFromSine(){
        float[] data;
        float[] dataMin1 = new float[4];
        RXImage rxImage = null;
        new ImageJ();

        JPanelPlot panelPlot = new JPanelPlot();
        try {
//            rxImage = (RXImage) new RXImageIO_IJ().load(Paths.get("D:\\project\\rxbuild\\RX-tomoJ\\test\\resources\\image\\darkField\\orientation.tif"),false);
            rxImage = (RXImage) new RXImageIO_IJ().load(Paths.get(Test_constant.pathOrientationImage2),false);
//            rxImage = (RXImage) new RXImageIO_IJ().load(Paths.get("D:\\project\\rxbuild\\RX-tomoJ\\test\\" +
//                    "resources\\image\\darkField\\orientation16.tif"),false);
//            rxImage = (RXImage) new RXImageIO_IJ().load(Paths.get("D:\\project\\rxbuild\\RX-tomoJ\\test\\" +
//                    "resources\\image\\darkField\\orientation32.tif"),false);
        } catch (RXImageException e) {
            e.printStackTrace();
        }


//        int enlarge = 100;
        for ( int k = 0 ; k < rxImage.getSize()/2 ; k++ ) {
            data = rxImage.copySlice(k);

//            float[] valX2 = new float[data.length];
//            for (int i = 0; i < valX2.length; i++) {
//                valX2[i] = (float) (i*((2*Math.PI)/data.length));
//            }

//            panelPlot.addData(valX2, data, Color.BLACK);
//
//            FittingUtils fittingUtils = new FittingUtils();
//            fittingUtils.setUseMobileMean(false);
//            fittingUtils.setUseMobileMean(true);
//            //Bug under 2
//            fittingUtils.setMobileWindow(8);
//            fittingUtils.fitCurveOnFullLengthSine(valX2, data);
//            System.out.println(""+fittingUtils.getSineParameters()[2]);
//
//
//            if ( fittingUtils.getSineParameters()[2] < 6.28 ) {
//                float[] fitted = fittingUtils.getFittedValue(valX2);
//                panelPlot.addData(valX2, fitted, Color.RED);
//            }else{
//                float[] fitted = fittingUtils.getFittedValue(valX2);
//                panelPlot.addData(valX2, fitted, Color.BLUE);
//            }

            //Mini dataVersion
            System.arraycopy(data,0,dataMin1,0,dataMin1.length);

            float[] valX2 = new float[dataMin1.length];
            for (int i = 0; i < valX2.length; i++) {
                valX2[i] = (float) (i*((2*Math.PI)/dataMin1.length));
            }

            FittingUtils fittingUtils = new FittingUtils();
            fittingUtils.setUseMobileMean(false);
            fittingUtils.fitCurveOnFullLengthSine(valX2, dataMin1,1);
            System.out.println(""+fittingUtils.getSineParameters()[2]);

            panelPlot.addData(valX2, dataMin1, Color.BLACK);
            float[] fitted2 = fittingUtils.getFittedValue(valX2);
            panelPlot.addData(valX2, fitted2, Color.RED);

//            //Test Transform Data to Enlarge
//            float[] bigData = new float[(data.length)*enlarge];
//            for ( int i = 0 ; i < data.length ; i++ ){
//                bigData[i*enlarge] = data[i];
//                if ( i < data.length - 1) {
//                    for (int m = 1; m < enlarge; m++) {
//                        //System.out.println((i*10) + m);
//                        bigData[(i*enlarge) + m] = (float) (bigData[(i*enlarge) + m - 1] + ((data[i + 1]-data[i])  * (1.0d / enlarge)));
//                    }
//                }else{
//                    for (int m = 1; m < enlarge; m++) {
//                        //System.out.println((i*10) + m);
//                        bigData[(i*enlarge) + m] = (float) (bigData[(i*enlarge) + m - 1] + ((data[0]-data[i])  * (1.0d / enlarge)));
//                    }
//                }
//            }
//
//            float[] valX = new float[bigData.length];
//            for (int i = 0; i < valX.length; i++) {
//                valX[i] = (float) (i*((2*Math.PI)/bigData.length));
//            }
//
//            float[] valX2 = new float[data.length];
//            for (int i = 0; i < valX2.length; i++) {
//                valX2[i] = (float) (i*((2*Math.PI)/data.length));
//            }
//
//            float[] valXp = new float[bigData.length];
//            for (int i = 0; i < valXp.length; i++) {
//                valXp[i] = (float) (i*((2*Math.PI)/bigData.length));
//            }
//
////            float[] valXp = new float[360];
////            for (int i = 0; i < valXp.length; i++) {
////                valXp[i] = (float) ((float) i*Math.toRadians(360.0d/valXp.length));
////            }
////
//            panelPlot.addData(valX, bigData, Color.BLACK);


//            float[] fitted2 = fittingUtils.getFittedValue(valX);
//            panelPlot.addData(valX, fitted2, Color.blue);
        }

//        panelPlot.addData(x,yCoputed, Color.red);
//        panelPlot.addData(x,yReal,Color.black);

        JFrame frame = new JFrame();
        frame.setSize(500, 300);
        frame.setContentPane(panelPlot);

        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        while (true){
            continue;
        }

    }
}
